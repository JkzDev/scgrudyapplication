package com.scg.rudy.data.remote;


import com.scg.rudy.model.pojo.customer_detail.CustomerDetail;
import com.scg.rudy.utils.Utils;
import java.util.List;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DekDroidDev on 30/3/2018 AD.
 */
    public interface RudyService {
    String url = ApiEndPoint.Url;

    @FormUrlEncoded
    @POST(url + "{lang}/login")
    Call<String> login(
            @Field("userID") String userID,
            @Field("password") String password,
            @Path("lang") String lang);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{groupId}?act=addPGPic")
    Call<String> addPGPic(
            @Path("lang") String lang,
            @Path("groupId") String groupId,
            @Field("pic") String pic);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{projectId}?act=editLocation")
    Call<String> updateLocation(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Field("lat_lng") String lat_lng,
            @Field("project_address") String project_address);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{projectId}?act=editProject")
    Call<String> updateDepartment(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Field("project_name") String project_name,
            @Field("project_type_id") String project_type_id,
            @Field("phase_id") String phase_id,
            @Field("units") String units,
            @Field("unit_area") String unit_area,
            @Field("unit_budget") String unit_budget,
            @Field("project_stories") String project_stories);

    @FormUrlEncoded
    @POST(url + "{lang}/customer/{userID}?act=pic_v2")
    Call<String> addPicCustomer(
            @Path("lang") String lang,
            @Path("userID") String userID,
            @Query("customer_id") String customerId,
            @Field("pic") String pic);

    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{transId}?act=editQODetail")
    Call<String> reviseQo(
            @Path("lang") String lang,
            @Path("transId") String transId,
            @Field("discount") String discount,
            @Field("discount_type") String discount_type,
            @Field("tax_type") String tax_type,
            @Field("delivery_cost") String delivery_cost,
            @Field("payment_type") String payment_type,
            @Field("credit_day") String credit_day,
            @Field("credit_type") String credit_type);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{projectId}?act=editCustomerProject_v2")
    Call<String> UpdateCustomerProject(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Field("customer_type") String customer_type,
            @Field("customer_company") String customer_company,
            @Field("customer_name") String customer_name,
            @Field("customer_phone") String customer_phone,
            @Field("customer_code") String customer_code,
            @Field("customer_email") String customer_email,
            @Field("customer_line") String customer_line,
            @Field("customer_tax_no") String customer_tax_no,
            @Field("customer_note") String customer_note,
            @Field("house_owner_name") String house_owner_name,
            @Field("house_owner_phone") String house_owner_phone,
            @Field("house_owner_line") String house_owner_line,
            @Field("house_owner_note") String house_owner_note,
            @Field("project_owner_name") String project_owner_name,
            @Field("project_owner_phone") String project_owner_phone,
            @Field("project_owner_line") String project_owner_line,
            @Field("project_owner_note") String project_owner_note);

    @POST(url + "{lang}/project/{userId}?act=del")
    Call<String> deleteProject(
            @Path("lang") String lang,
            @Path("userId") String userId,
            @Query("id") String id);

    @FormUrlEncoded
    @POST(url + "{lang}/project?act=add_v2")
    Call<String> addNewProject(
            @Path("lang") String lang,
            @Field("pic[]") List<String> pic,
            @Field("user_id") String user_id,
            @Field("project_name") String project_name,
            @Field("project_address") String project_address,
            @Field("lat_lng") String lat_lng,
            @Field("project_type_id") String project_type_id,
            @Field("phase_id") String phase_id,
            @Field("units") String units,
            @Field("unit_area") String unit_area,
            @Field("unit_budget") String unit_budget,
            @Field("project_stories") String project_stories,
            @Field("customer_type") String customer_type,
            @Field("customer_name") String customer_name,
            @Field("customer_code") String customer_code,
            @Field("champ_customer_code") String champ_customer_code,
            @Field("customer_company") String customer_company,
            @Field("customer_phone") String customer_phone,
            @Field("customer_email") String customer_email,
            @Field("customer_line") String customer_line,
            @Field("customer_tax_no") String customer_tax_no,
            @Field("customer_note") String customer_note,
            @Field("house_owner_name") String house_owner_name,
            @Field("house_owner_phone") String house_owner_phone,
            @Field("house_owner_line") String house_owner_line,
            @Field("House_owner_note") String House_owner_note,
            @Field("project_owner_name") String project_owner_name,
            @Field("project_owner_phone") String project_owner_phone,
            @Field("project_owner_line") String project_owner_line,
            @Field("project_owner_note") String project_owner_note,
            @Field("project_group_id") String project_group_id);

    @POST(url + "{lang}/catalogues/{projectId}?nocache=1")
    Call<String> removeCacheUrl(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Query("cate_id") String cate_id);

    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{transId}?act=delProduct")
    Call<String> deletePr(
            @Path("lang") String lang,
            @Path("transId") String transId,
            @Field("product_id") String product_id,
            @Field("project_id") String project_id);

    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{transId}?act=editPrice")
    Call<String> editPrice(
            @Path("lang") String lang,
            @Path("transId") String transId,
            @Field("project_id") String project_id,
            @Field("product_id") String product_id,
            @Field("price") String price,
            @Field("user_id") String user_id,
            @Field("qty") String qty,
            @Field("discount") String discount,
            @Field("discount_type") String discount_type);

    @FormUrlEncoded
    @POST(url + "{lang}/customer/{user_id}?act=edit_v2")
    Call<String> updateCustomer(
            @Path("lang") String lang,
            @Path("user_id") String user_id,
            @Field("customer_name") String customer_name,
            @Field("customer_company") String customer_company,
            @Field("customer_phone") String customer_phone,
            @Field("customer_line") String customer_line,
            @Field("customer_code") String customer_code,
            @Field("customer_type") String customer_type,
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{transId}?act=editNote")
    Call<String> commentQo(
            @Path("lang") String lang,
            @Path("transId") String transId,
            @Field("note") String note);


    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{transId}?act=toQO")
    Call<String> getPrToQo(
            @Path("lang") String lang,
            @Path("transId") String transId,
            @Field("product_id") String product_id,
            @Field("cus_type") String cus_type,
            @Field("discount") String discount,
            @Field("discount_type") String discount_type,
            @Field("tax_type") String tax_type,
            @Field("delivery_cost") String delivery_cost,
            @Field("payment_type") String payment_type,
            @Field("credit_day") String credit_day,
            @Field("credit_type") String credit_type);


    @POST(url + "{lang}/favclass/{projectId}?act=add")
    Call<String> addClassFav(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Query("class_id") String classId,
            @Query("user_id") String user_id);

    @POST(url + "{lang}/transaction/{projectId}")
    Call<String> updateTransId(
            @Path("lang") String lang,
            @Path("projectId") String projectId);

    @POST(url + "{lang}/checkin/{checkInID}/?act=del")
    Call<String> deleteCheckIn(
            @Path("lang") String lang,
            @Path("checkInID") String checkInID);

    @POST(url + "{lang}/favclass/{projectId}?act=del")
    Call<String> removeFAVClass(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Query("class_id") String class_id);

    @POST(url + "{lang}/fav/{projectId}?act=add")
    Call<String> addFAV(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Query("product_id") String product_id,
            @Query("user_id") String user_id);

    @POST(url + "{lang}/fav/{projectId}?act=del")
    Call<String> removeFAV(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Query("product_id") String product_id);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{projectID}?act=addNote")
    Call<String> addNote(
            @Path("lang") String lang,
            @Path("projectID") String projectID,
            @Field("note") String note);

    @POST(url + "{lang}/transaction/{project_id}")
    Call<String> getTransactionId(
            @Path("lang") String lang,
            @Path("project_id") String project_id);

    @POST(url + "{lang}/transaction/{transaction}?act=detail")
    Call<String> getTransectionDetail(
            @Path("lang") String lang,
            @Path("transaction") String transaction);

    @FormUrlEncoded
    @POST(url + "{lang}/transaction/{project_id}?act=pr")
    Call<String> addBasket(
            @Path("lang") String lang,
            @Path("project_id") String project_id,
            @Field("product_id") String product_id,
            @Field("qty") String qty,
            @Field("price") String price,
            @Field("user_id") String user_id,
            @Field("note") String note,
            @Field("unit_id") String unit_id,
            @Field("discount") String discount,
            @Field("discount_type") String discount_type);

    @POST(url + "{lang}/sku/{skuid}")
    Call<String> getSkuDetail(
            @Path("lang") String lang,
            @Path("skuid") String skuid);

    @POST(url + "{lang}/checkin/{checkinID}/?act=delGallery")
    Call<String> deletePicCheckIn(
            @Path("lang") String lang,
            @Path("checkinID") String checkinID);

    @FormUrlEncoded
    @POST(url + "{lang}/checkin/?act=checkVisitProject")
    Call<String> checkIsCheckgin(
            @Path("lang") String lang,
            @Field("project_id") String project_id,
            @Field("lat") String lat,
            @Field("lng") String lng);

    @FormUrlEncoded
    @POST(url + "{lang}/checkin/?act=add")
    Call<String> postCheckin(
            @Path("lang") String lang,
            @Field("project_id") String project_id,
            @Field("type") String type,
            @Field("detail") String detail,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{project_id}?act=addPic_multi")
    Call<String> addPic(
            @Path("lang") String lang,
            @Path("project_id") String project_id,
            @Field("pic[]") List<String> pic);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{project_id}?act=delPic")
    Call<String> deletePic(
            @Path("lang") String lang,
            @Path("project_id") String project_id,
            @Field("pic") String pic);

    @POST(url + "{lang}/fav/{project_id}?act=list")
    Call<String> getFAV(
            @Path("lang") String lang,
            @Path("project_id") String project_id);

    @FormUrlEncoded
    @POST(url + "{lang}/checkin/?act=addGalleryMulti")
    Call<String> addPicCheckin(
            @Path("lang") String lang,
            @Field("checkin_id") String checkin_id,
            @Field("pic[]") List<String> pic);

    @POST(url + "{lang}/sku/{skuid}")
    Call<String> productUnit(
            @Path("lang") String lang,
            @Path("skuid") String skuid);

    @POST(url + "{lang}/project/{user_id}/?act=new-eyes-project-count")
    Call<String> countEye(
            @Path("lang") String lang,
            @Path("user_id") String user_id);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{uId}")
    Call<String> getMyproject(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String list_v2,
            @Query("page") String page,
            @Field("search") String search,
            @Field("project_type_id") String project_type_id,
            @Field("phase_id") String phase_id,
            @Field("assign_user") String assign_user,
            @Field("previous_date_start") String previous_date_start,
            @Field("sort_field") String sort_field);



    @FormUrlEncoded
    @POST(url + "{lang}/customer/{uId}")
    Call<String> getCustomerList(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String list,
            @Query("page") String page,
            @Field("search") String search);



//    @FormUrlEncoded
    @POST(url + "{lang}/customer/{uId}")
    Call<String> getCustomerDetailProjectList(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String plist,
            @Query("customer_id") String customer_id,
            @Query("page") String page);

//    https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/customer/2?act=translist&customer_id=24030&page=1

    @POST(url + "{lang}/customer/{uId}")
    Call<String> getSaleCustomerList(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String translist,
            @Query("customer_id") String customer_id,
            @Query("page") String page);




    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> catalogues(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String v2,
            @Query("type_id") String type_id);


    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> catePhase(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String cate_view);


    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> phaseList(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String v2,
            @Query("phase_id") String phase_id);

    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> groupProject(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String v2,
            @Query("sub_phase_id") String sub_phase_id);

    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> classProject(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String v2,
            @Query("cate_id") String cate_id,
            @Query("sub_phase_id") String sub_phase_id);


    @GET(url + "{lang}/catalogues/{pId}")
    Call<String> classByPhase(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("cate_id") String cate_id);


    @GET(url + "{lang}/transaction/{tId}")
    Call<String> getPdfName(
            @Path("lang") String lang,
            @Path("tId") String tId,
            @Query("act") String get_pdf_name);

    @GET(url + "{lang}/transaction/{tId}")
    Call<String> qoToSo(
            @Path("lang") String lang,
            @Path("tId") String tId,
            @Query("act") String toSO);



    @GET(url + "{lang}/project/{pId}")
    Call<String> projectDetail(
            @Path("lang") String lang,
            @Path("pId") String pId);

    @GET(url + "{lang}/project/{pId}")
    Call<String> projectGroupDetail(
            @Path("lang") String lang,
            @Path("pId") String pId,
            @Query("act") String getPGdetail
    );


    @FormUrlEncoded
    @POST(url + "{lang}/project/{pId}?act=addAudio")
    Call<String> AddAudio(
            @Field("audio") String audio,
            @Path("lang") String lang,
            @Path("pId") String pId);

//    @GET(url +"{lang}/customer/{uId}")
//    Call<Customer> getCustomer(
//            @Path("lang") String lang,
//            @Path("uId") String uId,
//            @Query("act") String list);

    @FormUrlEncoded
    @POST(url + "{lang}/project/{pId}?act=delAudio")
    Call<String> DeleteAudio(
            @Field("file_name") String file_name,
             @Path("lang") String lang,
            @Path("pId") String pId);

    @GET(url + "{lang}/project?act=projectType")
    Call<String> projectType(
            @Path("lang") String lang);

    @GET(url + "{lang}/admin/{user_id}/?act=staff")
    Call<String> getStaff(
            @Path("lang") String lang,
            @Path("user_id") String user_id);

    @GET(url +"{lang}/time/?act=previous_range_list")
    Call<String> getDateFrom(
            @Path("lang") String lang);

    @GET(url +"{lang}/forgotpassword")
    Call<String> forgotPassword(
            @Path("lang") String lang,
            @Query("userID") String pId);

    @GET(url +"{lang}/catalogues/{projecId}")
    Call<String> callType(
            @Path("lang") String lang,
            @Path("projecId") String projectId,
            @Query("type_id") String typeId);

    @GET(url +"{lang}/catalogues/{projecId}")
    Call<String> callPhase(
            @Path("lang") String lang,
            @Path("projecId") String projectId,
            @Query("phase_id") String phaseId);

    @GET(url +"{lang}/catalogues/{projecId}")
    Call<String> callSubPhase(
            @Path("lang") String lang,
            @Path("projecId") String projectId,
            @Query("sub_phase_id") String subPhaseId);

    @GET(url +"{lang}/catalogues/{lang}")
    Call<String> callCategorie(
            @Path("lang") String lang,
            @Path("projecId") String projectId,
            @Query("cate_id") String cateId);

    @GET(url +"{lang}/sku/{shopID}")
    Call<String> callSkuList(
            @Path("lang") String lang,
            @Path("shopID") String shopID,
            @Query("act") String byClass_v2,
            @Query("user_id") String user_id,
            @Query("project_id") String project_id,
            @Query("class_id") String class_id,
            @Query("page") String page,
            @Query("orderBy") String popularity,
            @Query("subphase_id") String subphase_id,
            @Query("multiFilter") String filter);

    @GET(url +"{lang}/sku/{shopID}")
    Call<String> callPromoList(
            @Path("lang") String lang,
            @Path("shopID") String shopID,
            @Query("act") String promotion,
            @Query("user_id") String user_id,
            @Query("project_id") String project_id,
            @Query("class_id") String class_id,
            @Query("page") String page,
            @Query("orderBy") String popularity,
            @Query("subphase_id") String subphase_id,
            @Query("multiFilter") String filter);


    @GET(url +"{lang}/sku/{shopID}")
    Call<String> callRecommendList(
            @Path("lang") String lang,
            @Path("shopID") String shopID,
            @Query("act") String promotion,
            @Query("user_id") String user_id,
            @Query("project_id") String project_id,
            @Query("class_id") String class_id,
            @Query("page") String page,
            @Query("orderBy") String popularity,
            @Query("subphase_id") String subphase_id,
            @Query("multiFilter") String filter);



    @FormUrlEncoded
    @POST(url +"{lang}/sku/{shopID}")
    Call<String> SearchSkuList(
            @Field("search") String keyword,
            @Path("lang") String lang,
            @Path("shopID") String shopID,
            @Query("act") String search_v2,
            @Query("project_id") String project_id,
            @Query("user_id") String user_id,
            @Query("page") String page,
            @Query("orderBy") String popularity,
            @Query("multiFilter") String filter);

    @GET(url +"{lang}/transaction/{transectionId}") //?act=detail
    Call<String> callTransactionList(
            @Path("lang") String lang,
            @Path("transectionId") String transectionId,
            @Query("act") String detail);


    @GET(url +"{lang}/fav/{projecId}?act=list")
    Call<String> callFavoriteList(
            @Path("lang") String lang,
            @Path("projecId") String projectId);


    @GET(url +"{lang}/customer/{cId}")
    Call<CustomerDetail> getCustomerDetail(
            @Path("lang") String lang,
            @Path("cId") String cId,
            @Query("act") String detail_v2,
            @Query("customer_id") String customer_id);

    @GET(url +"{lang}/dashboard/{uId}")
    Call<String> getDashboard(
            @Path("lang") String lang,
            @Path("uId") String uId);

    @GET(url +"{lang}/transaction/{shopid}?act=creditTypeList")
    Call<String> getCreditType(
            @Path("lang") String lang,
            @Path("shopid") String uId);

    @GET(url +"{lang}/transaction/{transaction_id}?act=get_template_note")
    Call<String> getCommnet(
            @Path("lang") String lang,
            @Path("transaction_id") String transaction_id);

    @GET(url +"{lang}/checkin/{checkInID}/?act=detail")
    Call<String> getCheckInDetail(
            @Path("lang") String lang,
            @Path("checkInID") String cId);

    @FormUrlEncoded
    @POST(url +"{lang}/checkin")
    Call<String> getCheckinHistory(
            @Path("lang") String lang,
            @Field("project_id") String projectId);

    @FormUrlEncoded
    @POST(url +"{lang}/checkin/{checkInID}/?act=edit")
    Call<String> postEditCheckin(
            @Path("lang") String lang,
            @Path("checkInID") String checkInID,
            @Field("project_id") String projectId,
            @Field("type") String type,
            @Field("detail") String detail,
            @Field("distance") String distance,
            @Field("lat_lng") String latlng
    );

    @FormUrlEncoded
    @POST(url +"{lang}/project/{projectId}/?act=updateprocess")
    Call<String> postUpdateProcess(
            @Path("lang") String lang,
            @Path("projectId") String projectId,
            @Field("phase_id") String phase_id,
            @Field("sub_phase_id_list") String sub_phase_id_list,
            @Field("status_list") String status_list
            );

    @FormUrlEncoded
    @POST(url +"{lang}/calendar")
    Call<String> getCalendar(
            @Path("lang") String lang,
            @Field("user_id") String userID,
            @Field("y") String year,
            @Field("m") String month);

    @GET(url +"{lang}/calendar/{cld_id}")
    Call<String> getCalendarDetail(
            @Path("lang") String lang,
            @Path("cld_id") String cld_id,
            @Query("act") String detail);

    @GET(url +"{lang}/project/?act=projectGroup")
    Call<String> getProjectGroup(
                    @Path("lang") String lang);


    @GET(url +"{lang}/project/?act=contractType")
    Call<String> getContractType(
            @Path("lang") String lang);


    @FormUrlEncoded
    @POST(url +"{lang}/project/?act=add_project_group")
    Call<String> newGroupProject(
            @Path("lang") String lang,
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("lat_lng") String lat_lng,
            @Field("project_address") String project_address,
            @Field("project_group_list_id") String project_group_list_id,
            @Field("developer_id") String developer_id,
            @Field("developer_name_other") String developer_name_other,
            @Field("start_date") String start_date,
            @Field("end_date") String end_date,
            @Field("customer_type") String customer_type,
            @Field("customer_name") String customer_name,
            @Field("customer_code") String customer_code,
            @Field("champ_customer_code") String champ_customer_code,
            @Field("customer_company") String customer_company,
            @Field("customer_phone") String customer_phone,
            @Field("customer_email") String customer_email,
            @Field("customer_line") String customer_line,
            @Field("customer_tax_no") String customer_tax_no,
            @Field("customer_note") String customer_note,
            @Field("pic[]") List<String> pic
    );


    @GET(url +"{lang}/project/{uId}")
    Call<String> getMapDupplicate(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String byPGLocation,
            @Query("lat") String lat,
            @Query("lng") String lng
    );

//    @GET(url +"{lang}/project/?act=contractType")
//    Call<ContractTypePojo> getContractType(
//            @Path("lang") String lang);



    @FormUrlEncoded
    @POST(url +"{lang}/project/{project_group_id}/?act=editPGProject")
    Call<String> postEditProjectGroup(
            @Path("lang") String lang,
            @Path("project_group_id") String project_group_id,
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("project_group_list_id") String project_group_list_id,
            @Field("developer_id") String developer_id,
            @Field("developer_name_other") String developer_name_other,
            @Field("start_date") String start_date,
            @Field("end_date") String end_date,
            @Field("customer_type") String customer_type,
            @Field("customer_name") String customer_name,
            @Field("customer_code") String customer_code,
            @Field("champ_customer_code") String champ_customer_code,
            @Field("customer_company") String customer_company,
            @Field("customer_phone") String customer_phone,
            @Field("customer_email") String customer_email,
            @Field("customer_line") String customer_line,
            @Field("customer_tax_no") String customer_tax_no,
            @Field("customer_note") String customer_note
    );

    @FormUrlEncoded
    @POST(url +"{lang}/project/{project_group_id}?act=editPGCustomerProject")
    Call<String> postEditCustomerProjectGroup(
            @Path("lang") String lang,
            @Path("project_group_id") String project_group_id,
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("project_group_list_id") String project_group_list_id,
            @Field("developer_id") String developer_id,
            @Field("developer_name_other") String developer_name_other,
            @Field("start_date") String start_date,
            @Field("end_date") String end_date,
            @Field("customer_type") String customer_type,
            @Field("customer_name") String customer_name,
            @Field("customer_code") String customer_code,
            @Field("champ_customer_code") String champ_customer_code,
            @Field("customer_company") String customer_company,
            @Field("customer_phone") String customer_phone,
            @Field("customer_email") String customer_email,
            @Field("customer_line") String customer_line,
            @Field("customer_tax_no") String customer_tax_no,
            @Field("customer_note") String customer_note
    );




    @GET(url +"{lang}/project/{project_group_id}/?act=getPGdetail")
    Call<String> getPGdetail(
            @Path("lang") String lang,
            @Path("project_group_id") String project_group_id
    );

    @FormUrlEncoded
    @POST(url +"{lang}/project/{uId}")
    Call<String> getGroupProject(
            @Path("lang") String lang,
            @Path("uId") String uId,
            @Query("act") String pglist,
            @Query("page") String page,
            @Field("search") String search,
            @Field("project_type_id") String project_type_id,
            @Field("developer_id") String devId);

//    /app/merudy/MTIzcXdlMTIz6969/{lang}/wallet/{shopID}
//
    @FormUrlEncoded
    @POST(url +"{lang}/wallet/{shopID}")
    Call<String> getSaleWallet(
            @Path("lang") String lang,
            @Path("shopID") String shopID,
            @Query("page") String page,
            @Field("y") String year,
            @Field("m") String month);

    @FormUrlEncoded
    @POST(url +"{lang}/sku/{shopId}/?act=promotion_detail")
    Call<String> getPromotionDetail(
            @Path("lang") String lang,
            @Path("shopId") String shopId,
            @Field("promotion_detail_id") String promotionDetailId);


    @FormUrlEncoded
    @POST(url+"project/?act=list_developer_by_name")
    Call<String> getListDeveloerByName(
            @Path("lang") String lang,
            @Field("name") String name
    );

    @FormUrlEncoded
    @POST(url+"project/{project_group_contact_id}/?act=getPGContactdetail")
    Call<String> getPGContactdetail(
            @Path("lang") String lang,
            @Path("project_group_contact_id") String project_group_contact_id,
            @Field("name") String name
    );

    @GET(url + "{lang}/project/{userid}?act=new-byLocation&type=all&nocache=1")
    Observable<String> location(
            @Path("lang") String lang,
            @Path("userid") String userid,
            @Query("lat") String latt,
            @Query("lng") String longt
    );

    @GET(url + "{lang}/project/{userid}?act=new-byLocation&type=all")
    Call<String> locationCache(
            @Path("lang") String lang,
            @Path("userid") String userid,
            @Query("lat") String latt,
            @Query("lng") String longt
    );

    @FormUrlEncoded
    @POST("/shop/merudy/MTIzcXdlMTIz6969/{lang}/op/{user_id}?act=pull-project")
    Call<String> pullEye(
            @Path("lang") String lang,
            @Path("user_id") String user_id,
            @Field("biz_id") String biz_id,
            @Field("project_id") String project_id,
            @Field("shop_id") String shop_id,
            @Field("is_eyes") boolean is_eyes
    );

    @FormUrlEncoded
    @POST(url +"{lang}/sku/{shopid}?act=recommend_biz_detail")
    Call<String> getReccomendBizDetail(
            @Path("lang") String lang,
            @Path("shopid") String shopid,
            @Field("product_id") String product_id
        );
}
