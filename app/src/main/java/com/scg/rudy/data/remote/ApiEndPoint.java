package com.scg.rudy.data.remote;

import com.scg.rudy.utils.Utils;

/**
 * Created by DekDroidDev on 5/10/2017 AD.
 */

public class ApiEndPoint {
    public static  String LANG = Utils.APP_LANGUAGE;
    public static  String isDev = ""; // isDev = "/" , isProduction = ""
    public static  final String SEARCH_NOR = "";
    public static  final String SEARCH_LTH = "low2high";
    public static  final String SEARCH_HTL = "high2low";

    public static final String HOST = "https://api.merudy.com"; ////  API production
//    public static final String HOST = "https://api-cpac.herokuapp.com/"; ////  API HEROKU (test server)
//    public static final String HOST = "http://18.139.158.79:8080"; ////  API Clound9 (test server)


    public static final String Url = "/app/merudy/MTIzcXdlMTIz6969/"; //for production
//    public static final String Url = "public/app/merudy/MTIzcXdlMTIz6969/"; //for test server

//    public static final String HOST = "http://13.251.33.233:8080/public"; ////  API test


    public static final String replaceImageName = "https://files.merudy.com/projects/";
    public static final String replaceImageProjectName = "https://files.merudy.com/project_groups/";
    private static final String LOGIN = HOST+"/app/merudy/MTIzcXdlMTIz/%s/login";
//    private static final String ProjectByLocation = HOST+"/%s/step1/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=projectByLocation&lat=%s&lng=%s";
    private static final String PHASE = HOST+"/%s/step1/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=phase";
    private static final String CATE = HOST+"/%s/step1/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=cate";
    private static final String ADD_STEP1 = HOST+"/%s/step1/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=add";
    private static final String SEARCH_USER = HOST+"/%s/step2/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=searchUser&search=%s";
    private static final String EDIT_STEP2 = HOST+"/%s/step2/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=edit";
    private static final String ADD_STEP2 = HOST+"/%s/step2/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=add";
    private static final String STEP2 = HOST+"/%s/step2/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String BY_PROJECT = HOST+"/%s/myprojects/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=byProjects";
    private static final String BY_CUSTOMER = HOST+"/%s/myprojects/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=byCustomers";
    private static final String PHASE3 = HOST+"/%s/step3/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=getPhase";
    private static final String BY_LOCATION = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=new-byLocation&lat=%s&lng=%s&type=all";

    private static final String BOQ = HOST+"/%s/myprojects/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=boq&phase=%s&floor=%s&area=%s";

    private static final String VIEW_DETAIL = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s";

    private static final String STEP1_GET = HOST+"/%s/step1/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String STEP1_EDIT = HOST+"/%s/step1/$s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=edit";
//    private static final String NEW_ADD = HOST+"/%s/add/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String NEW_ADD = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project?act=add_v2";

    private static final String NEW_GROUP_ADD = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/?act=add_project_group";



    private static final String PUSH_PRODUCT = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=push1&phase=%s";
    private static final String CATE_SELECT = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=push2&category=%s";
    private static final String CHOOSE_SKU = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=sku&subclass=%s";
    private static final String ADD_CART = HOST+"/%s/transitions/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=add";
    private static final String PR_CODE_DETAIL = HOST+"/%s/prcode/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String DELETE_PR_CODE = HOST+"/%s/prcode/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=delete&project_id=%s";




    private static final String DELETE_PROJECT = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=del&id=%s";

    private static final String GET_FAV = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/fav/%s?act=list";
    private static final String ADD_FAV = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=addFAV&user_id=%s&product_id=%s&project_id=%s";
    private static final String DELETE_FAV = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=delFAV&user_id=%s&product_id=%s&project_id=%s";
//    private static final String GET_BASKET = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=basket&user_id=%s&project_id=%s";

    private static final String GET_BASKET = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=detail";
    private static final String ADD_BASKET = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=pr";
    private static final String DELETE_BASKET = HOST+"/%s/pushproduct/%s/?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&act=delBasket&user_id=%s&product_id=%s&project_id=%s";

//    private static final String purchase_create = HOST+"/api/purchase/create?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&project=%s";
    private static final String purchase_create = HOST+"/api/purchase/create?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String list_prcode = HOST+"/api/purchase/list?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&project=%s";
    private static final String list_pr_detail = HOST+"/api/trans/listbypr?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&pr=%s";
    private static final String quat_open = HOST+"/api/quat/open?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&pr=%s";
    private static final String quat_list = HOST+"/api/quat/list?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&project=%s";
    private static final String trans_list = HOST+"/api/trans/list?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&qo=%s";
    private static final String trans_update = HOST+"/api/trans/update?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String trans_delete = HOST+"/api/trans/delete?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String quat_revise = HOST+"/api/quat/revise?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String quat_print = HOST+"/api/quat/print?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969";
    private static final String req_pdf = HOST+"/api/attach/export?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf86969&qo=%s";
//   NEW API
    private static final String del_pic = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=delPic";
    private static final String add_pic = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=addPic";
    private static final String add_pic_group_project = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=addPGPic";


    private static final String update_location = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=editLocation";
    private static final String update_depart = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=editProject";
    private static final String update_customer = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=editCustomerProject_v2";

    private static final String update_customer_detail = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/customer/%s?act=edit_v2";

    private static final String add_note = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=addNote";
    private static final String addFAV = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/fav/%s?act=add&product_id=%s&user_id=%s";
    private static final String deleteFAV = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/fav/%s?act=del&product_id=%s";
    private static final String skuDetail = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/sku/%s";
    private static final String edit_price = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=editPrice";
    private static final String delete_pr = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=delProduct";
    private static final String prTOqo = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=toQO";
    private static final String commentQO = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=editNote";
    private static final String delete_fav = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/fav/%s?act=del&product_id=%s";
    private static final String getTranDetail = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=detail";
    private static final String SEARCH = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/sku/%s?act=search&project_id=%s&page=%s&orderBy=%s";
    private static final String PIC_CUTOMER = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/customer/%s?act=pic_v2&customer_id=%s";
    private static final String PIC_CHECKIN = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/checkin/?act=addGallery";
    private static final String DELETE_PIC_CHECKIN = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/checkin/%s/?act=delGallery";


    private static final String addClassFAV = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/favclass/%s?act=add&class_id=%s&user_id=%s";
    private static final String deleteClassFAV = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/favclass/%s?act=del&class_id=%s";
    private static final String editQoDetail = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/transaction/%s?act=editQODetail";


    private static final String removeCateCache = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/catalogues/%s?cate_id=%s&nocache=1";


    private static final String addProjectGroupPicture = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project/%s?act=addPGPic";



    public static  String getAddProjectGroupPicture(String lang,String gId){
        String url = String.format(ApiEndPoint.addProjectGroupPicture,lang,gId);
        return url;
    }




    public static  String getRemoveCateCache(String lang,String projectId,String cateId){
        String url = String.format(ApiEndPoint.removeCateCache,lang,projectId,cateId);
        return url;
    }


    public static  String getEditQoDetail(String lang,String tranId){
        String url = String.format(ApiEndPoint.editQoDetail,lang,tranId);
        return url;
    }

    public static  String getDeleteClassFav(String lang,String projectId,String classId){
        String url = String.format(ApiEndPoint.deleteClassFAV,lang,projectId,classId);
        return url;
    }

    public static  String getAddClassFav(String lang,String projectId,String classId,String user_id){
        String url = String.format(ApiEndPoint.addClassFAV,lang,projectId,classId,user_id);
        return url;
    }


    public static  String getDeletePicCheckin(String lang,String checkinID){
        String url = String.format(ApiEndPoint.DELETE_PIC_CHECKIN,lang,checkinID);
        return url;
    }


    public static  String getPicCheckin(String lang){
        String url = String.format(ApiEndPoint.PIC_CHECKIN,lang);
        return url;
    }

    public static  String update_picCustomer(String lang,String userID,String customerId){
        String url = String.format(ApiEndPoint.PIC_CUTOMER,lang,userID,customerId);
        return url;
    }

    public static  String getGetTranDetail(String lang,String transaction){
        String url = String.format(ApiEndPoint.getTranDetail,lang,transaction);
        return url;
    }





    public static  String getPrTOqo(String lang,String tranId){
        String url = String.format(ApiEndPoint.prTOqo,lang,tranId);
        return url;
    }

    public static  String sendCommentQO(String lang,String tranId){
        String url = String.format(ApiEndPoint.commentQO,lang,tranId);
        return url;
    }

    public static  String getDelete_pr(String lang,String tranId){
        String url = String.format(ApiEndPoint.delete_pr,lang,tranId);
        return url;
    }

    public static  String getEdit_price(String lang,String tranId){
        String url = String.format(ApiEndPoint.edit_price,lang,tranId);
        return url;
    }

    public static  String getSkuDetail(String lang,String skuId){
        String url = String.format(ApiEndPoint.skuDetail,lang,skuId);
        return url;
    }


    public static  String getDeleteFAV(String lang,String projectID,String productId){
        String url = String.format(ApiEndPoint.delete_fav,lang,projectID,productId);
        return url;
    }

    public static  String getDeleteFav(String lang,String projectId,String productId){
        String url = String.format(ApiEndPoint.deleteFAV,lang,projectId,productId);
        return url;
    }

    public static  String getAddFav(String lang,String projectId,String productId,String user_id){
        String url = String.format(ApiEndPoint.addFAV,lang,projectId,productId,user_id);
        return url;
    }

    public static  String getAdd_note(String lang,String projectId){
        String url = String.format(ApiEndPoint.add_note,lang,projectId);
        return url;
    }

    public static  String getUpdate_customer(String lang,String projectId){
        String url = String.format(ApiEndPoint.update_customer,lang,projectId);
        return url;
    }

    public static  String getUpdate_customer_detail(String lang,String userId){
        String url = String.format(ApiEndPoint.update_customer_detail,lang,userId);
        return url;
    }

    public static  String getUpdate_depart(String lang,String projectId){
        String url = String.format(ApiEndPoint.update_depart,lang,projectId);
        return url;
    }

    public static  String getUpdate_location(String lang,String projectId){
        String url = String.format(ApiEndPoint.update_location,lang,projectId);
        return url;
    }




    public static  String getAdd_pic_group_project(String lang,String gId){
        String url = String.format(ApiEndPoint.add_pic_group_project,lang,gId);
        return url;
    }

    public static  String add_picProject(String lang,String projectId){
        String url = String.format(ApiEndPoint.add_pic,lang,projectId);
        return url;
    }

    public static  String dele_pic(String lang,String projectId){
        String url = String.format(ApiEndPoint.del_pic,lang,projectId);
        return url;
    }

    public static  String getReq_pdf(String QoId){
        String url = String.format(ApiEndPoint.req_pdf,QoId);
        return url;
    }

    public static  String getQuat_print(String QoId){
        String url = String.format(ApiEndPoint.quat_print);
        return url;
    }

    public static  String getQuat_revise(String QoId){
        String url = String.format(ApiEndPoint.quat_revise);
        return url;
    }

    public static  String getTrans_delete(String itemId){
        String url = String.format(ApiEndPoint.trans_delete);
        return url;
    }

    public static  String getTrans_update(String itemId,String itemQty,String itemPrice){
        String url = String.format(ApiEndPoint.trans_update);
        return url;
    }

    public static  String getTrans_list(String qoId){
        String url = String.format(ApiEndPoint.trans_list,qoId);
        return url;
    }

    public static  String getQuat_list(String projectId){
        String url = String.format(ApiEndPoint.quat_list,projectId);
        return url;
    }

    public static  String getList_pr_detail(String pr_id){
        String url = String.format(ApiEndPoint.list_pr_detail,pr_id);
        return url;
    }

    public static  String getQuat_open(String pr_id){
        String url = String.format(ApiEndPoint.quat_open,pr_id);
        return url;
    }

    public static  String getList_prcode(String project_id){
        String url = String.format(ApiEndPoint.list_prcode,project_id);
        return url;
    }

    public static  String getPurchase_create(String project_id){
        String url = String.format(ApiEndPoint.purchase_create);
        return url;
    }

    public static  String getDeleteBasket(String lang,String shopId,String userId,String product_id,String project_id){
        String url = String.format(ApiEndPoint.DELETE_BASKET,lang,shopId,userId,product_id,project_id);
        return url;
    }
    public static  String getAddBasket(String lang,String project_id){
        String url = String.format(ApiEndPoint.ADD_BASKET,lang,project_id);
        return url;
    }

    public static  String getGetBasket(String lang,String transactionId){
        String url = String.format(ApiEndPoint.GET_BASKET,lang,transactionId);
        return url;
    }

    public static  String getGetFav(String lang,String projectId){
        String url = String.format(ApiEndPoint.GET_FAV,lang,projectId);
        return url;
    }

    public static  String getDeleteFav(String lang,String shopId,String userId,String product_id,String projectId){
        String url = String.format(ApiEndPoint.DELETE_FAV,lang,shopId,userId,product_id,projectId);
        return url;
    }

    public static  String getAddFav(String lang,String shopId,String userId,String product_id,String projectId){
        String url = String.format(ApiEndPoint.ADD_FAV,lang,shopId,userId,product_id,projectId);
        return url;
    }

    public static  String getDeleteProject(String lang,String userId,String projectID){
        String url = String.format(ApiEndPoint.DELETE_PROJECT,lang,userId,projectID);
        return url;
    }

    ///app/merudy/MTIzcXdlMTIz6969/%s/sku/[shop id]?act=search&project_id=[project id]&page=[page]&orderBy=popularity
    public static  String getSearch(String lang,String shopId,String projectId,String page,String order){
        String url = String.format(ApiEndPoint.SEARCH,lang,shopId,projectId,page,order);
        return url;
    }


    public static  String getDeletePrCode(String lang,String prCode,String projectId){
        String url = String.format(ApiEndPoint.DELETE_PR_CODE,lang,prCode,projectId);
        return url;
    }


    public static  String getPrCodeDetail(String lang,String prCode){
        String url = String.format(ApiEndPoint.PR_CODE_DETAIL,lang,prCode);
        return url;
    }

    public static  String getAddCart(String lang,String uId){
        String url = String.format(ApiEndPoint.ADD_CART,lang,uId);
        return url;
    }

    public static  String getChooseSku(String lang,String shopId,String cId){
        String url = String.format(ApiEndPoint.CHOOSE_SKU,lang,shopId, cId);
        return url;
    }

    public static  String getCateSelect(String lang,String shopId,String cId){
        String url = String.format(ApiEndPoint.CATE_SELECT,lang,shopId, cId);
        return url;
    }

    public static  String getPushProduct(String lang,String shopId,String phase){
        String url = String.format(ApiEndPoint.PUSH_PRODUCT,lang, shopId,phase);
        return url;
    }

    public static  String getViewDetail(String lang,String projectId){
        String url = String.format(ApiEndPoint.VIEW_DETAIL, lang,projectId);
        return url;
    }


    public static  String getBoq(String lang,String userId,String phase,String floor,String area){
        String url = String.format(ApiEndPoint.BOQ, lang,userId,phase,floor,area);
        return url;
    }


    public static  String getNewAdd(String lang){
        String url = String.format(ApiEndPoint.NEW_ADD, lang);
        return url;
    }

    public static  String getNewGroupAdd(String lang){
        String url = String.format(ApiEndPoint.NEW_GROUP_ADD, lang);
        return url;
    }




    public static  String getStep1Edit(String lang,String cateId){
        String url = String.format(ApiEndPoint.STEP1_EDIT, lang,cateId);
        return url;
    }

    public static  String getStep1Get(String lang,String cateId){
        String url = String.format(ApiEndPoint.STEP1_GET, lang,cateId);
        return url;
    }

    public static  String getByLocation(String lang,String userId,String lat,String lng){
        String url = String.format(ApiEndPoint.BY_LOCATION, lang,userId,lat,lng);
        return url;
    }


    public static  String getPhase3(String lang,String userId){
        String url = String.format(ApiEndPoint.PHASE3, lang,userId);
        return url;
    }


    public static  String getByCustomer(String lang,String cusId){
        String url = String.format(ApiEndPoint.BY_CUSTOMER, lang,cusId);
        return url;
    }


    public static  String getByProject(String lang,String userId){
        String url = String.format(ApiEndPoint.BY_PROJECT, lang,userId);
        return url;
    }


    public static  String getEditStep2(String lang,String userId){
        String url = String.format(ApiEndPoint.EDIT_STEP2, lang,userId);
        return url;
    }


    public static  String getStep2(String lang,String userId){
        String url = String.format(ApiEndPoint.STEP2, lang,userId);
        return url;
    }

    public static  String getAddStep2(String lang,String projectId){
        String url = String.format(ApiEndPoint.ADD_STEP2, lang,projectId);
        return url;
    }


    public static  String getSearchUser(String lang,String userId,String keyword){
        String url = String.format(ApiEndPoint.SEARCH_USER, lang,userId,keyword);
        return url;
    }

    public static  String getAddStep1(String lang,String shopId){
        String url = String.format(ApiEndPoint.ADD_STEP1, lang,shopId);
        return url;
    }

    public static  String getCate(String lang){
        String url = String.format(ApiEndPoint.CATE, lang);
        return url;
    }

    public static  String getPhase(String lang,String cateId){
        String url = String.format(ApiEndPoint.PHASE, lang,cateId);
        return url;
    }

    public static String getLogin(String lang){
//        String url = String.format(ApiEndPoint.LOGIN, lang,email,password);
        String url = String.format(ApiEndPoint.LOGIN, lang);
        return url;
    }
//    public static final String getProjectByLocation(String lang,String pId,String lat,String lng){
////        String url = String.format(ApiEndPoint.LOGIN, lang,email,password);
//        String url = String.format(ApiEndPoint.ProjectByLocation, lang,pId,lat,lng);
//        return url;
//    }

}
