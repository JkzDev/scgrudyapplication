package com.scg.rudy.data.network;

import com.scg.rudy.data.remote.ApiHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

/** Created by DekDroidDev on 30/3/2018 AD. */
@Singleton
public class NetworkConnectionManager {
    private final ApiHelper mApiHelper;

    @Inject
    public NetworkConnectionManager(ApiHelper apiHelper) {
        this.mApiHelper = apiHelper;
    }

}