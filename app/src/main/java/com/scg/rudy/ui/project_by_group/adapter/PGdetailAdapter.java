package com.scg.rudy.ui.project_by_group.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.project_group_detail.ProjectListItem;
import com.scg.rudy.ui.dealbyproject.DealByProjectActivity;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class PGdetailAdapter extends RecyclerView.Adapter<PGdetailAdapter.PGdetailViewHolder> {
    private Context context;
    private List<ProjectListItem> arrayList;


    public PGdetailAdapter(Context context, List<ProjectListItem> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public class PGdetailViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name,tv_baht,tv_cart,fav_txt,tv_status;
        public LinearLayout btn_sell;

        public PGdetailViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.name);
            tv_baht = itemView.findViewById(R.id.tv_baht);
            tv_cart = itemView.findViewById(R.id.tv_cart);
            fav_txt = itemView.findViewById(R.id.fav_txt);
            tv_status = itemView.findViewById(R.id.tv_status);
            btn_sell = itemView.findViewById(R.id.btn_sell);
        }
    }

    @Override
    public PGdetailAdapter.PGdetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_projectbygroup, parent, false);
        return new PGdetailViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PGdetailAdapter.PGdetailViewHolder holder, final int position) {
        ProjectListItem item = arrayList.get(position);
        holder.tv_name.setText(item.getProjectName());
        holder.tv_status.setText(item.getStatus() + context.getResources().getString(R.string.list));
        holder.fav_txt.setText(item.getCountFAV());
        holder.tv_cart.setText(item.getCountPR());
        holder.tv_baht.setText(item.getUnitBudget());

        holder.btn_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DealByProjectActivity.class);
                intent.putExtra("projectId", item.getId());
                if(item.getProjectTypeId()==null||item.getProjectTypeId().equalsIgnoreCase("")){
                    intent.putExtra("projectTypeId", "11");
                }else{
                    intent.putExtra("projectTypeId", item.getProjectTypeId());
                }
                intent.putExtra("phaseId", item.getPhaseId());

                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
