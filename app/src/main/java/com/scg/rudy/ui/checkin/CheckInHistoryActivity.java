package com.scg.rudy.ui.checkin;

import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.checkin_history_list.CheckinHistory;
import com.scg.rudy.model.pojo.checkin_history_list.ItemsItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.utils.Utils;

import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CheckInHistoryActivity extends BaseActivity implements View.OnClickListener {

    private TextView clsBtn;
    private RecyclerView recyclerView;
    private RudyService rudyService;
    private Bundle extras;
    private String projectId;
    private LinearLayout nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_history);
        initView();
        setOnClick();


    }

    @Override
    public void onResume() {
        super.onResume();
        loadCheckinHistory();
    }



    private Call<String> callCheckinApi() {
        return rudyService.getCheckinHistory(
                Utils.APP_LANGUAGE,
                projectId
        );
    }

    private void loadCheckinHistory() {
        showLoading(this);
        callCheckinApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    CheckinHistory obj  = gson.fromJson(resp, CheckinHistory.class);
                    List<ItemsItem> fetchResults =   customFetchResults(obj);
                    setCheckinHistoryData(fetchResults);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInHistoryActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                nodata.setVisibility(View.VISIBLE);
                showToast(CheckInHistoryActivity.this, t.getMessage());
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private List<ItemsItem> fetchResults(Response<CheckinHistory> response) {
        CheckinHistory checkinHistory = response.body();
        return checkinHistory.getItems();
    }

    private List<ItemsItem> customFetchResults(CheckinHistory checkinHistory) {
        return checkinHistory.getItems();
    }

    private void setCheckinHistoryData( List<ItemsItem> fetchResults){
        FavCheckInListAdapter adapter = new FavCheckInListAdapter(this,fetchResults,projectId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

    }

    private void setOnClick() {
        clsBtn.setOnClickListener(this);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return true;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        extras = getIntent().getExtras();
        if (extras != null) {
            projectId = extras.getString("projectId");
        }
        rudyService = ApiHelper.getClient();
        clsBtn = (TextView) findViewById(R.id.cls_btn);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        nodata = (LinearLayout) findViewById(R.id.nodata);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
    }
}
