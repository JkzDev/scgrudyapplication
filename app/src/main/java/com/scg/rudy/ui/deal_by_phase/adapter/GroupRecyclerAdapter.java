package com.scg.rudy.ui.deal_by_phase.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem;
import com.scg.rudy.ui.deal_by_phase.byPhaseInterface;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class GroupRecyclerAdapter extends RecyclerView.Adapter<GroupRecyclerAdapter.ProjectViewHolder> {

    private  List<com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem>  itemList ;

    private Context context;
    private int selected_position = 0;
    private byPhaseInterface listener;

    public GroupRecyclerAdapter(Context context,List<com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem>  itemList, byPhaseInterface listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.sale_product_item, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        final ItemsItem groupItem = itemList.get(position);
//        holder.itemView.setBackgroundColor(selected_position == position ? Color.GREEN : Color.TRANSPARENT);
//        holder.sub_phase_name.setText(groupItem.getName());
        if(!groupItem.getImg().isEmpty()){
            Glide.with(context)
                    .load(groupItem.getImg())
                    .into(holder.image_item);
        }

        holder.bg_select.setBackground(selected_position == position ? context.getDrawable(R.drawable.bg_frame_group_select) :context.getDrawable(R.drawable.bg_project_normal));
        holder.product_name.setTextColor(selected_position == position ? Color.WHITE : Color.BLACK);


        holder.status.setText(context.getResources().getString(R.string.reason));
        holder.total.setText(groupItem.getId());
        holder.product_name.setText(groupItem.getName());
//        holder.product_name.setTypeface(bold);
//        holder.view.setId(Integer.parseInt(detailArrayList.get(i).getId()));

//        holder.selectIco.setVisibility(selected_position == position ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        }
        return itemList.size();
    }



    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView status ;
        private TextView total;
        private TextView product_name ;
        private ImageView image_item ;
        private CardView product_bg ;
        private RelativeLayout bg_select;


        public ProjectViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            status = v.findViewById(R.id.status);
            total = v.findViewById(R.id.total);
            product_name = v.findViewById(R.id.product_name);
            image_item = v.findViewById(R.id.image_item);
            product_bg= v.findViewById(R.id.product_bg);
            bg_select= v.findViewById(R.id.bg_select);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) {
                return;
            }
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            listener.onGroupClick(itemList.get(selected_position).getId());
        }
    }


}