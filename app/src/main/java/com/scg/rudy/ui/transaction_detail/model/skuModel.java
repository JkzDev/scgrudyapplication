package com.scg.rudy.ui.transaction_detail.model;

public class skuModel  {
    private String ispromotion;
    private String unit;
    private String price;
    private String classId;
    private String name;
    private String recommend;
    private int fav;
    private String commission;
    private String id;
    private String pic;
    private int used2use;
    private int hot;

    public String getIspromotion() {
        return ispromotion;
    }

    public void setIspromotion(String ispromotion) {
        this.ispromotion = ispromotion;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public int getFav() {
        return fav;
    }

    public void setFav(int fav) {
        this.fav = fav;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getUsed2use() {
        return used2use;
    }

    public void setUsed2use(int used2use) {
        this.used2use = used2use;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }
}