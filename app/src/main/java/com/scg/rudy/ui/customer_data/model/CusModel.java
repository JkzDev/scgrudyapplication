package com.scg.rudy.ui.customer_data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

public class CusModel  {
    private String customer_name;
    private String customer_phone;
    private String pic;
    private String customer_line;
    private String customer_note;
    private String customer_phone_extension;
    private String customer_email;
    private String customer_taxno;
    private String customer_code;
    private String champ_customer_code;


    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getChamp_customer_code() {
        return champ_customer_code;
    }

    public void setChamp_customer_code(String champ_customer_code) {
        this.champ_customer_code = champ_customer_code;
    }

    public String getCustomer_phone_extension() {
        return customer_phone_extension;
    }

    public void setCustomer_phone_extension(String customer_phone_extension) {
        this.customer_phone_extension = customer_phone_extension;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_taxno() {
        return customer_taxno;
    }

    public void setCustomer_taxno(String customer_taxno) {
        this.customer_taxno = customer_taxno;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getCustomer_line() {
        return customer_line;
    }

    public void setCustomer_line(String customer_line) {
        this.customer_line = customer_line;
    }

    public String getCustomer_note() {
        return customer_note;
    }

    public void setCustomer_note(String customer_note) {
        this.customer_note = customer_note;
    }
}