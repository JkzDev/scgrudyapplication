package com.scg.rudy.ui.by_projects.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.byproject.CustomerPhoneListItem;
import com.scg.rudy.ui.by_projects.ByProjectsListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CheckInTelAdapter extends RecyclerView.Adapter<CheckInTelAdapter.MyViewHolder>{
    private Context context;
    private List<CustomerPhoneListItem> item;
    private ByProjectsListener listener;
    private Dialog dialog;

    public CheckInTelAdapter(Context _context, List<CustomerPhoneListItem> _item, ByProjectsListener _listener, Dialog _dialog){
        this.context = _context;
        this.item = _item;
        this.listener = _listener;
        this.dialog = _dialog;
    }

    @NonNull
    @Override
    public CheckInTelAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.popup_item_tel, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull CheckInTelAdapter.MyViewHolder holder, int position) {
        holder.cusName.setText(item.get(position).getName());
        holder.cusType.setText(item.get(position).getType());
        Glide.with(context)
        .load(item.get(position).getPic())
        .into(holder.cusImage);
        CheckInTelNumberAdapter adapter = new CheckInTelNumberAdapter(context, item.get(position).getPhone(), listener, dialog);
        holder.reNumberPhone.setLayoutManager(new LinearLayoutManager(context));
        holder.reNumberPhone.setItemAnimator(new DefaultItemAnimator());
        holder.reNumberPhone.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView cusName;
        private TextView cusType;
        private CircleImageView cusImage;
        private RecyclerView reNumberPhone;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cusName = itemView.findViewById(R.id.cus_name);
            cusType = itemView.findViewById(R.id.cus_type);
            cusImage =  itemView.findViewById(R.id.cus_image);
            reNumberPhone = itemView.findViewById(R.id.reNumberPhone);
        }
    }
}
