package com.scg.rudy.ui.customer_detail.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.customerDetailPage.saleList.ItemsItem;
import com.scg.rudy.ui.by_customers.ByCustomerListener;
import com.scg.rudy.ui.print_qo.PrintQoActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class saleListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private LayoutInflater inflater;
    private ByCustomerListener listener;

    public saleListRecyclerAdapter(Context context, ByCustomerListener listener) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = new ArrayList<>();
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.item_pr_view, parent, false);
            return new saleListRecyclerAdapter.CustomerViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new saleListRecyclerAdapter.LoadingVH(viewLoading);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(final @NonNull RecyclerView.ViewHolder vholder, final int position) {
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ItemsItem transitionListItem = arrayList.get(position);
            CustomerViewHolder holder = (CustomerViewHolder) vholder;
            String date[] = transitionListItem.getLastUpdate().split(" ");
//@color/color_next_btn
            holder.dateText.setText( date[0]);
            holder.prcode.setText(transitionListItem.getStatusTxt());

            holder.nameTxt.setText(transitionListItem.getProjectName());
//            for (int i = 0; i < _projectListItems.size(); i++) {
//                if(transitionListItem.getProjectId().equalsIgnoreCase(_projectListItems.get(i).getId())){
//                    holder.nameTxt.setText(_projectListItems.get(i).getProjectName());
//
//
//                }
//            }



//        holder.netAmount.setText(Utils.moneyFormat(transitionListItem.getOrderNumber()));
            holder.netAmount.setText(transitionListItem.getOrderNumber());

//        if(transitionListItem.getStatusTxt().toLowerCase().equalsIgnoreCase("pr")){
//            holder.prcode.setTextColor(context.getResources().getColor(R.color.txt_tab2));
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent =new Intent( context,TransactionDetailActivity.class);
//                    intent.putExtra("transaction_id",transitionListItem.getTransactionId());
//                    context.startActivity(intent);
//                }
//            });
//        }else{
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePrefer(context,"print","");
                    Intent intent =new Intent( context, PrintQoActivity.class);
                    PrintQoActivity.fromCart = false;
//                    intent.putExtra("url","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+transitionListItem.getId()+"?act=qv2");
//                    intent.putExtra("urlOpen","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+transitionListItem.getId()+"?act=qv");
                    intent.putExtra("transaction_id",transitionListItem.getId());
                    Utils.pdfName = transitionListItem.getId();
                    context.startActivity(intent);
                }
            });


        }else if(itemPosition == LOADING){
            LoadingVH loadingVH = (LoadingVH) vholder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view,double percent,int maxBarSize){
        double fn_size = (percent*maxBarSize)/100;
        if(fn_size==0){
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        }else{
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class CustomerViewHolder extends RecyclerView.ViewHolder {
        TextView dateText;
        TextView prcode ;
        TextView nameTxt;
        TextView netAmount;
        CardView cardPhase;

        public CustomerViewHolder(View view) {
            super(view);
            dateText = itemView.findViewById(R.id.dateText);
            prcode = itemView.findViewById(R.id.prcode);
            nameTxt = itemView.findViewById(R.id.nameTxt);
            netAmount = itemView.findViewById(R.id.netAmount);
            cardPhase = itemView.findViewById(R.id.card_phase);

        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
       ItemsItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return arrayList.get(position);
    }


    public void add(ItemsItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }
    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }


}