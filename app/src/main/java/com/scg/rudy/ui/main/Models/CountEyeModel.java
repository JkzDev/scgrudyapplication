
package com.scg.rudy.ui.main.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CountEyeModel {

    @SerializedName("items")
    private Items mItems;
    @SerializedName("status")
    private Long mStatus;

    public Items getItems() {
        return mItems;
    }

    public void setItems(Items items) {
        mItems = items;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

}
