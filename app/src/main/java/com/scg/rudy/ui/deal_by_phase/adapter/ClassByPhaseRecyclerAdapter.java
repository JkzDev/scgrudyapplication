package com.scg.rudy.ui.deal_by_phase.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.class_by_phase.ItemsItem;
import com.scg.rudy.ui.deal_by_phase.byPhaseInterface;

import java.util.List;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class ClassByPhaseRecyclerAdapter extends RecyclerView.Adapter<ClassByPhaseRecyclerAdapter.ProjectViewHolder> {

    private  List<ItemsItem> itemList ;

    private Context context;
    private int selected_position = 0;
    private byPhaseInterface listener;

    public ClassByPhaseRecyclerAdapter(Context context, List<ItemsItem> itemList, byPhaseInterface listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.sale_class_item, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        final ItemsItem classItem = itemList.get(position);
//        holder.itemView.setBackgroundColor(selected_position == position ? Color.GREEN : Color.TRANSPARENT);
//        holder.sub_phase_name.setText(groupItem.getName());


        holder.subPhaseName.setText(classItem.getName());

        if(String.valueOf(classItem.getFav()).equalsIgnoreCase("1")){
            holder.icon_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
        }else{
            holder.icon_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
        }

        if(String.valueOf(classItem.getIspromotion()).equalsIgnoreCase("1")){
            holder.promo_ico.setVisibility(View.VISIBLE);
        }else{
            holder.promo_ico.setVisibility(View.INVISIBLE);
        }

        holder.add_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(classItem.getFav()==0){
//                    classes.get(finalI).setFav(1);
//                    holder.icon_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
//                    addFAV(classes.get(finalI).getId(),classes);
                    listener.addFAVClass(classItem.getId(),true);


                }else{
//                    classes.get(finalI).setFav(0);
//                    holder.icon_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
                    listener.addFAVClass(classItem.getId(),false);

                }
            }
        });



//        holder.bg_select.setBackground(selected_position == position ? context.getDrawable(R.drawable.bg_frame_group_select) :context.getDrawable(R.drawable.bg_project_normal));
//        holder.product_name.setTextColor(selected_position == position ? Color.WHITE : Color.BLACK);
//        holder.status.setText("เหตุผล");
//        holder.total.setText(groupItem.getId());
//        holder.product_name.setText(groupItem.getName());

//        holder.product_name.setTypeface(bold);
//        holder.view.setId(Integer.parseInt(detailArrayList.get(i).getId()));

//        holder.selectIco.setVisibility(selected_position == position ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        }
        return itemList.size();
    }



    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView subPhaseName ;
        private RelativeLayout add_fav ;
        private ImageView icon_fav ;
        private ImageView promo_ico ;

        public ProjectViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            subPhaseName = v.findViewById(R.id.sub_phase_name);
            add_fav = v.findViewById(R.id.add_fav);
            icon_fav = v.findViewById(R.id.icon_fav);
            promo_ico = v.findViewById(R.id.promo_ico);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) {
                return;
            }
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            listener.clickClass(itemList.get(selected_position));
        }
    }


}