package com.scg.rudy.ui.call_to_customer.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Models.CallAllModel;

import java.util.ArrayList;

public class CallAllAdapter extends RecyclerView.Adapter<CallAllAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CallAllModel> callAllModels;

    public CallAllAdapter(Context _context, ArrayList<CallAllModel> _list) {
        context = _context;
        callAllModels = _list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        Button phone;
        ImageView more;
        TextView name;
        TextView company;



        public ViewHolder(View view)
        {
            super(view);
            image = view.findViewById(R.id.CallAllItemImage);
            phone = view.findViewById(R.id.CallAllItemBtnPhone);
            more = view.findViewById(R.id.CallAllItemImageMore);
            name = view.findViewById(R.id.CallAllItemName);
            company = view.findViewById(R.id.CallAllCompany);

        }

    }

    @NonNull
    @Override
    public CallAllAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_all_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CallAllAdapter.ViewHolder holder, int position) {
        holder.image.setImageDrawable(callAllModels.get(position).getImage());
        holder.more.setImageDrawable(context.getDrawable(R.drawable.ic_action_more_back));
        holder.name.setText(callAllModels.get(position).getName());
        holder.company.setText(callAllModels.get(position).getCompany());

    }

    @Override
    public int getItemCount() {
        return callAllModels.size();
    }
}
