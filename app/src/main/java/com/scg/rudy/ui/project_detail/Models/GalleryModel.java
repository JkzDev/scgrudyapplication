
package com.scg.rudy.ui.project_detail.Models;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GalleryModel implements Parcelable  {

    public GalleryModel(String _mCkId, String _mName, String _mUpdDate){
        this.mCkId = _mCkId;
        this.mName = _mName;
        this.mUpdDate = _mUpdDate;
    }


    @SerializedName("ckId")
    private String mCkId;
    @SerializedName("name")
    private String mName;
    @SerializedName("updDate")
    private String mUpdDate;

    public String getCkId() {
        return mCkId;
    }

    public void setCkId(String ckId) {
        mCkId = ckId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUpdDate() {
        return mUpdDate;
    }

    public void setUpdDate(String updDate) {
        mUpdDate = updDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mCkId);
        dest.writeString(this.mName);
        dest.writeString(this.mUpdDate);
    }

    public GalleryModel(Parcel in) {
        this.mCkId = in.readString();
        this.mName = in.readString();
        this.mUpdDate = in.readString();
    }


    public static final Parcelable.Creator<GalleryModel> CREATOR = new Parcelable.Creator<GalleryModel>() {
        @Override
        public GalleryModel createFromParcel(Parcel source) {
            return new GalleryModel(source);
        }

        @Override
        public GalleryModel[] newArray(int size) {
            return new GalleryModel[size];
        }
    };
}
