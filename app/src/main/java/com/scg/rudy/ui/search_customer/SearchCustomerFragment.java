//package com.scg.rudy.ui.search_customer;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.scg.rudy.R;
//import com.scg.rudy.data.remote.ApiEndPoint;
//import com.scg.rudy.injection.component.ActivityComponent;
//import com.scg.rudy.model.pojo.byCustomer.Customer;
//import com.scg.rudy.model.pojo.byCustomer.ItemsItem;
//import com.scg.rudy.base.BaseFragment;
//import com.scg.rudy.ui.by_customers.ByCustomerRecyclerAdapter;
//import com.scg.rudy.ui.main.MainActivity;
//import com.scg.rudy.utils.Utils;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.inject.Inject;
//
//import butterknife.ButterKnife;
//import okhttp3.ResponseBody;
//
///**
// * Created by DekDroidDev on 24/4/2018 AD.
// */
//public class SearchCustomerFragment extends BaseFragment implements View.OnClickListener, SearchCustomerMvpView {
//    private ImageView clsBtn;
//    private EditText searchText;
//    private ImageView cancel;
//    private TextView nodata;
//    private Bundle extras;
//    private RecyclerView recyclerView;
//    private ByCustomerRecyclerAdapter adapter;
//    private Customer customer = null;
//    private List<ItemsItem> arrayList;
//    private List<ItemsItem> newArrayList;
//
//
//
//    @Inject
//    SearchCustomerPresenter<SearchCustomerMvpView> searchCustomerPresenter;
//    public static final String TAG = "SearchCustomerFragment";
//
//    public static SearchCustomerFragment newInstance(Customer customer ) {
//        Bundle args = new Bundle();
//        args.putParcelable("customer",customer);
//        SearchCustomerFragment fragment = new SearchCustomerFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//    @Override
//    public void onDestroyView() {
//        searchCustomerPresenter.detachView();
//        super.onDestroyView();
//    }
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        extras = getArguments();
//        if (!extras.isEmpty()) {
//            customer = extras.getParcelable("customer");
//            arrayList =customer.getItems();
//        }
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_search, container, false);
//        ActivityComponent component = getActivityComponent();
//        if (component != null) {
//            component.injectSearchCustomer(this);
//            setUnBinder(ButterKnife.bind(this, view));
//            searchCustomerPresenter.attachView(this);
//        }
//        initView(view);
//
//        newArrayList = new ArrayList<>();
//        clsBtn.setOnClickListener(this);
//        cancel.setOnClickListener(this);
//        getUserData();
//
//
//        searchText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                newArrayList = new ArrayList<>();
//                recyclerView.setAdapter(null);
//                for (int i = 0; i < arrayList.size(); i++) {
//                    if (arrayList.get(i).getCustomerName().toString().contains(s)) {
//                        newArrayList.add(arrayList.get(i));
//                    }
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if(newArrayList.size()==0){
//                    nodata.setVisibility(View.VISIBLE);
//                }else{
//                    nodata.setVisibility(View.GONE);
//                    adapter = new ByCustomerRecyclerAdapter(getBaseActivity(),newArrayList);
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setAdapter(adapter);
//
//                }
//            }
//        });
//
//
//
//
//        return view;
//    }
//
//    public void getUserData() {
//        try {
//            JSONObject object = new JSONObject(Utils.getPrefer(getActivity(), Utils.PERF_LOGIN));
//            JSONObject itemObject = object.getJSONObject("items");
//            user_id = itemObject.optString("id");
//            shopID = itemObject.optString("shop_id");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public void initView(View view){
//        recyclerView = view.findViewById(R.id.recyclerView);
//        clsBtn = view.findViewById(R.id.cls_btn);
//        searchText = view.findViewById(R.id.search_text);
//        cancel = view.findViewById(R.id.cancel);
//        nodata = view.findViewById(R.id.nodata);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        if(Utils.getPrefer(getActivity(),"print").length()>0){
//            MainActivity activ = (MainActivity) getActivity();
//            activ.onFragmentDetached(SearchCustomerFragment.TAG);
//        }else{
//            searchCustomerPresenter.getCustomerList(ApiEndPoint.LANG,user_id,"list");
//        }
//
//        super.onResume();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        if(v==clsBtn){
//                MainActivity activ = (MainActivity) getActivity();
//                activ.onFragmentDetached(TAG);
//        }
//        if(v==cancel){
//            searchText.setText("");
//            recyclerView.setAdapter(null);
//            nodata.setVisibility(View.VISIBLE);
//            Utils.hideKeyboard(getActivity());
//        }
//
//    }
//
//
//
//    @Override
//    public void showData(Customer customer) {
//
//    }
//
//    @Override
//    public void showErrorIsNull() {
//    }
//
//    @Override
//    public void BodyError(ResponseBody responseBodyError) {
//    }
//
//    @Override
//    public void Failure(Throwable t) {
//    }
//
//
//    @Override
//    protected void setUp(View view) {
//
//    }
//
//
//}
