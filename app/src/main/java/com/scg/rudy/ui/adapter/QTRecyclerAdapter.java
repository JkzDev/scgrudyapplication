package com.scg.rudy.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scg.rudy.model.pojo.project_detail.PrItem;
import com.scg.rudy.ui.print_qo.PrintQoActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.R;
import com.scg.rudy.utils.Utils;

import java.util.List;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class QTRecyclerAdapter extends RecyclerView.Adapter<QTRecyclerAdapter.ProjectViewHolder> {

    private List<PrItem> qtListModelArrayList ;
    private Context context;
    public QTRecyclerAdapter(Context context,  List<PrItem>  qtListModelArrayList) {
        this.qtListModelArrayList = qtListModelArrayList;
        this.context = context;
        for (int i = 0; i < qtListModelArrayList.size(); i++) {
            this.qtListModelArrayList.remove(qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr"));
        }

    }

    @Override
    public QTRecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_pr_view, parent, false);
        return new QTRecyclerAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QTRecyclerAdapter.ProjectViewHolder holder, int position) {
        final PrItem prCodeDetailModel = qtListModelArrayList.get(position);
//        ,prCodeDetailModel.getId()
        String date[] = prCodeDetailModel.getLastUpdate().split(" ");
//@color/color_next_btn
        holder.dateText.setText( date[0]);
        holder.prcode.setText(prCodeDetailModel.getStatusTxt());


        holder.netAmount.setText(Utils.moneyFormat(prCodeDetailModel.getNetAmount()));
        holder.nameTxt.setText(prCodeDetailModel.getProductName());
        if(prCodeDetailModel.getStatusTxt().toLowerCase().equalsIgnoreCase("pr")){
            holder.prcode.setTextColor(context.getResources().getColor(R.color.txt_tab2));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent( context,TransactionDetailActivity.class);
                    intent.putExtra("transaction_id",prCodeDetailModel.getTransactionId());
                    context.startActivity(intent);
                }
            });
        }else{
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePrefer(context,"print","");
                    Intent intent =new Intent( context,PrintQoActivity.class);
                    PrintQoActivity.fromCart = false;
                    intent.putExtra("transaction_id",prCodeDetailModel.getTransactionId());
                    intent.putExtra("url","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+prCodeDetailModel.getTransactionId()+"?act=qv2");
                    intent.putExtra("urlOpen","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+prCodeDetailModel.getTransactionId()+"?act=qv");


                    context.startActivity(intent);
                }
            });
        }


//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        if (qtListModelArrayList == null)
            return 0;
        return qtListModelArrayList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        TextView dateText;
        TextView prcode ;
        TextView nameTxt;
        TextView netAmount;
        CardView cardPhase;
        public ProjectViewHolder(View view) {
            super(view);
            dateText = itemView.findViewById(R.id.dateText);
            prcode = itemView.findViewById(R.id.prcode);
            nameTxt = itemView.findViewById(R.id.nameTxt);
            netAmount = itemView.findViewById(R.id.netAmount);
            cardPhase = itemView.findViewById(R.id.card_phase);
//            title = (TextView) view.findViewById(R.id.title);
//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


}