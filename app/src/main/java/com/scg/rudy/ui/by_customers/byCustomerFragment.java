//package com.scg.rudy.ui.by_customers;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//
//import com.scg.rudy.R;
//import com.scg.rudy.data.remote.ApiEndPoint;
//import com.scg.rudy.injection.component.ActivityComponent;
//import com.scg.rudy.model.pojo.byCustomer.Customer;
//import com.scg.rudy.ui.by_projects.byProjectFragment;
//import com.scg.rudy.ui.search_customer.SearchCustomerActivity;
//import com.scg.rudy.base.BaseFragment;
//import com.scg.rudy.ui.main.MainActivity;
//import com.scg.rudy.ui.search_customer.SearchCustomerFragment;
//import com.scg.rudy.utils.Utils;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import javax.inject.Inject;
//
//import butterknife.ButterKnife;
//import okhttp3.ResponseBody;
//
//import static com.scg.rudy.utils.Utils.showToast;
//
///**
// * Created by DekDroidDev on 24/4/2018 AD.
// */
//public class byCustomerFragment extends BaseFragment implements View.OnClickListener, byCustomerMvpView {
//    private ImageView clsBtn,search_btn;
//    private String user_id = "";
//    private RecyclerView recyclerView;
//    private ByCustomerRecyclerAdapter adapter;
//    private ImageView searchBtn;
//    private Customer customer = null;
//
//
//
//    @Inject
//    byCustomerPresenter<byCustomerMvpView> byCustomerPresenter;
//    public static final String TAG = "byCustomerFragment";
//
//    public static byCustomerFragment newInstance() {
//        Bundle args = new Bundle();
//        byCustomerFragment fragment = new byCustomerFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_by_customers, container, false);
//        ActivityComponent component = getActivityComponent();
//        if (component != null) {
//            component.injectByCustomer(this);
//            setUnBinder(ButterKnife.bind(this, view));
//            byCustomerPresenter.attachView(this);
//        }
//        initView(view);
//        getUserData();
//        clsBtn.setOnClickListener(this);
//        search_btn.setOnClickListener(this);
//        return view;
//    }
//    @Override
//    public void onDestroyView() {
//        byCustomerPresenter.detachView();
//        super.onDestroyView();
//    }
//    public void getUserData() {
//        try {
//            JSONObject object = new JSONObject(Utils.getPrefer(getActivity(), Utils.PERF_LOGIN));
//            JSONObject itemObject = object.getJSONObject("items");
//            user_id = itemObject.optString("id");
//            shopID = itemObject.optString("shop_id");
////            if(BuildConfig.DEBUG){
////                user_id = "23";
////            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public void initView(View view){
//        clsBtn = view.findViewById(R.id.cls_btn);
//        search_btn = view.findViewById(R.id.search_btn);
//        recyclerView = view.findViewById(R.id.recyclerView);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        if(Utils.getPrefer(getActivity(),"print").length()>0){
//            MainActivity activ = (MainActivity) getActivity();
//            activ.onFragmentDetached(byCustomerFragment.TAG);
//        }else{
//            byCustomerPresenter.getCustomerList(ApiEndPoint.LANG,user_id,"list");
//        }
//
//        super.onResume();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        if(v==clsBtn){
//                MainActivity activ = (MainActivity) getActivity();
//                activ.onFragmentDetached(TAG);
//        }
//        if (v == search_btn) {
//            if(customer!=null){
//                if (getActivity() instanceof MainActivity) {
//                    MainActivity activ = (MainActivity) getActivity();
//                    activ.switchContent(SearchCustomerFragment.newInstance(customer),SearchCustomerFragment.TAG);
//                }
//            }else{
//                showToast(getBaseActivity(),"ยังไม่มีข้อมูลลูกค้า ไม่สามารถค้นหาได้ในขณะนี้");
//            }
//
//
////            Intent intent = new Intent(getActivity(), SearchCustomerActivity.class);
////            intent.putExtra("customer",customer);
////            startActivity(intent);
//
//        }
//    }
//
//
//
//    @Override
//    public void showData(Customer customer) {
//        this.customer = customer;
//
//        adapter = new ByCustomerRecyclerAdapter(getActivity(),customer.getItems());
////        SearchProjectActivity.arrayList = myprojects.getProjectDetailItems();
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
////        recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 0));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);
//    }
//
//    @Override
//    public void showErrorIsNull() {
//    }
//
//    @Override
//    public void BodyError(ResponseBody responseBodyError) {
//    }
//
//    @Override
//    public void Failure(Throwable t) {
//    }
//
//
//    @Override
//    protected void setUp(View view) {
//
//    }
//
//
//}
