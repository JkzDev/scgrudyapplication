package com.scg.rudy.ui.call_to_customer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Models.HideContentModel;

import java.util.ArrayList;

public class HideContentAdapter extends RecyclerView.Adapter<HideContentAdapter.ViewHolder> {
    private Context context;
    private ArrayList<HideContentModel> hideContentModels;

    public HideContentAdapter(Context _context, ArrayList<HideContentModel> _hideContentModels) {
        context = _context;
        hideContentModels = _hideContentModels;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtHead;
        TextView txtDetail;


        public ViewHolder(View view)
        {
            super(view);
            txtHead = view.findViewById(R.id.txtItemHead);
            txtDetail = view.findViewById(R.id.txtItemDetail);
        }

    }

    @NonNull
    @Override
    public HideContentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hide_content_call_to_customer, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HideContentAdapter.ViewHolder holder, int position) {
        holder.txtHead.setText(hideContentModels.get(position).getTxtHead());
        holder.txtDetail.setText(hideContentModels.get(position).getTxtDetail());
    }

    @Override
    public int getItemCount() {
        return hideContentModels.size();
    }
}
