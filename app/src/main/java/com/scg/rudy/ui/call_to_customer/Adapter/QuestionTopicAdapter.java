package com.scg.rudy.ui.call_to_customer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Models.QuestionTopicModel;

import java.util.ArrayList;

public class QuestionTopicAdapter extends RecyclerView.Adapter<QuestionTopicAdapter.ViewHolder> {

    private Context context;
    private ArrayList<QuestionTopicModel> questionTopicModels;

    public QuestionTopicAdapter(Context _context, ArrayList<QuestionTopicModel> _questionTopicModels) {
        context = _context;
        questionTopicModels = _questionTopicModels;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView txtHead;
        TextView txtBy;
        TextView txtDate;
        TextView amount;


        public ViewHolder(View view)
        {
            super(view);

            image = view.findViewById(R.id.imageHead);
            txtHead = view.findViewById(R.id.txtHead);
            txtBy = view.findViewById(R.id.txtBy);
            txtDate = view.findViewById(R.id.txtDate);
            amount = view.findViewById(R.id.amount);



        }

    }

    @NonNull
    @Override
    public QuestionTopicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_topic_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionTopicAdapter.ViewHolder holder, int position) {
        if (questionTopicModels.get(position).getImage() != null) {
            if (!questionTopicModels.get(position).getImage().isEmpty()) {
                Glide.with(context)
                        .load(questionTopicModels.get(position).getImage())
                        .into(holder.image);
            }else{
                Glide.with(context).clear(holder.image);
                holder.image.setImageDrawable(context.getDrawable(R.drawable.bg_photo_copy));
            }
        }

        holder.txtHead.setText(questionTopicModels.get(position).getTxtHead());
        holder.txtBy.setText(questionTopicModels.get(position).getBy());
        holder.txtDate.setText(questionTopicModels.get(position).getEndDate());
        holder.amount.setText(questionTopicModels.get(position).getAmount().toString());

    }

    @Override
    public int getItemCount() {
        return questionTopicModels.size();
    }
}
