package com.scg.rudy.ui.favorite;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.favorite.ListItem;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;

import java.util.ArrayList;

import static com.scg.rudy.ui.sku_confirm.SkuConfirmActivity.sub_class_name;

/**
 * @author DekDroidDev
 * @date 9/2/2018 AD
 */
public class FavRecyclerClassAdapter extends RecyclerView.Adapter<FavRecyclerClassAdapter.ProjectViewHolder> {

    private ArrayList<ListItem> cartModelArrayLis;
    private Context context;
    private FAvoriteInterface listener;
    private String projectId;

    public FavRecyclerClassAdapter(
            Context context,
            ArrayList<ListItem> cartModelArrayLis,
            FAvoriteInterface listener,
            String projectId) {
        this.cartModelArrayLis = cartModelArrayLis;
        this.context = context;
        this.listener = listener;
        this.projectId = projectId;
    }

    @Override
    public FavRecyclerClassAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fav_class_item, parent, false);
        return new FavRecyclerClassAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FavRecyclerClassAdapter.ProjectViewHolder holder, int position) {
        final ListItem prCodeDetailModel = cartModelArrayLis.get(position);

        holder.subPhaseName.setText(prCodeDetailModel.getName());
        holder.title_number.setText(String.format("%d", position + 1));
        holder.add_fav.setVisibility(View.VISIBLE);
        holder.promo_ico.setVisibility(View.INVISIBLE);
        holder.subPhaseName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChooseSkuActivity.class);
                intent.putExtra("class_id", prCodeDetailModel.getId());
                intent.putExtra("projectId", projectId);
                intent.putExtra("phase_id", "");
                intent.putExtra("subClass", prCodeDetailModel.getName());
                sub_class_name = prCodeDetailModel.getName();
                context.startActivity(intent);
            }
        });

        holder.add_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteClassFavorite(prCodeDetailModel.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null) {
            return 0;
        }
        return cartModelArrayLis.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        private TextView subPhaseName;
        private TextView title_number;
        private RelativeLayout add_fav;
        private ImageView icon_fav;
        private ImageView promo_ico;

        public ProjectViewHolder(View view) {
            super(view);
            subPhaseName = view.findViewById(R.id.sub_phase_name);
            add_fav = view.findViewById(R.id.add_fav);
            icon_fav = view.findViewById(R.id.icon_fav);
            promo_ico = view.findViewById(R.id.promo_ico);
            title_number = view.findViewById(R.id.title_number);
        }
    }
}
