package com.scg.rudy.ui.department_photo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.ui.adapter.GridViewAdapter;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_ui.floatingActionButton.FloatingActionButton;
import com.scg.rudy.utils.custom_view.rudyCamera.CameraActivity;
import com.scg.rudy.utils.custom_view.rudyCamera.ImageUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class TakeGroupPhotoActivity extends BaseActivity implements View.OnClickListener, TakePhotoInterface {
    private static final int REQUEST_CAMERA = 0;
    @BindView(R.id.cls_btn)
    ImageView clsBtn;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.save)
    LinearLayout save;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.txt_alert)
    TextView txtAlert;
    @BindView(R.id.btnSelectGallery)
    FloatingActionButton btnSelectGallery;
    @BindView(R.id.btnSelectPhoto)
    FloatingActionButton btnSelectPhoto;
    @BindView(R.id.grid_view)
    GridView gridView;
    private int SELECT_FILE = 1;
    private String userChoosenTask;
    public static ArrayList<Bitmap> mArrayList;
    public static ArrayList<String> nameList;
    private GridViewAdapter adapter;
    private Point mSize;
    public static final String TAG = "TakePhotoActivity";
    public static String group_id = "";
    private RudyService rudyService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        ButterKnife.bind(this);
        rudyService = ApiHelper.getClient();
        initView();
        getUserData();

        clsBtn.setOnClickListener(this);

        if(TakeGroupPhotoActivity.mArrayList==null){
            mArrayList = new ArrayList<>();
        }

        Display display = TakeGroupPhotoActivity.this.getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

        btnSelectPhoto.setVisibility(View.VISIBLE);
        if (mArrayList.size() > 0) {
            save.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {
            txtAlert.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
        }
//       //save.setVisibility(View.GONE);
        adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, this);
        gridView.setAdapter(adapter);
        btnSelectPhoto.setOnClickListener(this);
        save.setOnClickListener(this);
        btnSelectGallery.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(TakeGroupPhotoActivity.this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mArrayList.add(bm);
        if (mArrayList.size() > 0) {
            save.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {
            txtAlert.setVisibility(View.VISIBLE);
        }
        MainActivity.gdata2 = true;
        AddNewGroupProjectActivity.step2 = true;
        adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, this);
        gridView.setAdapter(adapter);
        ivImage.setImageBitmap(bm);
        if(group_id.length()>0){
            addPic(bm);

        }else{
            showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_save_image_success));
        }



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
            if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }

        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri photoUri = data.getData();
        Bitmap thumbnail = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mArrayList.add(thumbnail);
        if (mArrayList.size() > 0) {
            save.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {
            txtAlert.setVisibility(View.VISIBLE);
        }
        MainActivity.gdata2 = true;
        AddNewGroupProjectActivity.step2 = true;
        adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, this);
        gridView.setAdapter(adapter);
        ivImage.setImageBitmap(thumbnail);

        if(group_id.length()>0){
            addPic(thumbnail);
        }else{
            showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_save_image_success));
        }

    }

    public void requestForCameraPermission(View view) {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(TakeGroupPhotoActivity.this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(TakeGroupPhotoActivity.this, permission)) {
                showPermissionRationaleDialog(getResources().getString(R.string.dialog_pls_allow_to_access_camera), permission);
            } else {
                requestForPermission(permission);
            }
        } else {
            launch();
        }

    }

    private void showPermissionRationaleDialog(final String message, final String permission) {
        new AlertDialog.Builder(TakeGroupPhotoActivity.this)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestForPermission(permission);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }


    private Call<String> callDeletePic(String _pic) {
        return rudyService.deletePic(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deletePic(String _pic){
        showLoading(TakeGroupPhotoActivity.this);
        callDeletePic(_pic).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    if (mArrayList.size() > 0) {
                        save.setVisibility(View.VISIBLE);
                        txtAlert.setVisibility(View.GONE);
                    } else {
                        txtAlert.setVisibility(View.VISIBLE);
                    }
                    adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, TakeGroupPhotoActivity.this);
                    gridView.setAdapter(adapter);

                    showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_delete_image_success));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(TakeGroupPhotoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TakeGroupPhotoActivity.this, t.getMessage());
            }
        });

    }

//    @SuppressLint("StaticFieldLeak")
//    private void deletePic(final String url, final String imageName) {
//        showLoading(this);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("pic", imageName)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    if (mArrayList.size() > 0) {
//                        save.setVisibility(View.VISIBLE);
//                        txtAlert.setVisibility(View.GONE);
//                    } else {
//                        txtAlert.setVisibility(View.VISIBLE);
//                    }
//                    adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, TakeGroupPhotoActivity.this);
//                    gridView.setAdapter(adapter);
//
//                    showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_delete_image_success));
//                }
//
//            }
//        }.execute();
//    }


    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(TakeGroupPhotoActivity.this, new String[]{permission}, REQUEST_CAMERA);
    }

    private void launch() {
        Intent startCustomCameraIntent = new Intent(TakeGroupPhotoActivity.this, CameraActivity.class);
        startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                final int numOfRequest = grantResults.length;
                final boolean isGranted = numOfRequest == 1
                        && PackageManager.PERMISSION_GRANTED == grantResults[numOfRequest - 1];
                if (isGranted) {
                    launch();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void initView(){
        clsBtn = findViewById(R.id.cls_btn);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View v) {
        if (v == save) {
            if (mArrayList.size() > 0) {
                MainActivity.gdata2 = true;

                AddNewGroupProjectActivity.mArrayList = mArrayList;
            }else{

                MainActivity.gdata2 = false;
                AddNewGroupProjectActivity.step2 = false;
                AddNewGroupProjectActivity.mArrayList = new ArrayList<>();

            }

          finish();
        }
        if (v == btnSelectPhoto) {
            requestForCameraPermission(btnSelectPhoto);
        }

        if (v == btnSelectGallery) {
            galleryIntent();
        }
        if (v == clsBtn) {
            if (mArrayList.size() > 0) {
                MainActivity.gdata2 = true;
                AddNewGroupProjectActivity.mArrayList = mArrayList;

            } else {
                MainActivity.gdata2 = false;
                AddNewGroupProjectActivity.mArrayList = new ArrayList<>();
            }
          finish();
        }

    }



    @Override
    public void onClickDeletePhoto(String img) {
        if(Utils.project_id.length()>0){
            deletePic(nameList.get(mArrayList.indexOf(img)));
            mArrayList.remove(img);
        }else{
            mArrayList.remove(img);
            if (mArrayList.size() > 0) {
                save.setVisibility(View.VISIBLE);
                txtAlert.setVisibility(View.GONE);
            } else {
                txtAlert.setVisibility(View.VISIBLE);
            }

            adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, this);
            gridView.setAdapter(adapter);
            MainActivity.gdata2 = mArrayList.size() > 0;

            showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_delete_image_success));
        }
    }

    @Override
    public void onShowPhoto(String img) {
        final Dialog dialog = new Dialog(TakeGroupPhotoActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.view_image);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
        dialog.getWindow().setLayout(width, height);
        SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
        Button close = dialog.findViewById(R.id.close);
        RelativeLayout hideView = dialog.findViewById(R.id.hideView);

        new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... voids) {
                if (Utils.project_id.length() > 0){
                    try {
                        URL url = new URL(img);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);
                        return myBitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }else{
                    File f = new File(img);
                    Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                    return d;
                }
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                image.setImage(ImageSource.bitmap(bitmap));
                image.setMaxScale(20.0f);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                hideView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }.execute();
    }

    private Call<String> callAddPic(String _pic) {
        return rudyService.addPGPic(
                Utils.APP_LANGUAGE,
                group_id,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void addPic(Bitmap _bitmap){
        showLoading(TakeGroupPhotoActivity.this);
        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(_bitmap);
        callAddPic(image64).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, TakeGroupPhotoActivity.this);
                    gridView.setAdapter(adapter);
                    showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_save_image_success));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(TakeGroupPhotoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TakeGroupPhotoActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void addPic(final String url, final Bitmap bitmap) {
//        showLoading(this);
//        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bitmap);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("pic", image64)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    adapter = new GridViewAdapter(TakeGroupPhotoActivity.this, mArrayList, TakeGroupPhotoActivity.this);
//                    gridView.setAdapter(adapter);
//                    showToast(TakeGroupPhotoActivity.this, getResources().getString(R.string.toast_save_image_success));
//                }
//
//            }
//        }.execute();
//    }


    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
