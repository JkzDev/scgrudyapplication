package com.scg.rudy.ui.project_by_group;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.model.pojo.project_group_detail.ProjectListItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.department_data_child.DepartmentDataNewActivity;
import com.scg.rudy.ui.project_by_group.adapter.PGdetailAdapter;
import com.scg.rudy.utils.Utils;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class ProjectByGroupActivity extends BaseActivity implements View.OnClickListener {

    private String group_id;
    private RudyService rudyService;
    private PGdetailAdapter adapter;
    private ImageView mClsBtn;
    private TextView mTitleName;
    private LinearLayout mNavbar;
    private RecyclerView mRecyclerView;
    private Bundle extras;
    private RelativeLayout addSubProjectBtn;
    private ProjectGroupDetail pgDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projectbygroup);
        extras = getIntent().getExtras();
        if (extras != null) {
            group_id = extras.getString("group_id", "");
        }
        initView();
        setOnClick();

    }

    private void setOnClick(){
        addSubProjectBtn.setOnClickListener(this);
        mClsBtn.setOnClickListener(this);
        mTitleName.setOnClickListener(this);
        mNavbar.setOnClickListener(this);
        mRecyclerView.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getViewProjectGroup();
    }

    @Override
    public void onClick(View v) {
        if (v == mClsBtn) {
            onBackPressed();
        }

        if(v==addSubProjectBtn){
            addSubProject();
        }
    }

    private void addSubProject(){
        Intent intent = new Intent(ProjectByGroupActivity.this, DepartmentDataNewActivity.class);
        intent.putExtra("group_id", group_id);
        intent.putExtra("projectGroupName", pgDetail.getItems().getName());
        intent.putExtra("devName", pgDetail.getItems().getAddedDatetime());
        intent.putExtra("project_group_list_id", pgDetail.getItems().getProjectGroupListId());
        intent.putExtra("developer_id",pgDetail.getItems().getProjectGroupListId());
        intent.putExtra("timeStart",  pgDetail.getItems().getStartDate());
        intent.putExtra("timeEnd",   pgDetail.getItems().getEndDate());
        intent.putExtra("addNew",   "add");

        Utils.project_id = "";

        startActivity(intent);
    }

    private void getViewProjectGroup() {
        showLoading(this);
        Call<String> call = rudyService.getPGdetail(Utils.APP_LANGUAGE, group_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ProjectGroupDetail projectGroupDetail  = gson.fromJson(resp, ProjectGroupDetail.class);
                    pgDetail = projectGroupDetail;
                    List<ProjectListItem> list =  pgDetail.getItems().getProjectList();
                    if(list.size()>0){
                        setProjectChildItem(list);
                    }else{
                        mRecyclerView.setAdapter(null);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProjectByGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                Toast.makeText(ProjectByGroupActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setProjectChildItem(List<ProjectListItem> list){
        adapter = new PGdetailAdapter(this, list);
        mRecyclerView.setAdapter(adapter);
    }

    private void initView() {
        rudyService = ApiHelper.getClient();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProjectByGroupActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mClsBtn = findViewById(R.id.cls_btn);
        mTitleName = findViewById(R.id.title_name);
        mNavbar = findViewById(R.id.navbar);
        addSubProjectBtn = findViewById(R.id.add_subProject_btn);
    }
}
