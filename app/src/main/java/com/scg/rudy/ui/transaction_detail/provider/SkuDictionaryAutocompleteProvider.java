/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.transaction_detail.provider;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scg.rudy.ui.transaction_detail.model.skuModel;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutocompleteProvider;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.Collection;

import timber.log.Timber;

import static com.scg.rudy.data.remote.ApiEndPoint.HOST;
import static com.scg.rudy.data.remote.ApiEndPoint.Url;

/**
 * @author jackie
 */
public class SkuDictionaryAutocompleteProvider extends SkuAbstractProvider implements DynamicAutocompleteProvider<skuModel> {
    private   String REST_URL = HOST + "/" + Url + "%s/sku/%s?act=search_v2&project_id=%s&user_id=%s&page=1";

    @Override
    public Collection<skuModel> provideDynamicAutocompleteItems(String constraint) {
        Timber.tag("Constraint");
        Timber.i("Constraint: " + constraint);
        String queryUrl = String.format(REST_URL,TH,shopID,projectId,userId);
        String json = invokeRestQuery(queryUrl,constraint);
        Type type = new TypeToken<Collection<skuModel>>(){}.getType();
        return new Gson().fromJson(json,type);
    }
}