package com.scg.rudy.ui.favorite;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CartModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.PRCodeDetailModel;
import com.scg.rudy.model.pojo.QTListModel;
import com.scg.rudy.model.pojo.favorite.Favorite;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/** @author jackie */
public class FavoriteDetailActivity extends BaseActivity
        implements View.OnClickListener,FAvoriteInterface {

    private RudyService rudyService;
    private ImageView clsBtn;
    private TextView nodata2;
    public static ArrayList<CartModel> cartModelArrayList;
    private String user_id = "";
    private Bundle extras;
    private String transaction_id = "";
    private TextView title_name;
    private RecyclerView recyclerView;
    public static String basketString = "";
    private ArrayList<PRCodeDetailModel> arrayList;
    public static QTListModel prCodeDetailModel;
    private TextView totalItem;
    private boolean isShowFAV = false;
    public static int from_fav = 0;
    int totalPrice = 0;
    ArrayList<String> idList;
    public static boolean isDelete = false;
    public static String productId = "";

    private RelativeLayout btnSku;
    private RelativeLayout btnClass;
    private TextView textSku;
    private TextView textClass;
    private int selectTab = 1;
    private Favorite favoriteData;

    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private boolean firstClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);
        arrayList = new ArrayList<>();
        initView();
        setOnclick();
        getUserData();
        idList = new ArrayList<>();
        extras = getIntent().getExtras();
        if (extras != null) {
            transaction_id = extras.getString("transaction_id");
        }
    }

    private Favorite getFavoriteData() {
        if (favoriteData != null) {
            return favoriteData;
        }
        return new Favorite();
    }

    private void setFavorite(Favorite favorite) {
        this.favoriteData = favorite;
    }

    private Call<String> getFavoriteList() {
        return rudyService.callFavoriteList(Utils.APP_LANGUAGE, Utils.project_id);
    }

    private void loadFavorite() {
       showLoading(this);
        hideErrorView();
        getFavoriteList()
                .enqueue(
                        new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                hideLoading();
                                hideErrorView();
                                recyclerView.setAdapter(null);
                                Gson gson = new Gson();
                                String resp = response.body();
                                if(resp.contains("200")){
                                    Favorite results  = gson.fromJson(resp, Favorite.class);
                                    if (results.getItems().get(0).getTotal() > 0
                                            || results.getItems().get(1).getTotal() > 0) {
                                        errorLayout.setVisibility(View.GONE);
                                        setFavoriteData(results);

                                    } else {


                                        textSku.setText(String.format(getResources().getString(R.string.sku_parame), 0));
                                        textClass.setText(String.format(getResources().getString(R.string.product_type_parame), 0));

                                        if (Utils.tabSelect == 1) {
                                            txtError.setText(getResources().getString(R.string.not_found_favorite_product_in_sku));
                                        } else {
                                            txtError.setText(getResources().getString(R.string.not_found_favorite_product_in_product_type));
                                        }

                                        errorLayout.setVisibility(View.VISIBLE);
                                    }
                                }else{
                                    APIError error  = gson.fromJson(resp, APIError.class);
                                    showToast(FavoriteDetailActivity.this, error.getItems());
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                hideLoading();
                                t.printStackTrace();
                                showErrorView(t);
                                textSku.setText(String.format(getResources().getString(R.string.sku_parame), 0));
                                textClass.setText(String.format(getResources().getString(R.string.product_type_parame), 0));

                                if (Utils.tabSelect == 1) {
                                    setSkuTab();
                                } else {
                                    setClassTab();
                                }
                            }
                        });
    }

    private void setFavoriteData(Favorite favoriteData) {
        setFavorite(favoriteData);
        textSku.setText(String.format(getResources().getString(R.string.sku_parame), favoriteData.getItems().get(0).getTotal()));
        textClass.setText(String.format(getResources().getString(R.string.product_type_parame), favoriteData.getItems().get(1).getTotal()));

        if(!firstClick){
            if (favoriteData.getItems().get(0).getTotal()>0){
                btnSku.performClick();
            }else{
                btnClass.performClick();
            }
        }


        if (Utils.tabSelect == 1) {
            setSkuTab();
        } else {
            setClassTab();
        }
    }

    private void setSkuTab() {
        recyclerView.setAdapter(null);

        if(getFavoriteData().getItems().get(0).getList().size()>0){
            hideErrorView();
            FavRecyclerListAdapter adapter =
                    new FavRecyclerListAdapter(
                            this,
                            getFavoriteData().getItems().get(0).getList(),
                            this);
            LinearLayoutManager linearLayoutManager =
                    new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }else{
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_favorite_product_in_sku));
            textSku.setText(String.format(getResources().getString(R.string.sku_parame), 0));


        }



    }

    private void setClassTab() {
        recyclerView.setAdapter(null);

        if(getFavoriteData().getItems().get(1).getList().size()>0){
            hideErrorView();
            FavRecyclerClassAdapter adapter =
                    new FavRecyclerClassAdapter(
                            this,
                            getFavoriteData().getItems().get(1).getList(),
                            this,
                            Utils.project_id);
            LinearLayoutManager linearLayoutManager =
                    new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }else{
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_favorite_product_in_product_type));
            textClass.setText(String.format(getResources().getString(R.string.product_type_parame), 0));
        }



    }

    private Favorite fetchResults(Response<Favorite> response) {
        return response.body();
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {


            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }

    public void setOnclick() {
        clsBtn.setOnClickListener(this);
        btnSku.setOnClickListener(this);
        btnClass.setOnClickListener(this);
    }

    private void initView() {
        Utils.tabSelect = 1;
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        nodata2 = findViewById(R.id.nodata2);
        title_name = findViewById(R.id.title_name);
        recyclerView = findViewById(R.id.recyclerView);
        if (prCodeDetailModel != null) {
            title_name.setText(prCodeDetailModel.getQo_code());
        }

        totalItem = findViewById(R.id.totalItem);
        btnSku = findViewById(R.id.btn_sku);
        btnClass = findViewById(R.id.btn_class);
        textSku = (TextView) findViewById(R.id.text_sku);
        textClass = (TextView) findViewById(R.id.text_class);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }

        if (v == btnSku) {
            firstClick = true;
            selectTab = 1;
            btnSku.setBackground(getDrawable(R.drawable.rounder_cus_left_select));
            btnClass.setBackground(getDrawable(R.drawable.rounder_cus_right_unselect));

            textSku.setTextColor(getResources().getColor(R.color.black));
            textClass.setTextColor(getResources().getColor(R.color.white));
            Utils.tabSelect = selectTab;

            setSkuTab();
        }

        if (v == btnClass) {
            firstClick = true;
            selectTab = 2;
            btnSku.setBackground(getDrawable(R.drawable.rounder_cus_left_unselect));
            btnClass.setBackground(getDrawable(R.drawable.rounder_cus_right_select));

            textSku.setTextColor(getResources().getColor(R.color.white));
            textClass.setTextColor(getResources().getColor(R.color.black));
            Utils.tabSelect = selectTab;
            setClassTab();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDelete) {
//            deleteItems(ApiEndPoint.getDeleteFAV(Utils.APP_LANGUAGE, Utils.project_id, productId));
            deleteItems();

        } else {
            //            favoritePresenter.callFavoriteList("th",Utils.project_id);
            loadFavorite();
        }
    }

    private Call<String> callRemoveFAV() {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                project_id,
                productId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteItems(){
        showLoading(FavoriteDetailActivity.this);
        callRemoveFAV().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    //                    showToast(FavoriteDetailActivity.this,
                    // "ลบรายการสินค้าสำเร็จ");
                    loadFavorite();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(FavoriteDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(FavoriteDetailActivity.this, t.getMessage());
            }
        });

    }


//    @SuppressLint("StaticFieldLeak")
//    private void deleteItems(final String url) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.getData(url);
//
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    //                    showToast(FavoriteDetailActivity.this,
//                    // "ลบรายการสินค้าสำเร็จ");
//                    loadFavorite();
//                }
//            }
//        }.execute();
//    }

    public void onFragmentDetached(String tag) {}

    public void onError(int resId) {}

    public void onError(String message) {}

    public void showMessage(String message) {}

    public void showMessage(int resId) {}

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {}

    public void showcallFavoriteList(Favorite favorite) {
        //        if(favorite.getSkuItems().size()>0){
        //            nodata2.setVisibility(View.GONE);
        //            FavRecyclerListAdapter adapter = new FavRecyclerListAdapter(this,
        // favorite.getSkuItems(), favoritePresenter);
        //            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //            recyclerView.setItemAnimator(new DefaultItemAnimator());
        //            recyclerView.setAdapter(adapter);
        //        }else{
        //            nodata2.setVisibility(View.VISIBLE);
        //            recyclerView.setAdapter(null);
        //        }

    }

    public void callFavoriteListError(ResponseBody responseBodyError) {
        nodata2.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
    }

    public void callFavoriteListFail(Throwable t) {
        nodata2.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
    }

    @Override
    public void deleteFAvorite(String _productId) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getResources().getString(R.string.dialog_delete_from_favorite))
                .content(getResources().getString(R.string.dialog_confirm_delete_from_favorite))
                .positiveText(getResources().getString(R.string.agreed))
                .negativeText(getResources().getString(R.string.cancel))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        productId = _productId;
//                        deleteItems( ApiEndPoint.getDeleteFAV(Utils.APP_LANGUAGE,Utils.project_id,productId));\
                        deleteItems();

                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                });

        MaterialDialog del_dialog = builder.build();
        del_dialog.show();



    }

    @Override
    public void deleteClassFavorite(String classId) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getResources().getString(R.string.dialog_delete_from_favorite))
                .content(getResources().getString(R.string.dialog_confirm_delete_from_favorite))
                .positiveText(getResources().getString(R.string.agreed))
                .negativeText(getResources().getString(R.string.cancel))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                        removeFavClass(classId);
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                });

        MaterialDialog del_dialog = builder.build();
        del_dialog.show();
    }

    private Call<String> callRemoveFavClass(String _classID) {
        return rudyService.removeFAVClass(
                Utils.APP_LANGUAGE,
                project_id,
                _classID

        );
    }


    @SuppressLint("StaticFieldLeak")
    public void removeFavClass(String _classID){
        showLoading(FavoriteDetailActivity.this);
        callRemoveFavClass(_classID).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(FavoriteDetailActivity.this,getResources().getString(R.string.dialog_delete_finish));
                    loadFavorite();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(FavoriteDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(FavoriteDetailActivity.this, t.getMessage());
            }
        });

    }

//    @SuppressLint("StaticFieldLeak")
//    public void removeFavClass(final String url, final String msg) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(FavoriteDetailActivity.this,msg);
//
//                    loadFavorite();
//                }
//            }
//        }.execute();
//    }


}
