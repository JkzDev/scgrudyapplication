package com.scg.rudy.ui.current_location;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.duppicate_project.ItemsItem;
import com.scg.rudy.model.pojo.duppicate_project.MapDupplicate;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.ui.add_new_project.AddNewProjectActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.scg.rudy.base.BaseActivity.LogException;
import static com.scg.rudy.utils.Utils.showToast;

/** @author jackie */
public class AddCurrentGroupLocationActivity extends FragmentActivity
        implements OnMapReadyCallback,
                View.OnClickListener,
                GoogleApiClient.OnConnectionFailedListener,
                GoogleMap.OnCameraChangeListener {

    public LocationManager mLocationManager;
    public static final String TAG = "AddCurrentLocationActivity";
    private final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    protected GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private Double currentLat = 0.0, currentLng = 0.0;
    public static Double DragLat = 0.0, DragLng = 0.0;
    private LatLng pos;
    private Location location;
    private LocationListener mLocationListener;
    private Marker mMarker;
    private LocationManager lm;
    private double lat, lng;
    private ImageView clsBtn;
    private RelativeLayout nextStep;
    private TextView address;
    private TextView latlng;
    private TextView titleName;
    private RelativeLayout latlngLayout;
    private LinearLayout typePhase;
    private File mFileTemp;
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private String pickerPath;
    private Bitmap bitmapProfile = null;
    private ImageView placeImage;
    private String cateId = "";
    private String phaseId = "";
    private String user_id = "";
    private String profileBase64;
    private String iname = "";
    private LatLng coordinate;
    private LinearLayout curBtn;
    private Dialog dialogLoader;
    private RelativeLayout edit_localtion_btn;
    private ImageView dragPin;
    private boolean isIsEditClick = false;
    // static var
    public static String shopID = "";
    public static boolean isEdit = false;
    public static String editlatlng = "";
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;

    public static boolean isGroup = false;

    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;

    private RudyService rudyService;
    private Dialog dialog;
    public static String project_address = ""; // : project location address

    private LayoutInflater layoutInflater;
    private ArrayList<ItemsItem> projectListItems = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getUserData();
        initView();
        if (isEdit) {
            edit_localtion_btn.setVisibility(View.VISIBLE);
            nextStep.setVisibility(View.GONE);
            dragPin.setVisibility(View.GONE);
        }

        dialogLoader = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoader.setCancelable(true);
        dialogLoader.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialogLoader, this);

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mGoogleApiClient =
                new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, GOOGLE_API_CLIENT_ID /* clientId */, this)
                        .addApi(Places.GEO_DATA_API)
                        .addApi(Places.PLACE_DETECTION_API)
                        .build();
        mGoogleApiClient.connect();

        mLocationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location loc) {
                        coordinate = new LatLng(loc.getLatitude(), loc.getLongitude());
                        lat = loc.getLatitude();
                        lng = loc.getLongitude();
                        if (mMarker != null) mMarker.remove();

                        if (editlatlng.length() > 0) {
                            String[] latarr = editlatlng.split(",");
                            lat = Double.parseDouble(latarr[0]);
                            lng = Double.parseDouble(latarr[1]);
                            mMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));
                            //                    mMarker.setSnippet();
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18.0f));
                        } else {
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18.0f));
                        }

                        try {
                            Geocoder gcd = new Geocoder(AddCurrentGroupLocationActivity.this, new Locale(Utils.APP_LANGUAGE.toUpperCase()));
                            List<Address> addresses = gcd.getFromLocation(lat, lng, 1);
                            if (addresses.size() > 0) {
                                String distric = addresses.get(0).getAddressLine(0);
                                String province = addresses.get(0).getAddressLine(1);
                                String country = addresses.get(0).getAddressLine(2);
                                String zip = addresses.get(0).getAddressLine(3);
                                //                        String location_all = distric + "," +
                                // province + "," + country + "," + zip;
                                String location_all =
                                        addresses.get(0).getFeatureName()
                                                + ", "
                                                + addresses.get(0).getSubAdminArea()
                                                + ", "
                                                + addresses.get(0).getAdminArea();
                                address.setText(
                                        location_all.replace("null", "").replace(",, ", ""));
                                latlng.setText(lat + ", " + lng);
                                DragLat = lat;
                                DragLng = lng;

                                currentLat  = DragLat;
                                currentLng = DragLng;

                                dialogLoader.dismiss();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        final Handler handler = new Handler();
                        handler.postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
//                                        onShowDupplicateLocation();

                                        callCheckMapDuplicateApi(DragLat,DragLng);

                                    }
                                },
                                2500);


                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {}

                    @Override
                    public void onProviderEnabled(String provider) {}

                    @Override
                    public void onProviderDisabled(String provider) {}
                };

        final Handler handler = new Handler();
        handler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        handlePermissionsAndGetLocation();
                    }
                },
                2000);
        setClickAction();
        setData();

    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }


    public void setData() {
        titleName.setText(getString(R.string.title_step1));
    }

    public void setClickAction() {
        nextStep.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        edit_localtion_btn.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void handlePermissionsAndGetLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission =
                    checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
            getLocation();
        } else {
            getLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Toast.makeText(this, "LOCATION Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        //        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(this);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);

        final Location location = getLastKnownLocation();
        if (location != null) {
            currentLat = location.getLatitude();
            currentLng = location.getLongitude();
            DragLat = location.getLatitude();
            DragLng = location.getLongitude();
            LatLng myLocation = null;
            if (isEdit) {
                if (editlatlng.length() > 0) {
                    String[] latarr = editlatlng.split(",");
                    lat = Double.parseDouble(latarr[0]);
                    lng = Double.parseDouble(latarr[1]);
                }
            } else {
                lat = location.getLatitude();
                lng = location.getLongitude();
            }
            myLocation = new LatLng(lat, lng);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(18.0f));
            //            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng),18.0f));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));


            dialogLoader.dismiss();
        }

        @SuppressLint("ResourceType")
        final View myLocationButton = mapFragment.getView().findViewById(0x2);
        myLocationButton.setVisibility(View.INVISIBLE);
        curBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myLocationButton.performClick();
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(currentLat, currentLng)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(DragLat, DragLng), 18.0f));
                        callCheckMapDuplicateApi(DragLat,DragLng);

                    }
                });
    }

    private void onShowDupplicateLocation(ArrayList<ItemsItem> items) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_map_group);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        LinearLayout project_group_dupplicate = dialog.findViewById(R.id.project_group_dupplicate);
        RelativeLayout btncreateNew = dialog.findViewById(R.id.btncreateNew);

        int sizeItems = (items.size() - items.size()) +1;
        for (int i = 0; i <items.size(); i++) {
            final View view = layoutInflater.inflate(R.layout.dialog_map_group_items, project_group_dupplicate, false);
            ImageView project_image = view.findViewById(R.id.project_image);
            TextView project_name = view.findViewById(R.id.project_name);
            TextView project_create = view.findViewById(R.id.project_create);

            project_name.setText(items.get(i).getName());
            project_create.setText(items.get(i).getAddedDatetime());
            int finalI = i;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nextStep.setVisibility(View.VISIBLE);
                    onShowSelectLocation(items.get(finalI));
                    dialog.dismiss();
                }
            });



            project_group_dupplicate.addView(view);
        }



        btncreateNew.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        nextStep.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }
                });

        dialog.show();
    }

    private void onShowSelectLocation(ItemsItem projectData) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_map_group_2);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        ImageView project_image = dialog.findViewById(R.id.project_image);
        TextView project_name = dialog.findViewById(R.id.project_name);
        TextView project_create = dialog.findViewById(R.id.project_create);

        FrameLayout frameLayout_yes = dialog.findViewById(R.id.frameLayout_yes);
        FrameLayout frameLayout_no = dialog.findViewById(R.id.frameLayout_no);

        project_name.setText(projectData.getName());
        project_create.setText(projectData.getAddedDatetime());

        frameLayout_yes.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AddNewGroupProjectActivity.group_id = projectData.getId();
                        AddNewGroupProjectActivity.DragLat = DragLat;
                        AddNewGroupProjectActivity.DragLng = DragLng;
                        AddNewGroupProjectActivity.address = address.getText().toString();
                        AddNewGroupProjectActivity.step1 = true;
                        MainActivity.gdata1 = true;
                        AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
                        AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
                        AddNewGroupProjectActivity.lat_lng = DragLat + "," + DragLng;
                        AddNewGroupProjectActivity.project_address = address.getText().toString();

                        AddNewGroupProjectActivity.name = projectData.getName();
                        AddNewGroupProjectActivity.project_group_list_id = projectData.getProjectGroupTypeId();
                        AddNewGroupProjectActivity.developer_id = projectData.getDeveloperId();
                        AddNewGroupProjectActivity.developer_name_other = projectData.getDeveloperName();
                        AddNewGroupProjectActivity.start_date = projectData.getStartDate();
                        AddNewGroupProjectActivity.end_date =projectData.getEndDate();


                        dialog.dismiss();
                        finish();

                    }
                });

        frameLayout_no.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onShowDupplicateLocation(projectListItems);
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }


    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        boolean isNetwork = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Location bestLocation = null;

        int time = 20 * 1000 * 60;
        if (isNetwork) {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, time, 15, mLocationListener);
            bestLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        if (bestLocation == null) {
            if (isGPS) {
                mLocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, time, 15, mLocationListener);
                bestLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }

        return bestLocation;
    }

    @SuppressLint("LongLogTag")
    protected void getLocation() {
        Log.v(TAG, "GetLocation");
        int LOCATION_REFRESH_TIME = 5 * 1000 * 60;
        int LOCATION_REFRESH_DISTANCE = 5;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)) {
                mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                mLocationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        LOCATION_REFRESH_TIME,
                        LOCATION_REFRESH_DISTANCE,
                        mLocationListener);
                mapFragment =
                        (SupportMapFragment)
                                getSupportFragmentManager().findFragmentById(R.id.mapView);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(this);
                }
            } else {
                Log.v("WEAVER_", "Does not have permission");
                Toast.makeText(this, "LOCATION Denied", Toast.LENGTH_SHORT).show();
                //                finish();
            }
        } else {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE,
                    mLocationListener);
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onClick(View v) {

        if (v == clsBtn) {
            finish();
        }

        if (v == nextStep) {
            AddNewGroupProjectActivity.DragLat = DragLat;
            AddNewGroupProjectActivity.DragLng = DragLng;
            AddNewGroupProjectActivity.address = address.getText().toString();
            AddNewGroupProjectActivity.step1 = true;
            MainActivity.gdata1 = true;
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewGroupProjectActivity.lat_lng = DragLat + "," + DragLng;
            AddNewGroupProjectActivity.project_address = address.getText().toString();
            finish();
        }
        if (v == edit_localtion_btn) {
            nextStep.setVisibility(View.VISIBLE);
            edit_localtion_btn.setVisibility(View.GONE);
            isIsEditClick = true;
            dragPin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        DragLat = cameraPosition.target.latitude;
        DragLng = cameraPosition.target.longitude;

        currentLat = DragLat;
        currentLng =DragLng;

        if (isIsEditClick) {
            try {
                Geocoder gcd = new Geocoder(AddCurrentGroupLocationActivity.this, new Locale(Utils.getLang(this)));
                List<Address> addresses =
                        gcd.getFromLocation(
                                cameraPosition.target.latitude, cameraPosition.target.longitude, 1);
                LatLng local =
                        new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
                Log.i("onCameraChange", "onCameraChange: " + addresses.toString());
                //            if (PlaceLat == 0.0 && PlaceLng == 0.00) {
                if (addresses.size() > 0) {
                    String distric = addresses.get(0).getAddressLine(0);
                    String province = addresses.get(0).getAddressLine(1);
                    String country = addresses.get(0).getAddressLine(2);
                    String zip = addresses.get(0).getAddressLine(3);
                    //                String location_all = distric + "," + province + "," + country
                    // + "," + zip;
                    String location_all =
                            addresses.get(0).getFeatureName()
                                    + ", "
                                    + addresses.get(0).getSubAdminArea()
                                    + ", "
                                    + addresses.get(0).getAdminArea();
                    address.setText(distric);
                    latlng.setText(DragLat + ", " + DragLng);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Geocoder gcd = new Geocoder(AddCurrentGroupLocationActivity.this, new Locale(Utils.getLang(this)));
                List<Address> addresses =
                        gcd.getFromLocation(
                                cameraPosition.target.latitude, cameraPosition.target.longitude, 1);
                LatLng local =
                        new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
                Log.i("onCameraChange", "onCameraChange: " + addresses.toString());
                //            if (PlaceLat == 0.0 && PlaceLng == 0.00) {
                if (addresses.size() > 0) {
                    String distric = addresses.get(0).getAddressLine(0);
                    String province = addresses.get(0).getAddressLine(1);
                    String country = addresses.get(0).getAddressLine(2);
                    String zip = addresses.get(0).getAddressLine(3);
                    //                String location_all = distric + "," + province + "," + country
                    // + "," + zip;
                    String location_all =
                            addresses.get(0).getFeatureName()
                                    + ", "
                                    + addresses.get(0).getSubAdminArea()
                                    + ", "
                                    + addresses.get(0).getAdminArea();
                    address.setText(distric);
                    latlng.setText(DragLat + ", " + DragLng);
                }


//                            getNearLocation(ApiEndPoint.getProjectByLocation("th", shopID, "" + DragLat, "" + DragLng));
            } catch (IOException e) {
                e.printStackTrace();
            }



        }


        @SuppressLint("ResourceType")
        final View myLocationButton = mapFragment.getView().findViewById(0x2);
        myLocationButton.setVisibility(View.INVISIBLE);
        curBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myLocationButton.performClick();
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(currentLat, currentLng)));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(DragLat, DragLng), 18.0f));
                        callCheckMapDuplicateApi(DragLat,DragLng);

                    }
                });
    }

    @Override
    public void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(mLocationListener);
        if (mapFragment != null) {
            try {
                mapFragment.onStop();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initView() {
        layoutInflater = LayoutInflater.from(this);
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        nextStep = findViewById(R.id.save);
        address = findViewById(R.id.address);
        latlng = findViewById(R.id.latlng);
        titleName = findViewById(R.id.title_name);
        edit_localtion_btn = findViewById(R.id.edit_localtion_btn);
        curBtn = findViewById(R.id.cur_btn);
        dragPin = findViewById(R.id.dragPin);
    }

    private MapDupplicate fetchMapDupplicateResults(Response<MapDupplicate> response) {
        return response.body();
    }


    private void callCheckMapDuplicateApi(double lat,double lng) {
        Dialog dialog = showLoading(this);
        callPostEditProjectGroup(lat,lng).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading(dialog);
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    MapDupplicate mapDupplicate  = gson.fromJson(resp, MapDupplicate.class);
                    showMapDupplicateData(mapDupplicate);
                }else{
                    nextStep.setVisibility(View.VISIBLE);
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(AddCurrentGroupLocationActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading(dialog);
                t.printStackTrace();
                showToast(AddCurrentGroupLocationActivity.this, t.getMessage());
            }
        });
    }

    private void showMapDupplicateData(MapDupplicate data){
        if(data.getItems().size()>0){
            projectListItems = data.getItems();

            onShowDupplicateLocation(projectListItems);
            //showToast(AddCurrentGroupLocationActivity.this,String.valueOf(data.getItems().size()));
        }


    }

    private Call<String> callPostEditProjectGroup(double lat,double lng) {
        return rudyService.getMapDupplicate(
                Utils.APP_LANGUAGE,
                user_id,
                "byPGLocation",
                String.valueOf(lat),
                String.valueOf(lng)
        );
    }

    private  Dialog showLoading(Activity activity){
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_loading);
        ImageView image= dialog.findViewById(R.id.loader);
        image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_indefinitely) );
        if(!dialog.isShowing()){
            dialog.show();
        }



        return  dialog;
    }
    private void hideLoading(Dialog dialog){
        dialog.dismiss();
    }


    //    public void addMarkerView(ArrayList<ByLocationModel> arrayList){
    //        mMap.clear();
    //        for (int i = 0; i <arrayList.size() ; i++) {
    //            double lat = Double.parseDouble(arrayList.get(i).getLat());
    //            double lng = Double.parseDouble(arrayList.get(i).getLng());
    //            Marker marker = mMap.addMarker(new MarkerOptions().position(
    //                    new LatLng(lat,
    // lng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.pinmap)));
    //            startDropMarkerAnimation(marker);
    //        }
    //
    //    }

    private void startDropMarkerAnimation(final Marker marker) {
        final LatLng target = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point targetPoint = proj.toScreenLocation(target);
        final long duration = (long) (200 + (targetPoint.y * 0.6));
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.y = 0;
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        handler.post(
                new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed / duration);
                        double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                        double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                        marker.setPosition(new LatLng(lat, lng));
                        if (t < 1.0) {
                            // Post again 16ms later == 60 frames per second
                            handler.postDelayed(this, 16);
                        }
                    }
                });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void updateLocation(final String url, final String latlng, final String addressPost) {
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//        final RequestBody requestBody =
//                new FormBody.Builder()
//                        .add("lat_lng", latlng)
//                        .add("project_address", addressPost)
//                        .build();
//        //        requestBody.toString();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if (string.contains("200")) {
//                    editlatlng = latlng;
//                    nextStep.setVisibility(View.GONE);
//                    edit_localtion_btn.setVisibility(View.VISIBLE);
//                    isEdit = true;
//                    isIsEditClick = false;
//                    dragPin.setVisibility(View.GONE);
//                    if (mMarker != null) {
//                        mMarker.remove();
//                    }
//                    mMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(DragLat, DragLng)));
//
//                    AddNewProjectActivity.DragLat = DragLat;
//                    AddNewProjectActivity.DragLng = DragLng;
//                    AddNewProjectActivity.address = address.getText().toString();
//                    AddNewProjectActivity.step1 = true;
//                    MainActivity.data1 = true;
//
//                    if (AddNewProjectActivity.postProject != null) {
//                        AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
//                    }
//
//                    if (AddNewProjectActivity.fake_layout != null) {
//                        AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
//                    }
//
//                    AddNewProjectActivity.lat_lng = DragLat + "," + DragLng;
//                    AddNewProjectActivity.project_address = address.getText().toString();
//
//                    Toast.makeText(AddCurrentGroupLocationActivity.this,
//                                    R.string.toast_edit_location_success,
//                                    Toast.LENGTH_SHORT).show();
//                }
//            }
//        }.execute();
//    }
}
