package com.scg.rudy.ui.edit_qo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.activity.Models.CommentModel;
import com.scg.rudy.ui.activity.Models.CreditTypeModel;
import com.scg.rudy.ui.by_projects.ProjectTypeSpinner;
import com.scg.rudy.ui.qo_vat.Adapter.QoVatSpinner;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class EditQoDetailActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private String totalItemTxt, totalPrice, qo_id;
    private RelativeLayout closeLayout;
    private ImageView close;
    private Bundle extras;
    private RelativeLayout confirm;
    private EditText edDiscount;
    private ToggleButton unitCount;
    private CheckBox chkVat;
    private EditText edVat;
    private EditText edTran;
    private RelativeLayout openQO;
    private double totalSum;
    private int unit = 1;
    private int tax_type = 1;
    private String vTran = "0", vVat = "0", vDiscount = "0";
    private String typeCustomer = "1";
    private String pId = "";
    private RadioButton vatout, vatin;
    private RadioGroup rd_group;
    private RadioButton creditMoney;
    private RadioButton creditCred;
    private LinearLayout layoutCredit;
    private EditText creditDay;
    private String payment_type = "1";
    private String credit_day = "0";
    private RelativeLayout hideKey;
    private TextView textCurrency;
    private RudyService rudyService;
    private RadioButton vatzero;
    private int defultTaxType;
    private Double delivery_cost;
    private Double discount;
    private RelativeLayout liHideContent;
    private CreditTypeModel ListCreditType;
    private ArrayList<String> creditType;
    private ProjectTypeSpinner customSpinner;
    private Spinner creditTypeSpiner;
    private String credit_type;
    private int catePosition = 0;
    private RelativeLayout reDay;
    private TextView textday;

    private CommentModel commentModel;
    private QoVatSpinner unitCodeTypeSpinner;
    private Spinner commentSpinner;
    private EditText note;

    private String defultDiscount_type;
    private String defultPayment_type;
    private String defultCredit_day;
    private String defultCredit_type;
    private String defultNote;
    private boolean firstLoad = true;
    private CheckBox chkCustomerWalkin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_qo_detail);
        extras = getIntent().getExtras();
        if (extras != null) {
            totalPrice = extras.getString("totalPrice");
            qo_id = extras.getString("qo_id");
            defultTaxType = extras.getInt("tax_type");
            delivery_cost = extras.getDouble("delivery_cost");
            discount = extras.getDouble("discount");
            defultDiscount_type = extras.getString("discount_type");
            defultPayment_type = extras.getString("payment_type");
            defultCredit_day = extras.getString("credit_day");
            defultCredit_type = extras.getString("credit_type");
            defultNote = extras.getString("note");
        }
        rudyService = ApiHelper.getClient();

        initView();
        setOnClick();

        edDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edDiscount.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });


        edVat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edVat.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });

        edTran.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edTran.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });

        rd_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                showToast(EditQoDetailActivity.this,String.valueOf(tax_type));
            }
        });

        vatzero.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 3;
                }
            }
        });

        vatout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 2;
                }
            }
        });
        vatin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 1;
                }
            }
        });

        creditMoney.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    payment_type = "1";
                    liHideContent.setVisibility(View.GONE);
                }
            }
        });

        creditCred.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    liHideContent.setVisibility(View.VISIBLE);
                    payment_type = "2";
                }
            }
        });

        loadCreditType();
        loadComment();

        textCurrency.setText(Utils.currencyForShow);
        unitCount.setTextOn(Utils.currencyForShow);

        if (defultDiscount_type.equalsIgnoreCase("1")) {
            unitCount.setChecked(false);
        } else {
            unitCount.setChecked(true);
        }

        if (defultTaxType == 1) {
            tax_type = 1;
            vatin.setChecked(true);

        } else if (defultTaxType == 2) {
            tax_type = 2;
            vatout.setChecked(true);

        } else {
            tax_type = 3;
            vatzero.setChecked(true);
        }

        if (discount > 0 && discount != null) {
            edDiscount.setText(Double.toString(discount));
        }
        
//        if (delivery_cost > 0 && delivery_cost != null) {
//            edTran.setText(Double.toString(delivery_cost));
//            chkCustomerWalkin.setChecked(false);
//        }else{
//            chkCustomerWalkin.setChecked(true);
//        }


//        if (!defultDiscount_type.equalsIgnoreCase("") && defultDiscount_type != null){
//            if (defultDiscount_type.equalsIgnoreCase("1")){
//                creditMoney.setChecked(true);
//            }else{
//                creditCred.setChecked(true);
//                if (defultPayment_type.equalsIgnoreCase("1")){
//                    creditTypeSpiner.setSelection(1);
//                }else{
//                    creditTypeSpiner.setSelection(0);
//                    creditDay.setText(defultCredit_day);
//                }
//            }
//        }
    }

    private Call<String> callCreditType() {
        return rudyService.getCreditType(
                Utils.APP_LANGUAGE,
                shopID
        );
    }

    private void loadCreditType() {
        showLoading(this);
        callCreditType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ListCreditType = gson.fromJson(resp, CreditTypeModel.class);
                    Collections.reverse(ListCreditType.getItem());
                    creditType = new ArrayList<>();
                    for (int i = 0; i < ListCreditType.getItem().size(); i++) {
                        creditType.add(ListCreditType.getItem().get(i).getName());
                    }

                    customSpinner = new ProjectTypeSpinner(EditQoDetailActivity.this, creditType);
                    creditTypeSpiner.setAdapter(customSpinner);
                    creditTypeSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    creditTypeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            credit_type = ListCreditType.getItem().get(position).getId();
                            if (credit_type.equalsIgnoreCase("3")) {
                                reDay.setVisibility(View.GONE);
                                textday.setVisibility(View.GONE);
                            } else {
                                reDay.setVisibility(View.VISIBLE);
                                textday.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    creditTypeSpiner.setSelection(catePosition);

                    if (defultPayment_type.equalsIgnoreCase("1")) {
                        creditMoney.setChecked(true);
                    } else {
                        creditCred.setChecked(true);
                        if (defultCredit_type.equalsIgnoreCase("1")) {
                            creditTypeSpiner.setSelection(1);
                            creditDay.setText(defultCredit_day);
                            credit_type = "1";
                        } else {
                            creditTypeSpiner.setSelection(0);
                            credit_type = "3";
                        }
                    }


                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(EditQoDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(EditQoDetailActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callComment() {
        return rudyService.getCommnet(
                Utils.APP_LANGUAGE,
                qo_id
        );
    }

    private void loadComment() {
        showLoading(this);
        callComment().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    commentModel = gson.fromJson(resp, CommentModel.class);
                    ArrayList<String> listComment = new ArrayList<>();
                    for (int i = 0; i < commentModel.getItems().size(); i++) {
                        listComment.add(commentModel.getItems().get(i).getName());
                    }


                    unitCodeTypeSpinner = new QoVatSpinner(EditQoDetailActivity.this, listComment);
                    commentSpinner.setAdapter(unitCodeTypeSpinner);
                    commentSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            note.setText(commentModel.getItems().get(position).getTemplateNote());
                            if (firstLoad) {
                                note.setText(defultNote);
                                firstLoad = false;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    commentSpinner.setSelection(catePosition);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(EditQoDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(EditQoDetailActivity.this, t.getMessage());
            }
        });
    }


    private void setOnClick() {
        closeLayout.setOnClickListener(this);
        close.setOnClickListener(this);
        openQO.setOnClickListener(this);
        chkVat.setOnCheckedChangeListener(this);
        unitCount.setOnClickListener(this);
        hideKey.setOnClickListener(this);
        chkCustomerWalkin.setOnCheckedChangeListener(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
        }

    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
    }


    private void initView() {
        closeLayout = findViewById(R.id.close_layout);
        close = findViewById(R.id.close);
        confirm = findViewById(R.id.confirm);
        edDiscount = findViewById(R.id.ed_discount);
        unitCount = findViewById(R.id.unit_count);
        chkVat = findViewById(R.id.chk_vat);
        edVat = findViewById(R.id.ed_vat);
        edTran = findViewById(R.id.ed_tran);
        openQO = findViewById(R.id.submit);
        vatout = findViewById(R.id.vatout);
        vatin = findViewById(R.id.vatin);
        rd_group = findViewById(R.id.rd_group);
        creditMoney = findViewById(R.id.credit_money);
        creditCred = findViewById(R.id.credit_cred);
        layoutCredit = findViewById(R.id.cre_layout);
        creditDay = findViewById(R.id.credit_day);
        hideKey = findViewById(R.id.allView);
        Utils.inputMaxLength(edDiscount, 5);
        Utils.inputMaxLength(edVat, 1);
        Utils.inputMaxLength(edTran, 7);


        textCurrency = (TextView) findViewById(R.id.textCurrency);
        vatzero = (RadioButton) findViewById(R.id.vatzero);
        liHideContent = (RelativeLayout) findViewById(R.id.liHideContent);
        creditTypeSpiner = (Spinner) findViewById(R.id.creditTypeSpiner);
        reDay = (RelativeLayout) findViewById(R.id.reDay);
        textday = (TextView) findViewById(R.id.textday);
        commentSpinner = (Spinner) findViewById(R.id.commentSpinner);
        note = (EditText) findViewById(R.id.note);
        chkCustomerWalkin = (CheckBox) findViewById(R.id.chk_customerWalkin);
    }

    @Override
    public void onClick(View v) {
        if (v == closeLayout) {
            finish();
        }

        if (v == close) {
            finish();
        }
        if (v == hideKey) {
            Utils.hideKeyboardFrom(EditQoDetailActivity.this, hideKey);
        }

        if (v == unitCount) {
            unitCount.setTextOn(Utils.currencyForShow);
            edDiscount.setText("");
            if (unit == 1) {
                Utils.inputMaxLength(edDiscount, 7);
                unit = 2;
            } else {
                Utils.inputMaxLength(edDiscount, 5);
                unit = 1;
            }
            edDiscount.requestFocus();
            Utils.showKeyboardFrom(this, edDiscount);
            checkSum();
        }

        if (v == openQO) {
            UserPOJO userPOJO = Utils.setEnviroment(this);
            Bundle bundle = new Bundle();
            bundle.putString("email", userPOJO.getItems().getEmail());
            bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
            bundle.putString("email", userPOJO.getItems().getEmail());
            bundle.putString("shop", userPOJO.getItems().getShop());
            bundle.putString("total", totalPrice);
//            mFirebaseAnalytics.logEvent("OpenQo", bundle);
            Answers.getInstance().logCustom(new CustomEvent("OpenQo")
                    .putCustomAttribute("email", userPOJO.getItems().getEmail())
                    .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                    .putCustomAttribute("id", userPOJO.getItems().getId())
                    .putCustomAttribute("level", userPOJO.getItems().getLevel())
                    .putCustomAttribute("name", userPOJO.getItems().getName())
                    .putCustomAttribute("shop", userPOJO.getItems().getShop())
                    .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
                    .putCustomAttribute("total", totalPrice)
            );
            reviseQo();
        }
    }

    private Call<String> CallReviseQo() {
        return rudyService.reviseQo(
                Utils.APP_LANGUAGE,
                qo_id,
                vDiscount,
                String.valueOf(unit),
                String.valueOf(tax_type),
                vTran,
                payment_type,
                credit_day,
                payment_type.equalsIgnoreCase("2") ? credit_type : ""
        );
    }

    private Call<String> callCommentQo() {
        return rudyService.commentQo(
                Utils.APP_LANGUAGE,
                qo_id,
                note.getText().toString()
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void reviseQo() {
        showLoading(EditQoDetailActivity.this);
        if (!edTran.getText().toString().equalsIgnoreCase("")) {
            vTran = edTran.getText().toString().replace(",", "");
        }
        if (!edVat.getText().toString().equalsIgnoreCase("")) {
            vVat = edVat.getText().toString();
        }
        if (!edDiscount.getText().toString().equalsIgnoreCase("")) {
            vDiscount = edDiscount.getText().toString();
        }
        if (payment_type.equalsIgnoreCase("2")) {
            if (!credit_type.equalsIgnoreCase("3")) {
                credit_day = creditDay.getText().toString();
            }
        }




        CallReviseQo().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    callCommentQo().enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            hideLoading();
                            Gson gson2 = new Gson();
                            String resp2 = response.body();
                            if (resp2.contains("200")) {
                                showToast(EditQoDetailActivity.this, getResources().getString(R.string.toast_update_data_success));
                                finish();
                            } else {
                                APIError error = gson2.fromJson(resp2, APIError.class);
                                showToast(EditQoDetailActivity.this, error.getItems());
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            hideLoading();
                            showToast(EditQoDetailActivity.this, t.getMessage());
                        }
                    });
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(EditQoDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoDetailActivity.this, t.getMessage());
            }
        });

    }


//    @SuppressLint("StaticFieldLeak")
//    private void reviseQo(final String url) {
//        Log.i("post ", " : " + url);
//        showLoading(this);
//
//        if (!edTran.getText().toString().equalsIgnoreCase("")) {
//            vTran = edTran.getText().toString().replace(",", "");
//        }
//        if (!edVat.getText().toString().equalsIgnoreCase("")) {
//            vVat = edVat.getText().toString();
//        }
//        if (!edDiscount.getText().toString().equalsIgnoreCase("")) {
//            vDiscount = edDiscount.getText().toString();
//        }
//        if (payment_type.equalsIgnoreCase("2")) {
//            credit_day = creditDay.getText().toString();
//        }
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("discount", vDiscount);
//        formData.add("discount_type", String.valueOf(unit));
//        formData.add("tax_type", String.valueOf(tax_type));
//        formData.add("delivery_cost", vTran);
//        formData.add("payment_type", payment_type);
//        formData.add("credit_day", credit_day);
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//
//                    showToast(EditQoDetailActivity.this, getResources().getString(R.string.toast_update_data_success));
//
//                    finish();
//
//                }
//            }
//        }.execute();
//    }


    public void checkSum() {
        double toDb = Double.parseDouble(totalPrice.replace(",", ""));

        if (!edTran.getText().toString().equalsIgnoreCase("")) {
            vTran = edTran.getText().toString().replace(",", "");
        } else {
            vTran = "0";
        }
        if (!edVat.getText().toString().equalsIgnoreCase("")) {
            vVat = edVat.getText().toString();
        } else {
            vVat = "0";
        }
        if (!edDiscount.getText().toString().equalsIgnoreCase("")) {
            vDiscount = edDiscount.getText().toString();
        } else {
            vDiscount = "0";
        }

        Double dtran = Double.parseDouble(vTran);
        Double dvat = Double.parseDouble(vVat);
        Double dDis = Double.parseDouble(vDiscount);


        totalSum = toDb;
        if (unit == 1) {
            totalSum = (toDb + dtran);
            totalSum = totalSum - (totalSum * (dDis / 100));
            totalSum = totalSum + totalSum * (dvat / 100);

        } else {
            totalSum = (toDb + dtran);
            totalSum = totalSum - dDis;
            totalSum = totalSum + totalSum * (dvat / 100);

        }

//        totalAllsum2.setText(Utils.moneyFormat(String.valueOf(totalSum)));

    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
//        if (v == chkDiscount) {
//            if (isChecked) {
//                edDiscount.setEnabled(true);
//                edDiscount.requestFocus();
//                Utils.showKeyboardFrom(this, edDiscount);
//            } else {
//                edDiscount.setText("");
//                edDiscount.setEnabled(false);
//                Utils.hideKeyboardFrom(this, edDiscount);
//            }
//            checkSum();
//        }

        if (v == chkCustomerWalkin){
            if (isChecked){
                edTran.setEnabled(false);
                edTran.setText("");
            }else{
                edTran.setEnabled(true);
                edTran.setText("");
                Utils.hideKeyboardFrom(this, edTran);
            }
            checkSum();
        }

        if (v == chkVat) {
            if (isChecked) {
                edVat.setEnabled(true);
                edVat.requestFocus();
                Utils.showKeyboardFrom(this, edVat);
            } else {
                edVat.setEnabled(false);
                edVat.setText("");

                Utils.hideKeyboardFrom(this, edVat);
            }
            checkSum();
        }

//        if (v == chkTran) {
//            if (isChecked) {
//                edTran.setEnabled(true);
//                edTran.requestFocus();
//                Utils.showKeyboardFrom(this, edTran);
//            } else {
//                edTran.setEnabled(false);
//                edTran.setText("");
//                Utils.hideKeyboardFrom(this, edTran);
//            }
//            checkSum();
//        }

    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
