package com.scg.rudy.ui.qo_vat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.activity.Models.CommentModel;
import com.scg.rudy.ui.activity.Models.CreditTypeModel;
import com.scg.rudy.ui.by_projects.ProjectTypeSpinner;
import com.scg.rudy.ui.print_qo.PrintQoActivity;
import com.scg.rudy.ui.qo_vat.Adapter.QoVatSpinner;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.showToast;

public class QoVatActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private Bundle extras;
    private String totalItemTxt, totalPrice, qo_id;
    private RelativeLayout confirm;
    private ImageView clsBtn;
    private TextView titleName;
    private TextView totalAllsum;
    private TextView totalItem;
    private TextView totalAllsum2;
    private EditText edDiscount;
    private ToggleButton unitCount;
    private CheckBox chkVat;
    private EditText edVat;
    private EditText edTran;
    private RelativeLayout openQO;
    private double totalSum;
    private int unit = 1;
    private int tax_type = 1;
    private RelativeLayout hideKey;
    private String vTran = "0", vVat = "0", vDiscount = "0";
    private String typeCustomer = "1";
    private String pId = "";
    private RadioButton vatout, vatin;
    private RadioGroup rd_group;
    private RadioButton creditMoney;
    private RadioButton creditCred;
    private EditText creditDay;
    private String payment_type = "1";
    private String credit_day = "0";
    private Toolbar toolbar;
    private LinearLayout navbar;
    private RelativeLayout allView;
    private TextView textView15;
    private RelativeLayout addView;
    private TextView textView6;
    private TextView textView22;
    private LinearLayout line;
    private TextView textView16;
    private TextView textView18;
    private RelativeLayout relativeLayout5;
    private CardView dataView;
    private TextView textView17;
    private RadioGroup rdGroup;
    private LinearLayout creLayout;
    private TextView textView;
    private RelativeLayout totalView;
    private LinearLayout linearLayout11;
    private LinearLayout sumView;
    private TextView textView61;
    private TextView textView2;
    private TextView textCurrency;
    private RelativeLayout liHideContent;
    private Spinner creditTypeSpiner;
    private ProjectTypeSpinner customSpinner;
    private int catePosition = 0;
    private ArrayList<String> creditType;
    private RudyService rudyService;
    private CreditTypeModel ListCreditType;
    private String credit_type;

    private Spinner commentSpinner;
    private QoVatSpinner unitCodeTypeSpinner;
    private CommentModel commentModel;
    private EditText note;
    private RadioButton vatzero;
    private RelativeLayout reDay;
    private TextView textday;
    private CheckBox chkCustomerWalkin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qo_vat);
        rudyService = ApiHelper.getClient();
        extras = getIntent().getExtras();
        if (extras != null) {
            totalItemTxt = extras.getString("totalItem");
            totalPrice = extras.getString("totalPrice");
            qo_id = extras.getString("qo_id");
            typeCustomer = extras.getString("typeCustomer");
            pId = extras.getString("pId");
        }
//        totalItemTxt = "3";
//        totalPrice = "100";
        initView();
        setOnclick();
        totalItem.setText(totalItemTxt);
        totalAllsum.setText(totalPrice);
        totalAllsum2.setText(totalPrice);
        edDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edDiscount.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });


        edVat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edVat.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });

        edTran.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                edTran.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkSum();
            }
        });

        rd_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                showToast(QoVatActivity.this,String.valueOf(tax_type));
            }
        });

        vatzero.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 3;
                }
            }
        });

        vatout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 2;
                }
            }
        });
        vatin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tax_type = 1;
                }
            }
        });

        creditMoney.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    liHideContent.setVisibility(View.GONE);
                    payment_type = "1";
                }
            }
        });

        creditCred.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    liHideContent.setVisibility(View.VISIBLE);
                    payment_type = "2";
                }
            }
        });


        unitCount.setTextOn(Utils.currencyForShow);
        textView22.setText(Utils.currencyForShow);
        textView2.setText(Utils.currencyForShow);
        textCurrency.setText(Utils.currencyForShow);

        loadCreditType();
        loadComment();
        autoDefultTax();
    }

    private void autoDefultTax() {
        String json = Utils.getPrefer(getApplicationContext(), PERF_LOGIN);
        UserPOJO userPOJO = UserPOJO.create(json);
        if (userPOJO.getItems().getDefault_tax_type() != null) {
            if (userPOJO.getItems().getDefault_tax_type().equalsIgnoreCase("1")) {
                tax_type = 1;
                vatin.setChecked(true);

            } else if (userPOJO.getItems().getDefault_tax_type().equalsIgnoreCase("2")) {

                tax_type = 2;
                vatout.setChecked(true);

            } else if (userPOJO.getItems().getDefault_tax_type().equalsIgnoreCase("3")) {

                tax_type = 3;
                vatzero.setChecked(true);
            }
        }

    }

    public void setOnclick() {
        clsBtn.setOnClickListener(this);
        openQO.setOnClickListener(this);
        chkVat.setOnCheckedChangeListener(this);
        unitCount.setOnClickListener(this);
        hideKey.setOnClickListener(this);
        chkCustomerWalkin.setOnCheckedChangeListener(this);
    }

    private void initView() {
        confirm = findViewById(R.id.confirm);
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        totalAllsum = findViewById(R.id.total_allsum);
        totalItem = findViewById(R.id.totalItem);
        totalAllsum2 = findViewById(R.id.total_allsum2);
        edDiscount = findViewById(R.id.ed_discount);
        unitCount = findViewById(R.id.unit_count);
        chkVat = findViewById(R.id.chk_vat);
        edVat = findViewById(R.id.ed_vat);
        edTran = findViewById(R.id.ed_tran);
        openQO = findViewById(R.id.openQO);
        vatout = findViewById(R.id.vatout);
        vatin = findViewById(R.id.vatin);
        rd_group = findViewById(R.id.rd_group);

        Utils.inputMaxLength(edDiscount, 5);
        Utils.inputMaxLength(edVat, 1);
        Utils.inputMaxLength(edTran, 7);


        hideKey = findViewById(R.id.allView);
        creditMoney = (RadioButton) findViewById(R.id.credit_money);
        creditCred = (RadioButton) findViewById(R.id.credit_cred);
        creditDay = (EditText) findViewById(R.id.credit_day);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navbar = (LinearLayout) findViewById(R.id.navbar);
        allView = (RelativeLayout) findViewById(R.id.allView);
        textView15 = (TextView) findViewById(R.id.textView15);
        addView = (RelativeLayout) findViewById(R.id.addView);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView22 = (TextView) findViewById(R.id.textView22);
        line = (LinearLayout) findViewById(R.id.line);
        textView16 = (TextView) findViewById(R.id.textView16);
        textView18 = (TextView) findViewById(R.id.textView18);
        relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
        dataView = (CardView) findViewById(R.id.dataView);
        textView17 = (TextView) findViewById(R.id.textView17);
        creLayout = (LinearLayout) findViewById(R.id.cre_layout);
        textView = (TextView) findViewById(R.id.textView);
        totalView = (RelativeLayout) findViewById(R.id.totalView);
        linearLayout11 = (LinearLayout) findViewById(R.id.linearLayout11);
        sumView = (LinearLayout) findViewById(R.id.sumView);
        textView61 = (TextView) findViewById(R.id.textView61);
        textView2 = (TextView) findViewById(R.id.textView2);
        textCurrency = (TextView) findViewById(R.id.textCurrency);
        liHideContent = findViewById(R.id.liHideContent);
        creditTypeSpiner = findViewById(R.id.creditTypeSpiner);
        commentSpinner = findViewById(R.id.commentSpinner);
        note = (EditText) findViewById(R.id.note);
        vatzero = (RadioButton) findViewById(R.id.vatzero);
        reDay = (RelativeLayout) findViewById(R.id.reDay);
        textday = (TextView) findViewById(R.id.textday);
        chkCustomerWalkin = (CheckBox) findViewById(R.id.chk_customerWalkin);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == unitCount) {
            unitCount.setTextOn(Utils.currencyForShow);
            edDiscount.setText("");
            if (unit == 1) {
                Utils.inputMaxLength(edDiscount, 7);
                unit = 2;
            } else {
                Utils.inputMaxLength(edDiscount, 5);
                unit = 1;
            }
            edDiscount.requestFocus();
            Utils.showKeyboardFrom(this, edDiscount);
            checkSum();
        }
        if (v == hideKey) {
            Utils.hideKeyboardFrom(QoVatActivity.this, hideKey);
        }

        if (v == openQO) {
            UserPOJO userPOJO = Utils.setEnviroment(this);
            Bundle bundle = new Bundle();
            bundle.putString("email", userPOJO.getItems().getEmail());
            bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
            bundle.putString("email", userPOJO.getItems().getEmail());
            bundle.putString("shop", userPOJO.getItems().getShop());
            bundle.putString("total", totalPrice);
            mFirebaseAnalytics.logEvent("OpenQo", bundle);

            Answers.getInstance().logCustom(new CustomEvent("OpenQo")
                    .putCustomAttribute("email", userPOJO.getItems().getEmail())
                    .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                    .putCustomAttribute("id", userPOJO.getItems().getId())
                    .putCustomAttribute("level", userPOJO.getItems().getLevel())
                    .putCustomAttribute("name", userPOJO.getItems().getName())
                    .putCustomAttribute("shop", userPOJO.getItems().getShop())
                    .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
                    .putCustomAttribute("total", totalPrice)
            );


            Map<String, Object> props = new HashMap<String, Object>();
            props.put("email", userPOJO.getItems().getEmail());
            props.put("employee_id", userPOJO.getItems().getEmployeeId());
            props.put("shop", userPOJO.getItems().getShop());
            props.put("total", totalPrice);
            props.put("id", userPOJO.getItems().getId());
            props.put("level", userPOJO.getItems().getLevel());
            props.put("name", userPOJO.getItems().getName());
            //Appsee.addEvent("OpenQo", props);

            JSONObject object = new JSONObject();
            try {
                object.put("email", userPOJO.getItems().getEmail());
                object.put("employee_id", userPOJO.getItems().getEmployeeId());
                object.put("shop", userPOJO.getItems().getShop());
                object.put("total", totalPrice);
                object.put("id", userPOJO.getItems().getId());
                object.put("level", userPOJO.getItems().getLevel());
                object.put("name", userPOJO.getItems().getName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mixpanel.track("OpenQo", object);

//            trackPurchaseEvent(Parser.basketModelArrayList(CartActivity.basketString));


            reviseQo(typeCustomer);

        }
    }

    @SuppressLint("StaticFieldLeak")
    private void reviseQo(String typeCustomer) {
        if (!edTran.getText().toString().equalsIgnoreCase("")) {
            vTran = edTran.getText().toString().replace(",", "");
        }
        if (!edVat.getText().toString().equalsIgnoreCase("")) {
            vVat = edVat.getText().toString();
        }
        if (!edDiscount.getText().toString().equalsIgnoreCase("")) {
            vDiscount = edDiscount.getText().toString();
        }
        if (payment_type.equalsIgnoreCase("2")) {
            if (!credit_type.equalsIgnoreCase("3")) {
                credit_day = creditDay.getText().toString();
            }
        }

        getPrToQo(typeCustomer);
    }


    private Call<String> callGetPrToQo(String _typeCustomer) {
        return rudyService.getPrToQo(
                Utils.APP_LANGUAGE,
                qo_id,
                pId,
                _typeCustomer,
                vDiscount,
                String.valueOf(unit),
                String.valueOf(tax_type),
                vTran,
                payment_type,
                credit_day,
                payment_type.equalsIgnoreCase("2") ? credit_type : ""
        );
    }

    private Call<String> callCommentQo() {
        return rudyService.commentQo(
                Utils.APP_LANGUAGE,
                qo_id,
                note.getText().toString()
        );
    }

    private void getPrToQo(String _typeCustomer) {
        showLoading(QoVatActivity.this);
        callGetPrToQo(_typeCustomer).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {


                    callCommentQo().enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            hideLoading();
                            Gson gson2 = new Gson();
                            String resp2 = response.body();
                            if (resp2.contains("200")) {
                                try {

                                    JSONObject rObject = new JSONObject(resp2);
                                    String url = rObject.optString("items");
                                    Utils.savePrefer(QoVatActivity.this, "print", "ok");
                                    showToast(QoVatActivity.this, getResources().getString(R.string.toast_open_po_seccess));
                                    Intent intent = new Intent(QoVatActivity.this, PrintQoActivity.class);
//                                    intent.putExtra("url", "https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/" + qo_id + "?act=qv2");
//                                    intent.putExtra("urlOpen", "https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/" + qo_id + "?act=qv");
                                    intent.putExtra("transaction_id", qo_id);


                                    PrintQoActivity.fromCart = true;
                                    startActivity(intent);
                                    finish();


                                } catch (JSONException eComment) {
                                    eComment.printStackTrace();
                                }
                            } else {
                                APIError error = gson2.fromJson(resp2, APIError.class);
                                showToast(QoVatActivity.this, error.getItems());
                            }

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            hideLoading();
                            showToast(QoVatActivity.this, t.getMessage());
                        }
                    });
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(QoVatActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(QoVatActivity.this, t.getMessage());
            }
        });
    }

    public void checkSum() {
        double toDb = Double.parseDouble(totalPrice.replace(",", ""));

        if (!edTran.getText().toString().equalsIgnoreCase("")) {
            vTran = edTran.getText().toString().replace(",", "");
        } else {
            vTran = "0";
        }
        if (!edVat.getText().toString().equalsIgnoreCase("")) {
            vVat = edVat.getText().toString();
        } else {
            vVat = "0";
        }
        if (!edDiscount.getText().toString().equalsIgnoreCase("")) {
            vDiscount = edDiscount.getText().toString();
        } else {
            vDiscount = "0";
        }

        Double dtran = Double.parseDouble(vTran);
        Double dvat = Double.parseDouble(vVat);
        Double dDis = Double.parseDouble(vDiscount);


        totalSum = toDb;
        if (unit == 1) {
            totalSum = (toDb + dtran);
            totalSum = totalSum - (totalSum * (dDis / 100));
            totalSum = totalSum + totalSum * (dvat / 100);

        } else {
            totalSum = (toDb + dtran);
            totalSum = totalSum - dDis;
            totalSum = totalSum + totalSum * (dvat / 100);

        }

        totalAllsum2.setText(Utils.moneyFormat(String.valueOf(totalSum)));

    }

    @Override
    public void onCheckedChanged(CompoundButton v, boolean isChecked) {


        if (v == chkCustomerWalkin){
            if (isChecked){
                edTran.setEnabled(false);
                edTran.setText("");
            }else{
                edTran.setEnabled(true);
                edTran.setText("");
                Utils.hideKeyboardFrom(this, edTran);
            }
            checkSum();
        }


//        if (v == chkDiscount) {
//            if (isChecked) {
//                edDiscount.setEnabled(true);
//                edDiscount.requestFocus();
//                Utils.showKeyboardFrom(this, edDiscount);
//            } else {
//                edDiscount.setText("");
//                edDiscount.setEnabled(false);
//                Utils.hideKeyboardFrom(this, edDiscount);
//            }
//            checkSum();
//        }

        if (v == chkVat) {
            if (isChecked) {
                edVat.setEnabled(true);
                edVat.requestFocus();
                Utils.showKeyboardFrom(this, edVat);
            } else {
                edVat.setEnabled(false);
                edVat.setText("");

                Utils.hideKeyboardFrom(this, edVat);
            }
            checkSum();
        }

//        if (v == chkTran) {
//            if (isChecked) {
//                edTran.setEnabled(true);
//                edTran.requestFocus();
//                Utils.showKeyboardFrom(this, edTran);
//            } else {
//                edTran.setEnabled(false);
//                edTran.setText("");
//                Utils.hideKeyboardFrom(this, edTran);
//            }
//            checkSum();
//        }
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private Call<String> callCreditType() {
        return rudyService.getCreditType(
                Utils.APP_LANGUAGE,
                shopID
        );
    }

    private void loadCreditType() {
        showLoading(this);
        callCreditType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ListCreditType = gson.fromJson(resp, CreditTypeModel.class);
                    Collections.reverse(ListCreditType.getItem());
                    creditType = new ArrayList<>();
                    for (int i = 0; i < ListCreditType.getItem().size(); i++) {
                        creditType.add(ListCreditType.getItem().get(i).getName());
                    }

                    customSpinner = new ProjectTypeSpinner(QoVatActivity.this, creditType);
                    creditTypeSpiner.setAdapter(customSpinner);
                    creditTypeSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    creditTypeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            credit_type = ListCreditType.getItem().get(position).getId();
                            if (credit_type.equalsIgnoreCase("3")) {
                                reDay.setVisibility(View.GONE);
                                textday.setVisibility(View.GONE);
                            } else {
                                reDay.setVisibility(View.VISIBLE);
                                textday.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    creditTypeSpiner.setSelection(catePosition);

                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(QoVatActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(QoVatActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callComment() {
        return rudyService.getCommnet(
                Utils.APP_LANGUAGE,
                qo_id
        );
    }

    private void loadComment() {
        showLoading(this);
        callComment().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    commentModel = gson.fromJson(resp, CommentModel.class);
                    ArrayList<String> listComment = new ArrayList<>();
                    for (int i = 0; i < commentModel.getItems().size(); i++) {
                        listComment.add(commentModel.getItems().get(i).getName());
                    }


                    unitCodeTypeSpinner = new QoVatSpinner(QoVatActivity.this, listComment);
                    commentSpinner.setAdapter(unitCodeTypeSpinner);
                    commentSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            note.setText(commentModel.getItems().get(position).getTemplateNote());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    commentSpinner.setSelection(catePosition);

                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(QoVatActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(QoVatActivity.this, t.getMessage());
            }
        });
    }
}
