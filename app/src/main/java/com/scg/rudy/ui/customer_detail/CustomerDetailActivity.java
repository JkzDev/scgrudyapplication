package com.scg.rudy.ui.customer_detail;//package com.scg.rudy.ui.by_customers;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.customer_detail.CustomerDetail;
import com.scg.rudy.model.pojo.customer_detail.Items;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.customer_detail.customer_name_pager.CustomerPagerAdapter;
import com.scg.rudy.ui.edit_customer.EditCustomerActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.rudyCamera.CameraActivity;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageActivity;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getDeleteProject;
import static com.scg.rudy.utils.Utils.showToast;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_EXTRA_OPTIONS;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_EXTRA_SOURCE;
import static com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_EXTRA_BUNDLE;


/**
 * @author jackie
 */
public class CustomerDetailActivity extends BaseActivity implements View.OnClickListener, ImagePickerCallback, CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {
    private static final int REQUEST_CAMERA = 0;
    public static final String TAG = "CustomerDetailFragment";
    private ImageView clsBtn;
    private String user_id = "";
    private CircleImageView cusImage;
    private TextView custype;
    private TextView cusId;
    private TextView cusNick;
    private TextView cusName;
    private TextView cusCompany;
    private TextView cusTel;
    private TextView cusLine;
//    private TextView  project_list_nodata;
    private RelativeLayout editCus;
    private String id = "";
    private String customer_id = "";

    private Bundle extra;
    private CustomerDetail customerDetail;
    private RelativeLayout change_image;
    private Bitmap bitmapProfile = null;
    private int SELECT_FILE = 1;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 55125;
    public File mFileTemp;
    public static final int PICK_FROM_CAMERA = 1;
    public static final int CROP_IMAGE = 2;
    public static final int PICK_FROM_FILE = 3;
    private String pickerPath;
    private InputMethodManager imm;
    private Point mSize;
    private ViewPager pager;
    private TabLayout tabs;
    private ArrayList<String> sub_category ;
    private CustomerPagerAdapter customerPagerAdapter;

    private RudyService rudyService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customers_detail);
        rudyService = ApiHelper.getClient();
        ButterKnife.bind(this);
        extra = getIntent().getExtras();
        if(extra!=null){
            id = extra.getString("id");
            customer_id = extra.getString("customer_id");
        }
        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);
        initView();
        getUserData();
        clsBtn.setOnClickListener(this);
        change_image.setOnClickListener(this);
        editCus.setOnClickListener(this);
        setTabView();
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), Utils.TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), Utils.TEMP_PHOTO_FILE_NAME);
        }



    }

    private  void setTabView(){
        sub_category = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.cus_tab_name)));
        customerPagerAdapter = new CustomerPagerAdapter(this,getSupportFragmentManager(), sub_category,customer_id);
        pager.setAdapter(customerPagerAdapter);
        tabs.setupWithViewPager(pager);
        tabs.setTabTextColors(getResources().getColor(R.color.black), getResources().getColor(R.color.white));
        ViewGroup tabsNew = (ViewGroup) tabs.getChildAt(0);
        for (int i = 0; i < tabsNew.getChildCount() - 1; i++) {
            ViewGroup tab = (ViewGroup) tabsNew.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            //            layoutParams.weight = 0;
            layoutParams.setMarginEnd(5);
            layoutParams.setMarginEnd(5);
            //            layoutParams.width = 10;
            tab.setLayoutParams(layoutParams);
            tabs.requestLayout();
        }
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    public void getUserData() {
        try {
            UserPOJO userPOJO = Utils.setEnviroment(this);
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");


            cusCompany.setText(userPOJO.getItems().getShop());
            if(userPOJO.getItems().getShop().isEmpty()){
                cusCompany.setText("-");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        clsBtn = findViewById(R.id.cls_btn);
        cusImage = findViewById(R.id.cus_image);
        custype = findViewById(R.id.custype);
        cusId = findViewById(R.id.cus_id);
        cusNick = findViewById(R.id.cus_nick);
        cusName = findViewById(R.id.cus_name);
        cusCompany = findViewById(R.id.cus_company);
        cusTel = findViewById(R.id.cus_tel);
        cusLine = findViewById(R.id.cus_line);
        editCus = findViewById(R.id.edit_cus);
        change_image = findViewById(R.id.change_image);
        pager = (ViewPager) findViewById(R.id.pager);
        tabs = (TabLayout) findViewById(R.id.tabs);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
           finish();
        }
        if(v==editCus){
            Items customerDetailItem = customerDetail.getItems();

//            Intent intent = new Intent(CustomerDetailActivity.this, EditCustomerActivity.class);
//            intent.putExtra("customerDetail",customerDetail);
//            intent.putExtra("user_id",id);
//            startActivity(intent);
            Intent intent = new Intent(this, EditCustomerActivity.class);

            intent.putExtra("customer_name", customerDetailItem.getCustomerName());
            intent.putExtra("customer_phone", customerDetailItem.getPhone());
            intent.putExtra("customer_note", "");
            intent.putExtra("customer_line", customerDetailItem.getCustomerLine());

            intent.putExtra("projectId", "");
            intent.putExtra("ctype",customerDetailItem.getCustomerType());
            intent.putExtra("ctype_owner", "2");
            intent.putExtra("tax_number", "");
            intent.putExtra("customer_code", customerDetailItem.getCustomerCode());
            intent.putExtra("champ_customer_code", customerDetailItem.getChampCustomerCode());
            intent.putExtra("customer_company", customerDetailItem.getCustomerCompany());


            startActivity(intent);




        }
        if(v==change_image){
            requestForCameraPermission();
        }
    }

    private Call<String> callAddPic(String _pic) {
        return rudyService.addPicCustomer(
                Utils.APP_LANGUAGE,
                user_id,
                customer_id,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void addPic(String _pic, Uri uri) {
        showLoading(CustomerDetailActivity.this);
        callAddPic(_pic).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    cusImage.setImageURI(uri);
                    Toast.makeText(CustomerDetailActivity.this,getResources().getString(R.string.toast_change_profile_picture_success), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(CustomerDetailActivity.this, getResources().getString(R.string.toast_change_profile_picture_not_success), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(CustomerDetailActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void addPic(final String url, final RequestBody formBody, final Uri uri) {
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, formBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    cusImage.setImageURI(uri);
//                    Toast.makeText(CustomerDetailActivity.this,getResources().getString(R.string.toast_change_profile_picture_success), Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(CustomerDetailActivity.this, getResources().getString(R.string.toast_change_profile_picture_not_success), Toast.LENGTH_LONG).show();
//                }
//
//            }
//        }.execute();
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("picker_path", pickerPath);
        super.onSaveInstanceState(outState);
    }



    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

//    @SuppressLint("StaticFieldLeak")
//    private void DeleteProject(final String url) {
//        Log.i("URL", " : " + url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(CustomerDetailActivity.this, getResources().getString(R.string.toast_delete_unit_success));
//                    String url = ApiEndPoint.getByProject(Utils.APP_LANGUAGE, user_id);
////                    addRemoveFavorite(url);
//                }
//            }
//        }.execute();
//    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {

    }

    public void showData(CustomerDetail customer) {
        this.customerDetail = customer;
        Random rand = new Random();
        int n = rand.nextInt(1000000);
        Items items = customerDetail.getItems();
        Glide.with(this)
                .load(items.getPic()+"?"+n)
                .into(cusImage);

        cusNick.setText(items.getCustomerName());
        cusName.setText(items.getCustomerName());
        cusTel.setText(items.getPhone());
        cusLine.setText(items.getCustomerLine());
        cusId.setText(items.getCustomerCode());

        if(items.getCustomerLine().isEmpty()){
            cusLine.setText("-");
        }

    }

    public void showProjectTypeData(ProjectType projectType) {
        getCustomerDetail();

    }

    private Call<CustomerDetail> callCustomerDetail() {
        return rudyService.getCustomerDetail(
                Utils.APP_LANGUAGE,
                id,
                "detail_v2",
                customer_id
        );
    }

    public void getCustomerDetail() {
        showLoading(this);
        callCustomerDetail().enqueue(new Callback<CustomerDetail>() {
            @Override
            public void onResponse(Call<CustomerDetail> call, Response<CustomerDetail> response) {
                hideLoading();
                CustomerDetail detail = response.body();
                showData(detail);
            }

            @Override
            public void onFailure(Call<CustomerDetail> call, Throwable t) {
                hideLoading();
            }
        });
    }

    public void showErrorIsNull() {

    }

    public void BodyError(ResponseBody responseBodyError) {

    }

    public void Failure(Throwable t) {

    }

    @Override
    public void  onResume(){
        super.onResume();
        setTabView();
        getCustomerDetail();
    }

//    public void onClickDeleteProject(String productId) {
//        DeleteProject(getDeleteProject(Utils.APP_LANGUAGE, productId, user_id));
//    }

    private void startCropImageActivity(Uri imageUri) {
        final CropImageOptions mOptions = new  CropImageOptions();
        mOptions.aspectRatioX = 1;
        mOptions.aspectRatioY = 1;
        mOptions.minCropResultHeight = 320;
        mOptions.minCropResultWidth = 320;
        mOptions.fixAspectRatio = true;
        mOptions.cropShape = CropImageView.CropShape.RECTANGLE;
        mOptions.guidelines = CropImageView.Guidelines.ON;
        mOptions.validate();

        Intent intent = new Intent();
        intent.setClass(this, CropImageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(CROP_IMAGE_EXTRA_SOURCE, imageUri);
        bundle.putParcelable(CROP_IMAGE_EXTRA_OPTIONS, mOptions);
        intent.putExtra(CROP_IMAGE_EXTRA_BUNDLE, bundle);
//        intent.putExtra(CROP_IMAGE_EXTRA_SOURCE, imageUri);
//        intent.putExtra(CROP_IMAGE_EXTRA_OPTIONS, mOptions);
        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

//        Intent startCustomCameraIntent = new Intent(getBaseActivity(), CameraActivity.class);
//       CropImage.ActivityBuilder activity =  CropImage.activity(imageUri)
//               .setAspectRatio(1, 1)
//               .setMinCropResultSize(480, 480)
//               .setFixAspectRatio(true)
//               .setCropShape(CropImageView.CropShape.RECTANGLE)
//               .setGuidelines(CropImageView.Guidelines.ON);
//                .start(getBaseActivity());
    }

    public void requestForCameraPermission() {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showPermissionRationaleDialog(getResources().getString(R.string.dialog_pls_allow_to_access_camera), permission);
            } else {
                requestForPermission(permission);

            }
        } else {
//            CropImage.startPickImageActivity(getActivity());
            showDialogSource();
        }
    }


    public void showDialogSource(){
        final Dialog dialog = new Dialog(this,R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_select_photo);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * .7);
        dialog.getWindow().setLayout(width, height);


        RelativeLayout photo_layout = dialog.findViewById(R.id.photo_layout);
        RelativeLayout gall_layout= dialog.findViewById(R.id.gall_layout);
        photo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startCustomCameraIntent = new Intent(CustomerDetailActivity.this, CameraActivity.class);
                startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
                dialog.dismiss();
            }
        });
        gall_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void showPermissionRationaleDialog(final String message, final String permission) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestForPermission(permission);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }

    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_CAMERA);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(this, "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(this, "Image load failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            }
            if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                profileImage.setImageURI(result.getUri());
                Utils.mCropImageUri = result.getUri();
                try {
                    bitmapProfile = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result.getUri());
                    String profileBase64 =  "data:image/png;base64," +Utils.BitMapToStringPNG(bitmapProfile);

                    addPic(profileBase64, result.getUri());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



        }


    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri imageUri = CropImage.getPickImageResultUri(this, data);
        startCropImageActivity(imageUri);
//        Bitmap bm = null;
//        if (data != null) {
//            try {
//                bm = MediaStore.Images.Media.getBitmap(getBaseActivity().getContentResolver(), data.addRemoveFavorite());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private void onCaptureImageResult(Intent data) {
        Uri imageUri = CropImage.getPickImageResultUri(this, data);
        startCropImageActivity(imageUri);
    }


    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {

    }
}
