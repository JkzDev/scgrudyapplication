package com.scg.rudy.ui.by_projects;

import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.projectGroupList.ItemsItem;
import com.scg.rudy.ui.project_detail_group.ProjectDetailGroupActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 * @author jackie
 * @date 9/2/2018 AD
 */

public class ProjectListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private LayoutInflater inflater;
    private ByProjectsListener listener;


    public ProjectListRecyclerAdapter(Context context, ByProjectsListener listener) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = new ArrayList<>();
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.group_project_item, parent, false);
            return new ProjectViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new LoadingVH(viewLoading);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(final @NonNull RecyclerView.ViewHolder vholder, final int position) {
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ItemsItem projectModel = arrayList.get(position);
            final ProjectViewHolder holder = (ProjectViewHolder) vholder;
            holder.name.setText(projectModel.getName());
            holder.projectType.setText(projectModel.getProjectGroupTypeName());
            holder.tel.setVisibility(View.INVISIBLE);
            holder.trans.setText(projectModel.getSumTrans());
            holder.opportunity.setText(projectModel.getSumOpp());
            if(!String.valueOf(projectModel.getStatus()).equalsIgnoreCase("1")){
                holder.workStat.setBackground(context.getDrawable(R.drawable.noti_green));
            }


            holder.unitArea.setText(projectModel.getNumProj() + context.getResources().getString(R.string.units));
            holder.unitBudget.setText(projectModel.getSumUnitBudget() +context.getResources().getString( R.string.million) + Utils.currencyForShow);
            holder.startDate.setText(projectModel.getLastUpdate());


            holder.clickItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        savePrefer(context, "print", "");
                        Intent intent = new Intent(context,ProjectDetailGroupActivity.class);
                        intent.putExtra("projectId", projectModel.getId());
                        intent.putExtra("name", projectModel.getName());
                        context.startActivity(intent);
                    }
                });

        } else if (itemPosition == LOADING) {
            LoadingVH loadingVH = (LoadingVH) vholder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);
                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view, double percent, int maxBarSize) {
        double fn_size = (percent * maxBarSize) / 100;
        if (fn_size == 0) {
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        } else {
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public static class ProjectViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private CardView clickItem;
        private TextView projectType;
        private TextView unitBudget;
        private RelativeLayout tel;
        private ImageView workStat;
        private TextView statusTxt;
        private TextView unitArea;
        private TextView startDate;
        private TextView opportunity;
        private TextView trans;
        private TextView lastPurchase;

        public ProjectViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            clickItem = view.findViewById(R.id.clickItem);
            projectType = view.findViewById(R.id.project_type);
            unitBudget = view.findViewById(R.id.unit_budget);
            tel = view. findViewById(R.id.tel);
            workStat = view.findViewById(R.id.work_stat);
            statusTxt = view.findViewById(R.id.status_txt);
            unitArea = view.findViewById(R.id.unit_area);
            startDate = view.findViewById(R.id.start_date);
            opportunity = view.findViewById(R.id.opportunity);
            trans = view.findViewById(R.id.trans);
            lastPurchase = view.findViewById(R.id.last_purchase);


        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;
        private TextView name;
        private ImageView thumbnail;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
            name = itemView.findViewById(R.id.name);
            thumbnail = itemView.findViewById(R.id.thumbnail);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
        ItemsItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return arrayList.get(position);
    }

    public void add(ItemsItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }

    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }


}