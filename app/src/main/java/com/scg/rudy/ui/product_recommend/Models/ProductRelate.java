
package com.scg.rudy.ui.product_recommend.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ProductRelate {

    @SerializedName("id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("pic")
    private String mPic;
    @SerializedName("sku_code")
    private String mSkuCode;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPic() {
        return mPic;
    }

    public void setPic(String pic) {
        mPic = pic;
    }

    public String getSkuCode() {
        return mSkuCode;
    }

    public void setSkuCode(String skuCode) {
        mSkuCode = skuCode;
    }

}
