package com.scg.rudy.ui.project_detail_group;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.byproject.ItemsItem;
import com.scg.rudy.model.pojo.byproject.Myprojects;
import com.scg.rudy.model.pojo.project_group_detail.Items;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.ui.by_projects.byProjectPaginationScrollListener;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.current_location.AddCurrentLocationActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.department_data_child.DepartmentDataNewActivity;
import com.scg.rudy.ui.department_photo.TakeGroupPhotoActivity;
import com.scg.rudy.ui.department_photo.TakePhotoActivity;
import com.scg.rudy.ui.project_by_group.ProjectByGroupActivity;
import com.scg.rudy.ui.project_detail_group.adapter.ByProjectGroupRecyclerAdapter;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class ProjectDetailGroupActivity extends BaseActivity implements View.OnClickListener, OnRequestPermissionsResultCallback {

    private ImageView clsBtn;
    private Bundle extras;
    private String name;
    private String projectId;
    private TextView phaseId;
    public RelativeLayout edit_department;
    public Items detailItem;
    public String transaction_id = "", group_id = "";
    private RecyclerView rcRecycler, recyclerView;
    private LinearLayout layout_add;
    public static final String TAG = "ProjectDetailActivity";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    String user_id = "", shopID = "";
    private RudyService rudyService;
    private RelativeLayout btnProject;
    private TextView textProject;
    private RelativeLayout btnDoc;
    private TextView textDoc;
    private LinearLayout statusLayout;
    private LinearLayout docLayout;
    private String search = "";
    private String project_type_id = "";
    private String phase_id = "";
    private ByProjectGroupRecyclerAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private RecyclerView qtRecycler;
    private TextView type;
    private ImageView more;
    private TextView projectName;
    private TextView developerName;
    private TextView addedDatetime;
    private TextView unitBudget;
    private TextView status;
    private LinearLayout imageAdd;
    private RelativeLayout editimage;
    private TextView phaseBalance;
    private TextView totalProject;
    private TextView totalGroup;
    private RelativeLayout deal_phase;
    private ArrayList<String> nameList;
    private ArrayList<Bitmap> mArrayList;
    public Bitmap showBitmap;
    private TextView textCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail_group);
        getBundle();
        initView();
        getUserData();
        setOnClick();

        textCurrency.setText(getResources().getString(R.string.million) + Utils.currencyForShow);
    }

    private void getBundle() {
        extras = getIntent().getExtras();
        if (extras != null) {
            projectId = extras.getString("projectId");
            ChooseSkuActivity.projectId = projectId;
            Utils.project_id = projectId;
        } else {
            projectId = "96";
            Utils.project_id = projectId;
            ChooseSkuActivity.projectId = projectId;
        }
    }

    private void setOnClick() {
        clsBtn.setOnClickListener(this);
        edit_department.setOnClickListener(this);
        deal_phase.setOnClickListener(this);
        editimage.setOnClickListener(this);
    }

    private void setAdapter() {
        adapter = new ByProjectGroupRecyclerAdapter(this, null);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new byProjectPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        recyclerView.setNestedScrollingEnabled(false);
    }

    private Call<String> getProjectList() {
        rudyService = ApiHelper.getClient();
        return rudyService.getMyproject(
                Utils.APP_LANGUAGE,
                user_id,
                "list_v2",
                String.valueOf(currentPage),
                search,
                project_type_id,
                phase_id,
                "",
                "",
                ""
        );

    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void loadNextPage() {
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    if (currentPage < TOTAL_PAGES) {
                        adapter.addLoadingFooter();
                    } else {
                        isLastPage = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private List<ItemsItem> fetchResults(Response<Myprojects> response) {
        TOTAL_PAGES = 1;
        Myprojects skuItem = response.body();
        return skuItem.getItems();
    }

    private List<ItemsItem> customFetchResults(Myprojects skuItem) {
        TOTAL_PAGES = 1;
        return skuItem.getItems();
    }


    public void initView() {
        clsBtn = findViewById(R.id.cls_btn);
        edit_department = findViewById(R.id.edit_department);
        phaseId = findViewById(R.id.phase_id);
        rcRecycler = findViewById(R.id.rc_recycler);
        recyclerView = findViewById(R.id.recyclerView);
        layout_add = findViewById(R.id.layout_add);
        btnProject = findViewById(R.id.btn_project);
        textProject = findViewById(R.id.text_project);
        btnDoc = findViewById(R.id.btn_doc);
        textDoc = findViewById(R.id.text_doc);
        btnDoc.setOnClickListener(this);
        btnProject.setOnClickListener(this);
        statusLayout = findViewById(R.id.status_layout);
        docLayout = findViewById(R.id.doc_layout);
        qtRecycler = findViewById(R.id.qt_recycler);
        type = findViewById(R.id.type);
        more = findViewById(R.id.more);
        projectName = findViewById(R.id.projectName);
        developerName = findViewById(R.id.developerName);
        addedDatetime = findViewById(R.id.added_datetime);
        unitBudget = findViewById(R.id.unit_budget);
        status = findViewById(R.id.status);
        imageAdd = findViewById(R.id.image_add);
        editimage = findViewById(R.id.editimage);
        phaseBalance = findViewById(R.id.phase_balance);
        totalProject = findViewById(R.id.total_project);
        totalGroup = findViewById(R.id.total_group);
        deal_phase = findViewById(R.id.deal_phase);
        Utils.savePrefer(this, "call", "");
        AddNewGroupProjectActivity.group_id = "";
        rudyService = ApiHelper.getClient();


        textCurrency = (TextView) findViewById(R.id.textCurrency);
    }

    @Override
    public void onResume() {

        if (Utils.getPrefer(this, "print").length() > 0) {
            finish();
        } else {
            AddCurrentLocationActivity.shopID = "";
            AddCurrentLocationActivity.isEdit = false;
            AddCurrentLocationActivity.editlatlng = "";
            Utils.savePrefer(this, "cart", "");
            DealByPhaseActivity.QUICKSALE = "0";
            loadDetail();
        }

        super.onResume();
    }

    private void loadFirstPage() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("code", "" + response.code());
                Log.e("message", response.message());
                Log.e("response", response.raw().toString());

                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    if (results.size() > 0) {
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    TOTAL_PAGES = 1;
                    currentPage = PAGE_START;
                    isLoading = false;
                    isLastPage = false;
                    setAdapter();
                    showToast(ProjectDetailGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(ProjectDetailGroupActivity.this, t.getMessage());
            }
        });


    }

    public void setData(final Items detailItem) {
        Utils.project_id = detailItem.getId();
        this.detailItem = detailItem;
        group_id = detailItem.getId();
        setAdapter();
        adapter.addAll(detailItem.getProjectList());
        type.setText(detailItem.getProjectGroupListName());
        projectName.setText(detailItem.getName());
        developerName.setText(detailItem.getProjectGroupListName());
        addedDatetime.setText(detailItem.getAddedDatetime());
        unitBudget.setText(getString(R.string.value) + detailItem.getSumUnitBudget() + getString(R.string.million) + Utils.currencyForShow);
        status.setText(detailItem.getStatusTxt());
        phaseBalance.setText(String.valueOf(detailItem.getSumOpp()));
        totalProject.setText(String.valueOf(detailItem.getNumProj()));
        totalGroup.setText(String.valueOf(detailItem.getSumUnits()));
        imageAdd.removeAllViews();
        nameList = new ArrayList<>();
        mArrayList = new ArrayList<>();
        for (int i = 0; i < detailItem.getGallery().size(); i++) {
            final String image = detailItem.getGallery().get(i).replace(ApiEndPoint.replaceImageProjectName, "");
            Glide.with(this).asBitmap().load(detailItem.getGallery().get(i)).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    nameList.add(image);
                    mArrayList.add(resource);
                }
            });
            if (i == 0) {
                Glide.with(Objects.requireNonNull(this)).asBitmap().load(detailItem.getGallery().get(i)).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        showBitmap = resource;
                    }
                });
//                Glide.with(this)
//                        .load(detailItem.getGallery().get(i))
//                        .into(imgProject);

//                imgProject.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onShowPhoto(showBitmap);
//                    }
//                });
            }
            ImageView iv = new ImageView(this);
            iv.setAdjustViewBounds(true);
            Glide.with(this)
                    .load(detailItem.getGallery().get(i))
                    .into(iv);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            params.width = (int) getResources().getDimension(R.dimen._45sdp);
//            params.height = (int) getResources().getDimension(R.dimen._45sdp);
            iv.setLayoutParams(params);
            iv.setAdjustViewBounds(true);
            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Glide.with(ProjectDetailGroupActivity.this).asBitmap().load(detailItem.getGallery().get(finalI)).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            showBitmap = resource;
                        }
                    });
                }
            });

            iv.setPadding(0, 0, 13, 0);
            imageAdd.addView(iv);
        }
    }
//

    public void onShowPhoto(Bitmap bitmap) {
        if (bitmap != null) {
            final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.setContentView(R.layout.view_image);
            dialog.setCancelable(true);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
            dialog.getWindow().setLayout(width, height);
            SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
            Button close = dialog.findViewById(R.id.close);
            RelativeLayout hideView = dialog.findViewById(R.id.hideView);
            image.setImage(ImageSource.bitmap(bitmap));
            image.setMaxScale(20.0f);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            hideView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }


    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }

        if (v == btnProject) {
            btnDoc.setBackgroundResource(R.drawable.bg_project_normal);
            btnProject.setBackgroundResource(R.drawable._bg_save);
            textProject.setTextColor(getResources().getColor(R.color.white));
            textDoc.setTextColor(getResources().getColor(R.color.color_btn_save));
            statusLayout.setVisibility(View.VISIBLE);
            docLayout.setVisibility(View.GONE);
        }

        if (v == deal_phase) {
            Intent intent = new Intent(ProjectDetailGroupActivity.this, ProjectByGroupActivity.class);
            intent.putExtra("group_id", group_id);
            startActivity(intent);

        }

        if (v == btnDoc) {
            btnProject.setBackgroundResource(R.drawable.bg_project_normal);
            btnDoc.setBackgroundResource(R.drawable._bg_save);
            textDoc.setTextColor(getResources().getColor(R.color.white));
            textProject.setTextColor(getResources().getColor(R.color.color_btn_save));
            statusLayout.setVisibility(View.GONE);
            docLayout.setVisibility(View.VISIBLE);
        }

        if (v == edit_department) {
            Intent intent = new Intent(this, DepartmentDataNewActivity.class);
            intent.putExtra("group_id", group_id);
            intent.putExtra("addNew", "add");
            intent.putExtra("projectGroupName", detailItem.getProjectGroupListName());
            intent.putExtra("devName", detailItem.getDeveloperName());
            intent.putExtra("project_group_list_id", detailItem.getProjectGroupListId());
            intent.putExtra("developer_id", detailItem.getProjectGroupListId());
            intent.putExtra("timeStart", detailItem.getStartDate());
            intent.putExtra("timeEnd", detailItem.getEndDate());
            startActivity(intent);
        }

        if (v == editimage) {
            TakeGroupPhotoActivity.mArrayList = mArrayList;
            if (group_id.length() > 0) {
                TakeGroupPhotoActivity.nameList = nameList;
                TakeGroupPhotoActivity.group_id = group_id;
            }
            startActivity(new Intent(this, TakeGroupPhotoActivity.class));
        }

    }

    public boolean isNetworkConnected() {
        return true;
    }


    private Call<String> detailService() {
        return rudyService.projectGroupDetail(
                Utils.APP_LANGUAGE,
                projectId,
                "getPGdetail"
        );
    }

    private void loadDetail() {
        Log.d("", "loadDetail: ");
        showLoading(this);
        detailService().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectGroupDetail projectDetail = gson.fromJson(resp, ProjectGroupDetail.class);
                    setData(projectDetail.getItems());
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(ProjectDetailGroupActivity.this, t.getMessage());
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void reQuestPermissionGPS() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(ProjectDetailGroupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(
                    ProjectDetailGroupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(
                        ProjectDetailGroupActivity.this,
                        "You need to allow access to Contacts",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(
                                        ProjectDetailGroupActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        REQUEST_CODE_ASK_PERMISSIONS);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(
                    ProjectDetailGroupActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
    }

}
