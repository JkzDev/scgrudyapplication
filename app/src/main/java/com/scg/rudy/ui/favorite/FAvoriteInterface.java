package com.scg.rudy.ui.favorite;

/**
 * Created by DekDroidDev on 12/2/2018 AD.
 */

public interface FAvoriteInterface {
    void deleteFAvorite(String productId);
    void deleteClassFavorite(String classId);

}
