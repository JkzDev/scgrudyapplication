package com.scg.rudy.ui.dealbyproject;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.classsku.ClassProjectList;
import com.scg.rudy.model.pojo.dealbyproject.classsku.ItemsItem;
import com.scg.rudy.model.pojo.dealbyproject.other_type.ClassesItem;
import com.scg.rudy.model.pojo.dealbyproject.other_type.SkuItem;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.ui.add_new_project.AddNewProjectActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassDataFactory {

  public static List<ProjectClass> makeClassSku(ClassProjectList projectList) {
    return makeClass(projectList);
  }

  public static List<ProjectClass> makeClassSku(List<ClassesItem> classes) {
    return makeClass(classes);
  }

  public static List<ProjectClass> makeClass(List<ClassesItem> classes) {
    ArrayList<ProjectClass> projectClassArrayList = new ArrayList<>();
    for (int i = 0; i < classes.size(); i++) {
      List<com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem> skuItemArrayList  = new ArrayList<>();
      ClassesItem items = classes.get(i);
      for(int j = 0; j < classes.get(i).getSku().size(); j++) {
        skuItemArrayList = new ArrayList<>();
        SkuItem skuItem = classes.get(i).getSku().get(j);
        skuItemArrayList.add(new com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem(
                skuItem.getPromotionRemainDay(),
                skuItem.getbOQ(),
                skuItem.getClassId(),
                skuItem.getRecommend(),
                skuItem.getPic(),
                skuItem.getHot(),
                skuItem.getIspromotion(),
                skuItem.getUnit(),
                skuItem.getPrice(),
                skuItem.getName(),
                skuItem.getPromotionDetailId(),
                skuItem.getFav(),
                skuItem.getCommission(),
                skuItem.getId(),
                skuItem.getUsed2use(),
                skuItem.getStock(),
                skuItem.getPromotionRemainQty()
            ));
      }
      projectClassArrayList.add( new ProjectClass(
              items.getId(),
              items.getName(),
              String.valueOf(items.getFav()),
              String.valueOf(items.getIspromotion()),
              String.valueOf(items.getSumRank()),
              skuItemArrayList));
    }

    return projectClassArrayList;
  }


  public static List<ProjectClass> makeClass(ClassProjectList projectList) {
    ArrayList<ProjectClass> projectClassArrayList = new ArrayList<>();

    for (int i = 0; i < projectList.getItems().size(); i++) {

      ItemsItem items = projectList.getItems().get(i);
//      if( items.getSku().size()>0){
        projectClassArrayList.add( new ProjectClass(
                items.getId(),
                items.getName(),
                String.valueOf(items.getFav()),
                String.valueOf(items.getIspromotion()),
                String.valueOf(items.getSumRank()),
                items.getSku()));
//      }

    }

    return projectClassArrayList;
  }


}

