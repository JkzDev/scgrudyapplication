package com.scg.rudy.ui.search_project;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.byproject.ItemsItem;
import com.scg.rudy.model.pojo.byproject.Myprojects;
import com.scg.rudy.model.pojo.project_type.PhasesItem;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.ui.by_projects.Adapter.CheckInTelAdapter;
import com.scg.rudy.ui.by_projects.ByProjectRecyclerAdapter;
import com.scg.rudy.ui.by_projects.ByProjectsListener;
import com.scg.rudy.ui.by_projects.Models.Item;
import com.scg.rudy.ui.by_projects.Models.StaffModel;
import com.scg.rudy.ui.by_projects.ProjectTypeSpinner;
import com.scg.rudy.ui.by_projects.byProjectPaginationScrollListener;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class SearchProjectActivity extends BaseActivity implements View.OnClickListener, ByProjectsListener {
    private RudyService rudyService;
    private ImageView clsBtn;
    private String user_id = "";
    private RecyclerView recyclerView;
    private ByProjectRecyclerAdapter adapter;
    private ImageView clear_btn;
    private Myprojects myprojects = null;
    private List<ItemsItem> searchArrayList;
    private LinearLayoutManager linearLayoutManager;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private String class_id = "";
    public static final String TAG = "ByProjectActivity";
    private String search = "";
    private String project_type_id = "";
    private String phase_id = "";
    private RelativeLayout statusProject;
    private RelativeLayout chance;

    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private Spinner typeConstruc;
    private Spinner currentPhase;
    private ProjectTypeSpinner customSpinner;
    private int catePosition = 0;
    private int phasePosition = 0;
    private int staffPosition = 0;
    private int dateFromPosition = 0;
    private int sortPosition = 0;
    private String cateId = "";
    private String phaseId = "";
    private String staffId = "";
    private String dateFromId = "";
    private String sortId = "";
    private int maxPhase = 10;
    private TextView totalProject;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1234;
    private boolean isSearchClick = false;
    private EditText searchText;
    private LayoutInflater inflater;
    private Spinner spinnerStaff;
    private Spinner spinnerDateFrom;
    private RelativeLayout reSort;
    private Spinner spinnerSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_project);
        ButterKnife.bind(this);
        initView();
        getUserData();
        clsBtn.setOnClickListener(this);
        clear_btn.setOnClickListener(this);
        statusProject.setOnClickListener(this);
        chance.setOnClickListener(this);
        project_type_id = "";
        phase_id = "";
        setAdapter();
        TOTAL_PAGES = 1;
        currentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        loadProjectType();
        loadStaffAndDateFrom();
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                search = searchText.getText().toString();
            }
        });

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isSearchClick = true;
                    TOTAL_PAGES = 1;
                    currentPage = PAGE_START;
                    isLoading = false;
                    isLastPage = false;
                    setAdapter();
                    loadFirstPage();


                    return true;
                }
                return false;
            }
        });


    }

    private void setAdapter() {
        adapter = new ByProjectRecyclerAdapter(this, this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new byProjectPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private Call<String> getProjectList() {
        if (project_type_id.equalsIgnoreCase("")) {
            phase_id = "";
        }
        return rudyService.getMyproject(
                Utils.APP_LANGUAGE,
                user_id,
                "list_v2",
                String.valueOf(currentPage),
                search,
                project_type_id,
                phase_id,
                staffId,
                dateFromId,
                sortId
        );
    }

    private Call<String> getProjectType() {
        return rudyService.projectType(
                Utils.APP_LANGUAGE
        );
    }

    private void loadProjectType() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        getProjectType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectType projectType = gson.fromJson(resp, ProjectType.class);
                    setProjectTypeSpinner(projectType);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });


    }

    private void setProjectTypeSpinner(ProjectType projectType) {
//        showMessage(projectType.toString());

        categorieModelArrayList = new ArrayList<>();
        nameList = new ArrayList<>();
        ArrayList<PhasesItem> phases = new ArrayList<PhasesItem>();
        phases.add(new PhasesItem(
                "ทั้งหมด",
                "ทั้งหมด",
                "",
                "ทั้งหมด",
                null));


        projectType.getItems().add(0, new com.scg.rudy.model.pojo.project_type.ItemsItem(
                "",
                "ทั้งหมด",
                phases
        ));

        for (int i = 0; i < projectType.getItems().size(); i++) {
            projectType.getItems().get(i).getPhases().add(0, new PhasesItem(
                    getResources().getString(R.string.all),
                    getResources().getString(R.string.all),
                    "",
                    getResources().getString(R.string.all),
                    null));
            categorieModelArrayList.add(new CategorieModel(projectType.getItems().get(i).getId(), projectType.getItems().get(i).getName()));
            nameList.add(projectType.getItems().get(i).getName());
        }
        customSpinner = new ProjectTypeSpinner(this, nameList);
        typeConstruc.setAdapter(customSpinner);
        typeConstruc.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        typeConstruc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                catePosition = position;
                phasePosition = 0;
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                cateId = categorieModelArrayList.get(position).getId();
                project_type_id = cateId;
                setPhaseSpinner(projectType.getItems().get(position).getPhases());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        typeConstruc.setSelection(catePosition);
    }

    public void setPhaseSpinner(List<PhasesItem> phasesItemList) {
        phaseModelArrayList = new ArrayList<>();
        phaseList = new ArrayList<>();
        for (int i = 0; i < phasesItemList.size(); i++) {
            phaseModelArrayList.add(new CategorieModel(phasesItemList.get(i).getId(), phasesItemList.get(i).getName()));
            phaseList.add(phasesItemList.get(i).getName());
            if (phasesItemList.get(i).getId().equalsIgnoreCase("" + phasePosition)) {
                phasePosition = i;
            }
        }

        ProjectTypeSpinner adapterphase = new ProjectTypeSpinner(this, phaseList);
        maxPhase = phaseList.size();
        if (phaseList.size() > 0) {
            currentPhase.setAdapter(adapterphase);
        } else {
            currentPhase.setAdapter(null);
        }
        currentPhase.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        currentPhase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                phaseId = phaseModelArrayList.get(position).getId();
                phase_id = phaseId;
                phasePosition = position;
                TOTAL_PAGES = 1;
                currentPage = PAGE_START;
                isLoading = false;
                isLastPage = false;
                setAdapter();
                if (isSearchClick) {
                    loadFirstPage();
                }
//
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
//        TOTAL_PAGES = 1;
//        currentPage = PAGE_START;
//        isLoading = false;
//        isLastPage = false;
//        setAdapter();
//        if(isSearchClick){
//            loadFirstPage();
//        }
//
    }

    private ProjectType fetchProjectType(Response<ProjectType> response) {
        ProjectType projectType = response.body();
        return projectType;
    }


    private Call<String> getStaff() {
        return rudyService.getStaff(
                Utils.APP_LANGUAGE,
                user_id
        );
    }


    private Call<String> getDateFrom() {
        return rudyService.getDateFrom(
                Utils.APP_LANGUAGE
        );
    }


    private void loadStaffAndDateFrom() {
        showLoading(this);
        getStaff().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    StaffModel staffModel = gson.fromJson(resp, StaffModel.class);
                    staffModel.getItems().add(0, new Item("", getResources().getString(R.string.all)));
                    ArrayList<String> staffList = new ArrayList<>();
                    for (int i = 0; i < staffModel.getItems().size(); i++) {
                        staffList.add(staffModel.getItems().get(i).getDisplayName());
                    }
                    ProjectTypeSpinner ct = new ProjectTypeSpinner(SearchProjectActivity.this, staffList);
                    spinnerStaff.setAdapter(ct);
                    spinnerStaff.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    spinnerStaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            staffPosition = position;
                            ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                            staffId = staffModel.getItems().get(position).getId();
                            TOTAL_PAGES = 1;
                            currentPage = PAGE_START;
                            isLoading = false;
                            isLastPage = false;
                            setAdapter();
                            if (isSearchClick) {
                                loadFirstPage();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    spinnerStaff.setSelection(staffPosition);


                    getDateFrom().enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            hideLoading();
                            hideErrorView();
                            Gson gson = new Gson();
                            String resp = response.body();
                            if (resp.contains("200")) {


                                StaffModel dateFromModel = gson.fromJson(resp, StaffModel.class);
                                dateFromModel.getItems().add(0, new Item("", getResources().getString(R.string.all)));
                                ArrayList<String> dateFromList = new ArrayList<>();
                                for (int i = 0; i < dateFromModel.getItems().size(); i++) {
                                    dateFromList.add(dateFromModel.getItems().get(i).getDisplayName());
                                }
                                ProjectTypeSpinner ct = new ProjectTypeSpinner(SearchProjectActivity.this, dateFromList);
                                spinnerDateFrom.setAdapter(ct);
                                spinnerDateFrom.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                spinnerDateFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        dateFromPosition = position;
                                        ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                                        dateFromId = dateFromModel.getItems().get(position).getId();
                                        TOTAL_PAGES = 1;
                                        currentPage = PAGE_START;
                                        isLoading = false;
                                        isLastPage = false;
                                        setAdapter();
                                        if (isSearchClick) {
                                            loadFirstPage();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                spinnerDateFrom.setSelection(dateFromPosition);


                                spinnerDateFrom.setSelection(dateFromPosition);


                                ArrayList<Item> sortModel = new ArrayList<>();
                                sortModel.add(new Item("1", getResources().getString(R.string.text_updately)));
                                sortModel.add(new Item("2", getResources().getString(R.string.text_time_visit)));
                                sortModel.add(new Item("3", getResources().getString(R.string.text_creately)));

                                ArrayList<String> sortList = new ArrayList<>();
                                for (int i = 0; i < sortModel.size(); i++) {
                                    sortList.add(sortModel.get(i).getDisplayName());
                                }

                                ProjectTypeSpinner sl = new ProjectTypeSpinner(SearchProjectActivity.this, sortList);
                                spinnerSort.setAdapter(sl);
                                spinnerSort.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        sortPosition = position;
                                        ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                                        sortId = sortModel.get(position).getId();
                                        TOTAL_PAGES = 1;
                                        currentPage = PAGE_START;
                                        isLoading = false;
                                        isLastPage = false;
                                        setAdapter();
                                        if (isSearchClick) {
                                            loadFirstPage();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                spinnerSort.setSelection(sortPosition);



                            } else {
                                APIError error = gson.fromJson(resp, APIError.class);
                                showToast(SearchProjectActivity.this, error.getItems());
                            }

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            APIError error = gson.fromJson(resp, APIError.class);
                            showToast(SearchProjectActivity.this, error.getItems());
                        }
                    });


                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }


    private void loadFirstPage() {
        searchArrayList = new ArrayList<>();
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    if (Integer.parseInt(obj.getTotal()) > 0) {
                        totalProject.setText(obj.getTotal());
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(results, search);
                        addAll(results);
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    } else {
                        totalProject.setText("0");
                        errorLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    totalProject.setText("0");
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });


    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    adapter.addAll(results, search);
                    addAll(results);
                    if (currentPage < TOTAL_PAGES) {
                        adapter.addLoadingFooter();
                    } else {
                        isLastPage = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private List<ItemsItem> fetchResults(Response<Myprojects> response) {
        TOTAL_PAGES = 1;
        Myprojects skuItem = response.body();
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private List<ItemsItem> customFetchResults(Myprojects skuItem) {
        TOTAL_PAGES = 1;
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            totalProject.setText("0");
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_pls_try_again));

        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        if (Utils.getPrefer(this, "print").length() > 0) {
            finish();
        }
        super.onResume();

    }

    public void initView() {

        rudyService = ApiHelper.getClient();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        clsBtn = findViewById(R.id.cls_btn);
        clear_btn = findViewById(R.id.clear_btn);
        recyclerView = findViewById(R.id.recyclerView);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt);


        statusProject = (RelativeLayout) findViewById(R.id.status_project);
        chance = (RelativeLayout) findViewById(R.id.chance);
        typeConstruc = (Spinner) findViewById(R.id.type_construc);
        currentPhase = (Spinner) findViewById(R.id.current_phase);
        totalProject = (TextView) findViewById(R.id.total_project);
        searchText = (EditText) findViewById(R.id.search_text);
        spinnerStaff = (Spinner) findViewById(R.id.spinnerStaff);
        spinnerDateFrom = (Spinner) findViewById(R.id.spinnerDateFrom);
        reSort = (RelativeLayout) findViewById(R.id.reSort);
        spinnerSort = (Spinner) findViewById(R.id.spinnerSort);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == clear_btn) {
            searchText.setText("");
            totalProject.setText("0");
            TOTAL_PAGES = 1;
            currentPage = PAGE_START;
            isLoading = false;
            isLastPage = false;
            isSearchClick = false;

            typeConstruc.setSelection(0);
            currentPhase.setSelection(0);
            spinnerStaff.setSelection(0);
            spinnerDateFrom.setSelection(0);
            spinnerSort.setSelection(0);

            setAdapter();

//            if(isEmpty(searchText)){
//                return;
//            }else{
//                showToast(this,searchText.getText().toString());
//            }

        }
    }

//    @SuppressLint("StaticFieldLeak")
//    private void DeleteProject(final String url) {
//        Log.i("URL", " : " + url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//               hideLoading();
//                if (string.contains("200")) {
//                    showToast(SearchProjectActivity.this,getResources().getString(R.string.toast_delete_unit_success));
//
//                    TOTAL_PAGES = 1;
//                    currentPage = PAGE_START;
//                    isLoading = false;
//                    isLastPage = false;
//
//                    loadFirstPage();
//
//                }
//            }
//        }.execute();
//    }

    public void onFragmentDetached(String tag) {

    }


    public void showMessage(String message) {
        showToast(this, message);
    }


    public boolean isNetworkConnected() {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission(String tel) {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();

        if (!addPermission(this, permissionsList, Manifest.permission.CALL_PHONE)) {
            permissionsNeeded.add("Call");
        }
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showMessageOKCancel(SearchProjectActivity.this, message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        callCkeckin(tel);
    }


    private void callCkeckin(String tel) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        callIntent.setData(Uri.parse("tel:" + tel));
        startActivity(callIntent);
    }


    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }

    public void add(ItemsItem r) {
        searchArrayList.add(r);
    }

    @Override
    public void checkInTel(ItemsItem itemList) {//TODO
        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_tel_checkin);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final RecyclerView addViewNameTel = dialog.findViewById(R.id.addViewNameTel);
        CheckInTelAdapter checkInTelAdapter = new CheckInTelAdapter(this, itemList.getCustomerPhoneList(), this, dialog);
        addViewNameTel.setLayoutManager(new LinearLayoutManager(this));
        addViewNameTel.setItemAnimator(new DefaultItemAnimator());
        addViewNameTel.setAdapter(checkInTelAdapter);
        dialog.show();
    }

//    @Override
//    public void checkInTel(ItemsItem itemList) {
//        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        dialog.setContentView(R.layout.popup_tel_checkin);
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.setCancelable(true);
//        LinearLayout addViewNameTel = dialog.findViewById(R.id.addViewNameTel);
//        for (int i = 0; i <itemList.getCustomerPhoneList().size(); i++) {
//            final View view = inflater.inflate(R.layout.popup_item_tel, addViewNameTel, false);
//            final LinearLayout addNumber = view.findViewById(R.id.addNumber);
//            final LinearLayout line = view.findViewById(R.id.line);
//            final CircleImageView cus_image = view.findViewById(R.id.cus_image);
//            final TextView cus_name = view.findViewById(R.id.cus_name);
//            final TextView cus_type = view.findViewById(R.id.cus_type);
//            if(i==itemList.getCustomerPhoneList().size()-1){
//                line.setVisibility(View.GONE);
//            }
//
//            Glide.with(this)
//                    .load(itemList.getCustomerPhoneList().get(i).getPic())
//                    .into(cus_image);
//            cus_name.setText(itemList.getCustomerPhoneList().get(i).getName());
//            cus_type.setText(itemList.getCustomerPhoneList().get(i).getType());
//
//            for (int j = 0; j <itemList.getCustomerPhoneList().get(i).getPhone().size(); j++) {
//                final View viewNumber = inflater.inflate(R.layout.popup_item_tel_number, addNumber, false);
//                final TextView cus_cusnumber = viewNumber.findViewById(R.id.cus_cusnumber);
//                final RelativeLayout tel =  viewNumber.findViewById(R.id.tel);
//
//                cus_cusnumber.setText(itemList.getCustomerPhoneList().get(i).getPhone().get(j));
//                tel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        requestPermission(cus_cusnumber.getText().toString());
//                        dialog.dismiss();
//                    }
//                });
//                addNumber.addView(viewNumber);
//            }
//            addViewNameTel.addView(view);
//        }
//        dialog.show();
//
//
//
//
//
//
//
////
////        AlertDialog.Builder builder = new AlertDialog.Builder(this);
////        builder.setTitle("กรุณาเลือกเบอร์โทร");
////        String[] animals = {"horse", "cow", "camel", "sheep", "goat"};
////
////        builder.setItems(animals, new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                switch (which) {
////                    case 0: // horse
////                    case 1: // cow
////                    case 2: // camel
////                    case 3: // sheep
////                    case 4: // goat
////                }
////            }
////        });
////
////// create and show the alert dialog
////        AlertDialog dialog = builder.create();
////        dialog.show();
//
//
//    }

//
//
//    @Override
//    public void showData(Myprojects myprojects) {
//        this.myprojects = myprojects;
//        adapter = new ByProjectRecyclerAdapter(this,this);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);
//    }


//    @Override
//    public void onClickDeleteProject(String productId) {
//        DeleteProject(getDeleteProject("th",user_id,productId));
//    }


}

