
package com.scg.rudy.ui.product_recommend.Models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Items {

    @SerializedName("class_id")
    private String mClassId;
    @SerializedName("commission")
    private String mCommission;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("fav")
    private int mFav;
    @SerializedName("hot")
    private Long mHot;
    @SerializedName("id")
    private String mId;
    @SerializedName("intro_word")
    private String mIntroWord;
    @SerializedName("ispromotion")
    private Long mIspromotion;
    @SerializedName("link_video")
    private String mLinkVideo;
    @SerializedName("name")
    private String mName;
    @SerializedName("pic")
    private String mPic;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("product_relate")
    private List<ProductRelate> mProductRelate;
    @SerializedName("promotion_detail_id")
    private String mPromotionDetailId;
    @SerializedName("promotion_remain_day")
    private String mPromotionRemainDay;
    @SerializedName("promotion_remain_qty")
    private String mPromotionRemainQty;
    @SerializedName("recommend")
    private String mRecommend;
    @SerializedName("recommend_biz")
    private String mRecommendBiz;
    @SerializedName("recommend_commission")
    private String mRecommendCommission;
    @SerializedName("recommend_detail_id")
    private String mRecommendDetailId;
    @SerializedName("recommend_remain_quota")
    private String mRecommendRemainQuota;
    @SerializedName("sku_code")
    private String mSkuCode;
    @SerializedName("stock")
    private String mStock;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("used2use")
    private Long mUsed2use;
    @SerializedName("BOQ")
    private String bOQ;

    public String getClassId() {
        return mClassId;
    }

    public void setClassId(String classId) {
        mClassId = classId;
    }

    public String getCommission() {
        return mCommission;
    }

    public void setCommission(String commission) {
        mCommission = commission;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getFav() {
        return mFav;
    }

    public void setFav(int fav) {
        mFav = fav;
    }

    public Long getHot() {
        return mHot;
    }

    public void setHot(Long hot) {
        mHot = hot;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getIntroWord() {
        return mIntroWord;
    }

    public void setIntroWord(String introWord) {
        mIntroWord = introWord;
    }

    public Long getIspromotion() {
        return mIspromotion;
    }

    public void setIspromotion(Long ispromotion) {
        mIspromotion = ispromotion;
    }

    public String getLinkVideo() {
        return mLinkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        mLinkVideo = linkVideo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPic() {
        return mPic;
    }

    public void setPic(String pic) {
        mPic = pic;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public List<ProductRelate> getProductRelate() {
        return mProductRelate;
    }

    public void setProductRelate(List<ProductRelate> productRelate) {
        mProductRelate = productRelate;
    }

    public String getPromotionDetailId() {
        return mPromotionDetailId;
    }

    public void setPromotionDetailId(String promotionDetailId) {
        mPromotionDetailId = promotionDetailId;
    }

    public String getPromotionRemainDay() {
        return mPromotionRemainDay;
    }

    public void setPromotionRemainDay(String promotionRemainDay) {
        mPromotionRemainDay = promotionRemainDay;
    }

    public String getPromotionRemainQty() {
        return mPromotionRemainQty;
    }

    public void setPromotionRemainQty(String promotionRemainQty) {
        mPromotionRemainQty = promotionRemainQty;
    }

    public String getRecommend() {
        return mRecommend;
    }

    public void setRecommend(String recommend) {
        mRecommend = recommend;
    }

    public String getRecommendBiz() {
        return mRecommendBiz;
    }

    public void setRecommendBiz(String recommendBiz) {
        mRecommendBiz = recommendBiz;
    }

    public String getRecommendCommission() {
        return mRecommendCommission;
    }

    public void setRecommendCommission(String recommendCommission) {
        mRecommendCommission = recommendCommission;
    }

    public String getRecommendDetailId() {
        return mRecommendDetailId;
    }

    public void setRecommendDetailId(String recommendDetailId) {
        mRecommendDetailId = recommendDetailId;
    }

    public String getRecommendRemainQuota() {
        return mRecommendRemainQuota;
    }

    public void setRecommendRemainQuota(String recommendRemainQuota) {
        mRecommendRemainQuota = recommendRemainQuota;
    }

    public String getSkuCode() {
        return mSkuCode;
    }

    public void setSkuCode(String skuCode) {
        mSkuCode = skuCode;
    }

    public String getStock() {
        return mStock;
    }

    public void setStock(String stock) {
        mStock = stock;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public Long getUsed2use() {
        return mUsed2use;
    }

    public void setUsed2use(Long used2use) {
        mUsed2use = used2use;
    }

    public void setBOQ(String bOQ){
        this.bOQ = bOQ;
    }

    public String getBOQ(){
        return bOQ;
    }

}
