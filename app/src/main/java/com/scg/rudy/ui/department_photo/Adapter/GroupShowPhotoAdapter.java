package com.scg.rudy.ui.department_photo.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scg.rudy.R;
import com.scg.rudy.ui.department_photo.TakePhotoInterface;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;
import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;

import java.util.ArrayList;

public class GroupShowPhotoAdapter extends RecyclerView.Adapter<GroupShowPhotoAdapter.ViewHolder> {

    private ArrayList<GroupGalleryModel> groupGalleryModels;
    private Context context;
    private TakePhotoInterface listener;
    private ShowPhotoAdapter showPhotoAdapter;

    public GroupShowPhotoAdapter(Context _context,  ArrayList<GroupGalleryModel> _groupGalleryModels, TakePhotoInterface _listener){
        this.context = _context;
        this.groupGalleryModels = _groupGalleryModels;
        this.listener = _listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_show_photo_adapter, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GroupGalleryModel result = groupGalleryModels.get(position);
        if (result.getmKey().equalsIgnoreCase("")){
            holder.textName.setText("Galleries");
            ArrayList<String> listImage = new ArrayList<>();
            for (GalleryModel g : result.getmValue()){
                listImage.add(g.getName());
            }
            showPhotoAdapter = new ShowPhotoAdapter(context, listImage, listener, false);
            holder.reShowPhoto.setLayoutManager(new GridLayoutManager(context, 3));
            holder.reShowPhoto.setHasFixedSize(true);
            holder.reShowPhoto.setAdapter(showPhotoAdapter);
        }else{

            String date = result.getmValue().get(0).getUpdDate().split(" ")[0];
            String time = result.getmValue().get(0).getUpdDate().split(" ")[1];

            holder.textName.setText("เข้าเยี่ยม ณ วันที่ " + date + " เวลา " + time );
            ArrayList<String> listImage = new ArrayList<>();
            for (GalleryModel g : result.getmValue()){
                listImage.add(g.getName());
            }
            showPhotoAdapter = new ShowPhotoAdapter(context, listImage, listener, true);
            holder.reShowPhoto.setLayoutManager(new GridLayoutManager(context, 3));
            holder.reShowPhoto.setHasFixedSize(true);
            holder.reShowPhoto.setAdapter(showPhotoAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return groupGalleryModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textName;
        private RecyclerView reShowPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.textName);
            reShowPhoto = itemView.findViewById(R.id.reShowPhoto);

        }
    }
}
