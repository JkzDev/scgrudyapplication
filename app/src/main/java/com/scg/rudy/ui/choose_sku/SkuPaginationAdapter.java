package com.scg.rudy.ui.choose_sku;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.choose_sku.ItemsItem;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.sku_confirm.SkuConfirmActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import static com.scg.rudy.base.BaseActivity.LogException;
import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.getPrefer;

/**
 *
 * @author jackie
 * @date 19/10/16
 */

public class SkuPaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private List<ItemsItem> skuResults;
    private Context context;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private  ChooseSkuInterface  mCallback;
    private String errorMsg;
    private String subClass;
    private String subphase_id = "";
    private String searchText;

    public SkuPaginationAdapter(Context context, ChooseSkuInterface chooseSkuPresenter,String subClass,String subphase_id) {
        this.context = context;
        this.mCallback = chooseSkuPresenter;
        this.subClass = subClass;
        this.subphase_id = subphase_id;
        skuResults = new ArrayList<>();
    }

    public List<ItemsItem> getSku() {
        return skuResults;
    }

    public void setSku(List<ItemsItem> skuResults) {
        this.skuResults = skuResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.promo_recomd_item, parent, false);
            return new SkuVH(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new LoadingVH(viewLoading);

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemsItem items = skuResults.get(position);

        int i = getItemViewType(position);
        if (i == ITEM) {
            final SkuVH skuVH = (SkuVH) holder;
//            final boolean isFav = false;
            if (subphase_id.isEmpty()) {
                skuVH.price2.setText("-");
            } else {
                skuVH.price2.setText( Utils.moneyFormat(items.getBOQ()));
            }
            skuVH.stock.setText(context.getResources().getString(R.string.txt_stock) + Utils.moneyFormat(items.getStock())  +" " +items.getUnit());
//            skuVH.skuName.setText(items.getName());

            // highlight search text
            String fullText = items.getName();
            Spannable spannable = new SpannableString(fullText);
            String[] spSearchText = searchText.split(" ");
            for (int j = 0; j < spSearchText.length; j++) {
                if (spSearchText[j] != null && !spSearchText[j].isEmpty()) {
                    int startPos = fullText.toLowerCase(Locale.US).indexOf(spSearchText[j].toLowerCase(Locale.US));
                    int endPos = startPos + spSearchText[j].length();

                    if (startPos != -1) {
                        ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.RED});
                        TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                        spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannable.setSpan(new BackgroundColorSpan(Color.YELLOW), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        skuVH.skuName.setText(spannable);
                    } else {
                        skuVH.skuName.setText(fullText);
                    }
                } else {
                    skuVH.skuName.setText(fullText);
                }
            }

            skuVH.price.setText( Utils.moneyFormat(items.getPrice()) + " ฿/" + items.getUnit());
            if (items.getPic() != null) {
                if (!items.getPic().isEmpty()) {
                    Glide.with(context)
                            .load(items.getPic())
                            .into(skuVH.thumbnail);
                }else{
                    Glide.with(context).clear(skuVH.thumbnail);
                    skuVH.thumbnail.setImageDrawable(context.getDrawable(R.drawable.bg_photo_copy));
                }
            }

            if (items.getIspromotion().equalsIgnoreCase("0")){
                skuVH.thumbnail2.setVisibility(View.GONE);
            }

            if(items.getRecommend().equalsIgnoreCase("1")){

                skuVH.recmd_ico.setImageDrawable(context.getDrawable(R.drawable.recm_ico));
            }else{
                skuVH.recmd_ico.setVisibility(View.GONE);
            }


            if (String.valueOf(items.getFav()).equalsIgnoreCase("1")) {
                skuVH.img_addto_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
            } else {
                skuVH.img_addto_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
            }

            //Promotion
            if(String.valueOf(items.getIspromotion()).equalsIgnoreCase("1")){
                skuVH.recmd_ico.setImageDrawable(context.getDrawable(R.drawable.ribbon));
                skuVH.comm_layout.setVisibility(View.VISIBLE);
                skuVH.comm_txt.setText(items.getCommission());
            }else{
                skuVH.recmd_ico.setVisibility(View.GONE);
                skuVH.comm_layout.setVisibility(View.GONE);
            }
//          HOT
            if(String.valueOf(items.getHot()).equalsIgnoreCase("1")){
                skuVH.hotDeal.setVisibility(View.VISIBLE);
            }else{
                skuVH.hotDeal.setVisibility(View.GONE);
            }
//            USE2USE
            if(String.valueOf(items.getUsed2use()).equalsIgnoreCase("1")){
                skuVH.favorite.setVisibility(View.VISIBLE);
            }else{
                skuVH.favorite.setVisibility(View.GONE);
            }
//            Recommend
            if(String.valueOf(items.getRecommend()).equalsIgnoreCase("1")){
                skuVH.recomend.setVisibility(View.VISIBLE);
            }else{
                skuVH.recomend.setVisibility(View.GONE);
            }
//
            skuVH.img_addto_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (items.getFav() == 0) {
                        items.setFav(1);
                        skuVH.img_addto_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
                        mCallback.addFAV(items.getId());
                        notifyDataSetChanged();
                    } else {
                        items.setFav(0);
                        skuVH.img_addto_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
                        mCallback.removeFAV(items.getId());
                        notifyDataSetChanged();

                    }

                }
            });
//
            skuVH.addCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SkuConfirmActivity.class);
                    intent.putExtra("skuId", items.getId());
                    if (subphase_id.isEmpty()) {
                        intent.putExtra("total", "0");
                    } else {
                        intent.putExtra("total", items.getBOQ());
                    }
                    intent.putExtra("price", items.getPrice());
                    intent.putExtra("name", items.getName());
                    intent.putExtra("unit_code", items.getUnit());
                    intent.putExtra("subClass", subClass);
                    intent.putExtra("stock", items.getStock().replace(",",""));
                    ChooseSkuActivity.addItem = 0;
                    context.startActivity(intent);
                }
            });

            if(items.getIspromotion().equalsIgnoreCase("1")){
                skuVH.remain.setVisibility(View.VISIBLE);
                skuVH.remain_price.setText(context.getResources().getString(R.string.promotion_left) + items.getPromotionRemainQty()+(items.getUnit().replace(" ","").replace("\n","")));
                skuVH.remain_date.setText(items.getPromotionRemainDay()+context.getResources().getString( R.string.day));
//                skuVH.promo_ico.setVisibility(View.VISIBLE);
            }else{
                skuVH.remain.setVisibility(View.INVISIBLE);
//                skuVH.promo_ico.setVisibility(View.GONE);
            }

            skuVH.info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onClickinfo(items);
                }
            });

            skuVH.rowNumber.setText(Integer.toString(position + 1));


        } else if (i == LOADING) {
            LoadingVH loadingVH = (LoadingVH) holder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);
                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));
            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public int getItemCount() {
        return skuResults == null ? 0 : skuResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == skuResults.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }

    public void add(ItemsItem r) {
        skuResults.add(r);
        notifyItemInserted(skuResults.size() - 1);
    }

    public void addAll(List<ItemsItem> moveResults, String _searchText) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
        this.searchText = _searchText;
    }

    public void remove(ItemsItem r) {
        int position = skuResults.indexOf(r);
        if (position > -1) {
            skuResults.remove(position);
            notifyItemRemoved(position);
        }
    }

//    public void setAppSeeTag(String event,ItemsItem skuItems){
//        UserPOJO userPOJO =   UserPOJO.create(getPrefer(context,PERF_LOGIN));
//        Map<String, Object> props = new HashMap<String, Object>();
//        props.put("email", userPOJO.getItems().getEmail());
//        props.put("employee_id", userPOJO.getItems().getEmployeeId());
//        props.put("id", userPOJO.getItems().getId());
//        props.put("level", userPOJO.getItems().getLevel());
//        props.put("name", userPOJO.getItems().getName());
//        props.put("shop", userPOJO.getItems().getShop());
//        props.put("shop_id",userPOJO.getItems().getShopId());
//        props.put("ItemName",skuItems.getName());
//        props.put("ItemId",skuItems.getId());
//        props.put("Date", new Date());
//        //Appsee.addEvent(event, props);
//
//
//        JSONObject object = new JSONObject();
//        try {
//            object.put("email", userPOJO.getItems().getEmail());
//            object.put("employee_id", userPOJO.getItems().getEmployeeId());
//            object.put("id", userPOJO.getItems().getId());
//            object.put("level", userPOJO.getItems().getLevel());
//            object.put("name", userPOJO.getItems().getName());
//            object.put("shop", userPOJO.getItems().getShop());
//            object.put("shop_id",userPOJO.getItems().getShopId());
//            object.put("ItemName",skuItems.getName());
//            object.put("ItemId",skuItems.getId());
//            object.put("Date", new Date());
//        } catch (JSONException e) {
//            e.printStackTrace();
//            LogException("getUserData" ,e);
//        }
//        MixpanelAPI.getInstance(context, "18e0073190f10041c91bb5eda7e379fb").track(event, object);
//
//
//    }
    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = skuResults.size() - 1;
        ItemsItem result = getItem(position);

        if (result != null) {
            skuResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return skuResults.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(skuResults.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    protected class SkuVH extends RecyclerView.ViewHolder {
        private TextView skuName;
        private RelativeLayout recomend;
        private RelativeLayout hotDeal;
        private RelativeLayout favorite ;
        private TextView price ;
        private RelativeLayout addCart ;
        private RelativeLayout addto_fav;
        private ImageView img_addto_fav ;
        private TextView price2 ;
        private ImageView thumbnail;
        private ImageView thumbnail2;
        private RelativeLayout comm_layout;
        private TextView comm_txt;
        private TextView stock;
        private LinearLayout remain;
        private TextView remain_price;
        private TextView remain_date;
        private RelativeLayout info;
        private ImageView  recmd_ico;
        private TextView rowNumber;

        SkuVH(View view) {
            super(view);
            skuName = view. findViewById(R.id.skuName);
            recomend = view. findViewById(R.id.recomend);
            hotDeal = view. findViewById(R.id.hot_deal);
            favorite = view. findViewById(R.id.favorite);
            price = view. findViewById(R.id.price);
            addCart = view. findViewById(R.id.add_cart);
            addto_fav= view. findViewById(R.id.addto_fav);
            img_addto_fav = view. findViewById(R.id.add_fav);
            price2 = view. findViewById(R.id.price2);
            thumbnail = view. findViewById(R.id.thumbnail);
            thumbnail2 = view. findViewById(R.id.thumbnail2);
            comm_layout = view. findViewById(R.id.comm_layout);
            comm_txt = view. findViewById(R.id.comm_txt);
            stock = view. findViewById(R.id.stock);
            remain = view.findViewById(R.id.remain);
            remain_price = view.findViewById(R.id.remain_price);
            remain_date = view.findViewById(R.id.remain_date);
            info = view.findViewById(R.id.info);
            recmd_ico = view.findViewById(R.id.recmd_ico);
            rowNumber = view.findViewById(R.id.rowNumber);

        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
                mCallback.retryPageLoad();
            }
        }
    }

}
