package com.scg.rudy.ui.search_project;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.byproject.ItemsItem;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.utils.Utils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class SearchByProjectRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;


    public SearchByProjectRecyclerAdapter(Context context, List<ItemsItem> _arrayList) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = _arrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(context).inflate(R.layout.project_item, parent, false);
//        final ProjectViewHolder vh = new ProjectViewHolder(itemView);
//        return vh;
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.project_item, parent, false);
            return new SearchByProjectRecyclerAdapter.ProjectViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new SearchByProjectRecyclerAdapter.LoadingVH(viewLoading);
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vholder, int position) {
        final ItemsItem items = arrayList.get(position);
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ProjectViewHolder holder = (ProjectViewHolder) vholder;
            final ItemsItem projectModel = arrayList.get(position);
            final String[] createDate = projectModel.getAddedDatetime().split(" ");
            holder.startDate.setText(projectModel.getLastUpdateTxt());
            final String path = projectModel.getPic();
            if(path.replace("","").equalsIgnoreCase("")){
                Glide.with(context).clear(holder.icon_project);
                holder.icon_project.setImageDrawable(context.getDrawable(R.drawable.bg_tower));
            }else{
                Glide.with(context)
                        .load(path)
                        .into(holder.icon_project);
            }

            holder.name.setText(projectModel.getProjectName());
            holder.addDate.setText(context.getResources().getString(R.string.add_on) + createDate[0]);
            holder.unitArea.setText(projectModel.getUnitArea() + context.getResources().getString(R.string.cm2));
            holder.unitBudget.setText(projectModel.getUnitBudget() + context.getResources().getString(R.string.million) + Utils.currencyForShow);

            if(!projectModel.getStatus().equalsIgnoreCase("1")){
                holder.work_stat.setBackground(context.getDrawable(R.drawable.noti_green));
            }

            holder.work_progress.setText(String.format("%s %%", projectModel.getWorkProgress()));
            final ViewTreeObserver vto = holder.work_progress_bg.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int  maxBarSize = holder.work_progress_bg.getWidth();
                    setBarPercent(holder.work_progress_bar,Double.parseDouble(projectModel.getWorkProgress()),maxBarSize);
                }
            });


            final String rate_aplite[] = projectModel.getRating().split("\\.");
            final int rate_semi = Integer.parseInt(rate_aplite[1]);
            final int rate = Integer.parseInt(rate_aplite[0]);
            if(rate_semi==0){
                if(rate>0){
                    holder.rate_layout.removeAllViews();
                    for (int i = 0; i < 5; i++) {
                        ImageView iv = new ImageView(context);
                        if(i<rate){
                            Glide.with(context)
                                    .load(context.getDrawable(R.drawable.star_full))
                                    .into(iv);
                        }else{
                            Glide.with(context)
                                    .load(context.getDrawable(R.drawable.star_empty))
                                    .into(iv);
                        }
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.width = (int) context.getResources().getDimension(R.dimen._12sdp);
                        params.height = (int) context.getResources().getDimension(R.dimen._12sdp);
                        iv.setLayoutParams(params);
                        iv.setAdjustViewBounds(true);
                        holder.rate_layout.addView(iv);
                    }
                }else{
                    holder.rate_layout.removeAllViews();
                    for (int i = 0; i < 5; i++) {
                        ImageView iv = new ImageView(context);
                        Glide.with(context)
                                .load(context.getDrawable(R.drawable.star_empty))
                                .into(iv);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.width = (int) context.getResources().getDimension(R.dimen._12sdp);
                        params.height = (int) context.getResources().getDimension(R.dimen._12sdp);
                        iv.setLayoutParams(params);
                        iv.setAdjustViewBounds(true);
                        holder.rate_layout.addView(iv);
                    }
                }
            }else{
                if(rate>0){
                    holder.rate_layout.removeAllViews();
                    for (int i = 0; i < 5; i++) {
                        ImageView iv = new ImageView(context);
                        if(i==(rate-1)){
                            Glide.with(context)
                                    .load(context.getDrawable(R.drawable.star_semi))
                                    .into(iv);
                        }else{
                            if(i<rate){
                                Glide.with(context)
                                        .load(context.getDrawable(R.drawable.star_full))
                                        .into(iv);
                            }else{
                                Glide.with(context)
                                        .load(context.getDrawable(R.drawable.star_empty))
                                        .into(iv);
                            }
                        }


                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.width = (int) context.getResources().getDimension(R.dimen._12sdp);
                        params.height = (int) context.getResources().getDimension(R.dimen._12sdp);
                        iv.setLayoutParams(params);
                        iv.setAdjustViewBounds(true);
                        holder.rate_layout.addView(iv);
                    }
                }else{
                    holder.rate_layout.removeAllViews();
                    for (int i = 0; i < 5; i++) {
                        ImageView iv = new ImageView(context);
                        Glide.with(context)
                                .load(context.getDrawable(R.drawable.star_empty))
                                .into(iv);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.width = (int) context.getResources().getDimension(R.dimen._12sdp);
                        params.height = (int) context.getResources().getDimension(R.dimen._12sdp);
                        iv.setLayoutParams(params);
                        iv.setAdjustViewBounds(true);
                        holder.rate_layout.addView(iv);
                    }
                }
            }

            holder.phase_title.setText(projectModel.getRankName());
            holder.opportunity.setText(projectModel.getOpportunity());
            holder.trans.setText(projectModel.getTrans());
            holder.last_purchase.setText(projectModel.getLastPurchase());


            holder.clickItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePrefer(context, "print", "");
                    Intent intent = new Intent(context,ProjectDetailActivity.class);
                    intent.putExtra("projectId",projectModel.getId());
                    intent.putExtra("name", projectModel.getProjectName());
                    context.startActivity(intent);
                }
            });

//            if(listenner==null){
                holder.more_action.setVisibility(View.GONE);
//            }


            holder.more_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final PopupWindow popup = new PopupWindow(context);
                    View layout = LayoutInflater.from(context).inflate(R.layout.popup_delete, null);
                    popup.setContentView(layout);
                    LinearLayout delete = layout.findViewById(R.id.delete);
                    popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                    popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                    popup.setOutsideTouchable(true);
                    popup.setFocusable(true);
                    popup.setBackgroundDrawable(new BitmapDrawable());
                    popup.showAsDropDown(view, 10, -10, Gravity.LEFT);

                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popup.dismiss();
                            MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                                    .title(context.getResources().getString(R.string.dialog_want_to_delete_unit) + projectModel.getProjectName()+" ?")
                                    .positiveText(context.getResources().getString(R.string.agreed))
                                    .negativeText(context.getResources().getString(R.string.cancel))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
//                                            listenner.onClickDeleteProject(projectModel.getId());
                                            dialog.dismiss();
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    });

                            MaterialDialog del_dialog = builder.build();
                            del_dialog.show();
                        }
                    });

                }
            });
        }else if(itemPosition == LOADING){
            LoadingVH loadingVH = (LoadingVH) vholder;

            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view,double percent,int maxBarSize){
        double fn_size = (percent*maxBarSize)/100;
        if(fn_size==0){
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        }else{
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ProjectViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private CardView clickItem;
        private TextView addDate;
        private TextView startDate;
        private TextView unitArea;
        private TextView unitBudget;
        private ImageButton more_action;
        private ImageView icon_project;
        private ImageView work_stat;
        private LinearLayout rate_layout;
        private TextView work_progress;
        private ImageView work_progress_bar;
        private RelativeLayout work_progress_bg;
        private TextView opportunity;
        private TextView trans;
        private TextView last_purchase;
        private TextView phase_title;

        public ProjectViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            clickItem = view.findViewById(R.id.clickItem);
            addDate = view.findViewById(R.id.add_date);
            startDate = view.findViewById(R.id.start_date);
            unitArea = view.findViewById(R.id.unit_area);
            unitBudget = view.findViewById(R.id.unit_budget);
            more_action  = view.findViewById(R.id.more_action);
            icon_project  = view.findViewById(R.id.icon_project);

            work_stat = view.findViewById(R.id.work_stat);
            rate_layout = view.findViewById(R.id.rate_layout);
            work_progress = view.findViewById(R.id.work_progress);
            work_progress_bar = view.findViewById(R.id.work_progress_bar);
            work_progress_bg = view.findViewById(R.id.work_progress_bg);
            opportunity = view.findViewById(R.id.opportunity);
            trans = view.findViewById(R.id.trans);
            last_purchase = view.findViewById(R.id.last_purchase);
            phase_title = view.findViewById(R.id.phase_title);
        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
       ItemsItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return arrayList.get(position);
    }


    public void add(ItemsItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }
    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }


}