package com.scg.rudy.ui.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.audio_note.AudioNote;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class RecordRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AudioNote> audioNoteArrayList;
    private Context context;



    public RecordRecyclerAdapter(Context context, ArrayList<AudioNote> audioNoteArrayList) {
        this.audioNoteArrayList = audioNoteArrayList;
        this.context = context;
//        for (int i = 0; i < qtListModelArrayList.size(); i++) {
//            this.qtListModelArrayList.remove(qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr"));
//        }

    }

    //    @Override
    public int getItemViewType(int position) {
        // here your custom logic to choose the view type
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        String itemViewType = audioNoteArrayList.get(getItemViewType(viewType)).getType();

        if (itemViewType.equalsIgnoreCase("audio")) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_record, parent, false);
            return new AudioViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_note, parent, false);
            return new NoteViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final AudioNote audioNote = audioNoteArrayList.get(position);

        if (audioNote.getType().equalsIgnoreCase("audio")) {
            String dateTxt[] = audioNote.getName().split(" ");


            ((AudioViewHolder) holder).nameAudio.setText(audioNote.getName());
            ((AudioViewHolder) holder).dateAudio.setText(dateTxt[2]);
            ((AudioViewHolder) holder).card_audio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


        } else {
            ((NoteViewHolder) holder).nameNote.setText(audioNote.getFile());
            String dateTxt[] = audioNote.getName().split(" ");
            ((NoteViewHolder) holder).dareNote.setText(dateTxt[0]);
            ((NoteViewHolder) holder).card_note.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final Dialog dialog = new Dialog(context, R.style.full_screen_dialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    dialog.setContentView(R.layout.view_image);
                    int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.9);
                    int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.9);
                    dialog.getWindow().setLayout(width, height);
                    final SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
                    Button close = dialog.findViewById(R.id.close);
                    RelativeLayout hideView = dialog.findViewById(R.id.hideView);



                    Glide.with(Objects.requireNonNull(context)).asBitmap().load(audioNote.getFile()).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            image.setImage(ImageSource.bitmap(resource));
                            image.setMaxScale(20.0f);
                        }
                    });




                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    hideView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (audioNoteArrayList == null)
            return 0;
        return audioNoteArrayList.size();
    }




    public class NoteViewHolder extends RecyclerView.ViewHolder {
        private TextView nameNote;
        private TextView dareNote;
        private CardView card_note;

        public NoteViewHolder(View view) {
            super(view);

            nameNote =view.findViewById(R.id.name_note);
            dareNote = view.findViewById(R.id.dare_note);
            card_note= view.findViewById(R.id.card_note);

        }
    }

    public class AudioViewHolder extends RecyclerView.ViewHolder {
        private TextView nameAudio;
        private TextView timeAudio;
        private TextView dateAudio;
        private RelativeLayout playAudio;
        private ImageView icoAudio;
        private CardView card_audio;


        public AudioViewHolder(View view) {
            super(view);
            nameAudio = view.findViewById(R.id.name_audio);
            timeAudio = view.findViewById(R.id.time_audio);
            dateAudio = view.findViewById(R.id.date_audio);
            playAudio = view.findViewById(R.id.play_audio);
            icoAudio = view.findViewById(R.id.ico_audio);
            card_audio = view.findViewById(R.id.card_audio);

        }
    }


}