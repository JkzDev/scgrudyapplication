package com.scg.rudy.ui.product_recommend;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.product_recommend.Adapter.RelatedProductsAdapter;
import com.scg.rudy.ui.product_recommend.Models.Items;
import com.scg.rudy.ui.product_recommend.Models.ReccomendBizDetail;
import com.scg.rudy.ui.product_recommend.Models.RelatedProductsModel;
import com.scg.rudy.ui.sku_confirm.SkuConfirmActivity;
import com.scg.rudy.utils.DialogUtils;
import com.scg.rudy.utils.Utils;
import com.smarteist.autoimageslider.SliderLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

public class ProductRecommendNewActivity extends YouTubeFailureRecoveryActivity implements View.OnClickListener {

    private SliderLayout sliderLayout;
    private String product_id = "";
    private String subClass = "";
    private String phase_id = "";
    private Dialog progressDialog;
    private RudyService rudyService;
    private String videoID = "";
    private UserPOJO userPOJO = null;
    private String shopID = "";


    private TextView skuName;
    private RelativeLayout recomend;
    private RelativeLayout hotDeal;
    private TextView price ;
    private RelativeLayout addCart ;
    private RelativeLayout favorite ;
    private RelativeLayout addto_fav;
    private ImageView img_addto_fav ;
    private TextView price2 ;
    private ImageView thumbnail;
    private RelativeLayout comm_layout;
    private TextView comm_txt;
    private TextView stock;
    private LinearLayout remain;
    private TextView remain_price;
    private TextView remain_date;
    private CardView promotionItem;
    private ImageView recmd_ico;
    private RelativeLayout info;
    private ImageView cls_btn;
    private TextView DescHead;
    private TextView DescDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_recommend_new);

        rudyService = ApiHelper.getClient();

        userPOJO = Utils.setEnviroment(this);
        shopID = userPOJO.getItems().getShopId();


        Bundle extra = getIntent().getExtras();
        if (extra == null) {
            product_id = null;
        } else {
            product_id = extra.getString("product_id");
            subClass = extra.getString("subClass");
            phase_id = extra.getString("phase_id");
        }

        initView();
        setOnClick();
        loadReccomendBizDetail();
    }

    public void initView() {
        promotionItem = findViewById(R.id.promotionItem);
        skuName = findViewById(R.id.skuName);
        recomend = findViewById(R.id.recomend);
        hotDeal = findViewById(R.id.hot_deal);
        price = findViewById(R.id.price);
        addCart = findViewById(R.id.add_cart);
        favorite = findViewById(R.id.favorite);
        addto_fav= findViewById(R.id.addto_fav);
        img_addto_fav = findViewById(R.id.add_fav);
        price2 = findViewById(R.id.price2);
        thumbnail = findViewById(R.id.thumbnail);
        comm_layout = findViewById(R.id.comm_layout);
        comm_txt = findViewById(R.id.comm_txt);
        stock= findViewById(R.id.stock);
        remain = findViewById(R.id.remain);
        remain_price = findViewById(R.id.remain_price);
        remain_date = findViewById(R.id.remain_date);
        recmd_ico = findViewById(R.id.recmd_ico);
        info = findViewById(R.id.info);
        cls_btn = findViewById(R.id.cls_btn);
        DescHead = (TextView) findViewById(R.id.DescHead);
        DescDetail = (TextView) findViewById(R.id.DescDetail);
    }

    public void setOnClick(){
        cls_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == cls_btn){
            finish();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(videoID);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    private void setSliderViews() {

        sliderLayout = findViewById(R.id.imageSlider);
        ArrayList<String> bannerImage = new ArrayList<String>();
        bannerImage.add("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        bannerImage.add("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
        bannerImage.add("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        Utils.setSliderViews(this, sliderLayout, bannerImage);
    }

    private void loadReccomendBizDetail() {
        Log.d("", "loadReccomendBizDetail: ");
        showLoading();
        callReccomendBizDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ReccomendBizDetail reccomendBizDetail = gson.fromJson(resp, ReccomendBizDetail.class);
                    onShowReccomendBizDetail(reccomendBizDetail);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ProductRecommendNewActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(ProductRecommendNewActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callReccomendBizDetail() {
        return rudyService.getReccomendBizDetail(
                Utils.APP_LANGUAGE,
                shopID,
                product_id
        );
    }

    public void showLoading() {
        hideLoading();
        if (progressDialog == null) {
            progressDialog = DialogUtils.showLoadingDialog(this);
        } else {
            ImageView image = progressDialog.findViewById(R.id.loader);
            image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_indefinitely));
            progressDialog.show();
        }
    }

    public void hideLoading() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onShowReccomendBizDetail(ReccomendBizDetail reccomendBizDetail) {
        Items items = reccomendBizDetail.getItems();

//        promotionItem

        skuName.setText(items.getName());

//        recomend
//        hotDeal

        price.setText(Utils.moneyFormat(items.getPrice()) + " ฿/" + items.getUnit());

        addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductRecommendNewActivity.this, SkuConfirmActivity.class);
                intent.putExtra("skuId", items.getId());
                if (phase_id.isEmpty()) {
                    intent.putExtra("total", "0");
                } else {
                    intent.putExtra("total", items.getBOQ());
                }
                intent.putExtra("price", items.getPrice());
                intent.putExtra("name", items.getName());
                intent.putExtra("unit_code", items.getUnit());
                intent.putExtra("stock", items.getStock().replace(",",""));
                intent.putExtra("subClass", subClass);
                ChooseSkuActivity.addItem = 0;
                ProductRecommendNewActivity.this.startActivity(intent);
            }
        });

//        addto_fav

        if (String.valueOf(items.getFav()).equalsIgnoreCase("1")) {
            img_addto_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
        } else {
            img_addto_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
        }
        img_addto_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.getFav() == 0) {
                    items.setFav(1);
                    img_addto_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
                    favorite.setVisibility(View.VISIBLE);
                    addFAV(items.getId());
                } else {
                    items.setFav(0);
                    img_addto_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
                    favorite.setVisibility(View.GONE);
                    removeFAV(items.getId());
                }
            }
        });

//        price2

        DescHead.setText(items.getIntroWord());
        DescDetail.setText(items.getDescription());

        if (items.getPic() != null) {
            if (!items.getPic().isEmpty()) {
                Glide.with(this)
                        .load(items.getPic())
                        .into(thumbnail);
            }else{
                Glide.with(this).clear(thumbnail);
                thumbnail.setImageDrawable(getDrawable(R.drawable.bg_photo_copy));
            }
        }

//        comm_layout
//        comm_txt

        stock.setText(getResources().getString(R.string.txt_stock) + Utils.moneyFormat(items.getStock())  +" " +items.getUnit());

        if(items.getIspromotion()==1){
            remain.setVisibility(View.VISIBLE);
            remain_price.setText(getResources().getString(R.string.promotion_left) + items.getPromotionRemainQty()+(items.getUnit().replace(" ","").replace("\n","")));
            remain_date.setText(items.getPromotionRemainDay()+ getResources().getString(R.string.day));
        }else{
            remain.setVisibility(View.INVISIBLE);
        }

//        remain_price
//        remain_date
//        recmd_ico

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PromotionDetail(items.getPromotionDetailId());
            }
        });



        //related product
        ArrayList<RelatedProductsModel> relatedProductsModels = new ArrayList<>();
        for (int i = 0; i < items.getProductRelate().size(); i++)
        {
            relatedProductsModels.add(new RelatedProductsModel(items.getProductRelate().get(i).getPic(), items.getProductRelate().get(i).getName()));
        }

        RecyclerView recyclerView = findViewById(R.id.recRelatedProducts);
        RelatedProductsAdapter adapter = new RelatedProductsAdapter(this, relatedProductsModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
        //youtube video
        videoID = items.getLinkVideo().split("=")[1];
        YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);

        //product recommend
        setSliderViews();
    }

    public void addFAV(String skuId) {
        apiAddFav(skuId);
    }

    public void removeFAV(String skuId) {
        apiDeleteFav(skuId);
    }

    private Call<String> callAddFav(String _productId) {
        return rudyService.addFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _productId,
                userPOJO.getItems().getId()
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void apiAddFav(String _productId){
        showLoading();
        callAddFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(ProductRecommendNewActivity.this,getResources().getString(R.string.toast_add_producto_to_favorite_success));
                    //                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProductRecommendNewActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProductRecommendNewActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callDeleteFav(String _productId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                product_id,
                _productId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void apiDeleteFav(String _productId){
        showLoading();
        callDeleteFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(ProductRecommendNewActivity.this,getResources().getString(R.string.dialog_delete_finish));
                    //                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProductRecommendNewActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProductRecommendNewActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    public void getData(final String url) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
////                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
//                }
//            }
//        }.execute();
//    }

    private Call<String> callPromotionDetailApi(String promotionDetailId) {
        return rudyService.getPromotionDetail(
                Utils.APP_LANGUAGE,
                shopID,
                promotionDetailId
        );
    }

    private void PromotionDetail(String promotionDetailId) {
        Log.d("", "PromotionDetail: ");
        showLoading();
        callPromotionDetailApi(promotionDetailId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    PromotionDetail promotionDetail  = gson.fromJson(resp, PromotionDetail.class);
                    Utils.onShowPromotion(ProductRecommendNewActivity.this,promotionDetail);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProductRecommendNewActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(ProductRecommendNewActivity.this, t.getMessage());
            }
        });
    }
}
