//package com.scg.rudy.ui.search_project;
//
//import android.graphics.Typeface;
//import android.os.Bundle;
//import androidx.recyclerview.widget.DefaultItemAnimator;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.scg.rudy.R;
//import com.scg.rudy.ui.by_projects.ByProjectRecyclerAdapter;
//import com.scg.rudy.data.remote.ApiEndPoint;
//import com.scg.rudy.injection.component.ActivityComponent;
//import com.scg.rudy.model.pojo.byCustomer.Customer;
//import com.scg.rudy.model.pojo.byproject.ItemsItem;
//import com.scg.rudy.model.pojo.byproject.Myprojects;
//import com.scg.rudy.base.BaseFragment;
//import com.scg.rudy.ui.main.MainActivity;
//import com.scg.rudy.utils.Utils;
//import com.scg.rudy.utils.custom_view.MyDividerItemDecoration;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.inject.Inject;
//
//import butterknife.ButterKnife;
//import okhttp3.ResponseBody;
//
///**
// * Created by DekDroidDev on 24/4/2018 AD.
// */
//public class SearchProjectFragment extends BaseFragment implements View.OnClickListener, SearchProjectMvpView {
//    private ImageView clsBtn;
//    private EditText searchText;
//    private ImageView cancel;
//    private LinearLayout layoutAdd;
//    private TextView nodata;
//    private Bundle extras;
//    public LayoutInflater layoutInflater;
//    public Typeface bold,reg;
//    private ArrayList<ItemsItem> newArrayList;
//    public Myprojects myprojects;
//    public List<ItemsItem> arrayList;
//    private RecyclerView recyclerView;
//    private ByProjectRecyclerAdapter adapter;
//
//    @Inject
//    SearchProjectPresenter<SearchProjectMvpView> searchProjectPresenter;
//    public static final String TAG = "SearchProjectFragment";
//
//    public static SearchProjectFragment newInstance(Myprojects myprojects) {
//        Bundle args = new Bundle();
//        args.putParcelable("myprojects",myprojects);
//        SearchProjectFragment fragment = new SearchProjectFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//    @Override
//    public void onDestroyView() {
//        searchProjectPresenter.detachView();
//        super.onDestroyView();
//    }
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        extras = getArguments();
//        arrayList = new ArrayList<>();
//        if (!extras.isEmpty()) {
////            customer = extras.getParcelable("customer");
////            arrayList =customer.getProjectDetailItems();
//            myprojects = extras.getParcelable("myprojects");
//            arrayList =myprojects.getItems();
//        }
//
//    }
//
//
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_search_project, container, false);
//        ActivityComponent component = getActivityComponent();
//        if (component != null) {
//            component.injectSearchProject(this);
//            setUnBinder(ButterKnife.bind(this, view));
//            searchProjectPresenter.attachView(this);
//        }
//        initView(view);
//
//        newArrayList = new ArrayList<>();
//        clsBtn.setOnClickListener(this);
//        cancel.setOnClickListener(this);
//        getUserData();
//
//
//        searchText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                newArrayList = new ArrayList<>();
//                recyclerView.setAdapter(null);
//                for (int i = 0; i < arrayList.size(); i++) {
//                    if (arrayList.get(i).getProjectName().toString().contains(s)) {
//                        newArrayList.add(arrayList.get(i));
//                    }
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if(newArrayList.size()==0){
//                    nodata.setVisibility(View.VISIBLE);
//                }else{
//                    nodata.setVisibility(View.GONE);
////                    adapter = new ByCustomerRecyclerAdapter(getBaseActivity(),newArrayList);
////                    recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
////                    recyclerView.setItemAnimator(new DefaultItemAnimator());
////                    recyclerView.setAdapter(adapter);
//
////                    adapter = new ByProjectRecyclerAdapter(getBaseActivity(),newArrayList,null);
////        SearchProjectActivity.arrayList = myprojects.getProjectDetailItems();
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 0));
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setAdapter(adapter);
//
//
//
//                }
//            }
//        });
//
//
//
//
//        return view;
//    }
//
//    public void getUserData() {
//        try {
//            JSONObject object = new JSONObject(Utils.getPrefer(getActivity(), Utils.PERF_LOGIN));
//            JSONObject itemObject = object.getJSONObject("items");
//            user_id = itemObject.optString("id");
//            shopID = itemObject.optString("shop_id");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    public void initView(View view){
//        recyclerView = view.findViewById(R.id.recyclerView);
//        clsBtn = view.findViewById(R.id.cls_btn);
//        searchText = view.findViewById(R.id.search_text);
//        cancel = view.findViewById(R.id.cancel);
//        nodata = view.findViewById(R.id.nodata2);
//
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//    }
//
//    @Override
//    public void onResume() {
//        if(Utils.getPrefer(getActivity(),"print").length()>0){
//            MainActivity activ = (MainActivity) getActivity();
//            activ.onFragmentDetached(SearchProjectFragment.TAG);
//        }else{
//            searchProjectPresenter.getCustomerList(ApiEndPoint.LANG,user_id,"list");
//        }
//
//        super.onResume();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        if(v==clsBtn){
//                MainActivity activ = (MainActivity) getActivity();
//                activ.onFragmentDetached(TAG);
//        }
//        if(v==cancel){
//            searchText.setText("");
//            recyclerView.setAdapter(null);
//            nodata.setVisibility(View.VISIBLE);
//            Utils.hideKeyboard(getActivity());
//        }
//
//    }
//
//
//
//    @Override
//    public void showData(Customer customer) {
//
//    }
//
//    @Override
//    public void showErrorIsNull() {
//    }
//
//    @Override
//    public void BodyError(ResponseBody responseBodyError) {
//    }
//
//    @Override
//    public void Failure(Throwable t) {
//    }
//
//
//    @Override
//    protected void setUp(View view) {
//
//    }
//
//
//}
