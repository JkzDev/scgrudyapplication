package com.scg.rudy.ui.adapter.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scg.rudy.R;

import java.util.ArrayList;

public class TransectionDetailUnitCodeSpinner extends BaseAdapter {

    private Context _context;
    private ArrayList<String> itemList;
    private static LayoutInflater inflater = null;
    public TransectionDetailUnitCodeSpinner(Context context, ArrayList<String>  List) {
        _context=context;
        itemList=List;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return  itemList.size();
    }

    @Override
    public String getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView==null){
            holder= new Holder();
            convertView = inflater.inflate(R.layout.transection_detail_unitcode_spinner, null);
            holder.detail  = convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        }else{
            holder = (Holder)convertView.getTag();
        }

        holder.detail.setText(itemList.get(position));
        return convertView;
    }



    public class Holder {
        TextView detail;

        public TextView getDetail() {
            return detail;
        }

        public void setDetail(TextView detail) {
            this.detail = detail;
        }
    }

}
