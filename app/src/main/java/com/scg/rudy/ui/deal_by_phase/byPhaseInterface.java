package com.scg.rudy.ui.deal_by_phase;

import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;

/**
 * Created by DekDroidDev on 12/2/2018 AD.
 */

public interface byPhaseInterface {
    void onSubphaseClick(String subPhaseId);
    void onGroupClick(String groupId);
    void addFAVClass(String classId, boolean add);
    void clickClass(com.scg.rudy.model.pojo.dealbyproject.class_by_phase.ItemsItem classItem);
}
