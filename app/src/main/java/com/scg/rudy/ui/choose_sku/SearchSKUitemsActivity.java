package com.scg.rudy.ui.choose_sku;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.choose_sku.ChooseSku;
import com.scg.rudy.model.pojo.choose_sku.ItemsItem;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.onShowPromotion;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class SearchSKUitemsActivity extends BaseActivity implements View.OnClickListener, ChooseSkuInterface {

    private Bundle extras;
    private String shopId;
    private ImageView clsBtn;
    private EditText searchText;
    private ImageView cancel;
    private String user_id;
    private SkuPaginationAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView rv;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private RudyService rudyService;
    private String subphase_id;

    private LinearLayout mostUse;
    private ImageView icoMostuse;
    private TextView txtMostuse;
    private LinearLayout topSell;
    private ImageView icoTopsale;
    private TextView txtTopsale;
    private LinearLayout recomment;
    private ImageView icoRecc;
    private TextView txtRecc;
    private CardView shortHL;
    private TextView txtHL;
    private String shortBy = ApiEndPoint.SEARCH_NOR;
    private final int shortByUse = 1, shortBySale = 2, shortByRecomd = 3;
    private int shortType = 2;

    private boolean isUse = false, isBest = false, isRecm = false;
    private ArrayList<String> filterList;
    private String multifilter = "";
    private final String USE = "used2use";
    private final String POP = "popularity";
    private final String REC = "recommend";
    private TextView countText;
    private RelativeLayout reCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_skuitems);
        initView();
        setEnviroment();
        rudyService = ApiHelper.getClient();

        extras = getIntent().getExtras();
        if (extras != null) {
            shopId = extras.getString("shopId", "1");
            subphase_id = extras.getString("phase_id", "");
        }

        clsBtn.setOnClickListener(this);
        cancel.setOnClickListener(this);
        shortHL.setOnClickListener(this);
        mostUse.setOnClickListener(this);
        topSell.setOnClickListener(this);
        recomment.setOnClickListener(this);
        adapter = new SkuPaginationAdapter(this, this, "Search", subphase_id);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isLastPage = false;
                    adapter.clear();
                    currentPage = 1;

                    loadFirstPage();
                    Utils.hideKeyboard(SearchSKUitemsActivity.this);
                    return true;
                }
                return false;
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    adapter.clear();
                    currentPage = 1;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        countText.setText("0 " + getResources().getString(R.string.list));
    }


    private void initView() {
        filterList = new ArrayList<>();
        clsBtn = findViewById(R.id.cls_btn);
        searchText = findViewById(R.id.search_text);
        cancel = findViewById(R.id.cancel);
        rv = findViewById(R.id.select_sku_recycler);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);

        mostUse = findViewById(R.id.most_use);
        icoMostuse = findViewById(R.id.ico_mostuse);
        txtMostuse = findViewById(R.id.txt_mostuse);
        topSell = findViewById(R.id.top_sell);
        icoTopsale = findViewById(R.id.ico_topsale);
        txtTopsale = findViewById(R.id.txt_topsale);
        recomment = findViewById(R.id.recomment);
        icoRecc = findViewById(R.id.ico_recc);
        txtRecc = findViewById(R.id.txt_recc);
        shortHL = findViewById(R.id.shortHL);
        txtHL = findViewById(R.id.txtHL);

        countText = (TextView) findViewById(R.id.countText);
        reCount = (RelativeLayout) findViewById(R.id.reCount);
    }

    @Override
    public void setEnviroment() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
    }

    private Call<String> callSearchApi() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        Bundle bundle = new Bundle();
        bundle.putString("email", userPOJO.getItems().getEmail());
        bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
        bundle.putString("id", userPOJO.getItems().getId());
        bundle.putString("level", userPOJO.getItems().getLevel());
        bundle.putString("name", userPOJO.getItems().getName());
        bundle.putString("shop", userPOJO.getItems().getShop());
        bundle.putString("shop_id", userPOJO.getItems().getShopId());
        bundle.putString("search_key", searchText.getText().toString());
        mFirebaseAnalytics.logEvent("SearchSku", bundle);

        return rudyService.SearchSkuList(
                searchText.getText().toString(),
                Utils.APP_LANGUAGE,
                shopId,
                "search_v2",
                Utils.project_id,
                user_id,
                String.valueOf(currentPage),
                shortBy,
                multifilter
        );
//        return null;
    }

    private void loadFirstPage() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callSearchApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ChooseSku obj = gson.fromJson(resp, ChooseSku.class);
                    countText.setText(obj.getTotal() + " " + getResources().getString(R.string.list));
                    List<ItemsItem> results = customFetchResults(obj);
                    if (results.size() > 0) {
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(results, searchText.getText().toString());
                        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        countText.setText("0 " + getResources().getString(R.string.list));
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchSKUitemsActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        callSearchApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ChooseSku obj = gson.fromJson(resp, ChooseSku.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    adapter.addAll(results, searchText.getText().toString());
                    if (currentPage < TOTAL_PAGES) {
                        adapter.addLoadingFooter();
                    } else {
                        isLastPage = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchSKUitemsActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private List<ItemsItem> fetchResults(Response<ChooseSku> response) {
        TOTAL_PAGES = 1;
        ChooseSku skuItem = response.body();
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem != null ? skuItem.getItems() : null;
    }

    private List<ItemsItem> customFetchResults(ChooseSku skuItem) {
        TOTAL_PAGES = 1;
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem != null ? skuItem.getItems() : null;
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Call<String> callAddFavorite(String _skuId) {
        return rudyService.addFAV(
            Utils.APP_LANGUAGE,
                project_id,
                _skuId,
                user_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void addFavorite(String _skuId){
        showLoading(SearchSKUitemsActivity.this);
        callAddFavorite(_skuId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(SearchSKUitemsActivity.this, getResources().getString(R.string.toast_add_producto_to_favorite_success));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(SearchSKUitemsActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(SearchSKUitemsActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callRemoveFavorite(String _skuId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _skuId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void removeFavorite(String _skuId){
        showLoading(SearchSKUitemsActivity.this);
        callRemoveFavorite(_skuId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(SearchSKUitemsActivity.this, getResources().getString(R.string.dialog_delete_finish));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(SearchSKUitemsActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(SearchSKUitemsActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    public void addRemoveFavorite(final String url, final String textShow) {
//        Log.i("addRemoveFavorite", " : " + url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.getData(url);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(SearchSKUitemsActivity.this, textShow);
//                }
//            }
//        }.execute();
//    }

    @Override
    public void onClick(View v) {
        if (v == cancel) {
            isLastPage = false;
            searchText.setText("");
            adapter.clear();
            currentPage = 1;
            Utils.hideKeyboard(this);
            countText.setText("0 " + getResources().getString(R.string.list));
        }

        if (v == clsBtn) {
            finish();
        }

        if (v == mostUse) {
            shortType = shortByUse;
            shortItems(v);
        }

        if (v == topSell) {
            shortType = shortBySale;
            shortItems(v);
        }

        if (v == recomment) {
            shortType = shortByRecomd;
            shortItems(v);
        }

        if (v == shortHL) {
            final PopupWindow popup = new PopupWindow(this);
            View layout = LayoutInflater.from(this).inflate(R.layout.popup_short, null);
            popup.setContentView(layout);
            LinearLayout L2H = layout.findViewById(R.id.LH);
            LinearLayout H2L = layout.findViewById(R.id.HL);
            LinearLayout NM = layout.findViewById(R.id.NM);


            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setOutsideTouchable(true);
            popup.setFocusable(true);
            popup.setBackgroundDrawable(new BitmapDrawable());
            popup.showAsDropDown(shortHL, 10, -10, Gravity.LEFT);


            NM.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortBy = ApiEndPoint.SEARCH_NOR;
                    txtHL.setText(getResources().getString(R.string.txt_nm));
                    popup.dismiss();
                    if (!Utils.isEmpty(searchText)) {
                        isLastPage = false;
                        adapter.clear();
                        currentPage = 1;
                        loadFirstPage();
                    }
                }
            });


            L2H.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortBy = ApiEndPoint.SEARCH_HTL;
                    txtHL.setText(getResources().getString(R.string.txt_hl));
                    popup.dismiss();
                    if (!Utils.isEmpty(searchText)) {
                        isLastPage = false;
                        adapter.clear();
                        currentPage = 1;
                        loadFirstPage();
                    }
                }
            });

            H2L.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortBy = ApiEndPoint.SEARCH_LTH;
                    txtHL.setText(getResources().getString(R.string.txt_lh));
                    popup.dismiss();
                    if (!Utils.isEmpty(searchText)) {
                        isLastPage = false;
                        adapter.clear();
                        currentPage = 1;
                        loadFirstPage();
                    }


                }
            });


        }
    }

    private void shortItems(View v) {
        if (v == mostUse) {
            if (isUse) {
                mostUse.setBackground(getResources().getDrawable(R.drawable.bg_use_stoke));
                icoMostuse.setImageDrawable(getResources().getDrawable(R.drawable.used));
                txtMostuse.setTextColor(getResources().getColor(R.color.short_use));
                filterList.remove(USE);

            } else {
                mostUse.setBackground(getResources().getDrawable(R.drawable.bg_use_fill));
                icoMostuse.setImageDrawable(getResources().getDrawable(R.drawable.used_white));
                txtMostuse.setTextColor(getResources().getColor(R.color.white));
                filterList.add(USE);
            }
            isUse = !isUse;
        }

        if (v == topSell) {
            if (isBest) {
                topSell.setBackground(getResources().getDrawable(R.drawable.bg_sell_stoke));
                icoTopsale.setImageDrawable(getResources().getDrawable(R.drawable.filled_star));
                txtTopsale.setTextColor(getResources().getColor(R.color.short_best));
                filterList.remove(POP);

            } else {
                topSell.setBackground(getResources().getDrawable(R.drawable.bg_sell_fill));
                icoTopsale.setImageDrawable(getResources().getDrawable(R.drawable.filled_star_white));
                txtTopsale.setTextColor(getResources().getColor(R.color.white));
                filterList.add(POP);
            }
            isBest = !isBest;

        }

        if (v == recomment) {
            if (isRecm) {
                recomment.setBackground(getResources().getDrawable(R.drawable.bg_recoment_stoke));
                icoRecc.setImageDrawable(getResources().getDrawable(R.drawable.like_it_blue));
                txtRecc.setTextColor(getResources().getColor(R.color.short_recco));
                filterList.remove(REC);
            } else {
                recomment.setBackground(getResources().getDrawable(R.drawable.bg_recoment_fill));
                icoRecc.setImageDrawable(getResources().getDrawable(R.drawable.like_it_white));
                txtRecc.setTextColor(getResources().getColor(R.color.white));
                filterList.add(REC);
            }
            isRecm = !isRecm;

        }
        multifilter = filterList.toString().replace("[", "").replace("]", "").replace(" ", "");
        if (!Utils.isEmpty(searchText)) {
            isLastPage = false;
            adapter.clear();
            currentPage = 1;
            loadFirstPage();
        }


    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {
        showToast(this, message);

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    @Override
    public void showData(ChooseSku chooseSku) {

    }

    @Override
    public void showErrorIsNull() {

    }

    @Override
    public void BodyError(ResponseBody responseBodyError) {

    }

    @Override
    public void Failure(Throwable t) {

    }

    @Override
    public void retryPageLoad() {

    }

    @Override
    public void addFAV(String skuId) {
        addFavorite(skuId);

    }

    @Override
    public void removeFAV(String skuId) {
        removeFavorite(skuId);
    }

    @Override
    public void onClickinfo(ItemsItem items) {
//        showToast(this,items.getName());
        PromotionDetail(items.getPromotionDetailId());
    }


    private void PromotionDetail(String promotionDetailId) {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callPromotionDetailApi(promotionDetailId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    PromotionDetail promotionDetail = gson.fromJson(resp, PromotionDetail.class);
                    onShowPromotion(SearchSKUitemsActivity.this, promotionDetail);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SearchSKUitemsActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(SearchSKUitemsActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callPromotionDetailApi(String promotionDetailId) {
        return rudyService.getPromotionDetail(
                Utils.APP_LANGUAGE,
                shopID,
                promotionDetailId
        );
    }
}
