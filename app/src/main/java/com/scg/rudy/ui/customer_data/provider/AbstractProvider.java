/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.customer_data.provider;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.services.network.HttpRequest;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static com.scg.rudy.base.BaseActivity.LogException;

/**
 * Abstract class for common methods of data provider.
 */
public abstract class AbstractProvider {

    private static final String URF_8 = "UTF-8";
    private static final String EMPTY_STRING = "";

    /**
     * Build valid API URL enriched with query.
     *
     * @param baseUrl base
     * @param searchQuery
     * @return
     */
    public String buildQueryUrl(final String baseUrl, String searchQuery) {
        try {
            searchQuery = URLEncoder.encode(searchQuery.toLowerCase(Locale.getDefault()), URF_8);
            URL url = new URI(String.format(baseUrl, searchQuery)).toURL();

            return url.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return EMPTY_STRING;
    }

    /**
     * Make an HTTP request for query.
     *
     * @param url of API service.
     * @return result of HTTP query.
     */
    public String invokeRestQuery(String url,String searchQuery) {
        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        RequestBody requestBody = new FormBody.Builder()
                .add("search", searchQuery)
                .build();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        try {
            String str = client.newCall(request).execute().body().string();
            JSONObject object = new JSONObject(str);
            if(object.optInt("status")==200){
                return object.optString("items");
            }else{
                return EMPTY_STRING;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LogException(EMPTY_STRING ,e);
            return EMPTY_STRING;
        } catch (JSONException e) {
            e.printStackTrace();
            LogException(EMPTY_STRING ,e);
            return EMPTY_STRING;
        }
    }
}
