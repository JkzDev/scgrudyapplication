package com.scg.rudy.ui.dealbyproject;

import android.content.Context;
import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem;

import java.util.List;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class ProjectGroupRecyclerAdapter extends RecyclerView.Adapter<ProjectGroupRecyclerAdapter.ProjectViewHolder> {

    private List<ItemsItem> itemList ;
    private Context context;
    private int selected_position = 0;
    private byProjectInterface listener;

    public ProjectGroupRecyclerAdapter(Context context, List<ItemsItem> itemList,byProjectInterface listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.project_group_item, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        final ItemsItem groupItem = itemList.get(position);
//        holder.itemView.setBackgroundColor(selected_position == position ? Color.GREEN : Color.TRANSPARENT);
        holder.productName.setText(groupItem.getName());
        if(!groupItem.getImg().isEmpty()){
            Glide.with(context)
                    .load(groupItem.getImg())
                    .into(holder.imageItem);
        }

        holder.productBg.setBackground(selected_position == position ? context.getDrawable(R.drawable.bg_frame_group_select) :context.getDrawable(R.drawable.bg_project_normal));
        holder.productName.setTextColor(selected_position == position ? Color.WHITE : Color.BLACK);
        holder.selectIco.setVisibility(selected_position == position ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        }
        return itemList.size();
    }



    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout productBg;
        private ImageView selectIco;
        private RelativeLayout comment;
        private TextView productName;
        private ImageView imageItem;
        public ProjectViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            productBg = v.findViewById(R.id.product_bg);
            selectIco = v.findViewById(R.id.select_ico);
            comment = v.findViewById(R.id.comment);
            productName = v.findViewById(R.id.product_name);
            imageItem = v.findViewById(R.id.image_item);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) {
                return;
            }
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            listener.onClickCateItem(itemList.get(selected_position).getId());
        }
    }


}