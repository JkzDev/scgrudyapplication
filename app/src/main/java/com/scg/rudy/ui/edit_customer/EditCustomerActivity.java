package com.scg.rudy.ui.edit_customer;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.ui.customer_data.ExtendedDictionaryAutocompleteAdapter;
import com.scg.rudy.ui.customer_data.customerInterface;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutoCompleteTextView;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteListener;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class EditCustomerActivity extends BaseActivity implements View.OnClickListener, OnDynamicAutocompleteListener, customerInterface {
    private int selectTab = 1;
    private String cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
  //  private String ccompany = "";// บริษัทลูกค้า
    private String cphone = "";// เบอร์ติดต่อลูกค้า
    private String cline = "";// line ID ของลูกค้า
    private String cnote = "";// noteลูกค้า
    private String cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    private String ccompany_owner = "";// บริษัทลูกค้า
    private String cphone_owner = "";// เบอร์ติดต่อลูกค้า
    private String cline_owner = "";// line ID ของลูกค้า
    private String cnote_owner = "";// noteลูกค้า
    private String customer_code = "";
    private String champ_customer_code = "";
    private String customer_company = "";

    private int customerTypePosition = 0;
    private int ownerTypePosition = 0;
    private int ctype = 1;
    private int ctype_owner = 2;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
    private ImageView clsBtn;
    private Spinner typeCustomer;
    private Spinner ownerCustomer;
    private EditText lineid;
    private EditText note;
    private LinearLayout key_hide;
    private RelativeLayout save;
    private EditText nameCompany_owner;
    private EditText lineid_owner;
    private EditText note_owner;
    private RelativeLayout btn_cus;
    private RelativeLayout btn_owner;
    private TextView text_cus;
    private TextView text_owner;
    private LinearLayout typ_cus;
    private LinearLayout owner_cus;
    private RelativeLayout clear;
    private Bundle extras;
    public static  String projectId = "";
    private static final int THRESHOLD = 1;
    private DynamicAutoCompleteTextView name;
    private DynamicAutoCompleteTextView tel;
    private DynamicAutoCompleteTextView name_owner;
    private DynamicAutoCompleteTextView tel_owner;
    private ProgressBar mDictionaryAutoCompleteProgressBar;
    private ProgressBar telAutoCompleteProgressBar;
    private ProgressBar owner_nameProgressBar;
    private ProgressBar owner_telProgressBar;
    private ExtendedDictionaryAutocompleteAdapter nameAdapter;
    private ExtendedDictionaryAutocompleteAdapter telAdapter;
    private ExtendedDictionaryAutocompleteAdapter owner_nameAdapter;
    private ExtendedDictionaryAutocompleteAdapter owner_telAdapter;

    private int cusFirstSelect = 0;
    private int ownFirstSelect = 0;
    private EditText taxNo;
    private String cTaxNo = "",project_id="";
    private TextView champCode;
    private TextView champHead;
    private LinearLayout companyLayout;
    private EditText customerCompany;
    private RudyService rudyService;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_customer);
        initView();
        rudyService = ApiHelper.getClient();
        selectTab = 1;
        extras = getIntent().getExtras();
        clsBtn.setOnClickListener(this);
        clear.setOnClickListener(this);
        if (extras != null) {
            cname =  extras.getString("customer_name");
            cphone =  extras.getString("customer_phone");
            cline =  extras.getString("customer_line");
            cnote =  extras.getString("customer_note");
//            cname_owner =  extras.getString("project_owner_name");
//            cphone_owner =  extras.getString("project_owner_phone");
//            cline_owner =  extras.getString("project_owner_line");
//            cnote_owner =  extras.getString("project_owner_note");
            project_id =  extras.getString("projectId");

            if(extras.getString("ctype","1").contains(",")){
                ctype =  1;
            }else{
                ctype =  Integer.parseInt(extras.getString("ctype","1"));
            }

//            ctype_owner =  Integer.parseInt(extras.getString("ctype_owner","2"));
            cTaxNo =  extras.getString("tax_number");
            customer_code  =  extras.getString("customer_code");
            champ_customer_code  =  extras.getString("champ_customer_code");
            customer_company =  extras.getString("customer_company");

        }

        nameAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,1);
        telAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,1);

        owner_nameAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,2);
        owner_telAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,2);

//        cTaxNo
        name.setOnDynamicAutocompleteListener(this);
        name.setAdapter(nameAdapter);
        name.setThreshold(THRESHOLD);

        tel.setOnDynamicAutocompleteListener(this);
        tel.setAdapter(telAdapter);
        tel.setThreshold(THRESHOLD);


        if(ctype == 1){
            companyLayout.setVisibility(View.GONE);
        }else{
            companyLayout.setVisibility(View.VISIBLE);
        }

        name_owner.setOnDynamicAutocompleteListener(this);
        name_owner.setAdapter(owner_nameAdapter);
        name_owner.setThreshold(THRESHOLD);

        tel_owner.setOnDynamicAutocompleteListener(this);
        tel_owner.setAdapter(owner_telAdapter);
        tel_owner.setThreshold(THRESHOLD);

        mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        owner_nameProgressBar.setVisibility(View.INVISIBLE);
        owner_telProgressBar.setVisibility(View.INVISIBLE);

        if (cTaxNo.length() > 0) {
            taxNo.setText(cTaxNo);
        }

        if (cname.length() > 0) {
            name.setText(cname);
        }
        if (customer_company.length() > 0) {
            customerCompany.setText(customer_company);
        }
        if (cphone.length() > 0) {
            tel.setText(cphone);
        }
        if (cline.length() > 0) {
            lineid.setText(cline);
        }
        if (cnote.length() > 0) {
            note.setText(cnote);
        }
        if (cname_owner.length() > 0) {
            name_owner.setText(cname_owner);
        }
        if (ccompany_owner.length() > 0) {
            nameCompany_owner.setText(ccompany_owner);
        }
        if (cphone_owner.length() > 0) {
            tel_owner.setText(cphone_owner);
        }
        if (cline_owner.length() > 0) {
            lineid_owner.setText(cline_owner);
        }
        if (cnote_owner.length() > 0) {
            note_owner.setText(cnote_owner);
        }
        if(champ_customer_code.isEmpty()){
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
        }else{
            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(champ_customer_code);
        }



        if (cname.length() > 0 && cphone.length() > 0) {
            save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
        }else{
            save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }

        key_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(EditCustomerActivity.this, key_hide);
            }
        });


        customerCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customer_company = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                customerCompany.setTypeface(_font);
            }
        });

        lineid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cline = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                lineid.setTypeface(_font);
            }
        });


        taxNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cTaxNo = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                taxNo.setTypeface(_font);
            }
        });


        note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cnote = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                note.setTypeface(_font);
            }
        });

        nameCompany_owner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ccompany_owner = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                nameCompany_owner.setTypeface(_font);
            }
        });

        lineid_owner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cline_owner = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                lineid_owner.setTypeface(_font);
            }
        });

        note_owner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cnote_owner = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                note_owner.setTypeface(_font);
            }
        });

        btn_cus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTab = 1;
                btn_cus.setBackground(getDrawable(R.drawable.rounder_cus_left_select));
                btn_owner.setBackground(getDrawable(R.drawable.rounder_cus_right_unselect));
                text_cus.setTextColor(getResources().getColor(R.color.black));
                text_owner.setTextColor(getResources().getColor(R.color.white));
                typ_cus.setVisibility(View.VISIBLE);
                owner_cus.setVisibility(View.GONE);
                checkData(selectTab);

            }
        });

        btn_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTab = 2;
                btn_cus.setBackground(getDrawable(R.drawable.rounder_cus_left_unselect));
                btn_owner.setBackground(getDrawable(R.drawable.rounder_cus_right_select));

                text_cus.setTextColor(getResources().getColor(R.color.white));
                text_owner.setTextColor(getResources().getColor(R.color.black));
                typ_cus.setVisibility(View.GONE);
                owner_cus.setVisibility(View.VISIBLE);
                checkData(selectTab);
            }
        });

        CustomSpinner newUserTypeAdapter = new CustomSpinner(EditCustomerActivity.this, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.type_cus))));
        CustomSpinner owner_cusAdapter = new CustomSpinner(EditCustomerActivity.this, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.owner_cus))));

        typeCustomer.setAdapter(newUserTypeAdapter);
        ownerCustomer.setAdapter(owner_cusAdapter);
        typeCustomer.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        ownerCustomer.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        typeCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);

                if(cusFirstSelect>0){
                    ctype = position + 1;
                    customerTypePosition = position;
                }

                if(ctype == 1){
                    companyLayout.setVisibility(View.GONE);
                }else{
                    companyLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        ownerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);

                if(ownFirstSelect>0){
                    ctype_owner = position + 2;
                    ownerTypePosition = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(ctype==1){
            customerTypePosition = 0;
        }else{
            customerTypePosition =1;
        }

        if(ctype_owner==2){
            ownerTypePosition = 0;
        }else{
            ownerTypePosition = 1;
        }

        if(cusFirstSelect==0){
            typeCustomer.setSelection(customerTypePosition);
            cusFirstSelect++;
        }

        if(ownFirstSelect==0){
            ownerCustomer.setSelection(ownerTypePosition);
            ownFirstSelect++;
        }






        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cname = name.getText().toString();
//                ccompany = nameCompany.getText().toString();
                cphone = tel.getText().toString();
                cline = lineid.getText().toString();
                cnote = note.getText().toString();
                cname_owner = name_owner.getText().toString();
//                ccompany_owner = nameCompany_owner.getText().toString();
                cphone_owner = tel_owner.getText().toString();
                cline_owner = lineid_owner.getText().toString();
                cnote_owner = note_owner.getText().toString();
                cTaxNo = taxNo.getText().toString();

                if(ctype==1){
                    customer_company = "";
                }else{
                    customer_company = customerCompany.getText().toString();
                }

                    if (/*cname.length() > 0 && */cphone.length() > 0) {
                        updateCustomer();
                    } else {
                        Toast.makeText(EditCustomerActivity.this, R.string.toast_pls_fill_name_and_phone_contractor, Toast.LENGTH_LONG).show();
                    }




            }
        });



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if(v==clear){
            if(selectTab==1){
                name.setText("");
                customerCompany.setText("");
                tel.setText("");
                taxNo.setText("");
                lineid.setText("");
                note.setText("");
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }else{
                name_owner.setText("");
                nameCompany_owner.setText("");
                tel_owner.setText("");
                lineid_owner.setText("");
                note_owner.setText("");
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }

        }
    }

    public void checkData(int tab){
        if(tab==1){
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }

        }else{
            if (/*cname_owner.length() > 0 && */cphone_owner.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }
//            save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
//            save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }
    }


    private void initView() {
        clear = findViewById(R.id.clear);
        typeCustomer = findViewById(R.id.typeCustomer);
        ownerCustomer = findViewById(R.id.ownerCustomer);
        name =  findViewById(R.id.name);

        mDictionaryAutoCompleteProgressBar =  findViewById(R.id.dictionaryAutoCompleteProgressBar);
        telAutoCompleteProgressBar  = findViewById(R.id.telAutoCompleteProgressBar);
        owner_nameProgressBar   = findViewById(R.id.owner_nameProgressBar);
        owner_telProgressBar  = findViewById(R.id.owner_telProgressBar);


        tel = findViewById(R.id.tel);
        lineid = findViewById(R.id.lineid);
        note = findViewById(R.id.note);
        key_hide = findViewById(R.id.key_hide);
        save = findViewById(R.id.save);
        name_owner = findViewById(R.id.owner_name);
        nameCompany_owner = findViewById(R.id.owner_name_company);
        tel_owner = findViewById(R.id.owner_tel);
        lineid_owner = findViewById(R.id.owner_lineid);
        note_owner = findViewById(R.id.owner_note);
        btn_cus = findViewById(R.id.btn_cus);
        btn_owner = findViewById(R.id.btn_owner);
        text_cus = findViewById(R.id.text_cus);
        text_owner = findViewById(R.id.text_owner);
        typ_cus = findViewById(R.id.typ_cus);
        owner_cus = findViewById(R.id.owner_cus);
        clsBtn = findViewById(R.id.cls_btn);
        taxNo = findViewById(R.id.tax_no);
        champCode = findViewById(R.id.champCode);
        champHead = findViewById(R.id.champHead);
        companyLayout = (LinearLayout) findViewById(R.id.companyLayout);
        customerCompany = (EditText) findViewById(R.id.customer_company);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private Call<String> callUpdateCustomer() {
        return rudyService.updateCustomer(
                Utils.APP_LANGUAGE,
                user_id,
                cname.isEmpty() ? cname_owner:cname,
                customer_company,
                cphone.isEmpty() ? cphone_owner:cphone,
                cline,
                customer_code,
                "" + ctype,
                cphone.isEmpty() ? cphone_owner:cphone
                );
    }

    @SuppressLint("StaticFieldLeak")
    private void updateCustomer(){
        showLoading(EditCustomerActivity.this);
        callUpdateCustomer().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(EditCustomerActivity.this,getResources().getString(R.string.toast_update_data_success));
                    finish();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(EditCustomerActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditCustomerActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void updateCustomer(final String url) {
//        showLoading(this);
//        if(cname.isEmpty()){
//            cname = cname_owner;
//        }
//        if(cphone.isEmpty()){
//            cphone = cphone_owner;
//        }
//
//
//
//
////        final RequestBody requestBody;
////            requestBody = new FormBody.Builder()
////                    .add("customer_type", ""+ctype)
////                    .add("customer_name", cname)
////                    .add("customer_phone", cphone)
////
////                    .add("customer_email", "")
////                    .add("customer_line", cline)
////                    .add("customer_tax_no", cTaxNo)
////                    .add("customer_note", cnote)
////                    .build();
//
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("customer_name", cname)
//                .add("customer_company", customer_company)
//                .add("customer_phone", cphone)
//                .add("customer_line", cline)
//                .add("customer_code", customer_code)
//                .add("customer_type", ""+ctype)
//                .add("phone", cphone)
//                .build();
//
//
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
////                    AddNewProjectActivity.projectDetail = gson.fromJson(string, ProjectDetail.class);
//                    showToast(EditCustomerActivity.this,getResources().getString(R.string.toast_update_data_success));
//                    finish();
//                }
//            }
//        }.execute();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        name.dismissDropDown();
        tel.dismissDropDown();
        name_owner.dismissDropDown();
        tel_owner.dismissDropDown();
    }


    @Override
    public void onDynamicAutocompleteStart(AutoCompleteTextView v) {
        if (v == name) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }
        if(v==tel){
            telAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }

        if (v == name_owner) {
            owner_nameProgressBar.setVisibility(View.VISIBLE);
        }
        if(v==tel_owner){
            owner_telProgressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDynamicAutocompleteStop(AutoCompleteTextView v) {
        if (v == name) {
//            customer_code ="";
            mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cname = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            name.setTypeface(_font);
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }
        }
        if(v==tel){
//            customer_code ="";
            telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cphone = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            tel.setTypeface(_font);
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }
        }

        if (v == name_owner) {
            owner_nameProgressBar.setVisibility(View.INVISIBLE);
            cname_owner = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            name_owner.setTypeface(_font);
            if (/*cname_owner.length() > 0 && */cphone_owner.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }

        }
        if(v==tel_owner){

            owner_telProgressBar.setVisibility(View.INVISIBLE);
            cphone_owner = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            tel_owner.setTypeface(_font);
            if (/*cname_owner.length() > 0 &&*/ cphone_owner.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }


        }


    }




    @Override
    protected void onPause() {
        super.onPause();
        name.dismissDropDown();
        tel.dismissDropDown();
        name_owner.dismissDropDown();
        tel_owner.dismissDropDown();
    }

    @Override
    public void selectCusItem(CusModel cusModel, int tabIndex) {
        if(tabIndex==1){
            name.setText(cusModel.getCustomer_name());
            tel.setText(cusModel.getCustomer_phone());
            lineid.setText(cusModel.getCustomer_line());

            taxNo.setText(cusModel.getCustomer_taxno());
            customer_code = cusModel.getCustomer_code();
            champ_customer_code = cusModel.getChamp_customer_code();

        }else{
            name_owner.setText(cusModel.getCustomer_name());
            tel_owner.setText(cusModel.getCustomer_phone());
            lineid_owner.setText(cusModel.getCustomer_line());
            customer_code = cusModel.getCustomer_code();
//            champ_customer_code = cusModel.getChamp_customer_code();
        }

        if(champ_customer_code.isEmpty()){
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
        }else{
            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(champ_customer_code);
        }


        name.dismissDropDown();
        tel.dismissDropDown();
        name_owner.dismissDropDown();
        tel_owner.dismissDropDown();
        Utils.hideKeyboard(this);

    }
}
