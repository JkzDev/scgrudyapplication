package com.scg.rudy.ui.checkin_update_status;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class UpdateStatus implements Parcelable {

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private int status;

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UpdateStatus{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.items);
		dest.writeInt(this.status);
	}

	public UpdateStatus() {
	}

	protected UpdateStatus(Parcel in) {
		this.items = in.readString();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<UpdateStatus> CREATOR = new Parcelable.Creator<UpdateStatus>() {
		@Override
		public UpdateStatus createFromParcel(Parcel source) {
			return new UpdateStatus(source);
		}

		@Override
		public UpdateStatus[] newArray(int size) {
			return new UpdateStatus[size];
		}
	};
}