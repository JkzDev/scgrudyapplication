package com.scg.rudy.ui.deal_by_phase.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.phaseList.ItemsItem;
import com.scg.rudy.ui.deal_by_phase.byPhaseInterface;

import java.util.List;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class SubphaseRecyclerAdapter extends RecyclerView.Adapter<SubphaseRecyclerAdapter.ProjectViewHolder> {

    private  List<com.scg.rudy.model.pojo.dealbyproject.phaseList.ItemsItem> itemList ;

    private Context context;
    private int selected_position = 0;
    private byPhaseInterface listener;

    public SubphaseRecyclerAdapter(Context context, List<com.scg.rudy.model.pojo.dealbyproject.phaseList.ItemsItem> itemList, byPhaseInterface listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.sale_phase_item, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, int position) {
        final ItemsItem groupItem = itemList.get(position);
//        holder.itemView.setBackgroundColor(selected_position == position ? Color.GREEN : Color.TRANSPARENT);
        holder.sub_phase_name.setText(groupItem.getName());
//        if(!groupItem.getImg().isEmpty()){
//            Glide.with(context)
//                    .load(groupItem.getImg())
//                    .into(holder.imageItem);
//        }

        holder.sub_phase_name.setBackground(selected_position == position ? context.getDrawable(R.drawable.bg_frame_group_select) :context.getDrawable(R.drawable.bg_project_normal));
        holder.sub_phase_name.setTextColor(selected_position == position ? Color.WHITE : Color.BLACK);
//        holder.selectIco.setVisibility(selected_position == position ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        }
        return itemList.size();
    }



    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView sub_phase_name;
        public ProjectViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            sub_phase_name = v.findViewById(R.id.sub_phase_name);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) {
                return;
            }
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            listener.onSubphaseClick(itemList.get(selected_position).getId());
        }
    }


}