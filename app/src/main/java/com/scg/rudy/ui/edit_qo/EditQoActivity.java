package com.scg.rudy.ui.edit_qo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.transaction_detail.PrItem;
import com.scg.rudy.model.pojo.transaction_detail.Transaction;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeoutException;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getAddBasket;
import static com.scg.rudy.data.remote.ApiEndPoint.getGetTranDetail;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class EditQoActivity extends BaseActivity implements EditQoInterface, View.OnClickListener {
    private Bundle extras;
    private String transaction_id;
    private RudyService rudyService;
    private ImageView clsBtn;
    private RecyclerView recyclerView;
    private List<PrItem> cartModelArrayLis;
    private ArrayList<String> checkList;
    private String qoNumber;
    private TextView titleName;
    private TextView totalAllsum;
    private Transaction transactionData;
    private String itemId;
    private String itemQty;
    private String itemPrice;
    private RelativeLayout openQO;
    private String user_id = "";
    private RelativeLayout confirm;
    private TextView textCurrency;
    private int defultTaxType;
    private Double delivery_cost;
    private Double discount;

    private String discount_type;
    private String payment_type;
    private String credit_day;
    private String credit_type;
    private String note;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_qo);


        initView();
        setTitleName();
        setOnClick();
        loadTransection();

        if (DealByPhaseActivity.transaction_id.equalsIgnoreCase("")) {
            if (!TransactionDetailActivity.transaction_id.equalsIgnoreCase("")) {
                getTransectionDetail(TransactionDetailActivity.transaction_id);
            } else {
                return;
            }
        } else {
            getTransectionDetail(DealByPhaseActivity.transaction_id);
        }
    }

    public void addBasket() {
        AddBasket(getAddBasket(Utils.APP_LANGUAGE, Utils.project_id));
    }

    private void setTitleName() {
        titleName.setText(String.format(this.getResources().getString(R.string.edit_qo_parame), qoNumber));

    }

    private Call<String> callTransection() {
        return rudyService.callTransactionList(
                Utils.APP_LANGUAGE,
                transaction_id,
                "detail"
        );
    }

    public void showTransactionList(Transaction transaction) {
        if (transaction.getItems().getPr().size() > 0) {
            cartModelArrayLis = transaction.getItems().getPr();
            Collections.sort(cartModelArrayLis, new Comparator<PrItem>() {
                @Override
                public int compare(PrItem p1, PrItem p2) {
                    return Integer.valueOf(p1.getPriceRank()).compareTo(Integer.valueOf(p2.getPriceRank()));
                }
            });
            EditQoRecyclerAdapter adapter = new EditQoRecyclerAdapter(this, cartModelArrayLis, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);

            double totalPrice = 0;
            for (int i = 0; i < cartModelArrayLis.size(); i++) {
                totalPrice += Double.parseDouble(cartModelArrayLis.get(i).getNetAmount());
            }
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
            textCurrency.setText(Utils.currencyForShow);
            if (itemId != null) {
                checkMoveToBasket(cartModelArrayLis);
            }

        } else {
            recyclerView.setAdapter(null);
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(0.0)));
            textCurrency.setText(Utils.currencyForShow);
        }
    }

    private void checkMoveToBasket(List<PrItem> cartModelArrayLis) {
        for (int i = 0; i < cartModelArrayLis.size(); i++) {
            PrItem prItem = cartModelArrayLis.get(i);
            int priceRank = Integer.parseInt((prItem.getPriceRank()));
            int approve = Integer.parseInt(prItem.getApproveBy());
            if (priceRank > 2) {
                if (approve == 0) {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                            .title(getResources().getString(R.string.dialog_price_product) + prItem.getProductName() + getResources().getString(R.string.dialog_not_pass_criteria))
                            .content(getResources().getString(R.string.dialog_product) + prItem.getProductName() + getResources().getString(R.string.dialog_move_to_cart_and_delete_from_qo_no) + qoNumber)
                            .positiveText(getResources().getString(R.string.agreed))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    addBasket();
                                    dialog.dismiss();
                                }
                            });

                    MaterialDialog del_dialog = builder.build();
                    del_dialog.show();


                }
            }
//            stringBuilder.append(priceRank+"\n");
        }
//        showToast(this,stringBuilder.toString());
    }


    private void loadTransection() {
        showLoading(this);
        callTransection().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Transaction transaction = gson.fromJson(resp, Transaction.class);
                    defultTaxType = transaction.getItems().getTax_type();
                    delivery_cost = transaction.getItems().getDelivery_cost();
                    discount = transaction.getItems().getDiscount();
                    discount_type = transaction.getItems().getDiscount_type();
                    payment_type = transaction.getItems().getPayment_type();
                    credit_day = transaction.getItems().getCredit_day();
                    credit_type = transaction.getItems().getCredit_type();
                    note = transaction.getItems().getNote();
                    showTransactionList(transaction);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(EditQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(EditQoActivity.this, fetchErrorMessage(t));
            }
        });
    }


    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Transaction fetchResults(Response<Transaction> response) {
        Transaction dashboard = response.body();
        return dashboard;
    }

    private Call<String> callUpdatePriceQty(String productId, String itemQty, String itemPrice, String discount, String discount_type) {
        return rudyService.editPrice(
                Utils.APP_LANGUAGE,
                transaction_id,
                Utils.project_id,
                productId,
                itemPrice,
                user_id,
                itemQty,
                discount,
                discount_type
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updatePriceQty(String productId, String itemQty, String itemPrice, String discount, String discount_type ){
        showLoading(EditQoActivity.this);
        callUpdatePriceQty(productId, itemQty, itemPrice, discount, discount_type).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(EditQoActivity.this, getResources().getString(R.string.toast_edit_product_success));
                    loadTransection();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(EditQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void updatePriceQty(final String url, String productId, String itemQty, String itemPrice) {
//        Log.i("post ", " : " + url);
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("project_id", Utils.project_id);
//        formData.add("product_id", productId);
//        formData.add("price", itemPrice);
//        formData.add("user_id", user_id);
//        formData.add("qty", itemQty);
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(EditQoActivity.this, getResources().getString(R.string.toast_edit_product_success));
//                    loadTransection();
//                }
//
//            }
//        }.execute();
//    }

    private Call<String> callDeletePr(String productId) {
        return rudyService.deletePr(
                Utils.APP_LANGUAGE,
                transaction_id,
                productId,
                project_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteItems(String productId){
        showLoading(EditQoActivity.this);
        callDeletePr(productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                String resp = response.body();
                showToast(EditQoActivity.this, getResources().getString(R.string.toast_delete_from_qo_no) + qoNumber + getResources().getString(R.string.succeed));
                if (resp.contains("200")) {

                    loadTransection();
                } else {
                    loadTransection();
                    recyclerView.setAdapter(null);
                    totalAllsum.setText(Utils.moneyFormat(String.valueOf(0.0)));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void deleteItems(final String url, String productId) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("product_id", productId);
//        formData.add("project_id", Utils.project_id);
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                showToast(EditQoActivity.this, getResources().getString(R.string.toast_delete_from_qo_no) + qoNumber + getResources().getString(R.string.succeed));
//                if (string.contains("200")) {
//
//                    loadTransection();
//                } else {
//                    loadTransection();
//                    recyclerView.setAdapter(null);
//                    totalAllsum.setText(Utils.moneyFormat(String.valueOf(0.0)));
//                }
//
//            }
//        }.execute();
//    }


    private Call<String> callDeleteItemsBeforeAdd(String productId) {
        return rudyService.deletePr(
                Utils.APP_LANGUAGE,
                transaction_id,
                project_id,
                productId

        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteItemsBeforeAdd(String productId){
        showLoading(EditQoActivity.this);
        callDeleteItemsBeforeAdd(productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")){
                    postDataToCart(Utils.project_id);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(EditQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void deleteItemsBeforeAddd(final String url, String productId) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("product_id", productId);
//        formData.add("project_id", Utils.project_id);
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                postDataToCart(getAddBasket(Utils.APP_LANGUAGE, Utils.project_id));
//
//            }
//        }.execute();
//    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return true;
    }

    public void hideKeyboard() {

    }

    private void setOnClick() {
        clsBtn.setOnClickListener(this);
        openQO.setOnClickListener(this);
        confirm.setOnClickListener(this);
    }

    private void initView() {
        extras = getIntent().getExtras();
        if (extras != null) {
            transaction_id = extras.getString("transaction_id");
            qoNumber = extras.getString("qoNumber");
        }

        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();

        rudyService = ApiHelper.getClient();
        clsBtn = (ImageView) findViewById(R.id.cls_btn);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        titleName = (TextView) findViewById(R.id.title_name);
        totalAllsum = (TextView) findViewById(R.id.total_allsum);
        openQO = (RelativeLayout) findViewById(R.id.openQO);
        confirm = (RelativeLayout) findViewById(R.id.confirm);
        textCurrency = (TextView) findViewById(R.id.textView22);
    }

//    @Override
//    public void onClickEdit(String itemId, String itemQty, String itemPrice) {
//        this.itemId = itemId;
//        this.itemQty = itemQty;
//        this.itemPrice = itemPrice;
//        String url = ApiEndPoint.getEdit_price(Utils.APP_LANGUAGE, transaction_id);
//        updatePriceQty(url, itemId, itemQty, itemPrice);
//    }

    @Override
    public void onClickEdit(String itemId, String itemQty, String itemPrice, String user_id, String discount, String discount_type) {
        this.itemId = itemId;
        this.itemQty = itemQty;
        this.itemPrice = itemPrice;
        updatePriceQty(itemId, itemQty, itemPrice, discount, discount_type);
    }

    @Override
    public void onClickDelete(String itemId) {
        deleteItems(itemId);
    }

    @Override
    public void onPriceLower(String itemId, String itemQty, Double itemPrice) {

    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == confirm) {
            finish();
        }


        if (v == openQO) {
            Intent intent = new Intent(EditQoActivity.this, EditQoDetailActivity.class);
            intent.putExtra("totalPrice", totalAllsum.getText().toString());
            intent.putExtra("qo_id", transaction_id);
            intent.putExtra("tax_type", defultTaxType);
            intent.putExtra("delivery_cost", delivery_cost);
            intent.putExtra("discount", discount);
            intent.putExtra("discount_type", discount_type);
            intent.putExtra("payment_type", payment_type);
            intent.putExtra("credit_day", credit_day);
            intent.putExtra("credit_type", credit_type);
            intent.putExtra("note", note);

            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void AddBasket(final String url) { //TODO list 1
        if (DealByPhaseActivity.transaction_id.equalsIgnoreCase("")) {
            if (!TransactionDetailActivity.transaction_id.equalsIgnoreCase("")) {
                reDataTransectionDetail(TransactionDetailActivity.transaction_id);
            } else {
                deleteItemsBeforeAdd(itemId);
            }
        } else {
            reDataTransectionDetail(DealByPhaseActivity.transaction_id);
        }
    }

    private Call<String> callGetTransectionDetail(String transaction_id) {
        return rudyService.getTransectionDetail(
                Utils.APP_LANGUAGE,
                transaction_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getTransectionDetail(String transaction_id){
        showLoading(EditQoActivity.this);
        callGetTransectionDetail(transaction_id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    transactionData = gson.fromJson(resp, Transaction.class);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(EditQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void getTransectionDetail(final String url) {
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                Gson gson = new Gson();
//                transactionData = gson.fromJson(string, Transaction.class);
//            }
//        }.execute();
//    }


    @SuppressLint("StaticFieldLeak")
    private void reDataTransectionDetail(String transaction_id) {

        boolean isAddBudget = false;
        if (transactionData != null) {
            if (transactionData.getItems().getPr().size() > 0) {
                for (int i = 0; i < transactionData.getItems().getPr().size(); i++) {
                    if (transactionData.getItems().getPr().get(i).getProductId().equalsIgnoreCase(itemId)) {
                        isAddBudget = true;
                    }
                }
                if (isAddBudget) {
                    deleteItemsBeforeAdd(itemId);
                } else {
                    postDataToCart(transaction_id);
                }
            } else {
                postDataToCart(transaction_id);
            }
        } else {
            postDataToCart(transaction_id);
        }
    }

    private Call<String> callPostDataToCart(String transaction_id) {
        return rudyService.addBasket(
                Utils.APP_LANGUAGE,
                transaction_id,
                itemId,
                itemQty,
                itemPrice,
                "",
                user_id,
                "",
                "",
                ""
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void postDataToCart(String transaction_id){
        showLoading(EditQoActivity.this);
        callPostDataToCart(transaction_id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(EditQoActivity.this, getResources().getString(R.string.toast_move_product_to_cart_success));
                    deleteItems(itemId);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(EditQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(EditQoActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void postDataToCart(String url){
//        showLoading(this);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("product_id", itemId)
//                .add("qty", itemQty)
//                .add("price", itemPrice)
//                .add("note", "")
//                .add("user_id", user_id)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(EditQoActivity.this, getResources().getString(R.string.toast_move_product_to_cart_success));
//                    deleteItems(itemId);
//                }
//            }
//        }.execute();
//    }


}
