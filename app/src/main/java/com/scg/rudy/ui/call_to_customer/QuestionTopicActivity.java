package com.scg.rudy.ui.call_to_customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Adapter.QuestionTopicAdapter;
import com.scg.rudy.ui.call_to_customer.Models.QuestionTopicModel;
import com.scg.rudy.utils.Utils;
import com.smarteist.autoimageslider.SliderLayout;

import java.util.ArrayList;

public class QuestionTopicActivity extends AppCompatActivity {

    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_topic);

        setSliderViews();


        //set adapter hide content
        RecyclerView recyclerView = findViewById(R.id.revContent);
        ArrayList<QuestionTopicModel> questionTopicModels = new ArrayList<>();
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));
        questionTopicModels.add(new QuestionTopicModel("", "ภาพรวมการใช้แผ่นพื้น Hollow core กับ Prank concrete ในอาคารช่วงพาดขนาดกลาง", "CPAC", "18 พย. 62", 1125));

        QuestionTopicAdapter adapter = new QuestionTopicAdapter(this, questionTopicModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private void setSliderViews() {
        sliderLayout = findViewById(R.id.imageSlider);
        ArrayList<String> bannerImage = new ArrayList<String>();
        bannerImage.add("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        bannerImage.add("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
        bannerImage.add("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        Utils.setSliderViews(this, sliderLayout, bannerImage);
    }
}
