/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.department_data_group.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.ui.customer_data.customerInterface;
import com.scg.rudy.ui.customer_data.filter.ExtendedDictionaryAutocompleteFilter;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.department_data_group.DepartmentGroupInterface;
import com.scg.rudy.ui.department_data_group.model.Developer;
import com.scg.rudy.ui.department_data_group.provider.ExtendedDictionaryAutocompleteProvider;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteFilterListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author jackie
 */
public class DeveloperDictionaryAutocompleteAdapter extends ArrayAdapter<Developer> implements OnDynamicAutocompleteFilterListener<Developer> {

    private List<Developer> mListItems;
    private ExtendedDictionaryAutocompleteProvider mExtendedDictionaryAutocompleteProvider;
    private ExtendedDictionaryAutocompleteFilter mDictionaryAutocompleteFilter;
    private LayoutInflater mLayoutInflater;
    private int mLayoutId;
    private DepartmentGroupInterface listener;
    private int tabIndex;
    public DeveloperDictionaryAutocompleteAdapter(Context context, int textViewResourceId, DepartmentGroupInterface _listener, int _tabIndex) {
        super(context, textViewResourceId);
        mExtendedDictionaryAutocompleteProvider = new ExtendedDictionaryAutocompleteProvider();
        mDictionaryAutocompleteFilter = new ExtendedDictionaryAutocompleteFilter(mExtendedDictionaryAutocompleteProvider, this);
        mDictionaryAutocompleteFilter.useCache(true);
        mListItems = new ArrayList<>();
        mLayoutId = textViewResourceId;
        listener = _listener;
        tabIndex = _tabIndex;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void onDynamicAutocompleteFilterResult(Collection<Developer> result) {
        mListItems.clear();
        mListItems.addAll(result);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mDictionaryAutocompleteFilter;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public void clear() {
        mListItems.clear();
    }

    @Override
    public Developer getItem(int position) {
        return mListItems.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mLayoutId, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.countryNameText);
            viewHolder.code = (TextView) convertView.findViewById(R.id.countryCodeText);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Developer developer = getItem(position);
        viewHolder.name.setText(developer.getName());
        viewHolder.code.setText(developer.getDescription());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.selectCusItem(developer);
            }
        });

        return convertView;
    }

    /**
     * Holder for list item.
     */
    private static class ViewHolder {
        private TextView name;
        private TextView code;

    }
}