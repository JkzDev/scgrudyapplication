package com.scg.rudy.ui.department_data_group.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("customer_line")
	private String customerLine;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("pic")
	private String pic;

	@SerializedName("customer_note")
	private String customerNote;

	public void setCustomerLine(String customerLine){
		this.customerLine = customerLine;
	}

	public String getCustomerLine(){
		return customerLine;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setCustomerNote(String customerNote){
		this.customerNote = customerNote;
	}

	public String getCustomerNote(){
		return customerNote;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"customer_line = '" + customerLine + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",pic = '" + pic + '\'' + 
			",customer_note = '" + customerNote + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.customerLine);
		dest.writeString(this.customerPhone);
		dest.writeString(this.customerName);
		dest.writeString(this.pic);
		dest.writeString(this.customerNote);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.customerLine = in.readString();
		this.customerPhone = in.readString();
		this.customerName = in.readString();
		this.pic = in.readString();
		this.customerNote = in.readString();
	}

	public static final Creator<ItemsItem> CREATOR = new Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}