package com.scg.rudy.ui.add_group_project;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.departmentdata_new.ProjectNew;
import com.scg.rudy.model.pojo.project_group_detail.Items;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.ui.current_location.AddCurrentGroupLocationActivity;
import com.scg.rudy.ui.customer_data.CustomerDataProjectActivity;
import com.scg.rudy.ui.department_data_group.DepartmentDataGroupActivity;
import com.scg.rudy.ui.department_photo.TakeGroupPhotoActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.project_by_group.ProjectByGroupActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.cardview.widget.CardView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class AddNewGroupProjectActivity extends BaseActivity implements View.OnClickListener {
    public static CardView cardLocal;
    public static CardView cardTakepic;
    public static CardView cardDepartment;
    public static CardView cardCustomer;
    public static CardView cardByphase;
    public static ImageView ch1;
    public static ImageView ch2;
    public static ImageView ch3;
    public static ImageView ch4;
    private ImageView clsBtn;
    private TextView titleName;
    private LinearLayout adLocal;
    public static ImageView icoLocation;
    public static TextView txtLocal;
    public static CardView bgNum1;
    public static LinearLayout takePhoto;
    public static ImageView icoPhoto;
    public static TextView txtPhoto;
    public static CardView bgNum2;
    public static LinearLayout addDepartment;
    public static ImageView icoDepart;
    public static TextView txtDepart;
    public static CardView bgNum3;
    public static LinearLayout addCustomer;
    public static ImageView icoCustomer;
    public static TextView txtCustomer;
    public static CardView bgNum4;
    public static ImageView icoSale;
    public static TextView txtSale;
    public static CardView bgNum5;
    public static RelativeLayout postProject;
    public static RelativeLayout fake_layout;
    public static Bitmap bit1;
    public static Bitmap bit2;
    public static Bitmap bit3;
    public static Bitmap bit4;
    public static Bitmap bit5;
    public static Bitmap bit6;
    public static Double DragLat = 0.0, DragLng = 0.0;
    public static String address = "";
    public static boolean step1 = false;
    public static boolean step2 = false;
    public static boolean step3 = false;
    public static boolean step4 = false;
    public static String phaseId = "1";
    public static String cateId = "1";
    public static String unit_number = "";
    public static String floor_number = "";
    public static String c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
    public static String c_id_owner = "";
    public static int ctype = 1;
    public static int ctype_owner = 2;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
    public static String cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany = "";// บริษัทลูกค้า
    public static String cphone = "";// เบอร์ติดต่อลูกค้า
    public static String cline = "";// line ID ของลูกค้า
    public static String cnote = "";// noteลูกค้า
    public static String cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany_owner = "";// บริษัทลูกค้า
    public static String cphone_owner = "";// เบอร์ติดต่อลูกค้า
    public static String cline_owner = "";// line ID ของลูกค้า
    public static String cnote_owner = "";// noteลูกค้า
    public static String other_type = "";
    public static String shopID = "";
    public static String image64_1 = "";
    public static String image64_2 = "";
    public static String image64_3 = "";
    public static String image64_4 = "";
    public static String image64_5 = "";
    public static String image64_6 = "";
    public static int project_group_position = 0;
    public static int phasePosition = 0;
    public static int customerTypePosition = 0;
    public static int ownerTypePosition = 0;
    private String imagei_1 = "";
    private String imagei_2 = "";
    private String imagei_3 = "";
    private String imagei_4 = "";
    private String imagei_5 = "";
    private String imagei_6 = "";
    private LinearLayout saleByphase;
    private Bundle extras;
    private TextView txtPostProject;
    public static double unit_budget_number;
    public static int maxPhase = 10;
    private Button clearData;
    public static ProjectGroupDetail projectDetail;
    public static String group_id = "";
    //   New post data api
    public static String user_id ;//* : User who add the project
    public static String lat_lng  ="";//: project lat and lng [13.729194,100.622218]
    public static String project_address ="" ;//: project location address
    public static String project_name  ="";//: Project name [API will create if you did not post it]
    public static String project_type_id ="" ;//: project type id [API will create if you did not post it  ;//: 1]
    public static String phase_id ="" ;//: phase id [API will create if you did not post it  ;//: 1]
    public static String units  ="";//: How many unit [API will create if you did not post it  ;//: 1]
    public static String unit_area ="" ;//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
    public static String unit_budget  ="";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
    public static String project_stories ="" ;//: story [API will create if you did not post it  ;//: 2]
    public static String customer_name ="" ;//: Customer’s name
    public static String customer_phone ="" ;//: Customer’s phone
    public static String customer_email ="" ;//: Customer’s email
    public static String customer_line ="" ;//: Customer’s line
    public static String customer_tax_no ="" ;//: Customer’s tax no or ID card no.
    public static String customer_note ="" ;//: Customer’s note
    public static String house_owner_name ="" ;//: House owner name
    public static String house_owner_phone ="" ;//: house_owner_phone
    public static String house_owner_line ="" ;//: House owner line id
    public static String House_owner_note ="" ;//: House owner note
    public static String project_owner_name ="" ;//: Project owner name
    public static String project_owner_phone=""  ;//: project_owner_phone
    public static String project_owner_line ="" ;//: Project owner line
    public static String project_owner_note ="" ;//: Project owner note
    public static String customer_code ="" ; //: customer_code
    public static String champ_customer_code =""; //: champ_customer_code
    public static String pic ="";//: array post with base64
    private ArrayList<String> nameList;
    public static ArrayList<Bitmap> mArrayList;
    public static final String TAG = "AddNewProjectActivity";
    public static ArrayList<ProjectNew> arrayListNewProject;
    private RudyService rudyService;
    public static String customer_company ="";


    public static String projectGroupName;
//    project group
    public static String name;
    public static String project_group_list_id;
    public static String developer_id;
    public static String developer_name_other;
    public static String start_date;
    public static String end_date;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_project);
        clearStaticData();
        initView();
        getUserData();
        setOnclick();
        nameList = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        arrayListNewProject = new ArrayList<>();
    }

    private void clearStaticData() {
        DragLat = 0.0;
        DragLng = 0.0;
        address = "";
        step1 = false;
        step2 = false;
        step3 = false;
        step4 = false;
        phaseId = "1";
        cateId = "1";
        unit_number = "";
        floor_number = "";
        c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
        c_id_owner = "";
        ctype = 1;
        ctype_owner = 2;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
        cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
        ccompany = "";// บริษัทลูกค้า
        cphone = "";// เบอร์ติดต่อลูกค้า
        cline = "";// line ID ของลูกค้า
        cnote = "";// noteลูกค้า
        cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
        ccompany_owner = "";// บริษัทลูกค้า
        cphone_owner = "";// เบอร์ติดต่อลูกค้า
        cline_owner = "";// line ID ของลูกค้า
        cnote_owner = "";// noteลูกค้า
        other_type = "";
        shopID = "";
        image64_1 = "";
        image64_2 = "";
        image64_3 = "";
        image64_4 = "";
        image64_5 = "";
        image64_6 = "";
        project_group_position = 0;
        phasePosition = 0;
        customerTypePosition = 0;
        ownerTypePosition = 0;
        maxPhase = 10;
        group_id = "";
        lat_lng  ="";//: project lat and lng [13.729194,100.622218]
        project_address ="" ;//: project location address
        project_name  ="";//: Project name [API will create if you did not post it]
        project_type_id ="" ;//: project type id [API will create if you did not post it  ;//: 1]
        phase_id ="" ;//: phase id [API will create if you did not post it  ;//: 1]
        units  ="";//: How many unit [API will create if you did not post it  ;//: 1]
        unit_area ="" ;//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
        unit_budget  ="";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
        project_stories ="" ;//: story [API will create if you did not post it  ;//: 2]
        customer_name ="" ;//: Customer’s name
        customer_phone ="" ;//: Customer’s phone
        customer_email ="" ;//: Customer’s email
        customer_line ="" ;//: Customer’s line
        customer_tax_no ="" ;//: Customer’s tax no or ID card no.
        customer_note ="" ;//: Customer’s note
        house_owner_name ="" ;//: House owner name
        house_owner_phone ="" ;//: house_owner_phone
        house_owner_line ="" ;//: House owner line id
        House_owner_note ="" ;//: House owner note
        project_owner_name ="" ;//: Project owner name
        project_owner_phone=""  ;//: project_owner_phone
        project_owner_line ="" ;//: Project owner line
        project_owner_note ="" ;//: Project owner note
        customer_code ="" ; //: customer_code
        champ_customer_code =""; //: champ_customer_code
        pic ="";//: array post with base64
        customer_company ="";
    }

    public void getUserData() {
        extras = getIntent().getExtras();
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
            AddCurrentGroupLocationActivity.shopID = shopID;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setOnclick() {
        adLocal.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
        addDepartment.setOnClickListener(this);
        addCustomer.setOnClickListener(this);
        saleByphase.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        postProject.setOnClickListener(this);
    }

    public void setVariable() {
        Items projectDetailItems = projectDetail.getItems();
        shopID = projectDetailItems.getShopId();
        if (projectDetailItems.getLatLng().length() > 0) {
            String latlng[] = projectDetailItems.getLatLng().split(",");
            AddNewGroupProjectActivity.DragLat = Double.parseDouble(latlng[0]);
            AddNewGroupProjectActivity.DragLng = Double.parseDouble(latlng[1]);
        }

        AddNewGroupProjectActivity.cateId = projectDetailItems.getProjectGroupListId();
        AddNewGroupProjectActivity.c_id = projectDetailItems.getUserId();
        AddNewGroupProjectActivity.ctype = Integer.parseInt(projectDetailItems.getCustomerType());
        AddNewGroupProjectActivity.cname = projectDetailItems.getCustomerName();
        AddNewGroupProjectActivity.customer_company = projectDetailItems.getCustomerCompany();
        AddNewGroupProjectActivity.ccompany = "";
        AddNewGroupProjectActivity.cphone = projectDetailItems.getCustomerPhone();
        AddNewGroupProjectActivity.cline = projectDetailItems.getCustomerLine();
        AddNewGroupProjectActivity.cnote = projectDetailItems.getCustomerNote();
        AddNewGroupProjectActivity.projectGroupName = projectDetailItems.getName();
        AddNewGroupProjectActivity.customer_tax_no = projectDetailItems.getCustomerTaxNo();
        AddNewGroupProjectActivity.customer_name = projectDetailItems.getCustomerName();
        AddNewGroupProjectActivity.customer_code = projectDetailItems.getCustomerCode();
        AddNewGroupProjectActivity.champ_customer_code = projectDetailItems.getChampCustomerCode();
        AddNewGroupProjectActivity.customer_company = projectDetailItems.getCustomerCompany();
        AddNewGroupProjectActivity.customer_phone = projectDetailItems.getCustomerPhone();
        AddNewGroupProjectActivity.customer_email = projectDetailItems.getChampCustomerCode();
        AddNewGroupProjectActivity.customer_line = projectDetailItems.getCustomerLine();
        AddNewGroupProjectActivity.customer_tax_no = projectDetailItems.getCustomerTaxNo();
        AddNewGroupProjectActivity.customer_note = projectDetailItems.getCustomerNote();
    }

    public void initView() {
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        titleName.setText(getResources().getString(R.string.create_new_work_project));
        adLocal = findViewById(R.id.ad_local);
        saleByphase = findViewById(R.id.sale_byphase);
        txtPostProject = findViewById(R.id.txtPostProject);
        clearData = findViewById(R.id.clear_data);

        mArrayList = new ArrayList<>();
        icoLocation = findViewById(R.id.ico_location);
        txtLocal = findViewById(R.id.txt_local);
        bgNum1 = findViewById(R.id.bg_num1);
        takePhoto = findViewById(R.id.take_photo);
        icoPhoto = findViewById(R.id.ico_photo);
        txtPhoto = findViewById(R.id.txt_photo);
        bgNum2 = findViewById(R.id.bg_num2);
        addDepartment = findViewById(R.id.add_department);
        icoDepart = findViewById(R.id.ico_depart);
        txtDepart = findViewById(R.id.txt_depart);
        bgNum3 = findViewById(R.id.bg_num3);
        addCustomer = findViewById(R.id.add_customer);
        icoCustomer = findViewById(R.id.ico_customer);
        txtCustomer = findViewById(R.id.txt_customer);
        bgNum4 = findViewById(R.id.bg_num4);
        postProject = findViewById(R.id.save);
        fake_layout = findViewById(R.id.fake_layout);
        icoSale = findViewById(R.id.ico_sale);
        txtSale = findViewById(R.id.txt_sale);
        bgNum5 = findViewById(R.id.bg_num5);
        cardLocal = findViewById(R.id.card_local);
        cardTakepic = findViewById(R.id.card_takepic);
        cardDepartment = findViewById(R.id.card_department);
        cardCustomer = findViewById(R.id.card_customer);
        cardByphase = findViewById(R.id.card_byphase);
        ch1 = findViewById(R.id.ch1);
        ch2 = findViewById(R.id.ch2);
        ch3 = findViewById(R.id.ch3);
        ch4 = findViewById(R.id.ch4);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (projectDetail != null) {
            setVariable();
            nameList = new ArrayList<>();
//            projectDetail = projectDetail(detailData);
//            detailData = "";
            if (projectDetail.getItems().getGallery().size() > 0) {
                for (int i = 0; i < projectDetail.getItems().getGallery().size(); i++) {
                    final String image = projectDetail.getItems().getGallery().get(i).replace(ApiEndPoint.replaceImageProjectName, "");
                    Glide.with(this).asBitmap().load(projectDetail.getItems().getGallery().get(i)).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            nameList.add(image);
                            mArrayList.add(resource);
                        }
                    });
                }
            }
            titleName.setText(String.format(this.getResources().getString(R.string.edit_data_parame), projectDetail.getItems().getName()));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            txtPostProject.setText(getResources().getString(R.string.save));

            postProject.setOnClickListener(this);
        }

        if (DragLat != 0.0 || DragLng != 0.0) {
            step1 = true;
        }

        if (bit1 != null || bit2 != null || bit3 != null || bit4 != null || bit5 != null) {
            step2 = true;
        }

        if (!MainActivity.gdata1) {
            icoLocation.setImageDrawable(this.getDrawable(R.drawable.icons_8_address));
            cardLocal.setCardBackgroundColor(getResources().getColor(R.color.white));
            ch1.setVisibility(View.GONE);
            txtLocal.setTextColor(getResources().getColor(R.color.txt_color_login));
            bgNum1.setCardBackgroundColor(getResources().getColor(R.color.white));
        }

        if (MainActivity.gdata1) {
            icoLocation.setImageDrawable(this.getDrawable(R.drawable.icons_8_address_white));
            cardLocal.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            ch1.setVisibility(View.VISIBLE);
            txtLocal.setTextColor(getResources().getColor(R.color.white));
            bgNum1.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);

        }

        if (MainActivity.gdata2) {
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewGroupProjectActivity.icoPhoto.setImageDrawable(getDrawable(R.drawable.shape_white));
            AddNewGroupProjectActivity.cardTakepic.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.ch2.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.txtPhoto.setTextColor(getResources().getColor(R.color.white));
            AddNewGroupProjectActivity.bgNum2.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
        }

        if (!MainActivity.gdata2) {
            AddNewGroupProjectActivity.icoPhoto.setImageDrawable(getDrawable(R.drawable.shape));
            AddNewGroupProjectActivity.cardTakepic.setCardBackgroundColor(getResources().getColor(R.color.white));
            AddNewGroupProjectActivity.ch2.setVisibility(View.GONE);
            AddNewGroupProjectActivity.txtPhoto.setTextColor(getResources().getColor(R.color.txt_color_login));
            AddNewGroupProjectActivity.bgNum2.setCardBackgroundColor(getResources().getColor(R.color.white));
            AddNewGroupProjectActivity.postProject.setVisibility(View.GONE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.VISIBLE);
        }

        if (MainActivity.gdata3) {
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewGroupProjectActivity.icoDepart.setImageDrawable(getDrawable(R.drawable.icons_8_real_estate_white));
            AddNewGroupProjectActivity.cardDepartment.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.ch3.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.txtDepart.setTextColor(getResources().getColor(R.color.white));
            AddNewGroupProjectActivity.bgNum3.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);

        }

        if (MainActivity.gdata4) {
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewGroupProjectActivity.icoCustomer.setImageDrawable(getDrawable(R.drawable.icons_8_permanent_job_white));
            AddNewGroupProjectActivity.cardCustomer.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.ch4.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.txtCustomer.setTextColor(getResources().getColor(R.color.white));
            AddNewGroupProjectActivity.bgNum4.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
        }

        if (!MainActivity.gdata5) {
            icoSale.setImageDrawable(this.getDrawable(R.drawable.icons_8_briefcase));
            cardByphase.setCardBackgroundColor(getResources().getColor(R.color.white));
            txtSale.setTextColor(getResources().getColor(R.color.txt_color_login));
            bgNum5.setCardBackgroundColor(getResources().getColor(R.color.white));
        }

        if (MainActivity.gdata5) {
            icoSale.setImageDrawable(this.getDrawable(R.drawable.icons_8_briefcase_white));
            cardByphase.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            txtSale.setTextColor(getResources().getColor(R.color.white));
            bgNum5.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
        }

        if (Utils.getPrefer(this, "QC").length() > 0) {
            AddNewGroupProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewGroupProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewGroupProjectActivity.fake_layout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == adLocal) {
            if (group_id.length() > 0) {
                Intent intent = new Intent(this, AddCurrentGroupLocationActivity.class);
//                AddCurrentGroupLocationActivity.isEdit = true;
//                AddCurrentGroupLocationActivity.editlatlng = projectDetail.getItems().getLatLng();
//                AddCurrentGroupLocationActivity.shopID = projectDetail.getItems().getId();
                startActivity(intent);
            } else {
                AddCurrentGroupLocationActivity.isEdit = false;
                startActivity(new Intent(this, AddCurrentGroupLocationActivity.class));
            }
        }

        if (v == takePhoto) {
            TakeGroupPhotoActivity.mArrayList = mArrayList;
            if (group_id.length() > 0) {
                TakeGroupPhotoActivity.nameList = nameList;
                TakeGroupPhotoActivity.group_id = group_id;
            }
            startActivity(new Intent(AddNewGroupProjectActivity.this, TakeGroupPhotoActivity.class));
        }

        if (v == addDepartment) {
                Intent intent = new Intent(this, DepartmentDataGroupActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("project_name", projectGroupName);
                intent.putExtra("cateId", project_group_list_id);
                intent.putExtra("developer_id", developer_id);
                intent.putExtra("developer_name_other", developer_name_other);
                intent.putExtra("start_date", start_date);
                intent.putExtra("end_date", end_date);
                intent.putExtra("lat_lng", lat_lng);
                intent.putExtra("project_address", project_address);
                intent.putExtra("customer_type", ctype);
                intent.putExtra("customer_name", customer_name);
                intent.putExtra("customer_code", customer_code);
                intent.putExtra("champ_customer_code", champ_customer_code);
                intent.putExtra("customer_company", customer_company);
                intent.putExtra("customer_phone", customer_phone);
                intent.putExtra("customer_email", customer_email);
                intent.putExtra("customer_line", customer_line);
                intent.putExtra("customer_tax_no", customer_tax_no);
                intent.putExtra("customer_note", customer_note);

                startActivity(intent);
        }

        if (v == addCustomer) {
                Intent intent = new Intent(this, CustomerDataProjectActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("project_name", projectGroupName);
                intent.putExtra("cateId", project_group_list_id);
                intent.putExtra("developer_id", developer_id);
                intent.putExtra("developer_name_other", developer_name_other);
                intent.putExtra("start_date", start_date);
                intent.putExtra("end_date", end_date);
                intent.putExtra("ctype", String.valueOf(ctype));
                intent.putExtra("customer_name", customer_name);
                intent.putExtra("customer_code", customer_code);
                intent.putExtra("customer_company", customer_company);
                intent.putExtra("customer_phone", customer_phone);
                intent.putExtra("customer_email", customer_email);
                intent.putExtra("customer_line", customer_line);
                intent.putExtra("tax_number", customer_tax_no);
                intent.putExtra("customer_note", customer_note);
                intent.putExtra("champ_customer_code", champ_customer_code);
                startActivity(intent);
        }

        if (v == clsBtn) {
            Utils.savePrefer(this, "print", "");
            finish();
        }

        if (v == postProject) {
            Utils.savePrefer(this, "print", "");
            if (projectDetail == null) {
                    if(AddNewGroupProjectActivity.group_id.equalsIgnoreCase("")){
                        postNewGroupProject();
                    }else {
                        finish();
                    }
            } else {
                finish();
            }

        }

        if (v == saleByphase) {
            Utils.savePrefer(this, "QC", "1");
            Utils.savePrefer(this, "print", "");
            if(AddNewGroupProjectActivity.group_id.equalsIgnoreCase("")){
                postNewGroupProjectQuickSale();
            }else{
                Intent intent = new Intent(AddNewGroupProjectActivity.this, ProjectByGroupActivity.class);
                intent.putExtra("group_id",AddNewGroupProjectActivity.group_id);
                startActivity(intent);
            }
        }

    }

    private Call<String> callNewGroupProjectQuickSale(List<String> _pic) {
        return rudyService.newGroupProject(
                Utils.APP_LANGUAGE,
                user_id,
                name,
                lat_lng,
                project_address,
                project_group_list_id,
                developer_id,
                developer_name_other,
                start_date,
                end_date,
                ctype+"",
                customer_name,
                customer_code,champ_customer_code,
                customer_company,
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void postNewGroupProjectQuickSale() {
        showLoading(AddNewGroupProjectActivity.this);
        List<String> img = new ArrayList<>();
        for (int i = 0; i < mArrayList.size(); i++) {
            img.add("data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
        }

        callNewGroupProjectQuickSale(img).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
                if (resp.contains("200")) {
                    ProjectGroupDetail projectDetail = gson.fromJson(resp, ProjectGroupDetail.class);
                    AddNewGroupProjectActivity.group_id =projectDetail.getItems().getId();
                    postChildProject();

                } else {
                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(AddNewGroupProjectActivity.this, t.getMessage());
            }
        });

    }


//    @SuppressLint("StaticFieldLeak")
//    private void postNewGroupProjectQuickSale(final String url) {
//        showLoading(this);
//        final RequestBody requestBody;
//        final FormBody.Builder formData = new FormBody.Builder();
//        try {
//            for (int i = 0; i < mArrayList.size(); i++) {
//                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
//                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        formData.add("user_id",  user_id)
//                .add("name", name)
//                .add("lat_lng", lat_lng)
//                .add("project_address", project_address)
//                .add("project_group_list_id", project_group_list_id)
//                .add("developer_id", developer_id)
//                .add("developer_name_other", developer_name_other)
//                .add("start_date", start_date)
//                .add("end_date", end_date)
//                .add("customer_type", ctype+"")
//                .add("customer_name", customer_name)
//                .add("customer_code", customer_code)
//                .add("champ_customer_code", champ_customer_code)
//                .add("customer_company", customer_company)
//                .add("customer_phone", customer_phone)
//                .add("customer_email", customer_email)
//                .add("customer_line", customer_line)
//                .add("customer_tax_no", customer_tax_no)
//                .add("customer_note", customer_note);
//
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
//                    ProjectGroupDetail projectDetail = gson.fromJson(string, ProjectGroupDetail.class);
//                    AddNewGroupProjectActivity.group_id =projectDetail.getItems().getId();
//                    postChildProject(ApiEndPoint.getNewAdd(Utils.APP_LANGUAGE), 0);
//
//                } else {
//                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
//                }
//
//            }
//        }.execute();
//    }


    private Call<String> callNewGroupProject(List<String> _pic) {
        return rudyService.newGroupProject(
                Utils.APP_LANGUAGE,
                user_id,
                name,
                lat_lng,
                project_address,
                project_group_list_id,
                developer_id,
                developer_name_other,
                start_date,
                end_date,
                ctype+"",
                customer_name,
                customer_code,champ_customer_code,
                customer_company,
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void postNewGroupProject() {
        showLoading(AddNewGroupProjectActivity.this);
        List<String> img = new ArrayList<>();
        for (int i = 0; i < mArrayList.size(); i++) {
            img.add("data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
        }
        callNewGroupProject(img).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
                if (resp.contains("200")) {
                    AddNewGroupProjectActivity.DragLat = 0.0;
                    AddNewGroupProjectActivity.DragLng = 0.0;
                    AddNewGroupProjectActivity.bit1 = null;
                    AddNewGroupProjectActivity.bit2 = null;
                    AddNewGroupProjectActivity.bit3 = null;
                    AddNewGroupProjectActivity.bit4 = null;
                    AddNewGroupProjectActivity.bit5 = null;
                    AddNewGroupProjectActivity.bit6 = null;
                    finish();

                } else {
                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(AddNewGroupProjectActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void postNewGroupProject(final String url) {
//        showLoading(this);
//        final RequestBody requestBody;
//        final FormBody.Builder formData = new FormBody.Builder();
//        try {
//            for (int i = 0; i < mArrayList.size(); i++) {
//                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
//                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        formData.add("user_id",  user_id)
//                .add("name", name)
//                .add("lat_lng", lat_lng)
//                .add("project_address", project_address)
//                .add("project_group_list_id", project_group_list_id)
//                .add("developer_id", developer_id)
//                .add("developer_name_other", developer_name_other)
//                .add("start_date", start_date)
//                .add("end_date", end_date)
//                .add("customer_type", ctype+"")
//                .add("customer_name", customer_name)
//                .add("customer_code", customer_code)
//                .add("champ_customer_code", champ_customer_code)
//                .add("customer_company", customer_company)
//                .add("customer_phone", customer_phone)
//                .add("customer_email", customer_email)
//                .add("customer_line", customer_line)
//                .add("customer_tax_no", customer_tax_no)
//                .add("customer_note", customer_note);
//
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
//                    AddNewGroupProjectActivity.DragLat = 0.0;
//                    AddNewGroupProjectActivity.DragLng = 0.0;
//                    AddNewGroupProjectActivity.bit1 = null;
//                    AddNewGroupProjectActivity.bit2 = null;
//                    AddNewGroupProjectActivity.bit3 = null;
//                    AddNewGroupProjectActivity.bit4 = null;
//                    AddNewGroupProjectActivity.bit5 = null;
//                    AddNewGroupProjectActivity.bit6 = null;
//                    finish();
//
//                } else {
//                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
//                }
//
//            }
//        }.execute();
//    }

//    @SuppressLint("StaticFieldLeak")
//    private void postImageGroupProject(final String url) {
//        showLoading(this);
//        final RequestBody requestBody;
//        final FormBody.Builder formData = new FormBody.Builder();
//        try {
//            for (int i = 0; i < mArrayList.size(); i++) {
//                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
//                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        requestBody = formData.build();
//
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    finish();
//                } else {
//                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
//                }
//
//            }
//        }.execute();
//    }


    private Call<String> callPostChildProject(List<String> _pic) {
        return rudyService.addNewProject(
                Utils.APP_LANGUAGE,
                _pic,
                user_id,
                project_name,
                project_address,
                lat_lng,
                cateId,
                phase_id,
                units,
                unit_area,
                unit_budget,
                project_stories,
                ctype+"",
                customer_name,
                customer_code,
                champ_customer_code,
                customer_company,
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note,
                ctype_owner == 2 ? house_owner_name:"",
                ctype_owner == 2 ? house_owner_phone:"",
                ctype_owner == 2 ? house_owner_line:"",
                ctype_owner == 2 ? House_owner_note:"",
                ctype_owner == 2 ? "":project_owner_name,
                ctype_owner == 2 ? "":project_owner_phone,
                ctype_owner == 2 ? "":project_owner_line,
                ctype_owner == 2 ? "":project_owner_note,
                group_id

        );
    }

    @SuppressLint("StaticFieldLeak")
    private void postChildProject(){
        showLoading(AddNewGroupProjectActivity.this);
        if (!unit_budget.equalsIgnoreCase("")) {
            unit_budget_number = Double.parseDouble(unit_budget);
            unit_budget_number = unit_budget_number * 1000000;
        }

        if (c_id.equalsIgnoreCase("0")) {
            c_id = "";
        }

        if (c_id_owner.equalsIgnoreCase("0")) {
            c_id_owner = "";
        }

        List<String> img = new ArrayList<>();
        for (int i = 0; i < mArrayList.size(); i++) {
            img.add("data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
        }

        callPostChildProject(img).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
                if (resp.contains("200")) {
                    postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
                    postProject.setVisibility(View.VISIBLE);
                    fake_layout.setVisibility(View.GONE);
                    setCustomTag("Quick Sale");
                    projectDetail = gson.fromJson(resp, ProjectGroupDetail.class);
                    AddNewGroupProjectActivity.DragLat = 0.0;
                    AddNewGroupProjectActivity.DragLng = 0.0;
                    AddNewGroupProjectActivity.bit1 = null;
                    AddNewGroupProjectActivity.bit2 = null;
                    AddNewGroupProjectActivity.bit3 = null;
                    AddNewGroupProjectActivity.bit4 = null;
                    AddNewGroupProjectActivity.bit5 = null;
                    AddNewGroupProjectActivity.bit6 = null;

                    Intent intent = new Intent(AddNewGroupProjectActivity.this, ProjectByGroupActivity.class);
                    intent.putExtra("group_id",AddNewGroupProjectActivity.group_id);
                    startActivity(intent);

                } else {
                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(AddNewGroupProjectActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void postChildProject(final String url, final int state) {
//        showLoading(this);
//
//        ArrayList<String> picList = new ArrayList<>();
//        if (!unit_budget.equalsIgnoreCase("")) {
//            unit_budget_number = Double.parseDouble(unit_budget);
//            unit_budget_number = unit_budget_number * 1000000;
//        }
//
//        if (c_id.equalsIgnoreCase("0")) {
//            c_id = "";
//        }
//
//        if (c_id_owner.equalsIgnoreCase("0")) {
//            c_id_owner = "";
//        }
//
//
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        try {
//            for (int i = 0; i < mArrayList.size(); i++) {
//                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
//                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        if (ctype_owner == 2) {
//            formData.add("user_id", user_id)
//                    .add("project_name", project_name)
//                    .add("project_address", project_address)
//                    .add("lat_lng", lat_lng)
//                    .add("project_type_id", cateId)
//                    .add("phase_id", phase_id)
//                    .add("units", units)
//                    .add("unit_area", unit_area)
//                    .add("unit_budget", unit_budget)
//                    .add("project_stories", project_stories)
//                    .add("customer_type", ctype + "")
//                    .add("customer_name", customer_name)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_phone", customer_phone)
//                    .add("customer_email", customer_email)
//                    .add("customer_line", customer_line)
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_note", customer_note)
//                    .add("house_owner_name", house_owner_name)
//                    .add("house_owner_phone", house_owner_phone)
//                    .add("house_owner_line", house_owner_line)
//                    .add("House_owner_note", House_owner_note)
//                    .add("project_owner_name", "")
//                    .add("project_owner_phone", "")
//                    .add("project_owner_line", "")
//                    .add("project_owner_note", "")
//                    .add("project_group_id",group_id);
//        } else {
//            formData.add("user_id", user_id)
//                    .add("project_name", project_name)
//                    .add("project_address", project_address)
//                    .add("lat_lng", lat_lng)
//                    .add("project_type_id", cateId)
//                    .add("phase_id", phase_id)
//                    .add("units", units)
//                    .add("unit_area", unit_area)
//                    .add("unit_budget", unit_budget)
//                    .add("project_stories", project_stories)
//                    .add("customer_type", ctype + "")
//                    .add("customer_name", customer_name)
//                    .add("customer_phone", customer_phone)
//                    .add("customer_email", customer_email)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_line", customer_line)
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_note", customer_note)
//                    .add("house_owner_name", "")
//                    .add("house_owner_phone", "")
//                    .add("house_owner_line", "")
//                    .add("House_owner_note", "")
//                    .add("project_owner_name", project_owner_name)
//                    .add("project_owner_phone", project_owner_phone)
//                    .add("project_owner_line", project_owner_line)
//                    .add("project_owner_note", project_owner_note)
//                    .add("project_group_id",group_id);
//        }
//
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                postProject.setOnClickListener(AddNewGroupProjectActivity.this);
//                if (string.contains("200")) {
//                    postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
//                    postProject.setVisibility(View.VISIBLE);
//                    fake_layout.setVisibility(View.GONE);
//                    setCustomTag("Quick Sale");
//                    Gson gson = new Gson();
//                    projectDetail = gson.fromJson(string, ProjectGroupDetail.class);
//                    AddNewGroupProjectActivity.DragLat = 0.0;
//                    AddNewGroupProjectActivity.DragLng = 0.0;
//                    AddNewGroupProjectActivity.bit1 = null;
//                    AddNewGroupProjectActivity.bit2 = null;
//                    AddNewGroupProjectActivity.bit3 = null;
//                    AddNewGroupProjectActivity.bit4 = null;
//                    AddNewGroupProjectActivity.bit5 = null;
//                    AddNewGroupProjectActivity.bit6 = null;
//
//                    Intent intent = new Intent(AddNewGroupProjectActivity.this, ProjectByGroupActivity.class);
//                    intent.putExtra("group_id",AddNewGroupProjectActivity.group_id);
//                    startActivity(intent);
//
//                } else {
//                    showToast(AddNewGroupProjectActivity.this, getResources().getString(R.string.toast_error_pls_try_again));
////                    startActivity(new Intent(StepTwoActivity.this, StepThreeActivity.class));
//                }
//
//            }
//        }.execute();
//    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
