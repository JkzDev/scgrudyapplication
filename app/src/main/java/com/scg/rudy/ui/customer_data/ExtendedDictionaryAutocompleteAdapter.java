/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.customer_data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.ui.customer_data.filter.ExtendedDictionaryAutocompleteFilter;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.customer_data.model.ItemsItem;
import com.scg.rudy.ui.customer_data.provider.ExtendedDictionaryAutocompleteProvider;
import com.scg.rudy.ui.promotion.promotionInterface;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteFilterListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author jackie
 */
public class ExtendedDictionaryAutocompleteAdapter extends ArrayAdapter<CusModel> implements OnDynamicAutocompleteFilterListener<CusModel> {

    private List<CusModel> mListItems;
    private ExtendedDictionaryAutocompleteProvider mExtendedDictionaryAutocompleteProvider;
    private ExtendedDictionaryAutocompleteFilter mDictionaryAutocompleteFilter;
    private LayoutInflater mLayoutInflater;
    private int mLayoutId;
    private customerInterface listener;
    private int tabIndex;
    public ExtendedDictionaryAutocompleteAdapter(Context context, int textViewResourceId,customerInterface _listener,int _tabIndex) {
        super(context, textViewResourceId);
        mExtendedDictionaryAutocompleteProvider = new ExtendedDictionaryAutocompleteProvider();
        mDictionaryAutocompleteFilter = new ExtendedDictionaryAutocompleteFilter(mExtendedDictionaryAutocompleteProvider, this);
        mDictionaryAutocompleteFilter.useCache(true);
        mListItems = new ArrayList<>();
        mLayoutId = textViewResourceId;
        listener = _listener;
        tabIndex = _tabIndex;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void onDynamicAutocompleteFilterResult(Collection<CusModel> result) {
        mListItems.clear();
        mListItems.addAll(result);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mDictionaryAutocompleteFilter;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public void clear() {
        mListItems.clear();
    }

    @Override
    public CusModel getItem(int position) {
        return mListItems.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mLayoutId, null);
            viewHolder = new ViewHolder();
            viewHolder.flag = (ImageView) convertView.findViewById(R.id.flagImage);
            viewHolder.name = (TextView) convertView.findViewById(R.id.countryNameText);
            viewHolder.code = (TextView) convertView.findViewById(R.id.countryCodeText);
            viewHolder.champ_code= (TextView) convertView.findViewById(R.id.champ_code);
            viewHolder.champHead = (TextView) convertView.findViewById(R.id.champHead);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CusModel countryModel = getItem(position);
        Glide.with(getContext()).load(countryModel.getPic()).into(viewHolder.flag);
        viewHolder.name.setText(countryModel.getCustomer_name());
        viewHolder.code.setText(countryModel.getCustomer_phone());
        if(countryModel.getChamp_customer_code().isEmpty()){
            viewHolder.champ_code.setVisibility(View.GONE);
            viewHolder.champHead.setVisibility(View.GONE);
        }else{
            viewHolder.champHead.setVisibility(View.VISIBLE);
            viewHolder.champ_code.setVisibility(View.VISIBLE);
            viewHolder.champ_code.setText(countryModel.getChamp_customer_code());
        }



        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.selectCusItem(countryModel,tabIndex);
            }
        });

        return convertView;
    }

    /**
     * Holder for list item.
     */
    private static class ViewHolder {
        private ImageView flag;
        private TextView name;
        private TextView code;
        private TextView champ_code;
        private TextView champHead;

    }
}