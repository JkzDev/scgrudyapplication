
package com.scg.rudy.ui.sku_confirm.Models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ItemDetailModel {

    @SerializedName("cate_id")
    private String mCateId;
    @SerializedName("class_id")
    private String mClassId;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("description_short")
    private String mDescriptionShort;
    @SerializedName("gallery")
    private List<Object> mGallery;
    @SerializedName("id")
    private String mId;
    @SerializedName("img")
    private String mImg;
    @SerializedName("name")
    private String mName;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("price2")
    private String mPrice2;
    @SerializedName("price3")
    private String mPrice3;
    @SerializedName("promotion_detail_id")
    private String mPromotionDetailId;
    @SerializedName("recommend")
    private String mRecommend;
    @SerializedName("shop_id")
    private String mShopId;
    @SerializedName("sku_code")
    private String mSkuCode;
    @SerializedName("stock")
    private String mStock;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("unit_list")
    private List<UnitList> mUnitList;

    public String getCateId() {
        return mCateId;
    }

    public void setCateId(String cateId) {
        mCateId = cateId;
    }

    public String getClassId() {
        return mClassId;
    }

    public void setClassId(String classId) {
        mClassId = classId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescriptionShort() {
        return mDescriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        mDescriptionShort = descriptionShort;
    }

    public List<Object> getGallery() {
        return mGallery;
    }

    public void setGallery(List<Object> gallery) {
        mGallery = gallery;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImg() {
        return mImg;
    }

    public void setImg(String img) {
        mImg = img;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getPrice2() {
        return mPrice2;
    }

    public void setPrice2(String price2) {
        mPrice2 = price2;
    }

    public String getPrice3() {
        return mPrice3;
    }

    public void setPrice3(String price3) {
        mPrice3 = price3;
    }

    public String getPromotionDetailId() {
        return mPromotionDetailId;
    }

    public void setPromotionDetailId(String promotionDetailId) {
        mPromotionDetailId = promotionDetailId;
    }

    public String getRecommend() {
        return mRecommend;
    }

    public void setRecommend(String recommend) {
        mRecommend = recommend;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String shopId) {
        mShopId = shopId;
    }

    public String getSkuCode() {
        return mSkuCode;
    }

    public void setSkuCode(String skuCode) {
        mSkuCode = skuCode;
    }

    public String getStock() {
        return mStock;
    }

    public void setStock(String stock) {
        mStock = stock;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public List<UnitList> getUnitList() {
        return mUnitList;
    }

    public void setUnitList(List<UnitList> unitList) {
        mUnitList = unitList;
    }

}
