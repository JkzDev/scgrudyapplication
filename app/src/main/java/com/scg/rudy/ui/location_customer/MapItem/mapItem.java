package com.scg.rudy.ui.location_customer.MapItem;

import com.google.android.gms.maps.model.LatLng;
import net.sharewire.googlemapsclustering.ClusterItem;

public class mapItem implements ClusterItem {
    private final LatLng mPosition;
    private  String mTitle = "";
    private  String mSnippet = "";
    private  String mId = "";
    private  String mName = "";
    private  String mAddress = "";
    private  String mAppName = "";
    private  String mPin = "";
    private  String mPic = "";
    private  String Biz_id;
    private  boolean is_eyes;
    private  boolean pulled;


    public mapItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }
    public mapItem(double lat, double lng, String title) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
    }


    public mapItem(double lat,
                   double lng,
                   String title,
                   String snippet,
                   String id,
                   String name,
                   String AppName,
                   String Pin,
                   String Pic,
                   String Address,
                   String _Biz_id,
                   boolean _is_eyes,
                   boolean _pulled) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
        mId = id;
        mName = name;
        mAppName = AppName;
        mPin = Pin;
        mPic = Pic;
        mAddress = Address;
        Biz_id = _Biz_id;
        is_eyes = _is_eyes;
        pulled = _pulled;

    }

    public boolean isPulled() {
        return pulled;
    }

    public void setPulled(boolean pulled) {
        this.pulled = pulled;
    }

    public String getBiz_id() {
        return Biz_id;
    }

    public void setBiz_id(String biz_id) {
        Biz_id = biz_id;
    }

    public boolean isIs_eyes() {
        return is_eyes;
    }

    public void setIs_eyes(boolean is_eyes) {
        this.is_eyes = is_eyes;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmAppName() {
        return mAppName;
    }

    public void setmAppName(String mAppName) {
        this.mAppName = mAppName;
    }

    public String getmPin() {
        return mPin;
    }

    public void setmPin(String mPin) {
        this.mPin = mPin;
    }

    public String getmPic() {
        return mPic;
    }

    public void setmPic(String mPic) {
        this.mPic = mPic;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }


    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public double getLatitude() {
        return mPosition.latitude;
    }

    @Override
    public double getLongitude() {
        return mPosition.longitude;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }
}