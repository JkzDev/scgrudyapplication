package com.scg.rudy.ui.note_view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.base.BaseFragment;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.DrawingView;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;
import static java.lang.String.format;

/**
 * Created by DekDroidDev on 24/4/2018 AD.
 */
public class drawFragment extends BaseFragment implements View.OnClickListener {
    private DrawingView mDrawingView;
    private DrawingView mainDrawingView;
    private ImageView  eraseButton, newButton,brush_size,close_drawview;
    private Button saveButton;
    private RelativeLayout drawButton;
    private float smallBrush, mediumBrush, largeBrush;
    private ImageView currPaint;
    private ImageView skin;
    private ImageView black;
    private ImageView red;
    private ImageView green;
    private ImageView blue;
    private ImageView yellow;
    private LinearLayout brush_color;
    private int lineColorSelect = 0;
    private RelativeLayout bg_brush;
    private RelativeLayout bg_erase;
    private ImageButton next_page;
    private ImageButton pre_page;
    private TextView page;
    public static int pageTotal = 1;
    int currentPage = 1;
    private FrameLayout rootDrawLayout;
    private ArrayList<DrawingView> drawingViewArrayList;
    public static final String TAG = "drawFragment";
    ArrayList<Bitmap> bitmapArrayList;

    private RudyService rudyService;


//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }

    public static drawFragment newInstance() {
        Bundle args = new Bundle();
        drawFragment fragment = new drawFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_drawing_view, container, false);
        initView(view);
        bitmapArrayList= new ArrayList<>();
        // Getting the initial paint color.
        drawButton.setOnClickListener(this);
        eraseButton.setOnClickListener(this);
        newButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        // Set the initial brush size
        brush_size.setImageResource(R.drawable.small);
        mDrawingView.setBrushSize(smallBrush);
        close_drawview.setOnClickListener(this);
        next_page.setOnClickListener(this);
        pre_page.setOnClickListener(this);

        rudyService = ApiHelper.getClient();




        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.buttonBrush:
                // Show brush size chooser dialog
                showBrushSizeChooserDialog(v);
                break;
            case R.id.buttonErase:
                // Show eraser size chooser dialog
                showEraserSizeChooserDialog();
                break;
            case R.id.buttonNew:
                // Show new painting alert dialog
                showNewPaintingAlertDialog();
                break;
            case R.id.buttonSave:
                // Show save painting confirmation dialog.
                showSavePaintingConfirmationDialog();
                break;

            case R.id.skin :
                paintClicked(v);
                lineColorSelect = 1;
                break;
            case R.id.black :
                paintClicked(v);
                lineColorSelect = 0;
                break;
            case R.id.red :
                paintClicked(v);
                lineColorSelect = 2;
                break;
            case R.id.green :
                paintClicked(v);
                lineColorSelect = 3;
                break;
            case R.id.blue :
                lineColorSelect = 4;
                paintClicked(v);
                break;

            case R.id.yellow :
                lineColorSelect = 5;
                paintClicked(v);
                break;
            case R.id.close_drawview :
                if (getActivity() instanceof MainActivity) {
//                    mDrawingView.startNew();
//                    MainActivity activ = (MainActivity) getActivity();
//                    activ.hideView();
                }
                break;
            case R.id.next_page :
                bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
                bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                brush_color.setBackgroundColor(Color.BLACK);
                mDrawingView.setColor("BLACK");
                lineColorSelect = 0;
                mDrawingView.setErase(false);
                mDrawingView.setBrushSize(smallBrush);
                mDrawingView.setLastBrushSize(smallBrush);
                 if(currentPage ==pageTotal){
                     pageTotal++;
                     XmlPullParser parser = getResources().getXml(R.xml.drawing_view);
                     try {
                         parser.next();
                         parser.nextTag();
                     } catch (Exception e) {
                         e.printStackTrace();
                     }

                     final AttributeSet attr = Xml.asAttributeSet(parser);
                     final DrawingView drawingView = new DrawingView(getActivity(),attr);
                     drawingView.setId(currentPage);
                     drawingViewArrayList.add(drawingView);
                     rootDrawLayout.addView(drawingView);
                 }

                currentPage++;
                if(currentPage>1){
                    pre_page.setVisibility(View.VISIBLE);
                }
                for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                    if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                        if(rootDrawLayout.getChildAt(x).getId()==currentPage-1){
                            rootDrawLayout.getChildAt(x).bringToFront();

                            mDrawingView =  (DrawingView)rootDrawLayout.getChildAt(x);
                            mDrawingView.setBrushSize(smallBrush);

                        }
                    }
                }
                Log.i("next_page", " : "+currentPage);
                rootDrawLayout.invalidate();
                page.setText(format("%d/%d", currentPage, pageTotal));

                break;
            case R.id.pre_page :
                bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
                bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                brush_color.setBackgroundColor(Color.BLACK);
                mDrawingView.setColor("BLACK");
                lineColorSelect = 0;
                mDrawingView.setErase(false);
                mDrawingView.setBrushSize(smallBrush);
                mDrawingView.setLastBrushSize(smallBrush);
                currentPage--;
                if(currentPage==1){
                    pre_page.setVisibility(View.INVISIBLE);
                }else{
                    pre_page.setVisibility(View.VISIBLE);
                }

                Log.i("pre_page", " : "+currentPage);
//                rootDrawLayout.getChildAt(currentPage-1).bringToFront();
                if(currentPage>1){
                    for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                        if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                            if(rootDrawLayout.getChildAt(x).getId()==currentPage-1){
                                rootDrawLayout.getChildAt(x).bringToFront();
                                mDrawingView =  (DrawingView)rootDrawLayout.getChildAt(x);
                                mDrawingView.setBrushSize(smallBrush);
                            }
                        }
                    }
                }else{
                    for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                        if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                            if(rootDrawLayout.getChildAt(x).getId()==R.id.drawing){
                                rootDrawLayout.getChildAt(x).bringToFront();
                                mDrawingView =  (DrawingView)rootDrawLayout.getChildAt(x);
                                mDrawingView.setBrushSize(smallBrush);
                            }
                        }
                    }
                }
                page.setText(format("%d/%d", currentPage, pageTotal));
                break;
        }
    }

    public int drawViewChildCount(ViewGroup parent) {
        int count = 0;
        for (int x = 0; x < parent.getChildCount(); x++) {
            if (parent.getChildAt(x) instanceof DrawingView) {
                count++;
            }
        }
        return count;
    }


    public void paintClicked(View view) {
        if (view != currPaint) {
            // Update the color
            ImageView imageButton = (ImageView) view;
            String colorTag = imageButton.getTag().toString();
            mDrawingView.setColor(colorTag);
            brush_color.setBackgroundColor(Color.parseColor(colorTag));
            // Swap the backgrounds for last active and currently active image button.
            imageButton.setImageDrawable(getResources().getDrawable(R.drawable.pallet_pressed));
            currPaint.setImageDrawable(getResources().getDrawable(R.drawable.pallet));
            currPaint = (ImageView) view;
            mDrawingView.setErase(false);
            mDrawingView.setBrushSize(mDrawingView.getLastBrushSize());
        }
    }

    private void showBrushSizeChooserDialog(View showUnderView) {
        bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
        bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
//        final Dialog brushDialog = new Dialog(getActivity());
//        brushDialog.setContentView(R.layout.dialog_brush_size);
//        brushDialog.setTitle("Brush size:");
        final PopupWindow popup = new PopupWindow(getActivity());
        final View brushDialog = getActivity().getLayoutInflater().inflate(R.layout.dialog_brush_size, null);
        popup.setContentView(brushDialog);
        LinearLayout paintLayout = brushDialog.findViewById(R.id.paint_colors);
        // 0th child is white color, so selecting first child to give black as initial color.
        currPaint = (ImageView) paintLayout.getChildAt(lineColorSelect);
        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.pallet_pressed));

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());

        skin = brushDialog.findViewById(R.id.skin);
        black = brushDialog.findViewById(R.id.black);
        red = brushDialog.findViewById(R.id.red);
        green = brushDialog.findViewById(R.id.green);
        blue = brushDialog.findViewById(R.id.blue);
        yellow = brushDialog.findViewById(R.id.yellow);

        skin.setOnClickListener(drawFragment.this);
        black.setOnClickListener(drawFragment.this);
        red.setOnClickListener(drawFragment.this);
        green.setOnClickListener(drawFragment.this);
        blue.setOnClickListener(drawFragment.this);
        yellow.setOnClickListener(drawFragment.this);

        ImageButton smallBtn = brushDialog.findViewById(R.id.small_brush);
        smallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(smallBrush);
                mDrawingView.setLastBrushSize(smallBrush);
                brush_size.setImageResource(R.drawable.small);
//                popup.dismiss();
            }
        });
        ImageButton mediumBtn = brushDialog.findViewById(R.id.medium_brush);
        mediumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(mediumBrush);
                mDrawingView.setLastBrushSize(mediumBrush);
                brush_size.setImageResource(R.drawable.medium);
//                popup.dismiss();
            }
        });

        ImageButton largeBtn = brushDialog.findViewById(R.id.large_brush);
        largeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(largeBrush);
                mDrawingView.setLastBrushSize(largeBrush);
                brush_size.setImageResource(R.drawable.large);
//                popup.dismiss();
            }
        });
        mDrawingView.setErase(false);
        popup.showAsDropDown(showUnderView, -(int) getActivity().getResources().getDimension(R.dimen._10sdp), 0, Gravity.LEFT);

    }

    private void showEraserSizeChooserDialog() {
        bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
        mDrawingView.setErase(true);
        mDrawingView.setBrushSize(getResources().getDimension(R.dimen._20sdp));
//        final Dialog brushDialog = new Dialog(getActivity());
//        brushDialog.setTitle("Eraser size:");
//        brushDialog.setContentView(R.layout.dialog_brush_size);
//        ImageButton smallBtn = brushDialog.findViewById(R.id.small_brush);
//        smallBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDrawingView.setErase(true);
//                mDrawingView.setBrushSize(smallBrush);
//                brushDialog.dismiss();
//            }
//        });
//        ImageButton mediumBtn = brushDialog.findViewById(R.id.medium_brush);
//        mediumBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDrawingView.setErase(true);
//                mDrawingView.setBrushSize(mediumBrush);
//                brushDialog.dismiss();
//            }
//        });
//        ImageButton largeBtn = brushDialog.findViewById(R.id.large_brush);
//        largeBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDrawingView.setErase(true);
//                mDrawingView.setBrushSize(largeBrush);
//                brushDialog.dismiss();
//            }
//        });
//        brushDialog.show();
    }

    private void showNewPaintingAlertDialog() {
        AlertDialog.Builder newDialog = new AlertDialog.Builder(getActivity());
        newDialog.setTitle("New drawing");
        newDialog.setMessage("Start new drawing (you will lose the current drawing)?");
        newDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                            rootDrawLayout.removeAllViews();
                            rootDrawLayout.addView(mainDrawingView);
                            mDrawingView =  mainDrawingView;
                            bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
                            bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                            brush_color.setBackgroundColor(Color.BLACK);
                            mDrawingView.setColor("BLACK");
                            lineColorSelect = 0;
                            mDrawingView.setErase(false);
                            mDrawingView.setBrushSize(smallBrush);
                            mDrawingView.setLastBrushSize(smallBrush);
                            mDrawingView.startNew();
                            currentPage = 1;
                            pageTotal = 1;
                            page.setText(format("%d/%d", currentPage, pageTotal));
                            pre_page.setVisibility(View.INVISIBLE);

                dialog.dismiss();
            }
        });
        newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        newDialog.show();
    }

    private void showSavePaintingConfirmationDialog() {
        AlertDialog.Builder saveDialog = new AlertDialog.Builder(getActivity());
        saveDialog.setTitle("Save drawing");
        saveDialog.setMessage("Save drawing to device Gallery?");
        saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                bitmapArrayList = new ArrayList<>();
                //save drawing
                for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                    if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                        mDrawingView =  (DrawingView)rootDrawLayout.getChildAt(x);

                        mDrawingView.setDrawingCacheEnabled(true);
                        String imgSaved = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), mDrawingView.getDrawingCache(),
                                UUID.randomUUID().toString() + ".png", "drawing");


                        if(x==rootDrawLayout.getChildCount()-1){
                            if (imgSaved != null) {
                                Uri uri = Uri.parse(imgSaved);
                                Bitmap bmp = null;
                                    try {
                                        InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
                                        bmp = BitmapFactory.decodeStream(inputStream);
                                        inputStream.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bmp);
                                bitmapArrayList.add(bmp);
                                if(!Utils.project_id.equalsIgnoreCase("")){
//                                    addNote(ApiEndPoint.getAdd_note(Utils.APP_LANGUAGE,Utils.project_id),bmp);
                                    addNote(bmp);
                                }else{
                                    Toast savedToast = Toast.makeText(getActivity(), "Drawing saved to Gallery!", Toast.LENGTH_LONG);
                                    savedToast.show();
                                    rootDrawLayout.removeAllViews();
                                    rootDrawLayout.addView(mainDrawingView);
                                    mDrawingView =  mainDrawingView;
                                    bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
                                    bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                                    brush_color.setBackgroundColor(Color.BLACK);
                                    mDrawingView.setColor("BLACK");
                                    lineColorSelect = 0;
                                    mDrawingView.setErase(false);
                                    mDrawingView.setBrushSize(smallBrush);
                                    mDrawingView.setLastBrushSize(smallBrush);
                                    mDrawingView.startNew();
                                    currentPage = 1;
                                    pageTotal = 1;
                                    page.setText(format("%d/%d", currentPage, pageTotal));
                                    pre_page.setVisibility(View.INVISIBLE);
                                    close_drawview.performClick();
                                }


                            } else {
                                Toast unsavedToast = Toast.makeText(getActivity(), "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
                                unsavedToast.show();
                            }
                        }
                        // Destroy the current cache.
                        mDrawingView.destroyDrawingCache();
                    }
                }
//                chkThred(bitmapArrayList);


            }
        });
        saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        saveDialog.show();
    }



//    public void chkThred(final ArrayList<Bitmap> bitmapArrayList){
//        Thread thread = new Thread()
//        {
//            @Override
//            public void run() {
//                try {
//                    for (int i = 0; i < bitmapArrayList.size(); i++) {
//                        sleep(1000);
//
//                    }
//
//                } catch (InterruptedException e) {
//                    Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
//                }
//            }
//        };
//
//        thread.start();
//    }



    private Call<String> callAddNote(String _note) {
        return rudyService.addNote(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                _note
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void addNote(Bitmap _bitmap){
        showLoading();
        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(_bitmap);
        callAddNote(image64).enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Toast savedToast = Toast.makeText(getActivity(), "Drawing saved to you last project!", Toast.LENGTH_LONG);
                    savedToast.show();
                    rootDrawLayout.removeAllViews();
                    rootDrawLayout.addView(mainDrawingView);
                    mDrawingView =  mainDrawingView;
                    bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
                    bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
                    brush_color.setBackgroundColor(Color.BLACK);
                    mDrawingView.setColor("BLACK");
                    lineColorSelect = 0;
                    mDrawingView.setErase(false);
                    mDrawingView.setBrushSize(smallBrush);
                    mDrawingView.setLastBrushSize(smallBrush);
                    mDrawingView.startNew();
                    currentPage = 1;
                    pageTotal = 1;
                    page.setText(format("%d/%d", currentPage, pageTotal));
                    pre_page.setVisibility(View.INVISIBLE);
                    close_drawview.performClick();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void addNote(final String url, Bitmap bitmap) {
//        showLoading();
//        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bitmap);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("note", image64)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                        Toast savedToast = Toast.makeText(getActivity(), "Drawing saved to you last project!", Toast.LENGTH_LONG);
//                        savedToast.show();
//                        rootDrawLayout.removeAllViews();
//                        rootDrawLayout.addView(mainDrawingView);
//                        mDrawingView =  mainDrawingView;
//                        bg_brush.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_step4));
//                        bg_erase.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
//                        brush_color.setBackgroundColor(Color.BLACK);
//                        mDrawingView.setColor("BLACK");
//                        lineColorSelect = 0;
//                        mDrawingView.setErase(false);
//                        mDrawingView.setBrushSize(smallBrush);
//                        mDrawingView.setLastBrushSize(smallBrush);
//                        mDrawingView.startNew();
//                        currentPage = 1;
//                        pageTotal = 1;
//                        page.setText(format("%d/%d", currentPage, pageTotal));
//                        pre_page.setVisibility(View.INVISIBLE);
//                        close_drawview.performClick();
//                    }
//            }
//        }.execute();
//    }



    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initView(View v) {
        drawingViewArrayList = new ArrayList<>();
//        int count = attr.getAttributeCount();
        brush_size = v.findViewById(R.id.brush_size);
        brush_color = v.findViewById(R.id.brush_color);
        close_drawview  = v.findViewById(R.id.close_drawview);
        bg_brush =  v.findViewById(R.id.bg_brush);
        bg_erase =  v.findViewById(R.id.bg_erase);
        next_page =  v.findViewById(R.id.next_page);
        pre_page =  v.findViewById(R.id.pre_page);
        page =  v.findViewById(R.id.page);
        rootDrawLayout = v.findViewById(R.id.rootDrawLayout);
        mDrawingView = v.findViewById(R.id.drawing);
        mainDrawingView = mDrawingView;
        drawButton = v.findViewById(R.id.buttonBrush);
        eraseButton = v.findViewById(R.id.buttonErase);
        newButton = v.findViewById(R.id.buttonNew);
        saveButton = v.findViewById(R.id.buttonSave);

    }


    @Override
    protected void setUp(View view) {

    }
}

