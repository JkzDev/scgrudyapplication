package com.scg.rudy.ui.deal_by_phase;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.PushProductModel;
import com.scg.rudy.model.pojo.choose_sku.ChooseSku;
import com.scg.rudy.model.pojo.dealbyproject.catalog.Catalog;
import com.scg.rudy.model.pojo.dealbyproject.class_by_phase.ClassByPhase;
import com.scg.rudy.model.pojo.dealbyproject.group.GroupProject;
import com.scg.rudy.model.pojo.dealbyproject.phaseList.ItemsItem;
import com.scg.rudy.model.pojo.dealbyproject.phaseList.PhaseList;
import com.scg.rudy.model.pojo.phase.CatesItem;
import com.scg.rudy.model.pojo.phase.ClassesItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.catalogs.CatalogsActivity;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.choose_sku.SearchSKUitemsActivity;
import com.scg.rudy.ui.deal_by_phase.adapter.ClassByPhaseRecyclerAdapter;
import com.scg.rudy.ui.deal_by_phase.adapter.GroupRecyclerAdapter;
import com.scg.rudy.ui.deal_by_phase.adapter.SubphaseRecyclerAdapter;
import com.scg.rudy.ui.dealbyproject.DealByProjectActivity;
import com.scg.rudy.ui.favorite.FavoriteDetailActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.promotion.PromotionActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getGetBasket;
import static com.scg.rudy.data.remote.ApiEndPoint.getGetFav;
import static com.scg.rudy.ui.sku_confirm.SkuConfirmActivity.sub_class_name;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class DealByPhaseActivity extends BaseActivity implements View.OnClickListener,byPhaseInterface {
    private RelativeLayout cart;
    private ImageView clsBtn;
    private RelativeLayout btnP1;
    private TextView txtP1;
    private RelativeLayout btnP2;
    private TextView txtP2;
    private RelativeLayout btnP3;
    private TextView txtP3;
    private RelativeLayout btnP4;
    private TextView txtP4;
    private RelativeLayout btnP5;
    private TextView txtP5;
    private RelativeLayout btnP6;
    private TextView txtP6;
    private RelativeLayout btnP7;
    private TextView txtP7;
    private RelativeLayout btnP8;
    private TextView txtP8;
    private RelativeLayout btnP9;
    private TextView txtP9;
    private RelativeLayout btnP10;
    private TextView txtP10;
    private LinearLayout layoutSubAdd;
    private TextView noSubcate;
    private LinearLayout layoutProductAdd;
    private TextView noProduct;
    private LinearLayout selectProductAdd;
    private TextView noProductSelect;
    private String currentPhase = "2";
    private String user_id = "";
    private LayoutInflater layoutInflater;
    private PushProductModel.Items.Phase phase;
    private TextView subPhaseName;
    private int subPhaseClick = 0;
    private int subProductClick = 0;
    private Bundle extras;
    private String phass;
    public static String QUICKSALE = "1";
    private String projectId,projectTypeId;
    public static String transaction_id = "";
    private ImageView searchSku;
    private RelativeLayout badge;
    private TextView numberBadge;
    private RelativeLayout fav;
    private RelativeLayout badge_fav;
    private TextView number_fav;
    private ScrollView scroll_group,scroll_sub;
    public static int basketSize = 0;
    private Catalog catalogList;

    private RudyService rudyService;
    private int totalPromotion = 0;
    private String phaseId;
    private String subPhaseId;
    private String cateID;
    public static final String TAG = "DealByPhaseFragment";
    public static boolean print = false;
    private ImageView catalog;


    private RelativeLayout promotion;
    private TextView total_pro;
    private RecyclerView subphaseRecycler;
    private RecyclerView groupRecycler;
    private RecyclerView classRecycler;
    private String groupId;
    public String phase_id;
    public String classId;
    private RelativeLayout btnSalePhase;
    private RelativeLayout btnSaleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_by_phase);
        ButterKnife.bind(this);

        basketSize = 0;

        initView();


        findViewById(R.id.bhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(DealByPhaseActivity.this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });

        extras = getIntent().getExtras();
        if (extras!=null) {
//            shop = extras.getString("shop_id", "1");
            phass = extras.getString("phase", "1");
            transaction_id = extras.getString("transaction_id", "");
            projectId = extras.getString("projectId", "1");
            projectTypeId = extras.getString("projectTypeId", "1");
            phaseId = extras.getString("phaseId", "1");

        } else {
            phass = "1";
            projectId = "1";
            projectTypeId = "1";
        }

        getUserData();
        clsBtn.setOnClickListener(this);
        btnSaleType.setOnClickListener(this);
        loadPhaseList();
    }

    private void loadPhaseList() {
        showLoading(this);
        getPhaseList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Catalog results  = gson.fromJson(resp, Catalog.class);
                    showTypeData(results);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

    private void loadSubPhaseList(String phaseId) {
        showLoading(this);
        getSubPhaseList(phaseId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    PhaseList results  = gson.fromJson(resp, PhaseList.class);
                    setSubPhase(results.getItems());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

    private void loadGroupProjectList(String subPhaseId) {
        showLoading(this);
        getGroupProjectList(subPhaseId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    GroupProject results  = gson.fromJson(resp, GroupProject.class);
                    setGroup(results.getItems());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }


    private void loadClassList(String groupId) {
        showLoading(this);
        getClassByPhase(groupId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ClassByPhase results  = gson.fromJson(resp, ClassByPhase.class);
                    setClassByPhase(results.getItems());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> getPhaseList() {
        return rudyService.catalogues(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                projectTypeId
        );
    }


    private Call<String> getSubPhaseList(String phaseId) {
        this.phaseId = phaseId;
        return rudyService.phaseList(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                phaseId
        );
    }

    private Call<String> getGroupProjectList(String subPhaseId) {
        this.subPhaseId =subPhaseId;
        return rudyService.groupProject(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                subPhaseId
        );
    }

    private Call<String> getClassByPhase(String groupId) {
        this.groupId = groupId;
        return rudyService.classByPhase(
                Utils.APP_LANGUAGE,
                projectId,
                groupId
        );
    }


    private ClassByPhase fetchClassProjecResults(Response<ClassByPhase> response) {
        return response.body();
    }



    private PhaseList fetchSubPhaseListResults(Response<PhaseList> response) {
        return response.body();
    }


    private GroupProject fetchGroupProjectResults(Response<GroupProject> response) {
        return response.body();
    }


    private Catalog fetchCatalogResults(Response<Catalog> response) {
        return response.body();
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            LogException("getUserData" ,e);
            e.printStackTrace();
        }
    }

    public void initView(){
        rudyService = ApiHelper.getClient();
        layoutInflater = LayoutInflater.from(this);
        cart = findViewById(R.id.cart);
        clsBtn = findViewById(R.id.cls_btn);
        btnP1 = findViewById(R.id.btn_p1);
        txtP1 = findViewById(R.id.txt_p1);
        btnP2 = findViewById(R.id.btn_p2);
        txtP2 = findViewById(R.id.txt_p2);
        btnP3 = findViewById(R.id.btn_p3);
        txtP3 = findViewById(R.id.txt_p3);
        btnP4 = findViewById(R.id.btn_p4);
        txtP4 = findViewById(R.id.txt_p4);
        btnP5 = findViewById(R.id.btn_p5);
        txtP5 = findViewById(R.id.txt_p5);
        btnP6 = findViewById(R.id.btn_p6);
        txtP6 = findViewById(R.id.txt_p6);
        btnP7 = findViewById(R.id.btn_p7);
        txtP7 = findViewById(R.id.txt_p7);
        btnP8 = findViewById(R.id.btn_p8);
        txtP8 = findViewById(R.id.txt_p8);
        btnP9 = findViewById(R.id.btn_p9);
        txtP9 = findViewById(R.id.txt_p9);
        btnP10 = findViewById(R.id.btn_p10);
        txtP10 = findViewById(R.id.txt_p10);
        scroll_group = findViewById(R.id.scroll_group);
        scroll_sub= findViewById(R.id.scroll_sub);

        layoutSubAdd = findViewById(R.id.layout_sub_add);
        noSubcate = findViewById(R.id.no_subcate);
        layoutProductAdd = findViewById(R.id.layout_product_add);
        noProduct = findViewById(R.id.no_product);
        selectProductAdd = findViewById(R.id.select_product_add);
        noProductSelect = findViewById(R.id.no_product_select);
        numberBadge = findViewById(R.id.number_badge);
        searchSku = findViewById(R.id.search_sku);
        badge = findViewById(R.id.badge);
        fav = findViewById(R.id.fav);
        badge_fav = findViewById(R.id.badge_fav);
        number_fav = findViewById(R.id.number_fav);
        catalog =  findViewById(R.id.catalog);
        promotion = findViewById(R.id.promotion);
        total_pro = findViewById(R.id.total_pro);

        cart.setOnClickListener(this);
        fav.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        btnP1.setOnClickListener(this);
        btnP2.setOnClickListener(this);
        btnP3.setOnClickListener(this);
        btnP4.setOnClickListener(this);
        btnP5.setOnClickListener(this);
        btnP6.setOnClickListener(this);
        btnP7.setOnClickListener(this);
        btnP8.setOnClickListener(this);
        btnP9.setOnClickListener(this);
        btnP10.setOnClickListener(this);
        searchSku.setOnClickListener(this);
        catalog.setOnClickListener(this);


        subphaseRecycler = (RecyclerView) findViewById(R.id.subphase_recycler);
        groupRecycler = (RecyclerView) findViewById(R.id.group_recycler);
        classRecycler = (RecyclerView) findViewById(R.id.class_recycler);
        btnSalePhase = (RelativeLayout) findViewById(R.id.btn_sale_phase);
        btnSaleType = (RelativeLayout) findViewById(R.id.btn_sale_type);
    }
   
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadPromotion();


        if(Utils.getPrefer(this,"print").length()>0){
           finish();
        }
        else{
            updateTranId();
        }
    }

    private Call<String> callGetFAV() {
        return rudyService.getFAV(
                ApiEndPoint.LANG,
                project_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getFAV(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callGetFAV().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                String resp = response.body();
                if(!transaction_id.equalsIgnoreCase("")){

                    if (resp.contains("200")) {
                        setFavIcon(resp);
                    }
                    else{
                        BaseActivity.favItemSize = 0;
                        badge_fav.setVisibility(View.GONE);
                    }
                    getBasket();
                }else{
                    if (resp.contains("200")) {
                        setFavIcon(resp);
                    }
                    else{
                        BaseActivity.favItemSize = 0;
                        badge_fav.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });

    }

//    @SuppressLint("StaticFieldLeak")
//    private void getFAV(final String url) {
//        Log.i("", "Customer: " + url);
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if(!transaction_id.equalsIgnoreCase("")){
//
//                    if (string.contains("200")) {
//                        setFavIcon(string);
//                    }
//                    else{
//                        BaseActivity.favItemSize = 0;
//                        badge_fav.setVisibility(View.GONE);
//                    }
//                    getBasket(getGetBasket(Utils.APP_LANGUAGE,transaction_id));
//                }else{
//                    if (string.contains("200")) {
//                        setFavIcon(string);
//                    }
//                    else{
//                        BaseActivity.favItemSize = 0;
//                        badge_fav.setVisibility(View.GONE);
//                    }
//                }
//
//            }
//        }.execute();
//    }

    private Call<String> callUpdateTranId(){
        return rudyService.getTransactionId(
                ApiEndPoint.LANG,
                Utils.project_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updateTranId(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callUpdateTranId().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                getFAV();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONArray array = object.getJSONArray("items");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject itemObject = array.getJSONObject(i);
                            if(itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")){
                                transaction_id = itemObject.optString("transaction_id");
                            }
                        }
                    } catch (JSONException e) {
                        LogException(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+Utils.project_id ,e);
                        e.printStackTrace();
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void updateTranId() {
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+Utils.project_id);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                getFAV(getGetFav(Utils.APP_LANGUAGE, Utils.project_id));
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONArray array = object.getJSONArray("items");
//                        for (int i = 0; i < array.length(); i++) {
//                            JSONObject itemObject = array.getJSONObject(i);
//                            if(itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")){
//                                transaction_id = itemObject.optString("transaction_id");
//                            }
//                        }
//                    } catch (JSONException e) {
//                        LogException(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+Utils.project_id ,e);
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }.execute();
//    }

    private Call<String> callGetBasket() {
        return rudyService.getTransectionDetail(
                ApiEndPoint.LANG,
                transaction_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getBasket(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callGetBasket().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                String resp = response.body();
                if (resp.contains("200")) {
                    badge.setVisibility(View.VISIBLE);
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONObject itemObj = object.getJSONObject("items");
                        JSONArray itemArray = itemObj.getJSONArray("pr");
                        basketSize = itemArray.length();
                        if(basketSize>0){
                            numberBadge.setText(basketSize + "");
                        }else{
                            badge.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        LogException(resp,e);
                        e.printStackTrace();
                    }
                }else{
                    basketSize = 0;
                    badge.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void getBasket(final String url) {
//        Log.i("getBasket ", " : " + url);
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return  Utils.getData(url);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if (string.contains("200")) {
//                    badge.setVisibility(View.VISIBLE);
//                    setBasket(url,string);
//                }else{
//                    basketSize = 0;
//                    badge.setVisibility(View.GONE);
//                }
//            }
//        }.execute();
//    }

//    public void setBasket(String url,String  items){
//        try {
//            JSONObject object = new JSONObject(items);
//            JSONObject itemObj = object.getJSONObject("items");
//            JSONArray itemArray = itemObj.getJSONArray("pr");
//            basketSize = itemArray.length();
//            if(basketSize>0){
//                numberBadge.setText(basketSize + "");
//            }else{
//                badge.setVisibility(View.GONE);
//            }
//        } catch (JSONException e) {
//            LogException(url,e);
//            e.printStackTrace();
//        }
//
//    }

    public void setFavIcon(String  items){
        try {
            JSONObject object = new JSONObject(items);
            JSONArray itemArray = object.getJSONArray("items");
            BaseActivity.favItemSize = itemArray.getJSONObject(0).getInt("total")+itemArray.getJSONObject(1).getInt("total");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(BaseActivity.favItemSize>0){
            badge_fav.setVisibility(View.VISIBLE);
            number_fav.setText(BaseActivity.favItemSize+"");
        }else{
            badge_fav.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == cart) {
            if(basketSize>0){
                TransactionDetailActivity.productId = "";
                TransactionDetailActivity.isDelete = false;
                Intent intent = new Intent(this, TransactionDetailActivity.class);
                TransactionDetailActivity.from_fav = 0;
                intent.putExtra("transaction_id",transaction_id);
                startActivity(intent);

            }else{
                showToast(this,getResources().getString(R.string.toast_pls_add_product));
            }
        }

        if(v==fav){
            if(BaseActivity.favItemSize>0){
                TransactionDetailActivity.from_fav = 1;
                FavoriteDetailActivity.productId = "";
                FavoriteDetailActivity.isDelete = false;
                Intent intent = new Intent(this, FavoriteDetailActivity.class);
                intent.putExtra("transaction_id",transaction_id);
                startActivity(intent);
            }else{
                showToast(this,getResources().getString(R.string.toast_pls_add_product_interested));
            }
        }

        if (v == btnP1) {
            phaseClick(btnP1, txtP1, "1");
        }
        if (v == btnP2) {
            phaseClick(btnP2, txtP2, "2");
        }

        if (v == btnP3) {
            phaseClick(btnP3, txtP3, "3");
        }
        if (v == btnP4) {
            phaseClick(btnP4, txtP4, "4");
        }
        if (v == btnP5) {
            phaseClick(btnP5, txtP5, "5");

        }
        if (v == btnP6) {
            phaseClick(btnP6, txtP6, "6");

        }
        if (v == btnP7) {
            phaseClick(btnP7, txtP7, "7");

        }
        if (v == btnP8) {
            phaseClick(btnP8, txtP8, "8");

        }
        if (v == btnP9) {
            phaseClick(btnP9, txtP9, "9");

        }
        if (v == btnP10) {
            phaseClick(btnP10, txtP10, "10");
        }
        if (v == searchSku) {
//            showToast(getBaseActivity()," COMMING SOON");
            Intent intent = new Intent(this, SearchSKUitemsActivity.class);
            intent.putExtra("shopId", shopID);
            intent.putExtra("phase_id", "");
            startActivity(intent);
        }

        if(v==catalog){
            Intent intent = new Intent(this, CatalogsActivity.class);
            startActivity(intent);
        }

        if(v==btnSaleType){

            Intent intent = new Intent(this, DealByProjectActivity.class);
            intent.putExtra("projectId", projectId);
            intent.putExtra("projectTypeId", projectTypeId);
            intent.putExtra("phaseId", phaseId);

            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

            finish();

        }
    }

    public void showTypeData(Catalog catalog) { //TODO
        layoutSubAdd.removeAllViews();
        layoutProductAdd.removeAllViews();
        catalogList = catalog;
        for (int i = 0; i < catalogList.getItems().size(); i++) {
            RelativeLayout view = findViewById(this.getResources().getIdentifier("max" + (i + 1), "id", this.getPackageName()));
            view.setVisibility(View.VISIBLE);
            if(catalogList.getItems().get(i).getId().equalsIgnoreCase(phass)){
                phass = String.valueOf(i+1);
            }
        }
        if (phass.equalsIgnoreCase("1")) {
            phaseClick(btnP1, txtP1, phass);
        } else if (phass.equalsIgnoreCase("2")) {
            phaseClick(btnP2, txtP2, phass);
        } else if (phass.equalsIgnoreCase("3")) {
            phaseClick(btnP3, txtP3, phass);
        } else if (phass.equalsIgnoreCase("4")) {
            phaseClick(btnP4, txtP4, phass);
        } else if (phass.equalsIgnoreCase("5")) {
            phaseClick(btnP5, txtP5, phass);
        } else if (phass.equalsIgnoreCase("6")) {
            phaseClick(btnP6, txtP6, phass);
        } else if (phass.equalsIgnoreCase("7")) {
            phaseClick(btnP7, txtP7, phass);
        } else if (phass.equalsIgnoreCase("8")) {
            phaseClick(btnP8, txtP8, phass);
        } else if (phass.equalsIgnoreCase("9")) {
            phaseClick(btnP9, txtP9, phass);
        } else  {
            phaseClick(btnP1, txtP1, phass);
        }
    }

    public void phaseClick(RelativeLayout bgLayout, TextView phaseTxt, String phase) {
//        showLoading(this);
        layoutSubAdd.removeAllViews();
        layoutProductAdd.removeAllViews();
        selectProductAdd.removeAllViews();

        for (int i = 0; i < 10; i++) {
            final RelativeLayout view = findViewById(this.getResources().getIdentifier("btn_p" + (i + 1), "id", this.getPackageName()));
            final TextView text = findViewById(this.getResources().getIdentifier("txt_p" + (i + 1), "id", this.getPackageName()));

            view.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            text.setTextColor(getResources().getColor(android.R.color.black));
            text.setText(String.valueOf(i + 1));
        }

        phaseTxt.setTextColor(getResources().getColor(R.color.white));
        bgLayout.setBackgroundColor(getResources().getColor(R.color.txt_color_login));
        if(phase.equalsIgnoreCase("0")){
            phase = "1";
        }
        int pInt = Integer.parseInt(phase);
        SelectSku();
        String sub[] = catalogList.getItems().get(pInt-1).getName().toString().split("\\[");
        if(pInt>0){
            phaseTxt.setText(/*"เฟส "+(pInt)+" "+*/sub[0]);
            phaseTxt.setTextColor(getResources().getColor(R.color.white));
            loadSubPhaseList(catalogList.getItems().get(pInt-1).getId());

        }else{
            phaseTxt.setText(catalogList.getItems().get(0).getDetail());
            phaseTxt.setTextColor(getResources().getColor(R.color.white));
            loadSubPhaseList(catalogList.getItems().get(0).getId());
        }
    }

    public void SelectSku() {
        selectProductAdd.removeAllViews();
        final View view = layoutInflater.inflate(R.layout.sale_product_nodata, layoutProductAdd, false);
        TextView text = view.findViewById(R.id.no_product);
        text.setText(getResources().getString(R.string.loading_data));
        selectProductAdd.addView(view);
    }

    public void setGroup(List<com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem> items) {
        cateID = items.get(0).getId();
        GroupRecyclerAdapter adapter = new GroupRecyclerAdapter(this, items, this);
        groupRecycler.setLayoutManager(new LinearLayoutManager(this));
        groupRecycler.setItemAnimator(new DefaultItemAnimator());
        groupRecycler.setAdapter(adapter);
        loadClassList(cateID);
    }

    public void setClassByPhase(List<com.scg.rudy.model.pojo.dealbyproject.class_by_phase.ItemsItem> items) {
        classId = items.get(0).getId();
        ClassByPhaseRecyclerAdapter adapter = new ClassByPhaseRecyclerAdapter(this, items, this);
        classRecycler.setLayoutManager(new LinearLayoutManager(this));
        classRecycler.setItemAnimator(new DefaultItemAnimator());

        Log.i(TAG, ":::::: setClassByPhase: ::::::" );
        classRecycler.setAdapter(adapter);
    }


    public void setSubPhase(List<ItemsItem> items) {
        phase_id = items.get(0).getId();
        SubphaseRecyclerAdapter adapter = new SubphaseRecyclerAdapter(this, items,this);
        subphaseRecycler.setLayoutManager(new LinearLayoutManager(this));
        subphaseRecycler.setItemAnimator(new DefaultItemAnimator());
        subphaseRecycler.setAdapter(adapter);
        loadGroupProjectList(phase_id);

    }

    public void subPhaseChild(LinearLayout view) {
        int childcount = view.getChildCount();
        for (int i = 0; i < childcount; i++) {
            View v = view.getChildAt(i);
            if (v.getId() == subPhaseClick) {
                phase_id = String.valueOf(v.getId());
                Log.i("subPhasectClick", " : " + String.valueOf(v.getId()));
                (v.findViewById(R.id.sub_phase_name)).setBackgroundColor(getResources().getColor(R.color.txt_color_login));
                ((TextView) v.findViewById(R.id.sub_phase_name)).setTextColor(Color.WHITE);

            } else {
                (v.findViewById(R.id.sub_phase_name)).setBackgroundColor(Color.TRANSPARENT);
                ((TextView) v.findViewById(R.id.sub_phase_name)).setTextColor(Color.BLACK);
            }
        }
        hideLoading();
    }

    private void loadPromotion() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        callPromotionApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
               hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ChooseSku results  = gson.fromJson(resp, ChooseSku.class);
                    totalPromotion = Integer.parseInt(results.getTotal());

                    if(totalPromotion==0){
                        promotion.setVisibility(View.GONE);
                    }else{
                        total_pro.setText(String.valueOf(totalPromotion));
                    }

                    promotion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(DealByPhaseActivity.this, PromotionActivity.class);
                            intent.putExtra("projectId", projectId);
                            intent.putExtra("phase_id", phase_id);
                            startActivity(intent);
                        }
                    });

                    if(groupId!=null){
                        loadClassList(groupId);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                if(groupId!=null){
                    loadClassList(groupId);
                }
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

    private ChooseSku fetchResults(Response<ChooseSku> response) {
        ChooseSku skuItem = response.body();

        return skuItem;
    }

    private Call<String> callPromotionApi() {
        return rudyService.callPromoList(
                Utils.APP_LANGUAGE,
                shopID,
                "promotion",
                "",
                projectId,
                "",
                String.valueOf(1),
                "",
                "",
                ""
        );
//        return null;
    }

    public void setProduct(final List<CatesItem>  detailArrayList) {
        subPhaseChild(layoutSubAdd);
        layoutProductAdd.removeAllViews();
        selectProductAdd.removeAllViews();

        Typeface bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        Typeface reg = Typeface.createFromAsset(this.getAssets(),  getResources().getString(R.string.regular));
//        View view = LayoutInflater.from(this).inflate(R.layout.project_item,layoutAdd);
        if(detailArrayList.size()>0){
            for (int i = 0; i < detailArrayList.size(); i++) {
                if (!detailArrayList.get(i).getId().equals("")) {
                    final View view = layoutInflater.inflate(R.layout.sale_product_item, layoutProductAdd, false);
                    TextView status = view.findViewById(R.id.status);
                    TextView total = view.findViewById(R.id.total);
                    TextView product_name = view.findViewById(R.id.product_name);
                    ImageView image_item = view.findViewById(R.id.image_item);
                    if (detailArrayList.get(i).getImg().length() > 0) {
                        Glide.with(this)
                                .load(detailArrayList.get(i).getImg())
                                .into(image_item);
                    }
                    final int finalI = i;
                    status.setText(getResources().getString(R.string.reason));
                    total.setText(detailArrayList.get(finalI).getId());
                    product_name.setText(detailArrayList.get(finalI).getName());
                    product_name.setTypeface(bold);
                    view.setId(Integer.parseInt(detailArrayList.get(i).getId()));
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            subProductClick = Integer.parseInt(detailArrayList.get(finalI).getId());
                            selectCategorie(detailArrayList.get(finalI).getClasses());

                        }
                    });

                    subProductClick = Integer.parseInt(detailArrayList.get(0).getId());
                    selectCategorie(detailArrayList.get(0).getClasses());
                    layoutProductAdd.addView(view, finalI);
                    Animation animation = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.slide_in_bottom);
                    view.startAnimation(animation);

                } else {
                    final View view = layoutInflater.inflate(R.layout.sale_product_nodata, layoutProductAdd, false);
                    ((TextView) view.findViewById(R.id.no_product)).setTextColor(Color.WHITE);
                    final int finalI = i;
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                    linearLayout.addView(customView);
                            layoutProductAdd.addView(view, finalI);
                            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_bottom);
                            view.startAnimation(animation);
                        }
                    }, 500);

                }
            }
            scroll_group.fullScroll(View.FOCUS_UP);
//            scroll_sub.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    // Ready, move up
//
//                }
//            });

        }else{
            layoutProductAdd.removeAllViews();
            final View view = layoutInflater.inflate(R.layout.sale_product_nodata, layoutProductAdd, false);
            TextView text = view.findViewById(R.id.no_product);
            text.setText(getResources().getString(R.string.no_stock));
            layoutProductAdd.addView(view);

            selectProductAdd.removeAllViews();
            final View view2 = layoutInflater.inflate(R.layout.sale_product_nodata, selectProductAdd, false);
            TextView text2 = view2.findViewById(R.id.no_product);
            text2.setText(getResources().getString(R.string.no_stock));
            selectProductAdd.addView(view2);
        }

    }

    public void selectCategorie(List<ClassesItem> classes) {
        selectProductAdd.removeAllViews();
        if (classes.size()>0) {
            setSelectProduct(classes);
        } else {
            final View view = layoutInflater.inflate(R.layout.sale_product_nodata, layoutProductAdd, false);
            TextView text = view.findViewById(R.id.no_product);
            text.setText(getResources().getString(R.string.no_stock));
            selectProductAdd.addView(view);
        }
        subProductChild(layoutProductAdd);
    }

    public void setSelectProduct(final List<ClassesItem> classes) {
//        selectProductAdd.removeAllViews();
        Typeface bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        for (int i = 0; i < classes.size(); i++) {
            final View view = layoutInflater.inflate(R.layout.sale_class_item, selectProductAdd, false);
            final TextView subPhaseName = view.findViewById(R.id.sub_phase_name);
            final RelativeLayout add_fav = view.findViewById(R.id.add_fav);
            final ImageView icon_fav = view.findViewById(R.id.icon_fav);
            final ImageView promo_ico = view.findViewById(R.id.promo_ico);


            subPhaseName.setText(classes.get(i).getName());
            subPhaseName.setTypeface(bold);

            if(String.valueOf(classes.get(i).getFav()).equalsIgnoreCase("1")){
                icon_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
            }else{
                icon_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
            }

            if(String.valueOf(classes.get(i).getIspromotion()).equalsIgnoreCase("1")){
                promo_ico.setVisibility(View.VISIBLE);
            }else{
                promo_ico.setVisibility(View.INVISIBLE);
            }

//            promo_ico.setVisibility(View.VISIBLE);


            final int finalI = i;
            subPhaseName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DealByPhaseActivity.this, ChooseSkuActivity.class);
                    intent.putExtra("class_id", classes.get(finalI).getId());
                    intent.putExtra("phase_id", phase_id);
                    intent.putExtra("projectId", projectId);
                    intent.putExtra("subClass", classes.get(finalI).getName());

                    sub_class_name = classes.get(finalI).getName();
                    startActivity(intent);
                }
            });
            add_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//
//                    if(classes.get(finalI).getFav()==0){
//                        classes.get(finalI).setFav(1);
//                        icon_fav.setImageResource(R.drawable.ic_favorite_pink_48dp);
//                        addFAV(classes.get(finalI).getId(),classes);
//
//                    }else{
//                        classes.get(finalI).setFav(0);
//                        icon_fav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
//                        removeFAV(classes.get(finalI).getId(),classes);
//                    }
                }
            });
            selectProductAdd.addView(view, finalI);
        }
    }

    public void subProductChild(LinearLayout view) {
        int childcount = view.getChildCount();
        for (int i = 0; i < childcount; i++) {
            View v = view.getChildAt(i);
            if (v.getId() == subProductClick) {
                ((CardView)(v.findViewById(R.id.product_bg))).setCardBackgroundColor(getResources().getColor(R.color.txt_color_login));
                ((TextView) v.findViewById(R.id.product_name)).setTextColor(Color.WHITE);
            } else {
                ((CardView)(v.findViewById(R.id.product_bg))).setCardBackgroundColor(Color.WHITE);
                ((TextView) v.findViewById(R.id.product_name)).setTextColor(Color.BLACK);
            }
        }
    }


    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {
        showToast(this,message);
    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private void addFAV(String classId) {
        addClassFav(classId);
    }

    private void removeFAV(String classId) {
        removeClassFav(classId);
    }

    private Call<String> callAddClassFav(String _classId) {
        return rudyService.addClassFav(
                Utils.APP_LANGUAGE,
                projectId,
                _classId,
                user_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void addClassFav(String _classId){
        showLoading(DealByPhaseActivity.this);
        callAddClassFav(_classId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByPhaseActivity.this, getResources().getString(R.string.toast_add_producto_to_favorite_success));
                    loadClassList(groupId);
                    removeCacheUrl();
//                    getFAV();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callRemoveClassFav(String _classId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                projectId,
                _classId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void removeClassFav(String _classId){
        showLoading(DealByPhaseActivity.this);
        callRemoveClassFav(_classId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByPhaseActivity.this, getResources().getString(R.string.dialog_delete_finish));
                    loadClassList(groupId);
                    removeCacheUrl();
//                    getFAV();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    public void getData(final String url,final String msg,String classId) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                removeCacheUrl(classId);
//                if (string.contains("200")) {
//                    showToast(DealByPhaseActivity.this,msg);
//                    loadClassList(groupId);
//                    getFAV();
//
//                }
//            }
//        }.execute();
//    }


    private Call<String> callRemoveCacheUrl() {
        return rudyService.removeCacheUrl(
                Utils.APP_LANGUAGE,
                projectId,
                String.valueOf(subProductClick)
        );
    }

    private void removeCacheUrl(){
        showLoading(DealByPhaseActivity.this);
        callRemoveCacheUrl().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")){
                    getFAV();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByPhaseActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByPhaseActivity.this, t.getMessage());
            }
        });
    }


//    private void removeCacheUrl(String classId) {
//        removeCache(ApiEndPoint.getRemoveCateCache(Utils.APP_LANGUAGE,projectId,String.valueOf(subProductClick)));
//    }
//
//    @SuppressLint("StaticFieldLeak")
//    public void removeCache(final String url) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                getFAV();
//            }
//        }.execute();
//    }


    @Override
    public void onSubphaseClick(String subPhaseId) {
        phase_id = subPhaseId;
        loadGroupProjectList(phase_id);
//        showMessage(subPhaseId);

    }

    @Override
    public void onGroupClick(String groupId) {
        this.groupId = groupId;
        loadClassList(groupId);
    }

    @Override
    public void addFAVClass(String classId, boolean add) {
        if(add){
            addFAV(classId);
        }else{
            removeFAV(classId);
        }

    }

    @Override
    public void clickClass(com.scg.rudy.model.pojo.dealbyproject.class_by_phase.ItemsItem classItem) {
        this.classId=classItem.getId();
        Intent intent = new Intent(DealByPhaseActivity.this, ChooseSkuActivity.class);
        intent.putExtra("class_id", classId);
        intent.putExtra("projectId", projectId);
        intent.putExtra("phase_id", phase_id);
        sub_class_name = classItem.getName();
        startActivity(intent);




//        showMessage(classId);
    }
}
