package com.scg.rudy.ui.customer_detail.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.customerDetailPage.projectList.ItemsItem;
import com.scg.rudy.ui.by_customers.ByCustomerListener;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class projectListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private LayoutInflater inflater;
    private ByCustomerListener listener;

    public projectListRecyclerAdapter(Context context, ByCustomerListener listener) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = new ArrayList<>();
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.customer_detail_project_item, parent, false);
            return new projectListRecyclerAdapter.CustomerViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new projectListRecyclerAdapter.LoadingVH(viewLoading);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(final @NonNull RecyclerView.ViewHolder vholder, final int position) {
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ItemsItem projectModel = arrayList.get(position);
            CustomerViewHolder holder = (CustomerViewHolder) vholder;
            String[] createDate = projectModel.getLastUpdate().split(" ");
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currentTime = Calendar.getInstance().getTime();
            Date add_date  = Calendar.getInstance().getTime();
//            for (int i = 0; i < projectTypeObject.getItems().size(); i++) {
//                if(projectModel.getProjectTypeId().equalsIgnoreCase(projectTypeObject.getItems().get(i).getId())){
                    holder.projectType.setText(String.valueOf(projectModel.getProjectTypeName()));
//                    for (int j = 0; j < projectTypeObject.getItems().get(i).getPhases().size(); j++) {
//                        if(projectModel.getPhaseId().equalsIgnoreCase(projectTypeObject.getItems().get(i).getPhases().get(j).getId())){
//                            String sub[] = projectTypeObject.getItems().get(i).getPhases().get(j).getName().toString().split("\\[");
                            holder.phaseType.setText(projectModel.getPhaseName());
//                        }
//                    }
//
//
//                }
//            }
            holder.name.setText(projectModel.getProjectName());
            holder.addDate.setText(context.getResources().getString(R.string.add_on) + createDate[0]);
//        holder.unitArea.setText(projectModel.getUnit_area()+" ตรม.");
//        holder.unitBudget.setText(projectModel.getUnit_budget()+" ล้านบาท");
//
            holder.clickItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePrefer(context, "print", "");
                    Intent intent = new Intent(context, ProjectDetailActivity.class);
                    intent.putExtra("projectId",projectModel.getId());
                    intent.putExtra("name", projectModel.getProjectName());
                    context.startActivity(intent);
                }
            });



        }else if(itemPosition == LOADING){
            LoadingVH loadingVH = (LoadingVH) vholder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view,double percent,int maxBarSize){
        double fn_size = (percent*maxBarSize)/100;
        if(fn_size==0){
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        }else{
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class CustomerViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private LinearLayout clickItem;
        private TextView addDate;
        private TextView projectType;
        private TextView phaseType;
        public CustomerViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            clickItem = view.findViewById(R.id.clickItem);
            addDate = view.findViewById(R.id.add_date);
            projectType = view.findViewById(R.id.project_type);
            phaseType = view.findViewById(R.id.phase_type);
        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
       ItemsItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return arrayList.get(position);
    }


    public void add(ItemsItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }
    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }


}