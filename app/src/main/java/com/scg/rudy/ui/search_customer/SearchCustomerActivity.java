package com.scg.rudy.ui.search_customer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.byCustomer.Customer;
import com.scg.rudy.model.pojo.byCustomer.ItemsItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.by_customers.ByCustomerListener;
import com.scg.rudy.ui.by_customers.ByCustomerRecyclerAdapter;
import com.scg.rudy.ui.by_customers.byCustomerPaginationScrollListener;
import com.scg.rudy.ui.customer_detail.CustomerDetailActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.hideKeyboard;
import static com.scg.rudy.utils.Utils.showToast;

public class SearchCustomerActivity extends BaseActivity implements View.OnClickListener, ByCustomerListener {
    private ImageView clsBtn;
    private EditText searchText;
    private ImageView cancel;
    private TextView nodata;
    private Bundle extras;
    private RecyclerView recyclerView;
    private ByCustomerRecyclerAdapter adapter;
    private Customer customer = null;
    private List<ItemsItem> arrayList;
    private List<ItemsItem> newArrayList;

    private RudyService rudyService;
    private ImageView searchBtn;
    private String user_id = "",shopID="";
    public static final String TAG = "ByCustomersActivity";
    private LinearLayoutManager linearLayoutManager;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private String search = "";
    private LayoutInflater inflater;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
        newArrayList = new ArrayList<>();
        clsBtn.setOnClickListener(this);
        cancel.setOnClickListener(this);
        getUserData();

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                search = searchText.getText().toString();
            }
        });
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    TOTAL_PAGES = 1;
                    currentPage = PAGE_START;
                    isLoading = false;
                    isLastPage = false;
                    setAdapter();
                    loadCustomerList();
                    return true;
                }
                return false;
            }
        });


    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        clsBtn = findViewById(R.id.cls_btn);
        searchText = findViewById(R.id.search_text);
        cancel = findViewById(R.id.cancel);
        nodata = findViewById(R.id.nodata);

        rudyService = ApiHelper.getClient();
        searchBtn = findViewById(R.id.search_btn);
        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt);

    }

    @Override
    public void onClick(View v) {
        if(v==cancel){
            searchText.setText("");
            recyclerView.setAdapter(null);
            hideErrorView();
            Utils.showKeyboardFrom(this,searchText);
//            nodata.setVisibility(View.VISIBLE);
//            Utils.hideKeyboard(this);
        }

        if(v==clsBtn){
            Utils.hideKeyboard(this);
            finish();

        }

    }


    private void setAdapter(){
        adapter = new ByCustomerRecyclerAdapter(this,this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new byCustomerPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }
            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private Call<String> getCustomerList() {
        return rudyService.getCustomerList(
                Utils.APP_LANGUAGE,
                user_id,
                "list",
                String.valueOf(currentPage),
                search
        );
    }

    private void loadCustomerList() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        getCustomerList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Customer customer  = gson.fromJson(resp, Customer.class);
                    if(customer.getItems().size()>0){
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(customer.getItems(), searchText.getText().toString());
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        }
                        else {
                            isLastPage = true;
                        }
                    }else{
                        errorLayout.setVisibility(View.VISIBLE);
                        TOTAL_PAGES = 1;
                        currentPage = PAGE_START;
                        isLoading = false;
                        isLastPage = false;
                        setAdapter();
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(SearchCustomerActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage","TotalPage is : "+ TOTAL_PAGES);
        getCustomerList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Customer results  = gson.fromJson(resp, Customer.class);
                    adapter.addAll(results.getItems(), searchText.getText().toString());
                    if (currentPage < TOTAL_PAGES){
                        adapter.addLoadingFooter();
                    }
                    else {
                        isLastPage = true;
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(SearchCustomerActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Customer fetchCustomerdata(Response<Customer> response) {
        Customer customer = response.body();
        if(customer.getTotalPages()!=0){
            TOTAL_PAGES = customer.getTotalPages();
        }

        return customer;
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
//            totalProject.setText("0");
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_pls_try_again));
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onFragmentDetached(String tag) {

    }


    public boolean isNetworkConnected() {
        return true;
    }


    public void showData(Customer customer) {

    }

    public void showErrorIsNull() {

    }

    public void BodyError(ResponseBody responseBodyError) {

    }

    public void Failure(Throwable t) {

    }

    @Override
    public void checkInTel(ItemsItem itemList) {

    }

    @Override
    public void OnclickItems(ItemsItem projectModel) {
        Intent intent = new Intent(this, CustomerDetailActivity.class);
        intent.putExtra("id",projectModel.getUserId());
        intent.putExtra("customer_id",projectModel.getCustomerId());
        startActivity(intent);
    }
}
