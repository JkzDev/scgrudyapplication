
package com.scg.rudy.ui.call_to_customer.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class HideContentModel {

    @SerializedName("txtDetail")
    private String mTxtDetail;
    @SerializedName("txtHead")
    private String mTxtHead;

    public HideContentModel(String _txtHead, String _txtDetail){
        mTxtHead = _txtHead;
        mTxtDetail = _txtDetail;
    }

    public String getTxtDetail() {
        return mTxtDetail;
    }

    public void setTxtDetail(String txtDetail) {
        mTxtDetail = txtDetail;
    }

    public String getTxtHead() {
        return mTxtHead;
    }

    public void setTxtHead(String txtHead) {
        mTxtHead = txtHead;
    }

}
