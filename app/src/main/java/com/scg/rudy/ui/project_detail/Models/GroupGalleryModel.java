package com.scg.rudy.ui.project_detail.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupGalleryModel implements Serializable, Parcelable {

    @SerializedName("ckId")
    private String mKey;

    @SerializedName("name")
    private ArrayList<GalleryModel> mValue;

    public GroupGalleryModel(String _mKey, ArrayList<GalleryModel> _mValue){
        this.mKey = _mKey;
        this.mValue = _mValue;
    }

    protected GroupGalleryModel(Parcel in) {
        mKey = in.readString();
        mValue = in.createTypedArrayList(GalleryModel.CREATOR);
    }

    public static final Creator<GroupGalleryModel> CREATOR = new Creator<GroupGalleryModel>() {
        @Override
        public GroupGalleryModel createFromParcel(Parcel in) {
            return new GroupGalleryModel(in);
        }

        @Override
        public GroupGalleryModel[] newArray(int size) {
            return new GroupGalleryModel[size];
        }
    };

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public ArrayList<GalleryModel> getmValue() {
        return mValue;
    }

    public void setmValue(ArrayList<GalleryModel> mValue) {
        this.mValue = mValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKey);
        dest.writeTypedList(mValue);
    }
}
