package com.scg.rudy.ui.department_data;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.model.pojo.project_type.ItemsItem;
import com.scg.rudy.model.pojo.project_type.PhasesItem;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.ui.boq.BOQActivity;
import com.scg.rudy.ui.add_new_project.AddNewProjectActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class DepartmentDataActivity extends BaseActivity implements View.OnClickListener {
    private String phaseId = "0";
    private String cateId = "0";
    private String unit_number = "0";//1
    private String floor_number = "0";//2
    private String unit_area = "0";//150
    private String unit_budget = "0";//2.5
    private String project_name = "";
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private CustomSpinner customSpinner;
    private int catePosition = 0;
    private int phasePosition = 0;
    private int maxPhase = 10;
    private Spinner typeConstruc;
    private Spinner currentPhase;
    private EditText unit;
    private ImageView delUnit;
    private ImageView addUnit;
    private EditText totalfloor;
    private ImageView delfloor;
    private ImageView addfloor;
    private LinearLayout areaLayout;
    private LinearLayout select_phase;
    private EditText area;
    private ImageView delare;
    private ImageView addarea;
    private EditText price;
    private ImageView delprice;
    private ImageView addprice;
    private RelativeLayout save;
    private RelativeLayout boq_btn;
    private EditText name;
    private LinearLayout key_hide;
    private ImageView clsBtn;
    private Bundle extras;
    private ImageView clearText;
    private LinearLayout save_layout;
    public static boolean isEdit = false;
    public static String projectId = "";
    private RelativeLayout typeLayout;
    private RelativeLayout phaseLayout;
    private int first = 0;
    private String date;
    private RudyService rudyService;
    private TextView textCurrency;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_data);
        rudyService = ApiHelper.getClient();


        initView();
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        SimpleDateFormat dateformatCheck = new SimpleDateFormat("dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        date = simpledateformat.format(now.getTime());
        extras = getIntent().getExtras();

//        name = "DEbug ja";
//        projectId = "1036";

        if (extras != null) {
            unit_number = extras.getString("unit_number", "");
            floor_number = extras.getString("floor_number", "");
            unit_area = extras.getString("unit_area", "");
            unit_budget = extras.getString("unit_budget", "");
            project_name = extras.getString("project_name", "");
            cateId = extras.getString("cateId", "0");
            phaseId = extras.getString("phaseId", "0");
            catePosition = (Integer.parseInt(extras.getString("catePosition", "0")));
            phasePosition = (Integer.parseInt(extras.getString("phasePosition", "0")));
        }


        if (!project_name.contains(dateformatCheck.format(now.getTime()))) {
            name.setText(project_name);
        }

        if (cateId.equalsIgnoreCase("0")) {
            select_phase.setVisibility(View.GONE);
        } else {
            select_phase.setVisibility(View.VISIBLE);
        }


        Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
        name.setTypeface(_font);

        if (unit_number.length() > 0 ||
                floor_number.length() > 0 ||
                unit_area.length() > 0 ||
                unit_budget.length() > 0) {
            unit.setText(unit_number);
            totalfloor.setText(floor_number);
            area.setText(unit_area);
            price.setText(unit_budget);
        }


//        getCategorie(ApiEndPoint.getCate("th"));
        getProjectType();
        clearText.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        key_hide.setOnClickListener(this);
        save_layout.setOnClickListener(this);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    clearText.setVisibility(View.VISIBLE);
                } else {
                    clearText.setVisibility(View.GONE);
                }
            }
        });

        unit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (unit.getText().toString().length() == 1 && unit.getText().toString().equalsIgnoreCase(".")){
                    unit.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                unit_number = s.toString();
            }
        });

        totalfloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (totalfloor.getText().toString().length() == 1 && totalfloor.getText().toString().equalsIgnoreCase(".")){
                    totalfloor.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                floor_number = s.toString();
            }
        });

        area.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (area.getText().toString().length() == 1 && area.getText().toString().equalsIgnoreCase(".")){
                    area.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                unit_area = s.toString();
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (price.getText().toString().length() == 1 && price.getText().toString().equalsIgnoreCase(".")){
                    price.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                unit_budget = s.toString();

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                project_name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                name.setTypeface(_font);
            }
        });

        addUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!unit.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(unit.getText().toString());
                }
                unit_int = unit_int + 1;
                unit.setText(String.valueOf(unit_int));
            }
        });

        delUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!unit.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(unit.getText().toString());
                }
                if (unit_int > 0) {
                    unit_int = unit_int - 1;
                    if (unit_int <= 0) {
                        unit_int = 1;
                    }
                    unit.setText(String.valueOf(unit_int));
                }

            }
        });

        addfloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;

                if (!totalfloor.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(totalfloor.getText().toString());
                }
                unit_int = unit_int + 1;
                totalfloor.setText(String.valueOf(unit_int));
            }
        });

        delfloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;

                if (!totalfloor.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(totalfloor.getText().toString());
                }

                if (unit_int > 1) {
                    unit_int = unit_int - 1;
                    totalfloor.setText(String.valueOf(unit_int));
                }
            }
        });

        addarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!area.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(area.getText().toString());
                }
                unit_int = unit_int + 50;
                area.setText(String.valueOf(unit_int));
            }
        });

        delare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!area.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(area.getText().toString());
                }

                if (unit_int > 0) {
                    unit_int = unit_int - 50;
                    if (unit_int <= 0) {
                        unit_int = 1;
                    }
                    area.setText(String.valueOf(unit_int));
                }
            }
        });

        addprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!price.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(price.getText().toString());
                }

                unit_int = unit_int + .5;
                price.setText(String.valueOf(unit_int));
            }
        });

        delprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!price.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(price.getText().toString());
                }

                if (unit_int != 0) {
                    unit_int = unit_int - .5;
                    price.setText(String.valueOf(unit_int));
                }
            }
        });

        boq_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DepartmentDataActivity.this, BOQActivity.class);
                intent.putExtra("phaseId", phaseId);
                intent.putExtra("totalfloor", totalfloor.getText().toString());
                intent.putExtra("area", area.getText().toString());
                startActivity(intent);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showToast(DepartmentDataActivity.this,"cateId : "+cateId+"\n"+"phaseId : "+phaseId);

                unit_number = unit.getText().toString();
                floor_number = totalfloor.getText().toString();
                unit_area = area.getText().toString();
                unit_budget = price.getText().toString();
                project_name = name.getText().toString();


                if (!Utils.isNetworkConnected(DepartmentDataActivity.this)){

                    AddNewProjectActivity.storageDataOfflineModel.setUnit_number(unit_number);
                    AddNewProjectActivity.storageDataOfflineModel.setFloor_number(floor_number);
                    AddNewProjectActivity.storageDataOfflineModel.setUnit_area(unit_area);
                    AddNewProjectActivity.storageDataOfflineModel.setUnit_budget(unit_budget);
                    AddNewProjectActivity.storageDataOfflineModel.setProject_name(project_name);
                    AddNewProjectActivity.storageDataOfflineModel.setCateId(cateId);
                    AddNewProjectActivity.storageDataOfflineModel.setPhaseId(phaseId);
                    AddNewProjectActivity.storageDataOfflineModel.setCatePosition(catePosition);
                    AddNewProjectActivity.storageDataOfflineModel.setPhasePosition(phasePosition);
                    AddNewProjectActivity.storageDataOfflineModel.setMaxPhase(maxPhase);

                    MainActivity.data3 = true;
                    AddNewProjectActivity.step3 = true;

                    finish();
                }else{
                    if (project_name.length() > 0) {
                        if (Utils.project_id.length() > 0) {
                            AddNewProjectActivity.unit_number = unit_number;
                            AddNewProjectActivity.floor_number = floor_number;
                            AddNewProjectActivity.unit_area = unit_area;
                            AddNewProjectActivity.unit_budget = unit_budget;
                            AddNewProjectActivity.project_name = project_name;
                            AddNewProjectActivity.cateId = cateId;
                            AddNewProjectActivity.phaseId = phaseId;
                            AddNewProjectActivity.catePosition = catePosition;
                            AddNewProjectActivity.phasePosition = phasePosition;
                            AddNewProjectActivity.maxPhase = maxPhase;
                            MainActivity.data3 = true;
                            updateDepartment();
                        } else {
                            AddNewProjectActivity.unit_number = unit_number;
                            AddNewProjectActivity.floor_number = floor_number;
                            AddNewProjectActivity.unit_area = unit_area;
                            AddNewProjectActivity.unit_budget = unit_budget;
                            AddNewProjectActivity.project_name = project_name;
                            AddNewProjectActivity.cateId = cateId;
                            AddNewProjectActivity.phaseId = phaseId;
                            AddNewProjectActivity.catePosition = catePosition;
                            AddNewProjectActivity.phasePosition = phasePosition;
                            AddNewProjectActivity.maxPhase = maxPhase;
                            MainActivity.data3 = true;
                            AddNewProjectActivity.step3 = true;
                            finish();
                        }
                    } else {
                        if (Utils.project_id.length() > 0) {
                            AddNewProjectActivity.unit_number = unit_number;
                            AddNewProjectActivity.floor_number = floor_number;
                            AddNewProjectActivity.unit_area = unit_area;
                            AddNewProjectActivity.unit_budget = unit_budget;
                            AddNewProjectActivity.project_name = project_name;
                            AddNewProjectActivity.cateId = cateId;
                            AddNewProjectActivity.phaseId = phaseId;
                            AddNewProjectActivity.catePosition = catePosition;
                            AddNewProjectActivity.phasePosition = phasePosition;
                            AddNewProjectActivity.maxPhase = maxPhase;
                            MainActivity.data3 = true;
                            updateDepartment();
                        } else {
                            AddNewProjectActivity.unit_number = unit_number;
                            AddNewProjectActivity.floor_number = floor_number;
                            AddNewProjectActivity.unit_area = unit_area;
                            AddNewProjectActivity.unit_budget = unit_budget;
                            AddNewProjectActivity.project_name = project_name;
                            AddNewProjectActivity.cateId = cateId;
                            AddNewProjectActivity.phaseId = phaseId;
                            AddNewProjectActivity.catePosition = catePosition;
                            AddNewProjectActivity.phasePosition = phasePosition;
                            AddNewProjectActivity.maxPhase = maxPhase;
                            MainActivity.data3 = true;
                            AddNewProjectActivity.step3 = true;
                            finish();
                        }
                    }
                }
            }
        });

        Utils.hideKeyboard(this);
        textCurrency.setText(getResources().getString(R.string.value_work_per_building_million_bath) + " (" + getResources().getString(R.string.million) + Utils.currencyForShow + ")");
    }


    private Call<String> callUpdateDepartment() {
        return rudyService.updateDepartment(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                project_name,
                cateId,
                phaseId,
                unit.getText().toString(),
                area.getText().toString(),
                price.getText().toString(),
                totalfloor.getText().toString()
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updateDepartment(){
        showLoading(DepartmentDataActivity.this);
        callUpdateDepartment().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    AddNewProjectActivity.projectDetail = gson.fromJson(resp, ProjectDetail.class);
                    showToast(DepartmentDataActivity.this, getResources().getString(R.string.toast_update_data_success));
                    finish();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DepartmentDataActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void updateDepartment(final String url) {
//        showLoading(this);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("project_name", project_name)
//                .add("project_type_id", cateId)
//                .add("phase_id", phaseId)
//                .add("units", unit.getText().toString())
//                .add("unit_area", area.getText().toString())
//                .add("unit_budget", price.getText().toString())
//                .add("project_stories", totalfloor.getText().toString())
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
//                    AddNewProjectActivity.projectDetail = gson.fromJson(string, ProjectDetail.class);
//                    showToast(DepartmentDataActivity.this, getResources().getString(R.string.toast_update_data_success));
//                    finish();
//                }
//            }
//        }.execute();
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("project_name", project_name);
        outState.putString("unit_number", unit_number);
        outState.putString("floor_number", floor_number);
        outState.putString("unit_area", unit_area);
        outState.putString("unit_budget", unit_budget);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("project_name")) {
                project_name = savedInstanceState.getString("project_name");
            }

            if (savedInstanceState.containsKey("unit_number")) {
                unit_number = savedInstanceState.getString("unit_number");
            }
            if (savedInstanceState.containsKey("unit_number")) {
                floor_number = savedInstanceState.getString("floor_number");
            }
            if (savedInstanceState.containsKey("unit_area")) {
                unit_area = savedInstanceState.getString("unit_area");
            }
            if (savedInstanceState.containsKey("unit_budget")) {
                unit_budget = savedInstanceState.getString("unit_budget");
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == clearText) {
            name.setText("");
            clearText.setVisibility(View.GONE);
        }
        if (v == key_hide) {
            Utils.hideKeyboardFrom(DepartmentDataActivity.this, v);
        }
        if (v == save_layout) {
            Utils.hideKeyboardFrom(DepartmentDataActivity.this, v);
        }


    }

    private void initView() {
        clearText = findViewById(R.id.clear_text);
        typeConstruc = findViewById(R.id.type_construc);
        currentPhase = findViewById(R.id.current_phase);
        unit = findViewById(R.id.unit);
        delUnit = findViewById(R.id.delUnit);
        addUnit = findViewById(R.id.addUnit);
        totalfloor = findViewById(R.id.totalfloor);
        delfloor = findViewById(R.id.delfloor);
        addfloor = findViewById(R.id.addfloor);
        areaLayout = findViewById(R.id.area_layout);
        area = findViewById(R.id.area);
        delare = findViewById(R.id.delare);
        addarea = findViewById(R.id.addarea);
        price = findViewById(R.id.price);
        delprice = findViewById(R.id.delprice);
        addprice = findViewById(R.id.addprice);
        save = findViewById(R.id.save);
        boq_btn = findViewById(R.id.boq_btn);
        name = findViewById(R.id.project_name);
        key_hide = findViewById(R.id.key_hide);
        clsBtn = findViewById(R.id.cls_btn);
        save_layout = findViewById(R.id.save_layout);
        typeLayout = findViewById(R.id.type_layout);
        phaseLayout = findViewById(R.id.phase_layout);
        select_phase = findViewById(R.id.select_phase);
        textCurrency = (TextView) findViewById(R.id.textCurrency);
    }


    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    public void showData(final ProjectType projectType) {
        categorieModelArrayList = new ArrayList<>();
        nameList = new ArrayList<>();
        ArrayList<PhasesItem> phases = new ArrayList<PhasesItem>();
        phases.add(new PhasesItem(
                getResources().getString(R.string.pls_specify_phase),
                getResources().getString(R.string.pls_specify_phase),
                "0",
                getResources().getString(R.string.pls_specify_phase),
                null));

        projectType.getItems().add(0, new ItemsItem(
                "0",
                getResources().getString(R.string.pls_specify_type_construct),
                phases
        ));


        for (int i = 0; i < projectType.getItems().size(); i++) {
            if (Integer.parseInt(cateId) == Integer.parseInt(projectType.getItems().get(i).getId())) {
                catePosition = i;
            }
            categorieModelArrayList.add(new CategorieModel(projectType.getItems().get(i).getId(), projectType.getItems().get(i).getName()));
            nameList.add(projectType.getItems().get(i).getName());
        }
        customSpinner = new CustomSpinner(this, nameList);
        typeConstruc.setAdapter(customSpinner);
        typeConstruc.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        typeConstruc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                catePosition = position;
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                if (first > 0) {
                    phasePosition = 0;
                    phaseId = projectType.getItems().get(position).getPhases().get(phasePosition).getId();
                }
                cateId = categorieModelArrayList.get(position).getId();
                phaseId = projectType.getItems().get(position).getPhases().get(phasePosition).getId();
                if (cateId.equalsIgnoreCase("0")) {
                    select_phase.setVisibility(View.GONE);
//                    setPhaseSpinner(projectType.getItems().get(position).getPhases());
                } else {

                    select_phase.setVisibility(View.VISIBLE);
                    setPhaseSpinner(projectType.getItems().get(position).getPhases());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeConstruc.setSelection(catePosition);
        for (int i = 0; i < projectType.getItems().get(catePosition).getPhases().size(); i++) {
            if (projectType.getItems().get(catePosition).getPhases().get(i).getId().equalsIgnoreCase("" + phaseId)) {
                phasePosition = i;
            }
        }
        if (cateId.equalsIgnoreCase("0")) {
            select_phase.setVisibility(View.GONE);
        } else {
            select_phase.setVisibility(View.VISIBLE);
        }
    }

    public void setPhaseSpinner(List<PhasesItem> phasesItemList) {
        phaseModelArrayList = new ArrayList<>();
        phaseList = new ArrayList<>();
        for (int i = 0; i < phasesItemList.size(); i++) {
            phaseModelArrayList.add(new CategorieModel(phasesItemList.get(i).getId(), phasesItemList.get(i).getName()));
            phaseList.add(phasesItemList.get(i).getName());
        }
        CustomSpinner adapterphase = new CustomSpinner(this, phaseList);
        maxPhase = phaseList.size();
        if (phaseList.size() > 0) {
            currentPhase.setAdapter(adapterphase);
        } else {
            currentPhase.setAdapter(null);
        }
        currentPhase.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        currentPhase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                phaseId = phaseModelArrayList.get(position).getId();
                phasePosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (first == 0) {
            first++;
            currentPhase.setSelection(phasePosition);
        }

        if (cateId.equalsIgnoreCase("11")) {
            select_phase.setVisibility(View.GONE);
        } else {
            select_phase.setVisibility(View.VISIBLE);
        }

    }

    public void showErrorIsNull() {

    }

    public void BodyError(ResponseBody responseBodyError) {

    }

    public void Failure(Throwable t) {

    }

    private Call<String> callProjectType() {
        return rudyService.projectType(
                Utils.APP_LANGUAGE
        );
    }


    public void getProjectType() {
        showLoading(this);
        callProjectType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectType projectDetail = gson.fromJson(resp, ProjectType.class);
                    showData(projectDetail);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DepartmentDataActivity.this, t.getMessage());
            }
        });
    }
}
