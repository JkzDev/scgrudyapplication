package com.scg.rudy.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.PRCodeDetailModel;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class PrDetailRecyclerAdapter extends RecyclerView.Adapter<PrDetailRecyclerAdapter.ProjectViewHolder> {

    private ArrayList<PRCodeDetailModel>  cartModelArrayLis  = new ArrayList<>();
    private Context context;
    public PrDetailRecyclerAdapter(Context context,ArrayList<PRCodeDetailModel>  cartModelArrayLis) {
        this.cartModelArrayLis = cartModelArrayLis;
        this.context = context;
    }

    @Override
    public PrDetailRecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new PrDetailRecyclerAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PrDetailRecyclerAdapter.ProjectViewHolder holder, int position) {
        PRCodeDetailModel prCodeDetailModel = cartModelArrayLis.get(position);
//        ,prCodeDetailModel.getId()
        holder.name.setText( prCodeDetailModel.getItem_name());
        holder.totalItem.setText(Utils.moneyFormat(prCodeDetailModel.getQty()));
        if(prCodeDetailModel.getStatus().equalsIgnoreCase("1")){
            holder.price.setText(Utils.moneyFormat(prCodeDetailModel.getDefault_price()));
            holder.totalPrice.setText(Utils.moneyFormat(String.valueOf(Double.parseDouble(prCodeDetailModel.getDefault_price().replace("null","0"))*Double.parseDouble(prCodeDetailModel.getQty()))));
        }

        if(prCodeDetailModel.getStatus().equalsIgnoreCase("2")){
            holder.price.setText(Utils.moneyFormat(prCodeDetailModel.getRequest_price()));
            holder.totalPrice.setText(Utils.moneyFormat(String.valueOf(Double.parseDouble(prCodeDetailModel.getRequest_price().replace("null","0"))*Double.parseDouble(prCodeDetailModel.getQty()))));
        }
        if(prCodeDetailModel.getStatus().equalsIgnoreCase("3")){
            holder.price.setText(Utils.moneyFormat(prCodeDetailModel.getApprove_price()));
            holder.totalPrice.setText(Utils.moneyFormat(String.valueOf(Double.parseDouble(prCodeDetailModel.getApprove_price().replace("null","0"))*Double.parseDouble(prCodeDetailModel.getQty()))));
        }
        if(prCodeDetailModel.getQo_id().equalsIgnoreCase("null")){
            holder.status_layout.setVisibility(View.GONE);
            holder.pr_status.setText(context.getResources().getString(R.string.completed_quotation));
//            holder.pr_status.setText(String.format("ออกใบเสนอราคาเรียบร้อย เลขที่ %s", prCodeDetailModel.getQo_id()));
        }else{
            holder.status_layout.setVisibility(View.VISIBLE);
            holder.pr_status.setText(context.getResources().getString(R.string.completed_quotation));
//            holder.pr_status.setText(String.format("ออกใบเสนอราคาเรียบร้อย เลขที่ %s", prCodeDetailModel.getQo_id()));
        }


//        ออกใบเสนอราคาเรียบร้อย เลขที่QO00001
        holder.status.setText(prCodeDetailModel.getStatus());
        if (prCodeDetailModel.getStatus().contains("1")) {
            holder.status.setText(context.getResources().getString(R.string.case1));
            holder.status.setTextColor(context.getResources().getColor(R.color.favorite));
        }
        if (prCodeDetailModel.getStatus().contains("2")) {
            holder.status.setText(context.getResources().getString(R.string.case2));
            holder.status.setTextColor(context.getResources().getColor(R.color.txt_tab2));
        }
        if (prCodeDetailModel.getStatus().contains("3")) {
            holder.status.setText(context.getResources().getString(R.string.case3));
            holder.status.setTextColor(context.getResources().getColor(R.color.txt_tab3));
        }

//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null)
            return 0;
        return cartModelArrayLis.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price ;
        TextView totalItem ;
        TextView totalPrice;
        TextView status ;
        RelativeLayout status_layout;
        TextView pr_status;

        public ProjectViewHolder(View view) {
            super(view);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            totalItem = itemView.findViewById(R.id.total_item);
            totalPrice = itemView.findViewById(R.id.total_price);
            status = itemView.findViewById(R.id.status);
            status_layout  = itemView.findViewById(R.id.status_layout);
            pr_status = itemView.findViewById(R.id.pr_status);
//            title = (TextView) view.findViewById(R.id.title);
//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


}