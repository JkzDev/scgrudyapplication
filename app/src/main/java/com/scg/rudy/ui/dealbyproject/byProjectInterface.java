package com.scg.rudy.ui.dealbyproject;

import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.model.pojo.dealbyproject.other_type.ClassesItem;

import java.util.List;

/**
 * Created by DekDroidDev on 12/2/2018 AD.
 */

public interface byProjectInterface {
    void onClickCateItem(String subPhaseId);
    void onClickPhaseCateItem(List<ClassesItem> classes);
    void onClickFavCate(int type,boolean add,String subPhaseId);
    void onClickAddtoCart(SkuItem skuItem);
    void addFAVClass(String classId,boolean add);
    void clickClass(String classId,String name);
    void onClickInfo(SkuItem skuItem);
}
