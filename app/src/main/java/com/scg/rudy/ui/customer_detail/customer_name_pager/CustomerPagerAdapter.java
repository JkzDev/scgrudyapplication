package com.scg.rudy.ui.customer_detail.customer_name_pager;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.github.ksoichiro.android.observablescrollview.CacheFragmentStatePagerAdapter;

import java.util.ArrayList;


/**
 * @author jackie
 */
public class CustomerPagerAdapter extends CacheFragmentStatePagerAdapter {
    private ArrayList<String> customerCategory;
    private int mScrollY;
    private String customer;
    private Context context;
    public CustomerPagerAdapter(Context context,FragmentManager fm, ArrayList<String> customerCategory, String customer) {
        super(fm);
        this.customerCategory = customerCategory;
        this.customer = customer;
        this.context = context;
    }

    public void setScrollY(int scrollY) {
        mScrollY = scrollY;
    }

//    @Override
//    public ServiceFragment getItem(int position) {
//        return  ServiceFragment.newInstance(position);
//    }

    @Override
    protected Fragment createItem(int position) {
        switch (position){
            case 0 :
                return ProjectCustomerListFragment.newInstance(position,customer);

            case 1 :
                return SaleCustomerListFragment.newInstance(position,customer);

            case 2 :
                return WalletCustomerFragment.newInstance(position,customer);

            case 3 :
                return SumaryCustomerFragment.newInstance(position);

            default:
                return ProjectCustomerListFragment.newInstance(position,customer);
        }

    }

    @Override
    public int getCount() {
        if(customerCategory !=null){
            return customerCategory.size();
        }
        return 0;
    }



    @Override
    public CharSequence getPageTitle(final int position) {
        return customerCategory.get(position);
    }
}
