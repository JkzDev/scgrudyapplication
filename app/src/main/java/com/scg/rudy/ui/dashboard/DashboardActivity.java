package com.scg.rudy.ui.dashboard;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.dashboard.AllStatusChart;
import com.scg.rudy.model.pojo.dashboard.Dashboard;
import com.scg.rudy.model.pojo.dashboard.Items;
import com.scg.rudy.model.pojo.dashboard.SumTrans;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_ui.fitChart.FitChart;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.isNumeric;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class DashboardActivity extends BaseActivity implements View.OnClickListener {

    private ImageView clsBtn;
    private TextView titleName;
    private TextView totalPrice;
    private TextView priceList;
    private TextView priceListThb;
    private FitChart projectChart;
    private TextView priceProject;
    private FitChart typeChart;
    private FitChart fevChart;
    private TextView priceType;
    private TextView offerFev;
    private FitChart peopleChart;
    private TextView totalPeople;
    private RelativeLayout bgGreen;
    private ImageView barGreen;
    private TextView textPrice;
    private RelativeLayout bgBlue;
    private ImageView barBlue;
    private TextView textTotalsale;
    private RelativeLayout bgOrange;
    private ImageView barOrange;
    private TextView textPhaseDeal;
    private RelativeLayout bgPurple;
    private ImageView barPurple;
    private TextView textDeal;
    private RelativeLayout bgDarkblue;
    private ImageView barDarkblue;
    private TextView textAdtocart;
    private RelativeLayout bgYellow;
    private ImageView barYellow;
    private TextView textCloseDeal;
    private int minSize;
    private int maxBarSize;
    private RudyService rudyService;
    private TextView unitGreen;
    private TextView unitBlue;
    private TextView unitOrange;
    private TextView unitPurple;
    private TextView unitDarkblue;
    private TextView unitYellow;
    private TextView unProject;
    private TextView unCross;
    private TextView unCus;
    private TextView unFev;
    private TextView unitTran;
    private TextView txtCurrency;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setEnviroment();
        initView();
        setOnclick();
        loadDashboard();

        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());
        String dateArr[] = date.split("/");
//        Utils.showToast(this,date);
//        month = dateArr[1];
//        year = dateArr[2];

        titleName.setText(String.format("%s %s/%s", getResources().getString(R.string.Dashboard), dateArr[1], dateArr[2]));
        txtCurrency.setText(Utils.currencyForShow);
    }

    private void setOnclick() {
        clsBtn.setOnClickListener(this);
    }

    private Call<String> callDashboardApi() {
        return rudyService.getDashboard(
                Utils.APP_LANGUAGE,
                user_id
        );
    }

    private void loadDashboard() {
        showLoading(this);
        callDashboardApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Dashboard dashboard = gson.fromJson(resp, Dashboard.class);
                    Items dashboardResults = dashboard.getItems();
                    setDashboardData(dashboardResults);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(DashboardActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DashboardActivity.this, fetchErrorMessage(t));
            }
        });
    }

    private void setDashboardData(Items dashboardResults) {

        totalPrice.setText(Utils.moneyFormat(dashboardResults.getTransaction()));
        priceProject.setText(dashboardResults.getProject());
        priceType.setText(dashboardResults.getCrossSelling());
        offerFev.setText(dashboardResults.getFavorite());
        totalPeople.setText(dashboardResults.getCustomer());

        unProject.setText(String.format("/%s", dashboardResults.getTargetProject()));
        unCross.setText("/" + dashboardResults.getTargetCrossSelling());
        unCus.setText("/" + dashboardResults.getTargetCustomer());
        unFev.setText("/" + dashboardResults.getTarget_favorite());
        setFitChartValue(
                projectChart,
                Float.parseFloat(dashboardResults.getProject()),
                Float.parseFloat(dashboardResults.getTargetProject()),
                dashboardResults.getPProject());

        setFitChartValue(
                typeChart,
                Float.parseFloat(dashboardResults.getCrossSelling()),
                Float.parseFloat(dashboardResults.getTargetCrossSelling()),
                dashboardResults.getPCrossSelling());

        setFitChartValue(
                fevChart,
                Float.parseFloat(dashboardResults.getFavorite()),
                Float.parseFloat(dashboardResults.getTarget_favorite()),
                dashboardResults.getP_favorite());

        setFitChartValue(
                peopleChart,
                Float.parseFloat(dashboardResults.getCustomer()),
                Float.parseFloat(dashboardResults.getTargetCustomer()),
                dashboardResults.getPCustomer());

        setSumTran(dashboardResults.getSumTrans());
        setvalueBar(dashboardResults.getAllStatusChart());
    }

    private void setSumTran(SumTrans sumTran) {
        String tran[] = sumTran.getTransaction().split(" ");
        priceList.setText(sumTran.getNumber());
        priceListThb.setText(tran[0]);
        unitTran.setText(tran[1]);
    }

    private void setFitChartValue(FitChart fitChart, float minValue, float maxValue, String value) {
        fitChart.setMinValue(0f);
        fitChart.setMaxValue(100f);
        if (isNumeric(value.replace(".", ""))) {
            fitChart.setValue(Float.parseFloat(value));
        } else {
            fitChart.setValue(0f);
        }
    }

    private void setvalueBar(AllStatusChart statusChart) {
        setBarPercent(barGreen, Double.parseDouble(statusChart.getPStat1()));
        setBarPercent(barBlue, Double.parseDouble(statusChart.getPStat2()));
        setBarPercent(barOrange, Double.parseDouble(statusChart.getPStat3()));
        setBarPercent(barPurple, Double.parseDouble(statusChart.getPStat4()));
        setBarPercent(barDarkblue, Double.parseDouble(statusChart.getPStat5()));
        setBarPercent(barYellow, Double.parseDouble(statusChart.getPStat6()));

        setStatusPercentBar(textPrice, unitGreen, statusChart.getStat1());
        setStatusPercentBar(textTotalsale, unitBlue, statusChart.getStat2());
        setStatusPercentBar(textPhaseDeal, unitOrange, statusChart.getStat3());
        setStatusPercentBar(textDeal, unitPurple, statusChart.getStat4());
        setStatusPercentBar(textAdtocart, unitDarkblue, statusChart.getStat5());
        setStatusPercentBar(textCloseDeal, unitYellow, statusChart.getStat6());

    }


    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }


    @Override
    public void setEnviroment() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
    }

    private void setMinSize(View view, int width, int heigh) {
        view.getLayoutParams().height = heigh;
        view.getLayoutParams().width = width;
        view.requestLayout();
    }

    private void setBarPercent(View view, double percent) {
        double fn_size = (percent * maxBarSize) / 100;
        if (fn_size == 0) {
//           view.getLayoutParams().height = (int) TypedValue.applyDimension(
//                   TypedValue.COMPLEX_UNIT_PX,
//                   getResources().getDimension(R.dimen._1sdp),
//                   getResources().getDisplayMetrics());
//           view.getLayoutParams().width = (int) TypedValue.applyDimension(
//                   TypedValue.COMPLEX_UNIT_PX,
//                   getResources().getDimension(R.dimen._5sdp),
//                   getResources().getDisplayMetrics());
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        } else {
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }


    private void setStatusPercentBar(TextView textStat, TextView textUnit, String txtSplite) {
        String[] txt = txtSplite.split(" ");
        textStat.setText(txt[0]);
        textUnit.setText(txt[1]);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        rudyService = ApiHelper.getClient();
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen._26sdp),
                getResources().getDisplayMetrics());
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        totalPrice = findViewById(R.id.total_price);
        priceList = findViewById(R.id.price_list);
        priceListThb = findViewById(R.id.price_list_thb);
        projectChart = findViewById(R.id.projectChart);
        priceProject = findViewById(R.id.price_project);
        fevChart = findViewById(R.id.fevChart);
        typeChart = findViewById(R.id.typeChart);
        priceType = findViewById(R.id.price_type);
        offerFev = findViewById(R.id.offer_fev);
        peopleChart = findViewById(R.id.peopleChart);
        totalPeople = findViewById(R.id.total_people);
        bgGreen = findViewById(R.id.bg_green);
        barGreen = findViewById(R.id.bar_green);
        textPrice = findViewById(R.id.text_price);
        bgBlue = findViewById(R.id.bg_blue);
        barBlue = findViewById(R.id.bar_blue);
        textTotalsale = findViewById(R.id.text_totalsale);
        bgOrange = findViewById(R.id.bg_orange);
        barOrange = findViewById(R.id.bar_orange);
        textPhaseDeal = findViewById(R.id.text_phase_deal);
        bgPurple = findViewById(R.id.bg_purple);
        barPurple = findViewById(R.id.bar_purple);
        textDeal = findViewById(R.id.text_deal);
        bgDarkblue = findViewById(R.id.bg_darkblue);
        barDarkblue = findViewById(R.id.bar_darkblue);
        textAdtocart = findViewById(R.id.text_adtocart);
        bgYellow = findViewById(R.id.bg_yellow);
        barYellow = findViewById(R.id.bar_yellow);
        textCloseDeal = findViewById(R.id.text_close_deal);
        unitGreen = findViewById(R.id.unit_green);
        unitBlue = findViewById(R.id.unit_blue);
        unitOrange = findViewById(R.id.unit_orange);
        unitPurple = findViewById(R.id.unit_purple);
        unitDarkblue = findViewById(R.id.unit_darkblue);
        unitYellow = findViewById(R.id.unit_yellow);
        unProject = findViewById(R.id.un_project);
        unCross = findViewById(R.id.un_cross);
        unCus = findViewById(R.id.un_cus);
        unFev = findViewById(R.id.un_fev);
        unitTran = findViewById(R.id.unit_tran);
        txtCurrency = findViewById(R.id.txtCurrency);
        ViewTreeObserver vto = bgGreen.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                maxBarSize = bgGreen.getWidth();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
    }
}
