package com.scg.rudy.ui.boq;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scg.rudy.model.json_parser.Parser;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.BOQModel;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.data.remote.ApiEndPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BOQActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout layoutAdd;
    private TextView nodata;
    private String user_id = "";
    private String shopID = "";
    private TextView name;
    private CheckBox status;
    private TextView totalPhase;
    private TextView total;
    private ImageView clsBtn;
    private String phase = "1";
    private String floor = "2";
    private String area = "150";
    private Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boq);
        getUserData();
        initView();
        extras = getIntent().getExtras();

        if (extras != null) {
            phase = extras.getString("phaseId");
            floor = extras.getString("totalfloor");
            area = extras.getString("area");
        }
        String url = ApiEndPoint.getBoq(Utils.APP_LANGUAGE, shopID,phase,floor.replace("-","2"),area.replace("-","250"));
        getData(url);
        clsBtn.setOnClickListener(this);
//        getBoq
    }


    public void getUserData() {

        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");


        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }

    //do not change don't use
    @SuppressLint("StaticFieldLeak")
    private void getData(final String url) {
        Log.i("", "Customer: " + url);
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                String response = Utils.getData(url);
                return response;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                dialog.dismiss();
                if (string.contains("200")) {
//                    showToast(ByCustomersActivity.this, string);
                    final ArrayList<BOQModel> arrayList = Parser.BoqModelParser(string);
                    nodata.setVisibility(View.GONE);
                    setLayoutAdd(arrayList);
//                    model.getAddress();
                } else {
                    nodata.setVisibility(View.VISIBLE);
//                    showToast(BOQActivity.this, "don't have user ,please try again");
                }


            }
        }.execute();
    }


    public void setLayoutAdd(final ArrayList<BOQModel> arrayList) {
//        View view = LayoutInflater.from(this).inflate(R.layout.project_item,layoutAdd);
        Typeface bold = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        View view;
        for (int i = 0; i < arrayList.size(); i++) {
            view = layoutInflater.inflate(R.layout.boq_model, layoutAdd, false);
            name = view.findViewById(R.id.name);
            status = view.findViewById(R.id.status);
            totalPhase = view.findViewById(R.id.total_phase);
            total = view.findViewById(R.id.total);


            if(i==0){
                name.setText(arrayList.get(i).getPhase());
            }

            status.setText(arrayList.get(i).getProduct_cate());
            totalPhase.setText(arrayList.get(i).getArea());
            total.setText(arrayList.get(i).getUnit());

            name.setTypeface(bold);
            status.setTypeface(bold);
            totalPhase.setTypeface(bold);
            total.setTypeface(bold);


            layoutAdd.addView(view, i);
        }


    }


    private void initView() {
        layoutAdd = findViewById(R.id.layout_add);
        nodata = findViewById(R.id.nodata);

        clsBtn = findViewById(R.id.cls_btn);
    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
