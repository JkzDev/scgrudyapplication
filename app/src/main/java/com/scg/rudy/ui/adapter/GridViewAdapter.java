package com.scg.rudy.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.scg.rudy.R;
import com.scg.rudy.ui.department_photo.TakePhotoInterface;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 30/1/2018 AD.
 */

public class GridViewAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Bitmap> mArrayList;
    private LayoutInflater mInflater;
    private TakePhotoInterface listener;

    // Gets the context so it can be used later
    public GridViewAdapter(Context context, ArrayList<Bitmap> arrayList, TakePhotoInterface listener) {
        this.mContext = context;
        this.mArrayList = arrayList;
        this.mInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    public int getCount() {
        return mArrayList.size();
    }

    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if(convertView==null){
            holder = new Holder();
            convertView = mInflater.inflate(R.layout.item_photo_view, null);
            holder.img2 = convertView.findViewById(R.id.img2);
            holder.del2 = convertView.findViewById(R.id.del2);
            convertView.setTag(holder);
        }else{
            holder = (Holder)convertView.getTag();
        }
        holder.img2.setImageBitmap(mArrayList.get(position));

        holder.img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                listener.onShowPhoto(mArrayList.get(position));
            }
        });
        holder.del2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder builder =  new MaterialDialog.Builder(mContext)
                        .title(mContext.getResources().getString(R.string.dialog_want_to_delete_picture))
                        .positiveText(mContext.getResources().getString(R.string.agreed))
                        .negativeText(mContext.getResources().getString(R.string.cancel))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
//                                listener.onClickDeletePhoto(mArrayList.get(position));

                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        });

                MaterialDialog del_dialog = builder.build();
                del_dialog.show();


            }
        });

        return convertView;
    }

    public class Holder {
         ImageView img2;
         RelativeLayout del2;
    }
}