package com.scg.rudy.ui.department_photo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_detail.Items;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.ui.adapter.GridViewAdapter;
import com.scg.rudy.ui.adapter.QOModels.Gallery;
import com.scg.rudy.ui.add_new_project.AddNewProjectActivity;
import com.scg.rudy.ui.checkin_gallery.CheckInGalleryActivity;
import com.scg.rudy.ui.department_photo.Adapter.GroupShowPhotoAdapter;
import com.scg.rudy.ui.department_photo.Adapter.ShowPhotoAdapter;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;
import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_ui.floatingActionButton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class TakePhotoActivity extends BaseActivity implements View.OnClickListener, TakePhotoInterface {
    private static final int REQUEST_CAMERA = 0;
    @BindView(R.id.cls_btn)
    ImageView clsBtn;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.save)
    LinearLayout save;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.txt_alert)
    TextView txtAlert;
    @BindView(R.id.btnSelectPhoto)
    FloatingActionButton btnSelectPhoto;
    @BindView(R.id.grid_view)
    GridView gridView;
    private int SELECT_FILE = 1;
    private String userChoosenTask;
    public static ArrayList<GroupGalleryModel> mArrayList;
    private GroupShowPhotoAdapter adapter;
    private Point mSize;
    public static final String TAG = "TakePhotoActivity";
    public static String projectId = "";
    private RudyService rudyService;


    ArrayList<String> returnValue = new ArrayList<>();
    Options options;
    private RecyclerView reShowPhoto;

    private boolean flagGetLocationPicture = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);

        rudyService = ApiHelper.getClient();
        options = Options.init()
                .setRequestCode(100)
                .setCount(5)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setPreSelectedUrls(returnValue)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");


        ButterKnife.bind(this);
        initView();
        getUserData();
        clsBtn.setOnClickListener(this);


        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null){
            mArrayList = (ArrayList<GroupGalleryModel>) bundle.getSerializable("listImage");
        }else{
            mArrayList = new ArrayList<>();
        }

        Display display = TakePhotoActivity.this.getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

        btnSelectPhoto.setVisibility(View.VISIBLE);

        int countValueImage = 0;
        for (GroupGalleryModel g : mArrayList){
            for (GalleryModel gg : g.getmValue()){
                countValueImage++;
            }
        }

        if (countValueImage > 0) {
            save.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {
            txtAlert.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
        }

        adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, this);
//        reShowPhoto.setLayoutManager(new GridLayoutManager(this, 3));
//        reShowPhoto.setHasFixedSize(true);
        reShowPhoto.setLayoutManager(new LinearLayoutManager(this));
        reShowPhoto.setAdapter(adapter);

        btnSelectPhoto.setOnClickListener(this);
        save.setOnClickListener(this);
        clsBtn.setOnClickListener(this);

        showFirstInDialog();
    }

    private void showFirstInDialog() {
        final Dialog dialog = new Dialog(TakePhotoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_alert_add_pic_a_lot);
        dialog.setCanceledOnTouchOutside(false);

        TextView detail = dialog.findViewById(R.id.Detail);
        TextView textBtnAgree = dialog.findViewById(R.id.btnAgree);

        RelativeLayout agree = dialog.findViewById(R.id.agree);

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        detail.setText(getResources().getString(R.string.alert_first_in_dialog));
        textBtnAgree.setText(getResources().getString(R.string.checkin_area_dialog_btn_agree));

        dialog.show();
    }

    private Bitmap StringToBitMap(String encodedString) {
        try {
            String[] img = encodedString.split(",");
            byte[] encodeByte = Base64.decode(img[1], Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }


    private String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Bitmap> listBm = new ArrayList<>();
        List<String> listImg = new ArrayList<>();
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);



                    if (returnValue.size() > 0) {
                        if (Utils.project_id.length() > 0) {

                            //convert url to bitmap
                            for (int i = 0; i < returnValue.size(); i++) {
                                File f = new File(returnValue.get(i));
                                Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                                listBm.add(d);
                            }

                            //convert bitmap to string
                            for (int i = 0; i < listBm.size(); i++) {
                                String img = "data:image/jpg;base64," + BitMapToString(listBm.get(i));
                                listImg.add(img);
                            }

                            addPic(listImg);

                        } else {

                            ArrayList<GalleryModel> galleryModel = new ArrayList<>();

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());

                            for (int i = 0; i < returnValue.size(); i++) {

                                if (mArrayList.size() > 0){
                                    mArrayList.get(0).getmValue().add(new GalleryModel("", returnValue.get(i), currentDateandTime));
                                }else{
                                    galleryModel.add(new GalleryModel("", returnValue.get(i), currentDateandTime));
                                }
                            }

                            if (mArrayList.size() <= 0){
                                mArrayList.add(new GroupGalleryModel("", galleryModel));
                            }

                            int countValueImage = 0;
                            for (GroupGalleryModel g : mArrayList){
                                for (GalleryModel gg : g.getmValue()){


                                    if (!Utils.isNetworkConnected(this)){
                                        try {
                                            ExifInterface exif = new ExifInterface(gg.getName());

                                            float[] latlong = new float[2];

                                            exif.getLatLong(latlong);

                                            if ((latlong[0] > 0 || latlong[1] > 0) && flagGetLocationPicture){
                                                if ((AddNewProjectActivity.storageDataOfflineModel.getDragLat() == null || AddNewProjectActivity.storageDataOfflineModel.getDragLat() <= 0) &&
                                                        (AddNewProjectActivity.storageDataOfflineModel.getDragLng() == null || AddNewProjectActivity.storageDataOfflineModel.getDragLng() <= 0)){
                                                    AddNewProjectActivity.storageDataOfflineModel.setDragLat(Double.parseDouble(latlong[0]+""));
                                                    AddNewProjectActivity.storageDataOfflineModel.setDragLng(Double.parseDouble(latlong[1]+""));
                                                    flagGetLocationPicture = false;
                                                }
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }else{
                                        try {
                                            ExifInterface exif = new ExifInterface(gg.getName());

                                            float[] latlong = new float[2];

                                            exif.getLatLong(latlong);

                                            if ((latlong[0] > 0 || latlong[1] > 0) && flagGetLocationPicture){
                                                if ((AddNewProjectActivity.DragLat == null || AddNewProjectActivity.DragLat <= 0) &&
                                                        (AddNewProjectActivity.DragLng == null || AddNewProjectActivity.DragLng <= 0)){


                                                    AddNewProjectActivity.DragLat = Double.parseDouble(latlong[0]+"");
                                                    AddNewProjectActivity.DragLng = Double.parseDouble(latlong[1]+"");
                                                    AddNewProjectActivity.lat_lng = latlong[0]+","+latlong[1];
                                                    flagGetLocationPicture = false;
                                                }
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    countValueImage++;
                                }
                            }

                            if (countValueImage > 0) {
                                save.setVisibility(View.VISIBLE);
                                txtAlert.setVisibility(View.GONE);
                            } else {
                                txtAlert.setVisibility(View.VISIBLE);
                            }

                            adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, TakePhotoActivity.this);
                            reShowPhoto.setLayoutManager(new LinearLayoutManager(TakePhotoActivity.this));
                            reShowPhoto.setAdapter(adapter);
                            showToast(TakePhotoActivity.this, getResources().getString(R.string.toast_save_image_success));
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    public void requestForCameraPermission(View view) {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(TakePhotoActivity.this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(TakePhotoActivity.this, permission)) {
                showPermissionRationaleDialog(getResources().getString(R.string.dialog_pls_allow_to_access_camera), permission);
            } else {
                requestForPermission(permission);
            }
        } else {
            launch();
        }

    }

    private void showPermissionRationaleDialog(final String message, final String permission) {
        new AlertDialog.Builder(TakePhotoActivity.this)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestForPermission(permission);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }


    private Call<String> callDeletePic(String _pic) {
        return rudyService.deletePic(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                _pic
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deletePic(String _pic) {
        showLoading(this);
        callDeletePic(_pic).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
                    Items items = projectDetail.getItems();

                    if (items.getGalleryNew().size() > 0) {
                        save.setVisibility(View.VISIBLE);
                        txtAlert.setVisibility(View.GONE);

                        mArrayList = new ArrayList<>();

                        ArrayList<GroupGalleryModel> groupGalleryModels = new ArrayList<>();

                        Map<String,ArrayList<GalleryModel>> groupImage = new HashMap<>();

                        for(GalleryModel p : items.getGalleryNew()){
                            if(!groupImage.containsKey(p.getCkId())){
                                groupImage.put(p.getCkId(), new ArrayList<>());
                            }
                            groupImage.get(p.getCkId()).add(p);
                        }

                        for (Map.Entry<String, ArrayList<GalleryModel>> map : groupImage.entrySet()){
                            groupGalleryModels.add(new GroupGalleryModel(map.getKey(), map.getValue()));
                        }

                        Collections.sort(groupGalleryModels, new Comparator<GroupGalleryModel>() {
                            @Override
                            public int compare(GroupGalleryModel o1, GroupGalleryModel o2) {
                                return o1.getmKey().compareTo(o2.getmKey());
                            }
                        });

                        mArrayList = groupGalleryModels;

                        save.setVisibility(View.VISIBLE);
                        txtAlert.setVisibility(View.GONE);

                        adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, TakePhotoActivity.this);
                        reShowPhoto.setLayoutManager(new LinearLayoutManager(TakePhotoActivity.this));
                        reShowPhoto.setAdapter(adapter);

                    } else {
                        mArrayList = new ArrayList<>();
                        txtAlert.setVisibility(View.VISIBLE);
                        adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, TakePhotoActivity.this);
                        reShowPhoto.setLayoutManager(new LinearLayoutManager(TakePhotoActivity.this));
                        reShowPhoto.setAdapter(adapter);
                    }
                    showToast(TakePhotoActivity.this, getResources().getString(R.string.toast_delete_image_success));
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(TakePhotoActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TakePhotoActivity.this, t.getMessage());
            }
        });
    }

    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(TakePhotoActivity.this, new String[]{permission}, REQUEST_CAMERA);
    }

    private void launch() {
        Pix.start(TakePhotoActivity.this, options);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                final int numOfRequest = grantResults.length;
                final boolean isGranted = numOfRequest == 1
                        && PackageManager.PERMISSION_GRANTED == grantResults[numOfRequest - 1];
                if (isGranted) {
                    launch();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void initView() {
        clsBtn = findViewById(R.id.cls_btn);

        reShowPhoto = (RecyclerView) findViewById(R.id.reShowPhoto);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View v) {
        if (v == save) {

            if (!Utils.isNetworkConnected(this)){
                int CheckCountData = 0;
                for (GroupGalleryModel g : mArrayList){
                    if (g.getmValue().size() > 0){
                        CheckCountData++;
                    }
                }
                if (CheckCountData > 0) {
                    MainActivity.data2 = true;
                    AddNewProjectActivity.step2 = true;
                    AddNewProjectActivity.storageDataOfflineModel.setmArrayList(mArrayList);
                } else {
                    MainActivity.data2 = false;
                    AddNewProjectActivity.step2 = false;
                    AddNewProjectActivity.storageDataOfflineModel.setmArrayList(new ArrayList<>());
                }
            }else{
                int CheckCountData = 0;
                for (GroupGalleryModel g : mArrayList){
                    if (g.getmValue().size() > 0){
                        CheckCountData++;
                    }
                }
                if (CheckCountData > 0) {
                    MainActivity.data2 = true;
                    AddNewProjectActivity.step2 = true;
                    AddNewProjectActivity.mArrayList = mArrayList;
                } else {
                    MainActivity.data2 = false;
                    AddNewProjectActivity.step2 = false;
                    AddNewProjectActivity.mArrayList = new ArrayList<>();
                }
            }
            finish();
        }
        if (v == btnSelectPhoto) {
            requestForCameraPermission(btnSelectPhoto);
        }
        if (v == clsBtn) {
            if (mArrayList.size() > 0) {
                MainActivity.data2 = true;
//                AddNewProjectActivity.mArrayList = mArrayList;
            } else {
                MainActivity.data2 = false;
                AddNewProjectActivity.mArrayList = new ArrayList<>();
            }
            finish();
        }

    }


    @Override
    public void onClickDeletePhoto(String img) {
        if (Utils.project_id.length() > 0) {

            for (GroupGalleryModel g : mArrayList){
                for (GalleryModel gg : g.getmValue()){
                    if (gg.getName().equalsIgnoreCase(img)){
                        deletePic(img.split("https://files.merudy.com/projects/")[1]);
                    }
                }
            }
        } else {


            for (int i = 0; i < mArrayList.size() ; i++) {
                for (int j = 0; j < mArrayList.get(i).getmValue().size() ; j++) {
                    if (mArrayList.get(i).getmValue().get(j).getName().equalsIgnoreCase(img)){
                        mArrayList.get(i).getmValue().remove(mArrayList.get(i).getmValue().get(j));
                    }
                }
            }

            int countValueImage = 0;
            for (GroupGalleryModel g : mArrayList){
                for (GalleryModel gg : g.getmValue()){
                    countValueImage++;
                }
            }


            if (countValueImage > 0) {
                save.setVisibility(View.VISIBLE);
                txtAlert.setVisibility(View.GONE);
            } else {
                mArrayList = new ArrayList<>();
                txtAlert.setVisibility(View.VISIBLE);
            }
            adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, TakePhotoActivity.this);
            reShowPhoto.setLayoutManager(new LinearLayoutManager(TakePhotoActivity.this));
            reShowPhoto.setAdapter(adapter);

            showToast(TakePhotoActivity.this, getResources().getString(R.string.toast_delete_image_success));
        }
    }

    @Override
    public void onShowPhoto(String img) {
        final Dialog dialog = new Dialog(TakePhotoActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.view_image);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
        dialog.getWindow().setLayout(width, height);
        SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
        Button close = dialog.findViewById(R.id.close);
        RelativeLayout hideView = dialog.findViewById(R.id.hideView);

        new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... voids) {
                if (Utils.project_id.length() > 0){
                    try {
                        URL url = new URL(img);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);
                        return myBitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }else{
                    File f = new File(img);
                    Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                    return d;
                }
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                image.setImage(ImageSource.bitmap(bitmap));
                image.setMaxScale(20.0f);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                hideView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }.execute();
    }

    private Call<String> callAddPic(List<String> listPic) {
        return rudyService.addPic(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                listPic
        );
    }

    private void addPic(List<String> _listImg) {
        showLoading(this);
        if (_listImg.size() > 0) {
            callAddPic(_listImg).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideLoading();
                    Gson gson = new Gson();
                    String resp = response.body();
                    if (resp.contains("200")) {
                        ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
                        Items items = projectDetail.getItems();

                        mArrayList = new ArrayList<>();

                        ArrayList<GroupGalleryModel> groupGalleryModels = new ArrayList<>();

                        Map<String,ArrayList<GalleryModel>> groupImage = new HashMap<>();

                        for(GalleryModel p : items.getGalleryNew()){
                            if(!groupImage.containsKey(p.getCkId())){
                                groupImage.put(p.getCkId(), new ArrayList<>());
                            }
                            groupImage.get(p.getCkId()).add(p);
                        }

                        for (Map.Entry<String, ArrayList<GalleryModel>> map : groupImage.entrySet()){
                            groupGalleryModels.add(new GroupGalleryModel(map.getKey(), map.getValue()));
                        }

                        Collections.sort(groupGalleryModels, new Comparator<GroupGalleryModel>() {
                            @Override
                            public int compare(GroupGalleryModel o1, GroupGalleryModel o2) {
                                return o1.getmKey().compareTo(o2.getmKey());
                            }
                        });

                        mArrayList = groupGalleryModels;

                        int countValueImage = 0;
                        for (GroupGalleryModel g : mArrayList){
                            for (GalleryModel gg : g.getmValue()){
                                countValueImage++;
                            }
                        }

                        if (countValueImage > 0) {
                            save.setVisibility(View.VISIBLE);
                            txtAlert.setVisibility(View.GONE);
                        } else {
                            txtAlert.setVisibility(View.VISIBLE);
                        }

                        adapter = new GroupShowPhotoAdapter(TakePhotoActivity.this, mArrayList, TakePhotoActivity.this);
                        reShowPhoto.setLayoutManager(new LinearLayoutManager(TakePhotoActivity.this));
                        reShowPhoto.setAdapter(adapter);

                        showToast(TakePhotoActivity.this, getResources().getString(R.string.toast_save_image_success));

                    } else {
                        APIError error = gson.fromJson(resp, APIError.class);
                        showToast(TakePhotoActivity.this, error.getItems());
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    hideLoading();
                    showToast(TakePhotoActivity.this, t.getMessage());
                }
            });
        }
    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
