
package com.scg.rudy.ui.main.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Items {

    @SerializedName("count")
    private String mCount;

    public String getCount() {
        return mCount;
    }

    public void setCount(String count) {
        mCount = count;
    }

}
