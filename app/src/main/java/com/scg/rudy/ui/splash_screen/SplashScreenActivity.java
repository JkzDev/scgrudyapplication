package com.scg.rudy.ui.splash_screen;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.scg.rudy.BuildConfig;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.login.LoginActivity;
import com.scg.rudy.ui.call_to_customer.CallToCustomerActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.hover.service.RudyHoverMenuService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import timber.log.Timber;

import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.savePrefer;
import static com.scg.rudy.utils.Utils.setLang;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class SplashScreenActivity extends BaseActivity {
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private static final String TAG = "SplashScreenActivity";
    private static final String VERSION_KEY = "version";
    private PackageInfo pInfo;
    private int deviceVersionCode;
    private int deviceVersionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setNavigationBarTintEnabled(true);
        setContentView(R.layout.activity_splash_screen);

        //for support multiple language
        Utils.APP_LANGUAGE = Utils.getLang(this);
        setLang(this, Utils.APP_LANGUAGE);
        Utils.currencyForShow = Utils.getCurrencyForShow(this);
        Locale myLocale = new Locale(Utils.APP_LANGUAGE);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        Resources resources = getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
        //for support multiple language

//        installAPK atualizaApp = new installAPK();
//        atualizaApp.setContext(getApplicationContext());
//        atualizaApp.execute(new String[]{"rudy.apk"});
//        loadApk("rudy.apk",this);
//        runNextActivity();


    }

  @Override
  protected void onResume() {
        super.onResume();
        setmFirebaseRemoteConfig();
        getDeviceVersion();
        stopService(new Intent(this, RudyHoverMenuService.class));
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        stopService(new Intent(this, RudyHoverMenuService.class));
    }

    private void getDeviceVersion(){
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            deviceVersionCode = pInfo.versionCode;
            if(BuildConfig.DEBUG){
                deviceVersionName = 19;
            }else{
                deviceVersionName = Integer.parseInt(pInfo.versionName.replace(".",""));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        fetchWelcome();
    }

    private void setmFirebaseRemoteConfig(){
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
    }

    private void fetchWelcome() {
        long cacheExpiration = 3600;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        mFirebaseRemoteConfig.activateFetched();
                    } else {
                        runNextActivity();
                    }
                    displayWelcomeMessage();
                });
    }
    private void displayWelcomeMessage() {
         int serverVersionCode = deviceVersionCode;
         int serverVersionName = deviceVersionName;
        String welcomeMessage = mFirebaseRemoteConfig.getString(VERSION_KEY);
        try {
            JSONObject welcomObj = new JSONObject(welcomeMessage);
            serverVersionCode = Integer.parseInt(welcomObj.optString("build",String.valueOf(deviceVersionCode)));
            serverVersionName = Integer.parseInt(welcomObj.optString("app_version",pInfo.versionName).replace(".",""));
        } catch (JSONException e) {
            e.printStackTrace();
            LogException(welcomeMessage ,e);
        }
        if(deviceVersionName==serverVersionName){
            if(deviceVersionCode>=serverVersionCode){
                runNextActivity();
            }else{
                savePrefer(SplashScreenActivity.this, PERF_LOGIN, "");
                showUpdateDialog();
            }
        }else{
            if(deviceVersionCode>=serverVersionCode){
                runNextActivity();
            }else{
                savePrefer(SplashScreenActivity.this, PERF_LOGIN, "");
                showUpdateDialog();
            }
        }
    }

    private void showUpdateDialog(){
        MaterialDialog.Builder builder =  new MaterialDialog.Builder(this)
                .title(getResources().getString(R.string.dialog_pls_update_version))
                .content(getResources().getString(R.string.dialog_rudy_have_update_new_version))
                .positiveText(getResources().getString(R.string.agreed))
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    savePrefer(SplashScreenActivity.this, PERF_LOGIN, "");
                    dialog.dismiss();
                    finish();
                });

        MaterialDialog del_dialog = builder.build();
        del_dialog.show();
    }

    private void runNextActivity(){
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this, CallToCustomerActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this, DepartmentDataNewActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this, SaleWalletActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this, TransactionDetailActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this,  ProjectDetailActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this,  ViewDetailActivity.class));
//                startActivity(new Intent(SplashScreenActivity.this,  CalendarActivity.class));
            finish();
        }, 2 * 1000);
    }

    public class installAPK extends AsyncTask<String, Void, Void> {
        private Context context;
        public void setContext(Context contextf){
            context = contextf;
        }


        @Override
        protected Void doInBackground(String... arg0) {
            try {
                String nameapp =  arg0[0];
                HttpURLConnection c = (HttpURLConnection) new URL("https://dl.dropboxusercontent.com/s/0wauv8uug2x00ik/rudy.apk").openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                File file = new File("/mnt/sdcard/Download/");
                file.mkdirs();
                File outputFile = new File(file, nameapp);
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
                while (true) {
                    int len1 = is.read(buffer);
                    if (len1 == -1) {
                        break;
                    }
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();
                Intent intent = new Intent("android.intent.action.VIEW");
                Uri photoURI = FileProvider.getUriForFile(SplashScreenActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createApkFile(nameapp));
//                intent.setDataAndType(FileProvider.getUriForFile(SplashScreenActivity.this, BuildConfig.APPLICATION_ID + ".provider", new File("/mnt/sdcard/Download/" + nameapp)), "application/vnd.android.package-archive");
                intent.setDataAndType(photoURI, "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(268435456);
                context.startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(SplashScreenActivity.this,"Update error! " + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            return null;
        }
    }

//    @SuppressLint("StaticFieldLeak")
//    private void loadApk(final String nameapp,Context context) {
//        ProgressDialog dialogue;
//        dialogue = new ProgressDialog(context);
//        dialogue.setTitle("Loading items..");
//
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                dialogue.show();
//            }
//
//            @Override
//            protected String doInBackground(Void... voids) {
//                try {
//                    HttpURLConnection c = (HttpURLConnection) new URL("https://dl.dropboxusercontent.com/s/0wauv8uug2x00ik/rudy.apk").openConnection();
//                    c.setRequestMethod("GET");
//                    c.setDoOutput(true);
//                    c.connect();
//                    File file = new File("/mnt/sdcard/Download/");
//                    file.mkdirs();
//                    File outputFile = new File(file, nameapp);
//                    if (outputFile.exists()) {
//                        outputFile.delete();
//                    }
//                    FileOutputStream fos = new FileOutputStream(outputFile);
//                    InputStream is = c.getInputStream();
//                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
//                    while (true) {
//                        int len1 = is.read(buffer);
//                        if (len1 == -1) {
//                            break;
//                        }
//                        fos.write(buffer, 0, len1);
//                    }
//                    fos.close();
//                    is.close();
//                } catch (Exception e) {
//                    Toast.makeText(SplashScreenActivity.this,"Update error! " + e.getMessage(),Toast.LENGTH_LONG).show();
//                    Timber.tag("UpdateAPP").e("Update error! " + e.getMessage());
//                }
//
//                return null;
//            }
//            @Override
//            protected void onProgressUpdate(Void... progress) {
//            }
//
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialogue.dismiss();
//                    try {
//
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                            Uri apkUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider",
//                                    createApkFile(nameapp));
//                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
//                            intent.setDataAndType(apkUri,"application/vnd.android.package-archive");
////                            intent.setType("application/vnd.android.package-archive");
//                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.setFlags(268435456);
//                            context.startActivity(intent);
////                            File toInstall = createApkFile(nameapp);
////                            Uri apkUri = Uri.fromFile(toInstall);
////                            Intent intent = new Intent(Intent.ACTION_VIEW);
////                            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
////                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                            intent.setFlags(268435456);
////                            context.startActivity(intent);
//                        } else {
//                            File toInstall = createApkFile(nameapp);
//                            Uri apkUri = Uri.fromFile(toInstall);
//                            Intent intent = new Intent(Intent.ACTION_VIEW);
//                            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.setFlags(268435456);
//                            context.startActivity(intent);
//                        }
//                    } catch (Exception e) {
//                        Toast.makeText(SplashScreenActivity.this,"Update error! " + e.getMessage(),Toast.LENGTH_LONG).show();
//                        Log.e("UpdateAPP", "Update error! " + e.getMessage());
//                    }
//            }
//        }.execute();
//    }

    private File createApkFile(String nameapp) throws IOException {
        return new File("/mnt/sdcard/Download/" + nameapp);
    }

}
