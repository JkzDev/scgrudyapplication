
package com.scg.rudy.ui.adapter.QOModels;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Items {

    @SerializedName("boq")
    private String mBoq;
    @SerializedName("branch_stock")
    private String mBranchStock;
    @SerializedName("cate_id")
    private String mCateId;
    @SerializedName("class_id")
    private String mClassId;
    @SerializedName("color_name")
    private String mColorName;
    @SerializedName("color_shade")
    private String mColorShade;
    @SerializedName("dc_stock")
    private String mDcStock;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("description_short")
    private String mDescriptionShort;
    @SerializedName("gallery")
    private List<Gallery> mGallery;
    @SerializedName("id")
    private String mId;
    @SerializedName("img")
    private String mImg;
    @SerializedName("material_group")
    private String mMaterialGroup;
    @SerializedName("material_name")
    private String mMaterialName;
    @SerializedName("model")
    private String mModel;
    @SerializedName("name")
    private String mName;
    @SerializedName("pattern")
    private String mPattern;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("price2")
    private String mPrice2;
    @SerializedName("price3")
    private String mPrice3;
    @SerializedName("promotion_detail_id")
    private String mPromotionDetailId;
    @SerializedName("promotion_end_date")
    private String mPromotionEndDate;
    @SerializedName("promotion_start_date")
    private String mPromotionStartDate;
    @SerializedName("recommend")
    private String mRecommend;
    @SerializedName("series")
    private String mSeries;
    @SerializedName("shop_id")
    private String mShopId;
    @SerializedName("sku_code")
    private String mSkuCode;
    @SerializedName("stock")
    private String mStock;
    @SerializedName("style")
    private String mStyle;
    @SerializedName("texture")
    private String mTexture;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("unit_list")
    private List<UnitList> mUnitList;

    public String getBoq() {
        return mBoq;
    }

    public void setBoq(String boq) {
        mBoq = boq;
    }

    public String getBranchStock() {
        return mBranchStock;
    }

    public void setBranchStock(String branchStock) {
        mBranchStock = branchStock;
    }

    public String getCateId() {
        return mCateId;
    }

    public void setCateId(String cateId) {
        mCateId = cateId;
    }

    public String getClassId() {
        return mClassId;
    }

    public void setClassId(String classId) {
        mClassId = classId;
    }

    public String getColorName() {
        return mColorName;
    }

    public void setColorName(String colorName) {
        mColorName = colorName;
    }

    public String getColorShade() {
        return mColorShade;
    }

    public void setColorShade(String colorShade) {
        mColorShade = colorShade;
    }

    public String getDcStock() {
        return mDcStock;
    }

    public void setDcStock(String dcStock) {
        mDcStock = dcStock;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescriptionShort() {
        return mDescriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        mDescriptionShort = descriptionShort;
    }

    public List<Gallery> getGallery() {
        return mGallery;
    }

    public void setGallery(List<Gallery> gallery) {
        mGallery = gallery;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImg() {
        return mImg;
    }

    public void setImg(String img) {
        mImg = img;
    }

    public String getMaterialGroup() {
        return mMaterialGroup;
    }

    public void setMaterialGroup(String materialGroup) {
        mMaterialGroup = materialGroup;
    }

    public String getMaterialName() {
        return mMaterialName;
    }

    public void setMaterialName(String materialName) {
        mMaterialName = materialName;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPattern() {
        return mPattern;
    }

    public void setPattern(String pattern) {
        mPattern = pattern;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getPrice2() {
        return mPrice2;
    }

    public void setPrice2(String price2) {
        mPrice2 = price2;
    }

    public String getPrice3() {
        return mPrice3;
    }

    public void setPrice3(String price3) {
        mPrice3 = price3;
    }

    public String getPromotionDetailId() {
        return mPromotionDetailId;
    }

    public void setPromotionDetailId(String promotionDetailId) {
        mPromotionDetailId = promotionDetailId;
    }

    public String getPromotionEndDate() {
        return mPromotionEndDate;
    }

    public void setPromotionEndDate(String promotionEndDate) {
        mPromotionEndDate = promotionEndDate;
    }

    public String getPromotionStartDate() {
        return mPromotionStartDate;
    }

    public void setPromotionStartDate(String promotionStartDate) {
        mPromotionStartDate = promotionStartDate;
    }

    public String getRecommend() {
        return mRecommend;
    }

    public void setRecommend(String recommend) {
        mRecommend = recommend;
    }

    public String getSeries() {
        return mSeries;
    }

    public void setSeries(String series) {
        mSeries = series;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String shopId) {
        mShopId = shopId;
    }

    public String getSkuCode() {
        return mSkuCode;
    }

    public void setSkuCode(String skuCode) {
        mSkuCode = skuCode;
    }

    public String getStock() {
        return mStock;
    }

    public void setStock(String stock) {
        mStock = stock;
    }

    public String getStyle() {
        return mStyle;
    }

    public void setStyle(String style) {
        mStyle = style;
    }

    public String getTexture() {
        return mTexture;
    }

    public void setTexture(String texture) {
        mTexture = texture;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public List<UnitList> getUnitList() {
        return mUnitList;
    }

    public void setUnitList(List<UnitList> unitList) {
        mUnitList = unitList;
    }

}
