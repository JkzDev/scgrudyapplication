package com.scg.rudy.ui.call_to_customer;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Adapter.CallAllAdapter;
import com.scg.rudy.ui.call_to_customer.Models.CallAllModel;

import java.util.ArrayList;

public class CallSuccessFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_call_success, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.revCallSuccess);
        ArrayList<CallAllModel> callAllItemList = new ArrayList<>();
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));
        callAllItemList.add(new CallAllModel(getResources().getDrawable(R.drawable.thai_tag), "jackiezzz", "Rudy"));

        CallAllAdapter adapter = new CallAllAdapter(this.getContext(), callAllItemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        return v;
    }
}
