package com.scg.rudy.ui.department_photo;

import android.graphics.Bitmap;

public interface TakePhotoInterface {
    void onClickDeletePhoto(String img);
    void onShowPhoto(String img);
}
