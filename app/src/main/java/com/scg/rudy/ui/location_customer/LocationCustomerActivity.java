package com.scg.rudy.ui.location_customer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.by_location_new.ItemsItem;
import com.scg.rudy.model.pojo.by_location_new.NearLocation;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.location_customer.MapItem.mapItem;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.main.Models.CountEyeModel;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.hover.introduction.HoverMotion;

import net.sharewire.googlemapsclustering.Cluster;
import net.sharewire.googlemapsclustering.ClusterManager;
import net.sharewire.googlemapsclustering.DefaultIconGenerator;
import net.sharewire.googlemapsclustering.IconGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.showToast;

//import com.google.maps.android.clustering.ClusterManager;

/**
 * @author jackie
 */
public class LocationCustomerActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    public LocationManager mLocationManager;
    public static final String TAG = "LocationCustomerActivity";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    protected GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private LatLng pos;
    private Location location;
    private LocationListener mLocationListener;
    private Marker mMarker;
    private LocationManager lm;
    private double lat, lng;
    private ImageView clsBtn;
    private TextView address;

    private TextView titleName;
    private RelativeLayout latlngLayout;
    private LinearLayout typePhase;
    private File mFileTemp;
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private String pickerPath;
    private Bitmap bitmapProfile = null;
    private ImageView placeImage;
    private String cateId = "";
    private String phaseId = "";
    private String user_id = "";
    public static String shopID = "";
    private String profileBase64;
    private String iname = "";
    private LatLng coordinate;
    private LinearLayout curBtn;

    private TextView latlng;
    private Double currentLat = 0.0, currentLng = 0.0;
    private Double DragLat = 0.0, DragLng = 0.0;
    private Dialog dialogLoader;
    private boolean isLoad = false;
    private ClusterManager<mapItem> mClusterManager;

    private RudyService rudyService;
    private ArrayList<mapItem> locationList;

    private ImageView imgAlertEye;
    private TextView clsAlertEye;
    private TextView textHeadAlertEye;
    private TextView textDetailAlertEye;
    private RelativeLayout reAlertEye;
    private int CountEye;
    private Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_customer);
        setEnviroment();
        initView();

        rudyService = ApiHelper.getClient();

        extras = getIntent().getExtras();
        if (extras != null) {
            CountEye = extras.getInt("CountEye");
        }

        dialogLoader = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLoader.setCancelable(true);
        dialogLoader.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialogLoader, this);

        //for load not zoom
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        try {
            mLocationListener =
                    new LocationListener() {
                        @Override
                        public void onLocationChanged(Location loc) {
                            currentLat = loc.getLatitude();
                            currentLng = loc.getLongitude();
                            DragLat = currentLat;
                            DragLng = currentLng;
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                        }
                    };
        } catch (Exception e) {
            e.printStackTrace();
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handlePermissionsAndGetLocation();
            }
        }, 2000);

        setClickAction();

        //for dialog alert count eye
        imgAlertEye.setImageDrawable(getResources().getDrawable(R.drawable.alert_eye));
        if (CountEye > 0){
            textHeadAlertEye.setText(CountEye + " " + getResources().getString(R.string.alert_eyes_header));
        }else{
            reAlertEye.setVisibility(View.GONE);
        }
        textDetailAlertEye.setText(getResources().getString(R.string.alert_eyes_detail));
        clsAlertEye.setOnClickListener(this);
    }

    public void setEnviroment() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
    }


    public void setClickAction() {
        clsBtn.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void handlePermissionsAndGetLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
            getLocation();
        } else {
            getLocation();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Toast.makeText(this, "LOCATION Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mClusterManager = new ClusterManager<mapItem>(this, mMap);
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this::onInfoWindowClick);
        mMap.setMyLocationEnabled(true);
        final Location location = getLastKnownLocation();
        if (location != null) {
            currentLat = location.getLatitude();
            currentLng = location.getLongitude();
            DragLat = currentLat;
            DragLng = currentLng;
            try {
                Geocoder gcd = new Geocoder(LocationCustomerActivity.this, new Locale(Utils.getLang(LocationCustomerActivity.this)));
                List<Address> addressesMap = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addressesMap.size() > 0) {
                    String location_all = addressesMap.get(0).getFeatureName() + ", " + addressesMap.get(0).getSubAdminArea() + ", " + addressesMap.get(0).getAdminArea();
                    address.setText(location_all.replace("null,", "").replace(",,", ""));
                    latlng.setText(DragLat + ", " + DragLng);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(DragLat, DragLng), 18.0f));
            if (!isLoad) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadLocation(dialogLoader);
                                        }
                                    },
                        2000);
            } else {
                dialogLoader.dismiss();
            }
        }

        @SuppressLint("ResourceType") final View myLocationButton = mapFragment.getView().findViewById(0x2);
        myLocationButton.setVisibility(View.INVISIBLE);
        curBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Geocoder gcd = new Geocoder(LocationCustomerActivity.this, new Locale(Utils.getLang(LocationCustomerActivity.this)));
                    List<Address> addressesMap = gcd.getFromLocation(DragLat, DragLng, 1);
                    if (addressesMap.size() > 0) {
                        String location_all = addressesMap.get(0).getFeatureName() + ", " + addressesMap.get(0).getSubAdminArea() + ", " + addressesMap.get(0).getAdminArea();
                        address.setText(location_all.replace("null,", "").replace(",,", ""));
                        latlng.setText(DragLat + ", " + DragLng);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                myLocationButton.performClick();
            }
        });
    }


    private void addItems(List<ItemsItem> items, Dialog dialog) {
        if (mMap != null) {
            mMap.clear();
            Utils.DialogLoader(dialog, this);
            double lat = 13.8017851;
            double lng = 100.5368969;
            locationList = new ArrayList<>();
            for (int i = 0; i < items.size(); i++) {
                if (!items.get(i).getLatLng().isEmpty()) {
                    String[] ltlng = items.get(i).getLatLng().split(",");
                    lat = Double.parseDouble(ltlng[0]);
                    lng = Double.parseDouble(ltlng[1]);

                    mapItem offsetItem = new mapItem(lat, lng, items.get(i).getProjectName(),
                            getResources().getString(R.string.create_location_by) + " : " + items.get(i).getUser_name(),
                            items.get(i).getId(),
                            items.get(i).getProjectName(),
                            items.get(i).getAppName(),
                            items.get(i).getPin(),
                            items.get(i).getPic(),
                            items.get(i).getProjectAddress(),
                            items.get(i).getBiz_id(),
                            items.get(i).isIs_eyes(),
                            items.get(i).isPulled()

                    );
                    locationList.add(offsetItem);
                }
            }
            mClusterManager = new ClusterManager<mapItem>(this, mMap);
            mMap.setOnCameraIdleListener(mClusterManager);
            mClusterManager.setItems(locationList);
            mClusterManager.setIconGenerator(new IconGenerator<mapItem>() {
                @NonNull
                @Override
                public BitmapDescriptor getClusterIcon(@NonNull Cluster<mapItem> cluster) {
                    DefaultIconGenerator df = new DefaultIconGenerator(LocationCustomerActivity.this);
                    BitmapDescriptor dfIcon = df.getClusterIcon(cluster);
                    return dfIcon;
                }

                @NonNull
                @Override
                public BitmapDescriptor getClusterItemIcon(@NonNull mapItem clusterItem) {

                    BitmapDescriptor eyes = BitmapDescriptorFactory.fromResource(R.drawable.eyes);
                    BitmapDescriptor move = BitmapDescriptorFactory.fromResource(R.drawable.move);
                    BitmapDescriptor conn = BitmapDescriptorFactory.fromResource(R.drawable.eyes_accept);

                    if (clusterItem.getmAppName().contains("eye")) {
                        if (clusterItem.isPulled()) {
                            return conn;
                        } else {
                            return eyes;
                        }
                    } else {
                        return move;
                    }
                }
            });
            dialog.dismiss();
        }
    }

    //new by kitto
    private Call<String> pullEyeproject(String _biz_id, boolean _is_eye, String _projectID) {

        String json = Utils.getPrefer(getApplicationContext(), PERF_LOGIN);
        UserPOJO userPOJO = UserPOJO.create(json);

        return rudyService.pullEye(
                Utils.APP_LANGUAGE,
                userPOJO.getItems().getId(),
                _biz_id,
                _projectID,
                shopID,
                _is_eye
        );
    }

    public void onShowEyes(Marker marker) {
        final HoverMotion mHoverMotion = new HoverMotion();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.view_eyes_accept);
        final RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        final RelativeLayout accept = dialog.findViewById(R.id.accept);
        final ImageView rudy_logo = dialog.findViewById(R.id.rudy_logo);
        mHoverMotion.start(rudy_logo);

//        for get some data
        Cluster m = (Cluster) marker.getTag();
        ArrayList<mapItem> items = (ArrayList<mapItem>) m.getItems();
        mapItem item = items.get(0);


        final RelativeLayout hideView = dialog.findViewById(R.id.hideView);
        hideView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHoverMotion.stop();
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHoverMotion.stop();
                dialog.dismiss();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showToast(LocationCustomerActivity.this,getString(R.string.service_not_available));
                pullEyeproject(item.getBiz_id(), item.isIs_eyes(), item.getId()).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {

                        Gson gson = new Gson();
                        String resp = response.body();
                        if (resp.contains("200")) {

                            mHoverMotion.stop();
                            dialog.dismiss();

                            final Dialog dialogAlert = new Dialog(LocationCustomerActivity.this);
                            dialogAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialogAlert.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            dialogAlert.setContentView(R.layout.dialog_alert_pull_eyes);
                            dialogAlert.setCanceledOnTouchOutside(false);

                            TextView head = dialogAlert.findViewById(R.id.Header);
                            TextView detail = dialogAlert.findViewById(R.id.Detail);
                            RelativeLayout agree = dialogAlert.findViewById(R.id.accept);

                            agree.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogAlert.dismiss();
                                    Utils.DialogLoader(dialogLoader, LocationCustomerActivity.this);
                                    dialogLoader.isShowing();
                                    loadLocation(dialogLoader);
                                }
                            });

                            head.setText(getResources().getString(R.string.alert_pull_eyes_header));
                            detail.setText(getResources().getString(R.string.alert_pull_eyes_detail));

                            dialogAlert.show();
                        } else {
                            APIError error = gson.fromJson(resp, APIError.class);
                            showToast(LocationCustomerActivity.this, error.getItems());
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        showToast(LocationCustomerActivity.this, t.getMessage());
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        boolean isNetwork = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Location bestLocation = null;
        int time = 20 * 1000 * 60;
        if (isNetwork) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, time, 15, mLocationListener);
            bestLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        if (bestLocation == null) {
            if (isGPS) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, 15, mLocationListener);
                bestLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }


        return bestLocation;
    }

    @SuppressLint("LongLogTag")
    protected void getLocation() {
        Log.v(TAG, "GetLocation");
        int LOCATION_REFRESH_TIME = 5 * 60 * 1000;
        int LOCATION_REFRESH_DISTANCE = 5;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                Log.v("WEAVER_", "Has permission");
                mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(this);
                }
            } else {
                Log.v("WEAVER_", "Does not have permission");
                Toast.makeText(this, "LOCATION Denied", Toast.LENGTH_SHORT).show();
//                finish();
            }
        } else {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, mLocationListener);
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
            mapFragment.getMapAsync(this);
        }
    }


    //new by kitto
    private Observable<String> callLocation() {
        return rudyService.location(
                Utils.APP_LANGUAGE,
                user_id,
                DragLat.toString(),
                DragLng.toString()
        );
    }

    @SuppressLint("CheckResult")
    private void loadLocation(Dialog dialogLoader) {
        callLocation().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                dialogLoader.dismiss();
                bindLocation(s);
            }
        });

    }


    private Call<String> callCountEye() {
        return rudyService.countEye(
                Utils.APP_LANGUAGE,
                user_id
        );
    }

    private void bindLocation(String s) {

        isLoad = true;
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);

        Gson gson = new Gson();
        String resp = s;
        if (resp.contains("200")) {
            NearLocation projectDetail = gson.fromJson(resp, NearLocation.class);
            if (projectDetail.getItems().size() > 0) {

                addItems(projectDetail.getItems(), dialog);

                callCountEye().enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        dialog.dismiss();
                        Gson gson = new Gson();
                        String resp2 = response.body();
                        if(resp2.contains("200")){
                            CountEyeModel countEyeModel = gson.fromJson(resp2, CountEyeModel.class);
                            String Count = countEyeModel.getItems().getCount();
                            if (Integer.parseInt(Count) > 0){
                                CountEye = Integer.parseInt(Count);
                                textHeadAlertEye.setText(CountEye + " " + getResources().getString(R.string.alert_eyes_header));
                                reAlertEye.setVisibility(View.VISIBLE);
                            }else{
                                CountEye = 0;
                                textHeadAlertEye.setText(CountEye + " " + getResources().getString(R.string.alert_eyes_header));
                                reAlertEye.setVisibility(View.GONE);
                            }
                        }else{
                            APIError error  = gson.fromJson(resp2, APIError.class);
                            showToast(LocationCustomerActivity.this, error.getItems());
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        dialog.dismiss();
                        showToast(LocationCustomerActivity.this, t.getMessage());
                    }
                });
            }
        } else {
            APIError error = gson.fromJson(resp, APIError.class);
            showToast(LocationCustomerActivity.this, error.getItems());
            dialog.dismiss();
        }
    }


//    @SuppressLint({"LongLogTag", "StaticFieldLeak"})
//    private void getNearLocation(final String url) {
//        isLoad = true;
//
//        Log.i(TAG, "getNearLocation : " + url);
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
//                    NearLocation projectDetail = gson.fromJson(string, NearLocation.class);
//                    if (projectDetail.getItems().size() > 0) {
////                        addMarkerView(arrayList);
//                        addItems(projectDetail.getItems(), dialog);
//                    }
//                } else {
//                    dialog.dismiss();
//                }
//            }
//        }.execute();
//    }

    private void startDropMarkerAnimation(final Marker marker) {
        final LatLng target = marker.getPosition();
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point targetPoint = proj.toScreenLocation(target);
        final long duration = (long) (200 + (targetPoint.y * 0.6));
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        startPoint.y = 0;
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later == 60 frames per second
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {

        if (v == clsBtn) {
            finish();
        }else if(v == clsAlertEye){
            reAlertEye.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStop() {
        super.onStop();
        mLocationManager.removeUpdates(mLocationListener);
        if (mapFragment != null) {
            try {
                mapFragment.onStop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(mLocationListener);
        if (mapFragment != null) {
            try {
                mapFragment.onDestroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void initView() {
        clsBtn = findViewById(R.id.cls_btn);

        address = findViewById(R.id.address);
        latlng = findViewById(R.id.latlng);
        titleName = findViewById(R.id.title_name);
        curBtn = findViewById(R.id.cur_btn);
        imgAlertEye = (ImageView) findViewById(R.id.imgAlertEye);
        clsAlertEye = (TextView) findViewById(R.id.clsAlertEye);
        textHeadAlertEye = (TextView) findViewById(R.id.textHeadAlertEye);
        textDetailAlertEye = (TextView) findViewById(R.id.textDetailAlertEye);
        reAlertEye = (RelativeLayout) findViewById(R.id.reAlertEye);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // You have to save path in case your activity is killed.
        // In such a scenario, you will need to re-initialize the CameraImagePicker
//        outState.putString("picker_path", pickerPath);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // After Activity recreate, you need to re-intialize these
        // two values to be able to re-intialize CameraImagePicker
//        if (savedInstanceState != null) {
//            if (savedInstanceState.containsKey("picker_path")) {
//                pickerPath = savedInstanceState.getString("picker_path");
//            }
//        }
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
//        marker.setTitle("TEST");
        marker.showInfoWindow();
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        Cluster m = (Cluster) marker.getTag();
        ArrayList<mapItem> items = (ArrayList<mapItem>) m.getItems();
        mapItem item = items.get(0);

        if (item.getmAppName().contains("move")) {
            Intent intent = new Intent(LocationCustomerActivity.this, ProjectDetailActivity.class);
            intent.putExtra("projectId", item.getId());
            intent.putExtra("name", item.getName());
            startActivity(intent);
        }
        if (item.getmAppName().contains("eye")) {

            if (item.isPulled()) {
//                showToast(LocationCustomerActivity.this, "หน่วยงานนี้ถูกรับเข้าแล้ว");
                Intent intent = new Intent(LocationCustomerActivity.this, ProjectDetailActivity.class);
                intent.putExtra("projectId", item.getId());
                intent.putExtra("name", item.getName());
                startActivity(intent);
            } else {
                onShowEyes(marker);
            }
        }
    }
}
