package com.scg.rudy.ui.sale_wallet;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.favorite.ListItem;

import java.util.ArrayList;

/**
 * @author DekDroidDev
 * @date 9/2/2018 AD
 */
public class SaleWalletRecyclerListAdapter extends RecyclerView.Adapter<SaleWalletRecyclerListAdapter.ProjectViewHolder> {

    private ArrayList<ListItem> cartModelArrayLis;
    private Context context;


    public SaleWalletRecyclerListAdapter(
            Context context,
            ArrayList<ListItem> cartModelArrayLis) {
        this.cartModelArrayLis = cartModelArrayLis;
        this.context = context;
    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.sale_wallet_item_list, parent, false);
        return new ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProjectViewHolder holder, int position) {
//        final ListItem prCodeDetailModel = cartModelArrayLis.get(position);
//        holder.name.setText(prCodeDetailModel.getName());
//        holder.boq.setText(prCodeDetailModel.getBOQ()+(prCodeDetailModel.getUnit().replace("\n","")));
//
//        if (prCodeDetailModel.getPrice() != null) {
//            holder.price.setText(
//                    Utils.moneyFormat(
//                            prCodeDetailModel.getPrice().toString().replace("null", "1")));
//        }
//
//
//
//        holder.add_cart.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, SkuConfirmActivity.class);
//                        intent.putExtra("skuId", prCodeDetailModel.getId());
//                        intent.putExtra("total", prCodeDetailModel.getBOQ().replace(",",""));
//                        intent.putExtra("price", prCodeDetailModel.getPrice());
//                        intent.putExtra("name", prCodeDetailModel.getName());
//                        intent.putExtra("unit_code", prCodeDetailModel.getUnit());
//                        intent.putExtra("stock", prCodeDetailModel.getStock());
//                        context.startActivity(intent);
//                    }
//                });
//
//        holder.remove_fav.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        listener.deleteFAvorite(prCodeDetailModel.getId());
//                    }
//                });
    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null) {
            return 50;
        }
        return cartModelArrayLis.size();
    }



    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        private TextView qoNumber;
        private TextView number;
        private TextView promo;
        private TextView totalMoney;
        private TextView percent;
        private TextView moneyReceive;
        private ImageView icoStat;
        private TextView txtStat;
        public ProjectViewHolder(View view) {
            super(view);
            qoNumber = view.findViewById(R.id.qo_number);
//            number = view.findViewById(R.id.number);
            promo = view.findViewById(R.id.promo);
            totalMoney = view.findViewById(R.id.total_money);
            percent = view.findViewById(R.id.percent);
            moneyReceive = view.findViewById(R.id.money_receive);
            icoStat = view.findViewById(R.id.ico_stat);
            txtStat = view.findViewById(R.id.txt_stat);
        }
    }
}
