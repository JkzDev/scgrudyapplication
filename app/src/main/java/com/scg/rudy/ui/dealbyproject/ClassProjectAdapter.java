package com.scg.rudy.ui.dealbyproject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ClassProjectAdapter extends ExpandableRecyclerViewAdapter<ClassViewHolder, SkuListViewHolder> {


  byProjectInterface listener;

  public ClassProjectAdapter(List<? extends ExpandableGroup> groups,byProjectInterface listener) {
    super(groups);
    this.listener = listener;
  }

  @Override
  public ClassViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
    final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_project_item, parent, false);
    return new ClassViewHolder(view);
  }

  @Override
  public SkuListViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
    final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sku_expand, parent, false);
    return new SkuListViewHolder(view,parent.getContext());
  }

  @Override
  public void onBindChildViewHolder(final SkuListViewHolder holder, final int flatPosition,final ExpandableGroup group,final  int childIndex) {
    final SkuItem skuItem = ((ProjectClass) group).getSkuItems().get(childIndex);
    holder.setSkuListValue(((ProjectClass) group).getSkuItems(),childIndex,skuItem,listener);

  }

  @Override
  public void onBindGroupViewHolder(final ClassViewHolder holder,final int flatPosition,final ExpandableGroup group) {
    List<SkuItem> skuItems =  ((ProjectClass) group).getSkuItems();

    holder.setGenreTitle(skuItems,group,listener);

  }



}
