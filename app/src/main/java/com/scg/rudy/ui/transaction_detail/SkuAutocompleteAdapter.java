/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.transaction_detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.ui.customer_data.customerInterface;
import com.scg.rudy.ui.customer_data.filter.ExtendedDictionaryAutocompleteFilter;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.customer_data.provider.ExtendedDictionaryAutocompleteProvider;
import com.scg.rudy.ui.transaction_detail.filter.SkuDictionaryAutocompleteFilter;
import com.scg.rudy.ui.transaction_detail.model.skuModel;
import com.scg.rudy.ui.transaction_detail.provider.SkuDictionaryAutocompleteProvider;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteFilterListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author jackie
 */
public class SkuAutocompleteAdapter extends ArrayAdapter<skuModel> implements OnDynamicAutocompleteFilterListener<skuModel> {

    private List<skuModel> mListItems;
    private SkuDictionaryAutocompleteProvider mExtendedDictionaryAutocompleteProvider;
    private SkuDictionaryAutocompleteFilter mDictionaryAutocompleteFilter;
    private LayoutInflater mLayoutInflater;
    private int mLayoutId;
    private QoInterface listener;
    private int tabIndex;
    public SkuAutocompleteAdapter(Context context, int textViewResourceId, QoInterface _listener, int _tabIndex) {
        super(context, textViewResourceId);
        mExtendedDictionaryAutocompleteProvider = new SkuDictionaryAutocompleteProvider();
        mDictionaryAutocompleteFilter = new SkuDictionaryAutocompleteFilter(mExtendedDictionaryAutocompleteProvider, this);
        mDictionaryAutocompleteFilter.useCache(true);
        mListItems = new ArrayList<>();
        mLayoutId = textViewResourceId;
        listener = _listener;
        tabIndex = _tabIndex;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void onDynamicAutocompleteFilterResult(Collection<skuModel> result) {
        mListItems.clear();
        mListItems.addAll(result);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mDictionaryAutocompleteFilter;
    }

    @Override
    public int getCount() {
        return mListItems.size();
    }

    @Override
    public void clear() {
        mListItems.clear();
    }

    @Override
    public skuModel getItem(int position) {
        return mListItems.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mLayoutId, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.price = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        skuModel countryModel = getItem(position);
        viewHolder.name.setText(countryModel.getName());
        viewHolder.price.setText(countryModel.getPrice());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSearchItemClick(countryModel);
            }
        });

        return convertView;
    }

    /**
     * Holder for list item.
     */
    private static class ViewHolder {
        private TextView name;
        private TextView price;
    }
}