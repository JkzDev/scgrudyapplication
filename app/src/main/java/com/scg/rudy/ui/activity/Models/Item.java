
package com.scg.rudy.ui.activity.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Item implements Comparable<Item> {

    @SerializedName("active")
    private String mActive;
    @SerializedName("id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("template_note")
    private String mTemplateNote;

    public String getActive() {
        return mActive;
    }

    public void setActive(String active) {
        mActive = active;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getTemplateNote() {
        return mTemplateNote;
    }

    public void setTemplateNote(String templateNote) {
        mTemplateNote = templateNote;
    }

    @Override
    public int compareTo(Item o) {
        return getId().compareTo(o.getId());
    }
}
