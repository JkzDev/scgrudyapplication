package com.scg.rudy.ui.main.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.main.Models.LanguageModel;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author jackie
 */
public class ChangeLanguageAdapter extends RecyclerView.Adapter<ChangeLanguageAdapter.ViewHolder> {

    private Context context;
    private ArrayList<LanguageModel> languageModel;

    public ChangeLanguageAdapter(Context _context, ArrayList<LanguageModel> _languageModel) {
        context = _context;
        languageModel = _languageModel;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView langImag;
        TextView langText;
        LinearLayout parent_layout;

        public ViewHolder(View view)
        {
            super(view);
            langImag = view.findViewById(R.id.langImage);
            langText = view.findViewById(R.id.langText);
            parent_layout = view.findViewById(R.id.parent_layout);
        }

    }

    @NonNull
    @Override
    public ChangeLanguageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChangeLanguageAdapter.ViewHolder holder, int position) {
        holder.langText.setText(languageModel.get(position).getLangText());
        holder.langImag.setImageDrawable(languageModel.get(position).getLangImage());

        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String lang = languageModel.get(position).getLangValue();
                Utils.setLang(context, lang);
                Utils.APP_LANGUAGE = Utils.getLang(context);
                Locale myLocale = new Locale(Utils.APP_LANGUAGE);
                Locale.setDefault(myLocale);
                Configuration config = new Configuration();
                config.locale = myLocale;
                Resources resources = v.getResources();
                resources.updateConfiguration(config, resources.getDisplayMetrics());

                Utils.setChangeLangCurrencyForShow(context);
                Utils.currencyForShow = Utils.getCurrencyForShow(context);

                Intent intent = ((MainActivity)context).getIntent();
                context.startActivity(intent);
                ((MainActivity)context).finish();

            }
        });
    }

    @Override
    public int getItemCount() {
        return languageModel.size();
    }
}
