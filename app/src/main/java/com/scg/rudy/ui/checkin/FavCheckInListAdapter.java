package com.scg.rudy.ui.checkin;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.checkin_history_list.ItemsItem;
import com.scg.rudy.ui.checkin_history_detail.CheckInHistoryDetailActivity;

import java.util.List;

/**
 *
 * @author DekDroidDev
 * @date 9/2/2018 AD
 */

public class FavCheckInListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> CheckInList;
    private Context context;
    private String projectId;
    FavCheckInListAdapter(Context context, List<ItemsItem> CheckInList, String projectId) {
        this.CheckInList = CheckInList;
        this.context = context;
        this.projectId = projectId;
//        for (int i = 0; i < qtListModelArrayList.size(); i++) {
//            this.qtListModelArrayList.remove(qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr"));
//        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        String itemViewType = CheckInList.get(getItemViewType(viewType)).getType();

        if (itemViewType.equalsIgnoreCase("1")) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.items_checkin_tel, parent, false);
            return new FavCheckInListAdapter.checkInTelHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(context).inflate(R.layout.items_checkin_onsite, parent, false);
            return new FavCheckInListAdapter.checkInOnSiteViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemsItem favCheckin = CheckInList.get(position);

        if (favCheckin.getType().equalsIgnoreCase("1")) {
            ((checkInTelHolder) holder).type_txt.setText(favCheckin.getTypeTxt());
            ((checkInTelHolder) holder).time.setText(favCheckin.getAddedDatetime());
            ((checkInTelHolder) holder).detail.setText(favCheckin.getDetail());
            ((checkInTelHolder) holder).history_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CheckInHistoryDetailActivity.class);
                    intent.putExtra("checkInID",favCheckin.getId());
                    intent.putExtra("projectId",projectId);


                    context.startActivity(intent);
                }
            });
        } else {
            ((checkInOnSiteViewHolder) holder).type_txt.setText(favCheckin.getTypeTxt());
            ((checkInOnSiteViewHolder) holder).time.setText(favCheckin.getAddedDatetime());
            ((checkInOnSiteViewHolder) holder).detail.setText(favCheckin.getDetail());
            ((checkInOnSiteViewHolder) holder).history_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CheckInHistoryDetailActivity.class);
                    intent.putExtra("checkInID",favCheckin.getId());
                    intent.putExtra("projectId",projectId);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (CheckInList == null)
            return 0;
        return CheckInList.size();
    }




    public class checkInTelHolder extends RecyclerView.ViewHolder {
        private TextView type_txt;
        private TextView time;
        private TextView detail;
        private RelativeLayout history_detail;

        public checkInTelHolder(View view) {
            super(view);
            type_txt = view.findViewById(R.id.type_txt);
            time = view.findViewById(R.id.time);
            detail = view.findViewById(R.id.detail);
            history_detail = view.findViewById(R.id.history_detail);
        }
    }

    public class checkInOnSiteViewHolder extends RecyclerView.ViewHolder {
        private TextView type_txt;
        private TextView time;
        private TextView detail;
        private RelativeLayout history_detail;
        public checkInOnSiteViewHolder(View view) {
            super(view);
            type_txt = view.findViewById(R.id.type_txt);
            time = view.findViewById(R.id.time);
            detail = view.findViewById(R.id.detail);
            history_detail = view.findViewById(R.id.history_detail);
        }
    }

}