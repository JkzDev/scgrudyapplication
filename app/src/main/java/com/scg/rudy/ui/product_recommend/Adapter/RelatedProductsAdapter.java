package com.scg.rudy.ui.product_recommend.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.ui.product_recommend.Models.RelatedProductsModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<RelatedProductsModel> relatedProductsModels;

    public RelatedProductsAdapter(Context _context, ArrayList<RelatedProductsModel> _relatedProductsModels) {
        context = _context;
        relatedProductsModels = _relatedProductsModels;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView RPImage;
        TextView RPText;

        public ViewHolder(View view)
        {
            super(view);
            RPImage = view.findViewById(R.id.RPImage);
            RPText = view.findViewById(R.id.RPText);
        }
    }


    @NonNull
    @Override
    public RelatedProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.related_products_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RelatedProductsAdapter.ViewHolder holder, int position) {

        if (relatedProductsModels.get(position).getImage() != null) {
            if (!relatedProductsModels.get(position).getImage().isEmpty()) {
                Glide.with(context)
                        .load(relatedProductsModels.get(position).getImage())
                        .into(holder.RPImage);
            }else{
                Glide.with(context).clear(holder.RPImage);
                holder.RPImage.setImageDrawable(context.getDrawable(R.drawable.bg_photo_copy));
            }
        }

        holder.RPText.setText(relatedProductsModels.get(position).getText());

    }

    @Override
    public int getItemCount() {
        return relatedProductsModels.size();
    }
}
