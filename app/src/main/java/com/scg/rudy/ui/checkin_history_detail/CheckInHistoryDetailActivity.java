package com.scg.rudy.ui.checkin_history_detail;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.checkin_detail.CheckInDetail;
import com.scg.rudy.model.pojo.checkin_detail.GalleryItem;
import com.scg.rudy.model.pojo.checkin_detail.Items;
import com.scg.rudy.model.pojo.checkin_detail.Phaseprocess;
import com.scg.rudy.model.pojo.checkin_detail.SubPhasesItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.autolabel.AutoLabelUI;
import com.scg.rudy.utils.custom_view.autolabel.AutoLabelUISettings;

import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.scg.rudy.utils.Utils.showToast;

public class CheckInHistoryDetailActivity extends BaseActivity implements View.OnClickListener {

  private TextView clsBtn;
  private AutoLabelUI labelView;
  private String checkInID;
  private RudyService rudyService;
  private Bundle extras;
  private TextView time;
  private ImageView statIco;
  private RelativeLayout status;
  private TextView detail;
  private TextView currPhase;
  private TextView checkTitle;
  private LinearLayout gallery;
  private LinearLayout galleryLayout;
  private LinearLayout noGallery;
  private Bitmap showBitmap;
  private TextView edit;

  private String projectId;
  private String type;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_check_in_history_detail);
    initView();
    setOnClick();
    setAutoLabelUISettings();
    getCheckInDetail();
  }

  private Call<String> getCheckinDetail() {
    return rudyService.getCheckInDetail(Utils.APP_LANGUAGE, checkInID);
  }

  private void getCheckInDetail() {
    showLoading(this);
    getCheckinDetail()
        .enqueue(
            new Callback<String>() {
              @Override
              public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                  CheckInDetail checkInDetail  = gson.fromJson(resp, CheckInDetail.class);
                  setCheckInData(checkInDetail);
                }else{
                  APIError error  = gson.fromJson(resp, APIError.class);
                  showToast(CheckInHistoryDetailActivity.this, error.getItems());
                }
              }

              @Override
              public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInHistoryDetailActivity.this, fetchErrorMessage(t));
              }
            });
  }

  private void setCheckInData(CheckInDetail checkInDetail) {
    labelView.removeAllViews();
    Phaseprocess phaseprocess = checkInDetail.getItems().getPhaseprocess();
    List<SubPhasesItem> subPhases = phaseprocess.getSubPhases();
    for (int i = 0; i < subPhases.size(); i++) {
      labelView.addLabel(
          subPhases.get(i).getSubPhaseName(), i, Integer.parseInt(subPhases.get(i).getStatus()));
    }

    if (checkInDetail.getItems().getType().equalsIgnoreCase("1")) {
      statIco.setImageDrawable(getDrawable(R.drawable.call_blue));
      status.setBackgroundColor(Color.parseColor("#4a90e2"));
      galleryLayout.setVisibility(GONE);
    } else {
      statIco.setImageDrawable(getDrawable(R.drawable.onsite_green));
      status.setBackgroundColor(Color.parseColor("#7cb342"));
    }

    type = checkInDetail.getItems().getType();

    checkTitle.setText(checkInDetail.getItems().getTypeTxt());
    time.setText(checkInDetail.getItems().getAddedDatetime());
    detail.setText(checkInDetail.getItems().getDetail());
    currPhase.setText(getResources().getString(R.string.phase) + phaseprocess.getPhaseId() + " " + phaseprocess.getPhaseName());

    if (checkInDetail.getItems().getGallery() != null) {
      if (checkInDetail.getItems().getGallery().size() > 0) {
        setGallery(checkInDetail.getItems().getGallery());
      } else {
        noGallery.setVisibility(View.VISIBLE);
      }

    } else {
      noGallery.setVisibility(View.VISIBLE);
    }
  }

  private void setGallery(final List<GalleryItem> galleryItemList) {
    gallery.removeAllViews();
    for (int i = 0; i < galleryItemList.size(); i++) {
      ImageView iv = new ImageView(this);
      iv.setAdjustViewBounds(true);
      Glide.with(this).load(galleryItemList.get(i).getName()).into(iv);

      ViewGroup.LayoutParams params =
          new ViewGroup.LayoutParams(
              ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      params.width = (int) getResources().getDimension(R.dimen._100sdp);
      params.height = (int) getResources().getDimension(R.dimen._100sdp);
      iv.setLayoutParams(params);
      iv.setAdjustViewBounds(true);
      iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
      final int finalI = i;
      iv.setOnClickListener(
          new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Glide.with(CheckInHistoryDetailActivity.this)
                  .asBitmap()
                  .load(galleryItemList.get(finalI).getName())
                  .into(
                      new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(
                            Bitmap resource, Transition<? super Bitmap> transition) {
                          //                        showBitmap = resource;
                          onShowPhoto(resource);
                        }
                      });
              //
              //                Glide.with(CheckInHistoryDetailActivity.this)
              //                        .load(detailItem.getGallery().get(finalI))
              //                        .into(imgProject);
              //                imgProject.setOnClickListener(new View.OnClickListener() {
              //                    @Override
              //                    public void onClick(View v) {
              //
              //                    }
              //                });
            }
          });

      iv.setPadding(0, 13, 13, 13);
      gallery.addView(iv);
    }
  }

  public void onShowPhoto(Bitmap bitmap) {
    if (bitmap != null) {
      final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
      dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      dialog
          .getWindow()
          .setLayout(
              LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
      dialog.setContentView(R.layout.view_image);
      dialog.setCancelable(true);
      int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
      int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
      dialog.getWindow().setLayout(width, height);
      SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
      Button close = dialog.findViewById(R.id.close);
      RelativeLayout hideView = dialog.findViewById(R.id.hideView);
      image.setImage(ImageSource.bitmap(bitmap));
      image.setMaxScale(20.0f);
      close.setOnClickListener(
          new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              dialog.dismiss();
            }
          });
      hideView.setOnClickListener(
          new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              dialog.dismiss();
            }
          });
      dialog.show();
    }
  }

  private CheckInDetail checkInResults(Response<CheckInDetail> response) {
    CheckInDetail checkInDetail = response.body();
    return checkInDetail;
  }

  private String fetchErrorMessage(Throwable throwable) {
    String errorMsg = getResources().getString(R.string.error_msg_unknown);
    if (!isNetworkConnected()) {
      errorMsg = getResources().getString(R.string.error_msg_no_internet);
    } else if (throwable instanceof TimeoutException) {
      errorMsg = getResources().getString(R.string.error_msg_timeout);
    }
    return errorMsg;
  }

  private void setOnClick() {
    clsBtn.setOnClickListener(this);
    edit.setOnClickListener(this);
  }

  private void setAutoLabelUISettings() {
    AutoLabelUISettings autoLabelUISettings =
        new AutoLabelUISettings.Builder()
            .withBackgroundResource(R.drawable.bg_frame_login)
            .withIconCross(R.drawable.cross)
            .withMaxLabels(20)
            .withShowCross(true)
            .withLabelsClickables(false)
            .withTextColor(R.color.black)
            .withTextSize(R.dimen.text_size_standard)
            .withLabelPadding(10)
            .build();

    labelView.setSettings(autoLabelUISettings);
  }


  public void onFragmentDetached(String tag) {}


  public void onError(int resId) {}


  public void onError(String message) {}


  public void showMessage(String message) {}

  public void showMessage(int resId) {}

  public boolean isNetworkConnected() {
    return true;
  }

  public void hideKeyboard() {}

  private void initView() {
    extras = getIntent().getExtras();
    rudyService = ApiHelper.getClient();
    if (extras != null) {
      checkInID = extras.getString("checkInID");
      projectId = extras.getString("projectId");
    }

    clsBtn = (TextView) findViewById(R.id.cls_btn);
    labelView = (AutoLabelUI) findViewById(R.id.label_view);
    time = (TextView) findViewById(R.id.time);
    statIco = (ImageView) findViewById(R.id.stat_ico);
    status = (RelativeLayout) findViewById(R.id.status);
    detail = (TextView) findViewById(R.id.detail);
    currPhase = (TextView) findViewById(R.id.curr_phase);
    checkTitle = (TextView) findViewById(R.id.check_title);
    gallery = (LinearLayout) findViewById(R.id.gallery);
    galleryLayout = (LinearLayout) findViewById(R.id.gallery_layout);
    noGallery = (LinearLayout) findViewById(R.id.no_gallery);
    edit = (TextView) findViewById(R.id.edit);
  }

  @Override
  public void onClick(View v) {
    if (v == clsBtn) {
      finish();
    }
    if (v == edit) {
        showInputCustomerDialog();
    }
  }

  public void showInputCustomerDialog() {
    final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog
        .getWindow()
        .setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    dialog.setContentView(R.layout.popup_edit_checkin);
    dialog.setCanceledOnTouchOutside(true);
    dialog.setCancelable(true);

    final EditText detail = dialog.findViewById(R.id.detail);
    RelativeLayout cancel =  dialog.findViewById(R.id.cancel);
    RelativeLayout submit =  dialog.findViewById(R.id.submit);
    dialog.show();
    submit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dialog.dismiss();
          postEditCheckInAPI(detail.getText().toString());

        }
    });

    cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
          dialog.dismiss();
      }
    });

  }

  private Call<String> postEditCheckin(String detail) {
    return rudyService.postEditCheckin(
            Utils.APP_LANGUAGE,
            checkInID,
            projectId,
            type,
            detail,
            "",
            ""
    );
  }


  private void postEditCheckInAPI(String detail) {
    showLoading(this);
    postEditCheckin(detail).enqueue(new Callback<String>() {
      @Override
      public void onResponse(Call<String> call, Response<String> response) {
        hideLoading();
        Gson gson = new Gson();
        String resp = response.body();
        if(resp.contains("200")){
          Items dashboardResults  = gson.fromJson(resp, Items.class);
          setCheckInData();
        }else{
          APIError error  = gson.fromJson(resp, APIError.class);
          showToast(CheckInHistoryDetailActivity.this, error.getItems());
        }
      }

      @Override
      public void onFailure(Call<String> call, Throwable t) {
        hideLoading();
        t.printStackTrace();
        showToast(CheckInHistoryDetailActivity.this,fetchErrorMessage(t));
      }
    });
  }

  private void setCheckInData(){
    showToast(CheckInHistoryDetailActivity.this,getResources().getString(R.string.toast_update_data_success));
    getCheckInDetail();
  }


  private Items fetchResults(Response<CheckInDetail> response) {
    CheckInDetail checkInDetail = response.body();
    return checkInDetail.getItems();
  }


}
