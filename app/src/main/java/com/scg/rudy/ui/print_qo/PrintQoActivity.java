package com.scg.rudy.ui.print_qo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.get_pdf_name.PdfName;
import com.scg.rudy.model.pojo.qo_to_so.ToSo;
import com.scg.rudy.model.pojo.transaction_detail.Transaction;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.edit_qo.EditQoActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;
import com.shockwave.pdfium.PdfDocument;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.webkit.WebSettings.LOAD_NO_CACHE;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class PrintQoActivity extends BaseActivity implements View.OnClickListener, OnPageChangeListener, OnLoadCompleteListener, OnPageErrorListener {
    private Bundle extras;
    private String url;
    private ImageView clsBtn;
    private TextView titleName;
    private RelativeLayout save;
    private RelativeLayout sent_file;
    private PDFView pdfView;
    private static final String TAG = PrintQoActivity.class.getSimpleName();
    String fileName = "/Download/temp.pdf";
    private WebView webView;
    public static final String PACKAGE_NAME = "jp.naver.line.android";
    public static final String CLASS_NAME = "jp.naver.line.android.activity.selectchat.SelectChatActivity";
    private List<ApplicationInfo> m_appList;
    final private int RC_PERMISSION_WRITE_EXTERNAL_STORAGE = 992;
    public static boolean fromCart = false;
    private File imageFile = null;
    public static int viewPdf = 0;
    private String transaction_id;
    private File pdfFlie = null;
    private RudyService rudyService;
    private RelativeLayout editQo;
    private String qoNumber = "";
    private PdfName customer;
    private int checkStat = 1, saveShare = 2;
    private RelativeLayout submit;
    private TextView textConfirmPrint;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_qo);
        rudyService = ApiHelper.getClient();

        if (Build.VERSION.SDK_INT >= 21) {
            WebView.enableSlowWholeDocumentDraw();
        }

        DealByPhaseActivity.print = true;
        extras = getIntent().getExtras();
        if (extras != null) {
            transaction_id = extras.getString("transaction_id");
        } else {
            transaction_id = "711";
        }

        url = ApiEndPoint.HOST + "/" + ApiEndPoint.Url + Utils.APP_LANGUAGE + "/transaction/" + transaction_id + "?act=qv2";



        initView();
        setOnclick();
    }

    private Call<String> getPdfName() {
        return rudyService.getPdfName(
                Utils.APP_LANGUAGE,
                transaction_id,
                "get_pdf_name"
        );
    }

    private Call<String> getToSO() {
        return rudyService.qoToSo(
                Utils.APP_LANGUAGE,
                transaction_id,
                "toSO"
        );
    }

    private void loadPdfName(boolean isSent, int state) {
        showLoading(this);
        getPdfName().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    PdfName pdfName = gson.fromJson(resp, PdfName.class);
                    if (state == checkStat) {
                        if (!pdfName.getItems().getStatus().equalsIgnoreCase("2")) {
                            editQo.setVisibility(View.GONE);
                            submit.setVisibility(View.GONE);
                        } else {
                            sent_file.setVisibility(View.VISIBLE);
                            save.setVisibility(View.VISIBLE);
                            editQo.setVisibility(View.VISIBLE);
                            submit.setVisibility(View.VISIBLE);
                        }
                        downloadPDF(url, isSent, "");
                    } else {

                        customer = pdfName;
                        fileName = "/download/" + customer.getItems().getPdfName() + ".pdf";
                        downloadPDF(url, isSent, getResources().getString(R.string.toast_download_doc_success));
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(PrintQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(PrintQoActivity.this, t.getMessage());
            }
        });
    }

    private void qoToSo() {
        showLoading(this);
        getToSO().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(PrintQoActivity.this, getResources().getString(R.string.toast_confirm_order_success));
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(PrintQoActivity.this, error.getItems());
                }
                loadPdfName(false, 1);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(PrintQoActivity.this, t.getMessage());
            }
        });
    }

    private PdfName fetchData(Response<PdfName> response) {
        return response.body();
    }

    private ToSo fetchToSoData(Response<ToSo> response) {
        return response.body();
    }

    private void onShowsConfirmToSo() {

        final Dialog dialog = new Dialog(PrintQoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.dialog_print_qo_alert);
        dialog.setCanceledOnTouchOutside(false);

        TextView alert = dialog.findViewById(R.id.Alert);
        TextView head = dialog.findViewById(R.id.Head);
        RelativeLayout agree = dialog.findViewById(R.id.Agree);

        alert.setText(getResources().getString(R.string.text_alert_to_champ));
        head.setText(getResources().getString(R.string.text_alert_to_champ_head));
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                qoToSo();
            }
        });

        dialog.show();


//        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
//                .title(getResources().getString(R.string.confirm_order))
//                .content(getResources().getString(R.string.dialog_confirm_order))
//                .positiveText(getResources().getString(R.string.agreed))
//                .negativeText(getResources().getString(R.string.cancel))
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        dialog.dismiss();
//                        qoToSo();
//
//                    }
//                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        dialog.dismiss();
//                    }
//                });
//
//        MaterialDialog del_dialog = builder.build();
//        del_dialog.show();
    }


    public void setOnclick() {
        save.setOnClickListener(this);
        sent_file.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        editQo.setOnClickListener(this);
        submit.setOnClickListener(this);
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void initView() {

        TransactionDetailActivity.transaction_id = "";
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        save = findViewById(R.id.print);
        sent_file = findViewById(R.id.save);
        webView = findViewById(R.id.webView);
        editQo = findViewById(R.id.edit_qo);
        pdfView = findViewById(R.id.pdfView);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(false);
        settings.setCacheMode(LOAD_NO_CACHE);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        webView.clearCache(true);
        webView.setInitialScale(1);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);

        submit = (RelativeLayout) findViewById(R.id.submit);
        textConfirmPrint = (TextView) findViewById(R.id.textConfirmPrint);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            TransactionDetailActivity.transaction_id = "";
            if (PrintQoActivity.fromCart) {
                MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                        .title(getResources().getString(R.string.dialog_qo_success))
                        .content(getResources().getString(R.string.dialog_want_to_leave_this_page))
                        .positiveText(getResources().getString(R.string.agreed))
                        .negativeText(getResources().getString(R.string.cancel))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                                Utils.savePrefer(PrintQoActivity.this, "print", "ok");
                                DealByPhaseActivity.print = true;
                                finish();

                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        });

                MaterialDialog del_dialog = builder.build();
                del_dialog.show();
            } else {
                TransactionDetailActivity.transaction_id = "";
                finish();
            }

        }

        if (v == submit) {
            onShowsConfirmToSo();
        }

        if (v == sent_file) {
            DealByPhaseActivity.print = true;
            handlePermissionsn2();
        }
        if (v == save) {
            DealByPhaseActivity.print = true;
            handlePermissionsn();
        }

        if (v == editQo) {
            Intent intent = new Intent(PrintQoActivity.this, EditQoActivity.class);
            intent.putExtra("transaction_id", transaction_id);
            intent.putExtra("qoNumber", qoNumber);
            startActivity(intent);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RC_PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Accepted
                    takeScreenshot();
                } else {
                    // Denied
                    Toast.makeText(this, "WRITE_EXTERNAL_STORAGE Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void sendLineText(Uri uri) {
        hideLoading();
        if (checkLineInstalled()) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_SUBJECT, "QO");
            intent.putExtra(Intent.EXTRA_TEXT, "ใบเสนอราคา");
            intent.setType("file/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
//            intent.setClassName(PACKAGE_NAME, CLASS_NAME);
            intent.setPackage(PACKAGE_NAME);
            startActivity(intent);
        } else {
            Toast toast = Toast.makeText(this, "LINE application not install", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void sendEmailText(Uri uri) {
        sent_file.setVisibility(View.VISIBLE);
        save.setVisibility(View.VISIBLE);
//        String sendText = getString(R.string.share_fb_text,Utils.r_discount,Utils.rcode)+" "+linkShare;
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, customer.getItems().getPdfName());
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.setType("application/pdf");
//        emailIntent.setType("image/jpeg");
        startActivity(Intent.createChooser(emailIntent, "Sharing Options"));

    }

    private boolean checkLineInstalled() {
        PackageManager pm = getPackageManager();
        m_appList = pm.getInstalledApplications(0);
        boolean lineInstallFlag = false;
        for (ApplicationInfo ai : m_appList) {
            if (ai.packageName.equals(PACKAGE_NAME)) {
                lineInstallFlag = true;
                break;
            }
        }
        return lineInstallFlag;
    }

    public void sentLine() {
        MediaScannerConnection.scanFile(this,
                new String[]{pdfFlie.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
//                            openScreenshot(uri);
                        sendEmailText(uri);
                    }
                });
    }

    private void handlePermissionsn2() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                return;
            }
            takeScreenshot2();
        } else {

            takeScreenshot2();
        }
    }

    private void handlePermissionsn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                return;
            }
            takeScreenshot();
        } else {

            takeScreenshot();
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        loadPdfName(false, 2);
//
    }


    private void takeScreenshot2() {
        Date now = new Date();
        DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        showLoading(this);
        loadPdfName(true, 2);

    }


    public void showData(String data) {

    }

    public void showError(String error) {

    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    @Override
    public void onResume() {
        super.onResume();
//        downloadPDF(url,false);
//        getTransactionList();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
            getTransactionList();
        }else{
            getTransactionList();
        }
    }


    private Call<String> callTransactionList() {
        return rudyService.callTransactionList(
                Utils.APP_LANGUAGE,
                transaction_id,
                "detail"
        );
    }

    public void getTransactionList() {
        showLoading(this);
        callTransactionList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Transaction transaction = gson.fromJson(resp, Transaction.class);
                    if (transaction.getItems().getCredit_type().equalsIgnoreCase("3") && transaction.getItems().getNumber_of_installments() <= 0) {
                        submit.setBackgroundColor(getResources().getColor(R.color.gray));
                        submit.setClickable(false);
                        textConfirmPrint.setText(getResources().getString(R.string.alert_print_qo_saison));
                    }else{
                        submit.setBackground(getResources().getDrawable(R.drawable.background_shape_preset_button__pressed));
                        submit.setClickable(true);
                        textConfirmPrint.setText(getResources().getString(R.string.text_btn_send_qo_to_so));
                    }
                    loadPdfName(false, 1);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(PrintQoActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(PrintQoActivity.this, t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (PrintQoActivity.fromCart) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                    .title(getResources().getString(R.string.dialog_qo_success))
                    .content(getResources().getString(R.string.dialog_want_to_leave_this_page))
                    .positiveText(getResources().getString(R.string.agreed))
                    .negativeText(getResources().getString(R.string.cancel))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                            finish();
                            DealByPhaseActivity.print = true;
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    });
            MaterialDialog del_dialog = builder.build();
            del_dialog.show();
        } else {
            finish();
            DealByPhaseActivity.print = true;
        }


    }

    @SuppressLint("StaticFieldLeak")
    private void downloadPDF(){
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);




    }

    //do not change cus use url from api endpoint
    @SuppressLint("StaticFieldLeak")
    private void downloadPDF(final String url, boolean isSent, String showMessage) {
        Log.i("getPhase", " : " + url);
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                int count;
                try {

                    //for delete file if have file before create new file
                    pdfFlie = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/temp.pdf");
                    if (pdfFlie.exists()) {
                        pdfFlie.delete();
                    }

                    Log.d(TAG, "url = " + url);
                    URL _url = new URL(url);
                    URLConnection conection = _url.openConnection();
                    conection.connect();
                    InputStream input = new BufferedInputStream(_url.openStream(), 8192);
                    OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + fileName);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    Log.e("Error: ", e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                if (dialog != null) {
                    if (!PrintQoActivity.this.isFinishing()) {
                        dialog.dismiss();
                    }
                }

                onFileDownloaded(isSent, showMessage);
            }
        }.execute();
    }

    public void onFileDownloaded(boolean isSent, String showMessage) {
        if (customer != null) {
            final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/download/" + customer.getItems().getPdfName() + ".pdf");
            pdfFlie = file;
        } else {
            pdfFlie = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/download/temp.pdf");
        }
        pdfView.fromFile(pdfFlie)
                //.pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(true)
                .password(null)
                .scrollHandle(null)
                .onLoad(new OnLoadCompleteListener() {
                    @Override
                    public void loadComplete(int nbPages) {
                        pdfView.setMinZoom(1f);
                        pdfView.setMidZoom(2f);
                        pdfView.setMaxZoom(5f);
                        pdfView.zoomTo(1f);
//                            pdfView.scrollTo(100, 0);
//                            pdfView.moveTo(0f, 0f);
                    }
                })
                .load();


        if (pdfFlie.exists()) {
            if (isSent) {
                sentLine();
            } else {
                if (showMessage.length() > 0) {
                    showToast(this, showMessage);
                }

            }
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }


    @Override
    public void onPageChanged(int page, int pageCount) {

    }

    @Override
    public void onPageError(int page, Throwable t) {
        Log.e(TAG, "Cannot load page " + page);
    }


}


