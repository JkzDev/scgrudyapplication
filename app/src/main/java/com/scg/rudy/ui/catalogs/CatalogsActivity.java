package com.scg.rudy.ui.catalogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;

public class CatalogsActivity extends BaseActivity implements View.OnClickListener {

    private ImageView clsBtn;
    private WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogs);
        initView();
        setOnClick();
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setUseWideViewPort(true);

        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);

        String url = getString(R.string.cate_link_url);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        showLoading(this);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.i(TAG, "Processing webview url click...");
//                if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mms:") || url.startsWith("mmsto:")) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                    startActivity(intent);
//                }
//                else if(url.startsWith("mailto:")){
//                    Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
//                    startActivity(i);
//                }
//                else{
                    view.loadUrl(url);
//                }
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
//                view.evaluateJavascript(
//                        "(function() { return ('<html>'+document.getElementsByTagName('body')[0].innerHTML+'</body>'); })();",
//                        new ValueCallback<String>() {
//                            @Override
//                            public void onReceiveValue(String html) {
////                                Log.d("HTML", html);
////                                showToast(PrintQoActivity.this,html);
//                                if(html.contains("404 Page Not")){
//                                    sent_file.setVisibility(View.GONE);
//                                    save.setVisibility(View.GONE);
//                                }
//                                // code here
//                            }
//                        });
//                Log.i(TAG, "Finished loading URL: " +url);url
              hideLoading();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Log.e(TAG, "Error: " + description);
//                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                alertDialog.show();
            }
        });
        webView.loadUrl(url);


    }

    private void setOnClick(){
        clsBtn.setOnClickListener(this);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        clsBtn = (ImageView) findViewById(R.id.cls_btn);
        webView = (WebView) findViewById(R.id.webview);
    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }
    }
}
