/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.customer_data.filter;


import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.customer_data.model.ItemsItem;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutocompleteFilter;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutocompleteProvider;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteFilterListener;

/**
 * @author jackie CusModel
 */
public class ExtendedDictionaryAutocompleteFilter extends DynamicAutocompleteFilter<ItemsItem> {

    public ExtendedDictionaryAutocompleteFilter(
            DynamicAutocompleteProvider dynamicAutocompleteProvider,
            OnDynamicAutocompleteFilterListener onDynamicAutocompleteFilterListener) {
        super(dynamicAutocompleteProvider, onDynamicAutocompleteFilterListener);
    }

    @Override
    public CharSequence convertResultToString(Object resultValue) {
        if (resultValue instanceof CusModel) {
            CusModel countryModel = (CusModel) resultValue;

            return countryModel.getCustomer_name();
        }

        return super.convertResultToString(resultValue);
    }
}
