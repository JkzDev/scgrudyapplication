package com.scg.rudy.ui.by_customers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.byCustomer.Customer;
import com.scg.rudy.model.pojo.byCustomer.ItemsItem;
import com.scg.rudy.ui.customer_detail.CustomerDetailActivity;
import com.scg.rudy.ui.search_customer.SearchCustomerActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeoutException;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;


public class ByCustomersActivity extends BaseActivity implements View.OnClickListener,ByCustomerListener {
    private RudyService rudyService;
    private ImageView clsBtn,searchBtn;
    private String user_id = "",shopID="";
    private RecyclerView recyclerView;
    private ByCustomerRecyclerAdapter adapter;
    private Customer customer = null;
    public static final String TAG = "ByCustomersActivity";
    private LinearLayoutManager linearLayoutManager;
    private static final int PAGE_START = 1;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private String search = "";
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_by_customers);
        ButterKnife.bind(this);
        initView();
        getUserData();
        setAdapter();
        setOnClick();


    }

    private void setOnClick(){
        clsBtn.setOnClickListener(this);
        searchBtn.setOnClickListener(this);
    }


    private void setAdapter(){
        adapter = new ByCustomerRecyclerAdapter(this,this);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new byCustomerPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }
            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private Call<String> getCustomerList() {
        rudyService = ApiHelper.getClient();
        return rudyService.getCustomerList(
                Utils.APP_LANGUAGE,
                user_id,
                "list_v2",
                String.valueOf(currentPage),
                ""
        );
    }

    private void loadCustomerList() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        getCustomerList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Customer customer  = fetchCustomerdata(gson.fromJson(resp, Customer.class));
                    if(customer.getItems().size()>0){
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(customer.getItems(), "");
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        }
                        else {
                            isLastPage = true;
                        }
                    }else{
                        errorLayout.setVisibility(View.VISIBLE);
                        TOTAL_PAGES = 1;
                        currentPage = PAGE_START;
                        isLoading = false;
                        isLastPage = false;
                        setAdapter();
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ByCustomersActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage","TotalPage is : "+ TOTAL_PAGES);
        getCustomerList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Customer results  = fetchCustomerdata(gson.fromJson(resp, Customer.class));
                    adapter.addAll(results.getItems(), "");
                    if (currentPage < TOTAL_PAGES){
                        adapter.addLoadingFooter();
                    }
                    else {
                        isLastPage = true;
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ByCustomersActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Customer fetchCustomerdata(Customer response) {
        Customer customer = response;
        if(customer.getTotalPages()!=0){
            TOTAL_PAGES = customer.getTotalPages();
        }

        return customer;
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
//            totalProject.setText("0");
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_pls_try_again));
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        TOTAL_PAGES = 1;
        currentPage = PAGE_START;
        isLoading = false;
        isLastPage = false;
        setAdapter();
        loadCustomerList();

    }

    public void initView(){
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        searchBtn = findViewById(R.id.search_btn);
        recyclerView = findViewById(R.id.recyclerView);

        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt);


    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
           finish();
        }
        if (v == searchBtn) {
            if(adapter.getItemCount()!=0){
                Intent intent = new Intent(this, SearchCustomerActivity.class);
//            intent.putExtra("customer",customer);
            startActivity(intent);
            }else{
                showToast(this,getResources().getString(R.string.toast_not_found_customer_data_pls_try_again));
            }
        }
    }


    public boolean isNetworkConnected() {
        return true;
    }

    public void showData(Customer customer) {
        this.customer = customer;
//        adapter = new ByCustomerRecyclerAdapter(this,customer.getItems());
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);
    }


    @Override
    public void checkInTel(ItemsItem itemList) {

    }

    @Override
    public void OnclickItems(ItemsItem projectModel) {
        Intent intent = new Intent(this, CustomerDetailActivity.class);
        intent.putExtra("id",projectModel.getUserId());
        intent.putExtra("customer_id",projectModel.getCustomerId());
        startActivity(intent);
    }
}
