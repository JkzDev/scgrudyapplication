
package com.scg.rudy.ui.main.Models;

import android.graphics.drawable.Drawable;
import android.media.Image;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LanguageModel {

    @Expose
    private Drawable langImage;
    @Expose
    private String langText;
    @Expose
    private String langValue;

    public LanguageModel (Drawable _langImage, String _langText, String _langValue){
        langImage = _langImage;
        langText = _langText;
        langValue = _langValue;
    }

    public Drawable getLangImage() {
        return langImage;
    }

    public void setLangImage(Drawable langImage) {
        this.langImage = langImage;
    }

    public String getLangText() {
        return langText;
    }

    public void setLangText(String langText) {
        this.langText = langText;
    }

    public String getLangValue() {
        return langValue;
    }

    public void setLangValue(String langValue) {
        this.langValue = langValue;
    }

}
