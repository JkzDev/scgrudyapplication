package com.scg.rudy.ui.department_data_group;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.model.pojo.project_group_detail.ProjectListItem;
import com.scg.rudy.model.pojo.projectgroup.ProjectGroupPojo;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.department_data_child.DepartmentDataNewActivity;
import com.scg.rudy.ui.department_data_group.Adapter.DeveloperDictionaryAutocompleteAdapter;
import com.scg.rudy.ui.department_data_group.model.Developer;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutoCompleteTextView;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class DepartmentDataGroupActivity extends BaseActivity implements View.OnClickListener, OnDynamicAutocompleteListener , DepartmentGroupInterface {
    private String TAG = "DepartmentDataGroupActivity";

    private String project_group_list_id = "1";
    private String projectGroupName = "";
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private CustomSpinner customSpinner;
    private int project_group_position = 0;
    private int maxPhase = 10;
    private Spinner typeConstruc;
    private RelativeLayout save;
    private EditText name;
    private LinearLayout key_hide;
    private ImageView clsBtn;
    private Bundle extras;
    private ImageView clearText;
    private LinearLayout save_layout;
    public static boolean isEdit = false;
    public static String projectId = "";
    private RelativeLayout typeLayout;
    private int first = 0;
    private Button add_department;
    private EditText datetimeStart, datetimeEnd;
    private DynamicAutoCompleteTextView devname;
    private ProgressBar mDictionaryAutoCompleteProgressBar;
    private static final int THRESHOLD = 1;
    private DeveloperDictionaryAutocompleteAdapter nameAdapter;
    private RecyclerView mRecyclerView;
    private RudyService rudyService;
    private String user_id = "", shopID="";
    private DepartmentGroupAdapter adapter;
    private LinearLayout linear_header;

    private String developer_id = "";
    private String developer_name_other = "";
    private String start_date = "";
    private String end_date = "";
    private String customer_type = "";
    private String customer_name ="" ;//: Customer’s name
    private String customer_phone ="" ;//: Customer’s phone
    private String customer_email ="" ;//: Customer’s email
    private String customer_line ="" ;//: Customer’s line
    private String customer_tax_no ="" ;//: Customer’s tax no or ID card no.
    private String customer_note ="" ;//: Customer’s note
    private String customer_code ="" ; //: customer_code
    private String champ_customer_code =""; //: champ_customer_code
    private String customer_company =""; //: champ_customer_code
    private String lat_lng =""; //: champ_customer_code
    private String project_address =""; //: champ_customer_code

    public static String group_id = "";
    private TextView totalUnitAll;
    private TextView totalPriceAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_data_group);
        initView();
        getUserData();
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());
        extras = getIntent().getExtras();

        if (extras != null) {
            group_id =  extras.getString("group_id","");
            projectGroupName = extras.getString("project_name","");
            project_group_list_id = extras.getString("cateId","1");
            developer_id = extras.getString("developer_id","1");
            developer_name_other = extras.getString("developer_name_other","1");
            start_date = extras.getString("start_date","1");
            end_date = extras.getString("end_date","1");
            project_group_position = (Integer.parseInt(extras.getString("project_group_position","0")));

           lat_lng =  extras.getString("lat_lng","");
           project_address =  extras.getString("project_address","");
           customer_type =  extras.getString("customer_type","");
           customer_name =  extras.getString("customer_name","");
           customer_code =  extras.getString("customer_code","");
           champ_customer_code =  extras.getString("champ_customer_code","");
           customer_company =  extras.getString("customer_company","");
           customer_phone =  extras.getString("customer_phone","");
           customer_email =  extras.getString("customer_email","");
           customer_line =  extras.getString("customer_line","");
           customer_tax_no =  extras.getString("customer_tax_no","");
           customer_note =  extras.getString("customer_note","");;
        }


       if (projectGroupName.length() > 0) {
            name.setText(projectGroupName);
        } else {
            name.setText(date);
        }

        Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
        name.setTypeface(_font);

        clearText.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        add_department.setOnClickListener(this);


        if(developer_name_other.length()>0){
            devname.setText(developer_name_other);
        }

        key_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(DepartmentDataGroupActivity.this, key_hide);
            }
        });

        save_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(DepartmentDataGroupActivity.this, save_layout);
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    clearText.setVisibility(View.VISIBLE);
                    save.setBackground(getDrawable(R.drawable.linear_blue_chkin_detail));
                } else {
                    clearText.setVisibility(View.GONE);
                    save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
                }
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                projectGroupName = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                name.setTypeface(_font);
            }
        });

        save.setOnClickListener(this);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String formattedDate = df.format(c);


        if(start_date.length()>0){
            datetimeStart.setText(start_date);
        }else{
            datetimeStart.setText(formattedDate);
        }
        if(end_date.length()>0){
            datetimeEnd.setText(end_date);
        }else{
            datetimeEnd.setText(formattedDate);
        }


        datetimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.SelectDateDialogs(DepartmentDataGroupActivity.this, datetimeStart);
            }
        });

        datetimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.SelectDateDialogs(DepartmentDataGroupActivity.this, datetimeEnd);
            }
        });

        mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        nameAdapter = new DeveloperDictionaryAutocompleteAdapter(DepartmentDataGroupActivity.this, R.layout.extended_list_item_name, this, 1);
        devname.setOnDynamicAutocompleteListener(this);
        devname.setAdapter(nameAdapter);
        devname.setThreshold(THRESHOLD);
        Utils.hideKeyboard(this);
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == clearText) {
            name.setText("");
            clearText.setVisibility(View.GONE);
            save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }
        if(v==save){
            saveDataGroup();
        }
        if (v==add_department){
            start_date = datetimeStart.getText().toString();
            end_date = datetimeEnd.getText().toString();
            developer_name_other = devname.getText().toString();
            Intent intent = new Intent(DepartmentDataGroupActivity.this, DepartmentDataNewActivity.class);
            intent.putExtra("addNew", "add");
            intent.putExtra("projectGroupName", projectGroupName);
            intent.putExtra("devName", developer_name_other);
            intent.putExtra("project_group_list_id", project_group_list_id);
            intent.putExtra("developer_id", developer_id);
            intent.putExtra("timeStart",  start_date);
            intent.putExtra("timeEnd",   end_date);
            intent.putExtra("cateId",   "1");
            intent.putExtra("project_group_position",   String.valueOf(project_group_position));
            intent.putExtra("lat_lng", lat_lng);
            intent.putExtra("project_address", project_address);
            intent.putExtra("customer_type", customer_type);
            intent.putExtra("customer_name", customer_name);
            intent.putExtra("customer_code", customer_code);
            intent.putExtra("champ_customer_code", champ_customer_code);
            intent.putExtra("customer_company", customer_company);
            intent.putExtra("customer_phone", customer_phone);
            intent.putExtra("customer_email", customer_email);
            intent.putExtra("customer_line", customer_line);
            intent.putExtra("customer_tax_no", customer_tax_no);
            intent.putExtra("customer_note", customer_note);
            startActivity(intent);

        }
    }

    private void saveDataGroup(){
        start_date = datetimeStart.getText().toString();
        end_date = datetimeEnd.getText().toString();
        projectGroupName = name.getText().toString();

        if (projectGroupName.length() > 0) {
            developer_name_other = devname.getText().toString();
            if(AddNewGroupProjectActivity.group_id.length()>0){
                AddNewGroupProjectActivity.projectGroupName = projectGroupName;
                AddNewGroupProjectActivity.project_group_list_id = project_group_list_id;
                AddNewGroupProjectActivity.project_group_position = project_group_position;
                AddNewGroupProjectActivity.maxPhase = maxPhase;
                AddNewGroupProjectActivity.start_date = start_date;
                AddNewGroupProjectActivity.end_date = end_date;
                AddNewGroupProjectActivity.developer_name_other = developer_name_other;
                AddNewGroupProjectActivity.developer_id = developer_id;
                MainActivity.gdata3 = true;
                postEditProjectGroup();
            }else{
                AddNewGroupProjectActivity.projectGroupName = projectGroupName;
                AddNewGroupProjectActivity.project_group_list_id = project_group_list_id;
                AddNewGroupProjectActivity.project_group_position = project_group_position;
                AddNewGroupProjectActivity.maxPhase = maxPhase;
                AddNewGroupProjectActivity.start_date = start_date;
                AddNewGroupProjectActivity.end_date = end_date;
                AddNewGroupProjectActivity.developer_name_other = developer_name_other;
                AddNewGroupProjectActivity.developer_id = developer_id;
                MainActivity.gdata3 = true;
                AddNewGroupProjectActivity.step3 = true;
                finish();
            }
        } else {
            Toast.makeText(DepartmentDataGroupActivity.this, getResources().getString(R.string.toast_pls_fill_name_unit), Toast.LENGTH_LONG).show();
        }
    }


    private Call<String> getProjectGroup() {
        return rudyService.getProjectGroup(
                Utils.APP_LANGUAGE
        );
    }


    private Call<String> callPostEditProjectGroup() {
        start_date = datetimeStart.getText().toString();
        end_date = datetimeEnd.getText().toString();
        if(AddNewGroupProjectActivity.group_id.length()!=0){
            group_id = AddNewGroupProjectActivity.group_id;
        }


        return rudyService.postEditProjectGroup(
                Utils.APP_LANGUAGE,
                group_id,
                user_id,
                projectGroupName,
                project_group_list_id,
                developer_id,
                developer_name_other,
                start_date,
                end_date,
                customer_type,
                customer_name,
                customer_code,
                champ_customer_code,
                customer_company,
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note
        );
    }

    private void postEditProjectGroup() {
        showLoading(this);
        callPostEditProjectGroup().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    developer_name_other = devname.getText().toString();
                    ProjectGroupDetail projectGroupDetail  = gson.fromJson(resp, ProjectGroupDetail.class);
                    AddNewGroupProjectActivity.projectGroupName = projectGroupName;
                    AddNewGroupProjectActivity.project_group_list_id = project_group_list_id;
                    AddNewGroupProjectActivity.project_group_position = project_group_position;
                    AddNewGroupProjectActivity.maxPhase = maxPhase;
                    AddNewGroupProjectActivity.start_date = start_date;
                    AddNewGroupProjectActivity.end_date = end_date;
                    AddNewGroupProjectActivity.developer_name_other = developer_name_other;
                    AddNewGroupProjectActivity.developer_id = developer_id;
                    AddNewGroupProjectActivity.group_id = projectGroupDetail.getItems().getId();
                    MainActivity.gdata3 = true;
                    AddNewGroupProjectActivity.step3 = true;
                    finish();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataGroupActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DepartmentDataGroupActivity.this, t.getMessage());
            }
        });
    }

    private void callProjectGroupApi() {
        showLoading(this);
        getProjectGroup().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ProjectGroupPojo projectGroupPojo  = gson.fromJson(resp, ProjectGroupPojo.class);
                    showData(projectGroupPojo);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DepartmentDataGroupActivity.this, t.getMessage());
            }
        });
    }


    private ProjectGroupPojo fetchProjectGroupResults(Response<ProjectGroupPojo> response) {
        ProjectGroupPojo projectGroupPojo = response.body();
        return projectGroupPojo;
    }


    private void initView() {
        rudyService = ApiHelper.getClient();
        clearText = findViewById(R.id.clear_text);
        typeConstruc = findViewById(R.id.type_construc);
        save = findViewById(R.id.save);
        name = findViewById(R.id.project_name);
        key_hide = findViewById(R.id.key_hide);
        clsBtn = findViewById(R.id.cls_btn);
        save_layout = findViewById(R.id.save_layout);
        typeLayout = findViewById(R.id.type_layout);
        add_department = findViewById(R.id.add_department);
        datetimeStart = findViewById(R.id.datetimeStart);
        datetimeEnd = findViewById(R.id.datetimeEnd);
        devname = findViewById(R.id.devname);
        mDictionaryAutoCompleteProgressBar = findViewById(R.id.dictionaryAutoCompleteProgressBar);
        linear_header = findViewById(R.id.linear_header);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        totalUnitAll = (TextView) findViewById(R.id.total_unit_all);
        totalPriceAll = (TextView) findViewById(R.id.total_price_all);
    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void showData(final ProjectGroupPojo projectGroup) {
        categorieModelArrayList = new ArrayList<>();
        nameList = new ArrayList<>();
        for (int i = 0; i < projectGroup.items.size(); i++) {
            if (Integer.parseInt(project_group_list_id) == Integer.parseInt(projectGroup.items.get(i).id)) {
                project_group_position = i;
            }
            categorieModelArrayList.add(new CategorieModel(projectGroup.items.get(i).id, projectGroup.items.get(i).name));
            nameList.add(projectGroup.items.get(i).name);
        }
        customSpinner = new CustomSpinner(this, nameList);

        typeConstruc.setAdapter(customSpinner);
        typeConstruc.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        typeConstruc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                project_group_position = position;
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._11sdp));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                project_group_list_id = categorieModelArrayList.get(project_group_position).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeConstruc.setSelection(project_group_position);

    }



    @Override
    public void onDynamicAutocompleteStart(AutoCompleteTextView v) {
        if (v == devname) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDynamicAutocompleteStop(AutoCompleteTextView v) {
        if (v == devname) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            devname.setTypeface(_font);
        }
    }

    @Override
    public void selectCusItem(Developer developer) {
        devname.setText(developer.getName());
        developer_id = developer.getId();
        devname.dismissDropDown();
        Utils.hideKeyboard(this);
        devname.setFocusable(false);
        devname.setFocusableInTouchMode(true);
        developer_name_other = devname.getText().toString();

    }

    @Override
    protected void onResume() {
        super.onResume();
        callProjectGroupApi();
        if(AddNewGroupProjectActivity.group_id.isEmpty()){
            linear_header.setVisibility(View.GONE);
        }else {
            linear_header.setVisibility(View.VISIBLE);
            GetViewProjectGroup();
        }

    }

    private void GetViewProjectGroup() {
        showLoading(this);
        RudyService rudyService = ApiHelper.getClient();
        Call<String> call = rudyService.getPGdetail(Utils.APP_LANGUAGE, AddNewGroupProjectActivity.group_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ProjectGroupDetail items  = gson.fromJson(resp, ProjectGroupDetail.class);
                    List<ProjectListItem> arrayList = items.getItems().getProjectList();
                    adapter = new DepartmentGroupAdapter(DepartmentDataGroupActivity.this, arrayList);
                    mRecyclerView.setAdapter(adapter);
                    name.setText(items.getItems().getName());
                    datetimeStart.setText(items.getItems().getStartDate());
                    datetimeEnd.setText(items.getItems().getEndDate());
                    devname.setText(items.getItems().getDeveloperName());
                    totalUnitAll.setText(String.valueOf(arrayList.size()));
                    totalPriceAll.setText(items.getItems().getSumOpp());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataGroupActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                Toast.makeText(DepartmentDataGroupActivity.this, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
