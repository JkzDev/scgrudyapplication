package com.scg.rudy.ui.dealbyproject;

import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ProjectClass extends ExpandableGroup<SkuItem> {

  private String id;
  private String name;
  private String fav;
  private String ispromotion;
  private String sum_rank;

  public ProjectClass( String id,
                       String name,
                       String fav,
                       String ispromotion,
                       String sum_rank,
                       List<SkuItem> items) {
    super(id,name,fav,ispromotion,sum_rank, items);
    this.id = id;
    this.name = name;
    this.fav = fav;
    this.ispromotion = ispromotion;
    this.sum_rank = sum_rank;


  }

  public int getIconResId() {
    return Integer.parseInt(id);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ProjectClass)) return false;

    ProjectClass projectClass = (ProjectClass) o;

    return getIconResId() == projectClass.getIconResId();

  }

  @Override
  public int hashCode() {
    return getIconResId();
  }
}

