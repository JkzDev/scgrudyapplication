package com.scg.rudy.ui.calendar_detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.calendar.DetailItem;

import java.util.ArrayList;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class CalendarRecyclerAdapter extends RecyclerView.Adapter<CalendarRecyclerAdapter.ProjectViewHolder> {

    private  ArrayList<DetailItem> detailItemList ;
    private Context context;
    public CalendarRecyclerAdapter(Context context,  ArrayList<DetailItem> detailItemList) {
        this.detailItemList = detailItemList;
        this.context = context;

    }

    @Override
    public CalendarRecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.items_calendar, parent, false);
        return new CalendarRecyclerAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CalendarRecyclerAdapter.ProjectViewHolder holder, int position) {
        final DetailItem detailItem = detailItemList.get(position);

        holder.title.setText(detailItem.getTitle());
        holder.detail.setText(detailItem.getDescription());
        holder.type_txt.setText(detailItem.getTypeTxt());
        holder.time.setText(detailItem.getStartTime()+" - "+detailItem.getEndTime());

        if(detailItem.getType().equalsIgnoreCase("1")){
            holder.imageStat.setImageResource(R.drawable.cal_visit);
            holder.status.setBackgroundColor(Color.parseColor("#7cb342"));
        }else if(detailItem.getType().equalsIgnoreCase("2")){
            holder.imageStat.setImageResource(R.drawable.cal_sale);
            holder.status.setBackgroundColor(Color.parseColor("#7b5ce2"));
        }else{
            holder.imageStat.setImageResource(R.drawable.cal_other);
            holder.status.setBackgroundColor(Color.parseColor("#109ffc"));
        }


        if(detailItem.getDescription().length()==0){
            holder.detail.setText("-");
        }

        holder.calendar_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,CalendarListDetailActivity.class);
                intent.putExtra("calendarID",detailItem.getId());
                intent.putExtra("type",detailItem.getType());

//                intent.putExtra("daySelect",tagId);
//                intent.putExtra("realDate",dayOfTheMonthText.getText().toString());

//                intent.putExtra("month_select",Integer.parseInt(monthTxt)-1);
                context.startActivity(intent);
            }
        });

//        holder.status.setText(detailItem.getTitle());
//        holder.imageStat.setText(detailItem.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        if (detailItemList == null){
            return 0;
        }
//
        return detailItemList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView detail;
        TextView type_txt ;
        TextView time;
        RelativeLayout status;
        ImageView imageStat;
        RelativeLayout calendar_detail;

        public ProjectViewHolder(View view) {
            super(view);
            title = itemView.findViewById(R.id.title);
            detail = itemView.findViewById(R.id.detail);
            type_txt = itemView.findViewById(R.id.type_txt);
            time = itemView.findViewById(R.id.time);
            status = itemView.findViewById(R.id.status);
            imageStat = itemView.findViewById(R.id.imageStat);
            calendar_detail = itemView.findViewById(R.id.calendar_detail);
        }
    }


}