package com.scg.rudy.ui.recomend;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.choose_sku.ChooseSku;
import com.scg.rudy.model.pojo.recommend.Recommend;
import com.scg.rudy.model.pojo.recommend.ItemsItem;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.model.pojo.recommend.Recommend;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.choose_sku.PaginationScrollListener;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.promotion.PromotionActivity;
import com.scg.rudy.ui.promotion.PromotionPaginationAdapter;
import com.scg.rudy.ui.promotion.promotionInterface;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.onShowPromotion;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class RecomendProductActivity extends BaseActivity implements View.OnClickListener, recomendInterface {
    private RudyService rudyService;
    private LayoutInflater layoutInflater;
    private Bundle extras;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private String subClass;
    private String  projectId;
    private ImageView clsBtn;
    private RecomendPaginationAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView rv;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private String class_id = "";
    public String phase_id;
    private String shortBy = ApiEndPoint.SEARCH_LTH;
    private final int shortByUse = 1,shortBySale = 2,shortByRecomd = 3;
    private int shortType = 2;
    private boolean isUse =false,isBest =false,isRecm =false;
    private ArrayList<String> filterList;
    private String multifilter = "";
    private RelativeLayout btnPromoTab;
    private RelativeLayout btnRecomTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomend_product);
        rudyService = ApiHelper.getClient();
        extras = getIntent().getExtras();
        if (extras != null) {
            class_id = extras.getString("class_id","");
            projectId = extras.getString("projectId","");
            subClass  = extras.getString("subClass","");
            phase_id  = extras.getString("phase_id","");
        }else{
            class_id = "";
            projectId = "";
            subClass  = "";
            phase_id = "";
        }
        initView();
        setEnviroment();
        setAdapter();
        loadFirstPage();
        setOnClick();

    }

    private  void setOnClick(){
        clsBtn.setOnClickListener(this);
        btnPromoTab.setOnClickListener(this);
        btnRecomTab.setOnClickListener(this);
        findViewById(R.id.bhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(RecomendProductActivity.this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });

    }

    @Override
    public void setEnviroment() {
        if(Utils.getPrefer(this, Utils.PERF_LOGIN).length()>0){
            UserPOJO userPOJO = Utils.setEnviroment(this);
            user_id = userPOJO.getItems().getId();
            shopID = userPOJO.getItems().getShopId();
        }
    }

    private void setAdapter(){
        adapter = new RecomendPaginationAdapter(this,subClass,phase_id,this);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }
            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private Call<String> callPromotionApi() {
        return rudyService.callRecommendList(
                Utils.APP_LANGUAGE,
                shopID,
                "recommend_biz",
                user_id,
                projectId,
                class_id,
                String.valueOf(currentPage),
                shortBy,
                phase_id,
                multifilter
        );
    }

    private void loadFirstPage() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callPromotionApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Recommend obj  = gson.fromJson(resp, Recommend.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    if(results.size()>0){
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(results);
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        }
                        else {
                            isLastPage = true;
                        }
                    }else{
                        errorLayout.setVisibility(View.VISIBLE);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(RecomendProductActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage","TotalPage is : "+ TOTAL_PAGES);
        callPromotionApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Recommend obj  = gson.fromJson(resp, Recommend.class);
                    List<ItemsItem> results =   customFetchResults(obj);
                    adapter.addAll(results);
                    if (currentPage < TOTAL_PAGES){
                        adapter.addLoadingFooter();
                    }
                    else {
                        isLastPage = true;
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(RecomendProductActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private List<ItemsItem> fetchResults(Response<Recommend> response) {
        TOTAL_PAGES = 1;
        Recommend skuItem = response.body();
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem.getItems();
    }

    private List<ItemsItem> customFetchResults(Recommend skuItem) {
        TOTAL_PAGES = 1;
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem.getItems();
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(fetchErrorMessage(throwable));
        }
    }
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private void initView() {
        layoutInflater = LayoutInflater.from(getBaseContext());
        filterList = new ArrayList<>();
        clsBtn = findViewById(R.id.cls_btn);
        rv = findViewById(R.id.recom_recycler);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        btnPromoTab = findViewById(R.id.btn_promo_tab);
        btnRecomTab = findViewById(R.id.btn_recom_tab);
    }

    public boolean isNetworkConnected() {
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }

        if(v==btnPromoTab){

            Intent intent = new Intent(this, PromotionActivity.class);
            intent.putExtra("class_id",class_id);
            intent.putExtra("projectId",projectId);
            intent.putExtra("subClass",subClass);
            intent.putExtra("phase_id",phase_id);
            startActivity(intent);

            overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
            finish();
        }

        if(v==btnRecomTab){
           // startActivity(new Intent(this, RecomendProductActivity.class));
        }

    }

    private Call<String> callAddFav(String _productId) {
        return rudyService.addFAV(
                Utils.APP_LANGUAGE,
                projectId,
                _productId,
                user_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void apiAddFav(String _productId){
        showLoading(RecomendProductActivity.this);
        callAddFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(RecomendProductActivity.this,getResources().getString(R.string.toast_add_producto_to_favorite_success));
//                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(RecomendProductActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(RecomendProductActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callDeleteFav(String _productId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                projectId,
                _productId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void apiDeleteFav(String _productId){
        showLoading(RecomendProductActivity.this);
        callDeleteFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(RecomendProductActivity.this,getResources().getString(R.string.dialog_delete_finish));
//                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(RecomendProductActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(RecomendProductActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    public void getData(final String url) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
////                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
//                }
//            }
//        }.execute();
//    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }

    @Override
    public void addFAV(String skuId) {
        apiAddFav(skuId);
    }

    @Override
    public void removeFAV(String skuId) {
        apiDeleteFav(skuId);
    }

    @Override
    public void onClickinfo(ItemsItem items) {
//        showToast(this,items.getName());
        PromotionDetail(items.getPromotionDetailId());
    }


    private void PromotionDetail(String promotionDetailId) {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callPromotionDetailApi(promotionDetailId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    PromotionDetail promotionDetail  = gson.fromJson(resp, PromotionDetail.class);
                    onShowPromotion(RecomendProductActivity.this,promotionDetail);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(RecomendProductActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(RecomendProductActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callPromotionDetailApi(String promotionDetailId) {
        return rudyService.getPromotionDetail(
                Utils.APP_LANGUAGE,
                shopID,
                promotionDetailId
        );
    }
}