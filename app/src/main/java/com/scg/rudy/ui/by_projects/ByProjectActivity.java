package com.scg.rudy.ui.by_projects;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.byproject.ItemsItem;
import com.scg.rudy.model.pojo.byproject.Myprojects;
import com.scg.rudy.model.pojo.projectGroupList.ProjectGroupList;
import com.scg.rudy.model.pojo.project_type.PhasesItem;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.ui.by_projects.Adapter.CheckInTelAdapter;
import com.scg.rudy.ui.by_projects.Models.Item;
import com.scg.rudy.ui.by_projects.Models.StaffModel;
import com.scg.rudy.ui.search_project.SearchProjectActivity;
import com.scg.rudy.utils.Utils;

import org.checkerframework.checker.units.qual.A;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class ByProjectActivity extends BaseActivity implements View.OnClickListener, ByProjectsListener {
    private RudyService rudyService;
    private ImageView clsBtn;
    private String user_id = "";
    private RecyclerView recyclerView, recyclerViewProject;
    private ByProjectRecyclerAdapter adapter;
    private ProjectListRecyclerAdapter adapterProject;
    private ImageView searchBtn;
    private Myprojects myprojects = null;
    private List<ItemsItem> searchArrayList;
    private List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> projectArrayList;
    private LinearLayoutManager linearLayoutManager;
    private static final int PAGE_START = 1, PAGE_START_Pro = 1;
    private int TOTAL_PAGES = 1, TOTAL_PAGES_Pro = 1;
    private int currentPage = PAGE_START;
    private int currentPagePro = PAGE_START_Pro;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private boolean isLoadingPro = false;
    private boolean isLastPagePro = false;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private String class_id = "";
    public static final String TAG = "ByProjectActivity";
    private String search = "";
    private String project_type_id = "";
    private String phase_id = "";
    private RelativeLayout statusProject;
    private RelativeLayout chance;
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private Spinner typeConstruc;
    private Spinner currentPhase;
    private ProjectTypeSpinner customSpinner;
    private int catePosition = 0;
    private int phasePosition = 0;
    private int staffPosition = 0;
    private int dateFromPosition = 0;
    private int sortPosition = 0;
    private String cateId = "";
    private String phaseId = "";
    private String staffId = "";
    private String dateFromId = "";
    private String sortId = "";
    private int maxPhase = 10;
    private TextView totalProject;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1234;
    private LayoutInflater inflater;
    private RelativeLayout btnOther, btnProject;
    private TextView textOther, textProject;
    private LinearLayout linearLayoutOther, linearLayoutProject;
    private RelativeLayout reStaff;
    private Spinner spinnerStaff;
    private RelativeLayout reDateFrom;
    private Spinner spinnerDateFrom;
    private int firstLoadCount = 0;
    private RelativeLayout reSort;
    private Spinner spinnerSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_by_projects);

        rudyService = ApiHelper.getClient();


        ButterKnife.bind(this);
        initView();
        getUserData();
        clsBtn.setOnClickListener(this);
        searchBtn.setOnClickListener(this);
        statusProject.setOnClickListener(this);
        chance.setOnClickListener(this);
        project_type_id = "";
        phase_id = "";
        setAdapter();
        setAdapterProject();
        TOTAL_PAGES = 1;
        TOTAL_PAGES_Pro = 1;
        currentPage = PAGE_START;
        currentPagePro = PAGE_START_Pro;
        isLoading = false;
        isLastPage = false;
        isLoadingPro = false;
        isLastPagePro = false;
        loadProjectType();
        loadStaffAndDateFrom();
        linearLayoutOther.setVisibility(View.VISIBLE);
        linearLayoutProject.setVisibility(View.GONE);
    }


    private void setAdapter() {
        adapter = new ByProjectRecyclerAdapter(this, this);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new byProjectPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    private void setAdapterProject() {
        adapterProject = new ProjectListRecyclerAdapter(this, this);
        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerViewProject.setLayoutManager(linearLayoutManager);
        recyclerViewProject.setItemAnimator(new DefaultItemAnimator());

        recyclerViewProject.setAdapter(adapterProject);
        recyclerViewProject.addOnScrollListener(new byProjectPaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoadingPro = true;
                currentPagePro += 1;
                loadNextPagePro();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES_Pro;
            }

            @Override
            public boolean isLastPage() {
                return isLastPagePro;
            }

            @Override
            public boolean isLoading() {
                return isLoadingPro;
            }
        });
    }

    private Call<String> getProjectList() {
        if (project_type_id.equalsIgnoreCase("")) {
            phase_id = "";
        }
        return rudyService.getMyproject(
                Utils.APP_LANGUAGE,
                user_id,
                "list_v2",
                String.valueOf(currentPage),
                search,
                project_type_id,
                phase_id,
                staffId,
                dateFromId,
                sortId
        );
    }

    private Call<String> getProjectGroupList() {
        if (project_type_id.equalsIgnoreCase("")) {
            phase_id = "";
        }
        rudyService = ApiHelper.getClient();
        return rudyService.getGroupProject(
                Utils.APP_LANGUAGE,
                user_id,
                "pglist",
                String.valueOf(currentPagePro),
                search,
                project_type_id,
                phase_id
        );
    }

    private Call<String> getStaff() {
        return rudyService.getStaff(
                Utils.APP_LANGUAGE,
                user_id
        );
    }


    private Call<String> getDateFrom() {
        return rudyService.getDateFrom(
                Utils.APP_LANGUAGE
        );
    }


    private void loadStaffAndDateFrom() {
        showLoading(this);
        getStaff().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    StaffModel staffModel = gson.fromJson(resp, StaffModel.class);
                    staffModel.getItems().add(0, new Item("", getResources().getString(R.string.all)));
                    ArrayList<String> staffList = new ArrayList<>();
                    for (int i = 0; i < staffModel.getItems().size(); i++) {
                        staffList.add(staffModel.getItems().get(i).getDisplayName());
                    }
                    ProjectTypeSpinner ct = new ProjectTypeSpinner(ByProjectActivity.this, staffList);
                    spinnerStaff.setAdapter(ct);
                    spinnerStaff.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    spinnerStaff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            staffPosition = position;
                            ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                            staffId = staffModel.getItems().get(position).getId();
                            TOTAL_PAGES = 1;
                            currentPage = PAGE_START;
                            isLoading = false;
                            isLastPage = false;
                            setAdapter();
                            if (firstLoadCount > 0) {
                                loadFirstPage();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    spinnerStaff.setSelection(staffPosition);


                    getDateFrom().enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            hideLoading();
                            hideErrorView();
                            Gson gson = new Gson();
                            String resp = response.body();
                            if (resp.contains("200")) {


                                StaffModel dateFromModel = gson.fromJson(resp, StaffModel.class);
                                dateFromModel.getItems().add(0, new Item("", getResources().getString(R.string.all)));
                                ArrayList<String> dateFromList = new ArrayList<>();
                                for (int i = 0; i < dateFromModel.getItems().size(); i++) {
                                    dateFromList.add(dateFromModel.getItems().get(i).getDisplayName());
                                }
                                ProjectTypeSpinner ct = new ProjectTypeSpinner(ByProjectActivity.this, dateFromList);
                                spinnerDateFrom.setAdapter(ct);
                                spinnerDateFrom.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                spinnerDateFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        dateFromPosition = position;
                                        ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                                        dateFromId = dateFromModel.getItems().get(position).getId();
                                        TOTAL_PAGES = 1;
                                        currentPage = PAGE_START;
                                        isLoading = false;
                                        isLastPage = false;
                                        setAdapter();
                                        if (firstLoadCount > 0) {
                                            loadFirstPage();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                spinnerDateFrom.setSelection(dateFromPosition);


                                ArrayList<Item> sortModel = new ArrayList<>();
                                sortModel.add(new Item("1", getResources().getString(R.string.text_updately)));
                                sortModel.add(new Item("2", getResources().getString(R.string.text_time_visit)));
                                sortModel.add(new Item("3", getResources().getString(R.string.text_creately)));

                                ArrayList<String> sortList = new ArrayList<>();
                                for (int i = 0; i < sortModel.size(); i++) {
                                    sortList.add(sortModel.get(i).getDisplayName());
                                }

                                ProjectTypeSpinner sl = new ProjectTypeSpinner(ByProjectActivity.this, sortList);
                                spinnerSort.setAdapter(sl);
                                spinnerSort.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        sortPosition = position;
                                        ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                                        sortId = sortModel.get(position).getId();
                                        TOTAL_PAGES = 1;
                                        currentPage = PAGE_START;
                                        isLoading = false;
                                        isLastPage = false;
                                        setAdapter();
                                        loadFirstPage();
                                        firstLoadCount++;
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                spinnerSort.setSelection(sortPosition);


                            } else {
                                APIError error = gson.fromJson(resp, APIError.class);
                                showToast(ByProjectActivity.this, error.getItems());
                            }

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            APIError error = gson.fromJson(resp, APIError.class);
                            showToast(ByProjectActivity.this, error.getItems());
                        }
                    });


                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private Call<String> getProjectType() {
        return rudyService.projectType(
                Utils.APP_LANGUAGE
        );
    }

    private void loadProjectType() {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        getProjectType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectType projectType = gson.fromJson(resp, ProjectType.class);
                    setProjectTypeSpinner(projectType);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });


    }

    private void setProjectTypeSpinner(ProjectType projectType) {
        categorieModelArrayList = new ArrayList<>();
        nameList = new ArrayList<>();
        ArrayList<PhasesItem> phases = new ArrayList<PhasesItem>();
        phases.add(new PhasesItem(
                getResources().getString(R.string.all),
                getResources().getString(R.string.all),
                "",
                getResources().getString(R.string.all),
                null));

        projectType.getItems().add(0, new com.scg.rudy.model.pojo.project_type.ItemsItem(
                "",
                getResources().getString(R.string.all),
                phases
        ));

        for (int i = 0; i < projectType.getItems().size(); i++) {
            if (i > 0){
                projectType.getItems().get(i).getPhases().add(0, new PhasesItem(
                        getResources().getString(R.string.all),
                        getResources().getString(R.string.all),
                        "",
                        getResources().getString(R.string.all),
                        null));
            }
            categorieModelArrayList.add(new CategorieModel(projectType.getItems().get(i).getId(), projectType.getItems().get(i).getName()));
            nameList.add(projectType.getItems().get(i).getName());
        }

        customSpinner = new ProjectTypeSpinner(this, nameList);
        typeConstruc.setAdapter(customSpinner);
        typeConstruc.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        typeConstruc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                catePosition = position;
                phasePosition = 0;
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                cateId = categorieModelArrayList.get(position).getId();
                project_type_id = cateId;

                setPhaseSpinner(projectType.getItems().get(position).getPhases());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        typeConstruc.setSelection(catePosition);
    }

    public void setPhaseSpinner(List<PhasesItem> phasesItemList) {
        phaseModelArrayList = new ArrayList<>();
        phaseList = new ArrayList<>();
        for (int i = 0; i < phasesItemList.size(); i++) {
            phaseModelArrayList.add(new CategorieModel(phasesItemList.get(i).getId(), phasesItemList.get(i).getName()));
            phaseList.add(phasesItemList.get(i).getName());
            if (phasesItemList.get(i).getId().equalsIgnoreCase("" + phasePosition)) {
                phasePosition = i;
            }
        }

        ProjectTypeSpinner adapterphase = new ProjectTypeSpinner(this, phaseList);
        maxPhase = phaseList.size();
        if (phaseList.size() > 0) {
            currentPhase.setAdapter(adapterphase);
        } else {
            currentPhase.setAdapter(null);
        }
        currentPhase.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        currentPhase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                phaseId = phaseModelArrayList.get(position).getId();
                phase_id = phaseId;
                phasePosition = position;
                TOTAL_PAGES = 1;
                currentPage = PAGE_START;
                isLoading = false;
                isLastPage = false;
                setAdapter();
                if (firstLoadCount > 0) {
                    loadFirstPage();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private ProjectType fetchProjectType(Response<ProjectType> response) {
        ProjectType projectType = response.body();
        return projectType;
    }

    private void loadFirstPage() {
        searchArrayList = new ArrayList<>();
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("code", "" + response.code());
                Log.e("message", response.message());
                Log.e("response", response.raw().toString());

                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);

                    if (results.size() > 0) {
                        adapter.addAll(results, search);
                        addAll(results);
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    } else {
                        totalProject.setText("0");
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    totalProject.setText("0");
                    TOTAL_PAGES = 1;
                    currentPage = PAGE_START;
                    isLoading = false;
                    isLastPage = false;
                    setAdapter();
                    showToast(ByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });


    }

    private void loadFirstPageProject() {
        projectArrayList = new ArrayList<>();
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        getProjectGroupList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("code", "" + response.code());
                Log.e("message", response.message());
                Log.e("response", response.raw().toString());
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectGroupList obj = gson.fromJson(resp, ProjectGroupList.class);
                    List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> results = customFetchResultsProject(obj);
                    totalProject.setText("1");
                    if (results.size() > 0) {
//                    errorLayout.setVisibility(View.GONE);
                        adapterProject.addAll(results);
                        addAllProject(results);
                        if (currentPagePro < TOTAL_PAGES_Pro) {
                            adapterProject.addLoadingFooter();
                        } else {
                            isLastPagePro = true;
                        }
                    } else {
                        totalProject.setText("0");
                    }

                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showMessage(error.getItems());
                    TOTAL_PAGES_Pro = 1;
                    currentPagePro = PAGE_START_Pro;
                    isLoadingPro = false;
                    isLastPagePro = false;
                    setAdapterProject();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadNextPagePro() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        getProjectGroupList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapterProject.removeLoadingFooter();
                isLoadingPro = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectGroupList obj = gson.fromJson(resp, ProjectGroupList.class);
                    List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> results = customFetchResultsProject(obj);
                    adapterProject.addAll(results);
                    addAllProject(results);
                    if (currentPagePro < TOTAL_PAGES_Pro) {
                        adapterProject.addLoadingFooter();
                    } else {
                        isLastPagePro = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                showToast(ByProjectActivity.this, t.getMessage());
                TOTAL_PAGES_Pro = 1;
                currentPagePro = PAGE_START_Pro;
                isLoadingPro = false;
                isLastPagePro = false;
                setAdapterProject();
            }
        });
    }


    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        getProjectList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Myprojects obj = gson.fromJson(resp, Myprojects.class);
                    List<ItemsItem> results = customFetchResults(obj);
                    adapter.addAll(results, search);
                    addAll(results);
                    if (currentPage < TOTAL_PAGES) {
                        adapter.addLoadingFooter();
                    } else {
                        isLastPage = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private List<ItemsItem> fetchResults(Response<Myprojects> response) {
        TOTAL_PAGES = 1;
        Myprojects skuItem = response.body();
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private List<ItemsItem> customFetchResults(Myprojects skuItem) {
        TOTAL_PAGES = 1;
        totalProject.setText(String.valueOf(skuItem.getTotal()));
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> fetchResultsProject(Response<ProjectGroupList> response) {
        TOTAL_PAGES_Pro = 1;
        ProjectGroupList skuItem = response.body();
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES_Pro = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> customFetchResultsProject(ProjectGroupList skuItem) {
        TOTAL_PAGES_Pro = 1;
        if (skuItem.getTotalPages() != 0) {
            TOTAL_PAGES_Pro = skuItem.getTotalPages();
        }

        return skuItem.getItems();
    }

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {
        if (errorLayout.getVisibility() == View.GONE) {
            totalProject.setText("0");
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(getResources().getString(R.string.not_found_pls_try_again));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");

//            TEST USER เอาไว้เทส user อื่นๆ
//            user_id = "1071";


            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData", e);
        }
    }

    @Override
    public void onResume() {
        if (Utils.getPrefer(this, "print").length() > 0) {
            finish();
        }
        super.onResume();
    }

    public void initView() {
        rudyService = ApiHelper.getClient();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        clsBtn = findViewById(R.id.cls_btn);
        searchBtn = findViewById(R.id.search_btn);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerViewProject = findViewById(R.id.recyclerViewProject);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt);
        statusProject = findViewById(R.id.status_project);
        chance = findViewById(R.id.chance);
        typeConstruc = findViewById(R.id.type_construc);
        currentPhase = findViewById(R.id.current_phase);
        totalProject = findViewById(R.id.total_project);
        btnOther = findViewById(R.id.btn_other);
        btnOther.setOnClickListener(this);
        btnProject = findViewById(R.id.btn_project);
        btnProject.setOnClickListener(this);
        textOther = findViewById(R.id.text_other);
        textProject = findViewById(R.id.text_project);
        linearLayoutProject = findViewById(R.id.linearLayout_project);
        linearLayoutOther = findViewById(R.id.linearLayout_other);
        reStaff = (RelativeLayout) findViewById(R.id.reStaff);
        spinnerStaff = (Spinner) findViewById(R.id.spinnerStaff);
        reDateFrom = (RelativeLayout) findViewById(R.id.reDateFrom);
        spinnerDateFrom = (Spinner) findViewById(R.id.spinnerDateFrom);
        reSort = (RelativeLayout) findViewById(R.id.reSort);
        spinnerSort = (Spinner) findViewById(R.id.spinnerSort);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == searchBtn) {
            if (searchArrayList.size() > 0) {
                Intent intent = new Intent(this, SearchProjectActivity.class);
                startActivity(intent);
            } else {
                showToast(this, getResources().getString(R.string.toast_not_found_unit_data_pls_try_again));
            }
        }

        if (v == btnOther) {
            btnOther.setBackgroundResource(R.color.white);
            btnProject.setBackgroundResource(R.drawable._bg_save);
            textOther.setTextColor(getResources().getColor(R.color.color_btn_save));
            textProject.setTextColor(getResources().getColor(R.color.white));
            linearLayoutOther.setVisibility(View.VISIBLE);
            linearLayoutProject.setVisibility(View.GONE);
            TOTAL_PAGES = 1;
            currentPage = PAGE_START;
            isLoading = false;
            isLastPage = false;
            setAdapter();
            loadFirstPage();
        }

        if (v == btnProject) {
            btnProject.setBackgroundResource(R.color.white);
            btnOther.setBackgroundResource(R.drawable._bg_save);
            textProject.setTextColor(getResources().getColor(R.color.color_btn_save));
            textOther.setTextColor(getResources().getColor(R.color.white));
            linearLayoutProject.setVisibility(View.VISIBLE);
            linearLayoutOther.setVisibility(View.GONE);
            TOTAL_PAGES_Pro = 1;
            currentPagePro = PAGE_START_Pro;
            isLoadingPro = false;
            isLastPagePro = false;
            setAdapterProject();
            loadFirstPageProject();
        }


    }

//    @SuppressLint("StaticFieldLeak")
//    private void DeleteProject(final String url) {
//        Log.i("URL", " : " + url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    showToast(ByProjectActivity.this, getResources().getString(R.string.toast_delete_unit_success));
//
//                    TOTAL_PAGES = 1;
//                    currentPage = PAGE_START;
//                    isLoading = false;
//                    isLastPage = false;
//
//                    loadFirstPage();
//
//                }
//            }
//        }.execute();
//    }

    public void onFragmentDetached(String tag) {

    }


    public void showMessage(String message) {
        showToast(this, message);
    }


    public boolean isNetworkConnected() {
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission(String tel) {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();

        if (!addPermission(this, permissionsList, Manifest.permission.CALL_PHONE)) {
            permissionsNeeded.add("Call");
        }
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showMessageOKCancel(ByProjectActivity.this, message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        callCkeckin(tel);
    }

    private void callCkeckin(String tel) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        callIntent.setData(Uri.parse("tel:" + tel));
        startActivity(callIntent);
    }


    public void addAll(List<ItemsItem> moveResults) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
    }

    public void addAllProject(List<com.scg.rudy.model.pojo.projectGroupList.ItemsItem> moveResults) {
        for (com.scg.rudy.model.pojo.projectGroupList.ItemsItem result : moveResults) {
            addProject(result);
        }
    }

    public void add(ItemsItem r) {
        searchArrayList.add(r);
    }

    public void addProject(com.scg.rudy.model.pojo.projectGroupList.ItemsItem r) {
        projectArrayList.add(r);
    }

//    @Override
//    public void checkInTel(ItemsItem itemList) {//TODO
//        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        dialog.setContentView(R.layout.popup_tel_checkin);
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.setCancelable(true);
//        LinearLayout addViewNameTel = dialog.findViewById(R.id.addViewNameTel);
//        for (int i = 0; i < itemList.getCustomerPhoneList().size(); i++) {
//            final View view = inflater.inflate(R.layout.popup_item_tel, addViewNameTel, false);
//            final LinearLayout addNumber = view.findViewById(R.id.addNumber);
//            final LinearLayout line = view.findViewById(R.id.line);
//            final CircleImageView cus_image = view.findViewById(R.id.cus_image);
//            final TextView cus_name = view.findViewById(R.id.cus_name);
//            final TextView cus_type = view.findViewById(R.id.cus_type);
//            if (i == itemList.getCustomerPhoneList().size() - 1) {
//                line.setVisibility(View.GONE);
//            }
//
//            Glide.with(this)
//                    .load(itemList.getCustomerPhoneList().get(i).getPic())
//                    .into(cus_image);
//            cus_name.setText(itemList.getCustomerPhoneList().get(i).getName());
//            cus_type.setText(itemList.getCustomerPhoneList().get(i).getType());
//
//            for (int j = 0; j < itemList.getCustomerPhoneList().get(i).getPhone().size(); j++) {
//                final View viewNumber = inflater.inflate(R.layout.popup_item_tel_number, addNumber, false);
//                final TextView cus_cusnumber = viewNumber.findViewById(R.id.cus_cusnumber);
//                final RelativeLayout tel = viewNumber.findViewById(R.id.tel);
//
//                cus_cusnumber.setText(itemList.getCustomerPhoneList().get(i).getPhone().get(j));
//                tel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        requestPermission(cus_cusnumber.getText().toString());
//                        dialog.dismiss();
//                    }
//                });
//                addNumber.addView(viewNumber);
//            }
//            addViewNameTel.addView(view);
//        }
//        dialog.show();
//    }

    @Override
    public void checkInTel(ItemsItem itemList) {//TODO
        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_tel_checkin);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final RecyclerView addViewNameTel = dialog.findViewById(R.id.addViewNameTel);
        CheckInTelAdapter checkInTelAdapter = new CheckInTelAdapter(this, itemList.getCustomerPhoneList(), this, dialog);
        addViewNameTel.setLayoutManager(new LinearLayoutManager(this));
        addViewNameTel.setItemAnimator(new DefaultItemAnimator());
        addViewNameTel.setAdapter(checkInTelAdapter);
        dialog.show();
    }


}
