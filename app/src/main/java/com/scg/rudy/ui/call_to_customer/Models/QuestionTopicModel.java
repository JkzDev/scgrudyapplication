
package com.scg.rudy.ui.call_to_customer.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class QuestionTopicModel {

    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("by")
    private String mBy;
    @SerializedName("endDate")
    private String mEndDate;
    @SerializedName("image")
    private String mImage;
    @SerializedName("txtHead")
    private String mTxtHead;

    public QuestionTopicModel(String _image, String _txtHead, String _by, String _endDate, long _amount){
        mImage = _image;
        mTxtHead = _txtHead;
        mBy = _by;
        mEndDate = _endDate;
        mAmount = _amount;
    }

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public String getBy() {
        return mBy;
    }

    public void setBy(String by) {
        mBy = by;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        mEndDate = endDate;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getTxtHead() {
        return mTxtHead;
    }

    public void setTxtHead(String txtHead) {
        mTxtHead = txtHead;
    }

}
