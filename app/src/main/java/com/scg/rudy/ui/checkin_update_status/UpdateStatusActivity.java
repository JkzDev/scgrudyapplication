package com.scg.rudy.ui.checkin_update_status;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.checkin_detail.Phaseprocess;
import com.scg.rudy.model.pojo.checkin_detail.SubPhasesItem;
import com.scg.rudy.model.pojo.project_type.ItemsItem;
import com.scg.rudy.model.pojo.project_type.PhasesItem;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.model.pojo.project_type.SubphasesListItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.checkin.CheckInAreaActivity;
import com.scg.rudy.ui.checkin.CheckInTelActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.UpdateProjectSpinner;
import com.scg.rudy.utils.custom_view.customradiobtton.PresetRadioGroup;
import com.scg.rudy.utils.custom_view.customradiobtton.PresetValueButton;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class UpdateStatusActivity extends BaseActivity implements View.OnClickListener {

    private TextView clsBtn;
    private Spinner currentPhaseSpinner;
    private LinearLayout subPhaseAdd;
    private RelativeLayout saveCheckIn;
    private Bundle extras;
    private RudyService rudyService;
    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private UpdateProjectSpinner customSpinner;
    private String selectId = "";
    private LayoutInflater layoutInflater;

    private String projectId;
    private String projectDetailType = "บ้านเดี่ยว";
    private String subPhase;
    private TextView currentPhaseTitle;
    private int currPhase;
    private Phaseprocess phaseprocess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_status);
        initView();
        setOnClick();
        getProjectTypeAPI();
    }

    private void setOnClick(){
        saveCheckIn.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
    }

    private Call<String> getProjectType() {
        return rudyService.projectType(
                Utils.APP_LANGUAGE
        );
    }

    private Call<String> postUpdateProcess(String projectId, String phaseId, String sub_phase_id_list, String status_list) {
        return rudyService.postUpdateProcess(
                Utils.APP_LANGUAGE,
                projectId,
                phaseId,
                sub_phase_id_list,
                status_list
        );
    }

    private void postUpdateProcessAPI(String projectId, String phaseId, String sub_phase_id_list, String status_list) {
        showLoading(this);
        postUpdateProcess(projectId,phaseId,sub_phase_id_list,status_list).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    UpdateStatus updateStatus  = gson.fromJson(resp, UpdateStatus.class);
                    setCheckInDetail(updateStatus);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(UpdateStatusActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(UpdateStatusActivity.this,fetchErrorMessage(t));
            }
        });
    }


    private void getProjectTypeAPI() {
        showLoading(this);
        getProjectType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ProjectType projectType  = gson.fromJson(resp, ProjectType.class);
                    setCheckInData(projectType);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(UpdateStatusActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(UpdateStatusActivity.this,fetchErrorMessage(t));
            }
        });
    }

    private void setCheckInDetail(final UpdateStatus response){
        if(response.getStatus()==200){
            CheckInAreaActivity.isUpdate = "1";
            CheckInTelActivity.isUpdate = "1";
            showToast(this,getResources().getString(R.string.toast_update_unit_success));
            finish();
        }else{
            showToast(this,response.getItems());
        }
    }

    private void setCheckInData(final ProjectType projectType){

        ArrayList<String> idList = new ArrayList<>();
        nameList = new ArrayList<>();
        categorieModelArrayList = new ArrayList<>();
        List<ItemsItem> typeItems = projectType.getItems();
        List<PhasesItem> phasesItemList = null;

        for (int i = 0; i < typeItems.size(); i++) {
            List<PhasesItem> phases = typeItems.get(i).getPhases();
            if (projectDetailType.equalsIgnoreCase(typeItems.get(i).getName())) {
                phasesItemList = typeItems.get(i).getPhases();
                for (int j = 0; j < phases.size(); j++) {
                    categorieModelArrayList.add(new CategorieModel( phases.get(j).getId(),  phases.get(j).getName()));
                    nameList.add(phases.get(j).getName());
                    idList.add(phases.get(j).getId());
                }
            }
        }

        customSpinner = new UpdateProjectSpinner(this, nameList);
        currentPhaseSpinner.setAdapter(customSpinner);
        currentPhaseSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        final List<PhasesItem> finalPhasesItemList = phasesItemList;
        currentPhaseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                selectId = finalPhasesItemList.get(position).getId();
                addUpdateLayout(finalPhasesItemList.get(position).getSubphasesList());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        currPhase = idList.indexOf(subPhase);
        currentPhaseSpinner.setSelection(currPhase);
        currentPhaseTitle.setText(String.format(this.getResources().getString(R.string.phase_parame), (currPhase+1)+" "+phaseprocess.getPhaseName()));
    }

    private void addUpdateLayout(final List<SubphasesListItem> subphasesListItems){
        subPhaseAdd.removeAllViews();
        for (int i = 0; i < subphasesListItems.size(); i++) {
            final View view = layoutInflater.inflate(R.layout.custom_update_radio_button, subPhaseAdd, false);
            final TextView subphase_txt = view.findViewById(R.id.subphase_txt);
            final PresetRadioGroup radio_group = view.findViewById(R.id.radio_group);
            final PresetValueButton process = view.findViewById(R.id.process);
            final PresetValueButton done = view.findViewById(R.id.done);
            final PresetValueButton waiting = view.findViewById(R.id.waiting);

            final int finalI = i;
            if(String.valueOf(selectId).equalsIgnoreCase(phaseprocess.getPhaseId())){
                SubPhasesItem subPhasesItem = phaseprocess.getSubPhases().get(finalI);
                if(subPhasesItem.getStatus().equalsIgnoreCase("0")){
                    radio_group.setTag(subphasesListItems.get(finalI).getId()+",0");
                    process.setChecked(true);
                }
                if(subPhasesItem.getStatus().equalsIgnoreCase("1")){
                    radio_group.setTag(subphasesListItems.get(finalI).getId()+",1");
                    done.setChecked(true);
                }
                if(subPhasesItem.getStatus().equalsIgnoreCase("2")){
                    radio_group.setTag(subphasesListItems.get(finalI).getId()+",2");
                    waiting.setChecked(true);
                }
            }else{
                radio_group.setTag(subphasesListItems.get(finalI).getId()+",0");
            }

            radio_group.setOnCheckedChangeListener(new PresetRadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View radioGroup, View radioButton, boolean isChecked, int checkedId) {
                    if(radioButton==process){
                        radio_group.setTag(subphasesListItems.get(finalI).getId()+",0");
                    }
                    if(radioButton==done){
                        radio_group.setTag(subphasesListItems.get(finalI).getId()+",1");
                    }
                    if(radioButton==waiting){
                        radio_group.setTag(subphasesListItems.get(finalI).getId()+",2");
                    }
                }
            });

            subphase_txt.setText(String.format(this.getResources().getString(R.string.sub_phase_parame), subphasesListItems.get(i).getName()));
            subPhaseAdd.addView(view,i);
        }
    }

    private void getChildValue(){
        StringBuilder valId = new StringBuilder();
        StringBuilder valSta = new StringBuilder();
        for(int i=0; i<subPhaseAdd.getChildCount(); i++) {
            if(subPhaseAdd.getChildAt(i) instanceof LinearLayout){
                View child = subPhaseAdd.getChildAt(i);
                PresetRadioGroup radio_group = child.findViewById(R.id.radio_group);
                String sub_phase[] = radio_group.getTag().toString().split(",");
                if(i==0){
                    valId.append(sub_phase[0]);
                    valSta.append(sub_phase[1]);
                }else{
                    valId.append(",").append(sub_phase[0]);
                    valSta.append(",").append(sub_phase[1]);
                }
            }
        }
        postUpdateProcessAPI(projectId,selectId,valId.toString(),valSta.toString());
//        showToast(this,"sub phase : "+selectId+"\nsub phase id :"+valId.toString()+"\nphase stat : "+valSta.toString());
    }

    private  ProjectType fetchResults(Response<ProjectType> response) {
        ProjectType projectType = response.body();
        return projectType;
    }

    private  UpdateStatus fetchResultsDetail(Response<UpdateStatus> response) {
        UpdateStatus updateStatus = response.body();
        return updateStatus;
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }



    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return true;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        layoutInflater = LayoutInflater.from(this);
        extras = getIntent().getExtras();
        if (extras != null) {
            projectId = extras.getString("projectId");
            projectDetailType = extras.getString("projectType");
            subPhase  = extras.getString("subPhase");
            phaseprocess = extras.getParcelable("phaseprocess");
        }
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        currentPhaseSpinner = findViewById(R.id.current_phase);
        subPhaseAdd = findViewById(R.id.sub_phase_add);
        saveCheckIn = findViewById(R.id.saveCheckIn);
        currentPhaseTitle = findViewById(R.id.current_phase_title);
    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }
        if(v==saveCheckIn){
            getChildValue();
        }
    }
}
