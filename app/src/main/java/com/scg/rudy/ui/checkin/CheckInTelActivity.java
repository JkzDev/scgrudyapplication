package com.scg.rudy.ui.checkin;

import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.checkin_detail.CheckInDetail;
import com.scg.rudy.model.pojo.checkin_detail.Items;
import com.scg.rudy.model.pojo.checkin_detail.Phaseprocess;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.checkin_update_status.UpdateStatusActivity;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.utils.Utils;

import java.util.concurrent.TimeoutException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CheckInTelActivity extends BaseActivity implements View.OnClickListener {
    private RudyService rudyService;
    private TextView clsBtn;
    private Bundle extras;
    private String projectId;
    private String checkInID;
    private String subPhase;
    private CircleImageView cusImage;
    private TextView type;
    private TextView name;
    private TextView tel;
    private EditText detail;
    private CardView updateDepartment;
    private RelativeLayout saveCheckIn;

    private String CustomerName;
    private String CustomerPhone;
    private String CustomerPic;
    private String projectType;
    private Phaseprocess _phaseprocess;
    private LinearLayout hide_key;
    public static String isUpdate = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_tel);
        initView();
        setOnClick();
        name.setText(CustomerName);
        tel.setText(CustomerPhone);
        Glide.with(this)
                .load(CustomerPic)
                .into(cusImage);
        getCheckInDetail();
    }

    private Call<String> postEditCheckin() {
        return rudyService.postEditCheckin(
                Utils.APP_LANGUAGE,
                checkInID,
                projectId,
                "1",
                detail.getText().toString(),
                "",
                ""
        );
    }

    private Call<String> getCheckinDetail() {
        return rudyService.getCheckInDetail(
                Utils.APP_LANGUAGE,
                checkInID
        );
    }


    private void postEditCheckInAPI() {
        showLoading(this);
        postEditCheckin().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    Items dashboardResults  = gson.fromJson(resp, Items.class);
                    setCheckInData();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInTelActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInTelActivity.this,fetchErrorMessage(t));
            }
        });
    }

    private void getCheckInDetail() {
        showLoading(this);
        getCheckinDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    CheckInDetail checkInDetail  = gson.fromJson(resp, CheckInDetail.class);
                    setPhaseData(checkInDetail.getItems().getPhaseprocess());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInTelActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInTelActivity.this,fetchErrorMessage(t));
            }
        });
    }


    private void setPhaseData(Phaseprocess phaseData){
       _phaseprocess = phaseData;
    }

    private void setCheckInData(){
        ProjectDetailActivity.isUpdateCheckin = "1";
        showToast(CheckInTelActivity.this,getResources().getString(R.string.toast_visit_unit_success));
        finish();
    }


    private Items fetchResults(Response<CheckInDetail> response) {
        CheckInDetail checkInDetail = response.body();
        return checkInDetail.getItems();
    }

    private Phaseprocess phaseprocessResults(Response<CheckInDetail> response) {
        CheckInDetail checkInDetail = response.body();
        return checkInDetail.getItems().getPhaseprocess();
    }


    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }


    private void setOnClick(){
        clsBtn.setOnClickListener(this);
        updateDepartment.setOnClickListener(this);
        saveCheckIn.setOnClickListener(this);
        hide_key.setOnClickListener(this);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        extras = getIntent().getExtras();
        rudyService = ApiHelper.getClient();
        CheckInTelActivity.isUpdate = "0";
        if (extras != null) {
            projectId = extras.getString("projectId");
            checkInID = extras.getString("checkInID");
            CustomerName = extras.getString("CustomerName");
            CustomerPhone = extras.getString("CustomerPhone");
            CustomerPic = extras.getString("CustomerPic");
            projectType = extras.getString("type");
            subPhase  = extras.getString("subPhase");
        }
        clsBtn = (TextView) findViewById(R.id.cls_btn);
        cusImage = (CircleImageView) findViewById(R.id.cus_image);
        type = (TextView) findViewById(R.id.type);
        name = (TextView) findViewById(R.id.name);
        tel = (TextView) findViewById(R.id.tel);
        detail = (EditText) findViewById(R.id.detail);
        updateDepartment = (CardView) findViewById(R.id.update_department);
        saveCheckIn = (RelativeLayout) findViewById(R.id.saveCheckIn);
        hide_key =  findViewById(R.id.hide_key);
    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            setCheckInData();
//            if(!CheckInTelActivity.isUpdate.equalsIgnoreCase("0")){
//                ProjectDetailActivity.isUpdateCheckin = "1";
//                showToast(CheckInTelActivity.this,"เข้าเยี่ยมหน่วยงานเรียบร้อย");
//            }
//            finish();
        }

        if(v==saveCheckIn){
            if(detail.getText().toString().isEmpty()){
                showToast(CheckInTelActivity.this,getResources().getString(R.string.toast_pls_specify_detail_visit));
            }else {
                postEditCheckInAPI();
            }
        }

        if(v==updateDepartment){
            Intent intent = new Intent(CheckInTelActivity.this, UpdateStatusActivity.class);
            intent.putExtra("projectId",projectId);
            intent.putExtra("projectType",projectType);
            intent.putExtra("subPhase",subPhase);
            intent.putExtra("phaseprocess",_phaseprocess);
            startActivity(intent);
        }
        if(v==hide_key){
             Utils.hideKeyboard(this);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setCheckInData();
    }
}
