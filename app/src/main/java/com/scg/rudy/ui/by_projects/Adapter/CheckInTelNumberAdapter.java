package com.scg.rudy.ui.by_projects.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.scg.rudy.R;
import com.scg.rudy.ui.by_projects.ByProjectsListener;
import java.util.List;

public class CheckInTelNumberAdapter extends RecyclerView.Adapter<CheckInTelNumberAdapter.MyViewHolder>{

    private Context context;
    private List<String> phone;
    private ByProjectsListener listener;
    private Dialog dialog;

    public CheckInTelNumberAdapter(Context _context, List<String> _phone, ByProjectsListener _listener, Dialog _dialog){
        this.context = _context;
        this.phone = _phone;
        this.listener = _listener;
        this.dialog = _dialog;
    }

    @NonNull
    @Override
    public CheckInTelNumberAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.popup_item_tel_number, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull CheckInTelNumberAdapter.MyViewHolder holder, int position) {
        holder.cusNumber.setText(phone.get(position));
        holder.tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.requestPermission(phone.get(position));
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return phone.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView cusNumber;
        private RelativeLayout tel;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cusNumber = itemView.findViewById(R.id.cus_cusnumber);
            tel = itemView.findViewById(R.id.tel);
        }
    }
}
