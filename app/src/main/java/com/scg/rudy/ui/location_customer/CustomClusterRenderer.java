//package com.scg.rudy.ui.location_customer;
//
//import android.content.Context;
//
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.model.BitmapDescriptor;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.maps.android.clustering.ClusterManager;
//import com.google.maps.android.clustering.view.DefaultClusterRenderer;
//import com.scg.rudy.R;
//import com.scg.rudy.ui.location_customer.MapItem.mapItem;
//
//public class CustomClusterRenderer  extends DefaultClusterRenderer<mapItem> {
//
//    private final Context mContext;
//
//    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<mapItem> clusterManager) {
//        super(context, map, clusterManager);
//        mContext = context;
//    }
//
//    @Override protected void onBeforeClusterItemRendered(mapItem item,MarkerOptions markerOptions) {
//       // final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
//        final BitmapDescriptor eyes = BitmapDescriptorFactory.fromResource(R.drawable.eyes);
//        final BitmapDescriptor move = BitmapDescriptorFactory.fromResource(R.drawable.move);
//        final BitmapDescriptor conn = BitmapDescriptorFactory.fromResource(R.drawable.eyes_accept);
//        if(item.getmAppName().contains("eye")){
//            markerOptions.icon(eyes).title(item.getTitle());
//        }else if(item.getmAppName().contains("move")){
//            markerOptions.icon(move).title(item.getTitle());
//        }else{
//            markerOptions.icon(conn).title(item.getTitle());
//        }
//    }
//}
