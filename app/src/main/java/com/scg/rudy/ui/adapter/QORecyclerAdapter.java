package com.scg.rudy.ui.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.transaction_detail.PrItem;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.adapter.Adapter.TransectionDetailUnitCodeSpinner;
import com.scg.rudy.ui.adapter.QOModels.ProductModel;
import com.scg.rudy.ui.adapter.QOModels.UnitList;
import com.scg.rudy.ui.login.LoginActivity;
import com.scg.rudy.ui.transaction_detail.QoInterface;
import com.scg.rudy.utils.Utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.isEmpty;
import static com.scg.rudy.utils.Utils.showToast;

/**
 *
 * @author DekDroidDev
 * @date 9/2/2018 AD
 */

public class QORecyclerAdapter extends RecyclerView.Adapter<QORecyclerAdapter.ProjectViewHolder> {

    private List<PrItem>  cartModelArrayLis ;
    private Context context;
    private QoInterface  listener;
    private RudyService rudyService;
    private String skuid;
    private ArrayList<UnitList> unitList;
    private String unit_id;
    private String discountType;
    private int fristLoadFlag = 0;

    public QORecyclerAdapter(Context context, List<PrItem> cartModelArrayLis, QoInterface listener) {
        this.cartModelArrayLis = cartModelArrayLis;
        Collections.sort(this.cartModelArrayLis, new Comparator<PrItem>() {
            @Override
            public int compare(PrItem p1, PrItem p2) {
                return Integer.valueOf(p1.getPriceRank()).compareTo( Integer.valueOf(p2.getPriceRank()));
            }
        });
        this.context = context;
        this.listener = listener;
    }

    @Override
    public QORecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.qo_item, parent, false);
        final QORecyclerAdapter.ProjectViewHolder vh = new QORecyclerAdapter.ProjectViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(QORecyclerAdapter.ProjectViewHolder _holder,  int _position) {
        final int position = _position;
        final PrItem prCodeDetailModel = cartModelArrayLis.get(position);
        final ProjectViewHolder holder = _holder;
        rudyService = ApiHelper.getClient();


        holder.bindData(cartModelArrayLis.get(position));
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(cartModelArrayLis.get(position).isCheck());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton,final boolean check) {
                if(check){
                    final Double itemPrice = Double.parseDouble(prCodeDetailModel.getNetAmount());
                    cartModelArrayLis.get(holder.getAdapterPosition()).setCheck(check);
                    listener.onSelectItem(prCodeDetailModel.getProductId(),itemPrice,check);

                }else{
                    final Double itemPrice = Double.parseDouble(prCodeDetailModel.getNetAmount());
                    cartModelArrayLis.get(holder.getAdapterPosition()).setCheck(check);
                    listener.onSelectItem(prCodeDetailModel.getProductId(),-itemPrice,check);
                }
            }
        });
        holder.dataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean check  = cartModelArrayLis.get(position).isCheck();
                holder.checkBox.setChecked(!check);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null) {
            return 0;
        }
        return cartModelArrayLis.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        final TextView name;
        final TextView price ;
        final TextView totalItem ;
        final TextView totalPrice;
        final RelativeLayout status ;
        final RelativeLayout status2 ;
        final ImageButton more_action;
        final CheckBox checkBox;
        final TextView totalDiscount;
        final TextView textDiscount;
        final TextView comment;
        final LinearLayout liComment;
        final RelativeLayout dataView;



        public ProjectViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.name);
            this.price = view.findViewById(R.id.price);
            this.totalItem = view.findViewById(R.id.total_item);
            this.totalPrice = view.findViewById(R.id.total_price);
            this.status = view.findViewById(R.id.status);
            this.status2 = view.findViewById(R.id.status2);
            this.more_action = view.findViewById(R.id.more_action);
            this.checkBox = view.findViewById(R.id.checkBox);
            this.totalDiscount = view.findViewById(R.id.totalDiscount);
            this.textDiscount = view.findViewById(R.id.textDiscount);
            this.comment = view.findViewById(R.id.comment);
            this.liComment = view.findViewById(R.id.liComment);
            this.dataView = view.findViewById(R.id.reData);

        }


        private Call<String> callProductUnit(PrItem prItem) {
            return rudyService.productUnit(
                    Utils.APP_LANGUAGE,
                    prItem.getProductId()

            );
        }

        public void loadProductUnit(Dialog dialog, PrItem prItem) {
            callProductUnit(prItem).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Gson gson = new Gson();
                    String resp = response.body();
                    if(resp.contains("200")){
                        ProductModel productModel  = gson.fromJson(resp, ProductModel.class);
                        unitList = new ArrayList<>();
                        unitList = (ArrayList<UnitList>) productModel.getItems().getUnitList();

                        int positionUnit = 0;

                        if (unitList.size() > 0){

                            ArrayList<String> productUnit = new ArrayList<>();

                            for (int i = 0; i < unitList.size(); i++){
                                productUnit.add(unitList.get(i).getUnit());
                                if (prItem.getUnit_id().equalsIgnoreCase(unitList.get(i).getId())){
                                    positionUnit = i;
                                }
                            }

                            final Spinner spinner = dialog.findViewById(R.id.spinner);
                            TransectionDetailUnitCodeSpinner aa = new TransectionDetailUnitCodeSpinner(context, productUnit);
                            spinner.setAdapter(aa);
                            spinner.setSelection(positionUnit);

                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                                    unit_id = unitList.get(position).getId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            final ImageView imgDropDown = dialog.findViewById(R.id.imgDropDown);
                            imgDropDown.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    spinner.performClick();
                                }
                            });
                        }

                    }else{
                        unitList = null;
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    unitList = null;
                }
            });
        }


        public void bindData(final PrItem prCodeDetailModel) {
            name.setText( prCodeDetailModel.getProductName());
            if (prCodeDetailModel.getNote().length() > 0){
                liComment.setVisibility(View.VISIBLE);
                comment.setText(prCodeDetailModel.getNote());
            }else{
                liComment.setVisibility(View.GONE);
                comment.setText("");
            }
            totalItem.setText(Utils.moneyFormat(prCodeDetailModel.getQty().toString().replace("null","1")));
            totalDiscount.setText(prCodeDetailModel.getDiscount());
            if (prCodeDetailModel.getDiscount_type().equalsIgnoreCase("1")){
                textDiscount.setText(context.getResources().getString(R.string.discount) + "( % )");
            }else{
                textDiscount.setText(context.getResources().getString(R.string.discount) + "( "+ Utils.currencyForShow +" )");
            }
            price.setText(Utils.moneyFormat(prCodeDetailModel.getPrice()));
            totalPrice.setText(Utils.moneyFormat(prCodeDetailModel.getNetAmount()));
            status.setBackgroundColor(context.getResources().getColor(R.color.txt_tab2));
            status2.setBackgroundColor(context.getResources().getColor(R.color.txt_tab2));
            checkBox.setVisibility(View.INVISIBLE);

            if (prCodeDetailModel.getPriceRank().contains("1")) {
                status.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                status2.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                checkBox.setVisibility(View.VISIBLE);
            }
            if (prCodeDetailModel.getPriceRank().contains("2")) {
                status.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                status2.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                checkBox.setVisibility(View.VISIBLE);
            }
            if(!prCodeDetailModel.getApproveBy().equalsIgnoreCase("0")){
                status.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                status2.setBackgroundColor(context.getResources().getColor(R.color.status_prove));
                checkBox.setVisibility(View.VISIBLE);
            }




            more_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final PopupWindow popup = new PopupWindow(context);
                    View layout = LayoutInflater.from(context).inflate(R.layout.popup_edit, null);
                    popup.setContentView(layout);

                    View edit = layout.findViewById(R.id.edit);
                    View delete = layout.findViewById(R.id.delete);
                    // Set content width and height
                    popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                    popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                    // Closes the popup window when touch outside of it - when looses focus
                    popup.setOutsideTouchable(true);
                    popup.setFocusable(true);
                    // Show anchored to button
                    popup.setBackgroundDrawable(new BitmapDrawable());
                    popup.showAsDropDown(v, 0, 0, Gravity.LEFT);

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popup.dismiss();
                            final Dialog dialog = new Dialog(context);
//                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.edit_item_dialog);
                            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            dialog.setCanceledOnTouchOutside(false);
                            final EditText total = dialog.findViewById(R.id.total);
                            final EditText Edprice = dialog.findViewById(R.id.price);
                            final RelativeLayout submit = dialog.findViewById(R.id.submit);
                            final RelativeLayout cancel = dialog.findViewById(R.id.cancel);
                            final TextView skuName = dialog.findViewById(R.id.skuName);
                            final TextView price1 = dialog.findViewById(R.id.price1);

                            final EditText dialogTotalDiscount  = dialog.findViewById(R.id.dialogTotalDiscount);
                            final TextView priceDiscount = dialog.findViewById(R.id.priceDiscount);

                            final TextView textCurrency = dialog.findViewById(R.id.textCurrency);
                            textCurrency.setText(Utils.currencyForShow);

                            final ImageView imgSpinnerDiscount = dialog.findViewById(R.id.imgDropDownDiscount);
                            final Spinner spinnerDiscount = dialog.findViewById(R.id.spinnerDiscount);

                            final TextView netPrice = dialog.findViewById(R.id.netPrice);




                            ArrayList<String> aa = new ArrayList<>();
                            aa.add("%");
                            aa.add(Utils.currencyForShow);
                            TransectionDetailUnitCodeSpinner customSpinnerDiscount = new TransectionDetailUnitCodeSpinner(context, aa);
                            spinnerDiscount.setAdapter(customSpinnerDiscount);
                            spinnerDiscount.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                            total.setText(prCodeDetailModel.getQty().replace("null","1"));
                            Edprice.setText(prCodeDetailModel.getPrice());
                            skuName.setText(prCodeDetailModel.getProductName());
                            price1.setText(Utils.moneyFormat("" + prCodeDetailModel.getProductPrice().getPrice()));
                            discountType = prCodeDetailModel.getDiscount_type();
                            dialogTotalDiscount.setText(prCodeDetailModel.getDiscount());
                            if (discountType.equalsIgnoreCase("1")){
                                priceDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * Double.parseDouble(Edprice.getText().toString().replace(",", "")) * Double.parseDouble(totalDiscount.getText().toString()) / 100)));
                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * Double.parseDouble(Edprice.getText().toString().replace(",", "")) - (Double.parseDouble(total.getText().toString().replace(",", "")) * Double.parseDouble(Edprice.getText().toString().replace(",", "")) * Double.parseDouble(totalDiscount.getText().toString()) / 100))) + " " + Utils.currencyForShow);
                                spinnerDiscount.setSelection(0);
                            }else{
                                priceDiscount.setText(dialogTotalDiscount.getText().toString());
                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * Double.parseDouble(Edprice.getText().toString().replace(",", "")) - Double.parseDouble(priceDiscount.getText().toString().replace(",", "")))) + " " + Utils.currencyForShow);
                                spinnerDiscount.setSelection(1);
                            }
                            spinnerDiscount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                                    if (fristLoadFlag > 0 ){
                                        dialogTotalDiscount.setText("");
                                    }
                                    fristLoadFlag++;
                                    if (position == 0){
                                        Utils.inputMaxLength(dialogTotalDiscount, 2);
                                        discountType = "1";
                                    }else{
                                        Utils.inputMaxLength(dialogTotalDiscount, 7);
                                        discountType = "2";
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            imgSpinnerDiscount.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    spinnerDiscount.performClick();
                                }
                            });


                            loadProductUnit(dialog, prCodeDetailModel);

                            submit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if(Utils.isEmpty(total)){
                                        showToast((Activity)context, context.getResources().getString(R.string.toast_pls_fill_product_amount));
                                        return;
                                    }
                                    if(Utils.isEmpty(Edprice)){
                                        showToast((Activity)context, context.getResources().getString(R.string.toast_pls_fill_product_price));
                                        return;
                                    }
                                    listener.onClickEdit(prCodeDetailModel.getProductId(),total.getText().toString().replace(",",""),Edprice.getText().toString().replace(",",""), unit_id, dialogTotalDiscount.getText().toString(), discountType);
                                    fristLoadFlag = 0;
                                    dialog.dismiss();
                                }
                            });

                            dialogTotalDiscount.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (dialogTotalDiscount.getText().toString().length() == 1 && dialogTotalDiscount.getText().toString().equalsIgnoreCase(".")){
                                        dialogTotalDiscount.setText("");
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (discountType.equalsIgnoreCase("1")){
                                        if (isEmpty(total)) {
                                            netPrice.setText("0 " + Utils.currencyForShow);
                                            priceDiscount.setText("0");
                                        } else {
                                            if (!dialogTotalDiscount.getText().toString().matches("")) {
                                                priceDiscount.setText(Utils.moneyFormat("" + ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", "")))) * (Double.parseDouble(dialogTotalDiscount.getText().toString().replace(",", ""))/100))));
                                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", "")))) * (Double.parseDouble(dialogTotalDiscount.getText().toString().replace(",", ""))/100)))) + " " + Utils.currencyForShow);

                                            } else {
                                                priceDiscount.setText("0");
                                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))))) + " " + Utils.currencyForShow);
                                            }
                                        }

                                    }else {
                                        if (isEmpty(total)) {
                                            netPrice.setText("0 " + Utils.currencyForShow);
                                            priceDiscount.setText("0");
                                        } else {
                                            if (!dialogTotalDiscount.getText().toString().matches("")) {
                                                priceDiscount.setText(Utils.moneyFormat("" + dialogTotalDiscount.getText().toString()));
                                                netPrice.setText(Utils.moneyFormat("" + ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", "")))) - (Double.parseDouble(priceDiscount.getText().toString().replace(",", ""))))) + " " + Utils.currencyForShow);
                                            } else {
                                                priceDiscount.setText("0");
                                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", ""))))) + " " + Utils.currencyForShow);
                                            }
                                        }
                                    }


//                                    if (discountType.equalsIgnoreCase("1")){
//                                        if (!dialogTotalDiscount.getText().toString().matches("")){
//                                            priceDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(price.getText().toString().replace(",", "")) - (Double.parseDouble(price.getText().toString().replace(",", "")) * Double.parseDouble(totalDiscount.getText().toString()) / 100))));
//                                        }else{
//                                            priceDiscount.setText(Utils.moneyFormat("" + price.getText().toString()));
//                                        }
//                                    }else{
//                                        if (!dialogTotalDiscount.getText().toString().matches("")){
//                                            priceDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(price.getText().toString().replace(",", "")) - Double.parseDouble(totalDiscount.getText().toString()))));
//                                        }else{
//                                            priceDiscount.setText(Utils.moneyFormat("" + price.getText().toString()));
//                                        }
//                                    }
                                }
                            });

                            total.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (total.getText().toString().length() == 1 && total.getText().toString().equalsIgnoreCase(".")){
                                        total.setText("");
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (isEmpty(total)) {

                                        netPrice.setText("0 " + Utils.currencyForShow);
                                        priceDiscount.setText("0");

                                    } else {
                                        if (dialogTotalDiscount.getText().toString().matches("")) {
                                            netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))))) + " " + Utils.currencyForShow);
                                            priceDiscount.setText("0");
                                        } else {
                                            if (discountType.equalsIgnoreCase("1")){
                                                priceDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) * (Double.parseDouble(dialogTotalDiscount.getText().toString()) / 100))));
                                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", "")))) * (Double.parseDouble(dialogTotalDiscount.getText().toString()) / 100)))) + " " + Utils.currencyForShow);
                                            }else{
                                                priceDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(Edprice.getText().toString().replace(",", "")))) - (Double.parseDouble(dialogTotalDiscount.getText().toString()))))));
                                                netPrice.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - (Double.parseDouble(dialogTotalDiscount.getText().toString())))) + " " + Utils.currencyForShow);
                                            }
                                        }
                                    }
                                }
                            });

                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    fristLoadFlag = 0;
                                    dialog.dismiss();
                                }
                            });

                            dialog.show();
                        }
                    });



                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popup.dismiss();
                            MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                                    .title(context.getString(R.string.dialog_want_to_delete_product) + prCodeDetailModel.getProductName()+" ?")
                                    .positiveText(context.getResources().getString(R.string.agreed))
                                    .negativeText(context.getResources().getString(R.string.cancel))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {

                                            listener.onClickDelete(prCodeDetailModel.getProductId());
                                            dialog.dismiss();
//                                        listener.onClickDelete("");

                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    });

                            MaterialDialog del_dialog = builder.build();
                            del_dialog.show();
                        }
                    });



                }
            });





        }

    }


}