package com.scg.rudy.ui.transaction_detail;

import com.scg.rudy.model.pojo.transaction_detail.PrItem;
import com.scg.rudy.ui.transaction_detail.model.skuModel;

import java.util.List;

/**
 * Created by DekDroidDev on 12/2/2018 AD.
 */

public interface QoInterface {
    void onClickEdit(String itemId, String itemQty, String itemPrice, String unit_id, String discount, String discount_type);
    void onClickDelete(String itemId);
    void onSelectItem(String itemId,Double itemPrice,boolean isCheck);
    void onSearchItemClick(skuModel countryModel);

}
