package com.scg.rudy.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.model.pojo.user.Items;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.add_new_project.AddNewProjectActivity;
import com.scg.rudy.ui.add_new_project.Models.StorageDataOfflineModel;
import com.scg.rudy.ui.add_new_project.StorageDataOffline.StorageDataOffline;
import com.scg.rudy.ui.by_customers.ByCustomersActivity;
import com.scg.rudy.ui.by_projects.ByProjectActivity;
import com.scg.rudy.ui.calendar.CalendarActivity;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.dashboard.DashboardActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.dealbyproject.DealByProjectActivity;
import com.scg.rudy.ui.location_customer.LocationCustomerActivity;
import com.scg.rudy.ui.login.LoginActivity;
import com.scg.rudy.ui.main.Adapter.ChangeLanguageAdapter;
import com.scg.rudy.ui.main.Models.CountEyeModel;
import com.scg.rudy.ui.main.Models.LanguageModel;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.ui.sale_wallet.SaleWalletActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.hover.introduction.HoverMotion;
import com.scg.rudy.utils.custom_view.hover.service.RudyHoverMenuService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getDeleteProject;
import static com.scg.rudy.data.remote.ApiEndPoint.getViewDetail;
import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.savePrefer;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {
    boolean doubleBackToExitPressedOnce = false;
    private static final String PREFS_FILE = "hover";
    @BindView(R.id.quick_sale)
    LinearLayout quickSale;
    @BindView(R.id.bycustomer)
    LinearLayout bycustomer;
    @BindView(R.id.bylocation)
    LinearLayout bylocation;
    @BindView(R.id.byphase)
    LinearLayout byphase;
    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.dashboard)
    LinearLayout dashboard;
    @BindView(R.id.addnewdata)
    CardView addnewdata;
    @BindView(R.id.calendar)
    LinearLayout calendar;

    @BindView(R.id.cus_name)
    TextView cusName;
    @BindView(R.id.cus_type)
    TextView cusType;
    @BindView(R.id.sale_wallet)
    LinearLayout saleWallet;

    @BindView(R.id.rootView)
    RelativeLayout rootView;


    private String phaseId = "1";
    private String cateId = "1";
    private String unit_number = "";
    private String floor_number = "";
    //    private String unit_budget = "2.5";
    private String c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
    private String c_id_owner = "";
    private int ctype = 1;
    private int ctype_owner = 3;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
    private String cname = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
    private String ccompany = "Rudy";// บริษัทลูกค้า
    private String cphone = "";// เบอร์ติดต่อลูกค้า
    private String cline = "";// line ID ของลูกค้า
    private String cnote = "";// noteลูกค้า
    private String cname_owner = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
    private String ccompany_owner = "Rudy";// บริษัทลูกค้า
    private String cphone_owner = "";// เบอร์ติดต่อลูกค้า
    private String cline_owner = "";// line ID ของลูกค้า
    private String cnote_owner = "";// noteลูกค้า
    private String other_type = "";
    private String shopID = "";
    private String image64_1 = "";
    private String image64_2 = "";
    private String image64_3 = "";
    private String image64_4 = "";
    private String image64_5 = "";
    private String image64_6 = "";
    double unit_budget_number = 0.0;
    public static double DragLat = 0.0, DragLng = 0.0;
    public static String quick_projectId = "";
    public static boolean data1 = false;
    public static boolean data2 = false;
    public static boolean data3 = false;
    public static boolean data4 = false;
    public static boolean data5 = false;

    public static boolean gdata1 = false;
    public static boolean gdata2 = false;
    public static boolean gdata3 = false;
    public static boolean gdata4 = false;
    public static boolean gdata5 = false;

    public static final String TAG = "MainActivity";
    //   New post data api
    public static String user_id;//* : User who add the project
    public static String lat_lng = "";//: project lat and lng [13.729194,100.622218]
    public static String project_address = "";//: project location address
    public static String project_name = "";//: Project name [API will create if you did not post it]
    public static String project_type_id = "";//: project type id [API will create if you did not post it  ;//: 1]
    public static String phase_id = "";//: phase id [API will create if you did not post it  ;//: 1]
    public static String units = "";//: How many unit [API will create if you did not post it  ;//: 1]
    public static String unit_area = "";//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
    public static String unit_budget = "";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
    public static String project_stories = "";//: story [API will create if you did not post it  ;//: 2]

    public static int customer_type = 1;//: Customer type id [1= individual, 2=company]
    public static String customer_name = "";//: Customer’s name
    public static String customer_phone = "";//: Customer’s phone
    public static String customer_email = "";//: Customer’s email
    public static String customer_line = "";//: Customer’s line
    public static String customer_tax_no = "";//: Customer’s tax no or ID card no.
    public static String customer_note = "";//: Customer’s note

    public static String house_owner_name = "";//: House owner name
    public static String house_owner_phone = "";//: house_owner_phone
    public static String house_owner_line = "";//: House owner line id
    public static String House_owner_note = "";//: House owner note
    public static String Project_owner_name = "";//: Project owner name
    public static String Project_owner_phone = "";//: Project_owner_phone
    public static String Project_owner_line = "";//: Project owner linebyphase
    public static String Project_owner_note = "";//: Project owner note


    public static String pic = "";//: array post with base64
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private CircleImageView profileImage;
    private CardView cardLocation;
    private TextView textEyeCount;
    private RelativeLayout reEyeCount;
    private RudyService rudyService;
    private String countEye;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        rudyService = ApiHelper.getClient();


        savePrefer(this, "quick_projectId", "");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        initView();
        setUserData();
        setOnClick();

        //for show alert count eyes
        cardLocation.setElevation(1);
        reEyeCount.setElevation(10);
        textEyeCount.setElevation(100);

        receiveData();
        if (Utils.isNetworkConnected(MainActivity.this)){
            loadCountEye();
            CheckHaveNewProject();
        }
    }

    private void CheckHaveNewProject(){
        ArrayList<String> fileNameList = new ArrayList<>();
        fileNameList = Utils.searchFileNameDataNewProject();
        if (fileNameList.size() > 0){
            for(String item : fileNameList){
                String jsonData = Utils.readFromFileDataNewProject(MainActivity.this, item);
                Gson gson = new Gson();
                StorageDataOfflineModel storageDataOfflineModel = gson.fromJson(jsonData, StorageDataOfflineModel.class);
                postNewProject(storageDataOfflineModel);
            }
        }
    }


    private Call<String> callNewProject(List<String> _pic, StorageDataOfflineModel _storageDataOffline) {

        String latlang = "";
        if (_storageDataOffline.getDragLat() != null && _storageDataOffline.getDragLng() != null){
            latlang = _storageDataOffline.getDragLat() + "," + _storageDataOffline.getDragLng();
        }

        return rudyService.addNewProject(
                Utils.APP_LANGUAGE,
                _pic,
                user_id,
                "",
                _storageDataOffline.getAddress(),
                latlang,
                "",
                "",
                "0",
                "0",
                "0",
                "0",
                _storageDataOffline.getCtype()+"",
                _storageDataOffline.getCustomer_name(),
                _storageDataOffline.getCustomer_code(),
                _storageDataOffline.getChamp_customer_code(),
                _storageDataOffline.getCustomer_company(),
                _storageDataOffline.getCustomer_phone(),
                "",
                _storageDataOffline.getCustomer_line(),
                _storageDataOffline.getCustomer_tax_no(),
                _storageDataOffline.getCustomer_note(),
                _storageDataOffline.getCtype_owner() == 2 ? _storageDataOffline.getHouse_owner_name():"",
                _storageDataOffline.getCtype_owner() == 2 ? _storageDataOffline.getHouse_owner_phone():"",
                _storageDataOffline.getCtype_owner() == 2 ? _storageDataOffline.getHouse_owner_line():"",
                _storageDataOffline.getCtype_owner() == 2 ? _storageDataOffline.getHouse_owner_note():"",
                _storageDataOffline.getCtype_owner() == 2 ? "":_storageDataOffline.getProject_owner_name(),
                _storageDataOffline.getCtype_owner() == 2 ? "":_storageDataOffline.getProject_owner_phone(),
                _storageDataOffline.getCtype_owner() == 2 ? "":_storageDataOffline.getProject_owner_line(),
                _storageDataOffline.getCtype_owner() == 2 ? "":_storageDataOffline.getProject_owner_note(),
                "");
    }


    @SuppressLint("StaticFieldLeak")
    private void postNewProject(StorageDataOfflineModel _storageDataOffline){
        showLoading(MainActivity.this);
        ArrayList<String> listPic = new ArrayList<>();
        for (int i = 0; i < _storageDataOffline.mArrayList.size(); i++) {
            for (int j = 0; j < _storageDataOffline.mArrayList.get(i).getmValue().size() ; j++) {
                File f = new File(_storageDataOffline.mArrayList.get(i).getmValue().get(j).getName());
                Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                listPic.add("data:image/jpg;base64," + Utils.BitMapToString(d));
            }
        }


        callNewProject(listPic, _storageDataOffline).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(MainActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
                } else {
                    try {
                        JSONObject object = new JSONObject(resp);
                        showToast(MainActivity.this, object.optString("items",resp));
                    } catch (JSONException e) {
                        showToast(MainActivity.this, resp);
                        e.printStackTrace();
                        LogException(resp ,e);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(MainActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callCountEye() {
        return rudyService.countEye(
                Utils.APP_LANGUAGE,
                user_id
        );
    }


    public void loadCountEye() {
        showLoading(this);
        callCountEye().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    CountEyeModel countEyeModel = gson.fromJson(resp, CountEyeModel.class);
                    String Count = countEyeModel.getItems().getCount();
                    if (Double.parseDouble(Count) > 0){
                        countEye = Count;
                        if (Double.parseDouble(Count) >= 100){
                            textEyeCount.setText("99+");
                        }else{
                            textEyeCount.setText(Count);
                        }
                        reEyeCount.setVisibility(View.VISIBLE);
                    }else{
                        countEye = "0";
                        reEyeCount.setVisibility(View.GONE);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(MainActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(MainActivity.this, t.getMessage());
            }
        });
    }

    private void receiveData() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                // Handle text being sent
                handleSendText(intent);
            } else if (type.startsWith("image/")) {
                // Handle single image being sent
                handleSendImage(intent);
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                // Handle multiple images being sent
                handleSendMultipleImages(intent);
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }
    }


    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            if (sharedText.contains("https://maps.google.com")) {
                Log.i(TAG, "maps : " + sharedText.replace("https://maps.google.com", ""));
                String latlng = sharedText.replace("https://maps.google.com/maps?q=", "");
                try {
                    String afterDecode = URLDecoder.decode(latlng, "UTF-8");
                    onReceiveLatlng(afterDecode);
//                    showToast(this,"map location is : "+afterDecode);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    LogException(sharedText, e);
                }
//                showToast(this,"map location is : "+sharedText);
            }
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            return;
            // Update UI to reflect image being shared
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            return;
            // Update UI to reflect multiple images being shared
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    void onReceiveLatlng(String latlng) {
        double latitude = Double.parseDouble((latlng.split(","))[0]);
        double longitude = Double.parseDouble((latlng.split(","))[1]);


        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getResources().getString(R.string.dialog_want_to_create_unit_here))
                .content(getCompleteAddressString(latitude, longitude))
                .positiveText(getResources().getString(R.string.toast_create_unit))
                .negativeText(getResources().getString(R.string.cancel))
                .input(R.string.specify_name_of_unit, 0, false, (dialog, newComment) -> project_name = newComment.toString())
                .inputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                .onPositive((dialog, which) -> {
                    lat_lng = latlng;
                    if (dialog.getInputEditText().getText().toString().isEmpty()) {
                        showToast(MainActivity.this, getString(R.string.specify_name_of_unit));
                    } else {
                        project_name = dialog.getInputEditText().getText().toString();
                        project_address = getCompleteAddressString(latitude, longitude);
//                        showToast(MainActivity.this,address);

                        postNewProjectWithLatLng();
                        dialog.dismiss();
                    }
                })
                .onNegative((dialog, which) -> dialog.dismiss());

        MaterialDialog del_dialog = builder.build();
        del_dialog.show();
    }


    private void setOnClick() {
        addnewdata.setOnClickListener(this);
        setting.setOnClickListener(this);
        bycustomer.setOnClickListener(this);
        bylocation.setOnClickListener(this);
        byphase.setOnClickListener(this);
        quickSale.setOnClickListener(this);
        dashboard.setOnClickListener(this);
        calendar.setOnClickListener(this);
        saleWallet.setOnClickListener(this);
    }


    public void setUserData() {
        try {
            UserPOJO userPOJO = Utils.setEnviroment(this);
            Items userItems = userPOJO.getItems();
            user_id = userItems.getId();
            shopID = userItems.getShopId();
            cusName.setText(userItems.getName());

            if (userItems.getLevel().equalsIgnoreCase("1")) {
                cusType.setText(getResources().getString(R.string.txt_owner));
            } else if (userItems.getLevel().equalsIgnoreCase("2")) {
                cusType.setText(getResources().getString(R.string.txt_manager));
            } else {
                cusType.setText(getResources().getString(R.string.txt_sale));
            }
            if (!userItems.getPic().isEmpty()) {
                Glide.with(this).load(userItems.getPic()).into(profileImage);
            }
        } catch (Exception e) {
            savePrefer(MainActivity.this, PERF_LOGIN, "");
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
            e.printStackTrace();
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, RudyHoverMenuService.class));
    }

    private void handleExitApp() {
        if (doubleBackToExitPressedOnce) {
            SharedPreferences prefs = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat("rudy_dock_position", 0.6889401f);
            editor.putInt("rudy_dock_side", 1);
            editor.putString("rudy_selected_section", "app_state");
            editor.apply();


            super.onBackPressed();
            finish();
            return;
        }
        doubleBackToExitPressedOnce = true;
        showSnackbar("Please click BACK again to exit");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        ((TextView) snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text)).setTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }


    @Override
    public void onResume() {
        Utils.APP_LANGUAGE = Utils.getLang(this);
//        setEnviroment();
//        Utils.savePrefer(this,"quick_projectId",projectDetail.getProjectDetailItems().getId());


        if (Utils.getPrefer(this, "quick_projectId").length() > 0) {
//            String url = getViewDetail(Utils.APP_LANGUAGE, Utils.getPrefer(this, "quick_projectId"));
            getDetail(Utils.getPrefer(this, "quick_projectId"));
        } else {
            AddNewProjectActivity.DragLat = 0.0;
            AddNewProjectActivity.DragLng = 0.0;
            AddNewProjectActivity.bit1 = null;
            AddNewProjectActivity.bit2 = null;
            AddNewProjectActivity.bit3 = null;
            AddNewProjectActivity.bit4 = null;
            AddNewProjectActivity.bit5 = null;
            AddNewProjectActivity.bit6 = null;
            ChooseSkuActivity.projectId = "";
            phaseId = "1";
            cateId = "1";
//            unit_number = "1";
//            floor_number = "2";
//            unit_area = "150";
//            unit_budget = "2.5";
            c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
            c_id_owner = "";
            ctype = 1;
            ctype_owner = 3;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
            cname = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
            ccompany = "Rudy";// บริษัทลูกค้า
            cphone = "";// เบอร์ติดต่อลูกค้า
            cline = "";// line ID ของลูกค้า
            cnote = "";// noteลูกค้า
            cname_owner = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
            ccompany_owner = "Rudy";// บริษัทลูกค้า
            cphone_owner = "";// เบอร์ติดต่อลูกค้า
            cline_owner = "";// line ID ของลูกค้า
            cnote_owner = "";// noteลูกค้า
            other_type = "";
            image64_1 = "";
            image64_2 = "";
            image64_3 = "";
            image64_4 = "";
            image64_5 = "";
            image64_6 = "";
            project_name = "";
            unit_budget_number = 0.0;
            DragLat = 0.0;
            DragLng = 0.0;
            project_address = "";
            savePrefer(this, "quickSale", "0");
            ChooseSkuActivity.projectName = "";
            ChooseSkuActivity.projectId = "";
            DealByPhaseActivity.QUICKSALE = "1";
            MainActivity.quick_projectId = "";
        }



        if (Utils.isNetworkConnected(MainActivity.this)){
            CheckHaveNewProject();
            loadCountEye();
        }

        super.onResume();

    }


    @Override
    public void onBackPressed() {
        handleExitApp();
    }


    @Override
    public void onStart() {
        super.onStart();
        savePrefer(this, "QC", "");
    }

    private void OpenAddNewDataPage(){
        Utils.project_id = "";
        MainActivity.data1 = false;
        MainActivity.data2 = false;
        MainActivity.data3 = false;
        MainActivity.data4 = false;
        MainActivity.data5 = false;
        AddNewProjectActivity.step1 = false;
        AddNewProjectActivity.step2 = false;
        AddNewProjectActivity.step3 = false;
        AddNewProjectActivity.step4 = false;
        AddNewProjectActivity.bit1 = null;
        AddNewProjectActivity.bit2 = null;
        AddNewProjectActivity.bit3 = null;
        AddNewProjectActivity.bit4 = null;
        AddNewProjectActivity.bit5 = null;
        AddNewProjectActivity.bit6 = null;
        AddNewProjectActivity.cname_owner = "";
        AddNewProjectActivity.cline_owner = "";
        AddNewProjectActivity.cphone = "";
        AddNewProjectActivity.cnote_owner = "";
        AddNewProjectActivity.house_owner_name = "";
        AddNewProjectActivity.house_owner_phone = "";
        AddNewProjectActivity.house_owner_line = "";
        AddNewProjectActivity.House_owner_note = "";
        AddNewProjectActivity.customer_name = "";
        AddNewProjectActivity.customer_phone = "";
        AddNewProjectActivity.customer_email = "";
        AddNewProjectActivity.customer_line = "";
        AddNewProjectActivity.customer_tax_no = "";
        AddNewProjectActivity.customer_note = "";
        AddNewProjectActivity.project_name = "";
        AddNewProjectActivity.unit_number = "";
        AddNewProjectActivity.unit_area = "";
        AddNewProjectActivity.unit_budget = "";
        AddNewProjectActivity.floor_number = "";
        AddNewProjectActivity.cateId = "0";
        AddNewProjectActivity.projectDetail = null;
        AddNewProjectActivity.customer_code = "";
        AddNewProjectActivity.champ_customer_code = "";
        AddNewProjectActivity.customer_company = "";
        AddNewProjectActivity.catePosition = 0;
        AddNewProjectActivity.phasePosition = 0;
        AddNewProjectActivity.project_owner_name = "";
        AddNewProjectActivity.project_owner_phone = "";
        AddNewProjectActivity.project_owner_line = "";
        AddNewProjectActivity.project_owner_note = "";

        ProjectDetailActivity.edit = false;
        Intent intent = new Intent(MainActivity.this, AddNewProjectActivity.class);
        intent.putExtra("detailData", "");
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (!Utils.isNetworkConnected(this)){
            if (v == addnewdata){
                OpenAddNewDataPage();
            }else{
                showToast(this, getResources().getString(R.string.alert_text_network_not_connect));
            }
        }else{
            if (v == addnewdata) {
                OpenAddNewDataPage();
            }
            if (v == setting) {

                final PopupWindow popup = new PopupWindow(this);
                View layout = this.getLayoutInflater().inflate(R.layout.popup_content, null);
                popup.setContentView(layout);

                TextView about = layout.findViewById(R.id.about);
                TextView changeLanguage = layout.findViewById(R.id.changeLanguage);
                TextView signout = layout.findViewById(R.id.signout);

                SpannableString content = new SpannableString(getString(R.string.sign_out));
                content.setSpan(new UnderlineSpan(), 0, getString(R.string.sign_out).length(), 0);
                signout.setText(content);

                // Set content width and height
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                // Closes the popup window when touch outside of it - when looses focus
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                // Show anchored to button
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAsDropDown(setting, -100, 0, Gravity.LEFT);

                signout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popup.dismiss();
                        onShowProfile();
                    }
                });
                about.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popup.dismiss();
                        onShowAbout();
                    }
                });

                changeLanguage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popup.dismiss();
                        onShowLanguage();
                    }
                });
            }
            if (v == bycustomer) {
                MainActivity.data1 = false;
                MainActivity.data2 = false;
                MainActivity.data3 = false;
                MainActivity.data4 = false;
                MainActivity.data5 = false;
//            savePrefer(this,"print","");

                savePrefer(this, "print", "");
                savePrefer(this, "quick_projectId", "");
                startActivity(new Intent(this, ByCustomersActivity.class));

            }
            if (v == bylocation) {
                savePrefer(this, "print", "");
                savePrefer(this, "quick_projectId", "");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
                    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_CODE_ASK_PERMISSIONS);
                        return;
                    }
                    Intent intent = new Intent(this, LocationCustomerActivity.class);
                    intent.putExtra("CountEye", countEye);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, LocationCustomerActivity.class);
                    intent.putExtra("CountEye", countEye);
                    startActivity(intent);
                }
            }
            if (v == byphase) {
                savePrefer(this, "print", "");
                savePrefer(this, "quick_projectId", "");
                MainActivity.data1 = false;
                MainActivity.data2 = false;
                MainActivity.data3 = false;
                MainActivity.data4 = false;
                MainActivity.data5 = false;

                startActivity(new Intent(this, ByProjectActivity.class));
            }
            if (v == quickSale) {

                savePrefer(MainActivity.this, "print", "");
                postNewProject();
            }
            if (v == dashboard) {
                startActivity(new Intent(this, DashboardActivity.class));
            }
            if (v == calendar) {
                startActivity(new Intent(this, CalendarActivity.class));
            }
            if (v == saleWallet) {
                startActivity(new Intent(this, SaleWalletActivity.class));
            }
        }
    }

    public void onShowLanguage() {

        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.popup_change_language);

        ArrayList<LanguageModel> languageModels = new ArrayList<>();
        languageModels.add(new LanguageModel(getResources().getDrawable(R.drawable.eng_tag), "English", "en"));
        languageModels.add(new LanguageModel(getResources().getDrawable(R.drawable.thai_tag), "ภาษาไทย", "th"));

        RecyclerView recyclerView = dialog.findViewById(R.id.revChangeLang);
        ChangeLanguageAdapter adapter = new ChangeLanguageAdapter(this, languageModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        dialog.show();

    }

    public void onShowAbout() {
        HoverMotion mHoverMotion = new HoverMotion();
        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.view_about);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * .7);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * .35);
        dialog.getWindow().setLayout(width, height);
        TextView version = dialog.findViewById(R.id.version);
        TextView build = dialog.findViewById(R.id.build);
        ImageView rudy_logo = dialog.findViewById(R.id.rudy_logo);
        mHoverMotion.start(rudy_logo);
        RelativeLayout hideView = dialog.findViewById(R.id.hideView);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
            String versionName = pInfo.versionName;
            int verCode = pInfo.versionCode;

            version.setText(versionName);
            build.setText(verCode + "");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        hideView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHoverMotion.stop();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    public void onShowProfile() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.popup_logout);
        TextView company = dialog.findViewById(R.id.company);
        TextView username = dialog.findViewById(R.id.username);
        TextView name = dialog.findViewById(R.id.name);
        TextView email = dialog.findViewById(R.id.email);
        TextView sale_txt = dialog.findViewById(R.id.sale_txt);

        RelativeLayout logout = dialog.findViewById(R.id.logout);
        company.setText(userPOJO.getItems().getShop());
        username.setText(userPOJO.getItems().getEmployeeId());
        name.setText(userPOJO.getItems().getName());
        email.setText(userPOJO.getItems().getEmail());
        if (userPOJO.getItems().getLevel().equalsIgnoreCase("1")) {
            sale_txt.setText(getResources().getString(R.string.txt_owner));
        } else if (userPOJO.getItems().getLevel().equalsIgnoreCase("2")) {
            sale_txt.setText(getResources().getString(R.string.txt_manager));
        } else {
            sale_txt.setText(getResources().getString(R.string.txt_sale));
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.this)
                        .title(getResources().getString(R.string.dialog_want_to_sign_out))
                        .positiveText(getResources().getString(R.string.agreed))
                        .negativeText(getResources().getString(R.string.cancel))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                savePrefer(MainActivity.this, PERF_LOGIN, "");
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));

                                dialog.dismiss();
                                finish();

                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        });

                MaterialDialog del_dialog = builder.build();
                del_dialog.show();
            }
        });
        dialog.show();
    }


    private Call<String> callPostNewProjectWithLatLng() {
        List<String> listPic = new ArrayList<>();
        return rudyService.addNewProject(
                Utils.APP_LANGUAGE,
                listPic,
                user_id,
                project_name,
                project_address,
                lat_lng,
                project_type_id,
                phase_id,
                units,
                unit_area,
                unit_budget,
                project_stories,
                customer_type+"",
                customer_name,
                "",
                "",
                "",
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note,
                house_owner_name,
                house_owner_phone,
                Project_owner_line,
                Project_owner_note,
                "",
                "",
                "",
                "",
                ""
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void postNewProjectWithLatLng(){
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);

        callPostNewProjectWithLatLng().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
                if (projectDetail.getStatus() == 200) {
                    setCustomTag("Create Project");
                    String nameComm = projectDetail.getItems().getProjectName();
                    showToast(MainActivity.this, getResources().getString(R.string.toast_create_unit) + nameComm + getResources().getString(R.string.succeed));
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(MainActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(MainActivity.this, t.getMessage());
            }
        });

    }

//    @SuppressLint("StaticFieldLeak")
//    private void postNewProjectWithLatLng(final String url) {
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("user_id", user_id)
//                .add("project_name", project_name)
//                .add("project_address", project_address)
//                .add("lat_lng", lat_lng)
//                .add("project_type_id", project_type_id)
//                .add("phase_id", phase_id)
//                .add("units", units)
//                .add("unit_area", unit_area)
//                .add("unit_budget", unit_budget)
//                .add("project_stories", project_stories)
//                .add("customer_type", customer_type + "")
//                .add("customer_name", customer_name)
//                .add("customer_phone", customer_phone)
//                .add("customer_email", customer_email)
//                .add("customer_line", customer_line)
//                .add("customer_tax_no", customer_tax_no)
//                .add("customer_note", customer_note)
//                .add("house_owner_name", house_owner_name)
//                .add("house_owner_phone", house_owner_phone)
//                .add("house_owner_line", house_owner_line)
//                .add("House_owner_note", House_owner_note)
//                .add("Project_owner_name", Project_owner_name)
//                .add("Project_owner_phone", Project_owner_phone)
//                .add("Project_owner_line", Project_owner_line)
//                .add("Project_owner_note", Project_owner_note)
//                .add("project_group_id", "")
//                .add("pic", pic)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                Gson gson = new Gson();
//                ProjectDetail projectDetail = gson.fromJson(string, ProjectDetail.class);
//                if (projectDetail.getStatus() == 200) {
//                    setCustomTag("Create Project");
//                    String nameComm = projectDetail.getItems().getProjectName();
//                    showToast(MainActivity.this, getResources().getString(R.string.toast_create_unit) + nameComm + getResources().getString(R.string.succeed));
//                } else {
//                    APIError error = gson.fromJson(string, APIError.class);
//                    showToast(MainActivity.this, error.getItems());
//                }
//            }
//        }.execute();
//    }


    @SuppressLint("StaticFieldLeak")
    private void postNewProject(){
        lat_lng = "";
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);

        callPostNewProjectWithLatLng().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);

                if (projectDetail.getStatus() == 200) {
                    setCustomTag("Create Project");


                    Utils.project_id = projectDetail.getItems().getId();
                    MainActivity.quick_projectId = projectDetail.getItems().getId();

                    savePrefer(MainActivity.this, "quick_projectId", projectDetail.getItems().getId());


                    DealByPhaseActivity.QUICKSALE = "1";
                    savePrefer(MainActivity.this, "quickSale", "1");
                    savePrefer(MainActivity.this, "cart", "");
                    String nameComm = projectDetail.getItems().getProjectName();

                    TransactionDetailActivity.detailItem = projectDetail.getItems();

                    Utils.pdfName = projectDetail.getItems().getProjectName();
                    Intent intent = new Intent(MainActivity.this, DealByPhaseActivity.class);
                    intent.putExtra("phase", projectDetail.getItems().getPhaseId());
                    intent.putExtra("projectId", projectDetail.getItems().getId());
                    intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
                    intent.putExtra("transaction_id", "");
                    showToast(MainActivity.this, getResources().getString(R.string.toast_offer_sale_unit) + nameComm);
                    startActivity(intent);


                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(MainActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(MainActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void postNewProject(final String url) {
//        lat_lng = "";
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("user_id", user_id)
//                .add("project_name", project_name)
//                .add("project_address", project_address)
//                .add("lat_lng", lat_lng)
//                .add("project_type_id", project_type_id)
//                .add("phase_id", phase_id)
//                .add("units", units)
//                .add("unit_area", unit_area)
//                .add("unit_budget", unit_budget)
//                .add("project_stories", project_stories)
//                .add("customer_type", customer_type + "")
//                .add("customer_name", customer_name)
//                .add("customer_phone", customer_phone)
//                .add("customer_email", customer_email)
//                .add("customer_line", customer_line)
//                .add("customer_tax_no", customer_tax_no)
//                .add("customer_note", customer_note)
//                .add("house_owner_name", house_owner_name)
//                .add("house_owner_phone", house_owner_phone)
//                .add("house_owner_line", house_owner_line)
//                .add("House_owner_note", House_owner_note)
//                .add("Project_owner_name", Project_owner_name)
//                .add("Project_owner_phone", Project_owner_phone)
//                .add("Project_owner_line", Project_owner_line)
//                .add("Project_owner_note", Project_owner_note)
//                .add("project_group_id", "")
//                .add("pic", pic)
//
//                .build();
//
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                Gson gson = new Gson();
//                ProjectDetail projectDetail = gson.fromJson(string, ProjectDetail.class);
//
//                if (projectDetail.getStatus() == 200) {
//                    setCustomTag("Create Project");
//
//
//                    Utils.project_id = projectDetail.getItems().getId();
//                    MainActivity.quick_projectId = projectDetail.getItems().getId();
//
//                    savePrefer(MainActivity.this, "quick_projectId", projectDetail.getItems().getId());
//
//
//                    DealByPhaseActivity.QUICKSALE = "1";
//                    savePrefer(MainActivity.this, "quickSale", "1");
//                    savePrefer(MainActivity.this, "cart", "");
//                    String nameComm = projectDetail.getItems().getProjectName();
//
//                    TransactionDetailActivity.detailItem = projectDetail.getItems();
//
//                    Utils.pdfName = projectDetail.getItems().getProjectName();
//                    Intent intent = new Intent(MainActivity.this, DealByPhaseActivity.class);
//                    intent.putExtra("phase", projectDetail.getItems().getPhaseId());
//                    intent.putExtra("projectId", projectDetail.getItems().getId());
//                    intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
//                    intent.putExtra("transaction_id", "");
//                    showToast(MainActivity.this, getResources().getString(R.string.toast_offer_sale_unit) + nameComm);
//                    startActivity(intent);
//
//
//                } else {
//                    APIError error = gson.fromJson(string, APIError.class);
//                    showToast(MainActivity.this, error.getItems());
//                }
//
//
//            }
//        }.execute();
//    }

    private Call<String> callProjectDetail(String _pId) {
        return rudyService.projectDetail(
                Utils.APP_LANGUAGE,
                _pId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getDetail(String _pid) {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);

        callProjectDetail(_pid).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
                if (projectDetail.getStatus() == 200) {
                    if (projectDetail.getItems().getPr().size() == 0) {
                        DeleteProject(Utils.getPrefer(MainActivity.this, "quick_projectId"));
                    } else {
                        savePrefer(MainActivity.this, "quick_projectId", "");
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(MainActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(MainActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void getDetail(final String url) {
//        Log.i("URL", " : " + url);
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                Gson gson = new Gson();
//                ProjectDetail projectDetail = gson.fromJson(string, ProjectDetail.class);
//                if (projectDetail.getStatus() == 200) {
//                    if (projectDetail.getItems().getPr().size() == 0) {
//                        DeleteProject(getDeleteProject(Utils.APP_LANGUAGE, user_id, Utils.getPrefer(MainActivity.this, "quick_projectId")));
//                    } else {
//                        savePrefer(MainActivity.this, "quick_projectId", "");
//                    }
//                }
//            }
//        }.execute();
//    }


    private Call<String> callDeleteProject(String _pid) {
        return rudyService.deleteProject(
                Utils.APP_LANGUAGE,
                user_id,
                _pid
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void DeleteProject(String _pid) {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, this);

        callDeleteProject(_pid).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    AddNewProjectActivity.DragLat = 0.0;
                    AddNewProjectActivity.DragLng = 0.0;
                    AddNewProjectActivity.bit1 = null;
                    AddNewProjectActivity.bit2 = null;
                    AddNewProjectActivity.bit3 = null;
                    AddNewProjectActivity.bit4 = null;
                    AddNewProjectActivity.bit5 = null;
                    AddNewProjectActivity.bit6 = null;
                    ChooseSkuActivity.projectId = "";
                    AddNewProjectActivity.catePosition = 0;
                    AddNewProjectActivity.phasePosition = 0;

                    phaseId = "1";
                    cateId = "1";
                    unit_number = "1";
                    floor_number = "2";
                    unit_area = "150";
                    unit_budget = "2.5";
                    c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
                    c_id_owner = "";
                    ctype = 1;
                    ctype_owner = 3;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
                    cname = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
                    ccompany = "Rudy";// บริษัทลูกค้า
                    cphone = "";// เบอร์ติดต่อลูกค้า
                    cline = "";// line ID ของลูกค้า
                    cnote = "";// noteลูกค้า
                    cname_owner = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
                    ccompany_owner = "Rudy";// บริษัทลูกค้า
                    cphone_owner = "";// เบอร์ติดต่อลูกค้า
                    cline_owner = "";// line ID ของลูกค้า
                    cnote_owner = "";// noteลูกค้า
                    other_type = "";
                    image64_1 = "";
                    image64_2 = "";
                    image64_3 = "";
                    image64_4 = "";
                    image64_5 = "";
                    image64_6 = "";
                    project_name = "";
                    unit_budget_number = 0.0;
                    DragLat = 0.0;
                    DragLng = 0.0;
                    project_address = "";
                    savePrefer(MainActivity.this, "quickSale", "0");
                    ChooseSkuActivity.projectName = "";
                    ChooseSkuActivity.projectId = "";
                    DealByPhaseActivity.QUICKSALE = "1";
                    MainActivity.quick_projectId = "";
                    savePrefer(MainActivity.this, "quick_projectId", "");
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(MainActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(MainActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void DeleteProject(final String url) {
//        Log.i("URL", " : " + url);
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if (string.contains("200")) {
//
//                    AddNewProjectActivity.DragLat = 0.0;
//                    AddNewProjectActivity.DragLng = 0.0;
//                    AddNewProjectActivity.bit1 = null;
//                    AddNewProjectActivity.bit2 = null;
//                    AddNewProjectActivity.bit3 = null;
//                    AddNewProjectActivity.bit4 = null;
//                    AddNewProjectActivity.bit5 = null;
//                    AddNewProjectActivity.bit6 = null;
//                    ChooseSkuActivity.projectId = "";
//                    AddNewProjectActivity.catePosition = 0;
//                    AddNewProjectActivity.phasePosition = 0;
//
//                    phaseId = "1";
//                    cateId = "1";
//                    unit_number = "1";
//                    floor_number = "2";
//                    unit_area = "150";
//                    unit_budget = "2.5";
//                    c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
//                    c_id_owner = "";
//                    ctype = 1;
//                    ctype_owner = 3;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
//                    cname = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
//                    ccompany = "Rudy";// บริษัทลูกค้า
//                    cphone = "";// เบอร์ติดต่อลูกค้า
//                    cline = "";// line ID ของลูกค้า
//                    cnote = "";// noteลูกค้า
//                    cname_owner = "QuickSale";// ชื่อ-นามสกุล/ ชื่อเล่น*
//                    ccompany_owner = "Rudy";// บริษัทลูกค้า
//                    cphone_owner = "";// เบอร์ติดต่อลูกค้า
//                    cline_owner = "";// line ID ของลูกค้า
//                    cnote_owner = "";// noteลูกค้า
//                    other_type = "";
//                    image64_1 = "";
//                    image64_2 = "";
//                    image64_3 = "";
//                    image64_4 = "";
//                    image64_5 = "";
//                    image64_6 = "";
//                    project_name = "";
//                    unit_budget_number = 0.0;
//                    DragLat = 0.0;
//                    DragLng = 0.0;
//                    project_address = "";
//                    savePrefer(MainActivity.this, "quickSale", "0");
//                    ChooseSkuActivity.projectName = "";
//                    ChooseSkuActivity.projectId = "";
//                    DealByPhaseActivity.QUICKSALE = "1";
//                    MainActivity.quick_projectId = "";
//                    savePrefer(MainActivity.this, "quick_projectId", "");
//                }
//            }
//        }.execute();
//    }

    public void hideKeyboard() {

    }

    private void initView() {
        profileImage = findViewById(R.id.profile_image);
        cardLocation = (CardView) findViewById(R.id.cardLocation);
        textEyeCount = (TextView) findViewById(R.id.textEyeCount);
        reEyeCount = (RelativeLayout) findViewById(R.id.reEyeCount);
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.rootView);
//        fragment.onActivityResult(requestCode, resultCode, data);
//    }
}
