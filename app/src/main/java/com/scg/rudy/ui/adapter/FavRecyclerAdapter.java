package com.scg.rudy.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.favorite.ListItem;
import com.scg.rudy.ui.sku_confirm.SkuConfirmActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class FavRecyclerAdapter extends RecyclerView.Adapter<FavRecyclerAdapter.ProjectViewHolder> {

    private  ArrayList<ListItem>  cartModelArrayLis ;
    private Context context;
    public FavRecyclerAdapter(Context context, ArrayList<ListItem> cartModelArrayLis) {
        this.cartModelArrayLis = cartModelArrayLis;
        this.context = context;
    }

    @Override
    public FavRecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fav_item, parent, false);
        return new FavRecyclerAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FavRecyclerAdapter.ProjectViewHolder holder, int position) {
        final ListItem prCodeDetailModel = cartModelArrayLis.get(position);
////        ,prCodeDetailModel.getId()
        holder.textCurrency.setText(Utils.currencyForShow);
        holder.name.setText( prCodeDetailModel.getName());
        holder.price.setText(Utils.moneyFormat(prCodeDetailModel.getPrice().toString().replace("null","1")));
        holder.add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"COMMING SOON",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context,  SkuConfirmActivity.class);
                intent.putExtra("skuId",prCodeDetailModel.getId());
                intent.putExtra("total","99");
                intent.putExtra("price",prCodeDetailModel.getPrice());
                intent.putExtra("name",prCodeDetailModel.getName());
                intent.putExtra("unit_code",prCodeDetailModel.getUnit());
                intent.putExtra("stock",prCodeDetailModel.getStock());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null)
            return 0;
        return cartModelArrayLis.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price ;
        TextView textCurrency ;
        RelativeLayout add_cart;

        public ProjectViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name2);
            price = view.findViewById(R.id.price4);
            add_cart  = view.findViewById(R.id.add_cart);
            textCurrency = view.findViewById(R.id.price5);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


}