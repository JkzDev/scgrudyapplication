package com.scg.rudy.ui.sku_confirm;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.answers.AddToCartEvent;
import com.crashlytics.android.answers.Answers;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.transaction_detail.Transaction;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.favorite.FavoriteDetailActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.sku_confirm.Adapter.unitCodeTypeSpinner;
import com.scg.rudy.ui.sku_confirm.Models.ItemDetailModel;
import com.scg.rudy.ui.sku_confirm.Models.UnitList;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getAddBasket;
import static com.scg.rudy.data.remote.ApiEndPoint.getAddFav;
import static com.scg.rudy.data.remote.ApiEndPoint.getDeleteBasket;
import static com.scg.rudy.data.remote.ApiEndPoint.getDeleteFav;
import static com.scg.rudy.data.remote.ApiEndPoint.getGetTranDetail;
import static com.scg.rudy.data.remote.ApiEndPoint.getSkuDetail;
import static com.scg.rudy.utils.Utils.isEmpty;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

public class SkuConfirmActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {
    private ImageView clsBtn;
    private RelativeLayout case1;
    private TextView txtCase1;
    private RelativeLayout givePrice;
    private TextView txtCase2;
    private RelativeLayout case3;
    private TextView txtCase3;
    private EditText total;
    private EditText price;
    private RelativeLayout setDayLayout;
    private RelativeLayout allDay;
    private TextView txtAllday;
    private RelativeLayout morning;
    private TextView txtMorning;
    private RelativeLayout afternoon;
    private TextView txtNoon;
    private RelativeLayout typeNormal;
    private TextView txtNormal;
    private RelativeLayout typeQuick;
    private TextView txtQuick;
    private EditText note;
    private RelativeLayout save;
    private RelativeLayout dayExplane;
    private TextView titleName;
    boolean showDayLayout = false;
    private TextView txtCalendar;
    private RelativeLayout calendar;
    private Bundle extras;
    private String priceExtra = "", totalExtra = "", nameExtra = "", skuId = "", type = "";
    private String Cdate, phase, deliveryType;
    private boolean isFav = false;
    private String priceT = "";
    private String totalT = "";
    public static String basketString = "";
    private ScrollView scroll;
    private TextView ttSum;
    private LinearLayout tabHide;
    public static String sub_class_name;
    public ImageView nor_ico;
    public ImageView fst_ico;
    public Button cancel;
    private RelativeLayout recmd_price;
    private TextView price1;
    private TextView price2;
    private TextView price3;
    private Transaction transactionData;
    private String subClass = "";
    private String stock = "1";
    private String user_id = "";
    private TextView textCurrency;
    private TextView textCurrency2;
    private Toolbar toolbar;
    private LinearLayout navbar;
    private LinearLayout linearLayout8;
    private LinearLayout linearLayout9;
    private RelativeLayout tab1Layout;
    private LinearLayout remarkLayout;
    private LinearLayout calendarLayout;
    private CardView dataView;
    private ImageView imageView2;
    private ImageView imageMore;
    private ImageView norIco;
    private ImageView fstIco;
    private TextView textView4;
    private RelativeLayout priceLayout;
    private Spinner unitCodeSpiner;
    private RelativeLayout recmdPrice;
    private ImageView imageView21;
    private LinearLayout tab2Layout;
    private LinearLayout tab3Layout;
    private TextView textView8;
    private TextView textVolume;
    private List<UnitList> listUnitCode;
    private ArrayList<String> listUnitCodeShow;
    private String unit_id;
    private TextView textVolumeDiscount;
    private EditText totalDiscount;
    private TextView sumAfterDiscount;
    private TextView textCurrency3;
    private ImageView imgSpiner;
    private ImageView imgSpinerDiscount;
    private Spinner discountSpiner;
    private String discountType = "1";
    private RudyService rudyService;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sku_confirm);
        rudyService = ApiHelper.getClient();
        initView();
        setOnclick();
        case1.performClick();
        allDay.performClick();
        typeNormal.performClick();
        setTextCurrentDate();
        phase = "1";
        deliveryType = "1";
        type = "1";
//        priceExtra = "";
//        totalExtra = "";
//        nameExtra = "";
//        skuId  = "";
//        subClass  = "";
//        stock  = "";

        if (extras != null) {
            priceExtra = extras.getString("price", "0").replace(",", "");
            totalExtra = extras.getString("total", "0").replace(",", "");
            nameExtra = extras.getString("name", "").replace(",", "");
            skuId = extras.getString("skuId", "").replace(",", "");
            subClass = extras.getString("subClass", "").replace(",", "");
            stock = extras.getString("stock", "1").replace(",", "");
        }

        titleName.setText(nameExtra);
        total.setHint(Utils.moneyFormat(totalExtra));
        if(Double.parseDouble(totalExtra) > 0){
            total.setText(totalExtra);
        }else{
            total.setText("");
        }
        price.setHint(Utils.moneyFormat(priceExtra));
        price.setText(Utils.moneyFormat(priceExtra));
        ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(totalExtra) * (Double.parseDouble(priceExtra)))));

        Answers.getInstance().logAddToCart(new AddToCartEvent()
                .putItemPrice(BigDecimal.valueOf(Double.parseDouble(priceExtra)))
                .putCustomAttribute("Amount", BigDecimal.valueOf(0))
                .putCustomAttribute("Total", BigDecimal.valueOf(1 * Double.parseDouble(priceExtra)))
                .putCurrency(Currency.getInstance("THB"))
                .putItemName(titleName.getText().toString())
                .putItemType(subClass)
                .putItemId(skuId));

        UserPOJO userPOJO = Utils.setEnviroment(this);
        Bundle bundle = new Bundle();
        bundle.putString("email", userPOJO.getItems().getEmail());
        bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
        bundle.putString("id", userPOJO.getItems().getId());
        bundle.putString("level", userPOJO.getItems().getLevel());
        bundle.putString("name", userPOJO.getItems().getName());
        bundle.putString("shop", userPOJO.getItems().getShop());
        bundle.putString("shop_id", userPOJO.getItems().getShopId());
        bundle.putString("Price", priceExtra);
        bundle.putString("Amount", totalExtra);
        bundle.putString("Total", ttSum.getText().toString());
        bundle.putString("Currency", "THB");
        bundle.putString("Name", nameExtra);
        bundle.putString("Type", subClass);
        bundle.putString("Id", skuId);
        mFirebaseAnalytics.logEvent("AddToCart", bundle);


//        getData(getSkuDetail(Utils.APP_LANGUAGE, skuId));
        getData();

        total.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                total.setTypeface(_font);
                if (total.getText().toString().length() == 1 && total.getText().toString().equalsIgnoreCase(".")){
                    total.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (isEmpty(total)) {

                    ttSum.setText("0");
                    sumAfterDiscount.setText("0");

//                    if (totalDiscount.getText().toString().matches("")) {
//                        ttSum.setText(Utils.moneyFormat("" + (0 * 1)));
//                    } else {
//                        ttSum.setText(Utils.moneyFormat("" + (0 * (Double.parseDouble(sumAfterDiscount.getText().toString().replace(",", ""))))));
//                    }
                } else {
                    if (totalDiscount.getText().toString().matches("")) {
                        ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))))));
                        sumAfterDiscount.setText("0");
                    } else {
                        if (discountType.equalsIgnoreCase("1")){
                            sumAfterDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) * (Double.parseDouble(totalDiscount.getText().toString()) / 100))));
                            ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", "")))) * (Double.parseDouble(totalDiscount.getText().toString()) / 100)))));
                        }else{
                            sumAfterDiscount.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", "")))) - (Double.parseDouble(totalDiscount.getText().toString()))))));
                            ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - (Double.parseDouble(totalDiscount.getText().toString())))));
                        }
                    }
                }
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                price.setTypeface(_font);
                recmd_price.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable s) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recmd_price.setVisibility(View.GONE);
                    }
                }, 3 * 1000);

                if (isEmpty(price)) {
                    if (isEmpty(total)) {
                        ttSum.setText(Utils.moneyFormat("" + (0 * 1)));
                    } else {
                        ttSum.setText(Utils.moneyFormat("" + (0 * (Double.parseDouble(total.getText().toString().replace(",", ""))))));
                    }
                } else {
                    if (isEmpty(total)) {
                        ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(price.getText().toString().replace(",", "")) * 0)));
                    } else {
                        ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(price.getText().toString().replace(",", "")) * (Double.parseDouble(total.getText().toString().replace(",", ""))))));
                    }
                }
            }
        });

        textCurrency.setText(Utils.currencyForShow);
        textCurrency2.setText(Utils.currencyForShow);
        textCurrency3.setText(Utils.currencyForShow);
        textVolumeDiscount.setText(getResources().getString(R.string.discount));
        sumAfterDiscount.setText("0");
        totalDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (totalDiscount.getText().toString().length() == 1 && totalDiscount.getText().toString().equalsIgnoreCase(".")){
                    totalDiscount.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                //ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(totalExtra) * (Double.parseDouble(priceExtra)))));
                if (discountType.equalsIgnoreCase("1")){
                    if (isEmpty(total)) {
                        ttSum.setText("0");
                        sumAfterDiscount.setText("0");
                    } else {
                        if (!totalDiscount.getText().toString().matches("")) {
                            sumAfterDiscount.setText(Utils.moneyFormat("" + ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", "")))) * (Double.parseDouble(totalDiscount.getText().toString().replace(",", ""))/100))));
                            ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))) - ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", "")))) * (Double.parseDouble(totalDiscount.getText().toString().replace(",", ""))/100)))));

                        } else {
                            sumAfterDiscount.setText("0");
                            ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))))));
                        }
                    }

                }else {
                    if (isEmpty(total)) {
                        ttSum.setText("0");
                        sumAfterDiscount.setText("0");
                    } else {
                        if (!totalDiscount.getText().toString().matches("")) {
                            sumAfterDiscount.setText(Utils.moneyFormat("" + totalDiscount.getText().toString()));
                            ttSum.setText(Utils.moneyFormat("" + ((Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", "")))) - (Double.parseDouble(sumAfterDiscount.getText().toString().replace(",", ""))))));
                        } else {
                            sumAfterDiscount.setText("0");
                            ttSum.setText(Utils.moneyFormat("" + (Double.parseDouble(total.getText().toString().replace(",", "")) * (Double.parseDouble(price.getText().toString().replace(",", ""))))));
                        }
                    }
                }
            }
        });
    }

    private Call<String> callAddBasket(String _productid, String _qty, String _price, String _user_id, String _note, String _unit_id, String _discount, String _discount_type) {
        return rudyService.addBasket(
                ApiEndPoint.LANG,
                project_id,
                _productid,
                _qty,
                _price,
                _user_id,
                _note,
                _unit_id,
                _discount,
                _discount_type
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void AddBasketAPI(){
        showLoading(SkuConfirmActivity.this);
        callAddBasket(skuId, totalT, priceT, user_id, note.getText().toString(), unit_id, totalDiscount.getText().toString(), discountType).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    priceT = String.valueOf(priceT);
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONArray itemArray = object.optJSONArray("items");
                        String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");


                        TransactionDetailActivity.transaction_id = transaction_id;
                        DealByPhaseActivity.transaction_id = transaction_id;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(resp, e);
                    }


                    UserPOJO userPOJO = Utils.setEnviroment(SkuConfirmActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putString("email", userPOJO.getItems().getEmail());
                    bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
                    bundle.putString("id", userPOJO.getItems().getId());
                    bundle.putString("level", userPOJO.getItems().getLevel());
                    bundle.putString("name", userPOJO.getItems().getName());
                    bundle.putString("shop", userPOJO.getItems().getShop());
                    bundle.putString("shop_id", userPOJO.getItems().getShopId());
                    bundle.putString("Price", price.getText().toString());
                    bundle.putString("Amount", total.getText().toString());
                    bundle.putString("Total", ttSum.getText().toString());
                    bundle.putString("Currency", "THB");
                    bundle.putString("Name", nameExtra);
                    bundle.putString("Type", subClass);
                    bundle.putString("Id", skuId);
                    mFirebaseAnalytics.logEvent("StartCheckout", bundle);


                    Map<String, Object> props = new HashMap<String, Object>();
                    props.put("email", userPOJO.getItems().getEmail());
                    props.put("employee_id", userPOJO.getItems().getEmployeeId());
                    props.put("id", userPOJO.getItems().getId());
                    props.put("level", userPOJO.getItems().getLevel());
                    props.put("name", userPOJO.getItems().getName());
                    props.put("shop", userPOJO.getItems().getShop());
                    props.put("shop_id", userPOJO.getItems().getShopId());
                    props.put("Price", price.getText().toString());
                    props.put("Amount", total.getText().toString());
                    props.put("Total", ttSum.getText().toString());
                    props.put("Currency", "THB");
                    props.put("Name", nameExtra);
                    props.put("Type", subClass);
                    props.put("Id", skuId);
                    //Appsee.addEvent("Add to cart", props);


                    JSONObject object = new JSONObject();
                    try {
                        object.put("email", userPOJO.getItems().getEmail());
                        object.put("employee_id", userPOJO.getItems().getEmployeeId());
                        object.put("id", userPOJO.getItems().getId());
                        object.put("level", userPOJO.getItems().getLevel());
                        object.put("name", userPOJO.getItems().getName());
                        object.put("shop", userPOJO.getItems().getShop());
                        object.put("shop_id", userPOJO.getItems().getShopId());
                        object.put("Price", price.getText().toString());
                        object.put("Amount", total.getText().toString());
                        object.put("Total", ttSum.getText().toString());
                        object.put("Currency", "THB");
                        object.put("Name", nameExtra);
                        object.put("Type", subClass);
                        object.put("Id", skuId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(resp, e);
                    }
                    mixpanel.track("Add to cart", object);


                    ChooseSkuActivity.addItem = 1;
                    TransactionDetailActivity.productId = skuId;
                    TransactionDetailActivity.isDelete = true;
                    FavoriteDetailActivity.productId = skuId;
                    FavoriteDetailActivity.isDelete = true;
                    MainActivity.data5 = true;
                    showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_add_product) + titleName.getText() + getResources().getString(R.string.dialog_add_to_cart_success));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(SkuConfirmActivity.this, t.getMessage());
            }
        });
    }



    private void checkCaseBeforeAddBasket(){
        boolean isAddBudget = false;
        if (transactionData != null) {
            if (transactionData.getItems().getPr().size() > 0) {

                for (int i = 0; i < transactionData.getItems().getPr().size(); i++) {
                    if (transactionData.getItems().getPr().get(i).getProductId().equalsIgnoreCase(skuId)) {
                        isAddBudget = true;
                    }
                }
                if (isAddBudget) {
                    showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_have_product) + titleName.getText() + getResources().getString(R.string.dialog_alrady_in_cart));
                } else {
                    AddBasketAPI();
                }

            } else {
                AddBasketAPI();
            }
        } else {
            AddBasketAPI();
        }
    }


//    @SuppressLint("StaticFieldLeak")
//    private void AddBasket(final String url) {
//        boolean isAddBudget = false;
//        if (transactionData != null) {
//            if (transactionData.getItems().getPr().size() > 0) {
//
//                for (int i = 0; i < transactionData.getItems().getPr().size(); i++) {
//                    if (transactionData.getItems().getPr().get(i).getProductId().equalsIgnoreCase(skuId)) {
//                        isAddBudget = true;
//                    }
//                }
//                if (isAddBudget) {
//                    showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_have_product) + titleName.getText() + getResources().getString(R.string.dialog_alrady_in_cart));
//                } else {
//                    showLoading(this);
//                    final RequestBody requestBody = new FormBody.Builder()
//                            .add("product_id", skuId)
//                            .add("qty", totalT)
//                            .add("price", priceT)
//                            .add("user_id", user_id)
//                            .add("note", note.getText().toString())
//                            .add("unit_id", unit_id)
//                            .add("discount", totalDiscount.getText().toString())
//                            .add("discount_type", discountType)
//                            .build();
////        requestBody.toString();
//                    new AsyncTask<Void, Void, String>() {
//                        @Override
//                        protected String doInBackground(Void... voids) {
//                            String response = "error";
//                            response = Utils.postData(url, requestBody);
//                            return response;
//                        }
//
//                        @Override
//                        protected void onPostExecute(String string) {
//                            super.onPostExecute(string);
//                            hideLoading();
//                            if (string.contains("200")) {
//                                priceT = String.valueOf(priceT);
//                                try {
//                                    JSONObject object = new JSONObject(string);
//                                    JSONArray itemArray = object.optJSONArray("items");
//                                    String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");
//
//
//                                    TransactionDetailActivity.transaction_id = transaction_id;
//                                    DealByPhaseActivity.transaction_id = transaction_id;
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                    LogException(url, e);
//                                }
////                    Answers.getInstance().logStartCheckout(new StartCheckoutEvent()
////                            .putTotalPrice(BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("Amount",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))))
////                            .putCustomAttribute("Total",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("ItemName",titleName.getText().toString())
////                            .putCustomAttribute("ItemType",SkuConfirmActivity.sub_class_name)
////                            .putCustomAttribute("ItemId",skuId));
//
//                                UserPOJO userPOJO = Utils.setEnviroment(SkuConfirmActivity.this);
//                                Bundle bundle = new Bundle();
//                                bundle.putString("email", userPOJO.getItems().getEmail());
//                                bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
//                                bundle.putString("id", userPOJO.getItems().getId());
//                                bundle.putString("level", userPOJO.getItems().getLevel());
//                                bundle.putString("name", userPOJO.getItems().getName());
//                                bundle.putString("shop", userPOJO.getItems().getShop());
//                                bundle.putString("shop_id", userPOJO.getItems().getShopId());
//                                bundle.putString("Price", price.getText().toString());
//                                bundle.putString("Amount", total.getText().toString());
//                                bundle.putString("Total", ttSum.getText().toString());
//                                bundle.putString("Currency", "THB");
//                                bundle.putString("Name", nameExtra);
//                                bundle.putString("Type", subClass);
//                                bundle.putString("Id", skuId);
//                                mFirebaseAnalytics.logEvent("StartCheckout", bundle);
//
//
//                                Map<String, Object> props = new HashMap<String, Object>();
//                                props.put("email", userPOJO.getItems().getEmail());
//                                props.put("employee_id", userPOJO.getItems().getEmployeeId());
//                                props.put("id", userPOJO.getItems().getId());
//                                props.put("level", userPOJO.getItems().getLevel());
//                                props.put("name", userPOJO.getItems().getName());
//                                props.put("shop", userPOJO.getItems().getShop());
//                                props.put("shop_id", userPOJO.getItems().getShopId());
//                                props.put("Price", price.getText().toString());
//                                props.put("Amount", total.getText().toString());
//                                props.put("Total", ttSum.getText().toString());
//                                props.put("Currency", "THB");
//                                props.put("Name", nameExtra);
//                                props.put("Type", subClass);
//                                props.put("Id", skuId);
//                                //Appsee.addEvent("Add to cart", props);
//
//
//                                JSONObject object = new JSONObject();
//                                try {
//                                    object.put("email", userPOJO.getItems().getEmail());
//                                    object.put("employee_id", userPOJO.getItems().getEmployeeId());
//                                    object.put("id", userPOJO.getItems().getId());
//                                    object.put("level", userPOJO.getItems().getLevel());
//                                    object.put("name", userPOJO.getItems().getName());
//                                    object.put("shop", userPOJO.getItems().getShop());
//                                    object.put("shop_id", userPOJO.getItems().getShopId());
//                                    object.put("Price", price.getText().toString());
//                                    object.put("Amount", total.getText().toString());
//                                    object.put("Total", ttSum.getText().toString());
//                                    object.put("Currency", "THB");
//                                    object.put("Name", nameExtra);
//                                    object.put("Type", subClass);
//                                    object.put("Id", skuId);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                    LogException(url, e);
//                                }
//                                mixpanel.track("Add to cart", object);
//
//
//                                ChooseSkuActivity.addItem = 1;
//                                TransactionDetailActivity.productId = skuId;
//                                TransactionDetailActivity.isDelete = true;
//                                FavoriteDetailActivity.productId = skuId;
//                                FavoriteDetailActivity.isDelete = true;
//                                MainActivity.data5 = true;
//                                showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_add_product) + titleName.getText() + getResources().getString(R.string.dialog_add_to_cart_success));
//                                finish();
//                            }
//
//                        }
//                    }.execute();
//                }
//
//            } else {
//                showLoading(this);
//                final RequestBody requestBody = new FormBody.Builder()
//                        .add("product_id", skuId)
//                        .add("qty", totalT)
//                        .add("price", priceT)
//                        .add("user_id", user_id)
//                        .add("note", note.getText().toString())
//                        .add("unit_id", unit_id)
//                        .add("discount", totalDiscount.getText().toString())
//                        .add("discount_type", discountType)
//                        .build();
////        requestBody.toString();
//                new AsyncTask<Void, Void, String>() {
//                    @Override
//                    protected String doInBackground(Void... voids) {
//                        String response = "error";
//                        response = Utils.postData(url, requestBody);
//                        return response;
//                    }
//
//                    @Override
//                    protected void onPostExecute(String string) {
//                        super.onPostExecute(string);
//                        hideLoading();
//                        if (string.contains("200")) {
//                            priceT = String.valueOf(priceT);
//                            try {
//                                JSONObject object = new JSONObject(string);
//                                JSONArray itemArray = object.optJSONArray("items");
//                                String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");
//
//
//                                TransactionDetailActivity.transaction_id = transaction_id;
//                                DealByPhaseActivity.transaction_id = transaction_id;
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                                LogException(url, e);
//                            }
////                    Answers.getInstance().logStartCheckout(new StartCheckoutEvent()
////                            .putTotalPrice(BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("Amount",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))))
////                            .putCustomAttribute("Total",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("ItemName",titleName.getText().toString())
////                            .putCustomAttribute("ItemType",SkuConfirmActivity.sub_class_name)
////                            .putCustomAttribute("ItemId",skuId));
//                            ChooseSkuActivity.addItem = 1;
//
//
//                            TransactionDetailActivity.productId = skuId;
//                            TransactionDetailActivity.isDelete = true;
//
//                            FavoriteDetailActivity.productId = skuId;
//                            FavoriteDetailActivity.isDelete = true;
//                            MainActivity.data5 = true;
//
////                            if(Utils.getPrefer(SkuConfirmActivity.this,"QC").length()>0){
////                                AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
////                                AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
////                                AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
////                            }
//
//                            showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_add_product) + titleName.getText() + getResources().getString(R.string.dialog_add_to_cart_success));
//                            finish();
//                        }
//
//                    }
//                }.execute();
//            }
//        } else {
//            showLoading(this);
//            final RequestBody requestBody = new FormBody.Builder()
//                    .add("product_id", skuId)
//                    .add("qty", totalT)
//                    .add("price", priceT)
//                    .add("user_id", user_id)
//                    .add("note", note.getText().toString())
//                    .add("unit_id", unit_id)
//                    .add("discount", totalDiscount.getText().toString())
//                    .add("discount_type", discountType)
//
//                    .build();
////        requestBody.toString();
//            new AsyncTask<Void, Void, String>() {
//                @Override
//                protected String doInBackground(Void... voids) {
//                    String response = "error";
//                    response = Utils.postData(url, requestBody);
//                    return response;
//                }
//
//                @Override
//                protected void onPostExecute(String string) {
//                    super.onPostExecute(string);
//                    hideLoading();
//                    if (string.contains("200")) {
//                        priceT = String.valueOf(priceT);
//                        try {
//                            JSONObject object = new JSONObject(string);
//                            JSONArray itemArray = object.optJSONArray("items");
//                            String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");
//
//
//                            TransactionDetailActivity.transaction_id = transaction_id;
//                            DealByPhaseActivity.transaction_id = transaction_id;
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            LogException(url, e);
//                        }
////                    Answers.getInstance().logStartCheckout(new StartCheckoutEvent()
////                            .putTotalPrice(BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("Amount",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))))
////                            .putCustomAttribute("Total",BigDecimal.valueOf(Double.parseDouble(totalT.replace(",",""))*Double.parseDouble(priceT.replace(",",""))))
////                            .putCustomAttribute("ItemName",titleName.getText().toString())
////                            .putCustomAttribute("ItemType",SkuConfirmActivity.sub_class_name)
////                            .putCustomAttribute("ItemId",skuId));
//                        ChooseSkuActivity.addItem = 1;
//
//
//                        TransactionDetailActivity.productId = skuId;
//                        TransactionDetailActivity.isDelete = true;
//
//                        FavoriteDetailActivity.productId = skuId;
//                        FavoriteDetailActivity.isDelete = true;
//                        MainActivity.data5 = true;
//
////                        if(Utils.getPrefer(SkuConfirmActivity.this,"QC").length()>0){
////                            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
////                            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
////                            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
////                        }
//
//                        showToast(SkuConfirmActivity.this, getResources().getString(R.string.dialog_add_product) + titleName.getText() + getResources().getString(R.string.dialog_add_to_cart_success));
//                        finish();
//                    }
//
//                }
//            }.execute();
//        }
//
//
//    }


    public void setTextCurrentDate() {
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("E dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());

        String dateArr[] = date.split(" ");
        Cdate = dateArr[1];
//        txtCalendar.setText(date);
    }


//    @SuppressLint("StaticFieldLeak")
//    private void DeleteBasket(final String url) {
//        Log.i("", "Customer: " + url);
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                addBasket();
//
//            }
//        }.execute();
//    }

    private Call<String> callGetSkuDetail() {
        return rudyService.getSkuDetail(
                ApiEndPoint.LANG,
                skuId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getData(){
        showLoading(this);
        callGetSkuDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONObject itemObject = object.getJSONObject("items");
                        String item = object.optString("items");
                        ItemDetailModel itemDetailModel = gson.fromJson(item, ItemDetailModel.class);

                        listUnitCode = new ArrayList<>();
                        listUnitCode = itemDetailModel.getUnitList();
                        listUnitCodeShow = new ArrayList<>();
                        for (int i = 0; i < itemDetailModel.getUnitList().size(); i++) {
                            listUnitCodeShow.add(itemDetailModel.getUnitList().get(i).getUnit());
                        }


                        unitCodeTypeSpinner customSpinner = new unitCodeTypeSpinner(SkuConfirmActivity.this, listUnitCodeShow);
                        unitCodeSpiner.setAdapter(customSpinner);
                        unitCodeSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        unitCodeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                unit_id = listUnitCode.get(position).getId();
                                textVolume.setText(getResources().getString(R.string.volume) + " ( " + listUnitCode.get(position).getUnit().replace("\n", "") + " )");
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        imgSpiner.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                unitCodeSpiner.performClick();
                            }
                        });

                        price1.setText(itemObject.optString("price"));
                        price2.setText(itemObject.optString("price2"));
                        price3.setText(itemObject.optString("price3"));

                        ArrayList<String> aa = new ArrayList<>();
                        aa.add("%");
                        aa.add(Utils.currencyForShow);
                        unitCodeTypeSpinner customSpinnerDiscount = new unitCodeTypeSpinner(SkuConfirmActivity.this, aa);
                        discountSpiner.setAdapter(customSpinnerDiscount);
                        discountSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        discountSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                totalDiscount.setText("");
                                if (position == 0){
                                    Utils.inputMaxLength(totalDiscount, 5);
                                    discountType = "1";
                                }else{
                                    Utils.inputMaxLength(totalDiscount, 7);
                                    discountType = "2";
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        imgSpinerDiscount.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                discountSpiner.performClick();
                            }
                        });



                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(resp, e);
                    }
                }


                if (DealByPhaseActivity.transaction_id.equalsIgnoreCase("")) {
                    if (!TransactionDetailActivity.transaction_id.equalsIgnoreCase("")) {
                        getTransectionDetail(TransactionDetailActivity.transaction_id);
                    } else {
                        return;
                    }
                } else {
                    getTransectionDetail(DealByPhaseActivity.transaction_id);
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(SkuConfirmActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void getData(final String url) {
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONObject itemObject = object.getJSONObject("items");
//                        String item = object.optString("items");
//                        Gson gson = new Gson();
//                        ItemDetailModel itemDetailModel = gson.fromJson(item, ItemDetailModel.class);
//
//                        listUnitCode = new ArrayList<>();
//                        listUnitCode = itemDetailModel.getUnitList();
//                        listUnitCodeShow = new ArrayList<>();
//                        for (int i = 0; i < itemDetailModel.getUnitList().size(); i++) {
//                            listUnitCodeShow.add(itemDetailModel.getUnitList().get(i).getUnit());
//                        }
//
//
//                        unitCodeTypeSpinner customSpinner = new unitCodeTypeSpinner(SkuConfirmActivity.this, listUnitCodeShow);
//                        unitCodeSpiner.setAdapter(customSpinner);
//                        unitCodeSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                        unitCodeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                            @Override
//                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                unit_id = listUnitCode.get(position).getId();
//                                textVolume.setText(getResources().getString(R.string.volume) + " ( " + listUnitCode.get(position).getUnit().replace("\n", "") + " )");
//                            }
//
//                            @Override
//                            public void onNothingSelected(AdapterView<?> parent) {
//
//                            }
//                        });
//                        imgSpiner.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                unitCodeSpiner.performClick();
//                            }
//                        });
//
//
////                        ArrayAdapter aa = new ArrayAdapter(SkuConfirmActivity.this, android.R.layout.simple_spinner_item, listUnitCodeShow);
////                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////                        unitCodeSpiner.setAdapter(aa);
////                        unitCodeSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////                            @Override
////                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
////                                unit_id = listUnitCode.get(position).getId();
////                                textVolume.setText(getResources().getString(R.string.volume) + " ( " + listUnitCode.get(position).getUnit().replace("\n", "") + " )");
////                            }
////
////                            @Override
////                            public void onNothingSelected(AdapterView<?> parent) {
////
////                            }
////                        });
////
////                        imgSpiner.setOnClickListener(new View.OnClickListener() {
////                            @Override
////                            public void onClick(View v) {
////                                unitCodeSpiner.performClick();
////                            }
////                        });
//
//
//                        price1.setText(itemObject.optString("price"));
//                        price2.setText(itemObject.optString("price2"));
//                        price3.setText(itemObject.optString("price3"));
//
//                        ArrayList<String> aa = new ArrayList<>();
//                        aa.add("%");
//                        aa.add(Utils.currencyForShow);
//                        unitCodeTypeSpinner customSpinnerDiscount = new unitCodeTypeSpinner(SkuConfirmActivity.this, aa);
//                        discountSpiner.setAdapter(customSpinnerDiscount);
//                        discountSpiner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                        discountSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                            @Override
//                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                totalDiscount.setText("");
//                                if (position == 0){
//                                    Utils.inputMaxLength(totalDiscount, 2);
//                                    discountType = "1";
//                                }else{
//                                    Utils.inputMaxLength(totalDiscount, 7);
//                                    discountType = "2";
//                                }
//                            }
//
//                            @Override
//                            public void onNothingSelected(AdapterView<?> parent) {
//
//                            }
//                        });
//                        imgSpinerDiscount.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                discountSpiner.performClick();
//                            }
//                        });
//
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(url, e);
//                    }
//                }
//
//
//                if (DealByPhaseActivity.transaction_id.equalsIgnoreCase("")) {
//                    if (!TransactionDetailActivity.transaction_id.equalsIgnoreCase("")) {
//                        getTransectionDetail(getGetTranDetail(Utils.APP_LANGUAGE, TransactionDetailActivity.transaction_id));
//                    } else {
//                        return;
//                    }
//                } else {
//                    getTransectionDetail(getGetTranDetail(Utils.APP_LANGUAGE, DealByPhaseActivity.transaction_id));
//                }
//
//
//            }
//        }.execute();
//    }

    private Call<String> callGetTransectionDetail(String _transectionid) {
        return rudyService.getTransectionDetail(
                ApiEndPoint.LANG,
                _transectionid
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getTransectionDetail(String _transectionid){
        showLoading(this);
        callGetTransectionDetail(_transectionid).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                transactionData = gson.fromJson(resp, Transaction.class);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(SkuConfirmActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void getTransectionDetail(final String url) {
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                Gson gson = new Gson();
//                transactionData = gson.fromJson(string, Transaction.class);
//            }
//        }.execute();
//    }

//    public void setFavIcon(String items) {
//        try {
//            JSONObject object = new JSONObject(items);
//            JSONArray itemArray = object.getJSONArray("items");
//            BaseActivity.favItemSize = itemArray.length();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void setOnclick() {
        clsBtn.setOnClickListener(this);
        case1.setOnClickListener(this);
        givePrice.setOnClickListener(this);
        case3.setOnClickListener(this);
        dayExplane.setOnClickListener(this);
        allDay.setOnClickListener(this);
        morning.setOnClickListener(this);
        afternoon.setOnClickListener(this);
        typeNormal.setOnClickListener(this);
        typeQuick.setOnClickListener(this);
        calendar.setOnClickListener(this);
        save.setOnClickListener(this);
        tabHide.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    private void initView() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();

        extras = getIntent().getExtras();
        clsBtn = findViewById(R.id.cls_btn);
        case1 = findViewById(R.id.case1);
        txtCase1 = findViewById(R.id.txt_case1);
        givePrice = findViewById(R.id.give_price);
        txtCase2 = findViewById(R.id.txt_case2);
        case3 = findViewById(R.id.case3);
        txtCase3 = findViewById(R.id.txt_case3);
        total = findViewById(R.id.total);
        price = findViewById(R.id.price);
        setDayLayout = findViewById(R.id.set_day_layout);
        allDay = findViewById(R.id.all_day);
        txtAllday = findViewById(R.id.txt_allday);
        morning = findViewById(R.id.morning);
        txtMorning = findViewById(R.id.txt_morning);
        afternoon = findViewById(R.id.afternoon);
        txtNoon = findViewById(R.id.txt_noon);
        typeNormal = findViewById(R.id.type_normal);
        txtNormal = findViewById(R.id.txt_normal);
        typeQuick = findViewById(R.id.type_quick);
        txtQuick = findViewById(R.id.txt_quick);
        note = findViewById(R.id.note);
        save = findViewById(R.id.save);
        dayExplane = findViewById(R.id.day_explane);
        txtCalendar = findViewById(R.id.txt_calendar);
        calendar = findViewById(R.id.calendar);
        titleName = findViewById(R.id.title_name);
        scroll = findViewById(R.id.scroll);
        ttSum = findViewById(R.id.tt_sum);
        tabHide = findViewById(R.id.tabHide);
        nor_ico = findViewById(R.id.nor_ico);
        fst_ico = findViewById(R.id.fst_ico);
        cancel = findViewById(R.id.cancel);
        recmd_price = findViewById(R.id.recmd_price);
        price1 = findViewById(R.id.price1);
        price2 = findViewById(R.id.price2);
        price3 = findViewById(R.id.price3);


        textCurrency = (TextView) findViewById(R.id.textCurrency);
        textCurrency2 = (TextView) findViewById(R.id.textCurrency2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navbar = (LinearLayout) findViewById(R.id.navbar);
        linearLayout8 = (LinearLayout) findViewById(R.id.linearLayout8);
        linearLayout9 = (LinearLayout) findViewById(R.id.linearLayout9);
        tab1Layout = (RelativeLayout) findViewById(R.id.tab1_layout);
        remarkLayout = (LinearLayout) findViewById(R.id.remark_layout);
        calendarLayout = (LinearLayout) findViewById(R.id.calendar_layout);
        dataView = (CardView) findViewById(R.id.dataView);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageMore = (ImageView) findViewById(R.id.image_more);
        norIco = (ImageView) findViewById(R.id.nor_ico);
        fstIco = (ImageView) findViewById(R.id.fst_ico);
        textView4 = (TextView) findViewById(R.id.textView4);
        priceLayout = findViewById(R.id.price_layout);
        unitCodeSpiner = (Spinner) findViewById(R.id.unitCodeSpiner);
        recmdPrice = (RelativeLayout) findViewById(R.id.recmd_price);
        imageView21 = (ImageView) findViewById(R.id.imageView21);
        tab2Layout = (LinearLayout) findViewById(R.id.tab2_layout);
        tab3Layout = (LinearLayout) findViewById(R.id.tab3_layout);
        textView8 = (TextView) findViewById(R.id.textView8);
        textVolume = (TextView) findViewById(R.id.textVolume);
        textVolumeDiscount = (TextView) findViewById(R.id.textVolumeDiscount);
        totalDiscount = (EditText) findViewById(R.id.totalDiscount);
        sumAfterDiscount = (TextView) findViewById(R.id.sumAfterDiscount);
        textCurrency3 = (TextView) findViewById(R.id.textCurrency3);
        imgSpiner = (ImageView) findViewById(R.id.imgSpiner);
        imgSpinerDiscount = (ImageView) findViewById(R.id.imgSpiner_discount);
        discountSpiner = (Spinner) findViewById(R.id.discountSpiner);
    }

    public void setTab(View v) {
        if (v == case1) {
            type = "1";
            txtCase1.setTextColor(getResources().getColor(R.color.white));
            case1.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab1));

            txtCase2.setTextColor(getResources().getColor(R.color.txt_tab2));
            givePrice.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab2_white));
            txtCase3.setTextColor(getResources().getColor(R.color.txt_tab3));
            case3.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab3_white));
        }
        if (v == givePrice) {
            type = "2";
            txtCase1.setTextColor(getResources().getColor(R.color.txt_tab1));
            case1.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab1_white));
            txtCase3.setTextColor(getResources().getColor(R.color.txt_tab3));
            case3.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab3_white));

            txtCase2.setTextColor(getResources().getColor(R.color.white));
            givePrice.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab2));
        }
        if (v == case3) {
            type = "3";
            txtCase3.setTextColor(getResources().getColor(R.color.white));
            case3.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab3));

            txtCase1.setTextColor(getResources().getColor(R.color.txt_tab1));
            case1.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab1_white));
            txtCase2.setTextColor(getResources().getColor(R.color.txt_tab2));
            givePrice.setBackground(getResources().getDrawable(R.drawable.rounder_cart_tab2_white));
        }


    }

    public void setShowDayLayout(boolean isShow) {
        if (isShow) {
            setDayLayout.setVisibility(View.VISIBLE);
        } else {
            setDayLayout.setVisibility(View.GONE);
            txtCalendar.setText(getResources().getString(R.string.add_delivery_date));
        }
    }

    public void setTypeSent(View v) {
        if (v == typeNormal) {
            deliveryType = "1";
            txtNormal.setTextColor(getResources().getColor(R.color.white));
            txtQuick.setTextColor(getResources().getColor(R.color.splash_bg));

            typeNormal.setBackground(getResources().getDrawable(R.drawable.rounder_day_select));
            typeQuick.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            nor_ico.setImageDrawable(getDrawable(R.drawable.delivery_normal_white));
            fst_ico.setImageDrawable(getDrawable(R.drawable.delivery_fast_blue));


        }
        if (v == typeQuick) {
            deliveryType = "2";
            txtNormal.setTextColor(getResources().getColor(R.color.splash_bg));
            txtQuick.setTextColor(getResources().getColor(R.color.white));

            typeNormal.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            typeQuick.setBackground(getResources().getDrawable(R.drawable.rounder_day_select));
            nor_ico.setImageDrawable(getDrawable(R.drawable.delivery_normal));
            fst_ico.setImageDrawable(getDrawable(R.drawable.delivery_fast));


        }
    }

    public void setTabDay(View v) {
        if (v == allDay) {
            phase = "1";
            txtAllday.setTextColor(getResources().getColor(R.color.white));
            txtMorning.setTextColor(getResources().getColor(R.color.splash_bg));
            txtNoon.setTextColor(getResources().getColor(R.color.splash_bg));

            allDay.setBackground(getResources().getDrawable(R.drawable.rounder_day_select));
            morning.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            afternoon.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
        }
        if (v == morning) {
            phase = "2";
            txtAllday.setTextColor(getResources().getColor(R.color.splash_bg));
            txtMorning.setTextColor(getResources().getColor(R.color.white));
            txtNoon.setTextColor(getResources().getColor(R.color.splash_bg));
            allDay.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            morning.setBackground(getResources().getDrawable(R.drawable.rounder_day_select));
            afternoon.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
        }
        if (v == afternoon) {
            phase = "3";
            txtAllday.setTextColor(getResources().getColor(R.color.splash_bg));
            txtMorning.setTextColor(getResources().getColor(R.color.splash_bg));
            txtNoon.setTextColor(getResources().getColor(R.color.white));
            allDay.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            morning.setBackground(getResources().getDrawable(R.drawable.rounder_day_unselect));
            afternoon.setBackground(getResources().getDrawable(R.drawable.rounder_day_select));
        }

    }

    public void opencalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                SkuConfirmActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);

        dpd.show(getSupportFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == case1) {
            setTab(case1);
        }
        if (v == givePrice) {
            showToast(SkuConfirmActivity.this, "coming soon . .");
        }
        if (v == case3) {
            showToast(SkuConfirmActivity.this, "coming soon . .");
        }

        if (v == dayExplane) {
            opencalendar();
        }
        if (v == allDay) {
            setTabDay(allDay);
        }
        if (v == morning) {
            setTabDay(morning);
        }
        if (v == afternoon) {
            setTabDay(afternoon);
        }

        if (v == typeNormal) {
            setTypeSent(typeNormal);
        }
        if (v == typeQuick) {
            setTypeSent(typeQuick);
        }

        if (v == calendar) {
            //test hellow world ja
        }
        if (v == tabHide) {
            Utils.hideKeyboardFrom(SkuConfirmActivity.this, tabHide);
        }

        if (v == cancel) {
            setShowDayLayout(showDayLayout);
        }


        if (v == save) {

            if (ttSum.getText().toString().equalsIgnoreCase("0")) {
                showToast(SkuConfirmActivity.this, getResources().getString(R.string.toast_pls_specify_volume_product));
            } else {

                if (price.getText().toString().length() > 0) {
                    if (price.getText().toString().equalsIgnoreCase("0")) {
                        priceT = "1";
                    } else {
                        priceT = price.getText().toString().replace(",", "");
                    }
                }
                if (price.getText().toString().length() == 0) {
                    priceT = "1";
                }
                if (total.getText().toString().length() > 0) {
                    if (total.getText().toString().equalsIgnoreCase("0")) {
                        totalT = "0";
                    } else {
                        totalT = total.getText().toString().replace(",", "");
                    }
                }
                if (total.getText().toString().length() == 0) {
                    totalT = "0";
                }

                //check lower price
                if (Double.parseDouble(price3.getText().toString()) > 0){
                    checkLowerPrice(Double.parseDouble(price3.getText().toString()));
                }else if(Double.parseDouble(price2.getText().toString()) > 0){
                    checkLowerPrice(Double.parseDouble(price2.getText().toString()));
                }else if(Double.parseDouble(price1.getText().toString()) > 0){
                    checkLowerPrice(Double.parseDouble(price1.getText().toString()));
                }else{
                    checkStock();
                }
            }
        }
    }

    private void checkStock(){
        //check have stock
        if (Double.parseDouble(total.getText().toString().replace(",", "")) > Double.parseDouble(stock)) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                    .title(getResources().getString(R.string.dialog_volume_product_in_warehouse))
                    .content(getResources().getString(R.string.dialog_want_to_offer_this_volume))
                    .positiveText(getResources().getString(R.string.agreed))
                    .negativeText(getResources().getString(R.string.cancel))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            addBasket();
                            dialog.dismiss();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    });

            MaterialDialog del_dialog = builder.build();
            del_dialog.show();
        }else{
            addBasket();
        }
    }

    private void checkLowerPrice(Double _price){
        if (Double.parseDouble(priceT) < _price) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                    .title(getResources().getString(R.string.dialog_price_below_the_threshold))
                    .content(getResources().getString(R.string.alert_lower_price))
                    .positiveText(getResources().getString(R.string.agreed))
                    .negativeText(getResources().getString(R.string.cancel))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            MainActivity.data5 = true;
                            checkStock();
                            dialog.dismiss();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    });

            MaterialDialog del_dialog = builder.build();
            del_dialog.show();
        }else{
            checkStock();
        }
    }

    public void addBasket() {
        checkCaseBeforeAddBasket();
    }

//    public void deleteBasket() {
//        DeleteBasket(getDeleteBasket(Utils.APP_LANGUAGE, shopID, user_id, skuId, ChooseSkuActivity.projectId));
//    }

//    public void setFAV() {
//        getData(getAddFav(Utils.APP_LANGUAGE, shopID, user_id, skuId, ChooseSkuActivity.projectId));
//    }
//
//    public void deleteFAV() {
//        getData(getDeleteFav(Utils.APP_LANGUAGE, shopID, user_id, skuId, ChooseSkuActivity.projectId));
//
//    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("E", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        Date dateWeek = new Date(year, monthOfYear, dayOfMonth - 1);
        String dayOfWeek = simpledateformat.format(dateWeek);
        String date = dayOfWeek + " " + dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        String dateArr[] = date.split(" ");
        Cdate = dateArr[1];
        txtCalendar.setText(date);

        setShowDayLayout(!showDayLayout);

//        showDayLayout = !showDayLayout;

    }


    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }


    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        textVolume.setText(getResources().getString(R.string.volume) + "( " + listUnitCode.get(position) + " )");
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
