package com.scg.rudy.ui.location_customer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.json_parser.Parser;
import com.scg.rudy.model.pojo.ByLocationModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.location.AppUtils;
import com.scg.rudy.utils.location.FetchAddressIntentService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class ProjectLocationActivity extends AppCompatActivity
        implements OnMapReadyCallback,
                GoogleApiClient.ConnectionCallbacks,
                GoogleApiClient.OnConnectionFailedListener,
                LocationListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static String TAG = "MAP LOCATION";
    private Context mContext;
    //    TextView mLocationMarkerText;
    private LatLng mCenterLatLong;
    private View myLocationButton;
    private Double DragLat = 0.0, DragLng = 0.0;
    private String user_id = "";
    private String shopID = "";

    /**
     * Receiver registered with this activity to get the response from FetchAddressIntentService.
     */
    private AddressResultReceiver mResultReceiver;
    /** The formatted location address. */
    protected String mAddressOutput;
    protected String mData;

    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    protected EditText mLocationAddress;
    protected TextView mLocationText;
    protected static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    protected LinearLayout curBtn;
    private TextView address;
    private TextView latlng;

    private RudyService rudyService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setEnviroment();
        mContext = this;
        rudyService = ApiHelper.getClient();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        //        mLocationMarkerText = (TextView) findViewById(R.id.locationMarkertext);
        mLocationAddress = (EditText) findViewById(R.id.Address);
        mLocationText = (TextView) findViewById(R.id.Locality);
        curBtn = findViewById(R.id.cur_btn);

        mLocationText.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openAutocompleteActivity();
                    }
                });

        mapFragment.getMapAsync(this);
        mResultReceiver = new AddressResultReceiver(new Handler());

        if (checkPlayServices()) {
            if (!AppUtils.isLocationEnabled(mContext)) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton(
                        "Open location settings",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface paramDialogInterface, int paramInt) {
                                Intent myIntent =
                                        new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                            }
                        });
                dialog.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(
                                    DialogInterface paramDialogInterface, int paramInt) {
                                // TODO Auto-generated method stub
                            }
                        });
                dialog.show();
            }
            buildGoogleApiClient();

        } else {
            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT)
                    .show();
        }
        myLocationButton = mapFragment.getView().findViewById(0x2);


        initView();
    }

    public void setEnviroment() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        mMap = googleMap;
        myLocationButton.setVisibility(View.INVISIBLE);
        curBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myLocationButton.performClick();
                    }
                });
//        mMap.setOnCameraChangeListener(
//                new GoogleMap.OnCameraChangeListener() {
//                    @Override
//                    public void onCameraChange(CameraPosition cameraPosition) {
//                        Log.d("Camera postion change" + "", cameraPosition + "");
//                        mCenterLatLong = cameraPosition.target;
//
//                        mMap.clear();
//
//                        try {
//
//                            Location mLocation = new Location("");
//                            mLocation.setLatitude(mCenterLatLong.latitude);
//                            mLocation.setLongitude(mCenterLatLong.longitude);
//
//                            startIntentService(mLocation);
//                            //                    mLocationMarkerText.setText("Lat : " +
//                            // mCenterLatLong.latitude + "," + "Long : " +
//                            // mCenterLatLong.longitude);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //        mMap.setMyLocationEnabled(true);
        //        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        //
        //        // Add a marker in Sydney and move the camera
        //        LatLng sydney = new LatLng(-34, 151);
        //        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            //            if (location != null) {
            //                changeMap(location);
            //
            // LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            //            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient =
                new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(
                                resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                // finish();
            }
            return false;
        }
        return true;
    }

    private void changeMap(Location location) {

        Log.d(TAG, "Reaching map" + mMap);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                                this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;

            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition =
                    new CameraPosition.Builder().target(latLong).zoom(16f).tilt(0).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            myLocationButton.setVisibility(View.INVISIBLE);
            curBtn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myLocationButton.performClick();
                        }
                    });

            //            mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," +
            // "Long : " + location.getLongitude());

            latlng.setText(location.getLatitude() + "," + location.getLongitude());

            DragLat = location.getLatitude();
            DragLng = location.getLongitude();
            startIntentService(location);
            getNearLocation();

        } else {
            Toast.makeText(
                            getApplicationContext(),
                            "Sorry! unable to create maps",
                            Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void initView() {
        address = (TextView) findViewById(R.id.address);
        latlng = (TextView) findViewById(R.id.latlng);
    }



    /** Receiver for data sent from FetchAddressIntentService. */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /** Receives data sent from FetchAddressIntentService and updates the UI in MainActivity. */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);

            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);
            mData =  resultData.getString(AppUtils.LocationConstants.LOCATION_DATA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);

            displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));

            }
        }
    }

    /** Updates the address in the UI. */
    protected void displayAddressOutput() {
        //  mLocationAddressTextView.setText(mAddressOutput);
        try {
            if (mAreaOutput != null)
                // mLocationText.setText(mAreaOutput+ "");
                address.setText(mData);
            mLocationAddress.setText(mAddressOutput);
            mLocationText.setText(mAreaOutput);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);
        startService(intent);
    }

    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance()
                    .getErrorDialog(this, e.getConnectionStatusCode(), 0 /* requestCode */)
                    .show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message =
                    "Google Play Services is not available: "
                            + GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }

    /** Called after the autocomplete activity has finished to return its result. */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(mContext, data);

                // TODO call location based filter

                LatLng latLong;

                latLong = place.getLatLng();

                mLocationText.setText(place.getName() + "");
                latlng.setText(latLong.toString());

                CameraPosition cameraPosition =
                        new CameraPosition.Builder().target(latLong).zoom(16f).tilt(0).build();

                if (ActivityCompat.checkSelfPermission(
                                        this, Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(
                                        this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[]
                    // permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the
                    // documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(mContext, data);
        } else if (resultCode == RESULT_CANCELED) {
            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }
    }


    public void addMarkerView(ArrayList<ByLocationModel> arrayList){
        mMap.clear();

        for (int i = 0; i <arrayList.size() ; i++) {
            ByLocationModel byLocationModel = arrayList.get(i);
            if(byLocationModel.getLat_lng().length()>0){
                String latlng[] = byLocationModel.getLat_lng().split(",");


                double lat = Double.parseDouble(latlng[0]);
                double lng = Double.parseDouble(latlng[1]);
                Marker marker = mMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(lat, lng))
//                                .snippet(arrayList.get(i).getAddress())
                                .title(arrayList.get(i).getProject_name())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker)));

                marker.setTag(arrayList.get(i).getId()+","+arrayList.get(i).getProject_name());
                marker.setPosition(new LatLng(lat, lng));
//                startDropMarkerAnimation(marker);
            }
        }
        mMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        String tags[] = marker.getTag().toString().split(",");

        Intent intent = new Intent(this, ProjectDetailActivity.class);
        intent.putExtra("projectId",tags[0]);
        intent.putExtra("name",tags[1]);
        startActivity(intent);
    }

    private Call<String> callLogin() {
        return rudyService.locationCache(
                Utils.APP_LANGUAGE,
                user_id,
                DragLat.toString(),
                DragLng.toString()
        );
    }

    @SuppressLint({"LongLogTag", "StaticFieldLeak"})
    private void getNearLocation(){
        final Dialog dialog =  new Dialog(ProjectLocationActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,ProjectLocationActivity.this);

        callLogin().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ArrayList<ByLocationModel> arrayList = Parser.byLocationModels(resp);
                    if(arrayList.size()>0){
                        addMarkerView(arrayList);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProjectLocationActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(ProjectLocationActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint({"LongLogTag", "StaticFieldLeak"})
//    private void getNearLocation(final String url) {
////        isLoad = true;
//
//        Log.i(TAG, "URL: " + url);
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if (string.contains("200")) {
//                    ArrayList<ByLocationModel> arrayList = Parser.byLocationModels(string);
//                    if(arrayList.size()>0){
//                        addMarkerView(arrayList);
//                    }
//                }
//            }
//        }.execute();
//    }

}
