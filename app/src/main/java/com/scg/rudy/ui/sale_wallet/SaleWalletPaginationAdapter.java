package com.scg.rudy.ui.sale_wallet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.sale_wallet.DetailItem;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 *
 * @author jackie
 */

public class SaleWalletPaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private  List<DetailItem> detailItemList;
    private Context context;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private SaleWalletInterface  mCallback;
    private String errorMsg;

    public SaleWalletPaginationAdapter(Context context, SaleWalletInterface mCallback) {
        this.context = context;
        this.mCallback = mCallback;
        detailItemList = new ArrayList<>();


    }

    public List<DetailItem> getDetailItemList() {
        return detailItemList;
    }

    public void setSku(List<DetailItem> detailItemList) {
        this.detailItemList = detailItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.sale_wallet_item_list, parent, false);
            return new SaleWalletViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new LoadingViewHolder(viewLoading);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DetailItem items = detailItemList.get(position); // Movie
        int i = getItemViewType(position);
        if (i == ITEM) {
            SaleWalletViewHolder saleWalletViewHolder = (SaleWalletViewHolder) holder;
            saleWalletViewHolder.qoNumber.setText(items.getOrderNumber());
            saleWalletViewHolder.number_txt.setText(String.valueOf(items.getNo()));
            saleWalletViewHolder.promo.setText(items.getPromotionName());
            saleWalletViewHolder.totalMoney.setText(items.getTotalAmount());
            saleWalletViewHolder.percent.setText(items.getPercent());
            saleWalletViewHolder.moneyReceive.setText(items.getTotalCommission());
            if(items.getIspaid().equalsIgnoreCase("1")){
                saleWalletViewHolder.txtStat.setText(context.getResources().getString(R.string.paid));
                saleWalletViewHolder.icoStat.setVisibility(View.VISIBLE);
                saleWalletViewHolder.txtStat.setTextColor(context.getResources().getColor(R.color.black));
                saleWalletViewHolder.moneyReceive.setTextColor(context.getResources().getColor(R.color.green_salewallet));
//                saleWalletViewHolder.txtStat.setTextColor(context.getResources().getColor(R.color.green_salewallet));
            }else{
                saleWalletViewHolder.txtStat.setText(context.getResources().getString(R.string.owe));
                saleWalletViewHolder.icoStat.setVisibility(View.GONE);
                saleWalletViewHolder.txtStat.setTextColor(context.getResources().getColor(R.color.red_salewallet));
                saleWalletViewHolder.moneyReceive.setTextColor(context.getResources().getColor(R.color.red_salewallet));
            }


        } else if (i == LOADING) {
            LoadingViewHolder loadingVH = (LoadingViewHolder) holder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public int getItemCount() {
        return detailItemList == null ? 0 : detailItemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == detailItemList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }

    public void add(DetailItem r) {

        detailItemList.add(r);
        notifyItemInserted(detailItemList.size() - 1);
    }

    public void addAll(List<DetailItem> detailItemList) {
        for (DetailItem result : detailItemList) {
            add(result);
        }
    }

    public void remove(DetailItem r) {
        int position = detailItemList.indexOf(r);
        if (position > -1) {
            detailItemList.remove(position);
            notifyItemRemoved(position);
        }
    }


    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new DetailItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = detailItemList.size() - 1;
        DetailItem result = getItem(position);

        if (result != null) {
            detailItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private DetailItem getItem(int position) {
        return detailItemList.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(detailItemList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    private class SaleWalletViewHolder extends RecyclerView.ViewHolder {
        private TextView number_txt;
        private TextView qoNumber;
        private TextView promo;
        private TextView totalMoney;
        private TextView percent;
        private TextView moneyReceive;
        private ImageView icoStat;
        private TextView txtStat;

        SaleWalletViewHolder(View view) {
            super(view);

            number_txt = view.findViewById(R.id.number_txt);
            qoNumber = view.findViewById(R.id.qo_number);
            promo = view.findViewById(R.id.promo);
            totalMoney = view.findViewById(R.id.total_money);
            percent = view.findViewById(R.id.percent);
            moneyReceive = view.findViewById(R.id.money_receive);
            icoStat = view.findViewById(R.id.ico_stat);
            txtStat = view.findViewById(R.id.txt_stat);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
                mCallback.retryPageLoad();
            }
        }
    }

}
