package com.scg.rudy.ui.forgot_password;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.forgot_password.ForgotPassword;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

public class ForgotPasswordActivity extends BaseActivity {


    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.hideKey)
    RelativeLayout hideKey;
    @BindView(R.id.close)
    ImageButton close;

    private RudyService rudyService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        rudyService = ApiHelper.getClient();
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }



    public void showData(ForgotPassword forgotPassword) {
        showToast(this, forgotPassword.getItems());
        email.setText("");
        Utils.hideKeyboardFrom(this, email);
    }

    public void showErrorIsNull() {

    }

    public void BodyError(ResponseBody responseBodyError) {

    }

    public void Failure(Throwable t) {

    }

    @OnClick({R.id.submit, R.id.hideKey,R.id.close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (!Utils.isEmpty(email)) {
                    loadForgot();
                } else {
                    showToast(this, getResources().getString(R.string.toast_pls_fill_userid));
                }
                break;
            case R.id.hideKey:
                Utils.hideKeyboardFrom(this, hideKey);
                break;

            case R.id.close :
                finish();
                break;
        }
    }



    private Call<String> callForgot() {
        return rudyService.forgotPassword(
                Utils.APP_LANGUAGE,
                email.getText().toString()
        );
    }

    private void loadForgot() {
        showLoading(this);
        callForgot().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ForgotPassword forgotPassword  = gson.fromJson(resp, ForgotPassword.class);
                    showData(forgotPassword);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ForgotPasswordActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ForgotPasswordActivity.this, t.getMessage());
            }
        });
    }


}
