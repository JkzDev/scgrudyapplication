package com.scg.rudy.ui.department_data_group;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.project_group_detail.ProjectListItem;

import java.util.List;


public class DepartmentGroupAdapter extends RecyclerView.Adapter<DepartmentGroupAdapter.PGdetailViewHolder> {


    private Context context;
    private List<ProjectListItem> arrayList;

    public DepartmentGroupAdapter(Context context, List<ProjectListItem> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public class PGdetailViewHolder extends RecyclerView.ViewHolder {

        public TextView name,type,unit_area,unit;
        public LinearLayout btn_sell;

        public PGdetailViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            unit_area = itemView.findViewById(R.id.unit_area);
            unit = itemView.findViewById(R.id.unit);
        }
    }

    @Override
    public DepartmentGroupAdapter.PGdetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_department_data_group, parent, false);
        return new PGdetailViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DepartmentGroupAdapter.PGdetailViewHolder holder, final int position) {
        ProjectListItem item = arrayList.get(position);

        holder.name.setText(item.getProjectName());
        holder.type.setText(item.getProjectTypeName());
        holder.unit.setText(item.getUnits());
        holder.unit_area.setText(item.getUnitArea());
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
