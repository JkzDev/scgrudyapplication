package com.scg.rudy.ui.login;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.onesignal.OneSignal;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.fire_base_model.User;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.forgot_password.ForgotPasswordActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.logger.LoggerFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.PERF_LOGIN;
import static com.scg.rudy.utils.Utils.getPrefer;
import static com.scg.rudy.utils.Utils.isEmpty;
import static com.scg.rudy.utils.Utils.savePrefer;
import static com.scg.rudy.utils.Utils.showToast;


public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    TextView forgotPassword;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.hideKey)
    RelativeLayout hideKey;
    @BindView(R.id.ico_email)
    ImageView icoEmail;
    @BindView(R.id.bg_email)
    LinearLayout bgEmail;
    @BindView(R.id.ico_pwd)
    ImageView icoPwd;
    @BindView(R.id.bg_pwd)
    LinearLayout bgPwd;
    @BindView(R.id.email_title)
    TextView emailTitle;
    @BindView(R.id.pwd_title)
    TextView pwdTitle;


    private DatabaseReference mDatabase;
    private FirebaseAnalytics mFirebaseAnalytics;
    private UserPOJO userPOJO;
    public static final String KEY_USER = "USER";

    public static final String LASTUSER = "LASTUSER";
    private RudyService rudyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        rudyService = ApiHelper.getClient();

        initView();
        submit.setOnClickListener(this);
        hideKey.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);


        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                if (isEmpty(email)) {
                    icoEmail.setImageDrawable(getDrawable(R.drawable.icons_8_user));
                    bgEmail.setBackground(getResources().getDrawable(R.drawable.bg_frame_login));
                    email.setTextColor(Color.parseColor("#2c3e50"));
                    emailTitle.setTextColor(Color.parseColor("#2c3e50"));
                } else {
                    icoEmail.setImageDrawable(getDrawable(R.drawable.icons_8_user_blue));
                    bgEmail.setBackground(getResources().getDrawable(R.drawable.bg_frame_login_active));
                    email.setTextColor(getResources().getColor(R.color.txt_color_login));
                    emailTitle.setTextColor(getResources().getColor(R.color.txt_color_login));
                }
                email.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                if (isEmpty(password)) {
                    icoPwd.setImageDrawable(getDrawable(R.drawable.icons_8_lock));
                    bgPwd.setBackground(getResources().getDrawable(R.drawable.bg_frame_login));
                    password.setTextColor(Color.parseColor("#2c3e50"));
                    pwdTitle.setTextColor(Color.parseColor("#2c3e50"));
                } else {
                    icoPwd.setImageDrawable(getDrawable(R.drawable.icons_8_lock_blue));
                    bgPwd.setBackground(getResources().getDrawable(R.drawable.bg_frame_login_active));
                    password.setTextColor(getResources().getColor(R.color.txt_color_login));
                    pwdTitle.setTextColor(getResources().getColor(R.color.txt_color_login));
                }
                password.setTypeface(_font);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if(getPrefer(this, PERF_LOGIN).contains("200")){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setUserData(UserPOJO userPOJO) {
        if (userPOJO != null) {
            this.userPOJO = userPOJO;
            Utils.setCurrencyForShow(this, userPOJO.getItems().getCurrency());
            Utils.currencyForShow = Utils.getCurrencyForShow(this);
            LoggerFactory.getWftLog(TAG, "user: " + userPOJO);
            LoggerFactory.getWftLog(TAG, "user.getName(): " + userPOJO.getItems().getName());
            LoggerFactory.getWftLog(TAG, "user.getWebsite(): " + userPOJO.getItems().getShopId());
            LoggerFactory.getWftLog(TAG, "user.getCompany(): " + userPOJO.getItems().getName());
            final User user = new User(userPOJO.getItems().getName(), userPOJO.getItems().getEmail());
            mDatabase.child("users").child(userPOJO.getItems().getId()).setValue(user);
            Answers.getInstance().logLogin(new LoginEvent()
                    .putSuccess(true)
                    .putCustomAttribute("Email", userPOJO.getItems().getEmail())
                    .putCustomAttribute("Shop", userPOJO.getItems().getShop())
                    .putCustomAttribute("Name", userPOJO.getItems().getName())
                    .putCustomAttribute("Nickname", userPOJO.getItems().getNickname()));
            String serializedData = userPOJO.serialize();

            Bundle bundle = new Bundle();
            JSONObject tags = new JSONObject();
            bundle.putString("email", userPOJO.getItems().getEmail());
            bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
            bundle.putString("id", userPOJO.getItems().getId());
            bundle.putString("level", userPOJO.getItems().getLevel());
            bundle.putString("name", userPOJO.getItems().getName());
            bundle.putString("nickname", userPOJO.getItems().getNickname());
            bundle.putString("shop", userPOJO.getItems().getShop());
            bundle.putString("shop_id",userPOJO.getItems().getShopId());


            mixpanel.identify(userPOJO.getItems().getEmployeeId());
            mixpanel.getPeople().identify(userPOJO.getItems().getEmployeeId());
            mixpanel.getPeople().set("email", userPOJO.getItems().getEmail());
            mixpanel.getPeople().set("employee_id", userPOJO.getItems().getEmployeeId());
            mixpanel.getPeople().set("id", userPOJO.getItems().getId());
            mixpanel.getPeople().set("level", userPOJO.getItems().getLevel());
            mixpanel.getPeople().set("name", userPOJO.getItems().getName());
            mixpanel.getPeople().set("nickname", userPOJO.getItems().getNickname());
            mixpanel.getPeople().set("shop", userPOJO.getItems().getShop());
            mixpanel.getPeople().set("shop_id",userPOJO.getItems().getShopId());

            try {
                tags.put("email", userPOJO.getItems().getEmail());
                tags.put("employee_id", userPOJO.getItems().getEmployeeId());
                tags.put("id", userPOJO.getItems().getId());
                tags.put("level", userPOJO.getItems().getLevel());
                tags.put("name", userPOJO.getItems().getName());
                tags.put("nickname", userPOJO.getItems().getNickname());
                tags.put("shop", userPOJO.getItems().getShop());
                tags.put("shop_id",userPOJO.getItems().getShopId());
            } catch (JSONException e) {
                e.printStackTrace();

            }

            Map<String, Object> props = new HashMap<String, Object>();
            props.put("email", userPOJO.getItems().getEmail());
            props.put("employee_id", userPOJO.getItems().getEmployeeId());
            props.put("id", userPOJO.getItems().getId());
            props.put("level", userPOJO.getItems().getLevel());
            props.put("name", userPOJO.getItems().getName());
            props.put("nickname", userPOJO.getItems().getNickname());
            props.put("shop", userPOJO.getItems().getShop());
            props.put("shop_id",userPOJO.getItems().getShopId());
            props.put("Date", new Date());

            OneSignal.sendTags(tags);

            JSONObject object = new JSONObject();
            try {
                object.put("email", userPOJO.getItems().getEmail());
                object.put("employee_id", userPOJO.getItems().getEmployeeId());
                object.put("id", userPOJO.getItems().getId());
                object.put("level", userPOJO.getItems().getLevel());
                object.put("name", userPOJO.getItems().getName());
                object.put("nickname", userPOJO.getItems().getNickname());
                object.put("shop", userPOJO.getItems().getShop());
                object.put("shop_id",userPOJO.getItems().getShopId());
                object.put("Date", new Date());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mixpanel.track("Login", object);
            mixpanel.track(userPOJO.getItems().getShop(), object);

            JSONObject sprop = new JSONObject();
            try {
                sprop.put("ShopName", userPOJO.getItems().getShop());
                mixpanel.registerSuperProperties(sprop);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mFirebaseAnalytics.logEvent("User", bundle);
            mFirebaseAnalytics.setUserProperty("ShopName",userPOJO.getItems().getShop());
            savePrefer(LoginActivity.this, PERF_LOGIN, serializedData);
            savePrefer(LoginActivity.this, LASTUSER, userPOJO.getItems().getEmployeeId());
            showToast(LoginActivity.this, getResources().getString(R.string.toast_log_in_success));
            FirebaseMessaging.getInstance().subscribeToTopic("news")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            String msg = "subscribed";
                            if (!task.isSuccessful()) {
                                msg ="subscribe_failed";
                            }
                            Log.e("subscribeToTopic news", msg);
                        }
                    });
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    public void getUserData() {
//        if (getPrefer(this, PERF_LOGIN).contains("200")) {
//            UserPOJO userPOJO = Utils.setEnviroment(this);
//            Bundle bundle = new Bundle();
//            bundle.putString("email", userPOJO.getItems().getEmail());
//            bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
//            bundle.putString("id", userPOJO.getItems().getId());
//            bundle.putString("level", userPOJO.getItems().getLevel());
//            bundle.putString("name", userPOJO.getItems().getName());
//            bundle.putString("nickname", userPOJO.getItems().getNickname());
//            bundle.putString("shop", userPOJO.getItems().getShop());
//            bundle.putString("shop_id",userPOJO.getItems().getShopId());
//            mFirebaseAnalytics.logEvent("UsageUser", bundle);
//            Answers.getInstance().logCustom(new CustomEvent("UsageUser")
//                    .putCustomAttribute("email", userPOJO.getItems().getEmail())
//                    .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
//                    .putCustomAttribute("id", userPOJO.getItems().getId())
//                    .putCustomAttribute("level", userPOJO.getItems().getLevel())
//                    .putCustomAttribute("name", userPOJO.getItems().getName())
//                    .putCustomAttribute("nickname", userPOJO.getItems().getNickname())
//                    .putCustomAttribute("shop", userPOJO.getItems().getShop())
//                    .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
//            );
//
//            showToast(LoginActivity.this, getResources().getString(R.string.toast_log_in_success));
//
//            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//            finish();
//        } else {
//            fixTestData();
//        }

//        UserPOJO userPOJO = Utils.setEnviroment(this);
//        Bundle bundle = new Bundle();
//        bundle.putString("email", userPOJO.getItems().getEmail());
//        bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
//        bundle.putString("id", userPOJO.getItems().getId());
//        bundle.putString("level", userPOJO.getItems().getLevel());
//        bundle.putString("name", userPOJO.getItems().getName());
//        bundle.putString("nickname", userPOJO.getItems().getNickname());
//        bundle.putString("shop", userPOJO.getItems().getShop());
//        bundle.putString("shop_id",userPOJO.getItems().getShopId());
//        mFirebaseAnalytics.logEvent("UsageUser", bundle);
//        Answers.getInstance().logCustom(new CustomEvent("UsageUser")
//                .putCustomAttribute("email", userPOJO.getItems().getEmail())
//                .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
//                .putCustomAttribute("id", userPOJO.getItems().getId())
//                .putCustomAttribute("level", userPOJO.getItems().getLevel())
//                .putCustomAttribute("name", userPOJO.getItems().getName())
//                .putCustomAttribute("nickname", userPOJO.getItems().getNickname())
//                .putCustomAttribute("shop", userPOJO.getItems().getShop())
//                .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
//        );
//
//        showToast(LoginActivity.this, getResources().getString(R.string.toast_log_in_success));
//
//        startActivity(new Intent(LoginActivity.this, MainActivity.class));
//        finish();
    }

//    public void fixTestData() {
//        if (!getPrefer(this, LASTUSER).isEmpty()) {
//
//            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
//                icoEmail.setImageDrawable(getDrawable(R.drawable.icons_8_user_blue));
//                bgEmail.setBackground(getResources().getDrawable(R.drawable.bg_frame_login_active));
//                email.setTextColor(getResources().getColor(R.color.txt_color_login));
//                emailTitle.setTextColor(getResources().getColor(R.color.txt_color_login));
//            email.setTypeface(_font);
//            email.setText(getPrefer(this, LASTUSER));
//
//        }
//    }

    private void initView() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
        email.setTypeface(_font);
        password.setTypeface(_font);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View view) {
        if (view == submit) {
            if (password.getText().toString().length() >= 4) {
                if (Utils.isNetworkConnected(this)){
                    loadLogin();
                }else{
                    showToast(this, "Rudy Move ไม่สามารถเชื่อต่อ Server ได้ กรุณาตรวจสอบการเชื่อมอินเตอร์เน็ตของท่าน");
                }
            } else {
                showToast(this, getString(R.string.password_latest8));
                password.setError(getString(R.string.password_latest8));
            }
        }
        if (view == hideKey) {
            Utils.hideKeyboardFrom(LoginActivity.this, hideKey);
        }

        if(view==forgotPassword){
            startActivity(new Intent(this, ForgotPasswordActivity.class));
        }
    }

    private Call<String> callLogin() {
        return rudyService.login(
                email.getText().toString().toLowerCase(),
                password.getText().toString(),
                ApiEndPoint.LANG
        );
    }


    public void loadLogin() {
        showLoading(this);
        callLogin().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    UserPOJO user  = gson.fromJson(resp, UserPOJO.class);
                    setUserData(user);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(LoginActivity.this, error.getItems());
                    email.setText("");
                    password.setText("");
                    email.requestFocus();
                    Utils.showKeyboardFrom(LoginActivity.this, email);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(LoginActivity.this, t.getMessage());
                email.setText("");
                password.setText("");
                email.requestFocus();
                Utils.showKeyboardFrom(LoginActivity.this, email);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_USER, Parcels.wrap(userPOJO));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        userPOJO = Parcels.unwrap(savedInstanceState.getParcelable(KEY_USER));
    }


    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }
}
