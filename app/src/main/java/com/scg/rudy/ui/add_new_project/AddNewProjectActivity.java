package com.scg.rudy.ui.add_new_project;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.departmentdata_new.ProjectNew;
import com.scg.rudy.model.pojo.project_detail.Items;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.add_new_project.Models.StorageDataOfflineModel;
import com.scg.rudy.ui.current_location.AddCurrentLocationActivity;
import com.scg.rudy.ui.current_location.AddCurrentLocationForAddNewProjectActivity;
import com.scg.rudy.ui.customer_data.CustomerDataActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.dealbyproject.DealByProjectActivity;
import com.scg.rudy.ui.department_data.DepartmentDataActivity;
import com.scg.rudy.ui.department_photo.TakePhotoActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;
import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.cardview.widget.CardView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class AddNewProjectActivity extends BaseActivity implements View.OnClickListener {
    public static CardView cardLocal;
    public static CardView cardTakepic;
    public static CardView cardDepartment;
    public static CardView cardCustomer;
    public static CardView cardByphase;
    public static ImageView ch1;
    public static ImageView ch2;
    public static ImageView ch3;
    public static ImageView ch4;
    private ImageView clsBtn;
    private TextView titleName;
    private LinearLayout adLocal;
    public static ImageView icoLocation;
    public static TextView txtLocal;
    public static CardView bgNum1;
    public static LinearLayout takePhoto;
    public static ImageView icoPhoto;
    public static TextView txtPhoto;
    public static CardView bgNum2;
    public static LinearLayout addDepartment;
    public static ImageView icoDepart;
    public static TextView txtDepart;
    public static CardView bgNum3;
    public static LinearLayout addCustomer;
    public static ImageView icoCustomer;
    public static TextView txtCustomer;
    public static CardView bgNum4;
    public static ImageView icoSale;
    public static TextView txtSale;
    public static CardView bgNum5;
    public static RelativeLayout postProject;
    public static RelativeLayout fake_layout;
    public static Bitmap bit1;
    public static Bitmap bit2;
    public static Bitmap bit3;
    public static Bitmap bit4;
    public static Bitmap bit5;
    public static Bitmap bit6;
    public static Double DragLat = 0.0, DragLng = 0.0;
    public static String address = "";
    public static boolean step1 = false;
    public static boolean step2 = false;
    public static boolean step3 = false;
    public static boolean step4 = false;
    public static String cateId = "0";
    public static String unit_number = "";
    public static String floor_number = "";
    public static String c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
    public static String c_id_owner = "";
    public static int ctype = 1;
    public static int ctype_owner = 2;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
    public static String cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany = "";// บริษัทลูกค้า
    public static String cphone = "";// เบอร์ติดต่อลูกค้า
    public static String cline = "";// line ID ของลูกค้า
    public static String cnote = "";// noteลูกค้า
    public static String cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany_owner = "";// บริษัทลูกค้า
    public static String cphone_owner = "";// เบอร์ติดต่อลูกค้า
    public static String cline_owner = "";// line ID ของลูกค้า
    public static String cnote_owner = "";// noteลูกค้า
    public static String other_type = "";
    public static String shopID = "";
    public static String image64_1 = "";
    public static String image64_2 = "";
    public static String image64_3 = "";
    public static String image64_4 = "";
    public static String image64_5 = "";
    public static String image64_6 = "";
    public static int catePosition = 0;
    public static int phasePosition = 0;
    public static int customerTypePosition = 0;
    public static int ownerTypePosition = 0;
    private String imagei_1 = "";
    private String imagei_2 = "";
    private String imagei_3 = "";
    private String imagei_4 = "";
    private String imagei_5 = "";
    private String imagei_6 = "";
    private LinearLayout saleByphase;
    //    private String detailData = "";
    private Bundle extras;
    //    public static projectDetail projectDetail;
    private TextView txtPostProject;
    public static double unit_budget_number;
    public static int maxPhase = 10;
    private Button clearData;
    public static ProjectDetail projectDetail;
    public static String group_id = "";

    //   New post data api
    public static String user_id ;//* : User who add the project
    public static String lat_lng  ="";//: project lat and lng [13.729194,100.622218]
    public static String project_address ="" ;//: project location address
    public static String project_name  ="";//: Project name [API will create if you did not post it]
    public static String project_type_id ="" ;//: project type id [API will create if you did not post it  ;//: 1]
    public static String phaseId ="0" ;//: phase id [API will create if you did not post it  ;//: 1]
    public static String units  ="";//: How many unit [API will create if you did not post it  ;//: 1]
    public static String unit_area ="" ;//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
    public static String unit_budget  ="";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
   // public static String project_stories ="" ;//: story [API will create if you did not post it  ;//: 2]
    public static String customer_name ="" ;//: Customer’s name
    public static String customer_phone ="" ;//: Customer’s phone
    public static String customer_email ="" ;//: Customer’s email
    public static String customer_line ="" ;//: Customer’s line
    public static String customer_tax_no ="" ;//: Customer’s tax no or ID card no.
    public static String customer_note ="" ;//: Customer’s note
    public static String house_owner_name ="" ;//: House owner name
    public static String house_owner_phone ="" ;//: house_owner_phone
    public static String house_owner_line ="" ;//: House owner line id
    public static String House_owner_note ="" ;//: House owner note
    public static String project_owner_name ="" ;//: Project owner name
    public static String project_owner_phone=""  ;//: project_owner_phone
    public static String project_owner_line ="" ;//: Project owner line
    public static String project_owner_note ="" ;//: Project owner note
    public static String customer_code ="" ; //: customer_code
    public static String champ_customer_code =""; //: champ_customer_code

    public static String customer_company ="";
    public static String pic ="";//: array post with base64

    public static ArrayList<GroupGalleryModel> mArrayList;

    public static final String TAG = "AddNewProjectActivity";

    public static ArrayList<ProjectNew> arrayListNewProject;

    private RudyService rudyService;

    private static int reCode_TAKEPHOTO_ACTION_TWO = 2;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    public static StorageDataOfflineModel storageDataOfflineModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_project);
        ButterKnife.bind(this);
        rudyService = ApiHelper.getClient();
        clearStaticData();
        initView();
        getUserData();
        setOnclick();
        setEnviroment();

        storageDataOfflineModel = new StorageDataOfflineModel();

        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());

        arrayListNewProject = new ArrayList<>();

    }

    private void clearStaticData() {
        DragLat = 0.0;
        DragLng = 0.0;
        address = "";
        step1 = false;
        step2 = false;
        step3 = false;
        step4 = false;
        cateId = "0";
        unit_number = "";
        floor_number = "";
        c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
        c_id_owner = "";
        ctype = 1;
        ctype_owner = 2;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
        cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
        ccompany = "";// บริษัทลูกค้า
        cphone = "";// เบอร์ติดต่อลูกค้า
        cline = "";// line ID ของลูกค้า
        cnote = "";// noteลูกค้า
        cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
        ccompany_owner = "";// บริษัทลูกค้า
        cphone_owner = "";// เบอร์ติดต่อลูกค้า
        cline_owner = "";// line ID ของลูกค้า
        cnote_owner = "";// noteลูกค้า
        other_type = "";
        shopID = "";
        image64_1 = "";
        image64_2 = "";
        image64_3 = "";
        image64_4 = "";
        image64_5 = "";
        image64_6 = "";
        catePosition = 0;
        phasePosition = 0;
        customerTypePosition = 0;
        ownerTypePosition = 0;
        maxPhase = 10;
        group_id = "";
        lat_lng  ="";//: project lat and lng [13.729194,100.622218]
        project_address ="" ;//: project location address
        project_name  ="";//: Project name [API will create if you did not post it]
        project_type_id ="" ;//: project type id [API will create if you did not post it  ;//: 1]
        phaseId ="0" ;//: phase id [API will create if you did not post it  ;//: 1]
        units  ="";//: How many unit [API will create if you did not post it  ;//: 1]
        unit_area ="" ;//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
        unit_budget  ="";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
        customer_name ="" ;//: Customer’s name
        customer_phone ="" ;//: Customer’s phone
        customer_email ="" ;//: Customer’s email
        customer_line ="" ;//: Customer’s line
        customer_tax_no ="" ;//: Customer’s tax no or ID card no.
        customer_note ="" ;//: Customer’s note
        house_owner_name ="" ;//: House owner name
        house_owner_phone ="" ;//: house_owner_phone
        house_owner_line ="" ;//: House owner line id
        House_owner_note ="" ;//: House owner note
        project_owner_name ="" ;//: Project owner name
        project_owner_phone=""  ;//: project_owner_phone
        project_owner_line ="" ;//: Project owner line
        project_owner_note ="" ;//: Project owner note
        customer_code ="" ; //: customer_code
        champ_customer_code =""; //: champ_customer_code
        customer_company ="";
        pic ="";//: array post with base64

    }

    @Override
    public void setEnviroment() {
        extras = getIntent().getExtras();
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
        AddCurrentLocationActivity.shopID = shopID;
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }

    public void setOnclick() {
        adLocal.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
        addDepartment.setOnClickListener(this);
        addCustomer.setOnClickListener(this);
        saleByphase.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
        postProject.setOnClickListener(this);
    }

    public void setVariavle() {
        Items projectDetailItems = projectDetail.getItems();
        shopID = projectDetailItems.getShopId();
        if (projectDetailItems.getLatLng().length() > 0) {
            String latlng[] = projectDetailItems.getLatLng().split(",");
            AddNewProjectActivity.DragLat = Double.parseDouble(latlng[0]);
            AddNewProjectActivity.DragLng = Double.parseDouble(latlng[1]);
        }

        AddNewProjectActivity.address = projectDetailItems.getProjectAddress();
        AddNewProjectActivity.cateId = projectDetailItems.getProjectTypeId();
        AddNewProjectActivity.phaseId = projectDetailItems.getPhaseId();
        AddNewProjectActivity.unit_number = projectDetailItems.getUnits();
        AddNewProjectActivity.floor_number = projectDetailItems.getProjectStories();
        AddNewProjectActivity.unit_area = projectDetailItems.getUnitArea();
        AddNewProjectActivity.unit_budget = projectDetailItems.getUnitBudget();
        AddNewProjectActivity.c_id = projectDetailItems.getUserId();
        AddNewProjectActivity.ctype = Integer.parseInt(projectDetailItems.getCustomerType());
        AddNewProjectActivity.cname = projectDetailItems.getCustomerName();
        AddNewProjectActivity.customer_code = projectDetailItems.getCustomerCode();
        AddNewProjectActivity.champ_customer_code = projectDetailItems.getChampCustomerCode();
        AddNewProjectActivity.customer_company = projectDetailItems.getCustomerCompany();
        AddNewProjectActivity.ccompany = "";
        AddNewProjectActivity.cphone = projectDetailItems.getCustomerPhone();
        AddNewProjectActivity.cline = projectDetailItems.getCustomerLine();
        AddNewProjectActivity.cnote = projectDetailItems.getCustomerNote();
        AddNewProjectActivity.c_id_owner = "3";
        AddNewProjectActivity.cname_owner = projectDetailItems.getHouseOwnerName();
        AddNewProjectActivity.ccompany_owner = "";
        AddNewProjectActivity.cphone_owner = projectDetailItems.getHouseOwnerPhone();
        AddNewProjectActivity.cline_owner = projectDetailItems.getHouseOwnerLine();
        AddNewProjectActivity.cnote_owner = projectDetailItems.getHouseOwnerNote();
        AddNewProjectActivity.project_name = projectDetailItems.getProjectName();
        AddNewProjectActivity.customer_tax_no = projectDetailItems.getCustomerTaxNo();
        AddNewProjectActivity.project_owner_name = projectDetailItems.getProjectOwnerName();
        AddNewProjectActivity.project_owner_phone =projectDetailItems.getProjectOwnerPhone();
        AddNewProjectActivity.project_owner_line = projectDetailItems.getProjectOwnerLine();
        AddNewProjectActivity.project_owner_note = projectDetailItems.getProjectOwnerNote();

    }


    public void initView() {
        mArrayList = new ArrayList<>();
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        adLocal = findViewById(R.id.ad_local);
        icoLocation = findViewById(R.id.ico_location);
        txtLocal = findViewById(R.id.txt_local);
        bgNum1 = findViewById(R.id.bg_num1);
        takePhoto = findViewById(R.id.take_photo);
        icoPhoto = findViewById(R.id.ico_photo);
        txtPhoto = findViewById(R.id.txt_photo);
        bgNum2 = findViewById(R.id.bg_num2);
        addDepartment = findViewById(R.id.add_department);
        icoDepart = findViewById(R.id.ico_depart);
        txtDepart = findViewById(R.id.txt_depart);
        bgNum3 = findViewById(R.id.bg_num3);
        addCustomer = findViewById(R.id.add_customer);
        icoCustomer = findViewById(R.id.ico_customer);
        txtCustomer = findViewById(R.id.txt_customer);
        bgNum4 = findViewById(R.id.bg_num4);
        postProject = findViewById(R.id.save);
        fake_layout = findViewById(R.id.fake_layout);
        saleByphase = findViewById(R.id.sale_byphase);
        icoSale = findViewById(R.id.ico_sale);
        txtSale = findViewById(R.id.txt_sale);
        bgNum5 = findViewById(R.id.bg_num5);
        txtPostProject = findViewById(R.id.txtPostProject);
        clearData = findViewById(R.id.clear_data);
        cardLocal = findViewById(R.id.card_local);
        cardTakepic = findViewById(R.id.card_takepic);
        cardDepartment = findViewById(R.id.card_department);
        cardCustomer = findViewById(R.id.card_customer);
        cardByphase = findViewById(R.id.card_byphase);
        ch1 = findViewById(R.id.ch1);
        ch2 = findViewById(R.id.ch2);
        ch3 = findViewById(R.id.ch3);
        ch4 = findViewById(R.id.ch4);
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    private Call<String> callProjectDetail() {
        return rudyService.projectDetail(
                Utils.APP_LANGUAGE,
                Utils.project_id
        );
    }

    private void getProjectDetail(){
        callProjectDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson2 = new Gson();
                String resp2 = response.body();
                if (resp2.contains("200")) {
                    projectDetail = gson2.fromJson(resp2, ProjectDetail.class);

                    mArrayList = new ArrayList<>();

                    ArrayList<GroupGalleryModel> groupGalleryModels = new ArrayList<>();

                    Map<String,ArrayList<GalleryModel>> groupImage = new HashMap<>();

                    for(GalleryModel p : projectDetail.getItems().getGalleryNew()){
                        if(!groupImage.containsKey(p.getCkId())){
                            groupImage.put(p.getCkId(), new ArrayList<>());
                        }
                        groupImage.get(p.getCkId()).add(p);
                    }

                    for (Map.Entry<String, ArrayList<GalleryModel>> map : groupImage.entrySet()){
                        groupGalleryModels.add(new GroupGalleryModel(map.getKey(), map.getValue()));
                    }

                    Collections.sort(groupGalleryModels, new Comparator<GroupGalleryModel>() {
                        @Override
                        public int compare(GroupGalleryModel o1, GroupGalleryModel o2) {
                            return o1.getmKey().compareTo(o2.getmKey());
                        }
                    });

                    mArrayList = groupGalleryModels;

                    titleName.setText(String.format(AddNewProjectActivity.this.getResources().getString(R.string.edit_data_parame), projectDetail.getItems().getProjectName()));
                    AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
                    txtPostProject.setText(getResources().getString(R.string.save));
                    setVariavle();
                    postProject.setOnClickListener(AddNewProjectActivity.this);
                } else {
                    APIError error = gson2.fromJson(resp2, APIError.class);
                    showToast(AddNewProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(AddNewProjectActivity.this, t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == reCode_TAKEPHOTO_ACTION_TWO) {

        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (Utils.project_id.length() > 0){
            getProjectDetail();
        }

        if (DragLat != 0.0 || DragLng != 0.0) {
            step1 = true;
        }

        if (bit1 != null || bit2 != null
                || bit3 != null || bit4 != null
                || bit5 != null) {
            step2 = true;
        }

        if (!MainActivity.data1) {
            icoLocation.setImageDrawable(this.getDrawable(R.drawable.icons_8_address));
            cardLocal.setCardBackgroundColor(getResources().getColor(R.color.white));
            ch1.setVisibility(View.GONE);
            txtLocal.setTextColor(getResources().getColor(R.color.txt_color_login));
            bgNum1.setCardBackgroundColor(getResources().getColor(R.color.white));
        }
        if (MainActivity.data1) {
            icoLocation.setImageDrawable(this.getDrawable(R.drawable.icons_8_address_white));
//            adLocal.setBackground();
            cardLocal.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            ch1.setVisibility(View.VISIBLE);

            txtLocal.setTextColor(getResources().getColor(R.color.white));
            bgNum1.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);

        }

        if (MainActivity.data2) {
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewProjectActivity.icoPhoto.setImageDrawable(getDrawable(R.drawable.shape_white));
            AddNewProjectActivity.cardTakepic.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.ch2.setVisibility(View.VISIBLE);
            AddNewProjectActivity.txtPhoto.setTextColor(getResources().getColor(R.color.white));
            AddNewProjectActivity.bgNum2.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
        }
        if (!MainActivity.data2) {
            AddNewProjectActivity.icoPhoto.setImageDrawable(getDrawable(R.drawable.shape));
            AddNewProjectActivity.cardTakepic.setCardBackgroundColor(getResources().getColor(R.color.white));
            AddNewProjectActivity.ch2.setVisibility(View.GONE);
            AddNewProjectActivity.txtPhoto.setTextColor(getResources().getColor(R.color.txt_color_login));
            AddNewProjectActivity.bgNum2.setCardBackgroundColor(getResources().getColor(R.color.white));
            AddNewProjectActivity.postProject.setVisibility(View.GONE);
            AddNewProjectActivity.fake_layout.setVisibility(View.VISIBLE);
        }


        if (MainActivity.data3) {
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
            AddNewProjectActivity.icoDepart.setImageDrawable(getDrawable(R.drawable.icons_8_real_estate_white));
            AddNewProjectActivity.cardDepartment.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.ch3.setVisibility(View.VISIBLE);
            AddNewProjectActivity.txtDepart.setTextColor(getResources().getColor(R.color.white));
            AddNewProjectActivity.bgNum3.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);

        }

        if (MainActivity.data4) {
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);

            AddNewProjectActivity.icoCustomer.setImageDrawable(getDrawable(R.drawable.icons_8_permanent_job_white));
            AddNewProjectActivity.cardCustomer.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.ch4.setVisibility(View.VISIBLE);
            AddNewProjectActivity.txtCustomer.setTextColor(getResources().getColor(R.color.white));
            AddNewProjectActivity.bgNum4.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
        }

        if (!MainActivity.data5) {
            icoSale.setImageDrawable(this.getDrawable(R.drawable.icons_8_briefcase));
            cardByphase.setCardBackgroundColor(getResources().getColor(R.color.white));
            txtSale.setTextColor(getResources().getColor(R.color.txt_color_login));
            bgNum5.setCardBackgroundColor(getResources().getColor(R.color.white));
        }

        if (MainActivity.data5) {
            icoSale.setImageDrawable(this.getDrawable(R.drawable.icons_8_briefcase_white));
            cardByphase.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            txtSale.setTextColor(getResources().getColor(R.color.white));
            bgNum5.setCardBackgroundColor(getResources().getColor(R.color.color_text_select));
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
        }
        if (Utils.getPrefer(this, "QC").length() > 0) {
            AddNewProjectActivity.postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
            AddNewProjectActivity.postProject.setVisibility(View.VISIBLE);
            AddNewProjectActivity.fake_layout.setVisibility(View.GONE);
        }

    }


    @Override
    public void onClick(View v) {
        if (v == adLocal) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
                if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_CODE_ASK_PERMISSIONS);
                    return;
                }
                if (Utils.project_id.length() > 0) {
                    Intent intent = new Intent(this, AddCurrentLocationForAddNewProjectActivity.class);
                    AddCurrentLocationForAddNewProjectActivity.isEdit = true;
                    AddCurrentLocationForAddNewProjectActivity.editlatlng = projectDetail.getItems().getLatLng();
                    AddCurrentLocationForAddNewProjectActivity.shopID = projectDetail.getItems().getId();
                    startActivity(intent);
                } else {
                    AddCurrentLocationForAddNewProjectActivity.isEdit = false;
                    startActivity(new Intent(this, AddCurrentLocationForAddNewProjectActivity.class));
                }
            } else {
                if (Utils.project_id.length() > 0) {
                    Intent intent = new Intent(this, AddCurrentLocationForAddNewProjectActivity.class);
                    AddCurrentLocationForAddNewProjectActivity.isEdit = true;
                    AddCurrentLocationForAddNewProjectActivity.editlatlng = projectDetail.getItems().getLatLng();
                    AddCurrentLocationForAddNewProjectActivity.shopID = projectDetail.getItems().getId();
                    startActivity(intent);
                } else {
                    AddCurrentLocationForAddNewProjectActivity.isEdit = false;
                    startActivity(new Intent(this, AddCurrentLocationForAddNewProjectActivity.class));
                }
            }
        }

        if (v == takePhoto) {
            Intent intent = new Intent(AddNewProjectActivity.this, TakePhotoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("listImage", mArrayList);
            intent.putExtras(bundle);
            startActivityForResult(intent, reCode_TAKEPHOTO_ACTION_TWO);
        }

        if (v == addCustomer) {
            Intent intent = new Intent(this, CustomerDataActivity.class);
            intent.putExtra("customer_name", customer_name);
            intent.putExtra("customer_phone", customer_phone);
            intent.putExtra("customer_note", customer_note);
            intent.putExtra("customer_line", customer_line);
            intent.putExtra("tax_number", customer_tax_no);
            intent.putExtra("customer_code", customer_code);
            intent.putExtra("champ_customer_code", champ_customer_code);
            intent.putExtra("customer_company", customer_company);

            intent.putExtra("project_owner_name", project_owner_name);
            intent.putExtra("project_owner_phone", project_owner_phone);
            intent.putExtra("project_owner_line", project_owner_line);
            intent.putExtra("project_owner_note", project_owner_note);
            intent.putExtra("projectId", Utils.project_id);
            intent.putExtra("ctype",String.valueOf(ctype));
            intent.putExtra("ctype_owner", String.valueOf(ctype_owner));


            startActivity(intent);
        }

        if (v == clsBtn) {
            Utils.savePrefer(this, "print", "");
            finish();
        }

        if (v == postProject) {
            Utils.savePrefer(this, "print", "");
            if (projectDetail == null) {


                    if (!Utils.isNetworkConnected(AddNewProjectActivity.this)){
                        SaveDataNewProject();
                    }else{
                        postNewProject(0);
                    }

            } else {
                Toast.makeText(this, getResources().getString(R.string.save_success), Toast.LENGTH_LONG);
                finish();
            }

        }

        if (Utils.isNetworkConnected(AddNewProjectActivity.this)){

            if (v == saleByphase) {
                Utils.savePrefer(this, "QC", "1");
                Utils.savePrefer(this, "print", "");
                if (projectDetail != null) {
                    if(projectDetail.getItems().getProjectTypeId().equalsIgnoreCase("11")){
                        Intent intent = new Intent(AddNewProjectActivity.this, DealByProjectActivity.class);
                        intent.putExtra("projectId", projectDetail.getItems().getId());
                        intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(this, DealByPhaseActivity.class);
                        intent.putExtra("phase",projectDetail.getItems().getPhaseId());
                        intent.putExtra("projectId", projectDetail.getItems().getId());
                        intent.putExtra("projectTypeId",projectDetail.getItems().getProjectTypeId());
                        intent.putExtra("transaction_id","");
                        startActivity(intent);
                    }
                } else {
                    postNewProject(1);
                }
            }

            if (v == addDepartment) {
                Intent intent = new Intent(this, DepartmentDataActivity.class);
                intent.putExtra("unit_number", unit_number);
                intent.putExtra("floor_number", floor_number);
                intent.putExtra("unit_area", unit_area);
                intent.putExtra("unit_budget", unit_budget);
                intent.putExtra("project_name", project_name);
                intent.putExtra("cateId", cateId);
                intent.putExtra("phaseId", phaseId);
                intent.putExtra("catePosition", String.valueOf(catePosition));
                intent.putExtra("phasePosition", String.valueOf(phasePosition));
                startActivity(intent);
            }
        }else{
            if (v == saleByphase){
                showToast(this, getResources().getString(R.string.alert_text_network_not_connect));
            }
            if (v == addDepartment){
                showToast(this, getResources().getString(R.string.alert_text_network_not_connect));
            }
        }
    }

    private void SaveDataNewProject(){

        MaterialDialog.Builder builder = new MaterialDialog.Builder(AddNewProjectActivity.this)
                .title("แจ้งเตือน")
                .content("เนื่องจาก ไม่มีการเชื่อมต่อ internet จากโทรศัพท์ของท่าน ระบบจึงทำการบันทึกข้อมูลทั้งหมดไว้ก่อนและจะทำการสร้างหน่วยงานให้ท่านโดยอัตโนมัติเมื่อท่านเชื่อมต่อ internet อีกครั้ง")
                .positiveText("รับทราบ")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Gson gson = new Gson();
                        String json = gson.toJson(storageDataOfflineModel);
                        Utils.writeToFileDataNewProject(json, AddNewProjectActivity.this);
                        finish();
                    }
                });
        MaterialDialog del_dialog = builder.build();
        del_dialog.show();
    }


    private Call<String> callNewProject(List<String> _pic, int state) {
        return rudyService.addNewProject(
                Utils.APP_LANGUAGE,
                _pic,
                user_id,
                project_name,
                project_address,
                lat_lng,
                cateId,
                phaseId,
                unit_number.equalsIgnoreCase("") ? "0":unit_number,
                unit_area.equalsIgnoreCase("") ? "0":unit_area,
                unit_budget.equalsIgnoreCase("")? "0":unit_budget,
                floor_number.equalsIgnoreCase("") ? "0":floor_number,
                ctype+"",
                customer_name,
                customer_code,
                champ_customer_code,
                customer_company,
                customer_phone,
                customer_email,
                customer_line,
                customer_tax_no,
                customer_note,
                ctype_owner == 2 ? house_owner_name:"",
                ctype_owner == 2 ? house_owner_phone:"",
                ctype_owner == 2 ? house_owner_line:"",
                ctype_owner == 2 ? House_owner_note:"",
                ctype_owner == 2 ? "":project_owner_name,
                ctype_owner == 2 ? "":project_owner_phone,
                ctype_owner == 2 ? "":project_owner_line,
                ctype_owner == 2 ? "":project_owner_note,
                "");
    }

    @SuppressLint("StaticFieldLeak")
    private void postNewProject(int state){
        showLoading(AddNewProjectActivity.this);
        if (!unit_budget.equalsIgnoreCase("")) {
            unit_budget_number = Double.parseDouble(unit_budget);
            unit_budget_number = unit_budget_number * 1000000;
        }

        if (c_id.equalsIgnoreCase("0")) {
            c_id = "";
        }

        if (c_id_owner.equalsIgnoreCase("0")) {
            c_id_owner = "";
        }

        ArrayList<String> listPic = new ArrayList<>();
        for (int i = 0; i < mArrayList.size(); i++) {
            for (int j = 0; j < mArrayList.get(i).getmValue().size() ; j++) {
                File f = new File(mArrayList.get(i).getmValue().get(j).getName());
                Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                listPic.add("data:image/jpg;base64," + Utils.BitMapToString(d));
            }
        }


        callNewProject(listPic, state).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                postProject.setOnClickListener(AddNewProjectActivity.this);
                if (resp.contains("200")) {
                    if (state == 1) {
                        postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
                        postProject.setVisibility(View.VISIBLE);
                        fake_layout.setVisibility(View.GONE);

                        setCustomTag("Quick Sale");
                        projectDetail = gson.fromJson(resp, ProjectDetail.class);
                        Utils.pdfName = projectDetail.getItems().getProjectName();
                        try {

                            JSONObject jsonObject = new JSONObject(resp);
                            JSONObject itmObject = jsonObject.getJSONObject("items");
                            if (projectDetail.getItems().getProjectTypeId().equalsIgnoreCase("11")) {

                                DealByPhaseActivity.QUICKSALE = "1";
                                Utils.savePrefer(AddNewProjectActivity.this, "cart", "");
                                Utils.project_id = itmObject.optString("id");
                                TransactionDetailActivity.detailItem = projectDetail.getItems();

                                Intent intent = new Intent(AddNewProjectActivity.this, DealByProjectActivity.class);
                                intent.putExtra("projectId", projectDetail.getItems().getId());
                                intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
                                startActivity(intent);
                            } else {

                                DealByPhaseActivity.QUICKSALE = "1";
                                Utils.savePrefer(AddNewProjectActivity.this, "cart", "");
                                Utils.project_id = itmObject.optString("id");
                                TransactionDetailActivity.detailItem = projectDetail.getItems();
                                Intent intent = new Intent(AddNewProjectActivity.this, DealByPhaseActivity.class);
                                intent.putExtra("phase", projectDetail.getItems().getPhaseId());
                                intent.putExtra("projectId", projectDetail.getItems().getId());
                                intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
                                intent.putExtra("transaction_id", "");
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            LogException(resp ,e);


                        }
                        showToast(AddNewProjectActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
                    } else {
                        setCustomTag("Create Project");
                        try {
                            JSONObject jsonObject = new JSONObject(resp);
                            JSONObject itmObject = jsonObject.getJSONObject("items");
                            Utils.project_id = itmObject.optString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            LogException(resp ,e);
                        }
                        showToast(AddNewProjectActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
                        clsBtn.performClick();
                    }
                    AddNewProjectActivity.DragLat = 0.0;
                    AddNewProjectActivity.DragLng = 0.0;
                    AddNewProjectActivity.bit1 = null;
                    AddNewProjectActivity.bit2 = null;
                    AddNewProjectActivity.bit3 = null;
                    AddNewProjectActivity.bit4 = null;
                    AddNewProjectActivity.bit5 = null;
                    AddNewProjectActivity.bit6 = null;

                } else {
                    try {
                        JSONObject object = new JSONObject(resp);
//                        showToast(AddNewProjectActivity.this, "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง");
                        showToast(AddNewProjectActivity.this, object.optString("items",resp));



                    } catch (JSONException e) {
                        showToast(AddNewProjectActivity.this, resp);
                        e.printStackTrace();
                        LogException(resp ,e);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(AddNewProjectActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void postNewProject(final String url, final int state) {
//        showLoading(this);
//        ArrayList<String> picList = new ArrayList<>();
//        if (!unit_budget.equalsIgnoreCase("")) {
//            unit_budget_number = Double.parseDouble(unit_budget);
//            unit_budget_number = unit_budget_number * 1000000;
//        }
//
//        if (c_id.equalsIgnoreCase("0")) {
//            c_id = "";
//        }
//
//        if (c_id_owner.equalsIgnoreCase("0")) {
//            c_id_owner = "";
//        }
//
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        try {
//
//            for (int i = 0; i < mArrayList.size(); i++) {
//                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
//                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(mArrayList.get(i)));
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        if(units.equalsIgnoreCase("")){
//            unit_number = "0";
//        }
//        if(unit_area.equalsIgnoreCase("")){
//            unit_area = "0";
//        }
//        if(unit_budget.equalsIgnoreCase("")){
//            unit_budget = "0";
//        }
//        if(floor_number.equalsIgnoreCase("")){
//            floor_number = "0";
//        }
//
//
//
//
//
//        if(ctype_owner ==2){
//            formData.add("user_id", user_id)
//                    .add("project_name", project_name)
//                    .add("project_address", project_address)
//                    .add("lat_lng", lat_lng)
//                    .add("project_type_id", cateId)
//                    .add("phase_id", phaseId)
//                    .add("units", unit_number)
//                    .add("unit_area", unit_area)
//                    .add("unit_budget", unit_budget)
//                    .add("project_stories", floor_number)
//                    .add("customer_type", ctype+"")
//                    .add("customer_name", customer_name)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_company", customer_company)
//                    .add("customer_phone", customer_phone)
//                    .add("customer_email", customer_email)
//                    .add("customer_line", customer_line)
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_note", customer_note)
//                    .add("house_owner_name", house_owner_name)
//                    .add("house_owner_phone", house_owner_phone)
//                    .add("house_owner_line", house_owner_line)
//                    .add("House_owner_note", House_owner_note)
//                    .add("project_owner_name", "")
//                    .add("project_owner_phone", "")
//                    .add("project_owner_line", "")
//                    .add("project_owner_note", "");
//        } else {
//            formData.add("user_id", user_id)
//                    .add("project_name", project_name)
//                    .add("project_address", project_address)
//                    .add("lat_lng", lat_lng)
//                    .add("project_type_id", cateId)
//                    .add("phase_id", phaseId)
//                    .add("units", unit_number)
//                    .add("unit_area", unit_area)
//                    .add("unit_budget", unit_budget)
//                    .add("project_stories", floor_number)
//                    .add("customer_type", ctype+"")
//                    .add("customer_name", customer_name)
//                    .add("customer_phone", customer_phone)
//                    .add("customer_email", customer_email)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_company", customer_company)
//                    .add("customer_line", customer_line)
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_note", customer_note)
//                    .add("house_owner_name", "")
//                    .add("house_owner_phone", "")
//                    .add("house_owner_line", "")
//                    .add("House_owner_note", "")
//                    .add("project_owner_name", project_owner_name)
//                    .add("project_owner_phone", project_owner_phone)
//                    .add("project_owner_line", project_owner_line)
//                    .add("project_owner_note", project_owner_note);
//        }
//
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url, requestBody);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//               hideLoading();
//                postProject.setOnClickListener(AddNewProjectActivity.this);
//                if (string.contains("200")) {
//                    if (state == 1) {
//                        postProject.setBackground(getResources().getDrawable(R.drawable.bg_rounder_number_save));
//                        postProject.setVisibility(View.VISIBLE);
//                        fake_layout.setVisibility(View.GONE);
//
//                        setCustomTag("Quick Sale");
//                        // //setAppSeeTag("Quick Sale");
//                        Gson gson = new Gson();
//                        projectDetail = gson.fromJson(string, ProjectDetail.class);
//                        Utils.pdfName = projectDetail.getItems().getProjectName();
//                        try {
//
//                            JSONObject jsonObject = new JSONObject(string);
//                            JSONObject itmObject = jsonObject.getJSONObject("items");
//                            if (projectDetail.getItems().getProjectTypeId().equalsIgnoreCase("11")) {
//
//                                DealByPhaseActivity.QUICKSALE = "1";
//                                Utils.savePrefer(AddNewProjectActivity.this, "cart", "");
//                                Utils.project_id = itmObject.optString("id");
//                                TransactionDetailActivity.detailItem = projectDetail.getItems();
//
//                                Intent intent = new Intent(AddNewProjectActivity.this, DealByProjectActivity.class);
//                                intent.putExtra("projectId", projectDetail.getItems().getId());
//                                intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
//                                startActivity(intent);
//                            } else {
//
//                                DealByPhaseActivity.QUICKSALE = "1";
//                                Utils.savePrefer(AddNewProjectActivity.this, "cart", "");
//                                Utils.project_id = itmObject.optString("id");
//                                TransactionDetailActivity.detailItem = projectDetail.getItems();
//                                Intent intent = new Intent(AddNewProjectActivity.this, DealByPhaseActivity.class);
//                                intent.putExtra("phase", projectDetail.getItems().getPhaseId());
//                                intent.putExtra("projectId", projectDetail.getItems().getId());
//                                intent.putExtra("projectTypeId", projectDetail.getItems().getProjectTypeId());
//                                intent.putExtra("transaction_id", "");
//                                startActivity(intent);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            LogException(url ,e);
//
//
//                        }
//                        showToast(AddNewProjectActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
//                    } else {
//                        setCustomTag("Create Project");
//                        try {
//                            JSONObject jsonObject = new JSONObject(string);
//                            JSONObject itmObject = jsonObject.getJSONObject("items");
//                            Utils.project_id = itmObject.optString("id");
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            LogException(url ,e);
//                        }
//                        showToast(AddNewProjectActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
//                        clsBtn.performClick();
//                    }
//                    AddNewProjectActivity.DragLat = 0.0;
//                    AddNewProjectActivity.DragLng = 0.0;
//                    AddNewProjectActivity.bit1 = null;
//                    AddNewProjectActivity.bit2 = null;
//                    AddNewProjectActivity.bit3 = null;
//                    AddNewProjectActivity.bit4 = null;
//                    AddNewProjectActivity.bit5 = null;
//                    AddNewProjectActivity.bit6 = null;
//
//                } else {
//                    try {
//                        JSONObject object = new JSONObject(string);
////                        showToast(AddNewProjectActivity.this, "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง");
//                        showToast(AddNewProjectActivity.this, object.optString("items",string));
//
//
//
//                    } catch (JSONException e) {
//                        showToast(AddNewProjectActivity.this, string);
//                        e.printStackTrace();
//                        LogException(url ,e);
//                    }
//                }
//
//            }
//        }.execute();
//    }



    public boolean isNetworkConnected() {
        return false;
    }

}
