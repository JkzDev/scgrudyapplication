package com.scg.rudy.ui.calendar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.calendar.CalendarPojo;
import com.scg.rudy.model.pojo.calendar.ItemsItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.calendar_detail.CalendarDetailActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CalendarActivity extends BaseActivity implements View.OnClickListener {
    private  final String DAY_OF_THE_WEEK_TEXT = "dayOfTheWeekText";
    private  final String DAY_OF_THE_WEEK_LAYOUT = "dayOfTheWeekLayout";
    private  final String DAY_OF_THE_MONTH_LAYOUT = "dayOfTheMonthLayout";
    private  final String DAY_OF_THE_MONTH_TEXT = "dayOfTheMonthText";
    private  final String DAY_OF_THE_MONTH_BACKGROUND = "dayOfTheMonthBackground";
    private  final String DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 = "dayOfTheMonthCircleImage1";
    private  final String DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 = "dayOfTheMonthCircleImage2";
    private  final String DAY_OF_THE_MONTH_CIRCLE_IMAGE_3 = "dayOfTheMonthCircleImage3";
    private ViewGroup rudyCalendarMonthLayout;
    private  Calendar lastSelectedDayCalendar;
    private int  month_index = 0;
    private  Calendar currentCalendar;
    private View rootView;
    private  ViewGroup monthBgColor = null;
    private  TextView txtLastSelectColor = null;
    private  Calendar lastSelectCalendar = null;
    private  int lastSelectPosition ;
    private  ViewGroup CurrentmonthBgColor = null;
    private  TextView CurrenttxtColor = null;
    private  ViewGroup select_monthBgColor = null;
    private  TextView select_txtLastSelectColor = null;
    private  Calendar select_lastSelectCalendar = null;
    private  int select_lastSelectPosition ;
    private  ViewGroup select_CurrentmonthBgColor = null;
    private  TextView select_CurrenttxtColor = null;
    private  final Calendar FixCalendar = Calendar.getInstance();
    private ImageView clsBtn;
    private Spinner monthSpinner;
    private RudyService rudyService;
    public String yearTxt,monthTxt;
    private List<ItemsItem> dateLtems;
    private TextView day;
    private TextView date;
    private TextView month;
    private TextView year;
    private RelativeLayout addNew;
    private ArrayList<String> monthList;
    private CalendarPojo _calendarPojo;
    private LinearLayout addCalendar;
    private LayoutInflater inflate;
    private View calendarViewAdd;
    private int realDate;
    private int month_select;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        initView();
        monthList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.type_month)));
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE,dd,MMMM,yyyy", new Locale(Utils.APP_LANGUAGE, Utils.APP_LANGUAGE.toUpperCase()));
        String[] timeToday = formatter.format(new Date()).split(",");

        Calendar currentCalendar = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MM-yyyy");
        Date currentTime = Calendar.getInstance().getTime();
        String dateFormat = simpledateformat.format(currentTime.getTime());
        final String[] time_splite = dateFormat.split("-");
        monthTxt = time_splite[0];
        yearTxt = time_splite[1];

        setOnclick();

        CustomSpinner monthAdapter = new CustomSpinner(this, monthList);
        monthSpinner.setAdapter(monthAdapter);
        monthSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        monthSpinner.setSelection(Integer.parseInt(monthTxt)-1);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                monthTxt = String.valueOf(i+1);
                loadCalendar();
//                showToast(CalendarActivity.this,monthTxt);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        day.setText(timeToday[0].replace("วัน",""));
        date.setText(timeToday[1]);
        month.setText(timeToday[2]);
        int year_af = Integer.parseInt(timeToday[3]);
        if(year_af<2500){
            year_af = year_af+543;
        }
        year.setText(String.valueOf(year_af));
    }

    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }



    private Call<String> callCalendarApi() {
//        Test data
//        user_id = "2";
        return rudyService.getCalendar(
                Utils.APP_LANGUAGE,
                user_id,
                yearTxt,
                monthTxt
        );
    }

    private void loadCalendar() {
        showLoading(this);
        callCalendarApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    CalendarPojo Obj  = gson.fromJson(resp, CalendarPojo.class);
                    if(monthBgColor!=null){
                        monthBgColor.setBackgroundResource(android.R.color.transparent);
                        txtLastSelectColor.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
                    }
                    monthBgColor = null;
                    txtLastSelectColor = null;
                    List<ItemsItem> calendarResults =   customFetchResults(Obj);
                    setCalendarData(calendarResults);
                }else{
                    dateLtems = null;
                    setCalendarData(dateLtems);
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CalendarActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                dateLtems = null;
                setCalendarData(dateLtems);
                t.printStackTrace();
                showToast(CalendarActivity.this, t.getMessage());
            }
        });
    }

    private void setCalendarData( List<ItemsItem> calendarResults){
        dateLtems = calendarResults;
        setIdCalendar(calendarResults);


//        showToast(this,calendarResults.get(0).getDate());
    }


    private List<ItemsItem> fetchResults(Response<CalendarPojo> response) {
        CalendarPojo calendarPojo = response.body();
        _calendarPojo = calendarPojo;
        return calendarPojo.getItems();
    }

    private List<ItemsItem> customFetchResults(CalendarPojo calendarPojo) {
        _calendarPojo = calendarPojo;
        return calendarPojo.getItems();
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }
    

    private void setOnclick(){
        clsBtn.setOnClickListener(this);
    }

    private void  setIdCalendar( List<ItemsItem> calendarResults){
            final View calendarViewAdd = LayoutInflater.from(this).inflate(R.layout.rudy_calendar_week_layout, null);
            rootView = calendarViewAdd.findViewById(R.id.daysContainer);
            addCalendar.removeAllViews();
            addCalendar.addView(calendarViewAdd);

        for (int i = 0; i < 42; i++) {
            int weekIndex = (i % 7) + 1;
            ViewGroup dayOfTheWeekLayout =  rootView.findViewWithTag(DAY_OF_THE_WEEK_LAYOUT + weekIndex);
            View dayOfTheMonthLayout = LayoutInflater.from(this).inflate(R.layout.rudy_calendar_day_of_the_month_layout, null);
            View dayOfTheMonthText = dayOfTheMonthLayout.findViewWithTag(DAY_OF_THE_MONTH_TEXT);
            View dayOfTheMonthBackground = dayOfTheMonthLayout.findViewWithTag(DAY_OF_THE_MONTH_BACKGROUND);
            View dayOfTheMonthCircleImage1 = dayOfTheMonthLayout.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1);
            View dayOfTheMonthCircleImage2 = dayOfTheMonthLayout.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2);
            View dayOfTheMonthCircleImage3 = dayOfTheMonthLayout.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_3);
            int viewIndex = i + 1;
            dayOfTheMonthLayout.setTag(DAY_OF_THE_MONTH_LAYOUT + viewIndex);
            dayOfTheMonthText.setTag(DAY_OF_THE_MONTH_TEXT + viewIndex);
            dayOfTheMonthBackground.setTag(DAY_OF_THE_MONTH_BACKGROUND + viewIndex);
            dayOfTheMonthCircleImage1.setTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 + viewIndex);
            dayOfTheMonthCircleImage2.setTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 + viewIndex);
            dayOfTheMonthCircleImage3.setTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_3 + viewIndex);
            dayOfTheWeekLayout.removeView(dayOfTheMonthLayout);
            dayOfTheWeekLayout.addView(dayOfTheMonthLayout);
        }
        setUpCalligraphy(calendarResults);
    }

    private void setUpCalligraphy( List<ItemsItem> calendarResults) {
        // Initialize calendar for current month
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR));
        currentCalendar.set(Calendar.MONTH, Integer.parseInt(monthTxt)-1);



        setCalendar(currentCalendar,calendarResults);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void setCalendar(Calendar calendar, List<ItemsItem> calendarResults) {
        currentCalendar = calendar;
        updateView(calendar,calendarResults);
    }

    private void updateView(Calendar calendar,List<ItemsItem> calendarResults) {

        this.dateLtems = calendarResults;
//        setUpMonthLayout();
        setUpWeekDaysLayout();
        setUpDaysOfMonthLayout();
        setUpDaysInCalendar();
        markDayAsCurrentDay(calendar);
    }

    private void setUpWeekDaysLayout() {
        TextView dayOfWeek;
        String dayOfTheWeekString;
        String[] weekDaysArray = new DateFormatSymbols(new Locale(Utils.APP_LANGUAGE.toUpperCase())).getWeekdays();
        for (int i = 1; i < weekDaysArray.length; i++) {
            dayOfWeek = rootView.findViewWithTag(DAY_OF_THE_WEEK_TEXT + getWeekIndex(i, currentCalendar));
            dayOfTheWeekString = weekDaysArray[i];
            dayOfTheWeekString = checkSpecificLocales(dayOfTheWeekString, i);
//            dayOfWeek.setText(dayOfTheWeekString);
            dayOfWeek.setTextColor(getResources().getColor(R.color.french_blue));
            dayOfWeek.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.rudy_calendar_month_font));
        }
    }

    private void setUpDaysOfMonthLayout() {
        TextView dayOfTheMonthText;
        View circleImage1;
        View circleImage2;
        View circleImage3;
        ViewGroup dayOfTheMonthContainer;
        ViewGroup dayOfTheMonthBackground;

        for (int i = 1; i < 43; i++) {
            dayOfTheMonthContainer = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + i);
            dayOfTheMonthBackground = rootView.findViewWithTag(DAY_OF_THE_MONTH_BACKGROUND + i);
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + i);
            circleImage1 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 + i);
            circleImage2 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 + i);
            circleImage3 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_3 + i);
            dayOfTheMonthText.setVisibility(INVISIBLE);

//            Set task visible
            circleImage1.setVisibility(INVISIBLE);
            circleImage2.setVisibility(INVISIBLE);
            circleImage3.setVisibility(INVISIBLE);
            dayOfTheMonthText.setBackgroundResource(android.R.color.transparent);
            dayOfTheMonthText.setTypeface(null, Typeface.NORMAL);
            dayOfTheMonthText.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
            dayOfTheMonthText.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.rudy_calendar_day_of_week_font));
            dayOfTheMonthContainer.setBackgroundResource(android.R.color.transparent);
            dayOfTheMonthContainer.setOnClickListener(null);
            dayOfTheMonthBackground.setBackgroundResource(android.R.color.transparent);
        }
    }

    private void setUpDaysInCalendar() {
        Calendar auxCalendar = Calendar.getInstance(new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        auxCalendar.set(Calendar.MONTH, Integer.parseInt(monthTxt));


        Calendar beforeCalendar = Calendar.getInstance(new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        beforeCalendar.set(Calendar.MONTH, Integer.parseInt(monthTxt)-2);

        auxCalendar.setTime(currentCalendar.getTime());
        auxCalendar.set(Calendar.DAY_OF_MONTH, 1);

        int maxLastMonth = beforeCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDayOfMonth = auxCalendar.get(Calendar.DAY_OF_WEEK);

        int lastDayOfMonth = auxCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        TextView dayOfTheMonthText;
        ViewGroup dayOfTheMonthContainer;
        ViewGroup dayOfTheMonthLayout;
        View circleImage1;
        View circleImage2;
        View circleImage3;

        int dayOfTheMonthIndex = getWeekIndex(firstDayOfMonth, auxCalendar);
        int tagNextMonth = dayOfTheMonthIndex;

        for (int i = 1; i <= auxCalendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++, dayOfTheMonthIndex++) {
            tagNextMonth = dayOfTheMonthIndex;
            dayOfTheMonthContainer = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + dayOfTheMonthIndex);
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + dayOfTheMonthIndex);
            circleImage1 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1 + dayOfTheMonthIndex);
            circleImage2 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2 + dayOfTheMonthIndex);
            circleImage3 = rootView.findViewWithTag(DAY_OF_THE_MONTH_CIRCLE_IMAGE_3 + dayOfTheMonthIndex);

            if (dayOfTheMonthText == null) {
                break;
            }
            dayOfTheMonthContainer.setOnClickListener(onDayOfMonthClickListener);
            dayOfTheMonthText.setVisibility(VISIBLE);
            if(dateLtems!=null){
                for (int j = 0; j < dateLtems.size(); j++) {
                    if(String.valueOf(i).equalsIgnoreCase(dateLtems.get(j).getDay())){
                        String[] type = dateLtems.get(j).getType().split(",");
                        for ( String s:  type ) {
                            if(s.equalsIgnoreCase("1")){
                                circleImage1.setVisibility(VISIBLE);
                            }else if(s.equalsIgnoreCase("2")){
                                circleImage2.setVisibility(VISIBLE);
                            }else{
                                circleImage3.setVisibility(VISIBLE);
                            }
                        }
                    }
                }
            }

//                set color < not today
                Calendar nowCalendar = Calendar.getInstance();
                nowCalendar.set(Calendar.MONTH, Integer.parseInt(monthTxt));
                if(i < nowCalendar.getTime().getDate()){
                    dayOfTheMonthText.setText(String.valueOf(i));
                    dayOfTheMonthText.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
                }
                else{
                    dayOfTheMonthText.setText(String.valueOf(i));
                    dayOfTheMonthText.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
                }

            dayOfTheMonthText.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.rudy_calendar_day_of_week_font));
        }

        for (int i = 1; i < firstDayOfMonth; i++) {
            dayOfTheMonthContainer = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + i);
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + i);
            dayOfTheMonthText.setVisibility(VISIBLE);
            dayOfTheMonthText.setTextColor(getResources().getColor(R.color.french_gray));
            dayOfTheMonthText.setText(String.valueOf(maxLastMonth-(firstDayOfMonth-(i+1))));
            dayOfTheMonthContainer.setOnClickListener(null);
        }

        int nextMont = 1;
//        showMessage(tagNextMonth+1);
        tagNextMonth = tagNextMonth+1;


        for (int i = tagNextMonth; i < 36; i++) {
            dayOfTheMonthContainer = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + i);
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + i);
            dayOfTheMonthText.setVisibility(VISIBLE);
            dayOfTheMonthText.setTextColor(getResources().getColor(R.color.french_gray));
            dayOfTheMonthText.setText(String.valueOf(nextMont));
            dayOfTheMonthContainer.setOnClickListener(null);
            nextMont++;
        }

        for (int i = 36; i < 43; i++) {
            dayOfTheMonthText = rootView.findViewWithTag(DAY_OF_THE_MONTH_TEXT + i);
            dayOfTheMonthLayout = rootView.findViewWithTag(DAY_OF_THE_MONTH_LAYOUT + i);
            if (dayOfTheMonthText.getVisibility() == INVISIBLE) {
                dayOfTheMonthLayout.setVisibility(GONE);
            } else {
                dayOfTheMonthLayout.setVisibility(VISIBLE);
            }
        }
    }

    private void markDayAsCurrentDay(Calendar calendar) {
        // If it's the current month, mark current day
        if(calendar.get(Calendar.MONTH) != FixCalendar.get(Calendar.MONTH)){

            ViewGroup dayOfTheMonthBackground = getDayOfMonthBackground(calendar);
            TextView dayOfTheMonth = getDayOfMonthText(calendar);

            CurrentmonthBgColor  = dayOfTheMonthBackground;
            CurrenttxtColor =  dayOfTheMonth;

            CurrentmonthBgColor.setBackgroundResource(android.R.color.transparent);
            CurrenttxtColor.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));

            lastSelectedDayCalendar = calendar;
            lastSelectCalendar  = calendar;
        }else{
            ViewGroup dayOfTheMonthBackground = getDayOfMonthBackground(FixCalendar);
            TextView dayOfTheMonth = getDayOfMonthText(FixCalendar);

            CurrentmonthBgColor  = dayOfTheMonthBackground;
            CurrenttxtColor =  dayOfTheMonth;

            CurrentmonthBgColor.setBackgroundResource(R.drawable.circle_calendar);
            CurrenttxtColor.setTextColor(getResources().getColor(R.color.rudy_calendar_selected_day_font));

            lastSelectedDayCalendar = FixCalendar;
            lastSelectCalendar  = FixCalendar;
        }




    }

    private void markDayAsSelectedDay(Calendar calendar) {
        clearSelectedDay(calendar);
        lastSelectCalendar = calendar;

        ViewGroup dayOfTheMonthBackground = getDayOfMonthBackground(calendar);
        TextView dayOfTheMonth = getDayOfMonthText(calendar);

        monthBgColor = dayOfTheMonthBackground;
        txtLastSelectColor = dayOfTheMonth;

        monthBgColor.setBackgroundResource(R.drawable.circle_calendar);
        txtLastSelectColor.setTextColor(getResources().getColor(R.color.rudy_calendar_selected_day_font));

        if (calendar.get(Calendar.YEAR) == FixCalendar.get(Calendar.YEAR) && calendar.get(Calendar.MONTH) == FixCalendar.get(Calendar.MONTH)) {
            monthBgColor.setBackgroundResource(R.drawable.circle_calendar);
            txtLastSelectColor.setTextColor(getResources().getColor(R.color.rudy_calendar_selected_day_font));
        }

        setCurrentDate();
    }

    private void clearSelectedDay(Calendar calendar) {

        if(monthBgColor!=null){
            monthBgColor.setBackgroundResource(android.R.color.transparent);
            txtLastSelectColor.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
        }
//        else{
//            if (calendar != null) {
//                ViewGroup dayOfTheMonthBackground = getDayOfMonthBackground(calendar);
//                Calendar nowCalendar = Calendar.getInstance();
//                nowCalendar.set(Calendar.MONTH, Integer.parseInt(monthTxt)-1);
//
//
//                if (nowCalendar.get(Calendar.YEAR) == lastSelectedDayCalendar.get(Calendar.YEAR) && nowCalendar.get(Calendar.DAY_OF_YEAR) == lastSelectedDayCalendar.get(Calendar.DAY_OF_YEAR)) {
//                    dayOfTheMonthBackground.setBackgroundResource(R.drawable.ring);
//                } else {
//                    dayOfTheMonthBackground.setBackgroundResource(android.R.color.transparent);
//                }
//                TextView dayOfTheMonth = getDayOfMonthText(calendar);
//                dayOfTheMonth.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
//            }
//        }

        if (calendar.get(Calendar.YEAR) == FixCalendar.get(Calendar.YEAR) && calendar.get(Calendar.MONTH) == FixCalendar.get(Calendar.MONTH)) {
            CurrentmonthBgColor.setBackgroundResource(R.drawable.ring);
            CurrenttxtColor.setTextColor(getResources().getColor(R.color.rudy_calendar_day_of_the_month_font));
        }
    }

    private String checkSpecificLocales(String dayOfTheWeekString, int i) {
        // Set Wednesday as "X" in Spanish new Locale("TH")


//        if(dayOfTheWeekString.contains("อา")){
//            dayOfTheWeekString = dayOfTheWeekString.substring(3, 5).toUpperCase();
//        }else if(dayOfTheWeekString.contains("พฤ")){
//            dayOfTheWeekString = dayOfTheWeekString.substring(3, 5).toUpperCase();
//        }else if(dayOfTheWeekString.contains(getString(R.string.sat_check))){
//            dayOfTheWeekString = dayOfTheWeekString.substring(4, 5).toUpperCase();
//        }else{
//            dayOfTheWeekString = dayOfTheWeekString.substring(3, 4).toUpperCase();
//        }

//        if("TH".toLowerCase().equalsIgnoreCase("th")){
//            if(dayOfTheWeekString.contains(getString(R.string.sat_check))){
//                dayOfTheWeekString = dayOfTheWeekString.substring(4, 5).toUpperCase();
//            }else{
//                dayOfTheWeekString = dayOfTheWeekString.substring(3, 4).toUpperCase();
//            }
//        }else{
//            if(dayOfTheWeekString.contains("อา")){
//                dayOfTheWeekString = dayOfTheWeekString.substring(0, 2).toUpperCase();
//            }else if(dayOfTheWeekString.contains("พฤ")){
//                dayOfTheWeekString = dayOfTheWeekString.substring(0, 2).toUpperCase();
//            }else{
//                dayOfTheWeekString = dayOfTheWeekString.substring(0, 1).toUpperCase();
//            }
//        }
        return dayOfTheWeekString;
    }

    private ViewGroup getDayOfMonthBackground(Calendar currentCalendar) {
        ViewGroup viewGroup = (ViewGroup) getView(DAY_OF_THE_MONTH_BACKGROUND, currentCalendar);

        return viewGroup;
    }

    private TextView getDayOfMonthText(Calendar currentCalendar) {
        TextView textView =(TextView) getView(DAY_OF_THE_MONTH_TEXT, currentCalendar);
        return textView;
    }

    private ImageView getCircleImage1(Calendar currentCalendar) {
        ImageView imageView = (ImageView) getView(DAY_OF_THE_MONTH_CIRCLE_IMAGE_1, currentCalendar);
        return imageView;
    }

    private ImageView getCircleImage2(Calendar currentCalendar) {
        ImageView imageView = (ImageView) getView(DAY_OF_THE_MONTH_CIRCLE_IMAGE_2, currentCalendar);
        return imageView;
    }

    private ImageView getCircleImage3(Calendar currentCalendar) {
        ImageView imageView = (ImageView) getView(DAY_OF_THE_MONTH_CIRCLE_IMAGE_3, currentCalendar);
        return imageView;
    }

    private int getDayIndexByDate(Calendar currentCalendar) {
        int monthOffset = getMonthOffset(currentCalendar);
        int currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH);
        Log.e("getDay index ByDate","monthOffset:"+monthOffset+"currentDay:"+currentDay);
        return currentDay + monthOffset;
    }

    private int getMonthOffset(Calendar currentCalendar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentCalendar.getTime());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayWeekPosition = calendar.getFirstDayOfWeek();
        int dayPosition = calendar.get(Calendar.DAY_OF_WEEK);

        if (firstDayWeekPosition == 1) {
            return dayPosition - 1;
        } else {
            if (dayPosition == 1) {
                return 6;
            } else {
                return dayPosition - 2;
            }
        }
    }

    private int getWeekIndex(int weekIndex, Calendar currentCalendar) {
        int firstDayWeekPosition = currentCalendar.getFirstDayOfWeek();

        if (firstDayWeekPosition == 1) {
            return weekIndex;
        } else {

            if (weekIndex == 1) {
                return 7;
            } else {
                return weekIndex - 1;
            }
        }
    }

    private View getView(String key, Calendar currentCalendar) {
        int index = getDayIndexByDate(currentCalendar);
        Log.e("index :",""+index);


        if(key.contains(DAY_OF_THE_MONTH_BACKGROUND)){
            return rootView.findViewWithTag(key + index);
        }else{
            return rootView.findViewWithTag(key + index);
        }
    }

    View.OnClickListener onDayOfMonthClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            ViewGroup dayOfTheMonthContainer = (ViewGroup) view;
            String monthTagId = (String) dayOfTheMonthContainer.getTag();
            String tagId = monthTagId.substring(DAY_OF_THE_MONTH_LAYOUT.length());
            TextView dayOfTheMonthText = view.findViewWithTag(DAY_OF_THE_MONTH_TEXT + tagId);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, FixCalendar.get(Calendar.YEAR));
            calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dayOfTheMonthText.getText().toString()));
            calendar.set(Calendar.MONTH, Integer.parseInt(monthTxt)-1);

            realDate = Integer.parseInt(dayOfTheMonthText.getText().toString());
            month_select = (Integer.parseInt(monthTxt))-1;

            markDayAsSelectedDay(calendar);

            Intent intent = new Intent(CalendarActivity.this,CalendarDetailActivity.class);
            intent.putExtra("calendarData",_calendarPojo);
            intent.putExtra("daySelect",tagId);
            intent.putExtra("realDate",realDate);
            intent.putExtra("month_select",month_select);
            startActivity(intent);




        }
    };

    private void setCurrentDate(){
//        SimpleDateFormat formatter = new SimpleDateFormat("EEEE,dd,MMMM,yyyy", new Locale("th", "TH"));
//        String timeToday[] = formatter.format(new Date()).split(",");


        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE,dd,MMMM,yyyy", new Locale(Utils.APP_LANGUAGE, Utils.APP_LANGUAGE.toUpperCase()));
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.set(Calendar.YEAR, FixCalendar.get(Calendar.YEAR));
        currentCalendar.set(Calendar.MONTH, month_select);
        currentCalendar.set(Calendar.DAY_OF_MONTH, realDate);


        Date currentTime = currentCalendar.getTime();
        String dateFormat = simpledateformat.format(currentTime.getTime());
        final String time_splite[] = dateFormat.split(",");
//        monthTxt = time_splite[0];
//        yearTxt = time_splite[1];

        day.setText(time_splite[0].replace("วัน", ""));
        date.setText(time_splite[1]);
        month.setText(time_splite[2]);
        int year_af = Integer.parseInt(time_splite[3]);
        if (year_af < 2500) {
            year_af = year_af + 543;
        }
        year.setText(String.valueOf(year_af));
    }
    


    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {
        Utils.showToast(this,message);
    }

    public void showMessage(int resId) {
        Utils.showToast(this,""+resId);
    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private void initView() {
        rudyService = ApiHelper.getClient();
//        inflate = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        addCalendar = findViewById(R.id.addCalendar);

        calendarViewAdd = LayoutInflater.from(this).inflate(R.layout.rudy_calendar_week_layout, null);


        clsBtn = findViewById(R.id.cls_btn);
        monthSpinner = findViewById(R.id.monthSpinner);
        day = findViewById(R.id.day);
        date = findViewById(R.id.date);
        month = findViewById(R.id.month);
        year = findViewById(R.id.year);
        addNew = findViewById(R.id.addNew);

    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }
    }
}
