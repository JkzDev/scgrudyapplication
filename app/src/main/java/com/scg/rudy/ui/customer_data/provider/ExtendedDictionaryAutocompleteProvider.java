/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.customer_data.provider;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scg.rudy.RudyApplication;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutocompleteProvider;

import java.lang.reflect.Type;
import java.util.Collection;

import static com.scg.rudy.data.remote.ApiEndPoint.HOST;

/**
 * @author jackie
 */
public class ExtendedDictionaryAutocompleteProvider extends AbstractProvider implements DynamicAutocompleteProvider<CusModel> {
    private   String REST_URL = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/customer/%s/?act=list_by_name_phone";
    @Override
    public Collection<CusModel> provideDynamicAutocompleteItems(String constraint) {
        UserPOJO userPOJO = Utils.setEnviroment(RudyApplication.getAppContext());
        String queryUrl = String.format(REST_URL,Utils.APP_LANGUAGE,userPOJO.getItems().getId());
        String json = invokeRestQuery(queryUrl,constraint);
        Type type = new TypeToken<Collection<CusModel>>(){}.getType();
        return new Gson().fromJson(json,type);
    }
}