package com.scg.rudy.ui.transaction_detail.model;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("ispromotion")
	private String ispromotion;

	@SerializedName("unit")
	private String unit;

	@SerializedName("price")
	private String price;

	@SerializedName("class_id")
	private String classId;

	@SerializedName("name")
	private String name;

	@SerializedName("recommend")
	private String recommend;

	@SerializedName("fav")
	private int fav;

	@SerializedName("commission")
	private String commission;

	@SerializedName("id")
	private String id;

	@SerializedName("pic")
	private String pic;

	@SerializedName("used2use")
	private int used2use;

	@SerializedName("hot")
	private int hot;

	public void setIspromotion(String ispromotion){
		this.ispromotion = ispromotion;
	}

	public String getIspromotion(){
		return ispromotion;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setClassId(String classId){
		this.classId = classId;
	}

	public String getClassId(){
		return classId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRecommend(String recommend){
		this.recommend = recommend;
	}

	public String getRecommend(){
		return recommend;
	}

	public void setFav(int fav){
		this.fav = fav;
	}

	public int getFav(){
		return fav;
	}

	public void setCommission(String commission){
		this.commission = commission;
	}

	public String getCommission(){
		return commission;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setUsed2use(int used2use){
		this.used2use = used2use;
	}

	public int getUsed2use(){
		return used2use;
	}

	public void setHot(int hot){
		this.hot = hot;
	}

	public int getHot(){
		return hot;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"ispromotion = '" + ispromotion + '\'' + 
			",unit = '" + unit + '\'' + 
			",price = '" + price + '\'' + 
			",class_id = '" + classId + '\'' + 
			",name = '" + name + '\'' + 
			",recommend = '" + recommend + '\'' + 
			",fav = '" + fav + '\'' + 
			",commission = '" + commission + '\'' + 
			",id = '" + id + '\'' + 
			",pic = '" + pic + '\'' + 
			",used2use = '" + used2use + '\'' + 
			",hot = '" + hot + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.ispromotion);
		dest.writeString(this.unit);
		dest.writeString(this.price);
		dest.writeString(this.classId);
		dest.writeString(this.name);
		dest.writeString(this.recommend);
		dest.writeInt(this.fav);
		dest.writeString(this.commission);
		dest.writeString(this.id);
		dest.writeString(this.pic);
		dest.writeInt(this.used2use);
		dest.writeInt(this.hot);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.ispromotion = in.readString();
		this.unit = in.readString();
		this.price = in.readString();
		this.classId = in.readString();
		this.name = in.readString();
		this.recommend = in.readString();
		this.fav = in.readInt();
		this.commission = in.readString();
		this.id = in.readString();
		this.pic = in.readString();
		this.used2use = in.readInt();
		this.hot = in.readInt();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}