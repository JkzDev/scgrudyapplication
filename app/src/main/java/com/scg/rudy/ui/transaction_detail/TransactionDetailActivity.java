package com.scg.rudy.ui.transaction_detail;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.PRCodeDetailModel;
import com.scg.rudy.model.pojo.QTListModel;
import com.scg.rudy.model.pojo.favorite.Favorite;
import com.scg.rudy.model.pojo.project_detail.Items;
import com.scg.rudy.model.pojo.transaction_detail.PrItem;
import com.scg.rudy.model.pojo.transaction_detail.Transaction;
import com.scg.rudy.ui.adapter.FavRecyclerAdapter;
import com.scg.rudy.ui.adapter.QORecyclerAdapter;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.qo_vat.QoVatActivity;
import com.scg.rudy.ui.transaction_detail.model.skuModel;
import com.scg.rudy.ui.transaction_detail.provider.SkuAbstractProvider;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutoCompleteTextView;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteListener;

import org.checkerframework.checker.units.qual.C;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getAddBasket;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class TransactionDetailActivity extends BaseActivity implements View.OnClickListener, QoInterface, OnDynamicAutocompleteListener, TransactionInterface {
    @BindView(R.id.fav_recycler)
    RecyclerView favRecycler;
    @BindView(R.id.open_fav)
    ImageView openFav;
    @BindView(R.id.addView)
    RelativeLayout addView;

    private ImageView clsBtn;
    private RelativeLayout openQO;
    private String user_id = "";
    private TextView totalAllsum;
    private Bundle extras;
    public static String transaction_id = "";
    private TextView title_name;
    private RecyclerView recyclerView;
    public static String basketString = "";
    private ArrayList<PRCodeDetailModel> arrayList;
    public static QTListModel prCodeDetailModel;
    private TextView totalItem;
    private boolean isShowFAV = false;
    public static int from_fav = 0;
    private Double totalPrice = 0.0;
    private ArrayList<String> idList;
    public static String productId = "";
    public static boolean isDelete = false;
    private int favSize = 0;
    public static Items detailItem;
    private List<PrItem> cartModelArrayLis;
    private List<PrItem> origin_cartModelArrayLis;

    private String typeSelect = "";
    private String cname = "";
    private String cphone = "";
    private String customer_tax_no = "";
    private String customer_code = "";
    private String champ_customer_code = "";


    private ArrayList<String> checkList;
    private static final int THRESHOLD = 1;
    private TransactionAutocompleteAdapter nameAdapter;
    private TransactionAutocompleteAdapter telAdapter;
    private ProgressBar mDictionaryAutoCompleteProgressBar;
    private ProgressBar telAutoCompleteProgressBar;
    private DynamicAutoCompleteTextView name;
    private DynamicAutoCompleteTextView tel;

    private DynamicAutoCompleteTextView item_name;
    private ProgressBar itemNameAutoCompleteProgressBar;
    private SkuAutocompleteAdapter skuAutocompleteAdapter;
    private RelativeLayout addSkuBtn;
    private CardView searchSkuView;
    private Button cancel;
    private double totalSearchPrice = 0;
    private String title = "";
    public LinearLayout txLayout = null;
    public EditText tax_no;
    private TextView champCode;
    private TextView champHead;


    private boolean isCheckAll = false;
    private LinearLayout selectAll;
    private int stateClick = 2;
    private ImageView icoSelectAll;
    private RudyService rudyService;
    private TextView textView22;
    private TextView countItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qo);
        ButterKnife.bind(this);

        rudyService = ApiHelper.getClient();


        findViewById(R.id.bhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                Intent intents = new Intent(TransactionDetailActivity.this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });

        arrayList = new ArrayList<>();
        initView();
        setEnviroment();
        setOnclick();
        getUserData();
        idList = new ArrayList<>();

//        transaction_id = "403405";
        extras = getIntent().getExtras();
        if (extras != null) {
            transaction_id = extras.getString("transaction_id");
        }

        if (from_fav == 1) {
            setShowFAV(isShowFAV);
            isShowFAV = !isShowFAV;
        }
        SkuAbstractProvider.projectId = Utils.project_id;
        SkuAbstractProvider.shopID = shopID;
        SkuAbstractProvider.userId = user_id;

        skuAutocompleteAdapter = new SkuAutocompleteAdapter(this, R.layout.search_sku_list_item, this, 1);
        item_name.setOnDynamicAutocompleteListener(this);
        item_name.setAdapter(skuAutocompleteAdapter);
        item_name.setThreshold(THRESHOLD);
        itemNameAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        selectAll.setOnClickListener(this);

        textView22.setText(Utils.currencyForShow);

    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData", e);
        }


    }

    public void setOnclick() {
        clsBtn.setOnClickListener(this);
        openQO.setOnClickListener(this);
        openFav.setOnClickListener(this);
        addSkuBtn.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    private void initView() {
        clsBtn = findViewById(R.id.cls_btn);
        openQO = findViewById(R.id.openQO);
        title_name = findViewById(R.id.title_name);
        totalAllsum = findViewById(R.id.total_allsum);
        recyclerView = findViewById(R.id.recyclerView);
        item_name = findViewById(R.id.item_name);
        itemNameAutoCompleteProgressBar = findViewById(R.id.itemNameAutoCompleteProgressBar);

        if (prCodeDetailModel != null) {
            title_name.setText(prCodeDetailModel.getQo_code());
        }


        totalItem = findViewById(R.id.totalItem);

        addSkuBtn = (RelativeLayout) findViewById(R.id.add_sku_btn);
        searchSkuView = (CardView) findViewById(R.id.search_sku_view);
        cancel = (Button) findViewById(R.id.cancel);
//        selectAll = (CheckBox) findViewById(R.id.selectAll);
        selectAll = (LinearLayout) findViewById(R.id.selectAll);
        icoSelectAll = (ImageView) findViewById(R.id.icoSelectAll);
        textView22 = (TextView) findViewById(R.id.textView22);
        countItem = (TextView) findViewById(R.id.countItem);
    }

    public void setShowFAV(boolean isShow) {
        if (isShow) {
            addView.setVisibility(View.GONE);
            isShowFAV = !isShowFAV;
        } else {
            addView.setVisibility(View.VISIBLE);
            isShowFAV = !isShowFAV;
        }

    }

    private void checkIsCheckAll(boolean checkAlllick) {
        if (checkAlllick) {
            //normal
            if (stateClick == 0) {
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck_all));
                idList = new ArrayList<>();
                checkList = new ArrayList<>();
                stateClick = 2;
//                totalPrice = 0.0;
//                totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
                cartModelArrayLis = origin_cartModelArrayLis;
                QORecyclerAdapter adapter = new QORecyclerAdapter(TransactionDetailActivity.this, cartModelArrayLis, TransactionDetailActivity.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(TransactionDetailActivity.this));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);

            }

            //select
            else if (stateClick == 1) {
                selectItemAll();
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_on));

            }
//        dont'select
            else {
                selectItemAll();
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_off));

            }
        } else {
            //normal
            if (stateClick == 0) {
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck_all));
            }
            //select
            else if (stateClick == 1) {
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_on));
            }
//        dont'select
            else {
                icoSelectAll.setImageDrawable(getResources().getDrawable(R.drawable.ic_checkbox_off));
            }
        }

        if (idList.isEmpty()) {
            totalPrice = 0.0;
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
            countItem.setText("0");
        }
    }

    public void selectItemAll() {
        if (cartModelArrayLis != null || cartModelArrayLis.size() > 0) {
            for (int i = 0; i < cartModelArrayLis.size(); i++) {
                PrItem prCodeDetailModel = cartModelArrayLis.get(i);
                if (prCodeDetailModel.getPriceRank().contains("1")) {
                    Double itemPrice = Double.parseDouble(prCodeDetailModel.getNetAmount());
                    if (prCodeDetailModel.isCheck()) {
                        itemPrice = -itemPrice;
                    }
                    onSelectItem(prCodeDetailModel.getProductId(), itemPrice, isCheckAll);
                    cartModelArrayLis.get(i).setCheck(isCheckAll);
                }
                if (prCodeDetailModel.getPriceRank().contains("2")) {
                    Double itemPrice = Double.parseDouble(prCodeDetailModel.getNetAmount());
                    if (cartModelArrayLis.get(i).isCheck()) {
                        itemPrice = -itemPrice;
                    }
                    onSelectItem(prCodeDetailModel.getProductId(), itemPrice, isCheckAll);
                    cartModelArrayLis.get(i).setCheck(isCheckAll);
                }
                if (!prCodeDetailModel.getApproveBy().equalsIgnoreCase("0")) {
                    Double itemPrice = Double.parseDouble(prCodeDetailModel.getNetAmount());
                    if (cartModelArrayLis.get(i).isCheck()) {
                        itemPrice = -itemPrice;
                    }
                    onSelectItem(prCodeDetailModel.getProductId(), itemPrice, isCheckAll);
                    cartModelArrayLis.get(i).setCheck(isCheckAll);
                }
            }

            QORecyclerAdapter adapter = new QORecyclerAdapter(this, cartModelArrayLis, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }


    }


    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            hideKeyboard();
            finish();
        }
        if (v == selectAll) {
            if (stateClick == 0) {
                isCheckAll = false;
                stateClick = 2;
            } else if (stateClick == 1) {
                isCheckAll = false;
                stateClick = 2;
            } else {
                isCheckAll = true;
                stateClick = 1;
            }
            checkIsCheckAll(true);
        }


        if (v == openQO) {
            if (totalPrice == 0) {
                showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_pls_select_product_for_qo));
            } else {
                if (detailItem != null) {
                    if (!detailItem.getCustomerName().isEmpty() &&
                            !detailItem.getCustomerPhone().isEmpty() &&
                            !detailItem.getHouseOwnerName().isEmpty() &&
                            !detailItem.getHouseOwnerPhone().isEmpty()) {
                        showSelectCustomerDialog();
                    } else if (detailItem.getCustomerName().isEmpty() &&
                            detailItem.getCustomerPhone().isEmpty() &&
                            detailItem.getHouseOwnerName().isEmpty() &&
                            detailItem.getHouseOwnerPhone().isEmpty()) {


                        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                                .title(getResources().getString(R.string.dialog_qo))
                                .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + Utils.currencyForShow)
//                                .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + getResources().getString(R.string.bath))
                                .positiveText(getResources().getString(R.string.agreed))
                                .negativeText(getResources().getString(R.string.cancel))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        showInputCustomerDialog();
                                        dialog.dismiss();
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        dialog.dismiss();
                                    }
                                });

                        MaterialDialog del_dialog = builder.build();
                        del_dialog.show();
                    } else {
                        if (!detailItem.getCustomerName().isEmpty() &&
                                !detailItem.getCustomerPhone().isEmpty()) {
                            showConfirmPrice("1");
                        } else {
                            showConfirmPrice("2");
                        }
                    }

                } else {
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                            .title(getResources().getString(R.string.dialog_qo))
                            .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + Utils.currencyForShow)
//                            .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + getResources().getString(R.string.bath))
                            .positiveText(getResources().getString(R.string.agreed))
                            .negativeText(getResources().getString(R.string.cancel))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    showInputCustomerDialog();
                                    dialog.dismiss();
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    dialog.dismiss();
                                }
                            });

                    MaterialDialog del_dialog = builder.build();
                    del_dialog.show();
                }
            }
        }

        if (v == openFav) {
            if (favSize > 0) {
                setShowFAV(isShowFAV);
            } else {
                showToast(this, getResources().getString(R.string.toast_no_product_fivorite_pls_add));
            }
        }

        if (v == addSkuBtn) {
            addSkuBtn.setVisibility(View.GONE);
            searchSkuView.setVisibility(View.VISIBLE);
            item_name.setText("");
            item_name.requestFocus();
            Utils.showKeyboardFrom(this, item_name);
        }

        if (v == cancel) {
            addSkuBtn.setVisibility(View.VISIBLE);
            searchSkuView.setVisibility(View.GONE);
            hideKeyboard();
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (Utils.getPrefer(this, "print").length() > 0) {
            finish();
        } else {
            if (isDelete) {
                deleteFav();
//                deleteItems(ApiEndPoint.getDeleteFAV(Utils.APP_LANGUAGE, Utils.project_id, productId));
            } else {
                getFavoriteList();
                getTransactionList();
            }
        }


    }

    private Call<String> callFavoriteList() {
        return rudyService.callFavoriteList(
                Utils.APP_LANGUAGE,
                Utils.project_id
        );
    }

    public void getFavoriteList() {
        showLoading(this);
        callFavoriteList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Favorite favorite = gson.fromJson(resp, Favorite.class);
                    showcallFavoriteList(favorite);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(TransactionDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callTransactionList() {
        return rudyService.callTransactionList(
                Utils.APP_LANGUAGE,
                transaction_id,
                "detail"
        );
    }


    public void getTransactionList() {
        showLoading(this);
        callTransactionList().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Transaction transaction = gson.fromJson(resp, Transaction.class);
                    showTransactionList(transaction);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(TransactionDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });


    }

    public void showSelectCustomerDialog() {
        typeSelect = "";
        final Dialog dialog = new Dialog(TransactionDetailActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_select_cus);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final RelativeLayout submit = dialog.findViewById(R.id.submit);
        final RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        final RelativeLayout customer_sel = dialog.findViewById(R.id.customer_sel);
        final RelativeLayout owner_sel = dialog.findViewById(R.id.owner_sel);
        final TextView nameOwn = dialog.findViewById(R.id.name_own);
        final TextView telOwn = dialog.findViewById(R.id.tel_own);
        final ImageView ownSelect = dialog.findViewById(R.id.own_select);
        final ImageView cusSelect = dialog.findViewById(R.id.cus_select);
        final TextView nameCus = dialog.findViewById(R.id.name_cus);
        final TextView telCus = dialog.findViewById(R.id.tel_cus);

        nameCus.setText(detailItem.getCustomerName());
        telCus.setText(detailItem.getCustomerPhone());
        nameOwn.setText(detailItem.getHouseOwnerName());
        telOwn.setText(detailItem.getHouseOwnerPhone());

        customer_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typeSelect = "1";
                ownSelect.setVisibility(View.INVISIBLE);
                cusSelect.setVisibility(View.VISIBLE);
            }
        });
        owner_sel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typeSelect = "2";
                ownSelect.setVisibility(View.VISIBLE);
                cusSelect.setVisibility(View.INVISIBLE);

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (typeSelect.isEmpty()) {
                    showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_pls_select_bidder));
                } else {
                    showConfirmPrice(typeSelect);
                    dialog.dismiss();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void showConfirmPrice(final String typeCustomer) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(getResources().getString(R.string.dialog_qo))
                .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + Utils.currencyForShow)
//                .content(getResources().getString(R.string.dialog_count) + idList.size() + getResources().getString(R.string.list) + getResources().getString(R.string.dialog_total) + totalAllsum.getText().toString() + getResources().getString(R.string.bath))
                .positiveText(getResources().getString(R.string.agreed))
                .negativeText(getResources().getString(R.string.cancel))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        String url = ApiEndPoint.getPrTOqo(Utils.APP_LANGUAGE, transaction_id);
//                        reviseQo(url, typeCustomer);
                        dialog.dismiss();
                        Intent intent = new Intent(TransactionDetailActivity.this, QoVatActivity.class);
                        intent.putExtra("totalItem", String.valueOf(idList.size()));
                        intent.putExtra("totalPrice", totalAllsum.getText().toString());
                        intent.putExtra("qo_id", transaction_id);
                        intent.putExtra("typeCustomer", typeCustomer);
                        intent.putExtra("pId", idList.toString().replace("[", "").replace("]", ""));
                        startActivity(intent);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                });

        MaterialDialog del_dialog = builder.build();
        del_dialog.show();
    }

    public void showInputCustomerDialog() {
        typeSelect = "1";
        final Dialog dialog = new Dialog(TransactionDetailActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_input_cus);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.7);
        dialog.getWindow().setLayout(width, height);


        mDictionaryAutoCompleteProgressBar = dialog.findViewById(R.id.dictionaryAutoCompleteProgressBar);
        telAutoCompleteProgressBar = dialog.findViewById(R.id.telAutoCompleteProgressBar);
        name = dialog.findViewById(R.id.name);
        tel = dialog.findViewById(R.id.tel);


        nameAdapter = new TransactionAutocompleteAdapter(this, R.layout.extended_list_item, this, 1, name);
        telAdapter = new TransactionAutocompleteAdapter(this, R.layout.extended_list_item, this, 1, tel);


        tax_no = dialog.findViewById(R.id.tax_no);

        champCode = dialog.findViewById(R.id.champCode);
        champHead = dialog.findViewById(R.id.champHead);

        final RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        final RelativeLayout submit = dialog.findViewById(R.id.submit);
        final RelativeLayout select_cus_type = dialog.findViewById(R.id.select_cus_type);
        final TextView typSelect = dialog.findViewById(R.id.typSelect);

        final LinearLayout tax_layout = dialog.findViewById(R.id.tax_layout);


        txLayout = tax_layout;


        name.setOnDynamicAutocompleteListener(this);
        name.setAdapter(nameAdapter);
        name.setThreshold(THRESHOLD);

        tel.setOnDynamicAutocompleteListener(this);
        tel.setAdapter(telAdapter);
        tel.setThreshold(THRESHOLD);

        mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);


        select_cus_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PopupWindow popup = new PopupWindow(TransactionDetailActivity.this);
                View layout = getLayoutInflater().inflate(R.layout.popup_custype, null);
                popup.setContentView(layout);
                final TextView cus_type = layout.findViewById(R.id.cus_type);
                final TextView own_type = layout.findViewById(R.id.own_type);

                // Set content width and height
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAsDropDown(select_cus_type, 10, -10, Gravity.LEFT);
                cus_type.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        typeSelect = "1";
                        typSelect.setText(cus_type.getText());
                        popup.dismiss();
                    }
                });
                own_type.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        typeSelect = "2";
                        typSelect.setText(own_type.getText());
                        popup.dismiss();
                    }
                });
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cname = name.getText().toString();
                cphone = tel.getText().toString();
                customer_tax_no = tax_no.getText().toString();
                champ_customer_code = champCode.getText().toString();


                if (cname.isEmpty() || cphone.isEmpty()) {
                    showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_pls_complete_the_info));
                } else {
                    if (typeSelect.isEmpty()) {
                        showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_pls_selete_type_customer));
                    } else {
                        updateCustomer(typeSelect);
                        dialog.dismiss();
                    }


                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();
            }
        });
//
        dialog.show();
    }


    private Call<String> callUpdateCustomerProject(String typeCus) {
        return rudyService.UpdateCustomerProject(
                Utils.APP_LANGUAGE,
                project_id,
                typeSelect+"",
                cname,
                cphone,
                customer_code,
                champ_customer_code,
                "",
                detailItem.getCustomerLine(),
                typeCus.equalsIgnoreCase("1") ? customer_tax_no:"",
                detailItem.getCustomerNote(),
                typeCus.equalsIgnoreCase("1") ? detailItem.getHouseOwnerName():cname,
                typeCus.equalsIgnoreCase("1") ? detailItem.getHouseOwnerPhone():cphone,
                detailItem.getHouseOwnerLine(),
                detailItem.getHouseOwnerNote(),
                typeCus.equalsIgnoreCase("1") ? detailItem.getProjectOwnerName():cname,
                typeCus.equalsIgnoreCase("1") ? detailItem.getProjectOwnerPhone():cphone,
                detailItem.getHouseOwnerLine(),
                detailItem.getHouseOwnerNote()
        );
    }


    @SuppressLint("StaticFieldLeak")
    private void updateCustomer(String typeCus) {
        final Dialog dialog = new Dialog(TransactionDetailActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog, TransactionDetailActivity.this);

        callUpdateCustomerProject(typeCus).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Intent intent = new Intent(TransactionDetailActivity.this, QoVatActivity.class);

                intent.putExtra("totalItem", String.valueOf(idList.size()));
                intent.putExtra("totalPrice", totalAllsum.getText().toString());
                intent.putExtra("qo_id", transaction_id);
                intent.putExtra("typeCustomer", typeCus);
                intent.putExtra("pId", idList.toString().replace("[", "").replace("]", ""));
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });

    }



//    @SuppressLint("StaticFieldLeak")
//    private void updateCustomer(final String url, final String typeCus) {
//        final Dialog dialog = new Dialog(TransactionDetailActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, TransactionDetailActivity.this);
//        RequestBody requestBody = null;
//        if (typeCus.equalsIgnoreCase("1")) {
//            requestBody = new FormBody.Builder()
//                    .add("customer_type", "" + typeSelect)
//                    .add("customer_name", cname)
//                    .add("customer_phone", cphone)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_email", "")
//                    .add("customer_line", detailItem.getCustomerLine())
//                    .add("customer_tax_no", "")
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_note", detailItem.getCustomerNote())
//                    .add("house_owner_name", detailItem.getHouseOwnerName())
//                    .add("house_owner_phone", detailItem.getHouseOwnerPhone())
//                    .add("house_owner_line", detailItem.getHouseOwnerLine())
//                    .add("house_owner_note", detailItem.getHouseOwnerNote())
//                    .add("project_owner_name", detailItem.getProjectOwnerName())
//                    .add("project_owner_phone", detailItem.getProjectOwnerPhone())
//                    .add("project_owner_line", detailItem.getHouseOwnerLine())
//                    .add("project_owner_note", detailItem.getHouseOwnerNote())
//                    .build();
//        } else {
//            requestBody = new FormBody.Builder()
//                    .add("customer_type", "" + typeSelect)
//                    .add("customer_name", cname)
//                    .add("customer_phone", cphone)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_email", "")
//                    .add("customer_tax_no", customer_tax_no)
//                    .add("customer_line", detailItem.getCustomerLine())
//                    .add("customer_tax_no", "")
//                    .add("customer_note", detailItem.getCustomerNote())
//                    .add("house_owner_name", cname)
//                    .add("house_owner_phone", cphone)
//                    .add("house_owner_line", detailItem.getHouseOwnerLine())
//                    .add("house_owner_note", detailItem.getHouseOwnerNote())
//                    .add("project_owner_name", cname)
//                    .add("project_owner_phone", cphone)
//                    .add("project_owner_line", detailItem.getHouseOwnerLine())
//                    .add("project_owner_note", detailItem.getHouseOwnerNote())
//                    .build();
//        }
//
//
//        final RequestBody finalRequestBody = requestBody;
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, finalRequestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                dialog.dismiss();
//                super.onPostExecute(string);
//                dialog.dismiss();
////                String url = ApiEndPoint.getPrTOqo("th", transaction_id);
////                reviseQo(url, typeCus);
//                Intent intent = new Intent(TransactionDetailActivity.this, QoVatActivity.class);
//
//                intent.putExtra("totalItem", String.valueOf(idList.size()));
//                intent.putExtra("totalPrice", totalAllsum.getText().toString());
//                intent.putExtra("qo_id", transaction_id);
//                intent.putExtra("typeCustomer", typeCus);
//                intent.putExtra("pId", idList.toString().replace("[", "").replace("]", ""));
//                startActivity(intent);
//            }
//        }.execute();
//    }

    private Call<String> callUpdatePriceQty(String productId, String itemPrice, String unit_id, String itemQty, String discount, String discount_type) {
        return rudyService.editPrice(
                Utils.APP_LANGUAGE,
                transaction_id,
                Utils.project_id,
                productId,
                itemPrice,
                unit_id,
                itemQty,
                discount,
                discount_type
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updatePriceQty(String productId, String itemPrice, String unit_id, String itemQty, String discount, String discount_type){
        showLoading(TransactionDetailActivity.this);
        callUpdatePriceQty(productId, itemPrice, unit_id, itemQty, discount, discount_type).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    stateClick = 2;
                    showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_edit_product_success));

                    getTransactionList();

                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(TransactionDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void updatePriceQty(final String url, String productId, String itemQty, String itemPrice, String unit_id, String discount, String discount_type) {
//        Log.i("post ", " : " + url);
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("project_id", Utils.project_id);
//        formData.add("product_id", productId);
//        formData.add("price", itemPrice);
//        formData.add("qty", itemQty);
//        formData.add("unit_id", unit_id);
//        formData.add("discount", discount);
//        formData.add("discount_type", discount_type);
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    stateClick = 2;
//                    showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_edit_product_success));
//
//                    getTransactionList();
//
//                }
//
//            }
//        }.execute();
//    }

    private Call<String> callDeleteItems(String _productId) {
        return rudyService.deletePr(
                Utils.APP_LANGUAGE,
                transaction_id,
                _productId,
                project_id
        );
    }
    @SuppressLint("StaticFieldLeak")
    private void deleteItemsPr(String productId) {
        showLoading(TransactionDetailActivity.this);
        callDeleteItems(productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    getTransactionList();

                } else {
                    getTransactionList();
                    recyclerView.setAdapter(null);
                    totalPrice = 0.0;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void deleteItems(final String url, String productId) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("product_id", productId);
//        formData.add("project_id", Utils.project_id);
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                showToast(TransactionDetailActivity.this, getResources().getString(R.string.toast_delete_product_success));
//                if (string.contains("200")) {
//
//                    getTransactionList();
//
//                } else {
//                    getTransactionList();
//
//                    recyclerView.setAdapter(null);
//                    totalPrice = 0.0;
//                }
//
//            }
//        }.execute();
//    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {
        showToast(this, message);

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {
        Utils.hideKeyboard(this);
    }


    public void showTransactionList(Transaction transaction) {
        checkList = new ArrayList<>();
        idList = new ArrayList<>();
        checkIsCheckAll(false);
        if (transaction.getItems().getPr().size() > 0) {
            cartModelArrayLis = transaction.getItems().getPr();
            origin_cartModelArrayLis = transaction.getItems().getPr();
            QORecyclerAdapter adapter = new QORecyclerAdapter(this, cartModelArrayLis, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setAdapter(null);
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
        }


    }

    public void TransactionListError(ResponseBody responseBodyError) {
        recyclerView.setAdapter(null);
        totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
    }

    public void TransactionListFail(Throwable t) {
        recyclerView.setAdapter(null);
        totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));
    }

    public void showcallFavoriteList(Favorite favorite) {

        if (favorite.getItems().size() > 0) {
            favSize = favorite.getItems().get(0).getList().size();
            LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

            FavRecyclerAdapter adapter = new FavRecyclerAdapter(this, favorite.getItems().get(0).getList());
            favRecycler.setLayoutManager(horizontalLayoutManagaer);
            favRecycler.setItemAnimator(new DefaultItemAnimator());
            favRecycler.setAdapter(adapter);


            idList = new ArrayList<>();
            totalPrice = 0.0;
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(0)));
        } else {
            idList = new ArrayList<>();
            totalPrice = 0.0;
            totalAllsum.setText(Utils.moneyFormat(String.valueOf(0)));
            favRecycler.setAdapter(null);
        }
    }


    private Call<String> callDeleteFav() {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                productId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void deleteFav(){
        showLoading(TransactionDetailActivity.this);
        callDeleteFav().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    getFavoriteList();
                    getTransactionList();
                } else {
                    getFavoriteList();
                    getTransactionList();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void deleteItems(final String url) {
//        showLoading(this);
//
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        requestBody = formData.build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//
//                    getFavoriteList();
//                    getTransactionList();
//                } else {
//                    getFavoriteList();
//                    getTransactionList();
//                }
//
//            }
//        }.execute();
//    }


//    public void callFavoriteListError(ResponseBody responseBodyError) {
//        favSize = 0;
//        addView.setVisibility(View.GONE);
//        isShowFAV = !isShowFAV;
//        favRecycler.setAdapter(null);
//
//    }
//
//    public void callFavoriteListFail(Throwable t) {
//        favSize = 0;
//        addView.setVisibility(View.GONE);
//        isShowFAV = !isShowFAV;
//        favRecycler.setAdapter(null);
//
//    }


    @Override
    public void onClickEdit(String itemId, String itemQty, String itemPrice, String user_id, String discount, String discount_type) {
        updatePriceQty(itemId, itemPrice, user_id, itemQty, discount, discount_type);
    }

    @Override
    public void onClickDelete(String itemId) {
        deleteItemsPr(itemId);

    }

    @Override
    public void onSelectItem(String itemId, Double itemPrice, boolean isCheck) {
        if (isCheck) {
            idList.add(itemId);
            checkList.add(itemId);
        } else {
            idList.remove(itemId);
            checkList.remove(itemId);
        }

        if (checkList.size() != 0) {
            stateClick = 0;
        } else {
            stateClick = 2;
        }

        totalPrice = totalPrice + itemPrice;
        countItem.setText(Integer.toString(idList.size()));
        totalAllsum.setText(Utils.moneyFormat(String.valueOf(totalPrice)));


        checkIsCheckAll(false);
    }

    @Override
    public void onSearchItemClick(skuModel skuModel) {

//        showMessage(skuModel.getName());
        item_name.clearFocus();
        Utils.hideKeyboardFrom(TransactionDetailActivity.this, item_name);
        Utils.hideKeyboard(this);
        totalSearchPrice = Double.parseDouble(skuModel.getPrice());
        title = skuModel.getName();

        AddBasket(skuModel.getId());
        cancel.performClick();
    }

    @Override
    public void selectCusItem(CusModel cusModel, int tabIndex, EditText inputText) {

        name.setText(cusModel.getCustomer_name());
        tel.setText(cusModel.getCustomer_phone());
        customer_code = cusModel.getCustomer_code();
        champ_customer_code = cusModel.getChamp_customer_code();

        if (cusModel.getCustomer_taxno().length() > 0) {
            txLayout.setVisibility(View.VISIBLE);
            tax_no.setText(cusModel.getCustomer_taxno());
        } else {
            txLayout.setVisibility(View.GONE);
            tax_no.setText("");
        }

        if (cusModel.getChamp_customer_code().length() > 0) {

            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(champ_customer_code);
        } else {
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
            champCode.setText("");

        }

        inputText.clearFocus();
        Utils.hideKeyboardFrom(TransactionDetailActivity.this, inputText);
        Utils.hideKeyboard(this);
//        tel.clearFocus();
//        tax_no.clearFocus();
//        Utils.hideKeyboardFrom(TransactionDetailActivity.this,tel);
//        Utils.hideKeyboardFrom(TransactionDetailActivity.this,tax_no);
//
//
//        tax_no.clearFocus();
//        tel.clearFocus();
//        name.clearFocus();
//        Utils.hideKeyboardFrom(TransactionDetailActivity.this,tax_no);
//        Utils.hideKeyboardFrom(TransactionDetailActivity.this,tel);
//        Utils.hideKeyboardFrom(TransactionDetailActivity.this,name);
//
//
//

    }

    @Override
    public void onDynamicAutocompleteStart(AutoCompleteTextView v) {
        if (v == name) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }
        if (v == tel) {
            telAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }
        if (v == item_name) {
            itemNameAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onDynamicAutocompleteStop(AutoCompleteTextView v) {
        if (v == name) {
//            customer_code ="";
//            champ_customer_code = "";

//            if(champ_customer_code.isEmpty()){
//                champCode.setVisibility(View.GONE);
//                champHead.setVisibility(View.GONE);
//            }else{
//                champCode.setVisibility(View.VISIBLE);
//                champHead.setVisibility(View.VISIBLE);
//                champCode.setText(champ_customer_code);
//            }

            mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cname = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            name.setTypeface(_font);

        }
        if (v == tel) {
//            customer_code ="";
//            champ_customer_code ="";
//            if(champ_customer_code.isEmpty()){
//                champCode.setVisibility(View.GONE);
//                champHead.setVisibility(View.GONE);
//            }else{
//                champCode.setVisibility(View.VISIBLE);
//                champHead.setVisibility(View.VISIBLE);
//                champCode.setText(champ_customer_code);
//            }
            telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cphone = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            tel.setTypeface(_font);
        }
        if (v == item_name) {
            itemNameAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    private Call<String> callAddBasket(String _skuId) {
        return rudyService.addBasket(
                Utils.APP_LANGUAGE,
                project_id,
                _skuId,
                "1",
                String.valueOf(totalSearchPrice),
                user_id,
                "",
                "",
                "",
                ""
        );
    }


    @SuppressLint("StaticFieldLeak")
    private void AddBasket(String _skuId){
        showLoading(TransactionDetailActivity.this);
        callAddBasket(_skuId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONArray itemArray = object.optJSONArray("items");
                        String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");
                        TransactionDetailActivity.transaction_id = transaction_id;
                        DealByPhaseActivity.transaction_id = transaction_id;


                        getTransactionList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(resp, e);
                    }
                    showToast(TransactionDetailActivity.this, getResources().getString(R.string.dialog_add_product) + title + getResources().getString(R.string.dialog_add_to_cart_success));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(TransactionDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(TransactionDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void AddBasket(final String url, String skuId) {
//        boolean isAddBudget = false;
//        for (int i = 0; i < cartModelArrayLis.size(); i++) {
//            if (cartModelArrayLis.get(i).getProductId().equalsIgnoreCase(skuId)) {
//                isAddBudget = true;
//                title = cartModelArrayLis.get(i).getProductName();
//            }
//        }
//
//        if (isAddBudget) {
//            showToast(this, getResources().getString(R.string.dialog_have_product) + title + getResources().getString(R.string.dialog_alrady_in_cart));
//        } else {
//            showLoading(this);
//            final RequestBody requestBody = new FormBody.Builder()
//                    .add("product_id", skuId)
//                    .add("qty", "1")
//                    .add("price", String.valueOf(totalSearchPrice))
//                    .add("user_id", user_id)
//                    .add("note", "")
//                    .build();
//            new AsyncTask<Void, Void, String>() {
//                @Override
//                protected String doInBackground(Void... voids) {
//                    String response = "error";
//                    response = Utils.postData(url, requestBody);
//                    return response;
//                }
//
//                @Override
//                protected void onPostExecute(String string) {
//                    super.onPostExecute(string);
//                    hideLoading();
//                    if (string.contains("200")) {
//                        try {
//                            JSONObject object = new JSONObject(string);
//                            JSONArray itemArray = object.optJSONArray("items");
//                            String transaction_id = itemArray.getJSONObject(0).optString("transaction_id", "");
//                            TransactionDetailActivity.transaction_id = transaction_id;
//                            DealByPhaseActivity.transaction_id = transaction_id;
//
//
//                            getTransactionList();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            LogException(url, e);
//                        }
//                        showToast(TransactionDetailActivity.this, getResources().getString(R.string.dialog_add_product) + title + getResources().getString(R.string.dialog_add_to_cart_success));
//                    }
//
//                }
//            }.execute();
//        }
//    }
}
