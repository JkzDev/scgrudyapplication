package com.scg.rudy.ui.add_new_project.Models;

import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;

import java.util.ArrayList;

public class StorageDataOfflineModel {

    public Double getDragLat() {
        return DragLat;
    }

    public void setDragLat(Double dragLat) {
        DragLat = dragLat;
    }

    public Double getDragLng() {
        return DragLng;
    }

    public void setDragLng(Double dragLng) {
        DragLng = dragLng;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public ArrayList<GroupGalleryModel> getmArrayList() {
        return mArrayList;
    }

    public void setmArrayList(ArrayList<GroupGalleryModel> mArrayList) {
        this.mArrayList = mArrayList;
    }

    public String getUnit_number() {
        return unit_number;
    }

    public void setUnit_number(String unit_number) {
        this.unit_number = unit_number;
    }

    public String getFloor_number() {
        return floor_number;
    }

    public void setFloor_number(String floor_number) {
        this.floor_number = floor_number;
    }

    public String getUnit_area() {
        return unit_area;
    }

    public void setUnit_area(String unit_area) {
        this.unit_area = unit_area;
    }

    public String getUnit_budget() {
        return unit_budget;
    }

    public void setUnit_budget(String unit_budget) {
        this.unit_budget = unit_budget;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getCateId() {
        return cateId;
    }

    public void setCateId(String cateId) {
        this.cateId = cateId;
    }

    public String getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(String phaseId) {
        this.phaseId = phaseId;
    }

    public int getCatePosition() {
        return catePosition;
    }

    public void setCatePosition(int catePosition) {
        this.catePosition = catePosition;
    }

    public int getPhasePosition() {
        return phasePosition;
    }

    public void setPhasePosition(int phasePosition) {
        this.phasePosition = phasePosition;
    }

    public int getMaxPhase() {
        return maxPhase;
    }

    public void setMaxPhase(int maxPhase) {
        this.maxPhase = maxPhase;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getChamp_customer_code() {
        return champ_customer_code;
    }

    public void setChamp_customer_code(String champ_customer_code) {
        this.champ_customer_code = champ_customer_code;
    }

    public String getCustomer_company() {
        return customer_company;
    }

    public void setCustomer_company(String customer_company) {
        this.customer_company = customer_company;
    }

    public String getCcompany() {
        return ccompany;
    }

    public void setCcompany(String ccompany) {
        this.ccompany = ccompany;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getCustomer_tax_no() {
        return customer_tax_no;
    }

    public void setCustomer_tax_no(String customer_tax_no) {
        this.customer_tax_no = customer_tax_no;
    }

    public String getCustomer_line() {
        return customer_line;
    }

    public void setCustomer_line(String customer_line) {
        this.customer_line = customer_line;
    }

    public String getCustomer_note() {
        return customer_note;
    }

    public void setCustomer_note(String customer_note) {
        this.customer_note = customer_note;
    }

    public String getHouse_owner_name() {
        return house_owner_name;
    }

    public void setHouse_owner_name(String house_owner_name) {
        this.house_owner_name = house_owner_name;
    }

    public String getCcompany_owner() {
        return ccompany_owner;
    }

    public void setCcompany_owner(String ccompany_owner) {
        this.ccompany_owner = ccompany_owner;
    }

    public String getHouse_owner_phone() {
        return house_owner_phone;
    }

    public void setHouse_owner_phone(String house_owner_phone) {
        this.house_owner_phone = house_owner_phone;
    }

    public String getHouse_owner_line() {
        return house_owner_line;
    }

    public void setHouse_owner_line(String house_owner_line) {
        this.house_owner_line = house_owner_line;
    }

    public String getHouse_owner_note() {
        return House_owner_note;
    }

    public void setHouse_owner_note(String house_owner_note) {
        House_owner_note = house_owner_note;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public int getCtype_owner() {
        return ctype_owner;
    }

    public void setCtype_owner(int ctype_owner) {
        this.ctype_owner = ctype_owner;
    }

    public String getProject_owner_name() {
        return project_owner_name;
    }

    public void setProject_owner_name(String project_owner_name) {
        this.project_owner_name = project_owner_name;
    }

    public String getProject_owner_phone() {
        return project_owner_phone;
    }

    public void setProject_owner_phone(String project_owner_phone) {
        this.project_owner_phone = project_owner_phone;
    }

    public String getProject_owner_line() {
        return project_owner_line;
    }

    public void setProject_owner_line(String project_owner_line) {
        this.project_owner_line = project_owner_line;
    }

    public String getProject_owner_note() {
        return project_owner_note;
    }

    public void setProject_owner_note(String project_owner_note) {
        this.project_owner_note = project_owner_note;
    }

    //for add customer
    private String customer_name;
    private String customer_code;
    private String champ_customer_code;
    private String customer_company;
    private String ccompany;
    private String customer_phone;
    private String customer_tax_no;
    private String customer_line;
    private String customer_note;
    private String house_owner_name;
    private String ccompany_owner;
    private String house_owner_phone;
    private String house_owner_line;
    private String House_owner_note;
    private int ctype;
    private int ctype_owner;
    private String project_owner_name;
    private String project_owner_phone;
    private String project_owner_line;
    private String project_owner_note;


    //for add department
    private String unit_number;
    private String floor_number;
    private String unit_area;
    private String unit_budget;
    private String project_name;
    private String cateId;
    private String phaseId;
    private int catePosition;
    private int phasePosition;
    private int maxPhase ;

    //for add photo
    public ArrayList<GroupGalleryModel> mArrayList;

    //for add location
    private Double DragLat;
    private Double DragLng;
    private String Address;

}
