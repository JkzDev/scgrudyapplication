package com.scg.rudy.ui.checkin_gallery;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.ui.adapter.GridViewAdapter;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.checkin_detail.CheckInDetail;
import com.scg.rudy.model.pojo.checkin_detail.GalleryItem;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.department_photo.Adapter.GroupShowPhotoAdapter;
import com.scg.rudy.ui.department_photo.TakePhotoInterface;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;
import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_ui.floatingActionButton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CheckInGalleryActivity extends BaseActivity implements View.OnClickListener, TakePhotoInterface {
    private static final int REQUEST_CAMERA = 0;
    @BindView(R.id.cls_btn)
    ImageView clsBtn;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.save)
    LinearLayout save;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.txt_alert)
    TextView txtAlert;
    @BindView(R.id.btnSelectPhoto)
    FloatingActionButton btnSelectPhoto;
    @BindView(R.id.reShowPhoto)
    RecyclerView reShowPhoto;

    private Bundle extras;
    private String checkInID;
    private RudyService rudyService;

    private int SELECT_FILE = 1;
    private String userChoosenTask;
    private ArrayList<GroupGalleryModel> mArrayList;
    private GroupShowPhotoAdapter adapter;
    private Point mSize;
    private String TAG = "CheckInGalleryActivity";
    private String projectId = "";
    private Display display;

    ArrayList<String> returnValue = new ArrayList<>();
    Options options;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);
        ButterKnife.bind(this);
        initView();
        getUserData();
        clsBtn.setOnClickListener(this);

        mArrayList = new ArrayList<>();

        Display display = CheckInGalleryActivity.this.getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

        btnSelectPhoto.setVisibility(View.VISIBLE);
        btnSelectPhoto.setOnClickListener(this);
        save.setOnClickListener(this);
        clsBtn.setOnClickListener(this);

        rudyService = ApiHelper.getClient();
        options = Options.init()
                .setRequestCode(100)
                .setCount(5)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.LOW)
                .setPreSelectedUrls(returnValue)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                .setPath("/akshay/new");

        getCheckInDetail();

    }

    private Call<String> callGetCheckinDetail() {
        return rudyService.getCheckInDetail(
                Utils.APP_LANGUAGE,
                checkInID
        );
    }

    private void getCheckInDetail() {
        showLoading(this);
        callGetCheckinDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){

                    mArrayList = new ArrayList<>();

                    CheckInDetail obj  = gson.fromJson(resp, CheckInDetail.class);
                    List<GalleryItem> galleryItemList =   customPhaseprocessResults(obj);

                    if (galleryItemList.size() > 0) {
                        save.setVisibility(View.VISIBLE);
                        txtAlert.setVisibility(View.GONE);

                        ArrayList<GalleryModel> galleryModel = new ArrayList<>();

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                        String currentDateandTime = sdf.format(new Date());

                        for (int i = 0; i < galleryItemList.size(); i++) {
                            if (mArrayList.size() > 0){
                                mArrayList.get(0).getmValue().add(new GalleryModel(galleryItemList.get(i).getId(), galleryItemList.get(i).getName(), currentDateandTime));
                            }else{
                                galleryModel.add(new GalleryModel(galleryItemList.get(i).getId(), galleryItemList.get(i).getName(), currentDateandTime));
                            }
                        }

                        if (mArrayList.size() <= 0){
                            mArrayList.add(new GroupGalleryModel("", galleryModel));
                        }

                        save.setVisibility(View.VISIBLE);
                        txtAlert.setVisibility(View.GONE);

                        adapter = new GroupShowPhotoAdapter(CheckInGalleryActivity.this, mArrayList, CheckInGalleryActivity.this);
                        reShowPhoto.setLayoutManager(new LinearLayoutManager(CheckInGalleryActivity.this));
                        reShowPhoto.setAdapter(adapter);

                    } else {
                        adapter = new GroupShowPhotoAdapter(CheckInGalleryActivity.this, mArrayList, CheckInGalleryActivity.this);
                        reShowPhoto.setLayoutManager(new LinearLayoutManager(CheckInGalleryActivity.this));
                        reShowPhoto.setAdapter(adapter);
                        save.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInGalleryActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInGalleryActivity.this, t.getMessage());
                txtAlert.setVisibility(View.VISIBLE);
                save.setVisibility(View.GONE);
            }
        });
    }

//    private void setPhaseData(List<GalleryItem> galleryItemList){
//        if (galleryItemList.size() > 0) {
//            save.setVisibility(View.VISIBLE);
//            txtAlert.setVisibility(View.GONE);
//            for (int i = 0; i < galleryItemList.size(); i++) {
//                nameList.add(galleryItemList.get(i).getPicName());
////                final String image = galleryItemList.get(i).getreplace(ApiEndPoint.replaceImageName, "");
//                Glide.with(getApplicationContext()).asBitmap().load(galleryItemList.get(i).getName()).into(new SimpleTarget<Bitmap>() {
//                    @Override
//                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
//                        mArrayList.add(resource);
//                        adapter = new GridViewAdapter(CheckInGalleryActivity.this, mArrayList, CheckInGalleryActivity.this);
//                        gridView.setAdapter(adapter);
//                    }
//                });
//            }
//
//        } else {
//            txtAlert.setVisibility(View.VISIBLE);
//            save.setVisibility(View.GONE);
//        }
//    }

    private List<GalleryItem> phaseprocessResults(Response<CheckInDetail> response) {
        CheckInDetail checkInDetail = response.body();
        return checkInDetail.getItems().getGallery();
    }

    private List<GalleryItem> customPhaseprocessResults(CheckInDetail checkInDetail) {
        return checkInDetail.getItems().getGallery();
    }

//    private void onCaptureImageResult(Intent data) {
//        Uri photoUri = data.getData();
//        Bitmap thumbnail = ImageUtility.decodeSampledBitmapFromPath(photoUri.getPath(), mSize.x, mSize.x);
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
//        FileOutputStream fo;
//        try {
//            destination.createNewFile();
//            fo = new FileOutputStream(destination);
//            fo.write(bytes.toByteArray());
//            fo.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mArrayList.add(thumbnail);
//        if (mArrayList.size() > 0) {
//            save.setVisibility(View.VISIBLE);
//            txtAlert.setVisibility(View.GONE);
//        } else {
//            txtAlert.setVisibility(View.VISIBLE);
//        }
//        addPic(ApiEndPoint.getPicCheckin(Utils.APP_LANGUAGE),thumbnail);
//    }

    public void requestForCameraPermission(View view) {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(CheckInGalleryActivity.this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(CheckInGalleryActivity.this, permission)) {
                showPermissionRationaleDialog(getResources().getString(R.string.dialog_pls_allow_to_access_camera), permission);
            } else {
                requestForPermission(permission);
            }
        } else {
            launch();
        }

    }

    private void showPermissionRationaleDialog(final String message, final String permission) {
        new AlertDialog.Builder(CheckInGalleryActivity.this)
                .setMessage(message)
                .setPositiveButton("Ok", (dialog, which) -> requestForPermission(permission))
                .setNegativeButton("Cancel", (dialog, which) -> {
                    return;
                })
                .create()
                .show();
    }

    private Call<String> callDeletePic(String _CheckinID) {
        return rudyService.deletePicCheckIn(
                Utils.APP_LANGUAGE,
                _CheckinID

        );
    }


    public void deletePic(String _CheckinID) {
        showLoading(this);
        callDeletePic(_CheckinID).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    getCheckInDetail();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInGalleryActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(CheckInGalleryActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void deletePic(final String url) {
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return Utils.postData(url);
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    getCheckInDetail();
//                }
//            }
//        }.execute();
//    }


    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(CheckInGalleryActivity.this, new String[]{permission}, REQUEST_CAMERA);
    }

    private void launch() {
        Pix.start(CheckInGalleryActivity.this, options);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                final int numOfRequest = grantResults.length;
                final boolean isGranted = numOfRequest == 1
                        && PackageManager.PERMISSION_GRANTED == grantResults[numOfRequest - 1];
                if (isGranted) {
                    launch();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void initView(){
        extras = getIntent().getExtras();
        clsBtn = findViewById(R.id.cls_btn);
        extras = getIntent().getExtras();
        if (extras != null) {
            checkInID = extras.getString("checkInID");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v == save) {
            finish();
        }

        if (v == btnSelectPhoto) {
            requestForCameraPermission(btnSelectPhoto);
        }

        if (v == clsBtn) {
            finish();
        }
    }

    @Override
    public void onClickDeletePhoto(String img) {

        for (int i = 0; i < mArrayList.size() ; i++) {
            for (int j = 0; j < mArrayList.get(i).getmValue().size() ; j++) {
                if (mArrayList.get(i).getmValue().get(j).getName().equalsIgnoreCase(img)){
                    deletePic(mArrayList.get(i).getmValue().get(j).getCkId());
                }
            }
        }
    }

    @Override
    public void onShowPhoto(String img) {
        final Dialog dialog = new Dialog(CheckInGalleryActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        dialog.setContentView(R.layout.view_image);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
        dialog.getWindow().setLayout(width, height);
        SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
        Button close = dialog.findViewById(R.id.close);
        RelativeLayout hideView = dialog.findViewById(R.id.hideView);

        new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... voids) {
                if (Utils.project_id.length() > 0){
                    try {
                        URL url = new URL(img);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        Bitmap myBitmap = BitmapFactory.decodeStream(input);
                        return myBitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }else{
                    File f = new File(img);
                    Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                    return d;
                }
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                image.setImage(ImageSource.bitmap(bitmap));
                image.setMaxScale(20.0f);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                hideView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }.execute();
    }

    private String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Bitmap> listBm = new ArrayList<>();
        List<String> listImg = new ArrayList<>();
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    for (int i = 0; i < returnValue.size(); i++){
                        File f = new File(returnValue.get(i));
                        Bitmap d = new BitmapDrawable(getResources(), f.getAbsolutePath()).getBitmap();
                        listBm.add(d);
                    }

                    if (listBm.size() > 0){
                        for (int i = 0; i < listBm.size(); i++){
                            String img = "data:image/jpg;base64," + BitMapToString(listBm.get(i));
                            listImg.add(img);
                        }
                        addPic(listImg);
                    }
                }
            }
            break;
        }
    }

    private Call<String> callAddPicCheckin(List<String> listPic) {
        return rudyService.addPicCheckin(
                Utils.APP_LANGUAGE,
                checkInID,
                listPic
        );
    }

    private void addPic(List<String> _listImg){
        showLoading(this);
        if (_listImg.size() > 0){
            callAddPicCheckin(_listImg).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideLoading();
                    Gson gson = new Gson();
                    String resp = response.body();
                    if(resp.contains("200")){
                        getCheckInDetail();
                        showToast(CheckInGalleryActivity.this, getResources().getString(R.string.toast_save_image_success));
                    }else{
                        APIError error  = gson.fromJson(resp, APIError.class);
                        showToast(CheckInGalleryActivity.this, error.getItems());
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    hideLoading();
                    showToast(CheckInGalleryActivity.this, t.getMessage());
                }
            });
        }
    }

    public boolean isNetworkConnected() {
        return true;
    }
}
