package com.scg.rudy.ui.call_to_customer;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.scg.rudy.R;
import com.scg.rudy.ui.call_to_customer.Adapter.HideContentAdapter;
import com.scg.rudy.ui.call_to_customer.Models.HideContentModel;
import com.scg.rudy.ui.product_recommend.Adapter.RelatedProductsAdapter;
import com.scg.rudy.ui.product_recommend.Models.RelatedProductsModel;

import java.util.ArrayList;

public class CallToCustomerActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAll;
    private Button btnCallSuccess;
    private ImageView imageHead;
    private ImageButton btnShowContent;
    private RecyclerView revHideContent;
    private TextView txtHideAndShow;
    private LinearLayout lineHead;
    private LinearLayout liProductRelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_to_customer);

        initView();
        setOnclikc();

        //set default fragment
        CallAllFragment callAllFragment = new CallAllFragment();
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.CallFragment, callAllFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        //set image header
        imageHead.setImageDrawable(getResources().getDrawable(R.drawable.thai_tag));

        //hide content
        revHideContent.setVisibility(View.GONE);
        liProductRelative.setVisibility(View.GONE);

        //set adapter hide content
        RecyclerView recyclerView = findViewById(R.id.revHideContent);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.egg));
        ArrayList<HideContentModel> hideContentModels = new ArrayList<>();
        hideContentModels.add(new HideContentModel("“Fiber Cement Hollow core แพรงค์คอนกรีตช่วงพาดยาว”", "Fiber cement หรือที่รู้จักในนาม ซีเมนต์เสริมใยแก้ว ด้วยคุณสมบัติของใยแก้วที่มีอัตราการรับแรงดึงสูง เสมือนเหล็กในน้ำหนักที่เบากว่าทำให้ซีเมนต์เสริมใยแก้วเหมือนเป็น ซีเมนต์เสริมเหล็กในตัวสามารถใช้ ทำผนังรับน้ำหนักหรือ Wall bearing ได้อย่างสบายโดยลดขนาดเล็กลงได้ถึง 4mm"));
        hideContentModels.add(new HideContentModel("01. เคยใช้สินค้าชนิดนี้หรือไม่", "Fiber cement หรือที่รู้จักในนาม ซีเมนต์เสริมใยแก้ว ด้วยคุณสมบัติของใยแก้ว"));
        hideContentModels.add(new HideContentModel("01. เคยใช้สินค้าชนิดนี้หรือไม่", "Fiber cement หรือที่รู้จักในนาม ซีเมนต์เสริมใยแก้ว ด้วยคุณสมบัติของใยแก้ว"));
        hideContentModels.add(new HideContentModel("01. เคยใช้สินค้าชนิดนี้หรือไม่", "Fiber cement หรือที่รู้จักในนาม ซีเมนต์เสริมใยแก้ว ด้วยคุณสมบัติของใยแก้ว"));
        HideContentAdapter adapter = new HideContentAdapter(this, hideContentModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //set adapter hide product relative
        ArrayList<RelatedProductsModel> relatedProductsModels = new ArrayList<>();
        relatedProductsModels.add(new RelatedProductsModel(null, "ครอบตะเข้สัน คอนกรีต รุ่นเพรสทีจ สีทรอปปิคอล กรีน"));
        relatedProductsModels.add(new RelatedProductsModel(null, "ครอบตะเข้สัน คอนกรีต รุ่นเพรสทีจ สีทรอปปิคอล กรีน"));
        RecyclerView recProductRelative = findViewById(R.id.recProductRelative);
        RelatedProductsAdapter adapterProductRelative = new RelatedProductsAdapter(this, relatedProductsModels);
        recProductRelative.setAdapter(adapterProductRelative);
        recProductRelative.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recProductRelative);
    }

    private void setOnclikc() {
        btnAll.setOnClickListener(this);
        btnCallSuccess.setOnClickListener(this);
        btnShowContent.setOnClickListener(this);

    }

    private void initView() {
        btnAll = (Button) findViewById(R.id.btnAll);
        btnCallSuccess = (Button) findViewById(R.id.btnCallSuccess);
        imageHead = (ImageView) findViewById(R.id.imageHead);
        btnShowContent = (ImageButton) findViewById(R.id.btnShowContent);
        revHideContent = (RecyclerView) findViewById(R.id.revHideContent);
        txtHideAndShow = (TextView) findViewById(R.id.txtHideAndShow);
        lineHead = (LinearLayout) findViewById(R.id.lineHead);
        liProductRelative = (LinearLayout) findViewById(R.id.liProductRelative);
    }

    @Override
    public void onClick(View v) {

        if (v == btnAll) {
            CallAllFragment callAllFragment = new CallAllFragment();
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.CallFragment, callAllFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            btnAll.setTextColor(getResources().getColor(R.color.white));
            btnAll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            btnCallSuccess.setTextColor(getResources().getColor(R.color.colorPrimary));
            btnCallSuccess.setBackgroundColor(getResources().getColor(R.color.white));
        }

        if (v == btnCallSuccess) {
            CallSuccessFragment callSuccessFragment = new CallSuccessFragment();
            FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.CallFragment, callSuccessFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            btnCallSuccess.setTextColor(getResources().getColor(R.color.white));
            btnCallSuccess.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            btnAll.setTextColor(getResources().getColor(R.color.colorPrimary));
            btnAll.setBackgroundColor(getResources().getColor(R.color.white));
        }

        if (v == btnShowContent) {
            if (revHideContent.isShown()) {
                revHideContent.setVisibility(View.GONE);
                liProductRelative.setVisibility(View.GONE);
                btnShowContent.setImageDrawable(getResources().getDrawable(R.drawable.expand_arrow_down));
                txtHideAndShow.setText("แดสงรายละเอียด");

            } else {
                revHideContent.setVisibility(View.VISIBLE);
                liProductRelative.setVisibility(View.VISIBLE);
                btnShowContent.setImageDrawable(getResources().getDrawable(R.drawable.expand_arrow_up));
                txtHideAndShow.setText("ซ่อนรายละเอียด");
            }
        }
    }
}
