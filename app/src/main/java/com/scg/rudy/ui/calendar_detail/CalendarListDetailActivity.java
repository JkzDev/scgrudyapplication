package com.scg.rudy.ui.calendar_detail;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.calendar_detail.CalendarDetail;
import com.scg.rudy.model.pojo.calendar_detail.Items;
import com.scg.rudy.model.pojo.calendar_detail.Project;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.VerticalDotedView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CalendarListDetailActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.cls_btn)
    ImageView clsBtn;
    @BindView(R.id.title_name)
    TextView titleName;
    @BindView(R.id.navbar)
    LinearLayout navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageView10)
    ImageView imageView10;
    @BindView(R.id.edit)
    RelativeLayout edit;
    @BindView(R.id.status)
    RelativeLayout status;
    @BindView(R.id.appoint_txt)
    TextView appointTxt;
    @BindView(R.id.appoint_name)
    TextView appointName;
    @BindView(R.id.imageStat)
    ImageView imageStat;
    @BindView(R.id.line)
    VerticalDotedView line;
    @BindView(R.id.day)
    TextView day;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.month)
    TextView month;
    @BindView(R.id.year)
    TextView year;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.detail)
    TextView detail;
    @BindView(R.id.title_project)
    TextView titleProject;
    @BindView(R.id.date_project)
    TextView dateProject;
    @BindView(R.id.detail_project)
    TextView detailProject;
    @BindView(R.id.no_gallery)
    LinearLayout noGallery;
    @BindView(R.id.gallery)
    LinearLayout gallery;
    @BindView(R.id.imageMap)
    ImageView imageMap;
    @BindView(R.id.edit_location)
    RelativeLayout editLocation;
    @BindView(R.id.project_image)
    LinearLayout projectImage;
    @BindView(R.id.contact)
    RelativeLayout contact;
    @BindView(R.id.cus_name)
    TextView cusName;
    @BindView(R.id.cus_type)
    TextView cusType;
    @BindView(R.id.cus_image)
    CircleImageView cusImage;



    private String calendarID;
    private String type;
    private Bundle extras;
    private RudyService rudyService;
    private ArrayList<String> nameList;
    private ArrayList<Bitmap> mArrayList;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_list_detail);
        ButterKnife.bind(this);
        extras = getIntent().getExtras();
        rudyService = ApiHelper.getClient();
        if (extras != null) {
            calendarID  = extras.getString("calendarID");
            type = extras.getString("type");
        }
        getCheckInDetail();

        clsBtn.setOnClickListener(this);
    }


    private Call<String> getCalendarListDetail() {
        return rudyService.getCalendarDetail(Utils.APP_LANGUAGE, calendarID,"detail");
    }

    private void getCheckInDetail() {
        showLoading(this);
        getCalendarListDetail()
                .enqueue(
                        new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                hideLoading();
                                Gson gson = new Gson();
                                String resp = response.body();
                                if(resp.contains("200")){
                                    CalendarDetail calendarDetail  = gson.fromJson(resp, CalendarDetail.class);
                                    setCalendarDetailData(calendarDetail);
                                }else{
                                    APIError error  = gson.fromJson(resp, APIError.class);
                                    showToast(CalendarListDetailActivity.this, error.getItems());
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                hideLoading();
                                t.printStackTrace();
                                showToast(CalendarListDetailActivity.this, fetchErrorMessage(t));
                            }
                        });
    }

    private void setCalendarDetailData(CalendarDetail calendarDetail) {
        Items data = calendarDetail.getItems();
        Project projectData = data.getProject();

//        Utils.showToast(this,calendarDetail.toString());
        
        appointTxt.setText(data.getTypeTxt());
        appointName.setText(projectData.getProjectName());
        day.setText(data.getDayName());
        date.setText(data.getDay());
        month.setText(data.getMonthName());
        year.setText(data.getYear());

        time.setText(data.getStartTime()+"-"+data.getEndTime());
        time.setSelected(true);
        time.setEllipsize(TextUtils.TruncateAt.MARQUEE);

        txtTitle.setText(data.getTitle());
        detail.setText(data.getDescription());
        titleProject.setText(projectData.getProjectName());
        String[] addDate = projectData.getAddedDatetime().split(" ");

        dateProject.setText(getResources().getString(R.string.create_on) + addDate[0]);
        detailProject.setText(getResources().getString(R.string.value_project) + projectData.getUnitBudget() + getResources().getString(R.string.million) + Utils.currencyForShow);

        if(type.equalsIgnoreCase("1")){
            imageStat.setImageResource(R.drawable.cal_visit);
            status.setBackgroundColor(Color.parseColor("#7cb342"));
        }else if(type.equalsIgnoreCase("2")){
            imageStat.setImageResource(R.drawable.cal_sale);
            status.setBackgroundColor(Color.parseColor("#7b5ce2"));
        }else{
            imageStat.setImageResource(R.drawable.cal_other);
            status.setBackgroundColor(Color.parseColor("#109ffc"));
        }

        if (projectData.getLatLng().length() > 0) {
            Glide.with(this)
                    .load("https://maps.googleapis.com/maps/api/staticmap?center=" + projectData.getLatLng() + "&zoom=18&size=350x350&key=" + this.getResources().getString(R.string.google_maps_key))
                    .into(imageMap);
        }


        projectImage.removeAllViews();
        nameList = new ArrayList<>();
        mArrayList = new ArrayList<>();
        for (int i = 0; i < projectData.getGallery().size(); i++) {
            final String image = projectData.getGallery().get(i).replace(ApiEndPoint.replaceImageName, "");
            Glide.with(this).asBitmap().load(projectData.getGallery().get(i)).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    nameList.add(image);
                    mArrayList.add(resource);
                }
            });

            ImageView iv = new ImageView(this);
            iv.setAdjustViewBounds(true);
            Glide.with(this)
                    .load(projectData.getGallery().get(i))
                    .into(iv);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 64);
            params.width = (int) getResources().getDimension(R.dimen._45sdp);
            params.height = (int) getResources().getDimension(R.dimen._45sdp);
            iv.setLayoutParams(params);
            iv.setAdjustViewBounds(true);
            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Glide.with(CalendarListDetailActivity.this).asBitmap().load(projectData.getGallery().get(finalI)).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            onShowPhoto(resource);
                        }
                    });
                }
            });

            iv.setPadding(0, 0, 13, 0);
//            setMargins(iv, 10, 10, 10, 10);
            projectImage.addView(iv);
        }

       if( projectData.getCustomerPhone().length()>5){
           contact.setVisibility(View.VISIBLE);
       }else{
           contact.setVisibility(View.INVISIBLE);
       }

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission(projectData.getCustomerPhone());
            }
        });


//        noGallery;
//        gallery;

//        editLocation;
//        projectImage

//        contact;
        cusName.setText(projectData.getCustomerName());
        cusType.setText(projectData.getCustomerTypeTxt());
        Glide.with(this)
                .load(projectData.getCustomerPic())
                .into(cusImage);
    }

    public void onShowPhoto(Bitmap bitmap) {
        if (bitmap != null) {
            final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.setContentView(R.layout.view_image);
            dialog.setCancelable(true);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
            dialog.getWindow().setLayout(width, height);
            SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
            Button close = dialog.findViewById(R.id.close);
            RelativeLayout hideView = dialog.findViewById(R.id.hideView);
            image.setImage(ImageSource.bitmap(bitmap));
            image.setMaxScale(20.0f);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dialog.dismiss();
                }
            });
            hideView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission(String tel) {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();

        if (!addPermission(this,permissionsList, Manifest.permission.CALL_PHONE)){
            permissionsNeeded.add("Call");
        }
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showMessageOKCancel(this,message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        callCkeckin(tel);
    }


    private void callCkeckin(String tel){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        callIntent.setData(Uri.parse("tel:"+tel));
        startActivity(callIntent);
    }


    private CalendarDetail calendarResults(Response<CalendarDetail> response) {
        return response.body();
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    @Override
    public void onClick(View v) {
        if(v==clsBtn){
            finish();
        }

    }
}
