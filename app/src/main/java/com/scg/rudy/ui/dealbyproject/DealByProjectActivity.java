package com.scg.rudy.ui.dealbyproject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.dealbyproject.catalog.Catalog;
import com.scg.rudy.model.pojo.dealbyproject.classsku.ClassProjectList;
import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.model.pojo.dealbyproject.group.GroupProject;
import com.scg.rudy.model.pojo.dealbyproject.other_type.CatesItem;
import com.scg.rudy.model.pojo.dealbyproject.other_type.ClassesItem;
import com.scg.rudy.model.pojo.dealbyproject.other_type.PhaseCate;
import com.scg.rudy.model.pojo.dealbyproject.phaseList.ItemsItem;
import com.scg.rudy.model.pojo.dealbyproject.phaseList.PhaseList;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.catalogs.CatalogsActivity;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.choose_sku.SearchSKUitemsActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.favorite.FavoriteDetailActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.sku_confirm.SkuConfirmActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.data.remote.ApiEndPoint.getGetBasket;
import static com.scg.rudy.data.remote.ApiEndPoint.getGetFav;
import static com.scg.rudy.ui.dealbyproject.ClassDataFactory.makeClassSku;
import static com.scg.rudy.ui.sku_confirm.SkuConfirmActivity.sub_class_name;
import static com.scg.rudy.utils.Utils.onShowPromotion;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */

public class DealByProjectActivity extends BaseActivity implements View.OnClickListener,byProjectInterface {
    private RudyService rudyService;
    private View lastLabelClicked;
    private RelativeLayout cart;
    private RelativeLayout badge;
    private TextView numberBadge;
    private ImageView clsBtn;
    private ImageView searchSku;
    private ImageView catalog;
    private RelativeLayout fav;
    private RelativeLayout badgeFav;
    private TextView numberFav;
    private RecyclerView groupRecycler;
    private RecyclerView skuRecycler;
    private RelativeLayout badge_fav;
    private TextView number_fav;
    private String subPhaseId;
    private int basketSize = 0;
    private String transaction_id = "";
    private String cateID;
    private ClassProjectAdapter classAdapter;
    private Bundle extras;
    private LinearLayout layoutLine1;
    private LinearLayout layoutLine2;
    private LayoutInflater layoutInflater;
    private RelativeLayout btnSalePhase;
    private RelativeLayout btnSaleType;
    private LinearLayout viewTypeLayout;


    private String projectId;
    private String projectTypeId;
    private String phaseId;
//    
//     intent.putExtra("projectId", Utils.project_id);
//    intent.putExtra("projectTypeId", detailItem.getProjectTypeId());
//    intent.putExtra("phase", detailItem.getPhaseId());
//    
    


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_by_project);
        initView();
        setOnClick();
        extras = getIntent().getExtras();
        if (extras!=null) {
            projectId = extras.getString("projectId", "1");
            projectTypeId = extras.getString("projectTypeId", "");
            phaseId  = extras.getString("phaseId", "");
        }

//        if(projectTypeId.equalsIgnoreCase("11")){
//            viewTypeLayout.setVisibility(View.GONE);
//        }else{
//            viewTypeLayout.setVisibility(View.VISIBLE);
//        }

        projectTypeId = "11";
        Utils.project_id = projectId;
        loadCatalog();
        findViewById(R.id.bhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(DealByProjectActivity.this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });
    }


    private Call<String> getClassProject(String cateId,String subPhaseId) {
        this.cateID = cateId;
        return rudyService.classProject(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                cateId,
                subPhaseId
        );
    }

    private Call<String> getCatalogList() {
        return rudyService.catalogues(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                projectTypeId
        );
    }

    private Call<String> getPhaseCate() {
        return rudyService.catePhase(
                Utils.APP_LANGUAGE,
                projectId,
                "cate_view"
        );
    }

    private Call<String> getPhaseList(String phaseId) {
        this.phaseId = phaseId;
        return rudyService.phaseList(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                phaseId
        );
    }

    private Call<String> getGroupProjectList(String subPhaseId) {
        this.subPhaseId =subPhaseId;
        return rudyService.groupProject(
                Utils.APP_LANGUAGE,
                projectId,
                "v2",
                subPhaseId
        );
    }

    private Call<String> callPromotionDetailApi(String promotionDetailId) {
        return rudyService.getPromotionDetail(
                Utils.APP_LANGUAGE,
                shopID,
                promotionDetailId
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Utils.getPrefer(this,"print").length()>0){
            finish();
        }else{
            updateTranId();
            if(cateID!=null&&subPhaseId!=null){
                loadClassProject(String.valueOf(cateID),subPhaseId);
            }

        }
    }

    private void loadClassProject(String cateId,String subPhaseId) {
        skuRecycler.setAdapter(null);
        showLoading(this);
        getClassProject(cateId,subPhaseId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
               hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ClassProjectList results  = gson.fromJson(resp, ClassProjectList.class);
                    setClassSku(results);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
              hideLoading();
                t.printStackTrace();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });

    }

    private void loadPhaseList(String  phaseId) {
        showLoading(this);
        getPhaseList(phaseId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
               hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    PhaseList results  = gson.fromJson(resp, PhaseList.class);
                    setLabelViewGroup(results.getItems());
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
               hideLoading();
                t.printStackTrace();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });

    }

    private void loadGroupProject(int subPhaseId) {
       showLoading(this);
        getGroupProjectList(String.valueOf(subPhaseId)).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    GroupProject results  = gson.fromJson(resp, GroupProject.class);
                    setGroupList(results.getItems());
                    loadClassProject(results.getItems().get(0).getId(),String.valueOf(subPhaseId));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }

    private ClassProjectList fetchClassProjecResults(Response<ClassProjectList> response) {
        return response.body();
    }

    private PhaseList fetchPhaseListResults(Response<PhaseList> response) {
        return response.body();
    }


    private GroupProject fetchGroupProjectResults(Response<GroupProject> response) {
        return response.body();
    }

    private Catalog fetchCatalogResults(Response<Catalog> response) {
        return response.body();
    }


    private void loadCatalog() {//TODO
        showLoading(this);
            getCatalogList().enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    hideLoading();
                    Gson gson = new Gson();
                    String resp = response.body();
                    if(resp.contains("200")){
                        Catalog results  = gson.fromJson(resp, Catalog.class);
                        loadPhaseList(results.getItems().get(0).getId());
                    }else{
                        APIError error  = gson.fromJson(resp, APIError.class);
                        showToast(DealByProjectActivity.this, error.getItems());
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    hideLoading();
                    t.printStackTrace();
                    showToast(DealByProjectActivity.this, t.getMessage());
                }
            });
    }



    private void setOnClick(){
        clsBtn.setOnClickListener(this);
        catalog.setOnClickListener(this);
        cart.setOnClickListener(this);
        searchSku.setOnClickListener(this);
        cart.setOnClickListener(this);
        fav.setOnClickListener(this);
        btnSalePhase.setOnClickListener(this);
    }


    private void setPhaseGroupList(List<CatesItem> itemList){
        ProjectPhaseGroupRecyclerAdapter adapter = new ProjectPhaseGroupRecyclerAdapter(this, itemList,this);
        groupRecycler.setLayoutManager(new LinearLayoutManager(this));
        groupRecycler.setItemAnimator(new DefaultItemAnimator());
        groupRecycler.setAdapter(adapter);
        setPhaseClassSku(itemList.get(0).getClasses());
    }

    private void setLabelViewGroup(List<ItemsItem> list){
        layoutLine1.removeAllViews();
        layoutLine2.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            final View view1 = layoutInflater.inflate(R.layout.label_project, layoutLine1, false);
            final View view2 = layoutInflater.inflate(R.layout.label_project, layoutLine2, false);
            if(i==0){
                lastLabelClicked = view1;
                ((TextView)lastLabelClicked.findViewById(R.id.tvLabel)).setTextColor(getResources().getColor(R.color.white));
                lastLabelClicked.findViewById(R.id.label_bg).setBackground(getDrawable(R.drawable.bg_frame_project_select));
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.width =0;
            params.height = (int) getResources().getDimension(R.dimen._24sdp);
            params.weight = 1;
            if(i<6){
                view1.setLayoutParams(params);
                view1.setId(Integer.parseInt(list.get(i).getId()));
                TextView valueTV = view1.findViewById(R.id.tvLabel);
                LinearLayout line_project = view1.findViewById(R.id.line_project);
                valueTV.setText(list.get(i).getName());
                valueTV.setSingleLine(true);
                valueTV.setGravity(Gravity.CENTER);
                valueTV.setEllipsize(TextUtils.TruncateAt.END);
                if(i==5){
                    line_project.setVisibility(View.GONE);
                }
                setOnTabClick(view1);
                layoutLine1.addView(view1);

            }else{
                view2.setLayoutParams(params);
                view2.setId(Integer.parseInt(list.get(i).getId()));
                TextView valueTV = view2.findViewById(R.id.tvLabel);
                LinearLayout line_project = view2.findViewById(R.id.line_project);
                valueTV.setText(list.get(i).getName());
                valueTV.setSingleLine(true);
                valueTV.setGravity(Gravity.CENTER);
                valueTV.setEllipsize(TextUtils.TruncateAt.END);
                if(i==list.size()-1){
                    line_project.setVisibility(View.GONE);
                }
                setOnTabClick(view2);
                layoutLine2.addView(view2);
            }
        }

        loadGroupProject(lastLabelClicked.getId());

    }

    private void setOnTabClick(View tabClickview){
        tabClickview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TextView)lastLabelClicked.findViewById(R.id.tvLabel)).setTextColor(getResources().getColor(R.color.black));
                lastLabelClicked.findViewById(R.id.label_bg).setBackground(getDrawable(R.drawable.bg_project_normal));

                ((TextView)tabClickview.findViewById(R.id.tvLabel)).setTextColor(getResources().getColor(R.color.white));
                tabClickview.findViewById(R.id.label_bg).setBackground(getDrawable(R.drawable.bg_frame_project_select));

                showMessage(((TextView)tabClickview.findViewById(R.id.tvLabel)).getText().toString());
                loadGroupProject(tabClickview.getId());
                lastLabelClicked = tabClickview;
            }
        });
    }

    private void setClassSku(ClassProjectList projectList){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        skuRecycler.setLayoutManager(layoutManager);
        classAdapter = new ClassProjectAdapter(makeClassSku(projectList),this);
        skuRecycler.setAdapter(classAdapter);
    }

    private void setGroupList(List<com.scg.rudy.model.pojo.dealbyproject.group.ItemsItem> itemList){
        ProjectGroupRecyclerAdapter adapter = new ProjectGroupRecyclerAdapter(this, itemList,this);
        groupRecycler.setLayoutManager(new LinearLayoutManager(this));
        groupRecycler.setItemAnimator(new DefaultItemAnimator());
        groupRecycler.setAdapter(adapter);
    }

    public void showMessage(String message) {
        showToast(this,message);
    }

    private void initView() {
        rudyService = ApiHelper.getClient();
        layoutInflater = LayoutInflater.from(this);
        cart = findViewById(R.id.cart);
        badge = findViewById(R.id.badge);
        numberBadge = findViewById(R.id.number_badge);
        clsBtn = findViewById(R.id.cls_btn);
        searchSku = findViewById(R.id.search_sku);
        catalog = findViewById(R.id.catalog);
        fav = findViewById(R.id.fav);
        badge_fav = findViewById(R.id.badge_fav);
        number_fav = findViewById(R.id.number_fav);
        groupRecycler = findViewById(R.id.group_recycler);
        skuRecycler = findViewById(R.id.sku_recycler);
//        setAutoLabelUISettings();

        layoutLine1 = findViewById(R.id.layout_line1);
        layoutLine2 = findViewById(R.id.layout_line2);
        btnSalePhase = findViewById(R.id.btn_sale_phase);
        btnSaleType = findViewById(R.id.btn_sale_type);
        viewTypeLayout = findViewById(R.id.view_type_layout);
    }

    @Override
    public void onClick(View v) {

        if(v==clsBtn){
            finish();
        }

        if(v==catalog){
            Intent intent = new Intent(this, CatalogsActivity.class);
            startActivity(intent);
        }

        if(v==searchSku){
            Intent intent = new Intent(this, SearchSKUitemsActivity.class);
            intent.putExtra("shopId", shopID);
            intent.putExtra("phase_id", "");
            startActivity(intent);
        }

        if (v == cart) {
            if(basketSize>0){
                TransactionDetailActivity.productId = "";
                TransactionDetailActivity.isDelete = false;
                Intent intent = new Intent(this, TransactionDetailActivity.class);
                TransactionDetailActivity.from_fav = 0;
                intent.putExtra("transaction_id",transaction_id);
                startActivity(intent);

            }else{
                showToast(this,getResources().getString(R.string.toast_pls_add_product));
            }
        }

        if(v==fav){
            if(BaseActivity.favItemSize>0){
                TransactionDetailActivity.from_fav = 1;
                FavoriteDetailActivity.productId = "";
                FavoriteDetailActivity.isDelete = false;
                Intent intent = new Intent(this, FavoriteDetailActivity.class);
                intent.putExtra("transaction_id",transaction_id);
                startActivity(intent);
            }else{
                showToast(this,getResources().getString(R.string.toast_pls_add_product_interested));
            }
        }

        if(v==btnSalePhase){
            if(projectTypeId.equalsIgnoreCase("11")){
                projectTypeId = "1";
                phaseId = "1";
            }
            Intent intent = new Intent(this, DealByPhaseActivity.class);
            intent.putExtra("projectId", projectId);

            intent.putExtra("projectTypeId", projectTypeId);
            intent.putExtra("phase", phaseId);
            intent.putExtra("transaction_id", transaction_id);
            startActivity(intent);

            overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
            finish();
        }


    }

    @Override
    public void onClickCateItem(String cate) {
        cateID = cate;
        loadClassProject(String.valueOf(cateID),subPhaseId);
    }

    @Override
    public void onClickPhaseCateItem(List<ClassesItem> classes) { //TODO
//        showMessage(classes.get(0).getSku().toString());

        setPhaseClassSku(classes);
    }

    private void setPhaseClassSku(List<ClassesItem> classes){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        skuRecycler.setLayoutManager(layoutManager);
        classAdapter = new ClassProjectAdapter(makeClassSku(classes),this);
        skuRecycler.setAdapter(classAdapter);
    }


    @Override
    public void onClickFavCate(int type,boolean add,String cateId) {
            if(add){
                skuFAV(cateId);

            }else{
                DeleteFAV(cateId);
            }
        classAdapter.notifyDataSetChanged();
    }

    private Call<String> callSkuFav(String _cateId) {
        return rudyService.addFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _cateId,
                user_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void skuFAV(String _cateId){
        showLoading(DealByProjectActivity.this);
        callSkuFav(_cateId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByProjectActivity.this, getResources().getString(R.string.toast_add_producto_to_favorite_success));
                    getFAV();
                    if(projectTypeId.equalsIgnoreCase("11")){

                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callDeleteFav(String _cateId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _cateId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void DeleteFAV(String _cateId){
        showLoading(DealByProjectActivity.this);
        callDeleteFav(_cateId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByProjectActivity.this, getResources().getString(R.string.dialog_delete_finish));
                    getFAV();
                    if(projectTypeId.equalsIgnoreCase("11")){

                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    public void skuFAV(final String url,String message) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
////                    loadClassProject(String.valueOf(cateID),subPhaseId);
//
//                    showToast(DealByProjectActivity.this,message);
//                    getFAV(getGetFav(Utils.APP_LANGUAGE, projectId));
//                    if(projectTypeId.equalsIgnoreCase("11")){
//
//                    }
////                    chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
//                }
//            }
//        }.execute();
//    }



    @Override
    public void onClickAddtoCart(SkuItem skuItem) {
        Intent intent = new Intent(DealByProjectActivity.this, SkuConfirmActivity.class);
        intent.putExtra("skuId", skuItem.getId());
        intent.putExtra("total", skuItem.getBOQ());
        intent.putExtra("price", skuItem.getPrice());
        intent.putExtra("name", skuItem.getName());
        intent.putExtra("unit_code", skuItem.getUnit());
        intent.putExtra("subClass", "");
        intent.putExtra("stock", skuItem.getStock().replace(",",""));
        startActivity(intent);
    }

    @Override
    public void addFAVClass(String classId,boolean add) {
        if(add){
            addClassFav(classId);
        }else{
            removeClassFav(classId);
        }


        classAdapter.notifyDataSetChanged();
    }

    @Override
    public void clickClass(String classId,String name) {
        Intent intent = new Intent(DealByProjectActivity.this, ChooseSkuActivity.class);

        intent.putExtra("class_id", classId);
        intent.putExtra("projectId", projectId);
        intent.putExtra("phase_id", "");
        sub_class_name = name;
        startActivity(intent);
    }

    @Override
    public void onClickInfo(SkuItem skuItem) {
        PromotionDetail(skuItem.getPromotionDetailId());
    }


    private void PromotionDetail(String promotionDetailId) {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        callPromotionDetailApi(promotionDetailId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    PromotionDetail promotionDetail  = gson.fromJson(resp, PromotionDetail.class);
                    onShowPromotion(DealByProjectActivity.this,promotionDetail);
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callAddClassFav(String _classId) {
        return rudyService.addClassFav(
                Utils.APP_LANGUAGE,
                projectId,
                _classId,
                user_id
        );
    }

    public void addClassFav(String _classId){
        showLoading(DealByProjectActivity.this);
        callAddClassFav(_classId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByProjectActivity.this, getResources().getString(R.string.toast_add_producto_to_favorite_success));
                    getFAV();

                    if (projectTypeId.equalsIgnoreCase("11")){
                        loadClassProject(String.valueOf(cateID),subPhaseId);
                    }else{
                        loadCatalog();
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });

    }

    private Call<String> callRemoveClassFav(String _classId) {
        return rudyService.removeFAVClass(
                Utils.APP_LANGUAGE,
                projectId,
                _classId
        );
    }

    public void removeClassFav(String _classId){
        showLoading(DealByProjectActivity.this);
        callRemoveClassFav(_classId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(DealByProjectActivity.this, getResources().getString(R.string.dialog_delete_finish));
                    getFAV();

                    if (projectTypeId.equalsIgnoreCase("11")){
                        loadClassProject(String.valueOf(cateID),subPhaseId);
                    }else{
                        loadCatalog();
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });

    }

//    @SuppressLint("StaticFieldLeak")
//    public void getData(final String url,final String msg,String classId) {
//        Log.i("addRemoveFavorite", " : " +url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
////                removeCacheUrl(classId);
//                if (string.contains("200")) {
//                    showToast(DealByProjectActivity.this,msg);
//                    getFAV(getGetFav(Utils.APP_LANGUAGE, projectId));
//
//                    if (projectTypeId.equalsIgnoreCase("11")){
//                        loadClassProject(String.valueOf(cateID),subPhaseId);
//                    }else{
//                        loadCatalog();
//                    }
//                }
//            }
//        }.execute();
//    }

    private Call<String> callUpdateTranId() {
        return rudyService.updateTransId(
                Utils.APP_LANGUAGE,
                projectId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updateTranId(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callUpdateTranId().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                getFAV();
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONArray array = object.getJSONArray("items");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject itemObject = array.getJSONObject(i);
                            if(itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")){
                                transaction_id = itemObject.optString("transaction_id");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+projectId,e);
                    }
                }else{
                    dialog.dismiss();
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void updateTranId() {
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+projectId);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                getFAV(getGetFav(Utils.APP_LANGUAGE, projectId));
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONArray array = object.getJSONArray("items");
//                        for (int i = 0; i < array.length(); i++) {
//                            JSONObject itemObject = array.getJSONObject(i);
//                            if(itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")){
//                                transaction_id = itemObject.optString("transaction_id");
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(ApiEndPoint.HOST+"/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+projectId,e);
//                    }
//                }
//
//            }
//        }.execute();
//    }

    private Call<String> callGetFav() {
        return rudyService.getFAV(
                Utils.APP_LANGUAGE,
                projectId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getFAV(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callGetFav().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                if(!transaction_id.equalsIgnoreCase("")){

                    if (resp.contains("200")) {
                        badge_fav.setVisibility(View.VISIBLE);
                        setFavIcon(resp);
                    }
                    else{
                        BaseActivity.favItemSize = 0;
                        badge_fav.setVisibility(View.GONE);
                    }
                    getBasket();
                }else{
                    if (resp.contains("200")) {
                        setFavIcon(resp);
                    }
                    else{
                        BaseActivity.favItemSize = 0;
                        badge_fav.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void getFAV(final String url) {
//        Log.i("", "Customer: " + url);
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if(!transaction_id.equalsIgnoreCase("")){
//
//                    if (string.contains("200")) {
//                        badge_fav.setVisibility(View.VISIBLE);
//                        setFavIcon(string);
//                    }
//                    else{
//                        BaseActivity.favItemSize = 0;
//                        badge_fav.setVisibility(View.GONE);
//                    }
//                    getBasket(getGetBasket(Utils.APP_LANGUAGE,transaction_id));
//                }else{
//                    if (string.contains("200")) {
//                        setFavIcon(string);
//                    }
//                    else{
//                        BaseActivity.favItemSize = 0;
//                        badge_fav.setVisibility(View.GONE);
//                    }
//                }
//            }
//        }.execute();
//    }

    public void setFavIcon(String  items){
        try {
            JSONObject object = new JSONObject(items);
            JSONArray itemArray = object.getJSONArray("items");
            BaseActivity.favItemSize = itemArray.getJSONObject(0).getInt("total")+itemArray.getJSONObject(1).getInt("total");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException(items,e);
        }

        if(BaseActivity.favItemSize>0){
            badge_fav.setVisibility(View.VISIBLE);
            number_fav.setText(BaseActivity.favItemSize+"");
        }else{
            badge_fav.setVisibility(View.GONE);
        }
    }

    private Call<String> callGetBasket() {
        return rudyService.getTransectionDetail(
                Utils.APP_LANGUAGE,
                transaction_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getBasket(){
        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
        Utils.DialogLoader(dialog,this);

        callGetBasket().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismiss();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    badge.setVisibility(View.VISIBLE);
                    setBasket(resp);
                }else{
                    basketSize = 0;
                    badge.setVisibility(View.GONE);
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(DealByProjectActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                showToast(DealByProjectActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void getBasket(final String url) {
//        Log.i("getBasket ", " : " + url);
//        final Dialog dialog =  new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog,this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                return  Utils.getData(url);
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                dialog.dismiss();
//                if (string.contains("200")) {
//                    badge.setVisibility(View.VISIBLE);
//                    setBasket(string);
//                }else{
//                    basketSize = 0;
//                    badge.setVisibility(View.GONE);
//                }
//            }
//        }.execute();
//    }

    public void setBasket(String  items){
        try {
            JSONObject object = new JSONObject(items);
            JSONObject itemObj = object.getJSONObject("items");
            JSONArray itemArray = itemObj.getJSONArray("pr");
            basketSize = itemArray.length();
            if(basketSize>0){
                numberBadge.setText(basketSize + "");
            }else{
                badge.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            LogException(items,e);
        }

    }
}
