package com.scg.rudy.ui.edit_qo;

/**
 *
 * @author jackie
 * @date 12/2/2018 AD
 */

public interface EditQoInterface {
    void onClickEdit(String itemId, String itemQty, String itemPrice, String user_id, String discount, String discount_type);
    void onClickDelete(String itemId);
    void onPriceLower(String itemId,String itemQty, Double itemPrice);
}
