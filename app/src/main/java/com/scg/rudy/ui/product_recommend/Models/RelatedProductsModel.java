
package com.scg.rudy.ui.product_recommend.Models;

import android.graphics.drawable.Drawable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class RelatedProductsModel {

    @SerializedName("image")
    private String mImage;
    @SerializedName("text")
    private String mText;

    public RelatedProductsModel (String _mImage, String _mText){
        mImage = _mImage;
        mText = _mText;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

}
