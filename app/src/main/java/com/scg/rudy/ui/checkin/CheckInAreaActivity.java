package com.scg.rudy.ui.checkin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.checkin_detail.CheckInDetail;
import com.scg.rudy.model.pojo.checkin_detail.Items;
import com.scg.rudy.model.pojo.checkin_detail.Phaseprocess;
import com.scg.rudy.ui.checkin_gallery.CheckInGalleryActivity;
import com.scg.rudy.ui.checkin_update_status.UpdateStatusActivity;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.location.AppUtils;
import com.scg.rudy.utils.location.FetchAddressIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.concurrent.TimeoutException;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CheckInAreaActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private TextView clsBtn, distance_pin;
    private RudyService rudyService;
    private Bundle extras;
    private String projectId;
    private String checkInID;
    private String projectType;
    private String subPhase;
    private CardView addImage;
    private CardView updateDepartment;
    private RelativeLayout saveCheckIn;
    private EditText detail;
    private Phaseprocess _phaseprocess;
    private LinearLayout hide_key;
    public static String isUpdate = "0";
    private SupportMapFragment mapFragment;
    private Context mContext;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LatLng mCenterLatLong;
    private static String TAG = "MAP LOCATION";
    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private LinearLayout curBtn;
    private int kmInDec;
    private double meter;
    double meterInDec;
    private LatLngBounds bounds;
    private LatLngBounds.Builder builder;
    double curLat, curLng, pLat, pLng;
    String pName = "", checkInStatus = "", latlng = "";

    boolean isCheckIn = true;
    private RelativeLayout statLayout;
    private TextView msgTxt;
    private TextView distancePinNew;
    private TextView titleAlertText;
    private TextView clsBtnNew;
    private int CountCheck = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_area);
        initView();
        mapFragment.getMapAsync(this);


        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!AppUtils.isLocationEnabled(mContext)) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });
                dialog.show();
            }
            buildGoogleApiClient();
        } else {
            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }


        setOnclick();
        getCheckInDetail();
        Utils.hideKeyboardFrom(this, detail);

        clsBtnNew.setOnClickListener(this::onClick);


    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //finish();
            }
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    private Call<String> postEditCheckin() {
        return rudyService.postEditCheckin(
                Utils.APP_LANGUAGE,
                checkInID,
                projectId,
                "2",
                detail.getText().toString(),
                checkInStatus,
                latlng
        );
    }

    private Call<String> getCheckinDetail() {
        return rudyService.getCheckInDetail(
                Utils.APP_LANGUAGE,
                checkInID
        );
    }


    private void postEditCheckInAPI() {
        showLoading(this);
        postEditCheckin().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    Items dashboardResults = gson.fromJson(resp, Items.class);
                    setCheckInData(dashboardResults);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(CheckInAreaActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInAreaActivity.this, fetchErrorMessage(t));
            }
        });
    }

    private void getCheckInDetail() {
        showLoading(this);
        getCheckinDetail().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    CheckInDetail checkInDetail = gson.fromJson(resp, CheckInDetail.class);
                    setPhaseData(checkInDetail.getItems().getPhaseprocess());
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(CheckInAreaActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CheckInAreaActivity.this, fetchErrorMessage(t));
            }
        });
    }

    private void setPhaseData(Phaseprocess phaseData) {
        _phaseprocess = phaseData;
    }

    private void setCheckInData(Items dashboardResults) {
        ProjectDetailActivity.isUpdateCheckin = "1";
        showToast(CheckInAreaActivity.this, getResources().getString(R.string.toast_visit_unit_success));
        finish();
    }

    private Phaseprocess phaseprocessResults(Response<CheckInDetail> response) {
        CheckInDetail checkInDetail = response.body();
        return checkInDetail.getItems().getPhaseprocess();
    }

    private Items fetchResults(Response<CheckInDetail> response) {
        CheckInDetail dashboard = response.body();
        return dashboard.getItems();
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }


    private void setOnclick() {
        clsBtn.setOnClickListener(this);
        saveCheckIn.setOnClickListener(this);
        updateDepartment.setOnClickListener(this);
        addImage.setOnClickListener(this);
        hide_key.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return true;
    }

    public void hideKeyboard() {

    }

    private void changeMap(Location location) {

        Log.d(TAG, "Reaching map" + mMap);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;


            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(17.5f).tilt(0).build();

            mMap.setMyLocationEnabled(true);
            @SuppressLint("ResourceType") final View myLocationButton = mapFragment.getView().findViewById(0x2);
            myLocationButton.setVisibility(View.INVISIBLE);
            curBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myLocationButton.performClick();


                }
            });

            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.getUiSettings().setScrollGesturesEnabled(false);
//            mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            startIntentService(location);
            bounds = builder.build();
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
//            mMap.animateCamera(cu);

        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    protected void startIntentService(Location mLocation) { //TODO
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);
        startService(intent);

        latlng = String.valueOf(mLocation.getLatitude()) + "," + String.valueOf(mLocation.getLongitude());
        CalculationByDistance(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), new LatLng(pLat, pLng));
        Marker marker = mMap.addMarker(
                new MarkerOptions()
                        .position(new LatLng(pLat, pLng))
                        .title(pName)
//                        .snippet("ระยะ "+checkInStatus+" กม.")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker)));
        marker.showInfoWindow();
//        marker.setTag(arrayList.get(i).getId()+","+arrayList.get(i).getProject_name());
        marker.setPosition(new LatLng(pLat, pLng));
        builder.include(marker.getPosition());


        checkIsCheckgin(projectId, mLocation);


    }


    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####.##");
        DecimalFormat kmFormat = new DecimalFormat("####");
        kmInDec = Integer.valueOf(kmFormat.format(km));
        meter = valueResult % 1000;
        meterInDec = Double.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }

    private void initView() {
        extras = getIntent().getExtras();
        mContext = this;
        CheckInAreaActivity.isUpdate = "0";
        rudyService = ApiHelper.getClient();
        if (extras != null) {
            projectId = extras.getString("projectId");
            checkInID = extras.getString("checkInID");
            projectType = extras.getString("type");
            subPhase = extras.getString("subPhase");
            checkInStatus = extras.getString("checkInStatus");

            pLat = Double.parseDouble(extras.getString("pLat"));
            pLng = Double.parseDouble(extras.getString("pLng"));
            pName = extras.getString("pName");

            isCheckIn = extras.getBoolean("pName");

        }
        hide_key = findViewById(R.id.hide_key);
        distance_pin = findViewById(R.id.distance_pin);
        clsBtn = (TextView) findViewById(R.id.cls_btn);
        addImage = (CardView) findViewById(R.id.add_image);
        updateDepartment = (CardView) findViewById(R.id.update_department);
        saveCheckIn = (RelativeLayout) findViewById(R.id.saveCheckIn);
        detail = (EditText) findViewById(R.id.detail);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        curBtn = findViewById(R.id.cur_btn);


        statLayout = (RelativeLayout) findViewById(R.id.stat_layout);
        msgTxt = (TextView) findViewById(R.id.msg_txt);
        distancePinNew = (TextView) findViewById(R.id.distance_pin_new);
        titleAlertText = (TextView) findViewById(R.id.titleAlertText);
        clsBtnNew = (TextView) findViewById(R.id.cls_btn_new);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            if (!CheckInAreaActivity.isUpdate.equalsIgnoreCase("0")) {
                ProjectDetailActivity.isUpdateCheckin = "1";
                showToast(CheckInAreaActivity.this, getResources().getString(R.string.toast_visit_unit_success));
            }
            finish();
        }
        if (v == clsBtnNew) {
            if (!CheckInAreaActivity.isUpdate.equalsIgnoreCase("0")) {
                ProjectDetailActivity.isUpdateCheckin = "1";
                showToast(CheckInAreaActivity.this, getResources().getString(R.string.toast_visit_unit_success));
            }
            finish();
        }
        if (v == saveCheckIn) {
            if (detail.getText().toString().isEmpty()) {
                showToast(CheckInAreaActivity.this, getResources().getString(R.string.toast_pls_specify_detail_visit));
            } else {
                postEditCheckInAPI();
            }
        }
        if (v == updateDepartment) {
            Intent intent = new Intent(CheckInAreaActivity.this, UpdateStatusActivity.class);
            intent.putExtra("projectId", projectId);
            intent.putExtra("projectType", projectType);
            intent.putExtra("subPhase", subPhase);
            intent.putExtra("phaseprocess", _phaseprocess);
            startActivity(intent);
        }
        if (v == addImage) {
            Intent intent = new Intent(CheckInAreaActivity.this, CheckInGalleryActivity.class);
            intent.putExtra("checkInID", checkInID);
            startActivity(intent);
        }
        if (v == hide_key) {
            Utils.hideKeyboard(this);
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);

        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);

            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);

//            displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));
            }
        }

    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null)
                changeMap(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        mMap = googleMap;
        builder = new LatLngBounds.Builder();
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.d("Camera postion change" + "", cameraPosition + "");
                mCenterLatLong = cameraPosition.target;
                mMap.clear();
                try {

                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);
                    startIntentService(mLocation);
//                    mLocationMarkerText.setText("Lat : " + mCenterLatLong.latitude + "," + "Long : " + mCenterLatLong.longitude);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setMyLocationButtonEnabled(true);
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }


    private Call<String> callCheckIsCheckIn(String projectId,Location mLocation) {
        return rudyService.checkIsCheckgin(
                Utils.APP_LANGUAGE,
                projectId,
                String.valueOf(mLocation.getLatitude()),
                String.valueOf(mLocation.getLongitude())
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void checkIsCheckgin(String projectId,Location mLocation){
        showLoading(CheckInAreaActivity.this);
        callCheckIsCheckIn(projectId, mLocation).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONObject itemObj = object.getJSONObject("items");
                        checkInStatus = itemObj.optString("distance", "");

                        if (itemObj.optString("isCheck", "").equalsIgnoreCase("1")) {
                            isCheckIn = true;
                        } else {
                            isCheckIn = false;

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject",e);
                    }
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CheckInAreaActivity.this, error.getItems());
                }


                distance_pin.setText(getResources().getString(R.string.away_from_unit) + checkInStatus + getResources().getString(R.string.km));
                distancePinNew.setText("*** " + getResources().getString(R.string.away_from_unit) + " " + checkInStatus + " " + getResources().getString(R.string.km) + " ***");
                if (isCheckIn) {
                    msgTxt.setText(getResources().getString(R.string.you_at_same_location_unit));
                    statLayout.setBackground(getResources().getDrawable(R.drawable.linear_green_checkin));
//                    detail.setText("");
                    detail.setEnabled(true);
                    detail.setTextColor(getResources().getColor(R.color.colorPrimary));
                    titleAlertText.setText(getResources().getString(R.string.checkin_in));
                } else {

                    if (CountCheck <= 0){
                        CountCheck++;
                        final Dialog dialog = new Dialog(CheckInAreaActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        dialog.setContentView(R.layout.dialog_checkin_area_alert);
                        dialog.setCanceledOnTouchOutside(false);

                        TextView head = dialog.findViewById(R.id.Head);
                        TextView detail = dialog.findViewById(R.id.Detail);
                        TextView textBtnAgree = dialog.findViewById(R.id.btnAgree);

                        RelativeLayout agree = dialog.findViewById(R.id.agree);

                        agree.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        head.setText(getResources().getString(R.string.checkin_area_dialog_head));
                        detail.setText(getResources().getString(R.string.checkin_area_dialog_detail));
                        textBtnAgree.setText(getResources().getString(R.string.checkin_area_dialog_btn_agree));

                        dialog.show();
                    }


                    msgTxt.setText(getResources().getString(R.string.you_not_at_same_location_unit));
                    statLayout.setBackground(getResources().getDrawable(R.drawable.linear_red_checkin));
                    titleAlertText.setText(getResources().getString(R.string.checkin_out));
                    detail.setText(getResources().getString(R.string.not_at_in_visit_unit));
                    detail.setTextColor(getResources().getColor(R.color.red));
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(CheckInAreaActivity.this, t.getMessage());
            }
        });

    }



//    @SuppressLint("StaticFieldLeak")
//    private void checkIsCheckgin(final String projectId,Location mLocation) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
////        ArrayList<String> lisData = new ArrayList<>();
//        formData.add("project_id", projectId);
//        formData.add("lat", String.valueOf(mLocation.getLatitude()));
//        formData.add("lng", String.valueOf(mLocation.getLongitude()));
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.postData(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject", requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONObject itemObj = object.getJSONObject("items");
//                        checkInStatus = itemObj.optString("distance", "");
//
//                        if (itemObj.optString("isCheck", "").equalsIgnoreCase("1")) {
//                            isCheckIn = true;
//                        } else {
//                            isCheckIn = false;
//
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject",e);
//                    }
//                }
//
//                distance_pin.setText(getResources().getString(R.string.away_from_unit) + checkInStatus + getResources().getString(R.string.km));
//                distancePinNew.setText("*** " + getResources().getString(R.string.away_from_unit) + " " + checkInStatus + " " + getResources().getString(R.string.km) + " ***");
//                if (isCheckIn) {
//                    msgTxt.setText(getResources().getString(R.string.you_at_same_location_unit));
//                    statLayout.setBackground(getResources().getDrawable(R.drawable.linear_green_checkin));
////                    detail.setText("");
//                    detail.setEnabled(true);
//                    detail.setTextColor(getResources().getColor(R.color.colorPrimary));
//                    titleAlertText.setText(getResources().getString(R.string.checkin_in));
//                } else {
//
//                    if (CountCheck <= 0){
//                        CountCheck++;
//                        final Dialog dialog = new Dialog(CheckInAreaActivity.this);
//                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                        dialog.setContentView(R.layout.dialog_checkin_area_alert);
//                        dialog.setCanceledOnTouchOutside(false);
//
//                        TextView head = dialog.findViewById(R.id.Head);
//                        TextView detail = dialog.findViewById(R.id.Detail);
//                        TextView textBtnAgree = dialog.findViewById(R.id.btnAgree);
//
//                        RelativeLayout agree = dialog.findViewById(R.id.agree);
//
//                        agree.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                dialog.dismiss();
//                            }
//                        });
//
//                        head.setText(getResources().getString(R.string.checkin_area_dialog_head));
//                        detail.setText(getResources().getString(R.string.checkin_area_dialog_detail));
//                        textBtnAgree.setText(getResources().getString(R.string.checkin_area_dialog_btn_agree));
//
//                        dialog.show();
//                    }
//
//
//                    msgTxt.setText(getResources().getString(R.string.you_not_at_same_location_unit));
//                    statLayout.setBackground(getResources().getDrawable(R.drawable.linear_red_checkin));
//                    titleAlertText.setText(getResources().getString(R.string.checkin_out));
//                    detail.setText(getResources().getString(R.string.not_at_in_visit_unit));
//                    detail.setTextColor(getResources().getColor(R.color.red));
////                    detail.setEnabled(false);
//                }
//
//            }
//        }.execute();
//    }
}
