package com.scg.rudy.ui.favorite;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.favorite.ListItem;
import com.scg.rudy.ui.sku_confirm.SkuConfirmActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @author DekDroidDev
 * @date 9/2/2018 AD
 */
public class FavRecyclerListAdapter extends RecyclerView.Adapter<FavRecyclerListAdapter.ProjectViewHolder> {

    private ArrayList<ListItem> cartModelArrayLis;
    private Context context;
    private FAvoriteInterface listener;

    public FavRecyclerListAdapter(
            Context context,
            ArrayList<ListItem> cartModelArrayLis,
            FAvoriteInterface listener) {
        this.cartModelArrayLis = cartModelArrayLis;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public FavRecyclerListAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fav_item_list, parent, false);
        return new FavRecyclerListAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FavRecyclerListAdapter.ProjectViewHolder holder, int position) {
        final ListItem prCodeDetailModel = cartModelArrayLis.get(position);
        holder.textCurrency.setText(Utils.currencyForShow);
        holder.name.setText(prCodeDetailModel.getName());
        holder.boq.setText(Utils.moneyFormat(prCodeDetailModel.getBOQ())+" "+(prCodeDetailModel.getUnit().replace("\n","")));

        if (prCodeDetailModel.getPrice() != null) {
            holder.price.setText(
                    Utils.moneyFormat(
                            prCodeDetailModel.getPrice().toString().replace("null", "1")));
        }
        holder.stock.setText(context.getResources().getString(R.string.txt_stock) + Utils.moneyFormat(prCodeDetailModel.getStock())+" "+(prCodeDetailModel.getUnit().replace("\n","")));
        holder.add_cart.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, SkuConfirmActivity.class);
                        intent.putExtra("skuId", prCodeDetailModel.getId());
                        intent.putExtra("total", prCodeDetailModel.getBOQ().replace(",",""));
                        intent.putExtra("price", prCodeDetailModel.getPrice());
                        intent.putExtra("name", prCodeDetailModel.getName());
                        intent.putExtra("unit_code", prCodeDetailModel.getUnit());
                        intent.putExtra("stock", prCodeDetailModel.getStock());
                        context.startActivity(intent);
                    }
                });

        holder.remove_fav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.deleteFAvorite(prCodeDetailModel.getId());
                    }
                });
    }

    @Override
    public int getItemCount() {
        if (cartModelArrayLis == null) {
            return 0;
        }
        return cartModelArrayLis.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView price;
        private TextView boq;
        private RelativeLayout add_cart;
        private RelativeLayout remove_fav;
        private TextView stock;
        private ImageView displayImage;
        private TextView textCurrency;


        public ProjectViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name2);
            price = view.findViewById(R.id.price4);
            add_cart = view.findViewById(R.id.add_cart);
            remove_fav= view.findViewById(R.id.remove_fav);
            boq = view.findViewById(R.id.boq);
            stock = view.findViewById(R.id.stock);
            displayImage = view.findViewById(R.id.displayImage);
            textCurrency = view.findViewById(R.id.price5);
        }
    }
}
