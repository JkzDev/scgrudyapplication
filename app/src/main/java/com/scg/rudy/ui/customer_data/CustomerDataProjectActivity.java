package com.scg.rudy.ui.customer_data;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.ui.customer_data.model.CusModel;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutoCompleteTextView;
import com.scg.rudy.utils.custom_view.autocomplete.OnDynamicAutocompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class CustomerDataProjectActivity extends BaseActivity implements View.OnClickListener,OnDynamicAutocompleteListener,customerInterface {
    private int selectTab = 1;
    private String cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    private String ccompany = "";// บริษัทลูกค้า
    private String cphone = "";// เบอร์ติดต่อลูกค้า
    private String cline = "";// line ID ของลูกค้า
    private String cnote = "";// noteลูกค้า
    private String champ_customer_code = "";;
    private String customer_company = "";;
    private String customer_email = "";
    private String customer_code = "";

    private int customerTypePosition = 0;
    private int ctype = 1;
    private ImageView clsBtn;
    private Spinner typeCustomer;
    private EditText customerCompany;
    private EditText lineid;
    private EditText note;
    private LinearLayout key_hide;
    private RelativeLayout save;
    private RelativeLayout btn_cus;
    private TextView text_cus;
    private LinearLayout typ_cus;
    private RelativeLayout clear;
    private Bundle extras;
    public static  String projectId = "";
    private static final int THRESHOLD = 1;
    private DynamicAutoCompleteTextView name;
    private DynamicAutoCompleteTextView tel;
    private ProgressBar mDictionaryAutoCompleteProgressBar;
    private ProgressBar telAutoCompleteProgressBar;
    private ExtendedDictionaryAutocompleteAdapter nameAdapter;
    private ExtendedDictionaryAutocompleteAdapter telAdapter;
    private int cusFirstSelect = 0;

    private String group_id = "";
    private String project_group_name = "";
    private String developer_id = "";
    private String developer_name_other = "";
    private String start_date = "";
    private String end_date = "";
    private String lat_lng = "";
    private String project_address = "";
    private String cateId ="";
    private RudyService rudyService;
    private String user_id;


    private EditText taxNo;
    private String cTaxNo = "";
    private TextView champCode;
    private TextView champHead;
    private LinearLayout companyLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_data_project);
        initView();
        getUserData();
        selectTab = 1;
        extras = getIntent().getExtras();
        clsBtn.setOnClickListener(this);
        clear.setOnClickListener(this);
        if (extras != null) {
            group_id =  extras.getString("group_id","");
            project_group_name = extras.getString("project_name","");
            developer_id = extras.getString("developer_id","1");
            developer_name_other = extras.getString("developer_name_other","1");
            start_date = extras.getString("start_date","1");
            end_date = extras.getString("end_date","1");
            lat_lng =  extras.getString("lat_lng","");
            project_address =  extras.getString("project_address","");
            cateId = extras.getString("cateId","1");

            cname =  extras.getString("customer_name","");
            cphone =  extras.getString("customer_phone","");
            cline =  extras.getString("customer_line","");
            cnote =  extras.getString("customer_note","");
            ctype =  Integer.parseInt(extras.getString("ctype","1"));
            cTaxNo =  extras.getString("tax_number","");
            champ_customer_code =  extras.getString("champ_customer_code","");
            customer_company =  extras.getString("customer_company","");
            customer_code  =  extras.getString("customer_code");


        }


        nameAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,1);
        telAdapter = new ExtendedDictionaryAutocompleteAdapter(this, R.layout.extended_list_item,this,1);

//        cTaxNo
        name.setOnDynamicAutocompleteListener(this);
        name.setAdapter(nameAdapter);
        name.setThreshold(THRESHOLD);

        tel.setOnDynamicAutocompleteListener(this);
        tel.setAdapter(telAdapter);
        tel.setThreshold(THRESHOLD);

        mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
        telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);

        if(ctype == 1){
            companyLayout.setVisibility(View.GONE);
        }else{
            companyLayout.setVisibility(View.VISIBLE);
        }

        if (cTaxNo.length() > 0) {
            taxNo.setText(cTaxNo);
        }

        if (customer_company.length() > 0) {
            customerCompany.setText(customer_company);
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            customerCompany.setTypeface(_font);
        }

        if (cname.length() > 0) {
            name.setText(cname);
        }
        if (ccompany.length() > 0) {
            customerCompany.setText(ccompany);
        }
        if (cphone.length() > 0) {
            tel.setText(cphone);
        }
        if (cline.length() > 0) {
            lineid.setText(cline);
        }
        if (cnote.length() > 0) {
            note.setText(cnote);
        }

        if(champ_customer_code.isEmpty()){
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
        }else{
            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(champ_customer_code);
        }

        if (cname.length() > 0 && cphone.length() > 0) {
            save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
        }else{
            save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }

        key_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(CustomerDataProjectActivity.this, key_hide);
            }
        });


        customerCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customer_company = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                customerCompany.setTypeface(_font);
            }
        });

        lineid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cline = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                lineid.setTypeface(_font);
            }
        });


        taxNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cTaxNo = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                taxNo.setTypeface(_font);
            }
        });


        note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cnote = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                note.setTypeface(_font);
            }
        });

        btn_cus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTab = 1;
                btn_cus.setBackground(getDrawable(R.drawable.rounder_cus_left_select));
                text_cus.setTextColor(getResources().getColor(R.color.black));
                typ_cus.setVisibility(View.VISIBLE);
                checkData(selectTab);

            }
        });



        CustomSpinner newUserTypeAdapter = new CustomSpinner(CustomerDataProjectActivity.this, new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.type_cus))));
        typeCustomer.setAdapter(newUserTypeAdapter);
        typeCustomer.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        typeCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);

                if(cusFirstSelect>0){
                    ctype = position + 1;
                    customerTypePosition = position;
                }

                if(ctype == 1){
                    companyLayout.setVisibility(View.GONE);
                }else{
                    companyLayout.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if(ctype==1){
            customerTypePosition = 0;
        }else{
            customerTypePosition =1;
        }


        if(cusFirstSelect==0){
            typeCustomer.setSelection(customerTypePosition);
            cusFirstSelect++;
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cname = name.getText().toString();
                cphone = tel.getText().toString();
                cline = lineid.getText().toString();
                cnote = note.getText().toString();
                cTaxNo = taxNo.getText().toString();

                cname = name.getText().toString();

                if(ctype==1){
                    customer_company = "";
                }else{
                    customer_company = customerCompany.getText().toString();
                }


                if (/*cname.length() > 0 && */cphone.length() > 0) {
                    if(group_id.length()>0){
                        AddNewGroupProjectActivity.ctype = ctype;
                        AddNewGroupProjectActivity.customer_name =cname;
                        AddNewGroupProjectActivity.customer_code = customer_code;
                        AddNewGroupProjectActivity.champ_customer_code = champ_customer_code;
                        AddNewGroupProjectActivity.customer_company = customer_company;
                        AddNewGroupProjectActivity.ccompany =ccompany;
                        AddNewGroupProjectActivity.customer_phone =cphone;
                        AddNewGroupProjectActivity.customer_tax_no = cTaxNo;
                        AddNewGroupProjectActivity.customer_line =cline;
                        AddNewGroupProjectActivity.customer_note =cnote;
                        MainActivity.gdata4 = true;
                        postEditProjectGroup();
                    }else{
                        AddNewGroupProjectActivity.ctype = ctype;
                        AddNewGroupProjectActivity.customer_name =cname;
                        AddNewGroupProjectActivity.customer_code = customer_code;
                        AddNewGroupProjectActivity.champ_customer_code = champ_customer_code;
                        AddNewGroupProjectActivity.customer_company = customer_company;
                        AddNewGroupProjectActivity.ccompany =ccompany;
                        AddNewGroupProjectActivity.customer_phone =cphone;
                        AddNewGroupProjectActivity.customer_tax_no = cTaxNo;
                        AddNewGroupProjectActivity.customer_line =cline;
                        AddNewGroupProjectActivity.customer_note =cnote;

                        MainActivity.gdata4 = true;
                       finish();
                        }
                } else {
                    Toast.makeText(CustomerDataProjectActivity.this, R.string.toast_pls_fill_name_and_phone_contractor, Toast.LENGTH_LONG).show();
                }



            }
        });
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData" ,e);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if(v==clear){
                name.setText("");
                customerCompany.setText("");
                tel.setText("");
                taxNo.setText("");
                lineid.setText("");
                note.setText("");
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }
    }

    public void checkData(int tab){
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }

    }


    private void initView() {
        rudyService = ApiHelper.getClient();
        clear = findViewById(R.id.clear);
        typeCustomer = findViewById(R.id.typeCustomer);
        name =  findViewById(R.id.name);

        mDictionaryAutoCompleteProgressBar =  findViewById(R.id.dictionaryAutoCompleteProgressBar);
        telAutoCompleteProgressBar  = findViewById(R.id.telAutoCompleteProgressBar);


        tel = findViewById(R.id.tel);
        lineid = findViewById(R.id.lineid);
        note = findViewById(R.id.note);
        key_hide = findViewById(R.id.key_hide);
        save = findViewById(R.id.save);
        btn_cus = findViewById(R.id.btn_cus);
        text_cus = findViewById(R.id.text_cus);
        typ_cus = findViewById(R.id.typ_cus);
        clsBtn = findViewById(R.id.cls_btn);
        taxNo = findViewById(R.id.tax_no);
        champCode = findViewById(R.id.champCode);
        champHead = findViewById(R.id.champHead);
        companyLayout = (LinearLayout) findViewById(R.id.companyLayout);
        customerCompany = (EditText) findViewById(R.id.customer_company);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }

    private Call<String> callPostEditProjectGroup() {
        return rudyService.postEditCustomerProjectGroup(
                Utils.APP_LANGUAGE,
                group_id,
                user_id,
                project_group_name,
                cateId,
                developer_id,
                developer_name_other,
                start_date,
                end_date,
                String.valueOf(ctype),
                cname,
                customer_code,
                champ_customer_code,
                customer_company,
                cphone,
                customer_email,
                cline,
                cTaxNo,
                cnote
        );

    }


    private void postEditProjectGroup() {
        showLoading(this);
        callPostEditProjectGroup().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if(resp.contains("200")){
                    ProjectGroupDetail projectGroupDetail  = gson.fromJson(resp, ProjectGroupDetail.class);
                    AddNewGroupProjectActivity.projectDetail = projectGroupDetail;
                    showToast(CustomerDataProjectActivity.this,getResources().getString(R.string.toast_update_data_success));
                    finish();
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(CustomerDataProjectActivity.this, error.getItems());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(CustomerDataProjectActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void updateCustomer(final String url) {
//        showLoading(this);
//        final RequestBody requestBody;
//            requestBody = new FormBody.Builder()
////                    .add("customer_type", ""+ctype)
////                    .add("customer_name", cname)
////                    .add("customer_phone", cphone)
////                    .add("customer_email", "")
////                    .add("customer_line", cline)
////                    .add("customer_tax_no", cTaxNo)
////                    .add("customer_note", cnote)
//                    .add("user_id",  user_id)
//                    .add("name", cname)
//                    .add("lat_lng", lat_lng)
//                    .add("project_address", project_address)
//                    .add("project_group_list_id", cateId)
//                    .add("developer_id", developer_id)
//                    .add("developer_name_other", developer_name_other)
//                    .add("start_date", start_date)
//                    .add("end_date", end_date)
//                    .add("customer_type", ctype+"")
//                    .add("customer_name", cname)
//                    .add("customer_code", customer_code)
//                    .add("champ_customer_code", champ_customer_code)
//                    .add("customer_company", customer_company)
//                    .add("customer_phone", cphone)
//                    .add("customer_email", customer_email)
//                    .add("customer_line", cline)
//                    .add("customer_tax_no", cTaxNo)
//                    .add("customer_note", cnote)
//                    .add("project_group_id",group_id)
//                    .build();
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    Gson gson = new Gson();
//                    AddNewGroupProjectActivity.projectDetail = gson.fromJson(string, ProjectGroupDetail.class);
//                    showToast(CustomerDataProjectActivity.this,getResources().getString(R.string.toast_update_data_success));
//                    finish();
//                }
//            }
//        }.execute();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        name.dismissDropDown();
        tel.dismissDropDown();
    }


    @Override
    public void onDynamicAutocompleteStart(AutoCompleteTextView v) {
        if (v == name) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }
        if(v==tel){
            telAutoCompleteProgressBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDynamicAutocompleteStop(AutoCompleteTextView v) {
        if (v == name) {
            mDictionaryAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cname = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            name.setTypeface(_font);
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }
        }
        if(v==tel){
           telAutoCompleteProgressBar.setVisibility(View.INVISIBLE);
            cphone = v.getText().toString();
            Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
            tel.setTypeface(_font);
            if (cname.length() > 0 && cphone.length() > 0) {
                save.setBackground(getDrawable(R.drawable.bg_rounder_req_price));
            }else{
                save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
            }
        }

    }




    @Override
    protected void onPause() {
        super.onPause();
        name.dismissDropDown();
        tel.dismissDropDown();
    }

    @Override
    public void selectCusItem(CusModel cusModel, int tabIndex) {
        name.setText(cusModel.getCustomer_name());
        tel.setText(cusModel.getCustomer_phone());
        lineid.setText(cusModel.getCustomer_line());
        taxNo.setText(cusModel.getCustomer_taxno());
        customer_code = cusModel.getCustomer_code();
        champ_customer_code = cusModel.getChamp_customer_code();

        if(champ_customer_code.isEmpty()){
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
        }else{
            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(champ_customer_code);
        }


        name.dismissDropDown();
        tel.dismissDropDown();
        Utils.hideKeyboard(this);

    }
}
