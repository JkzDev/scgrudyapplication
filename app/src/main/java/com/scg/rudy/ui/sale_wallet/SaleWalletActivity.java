package com.scg.rudy.ui.sale_wallet;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.sale_wallet.DetailItem;
import com.scg.rudy.model.pojo.sale_wallet.ItemsItem;
import com.scg.rudy.model.pojo.sale_wallet.SaleWallet;
import com.scg.rudy.model.pojo.user.Items;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.choose_sku.PaginationScrollListener;
import com.scg.rudy.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class SaleWalletActivity extends BaseActivity implements View.OnClickListener, SaleWalletInterface {
    private RudyService rudyService;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private List<ItemsItem> saleWalletsItems;

    private ImageView clsBtn;
    private LinearLayout tabInfo;
    private TextView txtInfo;
    private LinearLayout tabWallet;
    private TextView txtWallet;
    private LinearLayout infoView;
    private LinearLayout walletView;
    private RecyclerView recyclerView;
    private String user_id;
    private String shopID;

    private LinearLayout errorLayout;
    private TextView error_txt;
    private Button btnRetry;
    private TextView txtError, error_txt_sale;
    private String year = "", month = "";

    private SaleWalletPaginationAdapter adapter;
    private Spinner monthSpinner;
    private Spinner statusSpinner;
    private EditText name;
    private EditText nickName;
    private EditText email;
    private EditText tel;
    private UserPOJO userPOJO;
    private CircleImageView cusImage;
    private TextView custype;
    private TextView cusId;
    private TextView cusName;
    private TextView cusCompany;
    private TextView opportunity;
    private TextView money;
    private boolean isSlect = false;
    private TextView textView28;
    private TextView textView31;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_wallet);
        initView();
        setTextCurrentDate();
        implementUserData();
        setOnClick();
        setSpinnerData();
        setSaleWlletData();
        setUserData();

        textView28.setText(Utils.currencyForShow);
        textView31.setText(Utils.currencyForShow);

    }

    public void setTextCurrentDate() {
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());
        String dateArr[] = date.split("/");
//        Utils.showToast(this,date);
        month = dateArr[1];
        year = dateArr[2];
    }


    private void setUserData() {
        Items userItem = userPOJO.getItems();
        name.setText(userItem.getName());
        nickName.setText(userItem.getNickname());
        email.setText(userItem.getEmail());
        tel.setText(userItem.getPhone());
//        Glide.with(this).load(userItem.get).into(cusImage);

        if (userItem.getLevel().equalsIgnoreCase("1")) {
            custype.setText(getResources().getString(R.string.txt_owner));
        } else if (userItem.getLevel().equalsIgnoreCase("2")) {
            custype.setText(getResources().getString(R.string.txt_manager));
        } else {
            custype.setText(getResources().getString(R.string.txt_sale));
        }
        cusId.setText(userItem.getEmployeeId());
        cusName.setText(userItem.getName());
        cusCompany.setText(userItem.getShop());

        if (!userItem.getPic().isEmpty()) {
            Glide.with(this).load(userItem.getPic()).into(cusImage);
        }

    }

    private Call<String> callSaleWalletApi() {
        return rudyService.getSaleWallet(
                Utils.APP_LANGUAGE,
                user_id,
                String.valueOf(currentPage),
                year,
                month
        );
    }


    private void setSpinnerData() {
        ArrayList<String> monthList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.type_month)));
        ArrayList<String> filterList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.type_sale_wallet)));

        MonthSpinner monthAdapter = new MonthSpinner(this, monthList);
        MonthSpinner filterAdapter = new MonthSpinner(this, filterList);

        monthSpinner.setAdapter(monthAdapter);
        statusSpinner.setAdapter(filterAdapter);

        monthSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        statusSpinner.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._11sdp));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._11sdp));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                if (isSlect) {
                    month = String.valueOf(position + 1);
                    setSaleWlletData();
                }
                isSlect = true;
                loadFirstPage();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!isSlect) {
            monthSpinner.setSelection(Integer.parseInt(month) - 1);
        }


    }

    public void implementUserData() {
        if (Utils.getPrefer(this, Utils.PERF_LOGIN).length() > 0) {
            userPOJO = Utils.setEnviroment(this);
            user_id = userPOJO.getItems().getId();
            shopID = userPOJO.getItems().getShopId();
        }
    }


    private void setSaleWlletData() {
        adapter = new SaleWalletPaginationAdapter(this, this);

//        SaleWalletRecyclerListAdapter adapter = new SaleWalletRecyclerListAdapter(this, null);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void loadFirstPage() {
//        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callSaleWalletApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                hideErrorView();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    SaleWallet obj = gson.fromJson(resp, SaleWallet.class);
                    List<DetailItem> detailItemList = customFetchResults(obj);
                    if (detailItemList.size() > 0) {
                        statusSpinner.setEnabled(true);
                        errorLayout.setVisibility(View.GONE);
                        adapter.addAll(detailItemList);
                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    } else {
                        statusSpinner.setEnabled(false);
                        error_txt_sale.setVisibility(View.VISIBLE);
                        error_txt_sale.setText(getResources().getString(R.string.not_found_order));
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SaleWalletActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                opportunity.setText("0");
                money.setText("0");
                error_txt_sale.setVisibility(View.VISIBLE);
                error_txt_sale.setText(getResources().getString(R.string.not_found_order));
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void setMoneyData() {

        opportunity.setText(saleWalletsItems.get(0).getOpportunity());
        money.setText(saleWalletsItems.get(0).getMoney());
    }


    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        callSaleWalletApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.removeLoadingFooter();
                isLoading = false;
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    SaleWallet obj = gson.fromJson(resp, SaleWallet.class);
                    List<DetailItem> detailItemList = customFetchResults(obj);
                    adapter.addAll(detailItemList);
                    if (currentPage < TOTAL_PAGES) {
                        adapter.addLoadingFooter();
                    } else {
                        isLastPage = true;
                    }
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(SaleWalletActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }


    private List<DetailItem> fetchResults(Response<SaleWallet> response) {
        List<DetailItem> detailItemList = new ArrayList<>();
        TOTAL_PAGES = 1;
        SaleWallet saleWallet = response.body();
        if (saleWallet.getItems() != null) {
            if (saleWallet.getItems().get(0).getDetail() != null && saleWallet.getItems().get(0).getDetail().size() > 0) {
                detailItemList = saleWallet.getItems().get(0).getDetail();
                TOTAL_PAGES = saleWallet.getItems().get(0).getTotalPages();
                saleWalletsItems = saleWallet.getItems();
                setMoneyData();
            }


        }

        return detailItemList;
    }

    private List<DetailItem> customFetchResults(SaleWallet saleWallet) {
        List<DetailItem> detailItemList = new ArrayList<>();
        TOTAL_PAGES = 1;
        if (saleWallet.getItems() != null) {
            if (saleWallet.getItems().get(0).getDetail() != null && saleWallet.getItems().get(0).getDetail().size() > 0) {
                detailItemList = saleWallet.getItems().get(0).getDetail();
                TOTAL_PAGES = saleWallet.getItems().get(0).getTotalPages();
                saleWalletsItems = saleWallet.getItems();
                setMoneyData();
            }


        }

        return detailItemList;
    }

    private void hideErrorView() {
        if (error_txt_sale.getVisibility() == View.VISIBLE) {
            error_txt_sale.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (error_txt_sale.getVisibility() == View.GONE) {
            error_txt_sale.setVisibility(View.VISIBLE);
            error_txt_sale.setText(getResources().getString(R.string.not_found_order));
//            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }


    private void setOnClick() {
        tabInfo.setOnClickListener(this);
        tabWallet.setOnClickListener(this);
        clsBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == tabInfo) {
            setTabClick(tabInfo, txtInfo, infoView);
        }
        if (v == tabWallet) {
            setTabClick(tabWallet, txtWallet, walletView);
        }
    }

    private void setTabClick(View tabClick, TextView text, LinearLayout dataView) {
        txtInfo.setTextColor(getResources().getColor(R.color.black));
        txtWallet.setTextColor(getResources().getColor(R.color.black));
        tabInfo.setBackgroundColor(getResources().getColor(R.color.white));
        tabWallet.setBackgroundColor(getResources().getColor(R.color.white));
        infoView.setVisibility(View.GONE);
        walletView.setVisibility(View.GONE);


        text.setTextColor(getResources().getColor(R.color.white));
        tabClick.setBackground(getResources().getDrawable(R.drawable.background_sale_wallet));
        dataView.setVisibility(View.VISIBLE);
    }

    public void onFragmentDetached(String tag) {
    }

    public void onError(int resId) {
    }

    public void onError(String message) {
    }

    public void showMessage(String message) {
        showToast(this, message);
    }

    public void showMessage(int resId) {
        showToast(this, "Message : " + resId);
    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {
    }

    private void initView() {
        rudyService = ApiHelper.getClient();
        clsBtn = findViewById(R.id.cls_btn);
        tabInfo = findViewById(R.id.tab_info);
        txtInfo = findViewById(R.id.txt_info);
        tabWallet = findViewById(R.id.tab_wallet);
        txtWallet = findViewById(R.id.txt_wallet);
        infoView = findViewById(R.id.info_view);
        walletView = findViewById(R.id.wallet_view);
        errorLayout = findViewById(R.id.error_layout);
        error_txt = findViewById(R.id.error_txt);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        error_txt_sale = findViewById(R.id.error_txt_sale);

        recyclerView = findViewById(R.id.recyclerView);
        monthSpinner = findViewById(R.id.monthSpinner);
        statusSpinner = findViewById(R.id.statusSpinner);
        name = findViewById(R.id.name);
        nickName = findViewById(R.id.nickName);
        email = findViewById(R.id.email);
        tel = findViewById(R.id.tel);
        cusImage = (CircleImageView) findViewById(R.id.cus_image);
        custype = (TextView) findViewById(R.id.custype);
        cusId = (TextView) findViewById(R.id.cus_id);
        cusName = (TextView) findViewById(R.id.cus_name);
        cusCompany = (TextView) findViewById(R.id.cus_company);
        opportunity = (TextView) findViewById(R.id.opportunity);
        money = (TextView) findViewById(R.id.money);
        textView28 = (TextView) findViewById(R.id.textView28);
        textView31 = (TextView) findViewById(R.id.textView31);
    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }
}
