package com.scg.rudy.ui.project_detail;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.PhaseModel;
import com.scg.rudy.model.pojo.audio_note.AudioNote;
import com.scg.rudy.model.pojo.byproject.CustomerPhoneListItem;
import com.scg.rudy.model.pojo.project_detail.AudiosItem;
import com.scg.rudy.model.pojo.project_detail.Items;
import com.scg.rudy.model.pojo.project_detail.NoteItem;
import com.scg.rudy.model.pojo.project_detail.PhaseprocessItem;
import com.scg.rudy.model.pojo.project_detail.PrItem;
import com.scg.rudy.model.pojo.project_detail.ProjectDetail;
import com.scg.rudy.model.pojo.project_type.ItemsItem;
import com.scg.rudy.model.pojo.project_type.PhasesItem;
import com.scg.rudy.model.pojo.project_type.ProjectType;
import com.scg.rudy.ui.adapter.QTRecyclerAdapter;
import com.scg.rudy.ui.adapter.RecordRecyclerAdapter;
import com.scg.rudy.ui.by_projects.Adapter.CheckInTelAdapter;
import com.scg.rudy.ui.by_projects.ByProjectsListener;
import com.scg.rudy.ui.checkin.CheckInAreaActivity;
import com.scg.rudy.ui.checkin.CheckInHistoryActivity;
import com.scg.rudy.ui.checkin.CheckInTelActivity;
import com.scg.rudy.ui.choose_sku.ChooseSkuActivity;
import com.scg.rudy.ui.current_location.AddCurrentLocationActivity;
import com.scg.rudy.ui.customer_data.CustomerDataActivity;
import com.scg.rudy.ui.deal_by_phase.DealByPhaseActivity;
import com.scg.rudy.ui.dealbyproject.DealByProjectActivity;
import com.scg.rudy.ui.department_data.DepartmentDataActivity;
import com.scg.rudy.ui.department_photo.TakePhotoActivity;
import com.scg.rudy.ui.favorite.FavoriteDetailActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;
import com.scg.rudy.ui.project_detail.Models.GroupGalleryModel;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.TimeUtils;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.broadcast_receiver.CallReceiver;
import com.scg.rudy.utils.custom_view.RButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class ProjectDetailActivity extends BaseActivity implements View.OnClickListener, OnRequestPermissionsResultCallback, ByProjectsListener {
    @BindView(R.id.line_cus)
    LinearLayout lineCus;
    @BindView(R.id.tab_cus)
    LinearLayout tabCus;
    @BindView(R.id.line_stat)
    LinearLayout lineStat;
    @BindView(R.id.tab_stat)
    LinearLayout tabStat;
    @BindView(R.id.line_doc)
    LinearLayout lineDoc;
    @BindView(R.id.tab_doc)
    LinearLayout tabDoc;
    @BindView(R.id.line_save)
    LinearLayout lineSave;
    @BindView(R.id.tab_save)
    LinearLayout tabSave;
    @BindView(R.id.cus_name)
    TextView cusName;
    @BindView(R.id.cus_company)
    TextView cusCompany;
    @BindView(R.id.cus_tel)
    TextView cusTel;
    @BindView(R.id.cus_line)
    TextView cusLine;
    @BindView(R.id.ow_name)
    TextView owName;
    @BindView(R.id.ow_company)
    TextView owCompany;
    @BindView(R.id.ow_tel)
    TextView owTel;
    @BindView(R.id.ow_line)
    TextView owLine;
    Unbinder unbinder;
    @BindView(R.id.cus_layout)
    LinearLayout cusLayout;
    @BindView(R.id.status_layout)
    LinearLayout statusLayout;
    @BindView(R.id.doc_layout)
    LinearLayout docLayout;
    @BindView(R.id.record_layout)
    LinearLayout recordLayout;
    @BindView(R.id.edit_customer)
    RButton editCustomer;
    @BindView(R.id.number_fav)
    TextView numberFav;
    @BindView(R.id.badge_fav)
    RelativeLayout badgeFav;
    @BindView(R.id.fav)
    RelativeLayout fav;
    @BindView(R.id.number_basket)
    TextView numberBasket;
    @BindView(R.id.badge_basket)
    RelativeLayout badgeBasket;
    @BindView(R.id.basket)
    RelativeLayout basket;
    @BindView(R.id.txtCus)
    TextView txtCus;
    @BindView(R.id.txtStat)
    TextView txtStat;
    @BindView(R.id.txtDoc)
    TextView txtDoc;
    @BindView(R.id.txtSave)
    TextView txtSave;
    private ImageView clsBtn;
    private TextView titleName;
    private ImageView imgProject;
    private LinearLayout imageAdd;
    private TextView phase;
    private TextView phaseOpp;
    private TextView phaseBalance;
    private TextView type;
    private TextView unit;
    private TextView floor;
    private TextView area;
    private TextView budget;
    private Bundle extras;
    private String name;
    private String projectId;
    private TextView phaseId;
    private TextView phaseName;
    private TextView phaseSales;
    private TextView phaseStatus;
    private TextView phaseOppView;
    private RelativeLayout bg_status;
    private TextView txtPhase;
    private ImageView moreBtn;
    private RelativeLayout deal_phase;
    private String shop_id = "";
    private String detailData = "";
    private TextView dateText;
    private RecyclerView recyclerView;
    private ArrayList<PhaseModel> phaseModelArrayList;
    public static boolean edit = true;
    public Bitmap showBitmap;
    public RButton edit_department;
    public RelativeLayout edit_location;
    public Items detailItem;
    private RelativeLayout editimage;
    private ImageView imageMap;
    private TextView qtNodata;
    private RecyclerView qtRecycler;
    private TextView numberBadge;
    private int basketSize = 0;
    public String transaction_id = "";
    private TextView rcNodata;
    private RecyclerView rcRecycler;
    private ArrayList<AudioNote> audioNoteArrayList;
    private LinearLayout layout_add;
    private float standard;
    private float smallest;
    private CallReceiver receiver;
    public static final String TAG = "ProjectDetailActivity";
    private RelativeLayout checkin;
    private boolean isCheckIn = false;
    private String lat, lng;
    private LocationListener mLocationListener;
    private Location mLastLocation;
    private double latitude = 1;
    private double longitude = 1;
    private int typeCheckIn = 1;
    private String checkInID = null;
    public static String isUpdateCheckin = "0";
    public LocationManager mLocationManager;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1234;
    private boolean isCallCheckIn = false;
    private Date now;
    private String dateNow;
    private String customerTel = "";
    private List<CustomerPhoneListItem> customerPhoneListItem;
    private Cursor managedCursor;
    String user_id = "", shopID = "";

    private int catePosition = 0;
    private int phasePosition = 0;

    private TextView own_tab;

    private String checkInStatus = "";
    private TextView champCode;
    private TextView champHead;

    private RudyService rudyService;
    private TextView textCurrency;

    private ArrayList<GroupGalleryModel> groupGalleryModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail);

        rudyService = ApiHelper.getClient();


        ButterKnife.bind(this);
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Utils.savePrefer(this, "call", "");
        findViewById(R.id.bhome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(ProjectDetailActivity.this, MainActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });
        ProjectDetailActivity.isUpdateCheckin = "0";

        extras = getIntent().getExtras();
        if (extras != null) {
            projectId = extras.getString("projectId");
            ChooseSkuActivity.projectId = projectId;
            Utils.project_id = projectId;
        } else {
            projectId = "10";
            Utils.project_id = projectId;
            ChooseSkuActivity.projectId = projectId;
        }
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location loc) {
//                latitude = loc.getLatitude();
//                longitude = loc.getLongitude();

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        initView();
        getUserData();


        startService(new Intent(ProjectDetailActivity.this, CallReceiver.class));
        clsBtn.setOnClickListener(this);
        edit_department.setOnClickListener(this);
        moreBtn.setOnClickListener(this);
        deal_phase.setOnClickListener(this);
        editimage.setOnClickListener(this);
        edit_location.setOnClickListener(this);
        tabCus.setOnClickListener(this);
        tabStat.setOnClickListener(this);
        tabDoc.setOnClickListener(this);
        tabSave.setOnClickListener(this);
        fav.setOnClickListener(this);
        basket.setOnClickListener(this);
        editCustomer.setOnClickListener(this);
        checkin.setOnClickListener(this);

        textCurrency.setText(getResources().getString(R.string.million) + Utils.currencyForShow);
    }

    public void setQTList(final List<PrItem> qtListModelArrayList) {
        for (int i = 0; i < qtListModelArrayList.size(); i++) {
            if (qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr")) {
                qtListModelArrayList.remove(i);
            }
        }
        if (qtListModelArrayList.size() > 0) {
            qtRecycler.setLayoutManager(new LinearLayoutManager(this));
            QTRecyclerAdapter adapter = new QTRecyclerAdapter(this, qtListModelArrayList);
//        qtRecycler.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
//        qtRecycler.setItemAnimator(new DefaultItemAnimator());
            qtRecycler.setAdapter(adapter);
            qtRecycler.setNestedScrollingEnabled(false);
            qtNodata.setVisibility(View.GONE);
        } else {
            qtNodata.setVisibility(View.VISIBLE);
        }
    }


    public void setRCList(final ArrayList<AudioNote> audioNoteArrayList) {
//        for (int i = 0; i < qtListModelArrayList.size(); i++) {
//            if (qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr")) {
//                qtListModelArrayList.remove(i);
//            }
//        }
        if (audioNoteArrayList.size() > 0) {
            rcRecycler.setLayoutManager(new LinearLayoutManager(this));
            RecordRecyclerAdapter adapter = new RecordRecyclerAdapter(this, audioNoteArrayList);
//        qtRecycler.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
//        qtRecycler.setItemAnimator(new DefaultItemAnimator());
            rcRecycler.setAdapter(adapter);
            rcRecycler.setNestedScrollingEnabled(false);
            rcNodata.setVisibility(View.GONE);
        } else {
            rcNodata.setVisibility(View.VISIBLE);
        }
    }


    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
            LogException("getUserData",e);
        }
    }

    public void initView() {
        now = Calendar.getInstance().getTime();
        dateNow = TimeUtils.getTime(now.getTime(), TimeUtils.DEFAULT_DATE_ONLY);
        recyclerView = findViewById(R.id.recyclerView);
        clsBtn = findViewById(R.id.cls_btn);
        titleName = findViewById(R.id.title_name);
        imgProject = findViewById(R.id.img_project);
        imageAdd = findViewById(R.id.image_add);
        phase = findViewById(R.id.phase);
        phaseOpp = findViewById(R.id.phase_opp);
        phaseBalance = findViewById(R.id.phase_balance);
        type = findViewById(R.id.type);
        unit = findViewById(R.id.unit);
        floor = findViewById(R.id.floor);
        area = findViewById(R.id.area);
        budget = findViewById(R.id.budget);
        edit_department = findViewById(R.id.edit_department);
        edit_location = findViewById(R.id.edit_location);
        phaseId = findViewById(R.id.phase_id);
        phaseName = findViewById(R.id.phase_name);
        phaseSales = findViewById(R.id.phase_sales);
        phaseStatus = findViewById(R.id.phase_status);
        phaseOppView = findViewById(R.id.phase_opp_view);
        txtPhase = findViewById(R.id.txtPhase);
        moreBtn = findViewById(R.id.more_btn);
        deal_phase = findViewById(R.id.deal_phase);
        editimage = findViewById(R.id.editimage);
        imageMap = findViewById(R.id.imageMap);
        qtNodata = findViewById(R.id.qt_nodata);
        qtRecycler = findViewById(R.id.qt_recycler);
        numberBadge = findViewById(R.id.number_badge);
        rcNodata = findViewById(R.id.rc_nodata);
        rcRecycler = findViewById(R.id.rc_recycler);
        layout_add = findViewById(R.id.layout_add);
        standard = txtCus.getTextSize();
        smallest = txtStat.getTextSize();
        checkin = (RelativeLayout) findViewById(R.id.checkin);
        own_tab = findViewById(R.id.own_tab);
        champCode = (TextView) findViewById(R.id.champCode);
        champHead = (TextView) findViewById(R.id.champHead);
        textCurrency = (TextView) findViewById(R.id.textCurrency);
    }

    @Override
    public void onResume() {
        isCheckIn = false;
        if (checkInID != null) {
            if (ProjectDetailActivity.isUpdateCheckin.equalsIgnoreCase("0")) {
                DeleteCheckIn();
            }
        }


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (Utils.getPrefer(getApplicationContext(), "call").length() > 0) {
//                    readDummyCallog();
//                }
//            }
//        }, 2 * 1000);

        new CountDownTimer(2 * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if(Utils.getPrefer(getApplicationContext(), "call").length() > 0){
                    readDummyCallog();
                }
            }
        }.start();


        if (Utils.getPrefer(this, "print").length() > 0) {
            finish();
        } else {
            AddCurrentLocationActivity.shopID = "";
            AddCurrentLocationActivity.isEdit = false;
            AddCurrentLocationActivity.editlatlng = "";
            Utils.savePrefer(this, "cart", "");
            DealByPhaseActivity.QUICKSALE = "0";
            updateTranId(projectId);
        }

        super.onResume();
    }


    //    TODO
    public void setData(final Items detailItem) {
        Utils.project_id = detailItem.getId();
        this.detailItem = detailItem;
        TransactionDetailActivity.detailItem = detailItem;


        if (detailItem.getChampCustomerCode().isEmpty()) {
            champCode.setVisibility(View.GONE);
            champHead.setVisibility(View.GONE);
        } else {
            champCode.setVisibility(View.VISIBLE);
            champHead.setVisibility(View.VISIBLE);
            champCode.setText(detailItem.getChampCustomerCode());
        }


        if (detailItem.getGalleryNew().size() == 0) {
            imageAdd.setVisibility(View.GONE);
            imgProject.setImageDrawable(getResources().getDrawable(R.drawable.bg_tower));
//            editimage.setVisibility(View.GONE);
        } else {
            imageAdd.setVisibility(View.VISIBLE);
            editimage.setVisibility(View.VISIBLE);
        }
        imageAdd.removeAllViews();

        groupGalleryModels = new ArrayList<>();

        Map<String,ArrayList<GalleryModel>> groupImage = new HashMap<>();

        for(GalleryModel p : detailItem.getGalleryNew()){
            if(!groupImage.containsKey(p.getCkId())){
                groupImage.put(p.getCkId(), new ArrayList<>());
            }
            groupImage.get(p.getCkId()).add(p);
        }

        for (Map.Entry<String, ArrayList<GalleryModel>> map : groupImage.entrySet()){
            groupGalleryModels.add(new GroupGalleryModel(map.getKey(), map.getValue()));
        }

        Collections.sort(groupGalleryModels, new Comparator<GroupGalleryModel>() {
            @Override
            public int compare(GroupGalleryModel o1, GroupGalleryModel o2) {
                return o1.getmKey().compareTo(o2.getmKey());
            }
        });

        for (int i = 0; i < detailItem.getGalleryNew().size(); i++) {

            //check blank image
            if (i == 0) {
                Glide.with(Objects.requireNonNull(this)).asBitmap().load(detailItem.getGalleryNew().get(i).getName()).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        showBitmap = resource;
                    }
                });
                Glide.with(this)
                        .load(detailItem.getGalleryNew().get(i).getName())
                        .into(imgProject);

                imgProject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onShowPhoto(showBitmap);
                    }
                });
            }

            ImageView iv = new ImageView(this);
            iv.setAdjustViewBounds(true);

            //add pic to linearlayout for show
            Glide.with(this)
                    .load(detailItem.getGalleryNew().get(i).getName())
                    .into(iv);

            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 64);
            params.width = (int) getResources().getDimension(R.dimen._45sdp);
            params.height = (int) getResources().getDimension(R.dimen._45sdp);
            iv.setLayoutParams(params);
            iv.setAdjustViewBounds(true);
            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Glide.with(ProjectDetailActivity.this).asBitmap().load(detailItem.getGalleryNew().get(finalI).getName()).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            showBitmap = resource;
                        }
                    });

                    Glide.with(ProjectDetailActivity.this)
                            .load(detailItem.getGalleryNew().get(finalI).getName())
                            .into(imgProject);
                    imgProject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onShowPhoto(showBitmap);
                        }
                    });
                }
            });

            iv.setPadding(0, 0, 13, 0);
//            setMargins(iv, 10, 10, 10, 10);
            imageAdd.addView(iv);
        }

        if (showBitmap != null) {

        }

        if (detailItem.getLatLng().length() > 0) {
            Glide.with(this)
                    .load("https://maps.googleapis.com/maps/api/staticmap?center=" + detailItem.getLatLng() + "&zoom=18&size=350x350&key=" + this.getResources().getString(R.string.google_maps_key))
                    .into(imageMap);
        }


//        set PR list
        Utils.pdfName = detailItem.getProjectName();
        if (detailItem.getPr().size() > 0) {
            setQTList(detailItem.getPr());
        }

//        set Record list
        audioNoteArrayList = new ArrayList<>();
        if (detailItem.getNote().size() > 0) {
            for (int i = 0; i < detailItem.getNote().size(); i++) {
                NoteItem noteItem = detailItem.getNote().get(i);
                audioNoteArrayList.add(new AudioNote("note", noteItem.getLastUpdate(), noteItem.getFile()));
            }
        }
        if (detailItem.getAudios().size() > 0) {
            for (int i = 0; i < detailItem.getAudios().size(); i++) {
                AudiosItem audiosItem = detailItem.getAudios().get(i);
                audioNoteArrayList.add(new AudioNote("audio", audiosItem.getName(), audiosItem.getFile()));
            }
        }
        if (audioNoteArrayList.size() > 0) {
            setRCList(audioNoteArrayList);
        }

        if (detailItem.getPhaseprocess().size() > 0) {
            setLayoutAdd(detailItem.getPhaseprocess());
        }


        phase.setText(detailItem.getRank());
        phaseOpp.setText(detailItem.getWorkProgress() + "");
        phaseBalance.setText(detailItem.getUnitBudget());
        type.setText(detailItem.getProjectTypeName());
        unit.setText(detailItem.getUnits());
        floor.setText(detailItem.getProjectStories());
        area.setText(detailItem.getUnitArea());
        budget.setText(detailItem.getUnitBudget() + " " + getResources().getString(R.string.million) + Utils.currencyForShow);
//        budget.setText(String.format(getResources().getString(R.string.million_baht_parame), String.format("%s", detailItem.getUnitBudget())));

//            OWner
        if (detailItem.getOwnerType().equalsIgnoreCase("2")) {
            own_tab.setText(getResources().getString(R.string.house_owner));
            owName.setText(detailItem.getHouseOwnerName());
            if (detailItem.getHouseOwnerName().length() == 0) {
                owName.setText("-");
            }

            owCompany.setText(detailItem.getHouseOwnerName());
            if (detailItem.getHouseOwnerName().length() == 0) {
                owCompany.setText("-");
            }

            owTel.setText(detailItem.getHouseOwnerPhone());
            if (detailItem.getHouseOwnerPhone().length() == 0) {
                owTel.setText("-");
            }

            owLine.setText(detailItem.getHouseOwnerLine());
            if (detailItem.getHouseOwnerLine().length() == 0) {
                owLine.setText("-");
            }
        } else {
            own_tab.setText(getResources().getString(R.string.project_owner));
            owName.setText(detailItem.getProjectOwnerName());
            if (detailItem.getProjectOwnerName().length() == 0) {
                owName.setText("-");
            }

            owCompany.setText(detailItem.getProjectOwnerName());
            if (detailItem.getProjectOwnerName().length() == 0) {
                owCompany.setText("-");
            }

            owTel.setText(detailItem.getProjectOwnerPhone());
            if (detailItem.getProjectOwnerPhone().length() == 0) {
                owTel.setText("-");
            }

            owLine.setText(detailItem.getProjectOwnerLine());
            if (detailItem.getProjectOwnerLine().length() == 0) {
                owLine.setText("-");
            }
        }


//            Customer
        cusName.setText(detailItem.getCustomerName());
        if (detailItem.getCustomerName().length() == 0) {
            cusName.setText("-");
        }

        cusCompany.setText(detailItem.getCustomerCompany());
        if (detailItem.getCustomerCompany().length() == 0) {
            cusCompany.setText("-");
        }

        cusTel.setText(detailItem.getCustomerPhone());
        customerTel = detailItem.getCustomerPhone();
        customerPhoneListItem = detailItem.getCustomerPhoneList();
        if (detailItem.getCustomerPhone().length() == 0) {
            cusTel.setText("-");
            customerTel = "";
        }

        cusLine.setText(detailItem.getCustomerLine());
        if (detailItem.getCustomerLine().length() == 0) {
            cusLine.setText("-");
        }

        getProjectType();
    }

    private Call<String> callProjectType() {
        return rudyService.projectType(
                Utils.APP_LANGUAGE
        );
    }

    public void getProjectType() {
        showLoading(this);
        callProjectType().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectType projectDetail = gson.fromJson(resp, ProjectType.class);
                    showProjectTypeData(projectDetail);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }

    public void setLayoutAdd(final List<PhaseprocessItem> opportunities_alls) {
        layout_add.removeAllViews();
        Typeface bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        Typeface reg = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view;
        for (int i = 0; i < opportunities_alls.size(); i++) {
            view = layoutInflater.inflate(R.layout.item_view, layout_add, false);
            phaseId = view.findViewById(R.id.phase_id);
            phaseName = view.findViewById(R.id.phase_name);
            phaseSales = view.findViewById(R.id.phase_sales);
            phaseStatus = view.findViewById(R.id.phase_status);
            phaseOppView = view.findViewById(R.id.phase_opp_view);
            bg_status = view.findViewById(R.id.bg_status);

            phaseId.setText(opportunities_alls.get(i).getRank());
            phaseName.setText(opportunities_alls.get(i).getPhaseName());
            phaseSales.setText(opportunities_alls.get(i).getTrans());
            phaseOppView.setText(opportunities_alls.get(i).getOpportunity());
//
//            phaseId.setText(i + 1 + "");
//            phaseName.setText(opportunities_alls.get(i).getPhase_name());
//            phaseSales.setText(opportunities_alls.get(i).getPhase_sales());
//            phaseOppView.setText(opportunities_alls.get(i).getPhase_opp());
            phaseId.setTypeface(bold);
            phaseName.setTypeface(reg);
            phaseSales.setTypeface(reg);
            phaseOppView.setTypeface(reg);


            if (opportunities_alls.get(i).getStatus().contains("1")) {
//                phaseStatus.setTextColor(getResources().getColor(R.color.view_finish));
                phaseStatus.setText(getResources().getString(R.string.succeed));
                bg_status.setBackground(getResources().getDrawable(R.drawable.bg_status_green));

            } else {
//                phaseStatus.setTextColor(getResources().getColor(R.color.view_unfinish));
                phaseStatus.setText(getResources().getString(R.string.process));
                bg_status.setBackground(getResources().getDrawable(R.drawable.bg_status_yellow));
            }
            phaseStatus.setTypeface(bold);
            final int finalI = i;
//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    Intent intent = new Intent(ProjectDetailActivity.this, BOQActivity.class);
////                    intent.putExtra("phaseId", arrayList.get(finalI).getPhase_id());
////                    intent.putExtra("totalfloor", floor.getText().toString());
////                    intent.putExtra("area", area.getText().toString());
////                    startActivity(intent);
//                }
//            });
            layout_add.addView(view, i);
        }
    }


    public void showView(View v) {
        lineCus.setVisibility(View.INVISIBLE);
        lineStat.setVisibility(View.INVISIBLE);
        lineDoc.setVisibility(View.INVISIBLE);
        lineSave.setVisibility(View.INVISIBLE);
        cusLayout.setVisibility(View.GONE);
        statusLayout.setVisibility(View.GONE);
        docLayout.setVisibility(View.GONE);
        recordLayout.setVisibility(View.GONE);
        txtCus.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallest);
        txtStat.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallest);
        txtDoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallest);
        txtSave.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallest);
        txtCus.setTextColor(getResources().getColor(R.color.white));
        txtStat.setTextColor(getResources().getColor(R.color.white));
        txtDoc.setTextColor(getResources().getColor(R.color.white));
        txtSave.setTextColor(getResources().getColor(R.color.white));
        tabCus.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_blue));
        tabStat.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_blue));
        tabDoc.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_blue));
        tabSave.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_blue));
        if (v == tabCus) {
            txtCus.setTextSize(TypedValue.COMPLEX_UNIT_PX, standard);
            txtCus.setTextColor(getResources().getColor(R.color.colorPrimary));
            tabCus.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_white));
            lineCus.setVisibility(View.VISIBLE);
            cusLayout.setVisibility(View.VISIBLE);

        } else if (v == tabStat) {
            txtStat.setTextSize(TypedValue.COMPLEX_UNIT_PX, standard);
            txtStat.setTextColor(getResources().getColor(R.color.colorPrimary));
            tabStat.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_white));
            lineStat.setVisibility(View.VISIBLE);
            statusLayout.setVisibility(View.VISIBLE);

        } else if (v == tabDoc) {
            txtDoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, standard);
            txtDoc.setTextColor(getResources().getColor(R.color.colorPrimary));
            tabDoc.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_white));
            lineDoc.setVisibility(View.VISIBLE);
            docLayout.setVisibility(View.VISIBLE);
            DealByPhaseActivity.print = false;

        } else {
            txtSave.setTextSize(TypedValue.COMPLEX_UNIT_PX, standard);
            txtSave.setTextColor(getResources().getColor(R.color.colorPrimary));
            tabSave.setBackground(getResources().getDrawable(R.drawable.bg_color_tab_white));
            lineSave.setVisibility(View.VISIBLE);
            recordLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, 20, bottom);
            view.requestLayout();
        }
    }

    public void onShowPhoto(Bitmap bitmap) {
        if (bitmap != null) {
            final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.setContentView(R.layout.view_image);
            dialog.setCancelable(true);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.9);
            dialog.getWindow().setLayout(width, height);
            SubsamplingScaleImageView image = dialog.findViewById(R.id.langImage);
            Button close = dialog.findViewById(R.id.close);
            RelativeLayout hideView = dialog.findViewById(R.id.hideView);
            image.setImage(ImageSource.bitmap(bitmap));
            image.setMaxScale(20.0f);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dialog.dismiss();
                }
            });
            hideView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dialog.dismiss();
                }
            });
            dialog.show();
        }

    }


    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }

        if (v == moreBtn) {
            showToast(this, " COMING SOON");
        }

        if (v == deal_phase) {
            if (detailItem.getProjectTypeId().equalsIgnoreCase("11")) {
                Intent intent = new Intent(this, DealByProjectActivity.class);
                intent.putExtra("projectId", projectId);
                intent.putExtra("projectTypeId", detailItem.getProjectTypeId());
                intent.putExtra("phaseId", detailItem.getPhaseId());

                startActivity(intent);
            } else {
                DealByPhaseActivity.QUICKSALE = "1";
                Utils.savePrefer(this, "quickSale", "0");
                String transaction_id = "";
                if (detailItem.getPr().size() > 0) {
                    for (int i = 0; i < detailItem.getPr().size(); i++) {
                        if (detailItem.getPr().get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr")) {
                            transaction_id = detailItem.getPr().get(i).getTransactionId();
                        }
                    }
                }

                DealByPhaseActivity.print = false;
                Intent intent = new Intent(this, DealByPhaseActivity.class);
                intent.putExtra("phase", detailItem.getPhaseId());
                intent.putExtra("projectId", Utils.project_id);
                intent.putExtra("projectTypeId", detailItem.getProjectTypeId());
                intent.putExtra("transaction_id", transaction_id);
                intent.putExtra("phaseId", detailItem.getPhaseId());

                startActivity(intent);

            }


        }

        if (v == edit_department) {
            DepartmentDataActivity.isEdit = true;
            Intent intent = new Intent(this, DepartmentDataActivity.class);
            intent.putExtra("unit_number", detailItem.getUnits());
            intent.putExtra("floor_number", detailItem.getProjectStories());
            intent.putExtra("unit_area", detailItem.getUnitArea());
            intent.putExtra("unit_budget", detailItem.getUnitBudget());
            intent.putExtra("project_name", detailItem.getProjectName());
            intent.putExtra("cateId", detailItem.getProjectTypeId());
            intent.putExtra("phaseId", detailItem.getPhaseId());
//            if(detailItem.getProjectTypeId().equalsIgnoreCase("11")){
//                intent.putExtra("catePosition", "10");
//                intent.putExtra("phasePosition", "0");
//            }else{
            intent.putExtra("catePosition", String.valueOf(catePosition));
            intent.putExtra("phasePosition", detailItem.getPhaseId());
//            }

            DepartmentDataActivity.projectId = detailItem.getId();
            startActivity(intent);
        }

        if (v == checkin) {
            requestPermission();
        }
        if (v == editimage) {
            Intent intent = new Intent(this, TakePhotoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("listImage", groupGalleryModels);
            intent.putExtras(bundle);
            TakePhotoActivity.projectId = detailItem.getId();
            startActivity(intent);
        }

        if (v == edit_location) {
            Intent intent = new Intent(this, AddCurrentLocationActivity.class);
            AddCurrentLocationActivity.isEdit = true;
            AddCurrentLocationActivity.editlatlng = detailItem.getLatLng();
            AddCurrentLocationActivity.shopID = detailItem.getId();
            startActivity(intent);
        }

        if (v == tabCus) {
            showView(v);
        }
        if (v == tabStat) {
            showView(v);
        }
        if (v == tabSave) {
            showView(v);
        }
        if (v == tabDoc) {
            showView(v);
        }
        if (v == editCustomer) {
            Intent intent = new Intent(this, CustomerDataActivity.class);

            intent.putExtra("customer_name", this.detailItem.getCustomerName());
            intent.putExtra("customer_phone", this.detailItem.getCustomerPhone());
            intent.putExtra("customer_note", this.detailItem.getCustomerNote());
            intent.putExtra("customer_line", this.detailItem.getCustomerLine());
            if (detailItem.getOwnerType().equalsIgnoreCase("2")) {
                intent.putExtra("project_owner_name", this.detailItem.getHouseOwnerName());
                intent.putExtra("project_owner_phone", this.detailItem.getHouseOwnerPhone());
                intent.putExtra("project_owner_line", this.detailItem.getHouseOwnerLine());
                intent.putExtra("project_owner_note", this.detailItem.getHouseOwnerNote());
            } else {
                intent.putExtra("project_owner_name", this.detailItem.getProjectOwnerName());
                intent.putExtra("project_owner_phone", this.detailItem.getProjectOwnerPhone());
                intent.putExtra("project_owner_line", this.detailItem.getProjectOwnerLine());
                intent.putExtra("project_owner_note", this.detailItem.getProjectOwnerNote());
            }


            intent.putExtra("projectId", this.detailItem.getId());
            intent.putExtra("ctype", this.detailItem.getCustomerType());
            intent.putExtra("ctype_owner", this.detailItem.getOwnerType());
            intent.putExtra("tax_number", this.detailItem.getCustomerTaxNo());
            intent.putExtra("customer_code", this.detailItem.getCustomerCode());
            intent.putExtra("champ_customer_code", this.detailItem.getChampCustomerCode());
            intent.putExtra("customer_company", this.detailItem.getCustomerCompany());


            startActivity(intent);
        }

        if (v == fav) {
            if (BaseActivity.favItemSize > 0) {
                TransactionDetailActivity.from_fav = 1;
                FavoriteDetailActivity.productId = "";
                FavoriteDetailActivity.isDelete = false;
                Intent intent = new Intent(this, FavoriteDetailActivity.class);
                intent.putExtra("transaction_id", transaction_id);
                startActivity(intent);


            } else {
                showToast(this, getResources().getString(R.string.toast_pls_add_product_interested));
            }

        }

        if (v == basket) {
            if (basketSize > 0) {
                TransactionDetailActivity.productId = "";
                TransactionDetailActivity.isDelete = false;
                Intent intent = new Intent(this, TransactionDetailActivity.class);
                TransactionDetailActivity.from_fav = 0;
                intent.putExtra("transaction_id", transaction_id);
                startActivity(intent);

            } else {
                showToast(this, getResources().getString(R.string.toast_pls_add_product));
            }

        }
    }

    private void showCheckinDialog(boolean isCheckIn) { //TODO
        final Dialog dialog = new Dialog(ProjectDetailActivity.this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.checkin_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout check_tel = dialog.findViewById(R.id.check_tel);
        RelativeLayout checkin_onsite = dialog.findViewById(R.id.checkin_onsite);
        RelativeLayout history_checkin = dialog.findViewById(R.id.history_checkin);
        RelativeLayout cannot_checkin = dialog.findViewById(R.id.cannot_checkin);
        LinearLayout worng_area = dialog.findViewById(R.id.worng_area);
        ImageView close_btn = dialog.findViewById(R.id.close_btn);
        CardView refresh_location = dialog.findViewById(R.id.refresh_location);


//        if(!isCheckIn){
//            checkin_onsite.setVisibility(View.GONE);
//            worng_area.setVisibility(View.VISIBLE);
//            cannot_checkin.setVisibility(View.VISIBLE);
//        }

        refresh_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                checkin.performClick();
            }
        });

        check_tel.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                typeCheckIn = 1;
                if (customerPhoneListItem.size() <= 0) {
                    showToast(ProjectDetailActivity.this, getResources().getString(R.string.toast_pls_update_phone_contractor));
                } else {
//                    readDummyCallog();
                    com.scg.rudy.model.pojo.byproject.ItemsItem itemList = new com.scg.rudy.model.pojo.byproject.ItemsItem();
                    itemList.setCustomerPhoneList(customerPhoneListItem);
                    checkInTel(itemList);
                }

            }
        });
        checkin_onsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                typeCheckIn = 2;
                postCheckIn(isCheckIn);

//                startActivity(new Intent(ProjectDetailActivity.this, CheckInAreaActivity.class));
            }
        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        history_checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(ProjectDetailActivity.this, CheckInHistoryActivity.class);
                intent.putExtra("projectId", projectId);
                startActivity(intent);
            }
        });


        dialog.show();
    }

    private void callCkeckin(String tel) {
        Utils.savePrefer(getApplicationContext(), "call", tel);
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        callIntent.setData(Uri.parse("tel:" + tel));
        startActivity(callIntent);
    }


    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        boolean isNetwork = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Location bestLocation = null;
        int time = 20 * 1000 * 60;
        if (isNetwork) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, time, 15, mLocationListener);
            bestLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        if (bestLocation == null) {
            if (isGPS) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, 15, mLocationListener);
                bestLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }


        return bestLocation;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return true;
    }

    public void hideKeyboard() {

    }


    public void showData(ProjectDetail projectDetail) {
        titleName.setText(projectDetail.getItems().getProjectName());
        setData(projectDetail.getItems());
    }

    public void showErrorIsNull() {

    }

    public void BodyError(ResponseBody responseBodyError) {

    }

    public void Failure(Throwable t) {

    }

    public void showProjectTypeData(ProjectType projectType) {
        List<ItemsItem> typeItems = projectType.getItems();
        for (int i = 0; i < typeItems.size(); i++) {
            List<PhasesItem> phases = typeItems.get(i).getPhases();


            if (detailItem.getProjectTypeId().equalsIgnoreCase(typeItems.get(i).getId())) {
//                type.setText(typeItems.get(i).getName());
                catePosition = i;
            }
            for (int j = 0; j < phases.size(); j++) {
//                if (detailItem.getPhaseId().equalsIgnoreCase(phases.get(j).getId())) {
//                    phasePosition = j;
//                }

            }
        }

    }

    public void showProjectTypeErrorIsNull() {

    }

    public void ProjectTypeBodyError(ResponseBody responseBodyError) {

    }

    public void ProjectTypeFailure(Throwable t) {

    }

    private Call<String> callPostCheckin() {
        return rudyService.postCheckin(
                Utils.APP_LANGUAGE,
                projectId,
                String.valueOf(typeCheckIn),
                "-",
                user_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void postCheckIn(boolean CheckIn) {
        showLoading(this);
        callPostCheckin().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                String resp = response.body();
                if(resp.contains("200")){
                    try {
                        Utils.savePrefer(getApplicationContext(), "call", "");
                        JSONObject object = new JSONObject(resp);
                        JSONObject itemObj = object.getJSONObject("items");
                        checkInID = itemObj.optString("id", "");
                        if (typeCheckIn == 1) {
                            Intent intent = new Intent(ProjectDetailActivity.this, CheckInTelActivity.class);
                            intent.putExtra("projectId", projectId);
                            intent.putExtra("checkInID", checkInID);
                            intent.putExtra("CustomerName", detailItem.getCustomerName());
                            intent.putExtra("CustomerPhone", detailItem.getCustomerPhone());
                            intent.putExtra("CustomerPic", detailItem.getCustomerPic());
                            intent.putExtra("type", type.getText().toString());
                            intent.putExtra("subPhase", detailItem.getPhaseId());
                            startActivity(intent);
                        } else {
                            if (detailItem.getLatLng().length() > 5) {
                                String[] lntlng = detailItem.getLatLng().split(",");
                                ProjectDetailActivity.isUpdateCheckin = "0";
                                Intent intent = new Intent(ProjectDetailActivity.this, CheckInAreaActivity.class);
                                intent.putExtra("curLat", latitude);
                                intent.putExtra("curLng", longitude);
                                intent.putExtra("pLat", lntlng[0]);
                                intent.putExtra("pLng", lntlng[1]);
                                intent.putExtra("pName", detailItem.getProjectName());
                                intent.putExtra("checkInStatus", checkInStatus);
                                intent.putExtra("isCheckIn", CheckIn);
                                intent.putExtra("projectId", projectId);
                                intent.putExtra("checkInID", checkInID);
                                intent.putExtra("type", type.getText().toString());
                                intent.putExtra("subPhase", detailItem.getPhaseId());
                                startActivity(intent);
                            } else {
                                showToast(ProjectDetailActivity.this, getResources().getString(R.string.toast_pls_specify_location));
                            }


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=add",e);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void postCheckIn(boolean CheckIn) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
//        formData.add("project_id", projectId);
//        formData.add("type", String.valueOf(typeCheckIn));
//        formData.add("detail", "-");
//        formData.add("user_id", user_id);
//
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.postData(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=add", requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    try {
//                        Utils.savePrefer(getApplicationContext(), "call", "");
//                        JSONObject object = new JSONObject(string);
//                        JSONObject itemObj = object.getJSONObject("items");
//                        checkInID = itemObj.optString("id", "");
//                        if (typeCheckIn == 1) {
//                            Intent intent = new Intent(ProjectDetailActivity.this, CheckInTelActivity.class);
//                            intent.putExtra("projectId", projectId);
//                            intent.putExtra("checkInID", checkInID);
//                            intent.putExtra("CustomerName", detailItem.getCustomerName());
//                            intent.putExtra("CustomerPhone", detailItem.getCustomerPhone());
//                            intent.putExtra("CustomerPic", detailItem.getCustomerPic());
//                            intent.putExtra("type", type.getText().toString());
//                            intent.putExtra("subPhase", detailItem.getPhaseId());
//                            startActivity(intent);
//                        } else {
//                            if (detailItem.getLatLng().length() > 5) {
//                                String[] lntlng = detailItem.getLatLng().split(",");
//                                ProjectDetailActivity.isUpdateCheckin = "0";
//                                Intent intent = new Intent(ProjectDetailActivity.this, CheckInAreaActivity.class);
//                                intent.putExtra("curLat", latitude);
//                                intent.putExtra("curLng", longitude);
//                                intent.putExtra("pLat", lntlng[0]);
//                                intent.putExtra("pLng", lntlng[1]);
//                                intent.putExtra("pName", detailItem.getProjectName());
//                                intent.putExtra("checkInStatus", checkInStatus);
//                                intent.putExtra("isCheckIn", CheckIn);
//                                intent.putExtra("projectId", projectId);
//                                intent.putExtra("checkInID", checkInID);
//                                intent.putExtra("type", type.getText().toString());
//                                intent.putExtra("subPhase", detailItem.getPhaseId());
//                                startActivity(intent);
//                            } else {
//                                showToast(ProjectDetailActivity.this, getResources().getString(R.string.toast_pls_specify_location));
//                            }
//
//
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=add",e);
//                    }
//                }
//            }
//        }.execute();
//    }

    private Call<String> callCheckIsCheckgin(String _lat, String _lan) {
        return rudyService.checkIsCheckgin(
                Utils.APP_LANGUAGE,
                projectId,
                _lat,
                _lan
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void checkIsCheckgin(String _lat, String _lan) {
        showLoading(this);
        callCheckIsCheckgin(_lat, _lan).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                String resp = response.body();
                if(resp.contains("200")){
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONObject itemObj = object.getJSONObject("items");
                        checkInStatus = itemObj.optString("distance", "");

                        if (itemObj.optString("isCheck", "").equalsIgnoreCase("1")) {
                            isCheckIn = true;
                        } else {
                            isCheckIn = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject",e);
                    }
                }
                showCheckinDialog(isCheckIn);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void checkIsCheckgin(final String projectId,final String lat,final String lng) {
//        showLoading(this);
//        final RequestBody requestBody;
//        FormBody.Builder formData = new FormBody.Builder();
////        ArrayList<String> lisData = new ArrayList<>();
//        formData.add("project_id", projectId);
//        formData.add("lat", lat);
//        formData.add("lng", lng);
//        requestBody = formData.build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.postData(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject", requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONObject itemObj = object.getJSONObject("items");
//                        checkInStatus = itemObj.optString("distance", "");
//
//                        if (itemObj.optString("isCheck", "").equalsIgnoreCase("1")) {
//                            isCheckIn = true;
//                        } else {
//                            isCheckIn = false;
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/?act=checkVisitProject",e);
//                    }
//                }
//                showCheckinDialog(isCheckIn);
//
//            }
//        }.execute();
//    }

    private Call<String> callDeleteCheckIn() {
        return rudyService.deleteCheckIn(
                Utils.APP_LANGUAGE,
                checkInID
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void DeleteCheckIn() {
        showLoading(ProjectDetailActivity.this);
        callDeleteCheckIn().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")){
                    ProjectDetailActivity.isUpdateCheckin = "0";
                    checkInID = null;
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void DeleteCheckIn() {
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/checkin/" + checkInID + "/?act=del");
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                ProjectDetailActivity.isUpdateCheckin = "0";
//                checkInID = null;
//            }
//        }.execute();
//    }

    private Call<String> callUpdateTranId(String _projectid) {
        return rudyService.updateTransId(
                Utils.APP_LANGUAGE,
                _projectid
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void updateTranId(final String _projectId) {
        showLoading(ProjectDetailActivity.this);
        callUpdateTranId(_projectId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    try {
                        JSONObject object = new JSONObject(resp);
                        JSONArray array = object.getJSONArray("items");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject itemObject = array.getJSONObject(i);
                            if (itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")) {
                                transaction_id = itemObject.optString("transaction_id");
                            }
                        }

                        if (!transaction_id.equalsIgnoreCase("")) {
//                            getBasket(getGetBasket(Utils.APP_LANGUAGE, transaction_id));
                            getBasket();
                        } else {
//                            getFAV(getGetFav(Utils.APP_LANGUAGE, Utils.project_id));
                            getFAV();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/transaction/" + projectId,e);
                    }
                } else {
                    getFAV();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void updateTranId(final String projectId) {
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/transaction/" + projectId);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    try {
//                        JSONObject object = new JSONObject(string);
//                        JSONArray array = object.getJSONArray("items");
//                        for (int i = 0; i < array.length(); i++) {
//                            JSONObject itemObject = array.getJSONObject(i);
//                            if (itemObject.optString("status_txt").toLowerCase().equalsIgnoreCase("pr")) {
//                                transaction_id = itemObject.optString("transaction_id");
//                            }
//                        }
//
//                        if (!transaction_id.equalsIgnoreCase("")) {
//                            getBasket(getGetBasket(Utils.APP_LANGUAGE, transaction_id));
//                        } else {
////                            getFAV(getGetFav(Utils.APP_LANGUAGE, Utils.project_id));
//                            getFAV();
//                        }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        LogException(ApiEndPoint.HOST + "/app/merudy/MTIzcXdlMTIz6969/th/transaction/" + projectId,e);
//                    }
//                } else {
////                    getFAV(getGetFav(Utils.APP_LANGUAGE, Utils.project_id));
//                    getFAV();
//                }
//            }
//        }.execute();
//    }

    private Call<String> callGetBasket() {
        return rudyService.getTransectionDetail(
                Utils.APP_LANGUAGE,
                transaction_id
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getBasket(){
        showLoading(ProjectDetailActivity.this);
        callGetBasket().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
//                    badge_fav.setVisibility(View.VISIBLE);
                    badgeBasket.setVisibility(View.VISIBLE);
                    setBasket(resp);
                } else {
                    badgeBasket.setVisibility(View.GONE);
                    basketSize = 0;
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private void getBasket(final String url) {
//        Log.i("getBasket ", " : " + url);
//        showLoading(this);
//
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
////                getFAV(getGetFav(Utils.APP_LANGUAGE, Utils.project_id));
//                getFAV();
//
//
//                if (string.contains("200")) {
////                    badge_fav.setVisibility(View.VISIBLE);
//                    badgeBasket.setVisibility(View.VISIBLE);
//                    setBasket(string);
//                } else {
//                    badgeBasket.setVisibility(View.GONE);
//                    basketSize = 0;
//
//                }
//            }
//        }.execute();
//    }

    public void setBasket(String items) {
        try {


            JSONObject object = new JSONObject(items);
            JSONObject itemObj = object.getJSONObject("items");
            JSONArray itemArray = itemObj.getJSONArray("pr");
            basketSize = itemArray.length();
            if (basketSize == 0) {
                badgeBasket.setVisibility(View.GONE);
            }
            numberBasket.setText(basketSize + "");
            getFAV();
        } catch (JSONException e) {
            e.printStackTrace();
            LogException(items ,e);

        }
    }

    private Call<String> callProjectDetail() {
        return rudyService.projectDetail(
                Utils.APP_LANGUAGE,
                projectId
        );
    }

    private Call<String> callGetFAV() {
        return rudyService.getFAV(
                Utils.APP_LANGUAGE,
                projectId
        );
    }

    @SuppressLint("StaticFieldLeak")
    private void getFAV() {
        showLoading(ProjectDetailActivity.this);
        callGetFAV().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    callProjectDetail().enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            hideLoading();
                            Gson gson2 = new Gson();
                            String resp2 = response.body();
                            if (resp2.contains("200")) {
                                ProjectDetail projectDetail = gson2.fromJson(resp2, ProjectDetail.class);
                                showData(projectDetail);
                                setFavIcon(resp);
                            } else {
                                APIError error = gson2.fromJson(resp2, APIError.class);
                                showToast(ProjectDetailActivity.this, error.getItems());
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            hideLoading();
                            showToast(ProjectDetailActivity.this, t.getMessage());
                        }
                    });
                } else {
                    hideLoading();
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ProjectDetailActivity.this, error.getItems());
                    BaseActivity.favItemSize = 0;
                    badgeFav.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                BaseActivity.favItemSize = 0;
                badgeFav.setVisibility(View.GONE);
                showToast(ProjectDetailActivity.this, t.getMessage());
            }
        });
    }



//    @SuppressLint("StaticFieldLeak")
//    private void getFAV(final String url) {
//        Log.i("", "Customer: " + url);
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
////                getProjectDetail();
//                if (string.contains("200")) {
//
//                    callProjectDetail().enqueue(new Callback<String>() {
//                        @Override
//                        public void onResponse(Call<String> call, Response<String> response) {
//                            hideLoading();
//                            Gson gson = new Gson();
//                            String resp = response.body();
//                            if (resp.contains("200")) {
//                                ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
//                                showData(projectDetail);
//                                setFavIcon(string);
//                            } else {
//                                APIError error = gson.fromJson(resp, APIError.class);
//                                showToast(ProjectDetailActivity.this, error.getItems());
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<String> call, Throwable t) {
//                            hideLoading();
//                            showToast(ProjectDetailActivity.this, t.getMessage());
//                        }
//                    });
//                } else {
//                    BaseActivity.favItemSize = 0;
//                    badgeFav.setVisibility(View.GONE);
//                }
//            }
//        }.execute();
//    }

//    public void getProjectDetail() {
//        showLoading(this);
//        callProjectDetail().enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                hideLoading();
//                Gson gson = new Gson();
//                String resp = response.body();
//                if (resp.contains("200")) {
//                    ProjectDetail projectDetail = gson.fromJson(resp, ProjectDetail.class);
//                    showData(projectDetail);
//                } else {
//                    APIError error = gson.fromJson(resp, APIError.class);
//                    showToast(ProjectDetailActivity.this, error.getItems());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                hideLoading();
//                showToast(ProjectDetailActivity.this, t.getMessage());
//            }
//        });
//    }

    public void setFavIcon(String items) {
        try {
            JSONObject object = new JSONObject(items);
            JSONArray itemArray = object.getJSONArray("items");
//            BaseActivity.favItemSize = itemArray.length();
            BaseActivity.favItemSize = itemArray.getJSONObject(0).getInt("total") + itemArray.getJSONObject(1).getInt("total");


        } catch (JSONException e) {
            e.printStackTrace();
            LogException(items ,e);
        }
        if (BaseActivity.favItemSize > 0) {
            badgeFav.setVisibility(View.VISIBLE);
            numberFav.setText(BaseActivity.favItemSize + "");
        } else {
            badgeFav.setVisibility(View.GONE);
        }


    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission() {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(this, permissionsList, Manifest.permission.ACCESS_FINE_LOCATION)) {
            permissionsNeeded.add("GPS");
        }
//        if (!addPermission(this,permissionsList, Manifest.permission.READ_CALL_LOG)){
//            permissionsNeeded.add("Read Call Log");
//        }
        if (!addPermission(this, permissionsList, Manifest.permission.CALL_PHONE)) {
            permissionsNeeded.add("Call");
        }
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(ProjectDetailActivity.this, message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        showCheckLocation();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void reQuestPermissionGPS() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(ProjectDetailActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(
                    ProjectDetailActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(
                        ProjectDetailActivity.this,
                        "You need to allow access to Contacts",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(
                                        ProjectDetailActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        REQUEST_CODE_ASK_PERMISSIONS);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(
                    ProjectDetailActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
        showCheckLocation();
    }

    private void showCheckLocation() {
        mLastLocation = getLastKnownLocation();
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            checkIsCheckgin(String.valueOf(latitude), String.valueOf(longitude));
        } else {
            checkIsCheckgin(String.valueOf(latitude), String.valueOf(longitude));
        }
    }

    @SuppressLint("MissingPermission")
    private void readDummyCallog() {
//        StringBuffer sb = new StringBuffer();
//        managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
//        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
//        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
//        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
//        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        sb.append("Call//");
//        if(managedCursor.moveToFirst()){
//            if(Utils.isTabletDevice(this)){
//                String phNumber = managedCursor.getString(number);
//                String callType = managedCursor.getString(type);
//                String callDate = managedCursor.getString(date);
//                Date callDayTime = new Date(Long.valueOf(callDate));
//                String dateCall = TimeUtils.getTime(Long.valueOf(callDate),TimeUtils.DEFAULT_DATE_ONLY);
//                String callDuration = managedCursor.getString(duration);
//                String dir = null;
//                int dircode = Integer.parseInt(callType);
//                if(Utils.isTabletDevice(this)){
//                    if(dircode==3){
//                        dircode = 2;
//                    }
//                }
//                switch (dircode) {
//                    case CallLog.Calls.OUTGOING_TYPE:
////                            dir = "OUTGOING";
////                            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
////                                    + dir + " \nCall Date:--- " + TimeUtils.getTime(Long.valueOf(callDate))
////                                    + " \nCall duration in sec :--- " + callDuration);
////                            sb.append("\n----------------------------------\n");
//                        sb.append(phNumber+","+dateCall+","+callDuration);
//
//                        break;
//
//                        default:
//                            break;
//                }
//            }else{
//                while (managedCursor.moveToNext()) {
//                    if(managedCursor.isLast()){
//                        String phNumber = managedCursor.getString(number);
//                        String callType = managedCursor.getString(type);
//                        String callDate = managedCursor.getString(date);
//                        Date callDayTime = new Date(Long.valueOf(callDate));
//                        String dateCall = TimeUtils.getTime(Long.valueOf(callDate),TimeUtils.DEFAULT_DATE_ONLY);
//                        String callDuration = managedCursor.getString(duration);
//                        String dir = null;
//                        int dircode = Integer.parseInt(callType);
//                        switch (dircode) {
//                            case CallLog.Calls.OUTGOING_TYPE:
////                            dir = "OUTGOING";
////                            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
////                                    + dir + " \nCall Date:--- " + TimeUtils.getTime(Long.valueOf(callDate))
////                                    + " \nCall duration in sec :--- " + callDuration);
////                            sb.append("\n----------------------------------\n");
//                                sb.append(phNumber+","+dateCall+","+callDuration);
//                                break;
//
//                                default:
//                                    break;
//                        }
//                    }
//                }
//            }
//
//
////            while (managedCursor.moveToNext()) {
////                if(managedCursor.isLast()){
////
////                }
////            }
//        }


        isCheckIsCorrectCall("");


    }

    private void isCheckIsCorrectCall(String callHistory) {

        postCheckIn(isCheckIn);

//        customerTel
//        dateNow
//        String realCallTxt = callHistory.replace("Call//","");
//        if(realCallTxt.replace(",","").isEmpty()){
//            showToast(ProjectDetailActivity.this,"การเช็คอินด้วยการโทรขัดข้องกรุณาลองอีกครั้ง");
//        }else{
//            String[] callTxt = realCallTxt.split(","); //0 phone ,1 date ,2 dulation
//            if(Integer.parseInt(callTxt[2])<5){
//                showToast(ProjectDetailActivity.this,"การเช็คอินด้วยการโทรขัดข้องกรุณาลองอีกครั้ง");
//            }else {
//                if(customerTel.equalsIgnoreCase(callTxt[0])){
//                    postCheckIn(isCheckIn);
//                }
////                showToast(ProjectDetailActivity.this,realCallTxt+"\nand Cus phone : "+customerTel+", "+dateNow);
//            }
//
//
//
//        }
//
//        managedCursor.close();

    }


    // Permission check functions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    insertDummyContact();
                    showCheckLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(ProjectDetailActivity.this, "GPS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.READ_CALL_LOG, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    /* && perms.get(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED*/) {
                    // All Permissions Granted
                    showCheckLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(ProjectDetailActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void checkInTel(com.scg.rudy.model.pojo.byproject.ItemsItem itemList) {
        final Dialog dialog = new Dialog(this, R.style.full_screen_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.popup_tel_checkin);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        final RecyclerView addViewNameTel = dialog.findViewById(R.id.addViewNameTel);
        CheckInTelAdapter checkInTelAdapter = new CheckInTelAdapter(this, itemList.getCustomerPhoneList(), this, dialog);
        addViewNameTel.setLayoutManager(new LinearLayoutManager(this));
        addViewNameTel.setItemAnimator(new DefaultItemAnimator());
        addViewNameTel.setAdapter(checkInTelAdapter);
        dialog.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void requestPermission(String tel) {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();

        if (!addPermission(this, permissionsList, Manifest.permission.CALL_PHONE)) {
            permissionsNeeded.add("Call");
        }
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showMessageOKCancel(ProjectDetailActivity.this, message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
        callCkeckin(tel);
    }
}
