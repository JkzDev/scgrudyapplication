package com.scg.rudy.ui.project_detail_group.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.project_group_detail.ProjectListItem;
import com.scg.rudy.ui.by_projects.ByProjectsListener;
import com.scg.rudy.ui.project_detail.ProjectDetailActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class ByProjectGroupRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ProjectListItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private LayoutInflater inflater;
    private ByProjectsListener listener;


    public ByProjectGroupRecyclerAdapter(Context context, ByProjectsListener listener) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = new ArrayList<>();
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.project_group_detail_list_item, parent, false);
            return new ProjectViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new LoadingVH(viewLoading);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(final @NonNull RecyclerView.ViewHolder vholder, final int position) {
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ProjectListItem projectModel = arrayList.get(position);
            final ProjectViewHolder holder = (ProjectViewHolder) vholder;
//                final String[] createDate = projectModel.getAddedDatetime().split(" ");
//                final String path = projectModel.getPhasePic();
//                if(path!=null||path.length()>0){
//                    Glide.with(context).clear(holder.projectImage);
//                    holder.projectImage.setImageDrawable(context.getDrawable(R.drawable.bg_tower));
//                }else{
//                    Glide.with(context)
//                            .load(path)
//                            .into(holder.projectImage);
//                }
//
//            holder.status_txt.setText(projectModel.getStatusTxt());
            holder.name.setText(projectModel.getProjectName());
            holder.projectType.setText(projectModel.getRankName());
            holder.devName.setText(projectModel.getPhaseName());
//            holder.totalSite.setText(projectModel.getUnits() + context.getResources().getString(R.string.ea));
            holder.totalSite.setText(projectModel.getProjectTypeName());
            holder.totalPrice.setText(projectModel.getUnitBudget() + context.getResources().getString(R.string.b));
            holder.clickItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        savePrefer(context, "print", "");
                        Intent intent = new Intent(context, ProjectDetailActivity.class);
                        intent.putExtra("projectId",projectModel.getId());
                        intent.putExtra("name", projectModel.getProjectName());
                        context.startActivity(intent);
//                        }
                }
            });
        }

        else if(itemPosition == LOADING){
            LoadingVH loadingVH = (LoadingVH) vholder;

            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view,double percent,int maxBarSize){
        double fn_size = (percent*maxBarSize)/100;
        if(fn_size==0){
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        }else{
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void initView() {

    }

    public static class ProjectViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout clickItem;
        private ImageView projectImage;
        private TextView name;
        private TextView totalSite;
        private TextView devName;
        private TextView projectType;
        private TextView totalPrice;


        public ProjectViewHolder(View v) {
            super(v);
            clickItem = v.findViewById(R.id.clickItem);
            projectImage = v.findViewById(R.id.project_image);
            name = v.findViewById(R.id.name);
            totalSite = v.findViewById(R.id.total_site);
            devName = v.findViewById(R.id.dev_name);
            projectType = v.findViewById(R.id.project_type);
            totalPrice = v.findViewById(R.id.total_price);
        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ProjectListItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
        ProjectListItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ProjectListItem getItem(int position) {
        return arrayList.get(position);
    }


    public void add(ProjectListItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }
    public void addAll(List<ProjectListItem> projectList) {
        for (ProjectListItem result : projectList) {
            add(result);
        }
    }


}