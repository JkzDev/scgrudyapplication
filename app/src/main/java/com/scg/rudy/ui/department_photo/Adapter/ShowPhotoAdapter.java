package com.scg.rudy.ui.department_photo.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.scg.rudy.R;
import com.scg.rudy.ui.department_photo.TakePhotoInterface;

import java.util.ArrayList;

public class ShowPhotoAdapter extends RecyclerView.Adapter<ShowPhotoAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<String> mArrayList;
    private LayoutInflater mInflater;
    private TakePhotoInterface listener;
    private boolean hideDelButton;

    public ShowPhotoAdapter(Context context, ArrayList<String> arrayList, TakePhotoInterface listener, boolean _hideDelButton) {
        this.mContext = context;
        this.mArrayList = arrayList;
        this.mInflater = LayoutInflater.from(context);
        this.listener = listener;
        this.hideDelButton = _hideDelButton;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_view, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.img2.setImageBitmap(mArrayList.get(position));

        Glide.with(mContext).load(mArrayList.get(position))
                .thumbnail(0.5f)
                .override(200,200)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(((ViewHolder) holder).img2);


        holder.img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onShowPhoto(mArrayList.get(position));
            }
        });
        holder.del2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog.Builder builder =  new MaterialDialog.Builder(mContext)
                        .title(mContext.getResources().getString(R.string.dialog_want_to_delete_picture))
                        .positiveText(mContext.getResources().getString(R.string.agreed))
                        .negativeText(mContext.getResources().getString(R.string.cancel))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                listener.onClickDeletePhoto(mArrayList.get(position));
                                dialog.dismiss();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        });

                MaterialDialog del_dialog = builder.build();
                del_dialog.show();
            }
        });

        if (hideDelButton){
            holder.reDel.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img2;
        RelativeLayout del2;
        RelativeLayout reDel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img2 = itemView.findViewById(R.id.img2);
            del2 = itemView.findViewById(R.id.del2);
            reDel = itemView.findViewById(R.id.reDel);
        }
    }
}
