/*
 * Copyright (C) 2017 Sylwester Sokolowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.ui.department_data_group.provider;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scg.rudy.ui.department_data_group.model.Developer;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.autocomplete.DynamicAutocompleteProvider;

import java.lang.reflect.Type;
import java.util.Collection;

import static com.scg.rudy.data.remote.ApiEndPoint.HOST;

/**
 * @author jackie
 */
public class ExtendedDictionaryAutocompleteProvider extends AbstractProvider implements DynamicAutocompleteProvider<Developer> {
    private   String REST_URL = HOST+"/app/merudy/MTIzcXdlMTIz6969/%s/project?act=list_developer_by_name";
    @Override
    public Collection<Developer> provideDynamicAutocompleteItems(String constraint) {
        String queryUrl = String.format(REST_URL,Utils.APP_LANGUAGE);
        String json = invokeRestQuery(queryUrl,constraint);
        Type type = new TypeToken<Collection<Developer>>(){}.getType();
        return new Gson().fromJson(json,type);
    }
}