package com.scg.rudy.ui.dealbyproject;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.RudyApplication;
import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.List;

public class SkuListViewHolder extends ChildViewHolder {

//  private TextView childTextView;
  private TextView skuName;
  private RelativeLayout hotDeal;
  private RelativeLayout recomend;
  private RelativeLayout favorite;
  private TextView price;
  private TextView price2;
  private TextView stock;
  private ImageView addFav;
  private RelativeLayout addCart;
  private ImageView thumbnail;
  private RelativeLayout commLayout;
  private TextView commTxt;
  private LinearLayout all_data;
  private LinearLayout remain;
  private TextView remain_price;
  private TextView remain_date;
  private ImageView promo_ico;
  private RelativeLayout info;
  private Context context;

  public SkuListViewHolder(View v, Context _context) {
    super(v);
    context = _context;
    skuName = v.findViewById(R.id.skuName);
    hotDeal = v.findViewById(R.id.hot_deal);
    recomend = v.findViewById(R.id.recomend);
    favorite = v.findViewById(R.id.favorite);
    price = v.findViewById(R.id.price);
    price2 = v.findViewById(R.id.price2);
    stock = v.findViewById(R.id.stock);
    addFav = v.findViewById(R.id.add_fav);
    addCart = v.findViewById(R.id.add_cart);
    thumbnail = v.findViewById(R.id.thumbnail);
    commLayout = v.findViewById(R.id.comm_layout);
    commTxt = v.findViewById(R.id.comm_txt);
    all_data = v.findViewById(R.id.all_data);
    remain = v.findViewById(R.id.remain);
    remain_price = v.findViewById(R.id.remain_price);
    remain_date = v.findViewById(R.id.remain_date);
    promo_ico = v.findViewById(R.id.promo_ico);
    info= v.findViewById(R.id.info);

  }

  public void setSkuListValue(List<SkuItem> skuItems,int position,SkuItem skuItem, byProjectInterface listener) {
    skuName.setText(skuItem.getName());
    price2.setText(skuItem.getBOQ()+" "+skuItem.getUnit());
    price.setText(skuItem.getPrice());
    if(!skuItem.getIspromotion().equalsIgnoreCase("0")){
      commLayout.setVisibility(View.VISIBLE);
      commTxt.setText(skuItem.getCommission());
    }else{
      commLayout.setVisibility(View.GONE);
    }
    stock.setText(RudyApplication.context.getResources().getString(R.string.txt_stock) + skuItem.getStock().replace(" ","")+""+(skuItem.getUnit().replace(" ","").replace("\n","")));

    if (String.valueOf(skuItem.getFav()).equalsIgnoreCase("1")) {
      addFav.setImageResource(R.drawable.ic_favorite_pink_48dp);
    } else {
      addFav.setImageResource(R.drawable.ic_favorite_border_pink_48dp);
    }

//    HOT
    if(String.valueOf(skuItem.getHot()).equalsIgnoreCase("1")){
      hotDeal.setVisibility(View.VISIBLE);
    }else{
      hotDeal.setVisibility(View.GONE);
    }
//    USE2USE

    if(String.valueOf(skuItem.getUsed2use()).equalsIgnoreCase("1")){
      favorite.setVisibility(View.VISIBLE);
    }else{
      favorite.setVisibility(View.GONE);
    }

    if(String.valueOf(skuItem.getRecommend()).equalsIgnoreCase("1")){
      recomend.setVisibility(View.VISIBLE);
    }else{
      recomend.setVisibility(View.GONE);
    }


    if(String.valueOf(skuItem.getIspromotion()).equalsIgnoreCase("1")){
      remain.setVisibility(View.VISIBLE);
      remain_price.setText(RudyApplication.context.getResources().getString(R.string.promotion_left) + skuItem.getPromotionRemainQty()+(skuItem.getUnit().replace(" ","").replace("\n","")));
      remain_date.setText(skuItem.getPromotionRemainDay() + RudyApplication.context.getResources().getString(R.string.day));
      promo_ico.setVisibility(View.VISIBLE);
    }else{
      remain.setVisibility(View.INVISIBLE);
      promo_ico.setVisibility(View.GONE);
    }

    if(!skuItem.getPic().isEmpty()){
      Glide.with(context)
              .load(skuItem.getPic())
              .into(thumbnail);
    }

    addFav.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(String.valueOf(skuItem.getFav()).equalsIgnoreCase("1")){
          skuItem.setFav(0);
          listener.onClickFavCate(2,false,skuItem.getId());

        }else{
          skuItem.setFav(1);
          listener.onClickFavCate(2,true,skuItem.getId());
        }
      }
    });

    addCart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onClickAddtoCart(skuItem);
      }
    });

    if((skuItems.size()-1)==position){
      all_data.setVisibility(View.VISIBLE);
    }else{
      all_data.setVisibility(View.GONE);
    }
    all_data.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.clickClass(skuItem.getClassId(),skuItem.getName());

      }
    });

    info.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onClickInfo(skuItem);
      }
    });




//    childTextView.setText(name);
  }
}
