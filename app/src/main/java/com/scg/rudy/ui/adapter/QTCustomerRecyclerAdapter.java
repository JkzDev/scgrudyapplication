package com.scg.rudy.ui.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.customer_detail.ProjectListItem;
import com.scg.rudy.model.pojo.customer_detail.TransactionListItem;
import com.scg.rudy.ui.print_qo.PrintQoActivity;
import com.scg.rudy.utils.Utils;

import java.util.List;

import static com.scg.rudy.utils.Utils.savePrefer;

/**
 * Created by DekDroidDev on 9/2/2018 AD.
 */

public class QTCustomerRecyclerAdapter extends RecyclerView.Adapter<QTCustomerRecyclerAdapter.ProjectViewHolder> {

    private  List<TransactionListItem> qtListModelArrayList ;
    List<ProjectListItem> _projectListItems;
    private Context context;
    public QTCustomerRecyclerAdapter(Context context,  List<TransactionListItem>  qtListModelArrayList,List<ProjectListItem> projectListItems) {
        this.qtListModelArrayList = qtListModelArrayList;
        this.context = context;
        this._projectListItems = projectListItems;
        for (int i = 0; i < qtListModelArrayList.size(); i++) {
            this.qtListModelArrayList.remove(qtListModelArrayList.get(i).getStatusTxt().toLowerCase().equalsIgnoreCase("pr"));
        }

    }

    @Override
    public QTCustomerRecyclerAdapter.ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_pr_view, parent, false);
        return new QTCustomerRecyclerAdapter.ProjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QTCustomerRecyclerAdapter.ProjectViewHolder holder, int position) {
        final TransactionListItem transitionListItem = qtListModelArrayList.get(position);
//        final ProjectListItem projectList = _projectListItems.get(position);
//        ,transitionListItem.getId()
        String date[] = transitionListItem.getLastUpdate().split(" ");
//@color/color_next_btn
        holder.dateText.setText( date[0]);
        holder.prcode.setText(transitionListItem.getStatusTxt());
        for (int i = 0; i < _projectListItems.size(); i++) {
            if(transitionListItem.getProjectId().equalsIgnoreCase(_projectListItems.get(i).getId())){
                holder.nameTxt.setText(_projectListItems.get(i).getProjectName());
            }
        }



//        holder.netAmount.setText(Utils.moneyFormat(transitionListItem.getOrderNumber()));
        holder.netAmount.setText(transitionListItem.getOrderNumber());

//        if(transitionListItem.getStatusTxt().toLowerCase().equalsIgnoreCase("pr")){
//            holder.prcode.setTextColor(context.getResources().getColor(R.color.txt_tab2));
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent =new Intent( context,TransactionDetailActivity.class);
//                    intent.putExtra("transaction_id",transitionListItem.getTransactionId());
//                    context.startActivity(intent);
//                }
//            });
//        }else{
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePrefer(context,"print","");
                    Intent intent =new Intent( context,PrintQoActivity.class);
                    PrintQoActivity.fromCart = false;
//                    intent.putExtra("url","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+transitionListItem.getId()+"?act=qv2");
//                    intent.putExtra("urlOpen","https://api.merudy.com/app/merudy/MTIzcXdlMTIz6969/th/transaction/"+transitionListItem.getId()+"?act=qv");
                    intent.putExtra("transaction_id",transitionListItem.getId());
                    Utils.pdfName = transitionListItem.getProjectName();
                    context.startActivity(intent);
                }
            });
//        }


//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        if (qtListModelArrayList == null)
            return 0;
        return qtListModelArrayList.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder {
        TextView dateText;
        TextView prcode ;
        TextView nameTxt;
        TextView netAmount;
        CardView cardPhase;
        public ProjectViewHolder(View view) {
            super(view);
            dateText = itemView.findViewById(R.id.dateText);
            prcode = itemView.findViewById(R.id.prcode);
            nameTxt = itemView.findViewById(R.id.nameTxt);
            netAmount = itemView.findViewById(R.id.netAmount);
            cardPhase = itemView.findViewById(R.id.card_phase);
//            title = (TextView) view.findViewById(R.id.title);
//            genre = (TextView) view.findViewById(R.id.genre);
//            year = (TextView) view.findViewById(R.id.year);
        }
    }


}