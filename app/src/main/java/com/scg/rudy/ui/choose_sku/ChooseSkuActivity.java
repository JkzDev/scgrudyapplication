package com.scg.rudy.ui.choose_sku;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.choose_sku.ChooseSku;
import com.scg.rudy.model.pojo.choose_sku.ItemsItem;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.onShowPromotion;
import static com.scg.rudy.utils.Utils.project_id;
import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class ChooseSkuActivity extends BaseActivity
        implements View.OnClickListener, ChooseSkuInterface {
    private LayoutInflater layoutInflater;
    private ImageView clsBtn;
    private Bundle extras;
    private String class_id = "";
    public static String projectId = "";
    public static String projectName = "";
    public static int addItem = 0;

    private SkuPaginationAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView rv;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to
    // modify.
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;
    private RudyService rudyService;
    private String subClass;

    private final String USE = "used2use";
    private final String POP = "popularity";
    private final String REC = "recommend";

    private LinearLayout mostUse;
    private ImageView icoMostuse;
    private TextView txtMostuse;
    private LinearLayout topSell;
    private ImageView icoTopsale;
    private TextView txtTopsale;
    private LinearLayout recomment;
    private ImageView icoRecc;
    private TextView txtRecc;
    private CardView shortHL;
    private TextView txtHL;
    private String shortBy = ApiEndPoint.SEARCH_NOR;
    private final int shortByUse = 1, shortBySale = 2, shortByRecomd = 3;
    private int shortType = 2;
    public String phase_id;
    private boolean isUse = false, isBest = false, isRecm = false;
    private ArrayList<String> filterList;
    private String multifilter = "";
    private ImageView searchSku;
    private TextView countText;
    private RelativeLayout reCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_sku);
        ButterKnife.bind(this);
        rudyService = ApiHelper.getClient();
        initView();
        setEnviroment();

        findViewById(R.id.bhome)
                .setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intents =
                                        new Intent(ChooseSkuActivity.this, MainActivity.class);
                                intents.addFlags(
                                        Intent.FLAG_ACTIVITY_NEW_TASK
                                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intents);
                                finish();
                            }
                        });

        extras = getIntent().getExtras();
        if (extras != null) {
            class_id = extras.getString("class_id");
            projectId = extras.getString("projectId");
            subClass = extras.getString("subClass");
            phase_id = extras.getString("phase_id");

        } else {
            class_id = "341";
            projectId = "1399";
            subClass = "TestData";
            phase_id = "TestData";
        }

        clsBtn.setOnClickListener(this);
        shortHL.setOnClickListener(this);
        mostUse.setOnClickListener(this);
        topSell.setOnClickListener(this);
        recomment.setOnClickListener(this);
        searchSku.setOnClickListener(this);
        if (phase_id == null) {
            phase_id = "";
        }

        adapter = new SkuPaginationAdapter(this, this, subClass, phase_id);

        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);
        rv.addOnScrollListener(
                new PaginationScrollListener(linearLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;
                        loadNextPage();
                    }

                    @Override
                    public int getTotalPageCount() {
                        return TOTAL_PAGES;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });

        countText.setText("0 " + getResources().getString(R.string.list));
        loadFirstPage();
    }

    @Override
    public void setEnviroment() {
        UserPOJO userPOJO = Utils.setEnviroment(this);
        user_id = userPOJO.getItems().getId();
        shopID = userPOJO.getItems().getShopId();
    }

    private Call<String> callAddFav(String _productId) {
        return rudyService.addFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _productId,
                user_id

        );
    }

    @SuppressLint("StaticFieldLeak")
    public void getAddFav(String _productId){
        showLoading(ChooseSkuActivity.this);
        callAddFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(ChooseSkuActivity.this, getResources().getString(R.string.toast_add_producto_to_favorite_success));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ChooseSkuActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ChooseSkuActivity.this, t.getMessage());
            }
        });
    }

    private Call<String> callRemoveFav(String _productId) {
        return rudyService.removeFAV(
                Utils.APP_LANGUAGE,
                project_id,
                _productId
        );
    }

    @SuppressLint("StaticFieldLeak")
    public void getRemoveFav(String _productId){
        showLoading(ChooseSkuActivity.this);
        callRemoveFav(_productId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    showToast(ChooseSkuActivity.this, getResources().getString(R.string.dialog_delete_finish));
                }else{
                    APIError error  = gson.fromJson(resp, APIError.class);
                    showToast(ChooseSkuActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                showToast(ChooseSkuActivity.this, t.getMessage());
            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    public void getData(final String url) {
//        showLoading(this);
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = Utils.getData(url);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
//                hideLoading();
//                if (string.contains("200")) {
//                    //
//                    // chooseSkuPresenter.callSkuList("th",shopID,"byClass",projectId,class_id,String.valueOf(currentPage),"popularity");
//                }
//            }
//        }.execute();
//    }

    private List<ItemsItem> fetchResults(Response<ChooseSku> response) {
        TOTAL_PAGES = 1;
        ChooseSku skuItem = response.body();
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem.getItems();
    }

    private List<ItemsItem> customFetchResults(ChooseSku skuItem) {
        TOTAL_PAGES = 1;
        TOTAL_PAGES = skuItem.getTotalPages();
        return skuItem.getItems();
    }

    private void loadNextPage() {
        Log.d("", "loadNextPage: " + currentPage);
        Log.d("TotalPage", "TotalPage is : " + TOTAL_PAGES);
        callSearchApi()
                .enqueue(
                        new Callback<String>() {
                            @Override
                            public void onResponse(
                                    Call<String> call, Response<String> response) {
                                adapter.removeLoadingFooter();
                                isLoading = false;
                                Gson gson = new Gson();
                                String resp = response.body();
                                if (resp.contains("200")) {
                                    ChooseSku obj = gson.fromJson(resp, ChooseSku.class);
                                    List<ItemsItem> results = customFetchResults(obj);
                                    adapter.addAll(results, "");
                                    if (currentPage < TOTAL_PAGES) {
                                        adapter.addLoadingFooter();
                                    } else {
                                        isLastPage = true;
                                    }
                                } else {
                                    APIError error = gson.fromJson(resp, APIError.class);
                                    showToast(ChooseSkuActivity.this, error.getItems());
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                t.printStackTrace();
                                adapter.showRetry(true, fetchErrorMessage(t));
                            }
                        });
    }

    private void loadFirstPage() {
        showLoading(this);
        hideErrorView();
        callSearchApi()
                .enqueue(
                        new Callback<String>() {
                            @Override
                            public void onResponse(
                                    Call<String> call, @NonNull Response<String> response) {
                                hideLoading();
                                hideErrorView();
                                Gson gson = new Gson();
                                String resp = response.body();
                                if (resp.contains("200")) {
                                    ChooseSku obj = gson.fromJson(resp, ChooseSku.class);
                                    countText.setText(obj.getTotal() + " " + getResources().getString(R.string.list));
                                    List<ItemsItem> results = customFetchResults(obj);
                                    if (results.size() > 0) {
                                        errorLayout.setVisibility(View.GONE);
                                        adapter.addAll(results, "");
                                        if (currentPage < TOTAL_PAGES) {
                                            adapter.addLoadingFooter();
                                        } else {
                                            isLastPage = true;
                                        }
                                    } else {
                                        errorLayout.setVisibility(View.VISIBLE);
                                        countText.setText("0 " + getResources().getString(R.string.list));
                                    }
                                } else {
                                    APIError error = gson.fromJson(resp, APIError.class);
                                    showToast(ChooseSkuActivity.this, error.getItems());
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<String> call, Throwable t) {
                                hideLoading();
                                t.printStackTrace();
                                showErrorView(t);
                            }
                        });
    }

    private Call<String> callSearchApi() {
        return rudyService.callSkuList(
                Utils.APP_LANGUAGE,
                shopID,
                "byClass_v2",
                user_id,
                projectId,
                class_id,
                String.valueOf(currentPage),
                shortBy,
                phase_id,
                multifilter);
        //        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ChooseSkuActivity.addItem == 1) {
            finish();
            ChooseSkuActivity.addItem = 0;
        }
    }

    private void initView() {
        layoutInflater = LayoutInflater.from(getBaseContext());
        filterList = new ArrayList<>();
        clsBtn = findViewById(R.id.cls_btn);
        rv = findViewById(R.id.select_sku_recycler);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);

        mostUse = findViewById(R.id.most_use);
        icoMostuse = findViewById(R.id.ico_mostuse);
        txtMostuse = findViewById(R.id.txt_mostuse);
        topSell = findViewById(R.id.top_sell);
        icoTopsale = findViewById(R.id.ico_topsale);
        txtTopsale = findViewById(R.id.txt_topsale);
        recomment = findViewById(R.id.recomment);
        icoRecc = findViewById(R.id.ico_recc);
        txtRecc = findViewById(R.id.txt_recc);
        shortHL = findViewById(R.id.shortHL);
        txtHL = findViewById(R.id.txtHL);
        searchSku = (ImageView) findViewById(R.id.search_sku);
        countText = (TextView) findViewById(R.id.countText);
        reCount = (RelativeLayout) findViewById(R.id.reCount);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }

        if (v == mostUse) {
            shortType = shortByUse;
            shortItems(v);
        }

        if (v == topSell) {
            shortType = shortBySale;
            shortItems(v);
        }

        if (v == recomment) {
            shortType = shortByRecomd;
            shortItems(v);
        }

        if (v == shortHL) {
            final PopupWindow popup = new PopupWindow(this);
            View layout = LayoutInflater.from(this).inflate(R.layout.popup_short, null);
            popup.setContentView(layout);
            LinearLayout L2H = layout.findViewById(R.id.LH);
            LinearLayout H2L = layout.findViewById(R.id.HL);
            LinearLayout NM = layout.findViewById(R.id.NM);

            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setOutsideTouchable(true);
            popup.setFocusable(true);
            popup.setBackgroundDrawable(new BitmapDrawable());

            NM.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            shortBy = ApiEndPoint.SEARCH_NOR;
                            txtHL.setText(getResources().getString(R.string.txt_nm));
                            adapter.clear();
                            isLastPage = false;
                            currentPage = 1;
                            loadFirstPage();
                            popup.dismiss();
                        }
                    });

            L2H.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            shortBy = ApiEndPoint.SEARCH_HTL;
                            txtHL.setText(getResources().getString(R.string.txt_hl));
                            adapter.clear();
                            isLastPage = false;
                            currentPage = 1;
                            loadFirstPage();
                            popup.dismiss();
                        }
                    });

            H2L.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            shortBy = ApiEndPoint.SEARCH_LTH;
                            txtHL.setText(getResources().getString(R.string.txt_lh));
                            adapter.clear();
                            isLastPage = false;
                            currentPage = 1;
                            loadFirstPage();
                            popup.dismiss();
                        }
                    });

            popup.showAsDropDown(shortHL, 10, -10, Gravity.LEFT);
        }

        if (v == searchSku) {
            Intent intent = new Intent(this, SearchSKUitemsActivity.class);
            intent.putExtra("shopId", shopID);
            startActivity(intent);
        }
    }

    private void shortItems(View v) {
        if (v == mostUse) {
            if (isUse) {
                mostUse.setBackground(getResources().getDrawable(R.drawable.bg_use_stoke));
                icoMostuse.setImageDrawable(getResources().getDrawable(R.drawable.used));
                txtMostuse.setTextColor(getResources().getColor(R.color.short_use));
                filterList.remove(USE);

            } else {
                mostUse.setBackground(getResources().getDrawable(R.drawable.bg_use_fill));
                icoMostuse.setImageDrawable(getResources().getDrawable(R.drawable.used_white));
                txtMostuse.setTextColor(getResources().getColor(R.color.white));
                filterList.add(USE);
            }
            isUse = !isUse;
        }

        if (v == topSell) {
            if (isBest) {
                topSell.setBackground(getResources().getDrawable(R.drawable.bg_sell_stoke));
                icoTopsale.setImageDrawable(getResources().getDrawable(R.drawable.filled_star));
                txtTopsale.setTextColor(getResources().getColor(R.color.short_best));
                filterList.remove(POP);

            } else {
                topSell.setBackground(getResources().getDrawable(R.drawable.bg_sell_fill));
                icoTopsale.setImageDrawable(
                        getResources().getDrawable(R.drawable.filled_star_white));
                txtTopsale.setTextColor(getResources().getColor(R.color.white));
                filterList.add(POP);
            }
            isBest = !isBest;
        }

        if (v == recomment) {
            if (isRecm) {
                recomment.setBackground(getResources().getDrawable(R.drawable.bg_recoment_stoke));
                icoRecc.setImageDrawable(getResources().getDrawable(R.drawable.like_it_blue));
                txtRecc.setTextColor(getResources().getColor(R.color.short_recco));
                filterList.remove(REC);
            } else {
                recomment.setBackground(getResources().getDrawable(R.drawable.bg_recoment_fill));
                icoRecc.setImageDrawable(getResources().getDrawable(R.drawable.like_it_white));
                txtRecc.setTextColor(getResources().getColor(R.color.white));
                filterList.add(REC);
            }
            isRecm = !isRecm;
        }
        multifilter = filterList.toString().replace("[", "").replace("]", "").replace(" ", "");
        //        showToast(this,multifilter);
        isLastPage = false;
        adapter.clear();
        currentPage = 1;
        loadFirstPage();
    }

    public void onFragmentDetached(String tag) {
    }

    public void onError(int resId) {
    }

    public void onError(String message) {
        adapter.showRetry(true, message);
    }

    public void showMessage(String message) {
        showToast(this, message);
    }

    public void showMessage(int resId) {
    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {
    }

    @Override
    public void showData(ChooseSku chooseSku) {
        if (chooseSku.getItems().size() > 0) {
            if (currentPage < TOTAL_PAGES) {
                adapter.addLoadingFooter();
            } else {
                isLastPage = true;
            }

            TOTAL_PAGES = 1;
            if (chooseSku.getTotal() != null) {
                if (chooseSku.getItems().size() > 0) {
                    TOTAL_PAGES =
                            Math.round(
                                    Integer.parseInt(chooseSku.getTotal())
                                            / chooseSku.getItems().size());
                }
            }

            adapter.addAll(chooseSku.getItems(), "");
        }
    }

    @Override
    public void showErrorIsNull() {
    }

    @Override
    public void BodyError(ResponseBody responseBodyError) {
    }

    @Override
    public void Failure(Throwable t) {
    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }

    @Override
    public void addFAV(String skuId) {
        //        adapter.clear();
        //        currentPage =1;
        getAddFav(skuId);
    }

    @Override
    public void removeFAV(String skuId) {
        //        adapter.clear();
        //        currentPage =1;
        getRemoveFav(skuId);
    }

    @Override
    public void onClickinfo(ItemsItem items) {
//        showToast(this,items.getName());
        PromotionDetail(items.getPromotionDetailId());
    }


    private void PromotionDetail(String promotionDetailId) {
        Log.d("", "loadFirstPage: ");
        showLoading(this);
        hideErrorView();
        callPromotionDetailApi(promotionDetailId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    PromotionDetail promotionDetail = gson.fromJson(resp, PromotionDetail.class);
                    onShowPromotion(ChooseSkuActivity.this, promotionDetail);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(ChooseSkuActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(ChooseSkuActivity.this, t.getMessage());
            }
        });
    }


    private Call<String> callPromotionDetailApi(String promotionDetailId) {
        return rudyService.getPromotionDetail(
                Utils.APP_LANGUAGE,
                shopID,
                promotionDetailId
        );
    }


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }
}
