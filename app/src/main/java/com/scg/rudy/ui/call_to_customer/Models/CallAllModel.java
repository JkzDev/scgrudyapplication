
package com.scg.rudy.ui.call_to_customer.Models;

import android.graphics.drawable.Drawable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CallAllModel {

    @SerializedName("Company")
    private String mCompany;
    @SerializedName("Image")
    private Drawable mImage;
    @SerializedName("Name")
    private String mName;

    public CallAllModel(Drawable _image, String _name, String _company){
        mImage = _image;
        mName = _name;
        mCompany = _company;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public Drawable getImage() {
        return mImage;
    }

    public void setImage(Drawable image) {
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
