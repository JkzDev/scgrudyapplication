package com.scg.rudy.ui.department_data_child;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.base.BaseActivity;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.CategorieModel;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.model.pojo.contractType.ContractTypePojo;
import com.scg.rudy.model.pojo.project_group_detail.ProjectGroupDetail;
import com.scg.rudy.model.pojo.projectgroup.PhasesItem;
import com.scg.rudy.model.pojo.projectgroup.ProjectGroupModel;
import com.scg.rudy.model.pojo.projectgroup.ProjectGroupPojo;
import com.scg.rudy.ui.add_group_project.AddNewGroupProjectActivity;
import com.scg.rudy.ui.department_data_group.DepartmentDataGroupActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.CustomSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;

/**
 * @author jackie
 */
public class DepartmentDataNewActivity extends BaseActivity implements View.OnClickListener {

    private ArrayList<CategorieModel> categorieModelArrayList;
    private ArrayList<CategorieModel> contractTypeArrayList;

    private ArrayList<String> nameList;
    private ArrayList<CategorieModel> phaseModelArrayList;
    private ArrayList<String> phaseList;
    private CustomSpinner customSpinner;
    private int catePosition = 0;
    private int phasePosition = 0;
    private int maxPhase = 10;
    private Spinner typeConstruc;
    private Spinner currentPhase;
    private Spinner contracttype;
    private EditText unit;
    private ImageView delUnit;
    private ImageView addUnit;
    private EditText totalfloor;
    private ImageView delfloor;
    private ImageView addfloor;
    private LinearLayout areaLayout;
    private LinearLayout select_phase;
    private EditText area;
    private ImageView delare;
    private ImageView addarea;
    private EditText price;
    private ImageView delprice;
    private ImageView addprice;
    private RelativeLayout save;
    private RelativeLayout boq_btn;
    private EditText name;
    private LinearLayout key_hide;
    private ImageView clsBtn;
    private Bundle extras;
    private ImageView clearText;
    private LinearLayout save_layout;
    public static boolean isEdit = false;
    public static String projectId = "";
    private RelativeLayout typeLayout;
    private RelativeLayout phaseLayout;
    private int first = 0;
    private EditText datetimeStart, datetimeEnd;
    private Button addImages;
    private ImageView imageViewSelect1, imageViewSelect2;
    private String shopID = "";
    private RudyService rudyService;
    private String group_id = "";
    private EditText mDevName;
    private int ctype = 1;
    private String user_id;//* : User who add the project
    private String lat_lng = "";//: project lat and lng [13.729194,100.622218]
    private String project_address = "";//: project location address
    private String project_name = "";//: Project name [API will create if you did not post it]
    private String project_type_id = "1";//: project type id [API will create if you did not post it  ;//: 1]
    private String phase_id = "1";//: phase id [API will create if you did not post it  ;//: 1]
    private String units = "1";//: How many unit [API will create if you did not post it  ;//: 1]
    private String unit_area = "150";//: unit area [Square meter] [API will create if you did not post it  ;//: 100]
    private String unit_budget = "2.5";//: budget [million Baht] [API will create if you did not post it  ;//: 3]
    private String phaseId = "1";
    private String cateId = "1";
    private String unit_number = "1";
    private String floor_number = "2";
    private String project_stories = "";//: story [API will create if you did not post it  ;//: 2]
    private String customer_name = "";//: Customer’s name
    private String customer_phone = "";//: Customer’s phone
    private String customer_email = "";//: Customer’s email
    private String customer_line = "";//: Customer’s line
    private String customer_tax_no = "";//: Customer’s tax no or ID card no.
    private String customer_note = "";//: Customer’s note
    private String house_owner_name = "";//: House owner name
    private String house_owner_phone = "";//: house_owner_phone
    private String house_owner_line = "";//: House owner line id
    private String House_owner_note = "";//: House owner note
    private String project_owner_name = "";//: Project owner name
    private String project_owner_phone = "";//: project_owner_phone
    private String project_owner_line = "";//: Project owner line
    private String project_owner_note = "";//: Project owner note
    private String customer_code = ""; //: customer_code
    private String champ_customer_code = ""; //: champ_customer_code
    private String pic = "";//: array post with base64
    private String projectGroupName = "";
    private String devName = "";
    private String project_group_list_id = "";
    private int project_group_position = 0;

    private String developer_id = "";
    private String timeStart = "";
    private String timeEnd = "";
    private String addNew = "";
    private String customer_company = "";
    private String c_start_date;
    private String c_end_date;
    private TextView textCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_data_new);

        initView();

        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());

        extras = getIntent().getExtras();
//        name = "DEbug ja";
//        projectId = "1036";
        project_name = date;
        if (extras != null) {
            group_id = extras.getString("group_id", "");
            unit_number = extras.getString("unit_number", "1");
            project_stories = extras.getString("floor_number", "2");
            unit_area = extras.getString("unit_area", "150");
            unit_budget = extras.getString("unit_budget", "2.5");
            project_name = extras.getString("project_name", "");
            cateId = extras.getString("cateId", "1");
            phaseId = extras.getString("phaseId", "1");
            catePosition = Integer.parseInt(extras.getString("catePosition", "0"));
            phasePosition = Integer.parseInt(extras.getString("phasePosition", "0"));
            projectGroupName = extras.getString("projectGroupName", "");
            devName = extras.getString("devName", "");
            project_group_list_id = extras.getString("project_group_list_id", "1");
            project_group_position = Integer.parseInt(extras.getString("project_group_list_id", "0"));
            developer_id = extras.getString("developer_id", "");
            timeStart = extras.getString("timeStart", "");
            timeEnd = extras.getString("timeEnd", "");
            addNew = extras.getString("addNew", "");
            customer_company = extras.getString("customer_company", "");


            customer_name = extras.getString("customer_name", "");
            customer_phone = extras.getString("customer_phone", "");
            customer_line = extras.getString("customer_line", "");
            customer_note = extras.getString("customer_note", "");
            ctype = Integer.parseInt(extras.getString("ctype", "1"));
            customer_tax_no = extras.getString("tax_number", "");
            champ_customer_code = extras.getString("champ_customer_code", "");
            customer_company = extras.getString("customer_company", "");
            customer_code = extras.getString("customer_code");

        }
        getUserData();
//        showToast(this,project_group_position+"");
        if (project_name.length() > 0) {
            name.setText(project_name);
        } else {
            name.setText(date);
        }

        Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
        name.setTypeface(_font);

        if (unit_number.length() > 0 &&
                project_stories.length() > 0 &&
                unit_area.length() > 0 &&
                unit_budget.length() > 0) {
            unit.setText(unit_number);
            totalfloor.setText(project_stories);
            area.setText(unit_area);
            price.setText(unit_budget);
        }
//        getCategorie(ApiEndPoint.getCate("th"));

        clearText.setOnClickListener(this);
        clsBtn.setOnClickListener(this);

        key_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(DepartmentDataNewActivity.this, key_hide);
            }
        });

        save_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideKeyboardFrom(DepartmentDataNewActivity.this, save_layout);
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    clearText.setVisibility(View.VISIBLE);
                    save.setBackground(getDrawable(R.drawable.linear_blue_chkin_detail));
                } else {
                    clearText.setVisibility(View.GONE);
                    save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
                }
            }
        });

        unit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                unit_number = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        totalfloor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                project_stories = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        area.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                unit_area = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                unit_budget = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                project_name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                Typeface _font = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.bold));
                name.setTypeface(_font);
            }
        });

        addUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!unit.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(unit.getText().toString());
                }
                unit_int = unit_int + 1;
                unit.setText(String.valueOf(unit_int));
            }
        });

        delUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!unit.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(unit.getText().toString());
                }
                if (unit_int > 0) {
                    unit_int = unit_int - 1;
                    if (unit_int <= 0) {
                        unit_int = 1;
                    }
                    unit.setText(String.valueOf(unit_int));
                }

            }
        });

        addfloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;

                if (!totalfloor.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(totalfloor.getText().toString());
                }
                unit_int = unit_int + 1;
                totalfloor.setText(String.valueOf(unit_int));
            }
        });

        delfloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;

                if (!totalfloor.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(totalfloor.getText().toString());
                }

                if (unit_int > 1) {
                    unit_int = unit_int - 1;
                    totalfloor.setText(String.valueOf(unit_int));
                }
            }
        });

        addarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!area.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(area.getText().toString());
                }
                unit_int = unit_int + 50;
                area.setText(String.valueOf(unit_int));
            }
        });

        delare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!area.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(area.getText().toString());
                }

                if (unit_int > 0) {
                    unit_int = unit_int - 50;
                    if (unit_int <= 0) {
                        unit_int = 1;
                    }
                    area.setText(String.valueOf(unit_int));
                }
            }
        });

        addprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!price.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(price.getText().toString());
                }

                unit_int = unit_int + .5;
                price.setText(String.valueOf(unit_int));
            }
        });

        delprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double unit_int = 0;
                if (!price.getText().toString().equalsIgnoreCase("")) {
                    unit_int = Double.parseDouble(price.getText().toString());
                }

                if (unit_int != 0) {
                    unit_int = unit_int - .5;
                    price.setText(String.valueOf(unit_int));
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(DepartmentDataNewActivity.this);
                unit_number = unit.getText().toString();
                project_stories = totalfloor.getText().toString();
                unit_area = area.getText().toString();
                unit_budget = price.getText().toString();
                project_name = name.getText().toString();
                c_start_date = datetimeStart.getText().toString();
                c_end_date = datetimeEnd.getText().toString();

                if (project_name.length() > 0) {
                    if (addNew.length() > 0) {
                        String gId = AddNewGroupProjectActivity.group_id;
                        if (gId.length() == 0) {
                            gId = group_id;
                        }
                        if (gId.length() == 0) {
                            postNewGroupProject(ApiEndPoint.getNewGroupAdd(Utils.APP_LANGUAGE));
                        } else {
                            postNewProject(ApiEndPoint.getNewAdd(Utils.APP_LANGUAGE));
                        }
                    } else {
                        if (AddNewGroupProjectActivity.group_id.length() == 0) {
                            String gId = group_id;
                            if (gId.length() == 0) {
                                postNewGroupProject(ApiEndPoint.getNewGroupAdd(Utils.APP_LANGUAGE));
                            } else {
                                postNewProject(ApiEndPoint.getNewAdd(Utils.APP_LANGUAGE));
                            }
                        } else {
                            updateDepartment(ApiEndPoint.getUpdate_depart(Utils.APP_LANGUAGE, Utils.project_id));
                        }
                    }

                    if (Utils.project_id.length() > 0) {
                        AddNewGroupProjectActivity.units = unit_number;
                        AddNewGroupProjectActivity.project_stories = project_stories;
                        AddNewGroupProjectActivity.unit_area = unit_area;
                        AddNewGroupProjectActivity.unit_budget = unit_budget;
                        AddNewGroupProjectActivity.project_name = project_name;
                        AddNewGroupProjectActivity.cateId = cateId;
                        AddNewGroupProjectActivity.phase_id = phaseId;
                        AddNewGroupProjectActivity.project_group_position = catePosition;
                        AddNewGroupProjectActivity.phasePosition = phasePosition;
                        AddNewGroupProjectActivity.maxPhase = maxPhase;
                        MainActivity.gdata3 = true;
                    } else {
                        AddNewGroupProjectActivity.units = unit_number;
                        AddNewGroupProjectActivity.project_stories = project_stories;
                        AddNewGroupProjectActivity.unit_area = unit_area;
                        AddNewGroupProjectActivity.unit_budget = unit_budget;
                        AddNewGroupProjectActivity.project_name = project_name;
                        AddNewGroupProjectActivity.cateId = cateId;
                        AddNewGroupProjectActivity.phase_id = phaseId;
                        AddNewGroupProjectActivity.project_group_position = catePosition;
                        AddNewGroupProjectActivity.phasePosition = phasePosition;
                        AddNewGroupProjectActivity.maxPhase = maxPhase;
                        MainActivity.gdata3 = true;
                        AddNewGroupProjectActivity.step3 = true;

                    }
                } else {
                    Toast.makeText(DepartmentDataNewActivity.this, getResources().getString(R.string.toast_pls_fill_name_unit), Toast.LENGTH_LONG).show();
                }
            }
        });

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        datetimeStart.setText(formattedDate);
        datetimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.SelectDateDialogs(DepartmentDataNewActivity.this, datetimeStart);
            }
        });

        datetimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.SelectDateDialogs(DepartmentDataNewActivity.this, datetimeEnd);
            }
        });

        Utils.hideKeyboard(this);
        textCurrency.setText(getResources().getString(R.string.value_work_per_building_million_bath) + " (" + getResources().getString(R.string.million) + Utils.currencyForShow + ")");
    }

    @Override
    public void onResume() {
        super.onResume();
        callProjectGroup();

    }


    @SuppressLint("StaticFieldLeak")
    private void updateDepartment(final String url) {
        showLoading(this);
        String gId = AddNewGroupProjectActivity.group_id;
        if (gId.length() == 0) {
            gId = group_id;
        }
        final RequestBody requestBody = new FormBody.Builder()
                .add("project_name", name.getText().toString())
                .add("phase_id", phaseId)
                .add("units", unit.getText().toString())
                .add("unit_area", area.getText().toString())
                .add("unit_budget", price.getText().toString())
                .add("project_stories", totalfloor.getText().toString())

                .add("user_id", user_id)
                .add("project_name", project_name)
                .add("project_address", project_address)
                .add("lat_lng", lat_lng)
                .add("project_type_id", project_type_id)
                .add("phase_id", phase_id)
                .add("units", unit_number)
                .add("unit_area", unit_area)
                .add("unit_budget", unit_budget)
                .add("project_stories", project_stories)
                .add("customer_type", ctype + "")
                .add("customer_name", customer_name)
                .add("customer_phone", customer_phone)
                .add("customer_email", customer_email)
                .add("customer_line", customer_line)
                .add("customer_tax_no", customer_tax_no)
                .add("customer_note", customer_note)
                .add("house_owner_name", house_owner_name)
                .add("house_owner_phone", house_owner_phone)
                .add("house_owner_line", house_owner_line)
                .add("House_owner_note", House_owner_note)
                .add("Project_owner_name", project_owner_name)
                .add("Project_owner_phone", project_owner_phone)
                .add("Project_owner_line", project_owner_line)
                .add("Project_owner_note", project_owner_note)
                .add("pic", pic)
                .add("start_date", c_start_date)
                .add("end_date", c_end_date)
                .add("project_group_id", gId)
                .build();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                String response = "error";
                response = Utils.postData(url, requestBody);
                return response;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                hideLoading();

                if (string.contains("200")) {
                    Gson gson = new Gson();
                    AddNewGroupProjectActivity.projectDetail = gson.fromJson(string, ProjectGroupDetail.class);
                    showToast(DepartmentDataNewActivity.this, getResources().getString(R.string.toast_update_data_success));
                    finish();
                }
            }
        }.execute();
    }


    @SuppressLint("StaticFieldLeak")
    private void postNewGroupProject(final String url) {
        showLoading(this);
        final RequestBody requestBody;
        final FormBody.Builder formData = new FormBody.Builder();
        try {
            for (int i = 0; i < AddNewGroupProjectActivity.mArrayList.size(); i++) {
                String data = URLDecoder.decode("pic[" + i + "]", "UTF-8");
                formData.add(data, "data:image/jpg;base64," + Utils.BitMapToString(AddNewGroupProjectActivity.mArrayList.get(i)));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        formData.add("user_id", user_id)
                .add("name", projectGroupName)
                .add("lat_lng", lat_lng)
                .add("project_address", project_address)
                .add("project_group_list_id", project_group_list_id)
                .add("developer_id", developer_id)
                .add("developer_name_other", devName)
                .add("start_date", timeStart)
                .add("end_date", timeEnd)
                .add("customer_type", ctype + "")
                .add("customer_name", customer_name)
                .add("customer_code", customer_code)
                .add("champ_customer_code", champ_customer_code)
                .add("customer_company", customer_company)
                .add("customer_phone", customer_phone)
                .add("customer_email", customer_email)
                .add("customer_line", customer_line)
                .add("customer_tax_no", customer_tax_no)
                .add("customer_note", customer_note);

        requestBody = formData.build();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                return Utils.postData(url, requestBody);
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                hideLoading();
                if (string.contains("200")) {
                    Gson gson = new Gson();
                    ProjectGroupDetail projectDetail = gson.fromJson(string, ProjectGroupDetail.class);
                    AddNewGroupProjectActivity.group_id = projectDetail.getItems().getId();
                    DepartmentDataGroupActivity.group_id = AddNewGroupProjectActivity.group_id;

                    postNewProject(ApiEndPoint.getNewAdd(Utils.APP_LANGUAGE));

                } else {

                    showToast(DepartmentDataNewActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
                    finish();

                }

            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private void postNewProject(final String url) {
        showLoading(this);
        String gId = AddNewGroupProjectActivity.group_id;
        if (gId.length() == 0) {
            gId = group_id;
        }
        Calendar now = Calendar.getInstance();
        now.get(Calendar.YEAR);
        now.get(Calendar.MONTH);
        now.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm dd/MM/yyyy", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
        String date = simpledateformat.format(now.getTime());
        String dateArr[] = date.split(" ");
        String Cdate = dateArr[1];
//        project_name = date;
//        unit_budget_number = Double.parseDouble(unit_budget);
//        unit_budget_number = unit_budget_number * 1000000;
        final RequestBody requestBody = new FormBody.Builder()
                .add("user_id", user_id)
                .add("project_name", project_name)
                .add("project_address", project_address)
                .add("lat_lng", lat_lng)
                .add("project_type_id", project_type_id)
                .add("phase_id", phase_id)
                .add("units", unit_number)
                .add("unit_area", unit_area)
                .add("unit_budget", unit_budget)
                .add("start_date", c_start_date)
                .add("end_date", c_end_date)
                .add("project_stories", project_stories)
                .add("customer_type", ctype + "")
                .add("customer_name", customer_name)
                .add("customer_phone", customer_phone)
                .add("customer_email", customer_email)
                .add("customer_line", customer_line)
                .add("customer_tax_no", customer_tax_no)
                .add("customer_note", customer_note)
                .add("house_owner_name", house_owner_name)
                .add("house_owner_phone", house_owner_phone)
                .add("house_owner_line", house_owner_line)
                .add("House_owner_note", House_owner_note)
                .add("Project_owner_name", project_owner_name)
                .add("Project_owner_phone", project_owner_phone)
                .add("Project_owner_line", project_owner_line)
                .add("Project_owner_note", project_owner_note)
                .add("pic", pic)
                .add("project_group_id", gId)
                .build();


        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                return Utils.postData(url, requestBody);
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                hideLoading();
                Gson gson = new Gson();
//                ProjectDetail projectDetail = gson.fromJson(string, ProjectDetail.class);
                if (string.contains("200")) {
                    setCustomTag("Create Project");
                    showToast(DepartmentDataNewActivity.this, getResources().getString(R.string.toast_create_data_unit_success));
                    finish();
                } else {
                    APIError error = gson.fromJson(string, APIError.class);
                    showToast(DepartmentDataNewActivity.this, error.getItems());
                }
            }
        }.execute();
    }

    public void getUserData() {
        try {
            JSONObject object = new JSONObject(Utils.getPrefer(this, Utils.PERF_LOGIN));
            JSONObject itemObject = object.getJSONObject("items");
            user_id = itemObject.optString("id");
            shopID = itemObject.optString("shop_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        outState.putString("project_name", project_name);
//        outState.putString("unit_number", unit_number);
//        outState.putString("floor_number", floor_number);
//        outState.putString("unit_area", unit_area);
//        outState.putString("unit_budget", unit_budget);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
//            if (savedInstanceState.containsKey("unit_number")) {
//                unit_number = savedInstanceState.getString("unit_number");
//            }
//            if (savedInstanceState.containsKey("unit_number")) {
//                floor_number = savedInstanceState.getString("floor_number");
//            }
//            if (savedInstanceState.containsKey("unit_area")) {
//                unit_area = savedInstanceState.getString("unit_area");
//            }
//            if (savedInstanceState.containsKey("unit_budget")) {
//                unit_budget = savedInstanceState.getString("unit_budget");
//            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (v == clsBtn) {
            finish();
        }
        if (v == clearText) {
            name.setText("");
            clearText.setVisibility(View.GONE);
            save.setBackground(getDrawable(R.drawable.bg_rounder_main_disable));
        }
    }

    private void initView() {
        rudyService = ApiHelper.getClient();
        clearText = findViewById(R.id.clear_text);
        typeConstruc = findViewById(R.id.type_construc);
        currentPhase = findViewById(R.id.current_phase);
        unit = findViewById(R.id.unit);
        delUnit = findViewById(R.id.delUnit);
        addUnit = findViewById(R.id.addUnit);
        totalfloor = findViewById(R.id.totalfloor);
        delfloor = findViewById(R.id.delfloor);
        addfloor = findViewById(R.id.addfloor);
        areaLayout = findViewById(R.id.area_layout);
        area = findViewById(R.id.area);
        delare = findViewById(R.id.delare);
        addarea = findViewById(R.id.addarea);
        price = findViewById(R.id.price);
        delprice = findViewById(R.id.delprice);
        addprice = findViewById(R.id.addprice);
        save = findViewById(R.id.save);
        boq_btn = findViewById(R.id.boq_btn);
        name = findViewById(R.id.dev_name);
        key_hide = findViewById(R.id.key_hide);
        clsBtn = findViewById(R.id.cls_btn);
        save_layout = findViewById(R.id.save_layout);
        typeLayout = (RelativeLayout) findViewById(R.id.type_layout);
        phaseLayout = (RelativeLayout) findViewById(R.id.phase_layout);
        select_phase = findViewById(R.id.select_phase);
        datetimeStart = findViewById(R.id.datetimeStart);
        datetimeEnd = findViewById(R.id.datetimeEnd);
        addImages = findViewById(R.id.addImages);
        imageViewSelect1 = findViewById(R.id.imageViewSelect1);
        imageViewSelect2 = findViewById(R.id.imageViewSelect2);
        contracttype = findViewById(R.id.contracttype);
        mDevName = (EditText) findViewById(R.id.dev_name);
        textCurrency = (TextView) findViewById(R.id.textCurrency);
    }

    public void onFragmentDetached(String tag) {

    }

    public void onError(int resId) {

    }

    public void onError(String message) {

    }

    public void showMessage(String message) {

    }

    public void showMessage(int resId) {

    }

    public boolean isNetworkConnected() {
        return false;
    }

    public void hideKeyboard() {

    }


    private Call<String> callProjectGroupApi() {
        return rudyService.getProjectGroup(
                Utils.APP_LANGUAGE
        );
    }

    private Call<String> callContractTypeApi() {
        return rudyService.getContractType(
                Utils.APP_LANGUAGE
        );
    }

    private void callProjectGroup() {
        showLoading(this);
        callProjectGroupApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ProjectGroupModel projectGroupPojo = gson.fromJson(resp, ProjectGroupModel.class);
                    showData(projectGroupPojo);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataNewActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DepartmentDataNewActivity.this, t.getMessage());
            }
        });
    }

    private void callContractType() {
        showLoading(this);
        callContractTypeApi().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hideLoading();
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {
                    ContractTypePojo contractTypePojo = gson.fromJson(resp, ContractTypePojo.class);
                    showDataContractType(contractTypePojo);
                } else {
                    APIError error = gson.fromJson(resp, APIError.class);
                    showToast(DepartmentDataNewActivity.this, error.getItems());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hideLoading();
                t.printStackTrace();
                showToast(DepartmentDataNewActivity.this, t.getMessage());
            }
        });
    }

    private ProjectGroupPojo fetchProjectGroupResults(Response<ProjectGroupPojo> response) {
        return response.body();
    }

    private ContractTypePojo fetchContractTypePojoResults(Response<ContractTypePojo> response) {
        return response.body();
    }

    public void showData(final ProjectGroupModel projectGroupPojo) {
        categorieModelArrayList = new ArrayList<>();
        nameList = new ArrayList<>();

        for (int i = 0; i < projectGroupPojo.items.size(); i++) {
            if (Integer.parseInt(project_group_list_id) == Integer.parseInt(projectGroupPojo.items.get(i).id)) {
                project_group_position = i;
            }
        }
        for (int i = 0; i < projectGroupPojo.items.get(project_group_position).projectType.size(); i++) {
            categorieModelArrayList.add(new CategorieModel(projectGroupPojo.items.get(project_group_position).projectType.get(i).id, projectGroupPojo.items.get(project_group_position).projectType.get(i).name));
            nameList.add(projectGroupPojo.items.get(project_group_position).projectType.get(i).name);
        }

        customSpinner = new CustomSpinner(this, nameList);
        typeConstruc.setAdapter(customSpinner);
        typeConstruc.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);


        typeConstruc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                catePosition = position;

                project_type_id = categorieModelArrayList.get(catePosition).getId();
                phase_id = projectGroupPojo.items.get(project_group_position).projectType.get(catePosition).phases.get(0).id;
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);

//                showToast(DepartmentDataNewActivity.this,
//                        "cateId :"+cateId+"\n"+
//                                "catePosition :"+catePosition+"\n"+
//                                "phaseId :"+phaseId+"\n"+
//                                "phasePosition :"+phasePosition+"\n");


                setPhaseSpinner(projectGroupPojo.items.get(project_group_position).projectType.get(catePosition).phases);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        callContractType();

    }

    public void setPhaseSpinner(List<PhasesItem> phasesItemList) {
        phaseModelArrayList = new ArrayList<>();
        phaseList = new ArrayList<>();
        for (int i = 0; i < phasesItemList.size(); i++) {
            phaseModelArrayList.add(new CategorieModel(phasesItemList.get(i).id, phasesItemList.get(i).name));
            phaseList.add(phasesItemList.get(i).name);
        }
        CustomSpinner adapterphase = new CustomSpinner(this, phaseList);
        maxPhase = phaseList.size();
        if (phaseList.size() > 0) {
            currentPhase.setAdapter(adapterphase);
        } else {
            currentPhase.setAdapter(null);
        }
        currentPhase.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        currentPhase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
                phaseId = phaseModelArrayList.get(position).getId();
                phasePosition = position;


//                showToast(DepartmentDataNewActivity.this,
//                                "cateId :"+cateId+"\n"+
//                                "catePosition :"+catePosition+"\n"+
//                                        "phaseId :"+phaseId+"\n"+
//                                "phasePosition :"+phasePosition+"\n");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void showDataContractType(ContractTypePojo contractTypePojo) {
        contractTypeArrayList = new ArrayList<>();
        nameList = new ArrayList<>();
        for (int i = 0; i < contractTypePojo.items.size(); i++) {
            contractTypeArrayList.add(new CategorieModel(contractTypePojo.items.get(i).id, contractTypePojo.items.get(i).name));
            nameList.add(contractTypePojo.items.get(i).name);
        }

        customSpinner = new CustomSpinner(this, nameList);
        contracttype.setAdapter(customSpinner);
        contracttype.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        contracttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0).findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.color_bg_location));
                parent.getChildAt(0).findViewById(R.id.line).setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
