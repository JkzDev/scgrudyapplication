package com.scg.rudy.ui.by_customers;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scg.rudy.R;
import com.scg.rudy.model.pojo.byCustomer.ItemsItem;
import com.scg.rudy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 *
 * @author jackie
 * @date 9/2/2018 AD
 */

public class ByCustomerRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemsItem> arrayList;
    private Context context;
    private int minSize;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private LayoutInflater inflater;
    private ByCustomerListener listener;
    private String searchText;

    public ByCustomerRecyclerAdapter(Context context, ByCustomerListener listener) {
        this.context = context;
        minSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                context.getResources().getDimension(R.dimen._10sdp),
                context.getResources().getDisplayMetrics());
        arrayList = new ArrayList<>();
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            View viewItem = inflater.inflate(R.layout.customer_model, parent, false);
            return new ByCustomerRecyclerAdapter.CustomerViewHolder(viewItem);

        } else if (viewType == LOADING) {
            View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
            return new ByCustomerRecyclerAdapter.LoadingVH(viewLoading);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == arrayList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return ITEM;
        }
    }


    @Override
    public void onBindViewHolder(final @NonNull RecyclerView.ViewHolder vholder, final int position) {
        final int itemPosition = getItemViewType(position);
        if (itemPosition == ITEM) {
            final ItemsItem projectModel = arrayList.get(position);
            CustomerViewHolder holder = (CustomerViewHolder) vholder;
//            holder.name.setText(projectModel.getCustomerName());

            // highlight search text
            String fullText = projectModel.getCustomerName();
            Spannable spannable = new SpannableString(fullText);
            String[] spSearchText = searchText.split(" ");
            for (int i = 0; i < spSearchText.length ; i++){
                if (spSearchText[i] != null && !spSearchText[i].isEmpty()) {
                    int startPos = fullText.toLowerCase(Locale.US).indexOf(spSearchText[i].toLowerCase(Locale.US));
                    int endPos = startPos + spSearchText[i].length();

                    if (startPos != -1) {
                        ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.RED});
                        TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);
                        spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannable.setSpan(new BackgroundColorSpan(Color.YELLOW), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        holder.name.setText(spannable);
                    } else {
                        holder.name.setText(fullText);
                    }
                } else {
                    holder.name.setText(fullText);
                }
            }




            holder.status.setText(String.format(context.getResources().getString(R.string.unit_parame), projectModel.getProcessProject()));
            if(projectModel.getLatestEvent()!=null){
                holder.lastAcc.setText(projectModel.getLatestEvent().getStatus());
                holder.lastDate.setText(projectModel.getLatestEvent().getLastUpdate());
                if(projectModel.getLatestEvent()!=null||projectModel.getLatestEvent().getLastUpdate().isEmpty()){
                    holder.lastDate.setText(context.getResources().getString(R.string.waiting_for_offering));
                }
            }

            if(projectModel.getLatestTransaction()!=null){
                holder.lastBill.setText(projectModel.getLatestTransaction().getTransaction() + " " +  Utils.currencyForShow);
//                holder.lastBill.setText(String.format(context.getResources().getString(R.string.bath_parame), projectModel.getLatestTransaction().getTransaction()));
                if(projectModel.getLatestTransaction().getTransaction().equalsIgnoreCase("0")){
                    holder.lastBill.setText(context.getResources().getString(R.string.no_order));
                }

                holder.date.setText(projectModel.getLatestTransaction().getLastUpdate());
                if(projectModel.getLatestTransaction().getLastUpdate().isEmpty()){
                    holder.date.setText(context.getResources().getString(R.string.waiting_for_offering));
                }

                holder.total.setText(projectModel.getLatestTransaction().getTransaction() + " " + Utils.currencyForShow);
//                holder.total.setText(String.format(context.getResources().getString(R.string.bath_parame), projectModel.getLatestTransaction().getTransaction()));
                if(projectModel.getLatestTransaction().getTransaction().equalsIgnoreCase("0")){
                    holder.total.setText(context.getResources().getString(R.string.no_order));
                }
            }


//            if(projectModel.getPic().contains("customers.png")){
                if (projectModel.getPic() != null) {
                    if (!projectModel.getPic().isEmpty()) {
                        Glide.with(context)
                                .load(projectModel.getPic())
                                .into(holder.profileImage);
                    }else{
                        Glide.with(context).clear(holder.profileImage);
                        holder.profileImage.setImageDrawable(context.getDrawable(R.drawable.bg_photo_copy));
                    }
                }
//            }else{
//                Random rand = new Random();
//                final int n = rand.nextInt(1000000);
//                Glide.with(context)
//                        .load(projectModel.getPic()+"?"+n)
//                        .into(holder.profileImage);
//            }



            holder.clickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnclickItems(projectModel);
//                if(projectModel.getCustomerPhone().length()>0){
//                    Intent intent = new Intent(context, CustomerDetailActivity.class);
//                    intent.putExtra("id",projectModel.getUserId());
//                    intent.putExtra("customer_id",projectModel.getCustomerId());
//                    context.startActivity(intent);
//                    if (context instanceof MainActivity) {
//                        MainActivity activ = (MainActivity) context;
//                        activ.switchContent(CustomerDetailFragment.newInstance(
//                                projectModel.getUserId(),
//                                projectModel.getCustomerPhone()
//                        ), CustomerDetailFragment.TAG);
//                    }
//                }else{
//                    showToast((Activity) context,"ไม่สามารถเรียกดูข้อมูลได้ขณะนี้");
//                }
                }
            });
        }else if(itemPosition == LOADING){
            LoadingVH loadingVH = (LoadingVH) vholder;
            if (retryPageLoad) {
                loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                loadingVH.mProgressBar.setVisibility(View.GONE);

                loadingVH.mErrorTxt.setText(
                        errorMsg != null ?
                                errorMsg :
                                context.getResources().getString(R.string.txt_error));

            } else {
                loadingVH.mErrorLayout.setVisibility(View.GONE);
                loadingVH.mProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setBarPercent(View view,double percent,int maxBarSize){
        double fn_size = (percent*maxBarSize)/100;
        if(fn_size==0){
            view.getLayoutParams().height = 0;
            view.getLayoutParams().width = 0;
            view.requestLayout();
        }else{
            view.getLayoutParams().height = minSize;
            view.getLayoutParams().width = (int) fn_size;
            view.requestLayout();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class CustomerViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView profileImage;
        private TextView status;
        private TextView lastAcc;
        private TextView lastDate;
        private TextView lastBill;
        private TextView date;
        private TextView total;
        private LinearLayout clickView;


        public CustomerViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            profileImage = view.findViewById(R.id.profileImage);
            status = view.findViewById(R.id.status);
            lastAcc = view.findViewById(R.id.last_acc);
            lastDate = view.findViewById(R.id.last_date);
            lastBill = view.findViewById(R.id.last_bill);
            date = view.findViewById(R.id.date);
            total = view.findViewById(R.id.total);
            clickView = view.findViewById(R.id.clickView);
        }
    }

    private class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int i = view.getId();
            if (i == R.id.loadmore_retry || i == R.id.loadmore_errorlayout) {
                showRetry(false, null);
//                mCallback.retryPageLoad();
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(arrayList.size() - 1);

        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ItemsItem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = arrayList.size() - 1;
       ItemsItem result = getItem(position);

        if (result != null) {
            arrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsItem getItem(int position) {
        return arrayList.get(position);
    }


    public void add(ItemsItem r) {
        arrayList.add(r);
        notifyItemInserted(arrayList.size() - 1);
    }
    public void addAll(List<ItemsItem> moveResults, String _searchText) {
        for (ItemsItem result : moveResults) {
            add(result);
        }
        this.searchText = _searchText;
    }


}