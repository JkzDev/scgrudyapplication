package com.scg.rudy.ui.dealbyproject;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;
import com.scg.rudy.model.pojo.dealbyproject.classsku.SkuItem;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.models.ExpandableGroup;
import com.scg.rudy.utils.custom_view.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class ClassViewHolder extends GroupViewHolder {
//
  private ImageView iconArrow;
  private TextView subPhaseName;
  private RelativeLayout addFav;
  private ImageView iconFav;

  public ClassViewHolder(final View itemView) {
    super(itemView);
      iconArrow = itemView.findViewById(R.id.icon_arrow);
      subPhaseName = itemView.findViewById(R.id.sub_phase_name);
      addFav = itemView.findViewById(R.id.add_fav);
      iconFav = itemView.findViewById(R.id.icon_fav);

  }

  public void setGenreTitle(List<SkuItem> skuItems,ExpandableGroup genre, byProjectInterface listener) {
      subPhaseName.setText(genre.getName());
      if (genre.getFav().equalsIgnoreCase("1")) {
        iconFav.setImageResource(R.drawable.favorite_fill_blue);
      } else {
        iconFav.setImageResource(R.drawable.favorite_line_blue);
      }
    iconFav.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (genre.getFav().equalsIgnoreCase("1")) {
          genre.setFav("0");
            listener.addFAVClass(genre.getId(),false);

        }else{
          genre.setFav("1");
            listener.addFAVClass(genre.getId(),true);
        }
      }
    });
      if(skuItems.size()>0){

        iconArrow.setVisibility(View.VISIBLE);
      }else {
        iconArrow.setVisibility(View.INVISIBLE);

      }
  }

  @Override
  public void expand() {
    animateExpand();
  }

  @Override
  public void collapse() {
    animateCollapse();
  }

  private void animateExpand() {
    final RotateAnimation rotate = new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    if(iconArrow.getVisibility()==View.VISIBLE){
      iconArrow.setAnimation(rotate);
    }

  }

  private void animateCollapse() {
    final  RotateAnimation rotate = new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
    rotate.setDuration(300);
    rotate.setFillAfter(true);
    if(iconArrow.getVisibility()==View.VISIBLE){
      iconArrow.setAnimation(rotate);
    }

  }
}
