package com.scg.rudy.ui.choose_sku;

import com.scg.rudy.model.pojo.choose_sku.ChooseSku;
import com.scg.rudy.model.pojo.choose_sku.ItemsItem;

import okhttp3.ResponseBody;

public interface ChooseSkuInterface {
    void showData(ChooseSku chooseSku);
    void showErrorIsNull();
    void BodyError(ResponseBody responseBodyError);
    void Failure(Throwable t);
    void retryPageLoad();
    void addFAV(String skuId);
    void removeFAV(String skuId);
    void onClickinfo(ItemsItem items);
}
