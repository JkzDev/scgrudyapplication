
package com.scg.rudy.ui.by_projects.Models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Item {

    @SerializedName("display_name")
    private String mDisplayName;
    @SerializedName("id")
    private String mId;

    public Item(String _id, String _displayName){
        mId = _id;
        mDisplayName = _displayName;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

}
