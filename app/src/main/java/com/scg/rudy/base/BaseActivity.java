package com.scg.rudy.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.PurchaseEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.scg.hover.overlay.OverlayPermission;
import com.scg.rudy.R;
import com.scg.rudy.RudyApplication;
import com.scg.rudy.model.pojo.BasketModel;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.ui.login.LoginActivity;
import com.scg.rudy.ui.splash_screen.SplashScreenActivity;
import com.scg.rudy.utils.DialogUtils;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_ui.floatingActionButton.FloatingActionButton;
import com.scg.rudy.utils.custom_ui.floatingActionButton.FloatingActionsMenu;
import com.scg.rudy.utils.custom_view.hover.service.RudyHoverMenuService;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import static com.scg.rudy.utils.Utils.PERF_LOGIN;

/**
 *
 * @author jackie
 * @date 3/9/2017 AD
 */

public abstract class BaseActivity extends AppCompatActivity {
    public FirebaseAnalytics mFirebaseAnalytics;
    private View holder;
    public FloatingActionButton soundRecord;
    public FloatingActionButton noteRecord;
    public FloatingActionsMenu fabMenu;
    public String filePath = "";
    public int color = 0;
    public int requestCode = 0;
    public static String user_id ;
    public static String shopID;
    public static int favItemSize = 0;
    private Dialog progressDialog;
    private LinearLayoutManager mLayoutManager;
    private View layout;
    private Animation translationIn;
    private static final int REQUEST_CODE_HOVER_PERMISSION = 7788;
    private boolean mPermissionsRequested = false;
    public static final String MIXPANEL_TOKEN = "18e0073190f10041c91bb5eda7e379fb";
    public MixpanelAPI mixpanel;
    public RudyApplication gTracker;
    public Tracker tracker;
    final private int RC_PERMISSION_WRITE_EXTERNAL_STORAGE = 992;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEnviroment();
        setSystemBar();
        defineVariable();

        setmFirebaseAnalytics(mFirebaseAnalytics);
        setTrackAppCenter();
    }

    private void setTrackAppCenter(){
//        AppCenter.start(getApplication(), "7cae7c58-1c21-4f0f-9154-855d23acf04f",
//                Analytics.class, Crashes.class);
        Locale myLocale = new Locale(Utils.getLang(getApplicationContext()));
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        Resources resources = getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    private void defineVariable(){
        mixpanel = MixpanelAPI.getInstance(getApplicationContext(), MIXPANEL_TOKEN);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        gTracker = (RudyApplication) getApplication();
        tracker = gTracker.getDefaultTracker();
    }

    private  void setSystemBar(){
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setNavigationBarTintEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this instanceof SplashScreenActivity || this instanceof LoginActivity) {
            return;
        } else {
            if (!mPermissionsRequested && !OverlayPermission.hasRuntimePermissionToDrawOverlay(this)) {
                @SuppressWarnings("NewApi")
                Intent myIntent = OverlayPermission.createIntentToRequestOverlayPermission(this);
                startActivityForResult(myIntent, REQUEST_CODE_HOVER_PERMISSION);
            } else {
                if (!mPermissionsRequested && !OverlayPermission.hasRuntimePermissionToDrawOverlay(this)) {
                    @SuppressWarnings("NewApi")
                    Intent myIntent = OverlayPermission.createIntentToRequestOverlayPermission(this);
                    startActivityForResult(myIntent, REQUEST_CODE_HOVER_PERMISSION);
                } else {
                    if (this instanceof SplashScreenActivity || this instanceof LoginActivity) {
                        return;
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        RC_PERMISSION_WRITE_EXTERNAL_STORAGE);
                                return;
                            }
                            RudyHoverMenuService.showFloatingMenu(this);
                        } else {
                            RudyHoverMenuService.showFloatingMenu(this);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setmFirebaseAnalytics(FirebaseAnalytics mFirebaseAnalytics){
            String s = this.getLocalClassName();
            s = s.substring(s.lastIndexOf('.') + 1).replace("Activity"," Screen");
            mixpanel.track(s);
            Log.e("app", "Activity name:" + s);

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Event.SELECT_CONTENT, s);

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putContentName(s)
                    .putContentType("Screen"));


        String json = Utils.getPrefer(getApplicationContext(),PERF_LOGIN);
        Bundle bundleTrack = new Bundle();
        bundleTrack.putString("appName", "Rudy");
        bundleTrack.putString("appVersion", Utils.getVersionName(getApplicationContext()));
        bundleTrack.putString("appVersionCode", String.valueOf(Utils.getVersionCode(getApplicationContext())));
        bundleTrack.putString("screenName", s);

        Map<String, String> mapAppCenter = new HashMap<String, String>();
        mapAppCenter.put("appName", "Rudy");
        mapAppCenter.put("appVersion", Utils.getVersionName(getApplicationContext()));
        mapAppCenter.put("appVersionCode", String.valueOf(Utils.getVersionCode(getApplicationContext())));
        mapAppCenter.put("screenName", s);

        if(json.length()>0){
            UserPOJO userPOJO = UserPOJO.create(json);
            bundleTrack.putString("email", userPOJO.getItems().getEmail());
            bundleTrack.putString("employee_id", userPOJO.getItems().getEmployeeId());
            bundleTrack.putString("id", userPOJO.getItems().getId());
            bundleTrack.putString("level", userPOJO.getItems().getLevel());
            bundleTrack.putString("name", userPOJO.getItems().getName());
            bundleTrack.putString("shop", userPOJO.getItems().getShop());
            bundleTrack.putString("shop_id",userPOJO.getItems().getShopId());

            mapAppCenter.put("email", userPOJO.getItems().getEmail());
            mapAppCenter.put("employee_id", userPOJO.getItems().getEmployeeId());
            mapAppCenter.put("id", userPOJO.getItems().getId());
            mapAppCenter.put("level", userPOJO.getItems().getLevel());
            mapAppCenter.put("name", userPOJO.getItems().getName());
            mapAppCenter.put("shop", userPOJO.getItems().getShop());
            mapAppCenter.put("shop_id",userPOJO.getItems().getShopId());

        }
        mFirebaseAnalytics.logEvent("openScreen", bundleTrack);

       // Analytics.trackEvent("openScreen", mapAppCenter);

        tracker.set("appName", "Rudy");
        tracker.set("appVersion", Utils.getVersionName(getApplicationContext()));
        tracker.set("appVersionCode", String.valueOf(Utils.getVersionCode(getApplicationContext())));
        tracker.set("screenName", s);
        if(json.length()>0){
            UserPOJO userPOJO = UserPOJO.create(json);
            tracker.set("email", userPOJO.getItems().getEmail());
            tracker.set("employee_id", userPOJO.getItems().getEmployeeId());
            tracker.set("id", userPOJO.getItems().getId());
            tracker.set("level", userPOJO.getItems().getLevel());
            tracker.set("name", userPOJO.getItems().getName());
            tracker.set("shop", userPOJO.getItems().getShop());
            tracker.set("shop_id",userPOJO.getItems().getShopId());
        }

        tracker.setScreenName(s);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());


    }

    public void setEnviroment() {
        if(Utils.getPrefer(this, PERF_LOGIN).length()>0){
            Utils.requestPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            Utils.requestPermission(this, android.Manifest.permission.RECORD_AUDIO);

            UserPOJO userPOJO = Utils.setEnviroment(this);
            BaseActivity.user_id = userPOJO.getItems().getId();
            BaseActivity.shopID = userPOJO.getItems().getShopId();
        }
    }

    public void trackPurchaseEvent(ArrayList<BasketModel> arrayList){
        UserPOJO userPOJO = Utils.setEnviroment(this);
        for (int i = 0; i < arrayList.size(); i++) {
            BasketModel basketModel = arrayList.get(i);
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())*Double.parseDouble(basketModel.getPrice())))
                    .putCustomAttribute("Amount",BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())))
                    .putCustomAttribute("Total",BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())*Double.parseDouble(basketModel.getPrice())))
                    .putCustomAttribute("Price",BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())))
                    .putCurrency(Currency.getInstance("THB"))
                    .putItemName(basketModel.getItem_name())
                    .putItemId(basketModel.getId())
                    .putSuccess(true));


            Map<String, Object> props = new HashMap<String, Object>();
            props.put("Price", BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())*Double.parseDouble(basketModel.getPrice())));
            props.put("Amount", BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())));
            props.put("Total", BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())*Double.parseDouble(basketModel.getPrice())));
            props.put("Price", BigDecimal.valueOf(Double.parseDouble(basketModel.getAmount())));
            props.put("Currency", Currency.getInstance("THB"));
            props.put("Name", basketModel.getItem_name());
            props.put("Id",basketModel.getId());
            props.put("shop", userPOJO.getItems().getShop());

        }

    }

    public void setCustomTag(String customTag){
        UserPOJO userPOJO = Utils.setEnviroment(this);
        JSONObject object = new JSONObject();
        try {
            object.put("email", userPOJO.getItems().getEmail());
            object.put("employee_id", userPOJO.getItems().getEmployeeId());
            object.put("id", userPOJO.getItems().getId());
            object.put("level", userPOJO.getItems().getLevel());
            object.put("name", userPOJO.getItems().getName());
            object.put("shop", userPOJO.getItems().getShop());
            object.put("shop_id",userPOJO.getItems().getShopId());
            object.put("Date", new Date());
        } catch (JSONException e) {
            e.printStackTrace();
            LogAnswer(e);
        }


        Bundle bundle = new Bundle();
        bundle.putString("email", userPOJO.getItems().getEmail());
        bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
        bundle.putString("id", userPOJO.getItems().getId());
        bundle.putString("level", userPOJO.getItems().getLevel());
        bundle.putString("name", userPOJO.getItems().getName());
        bundle.putString("shop", userPOJO.getItems().getShop());
        bundle.putString("shop_id",userPOJO.getItems().getShopId());
        mFirebaseAnalytics.logEvent(customTag, bundle);
        Answers.getInstance().logCustom(new CustomEvent(customTag)
                .putCustomAttribute("email", userPOJO.getItems().getEmail())
                .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                .putCustomAttribute("id", userPOJO.getItems().getId())
                .putCustomAttribute("level", userPOJO.getItems().getLevel())
                .putCustomAttribute("name", userPOJO.getItems().getName())
                .putCustomAttribute("shop", userPOJO.getItems().getShop())
                .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
        );

        Map<String, Object> props = new HashMap<String, Object>();
        props.put("email", userPOJO.getItems().getEmail());
        props.put("employee_id", userPOJO.getItems().getEmployeeId());
        props.put("id", userPOJO.getItems().getId());
        props.put("level", userPOJO.getItems().getLevel());
        props.put("name", userPOJO.getItems().getName());
        props.put("shop", userPOJO.getItems().getShop());
        props.put("shop_id",userPOJO.getItems().getShopId());
        props.put("Date", new Date());
        mixpanel.track(customTag, object);
    }

    public void setExceptionTag(Activity activity,String url,String exception){
        UserPOJO userPOJO = Utils.setEnviroment(this);
        String s = activity.getLocalClassName();
        JSONObject object = new JSONObject();

        s = s.substring(s.lastIndexOf('.') + 1);
        try {
            object.put("url", url);
            object.put("exception", exception);
            object.put("email", userPOJO.getItems().getEmail());
            object.put("employee_id", userPOJO.getItems().getEmployeeId());
            object.put("id", userPOJO.getItems().getId());
            object.put("level", userPOJO.getItems().getLevel());
            object.put("name", userPOJO.getItems().getName());
            object.put("shop", userPOJO.getItems().getShop());
            object.put("shop_id",userPOJO.getItems().getShopId());
            object.put("Date", new Date());
        } catch (JSONException e) {
            e.printStackTrace();
            LogAnswer(e);
        }

        mixpanel.track(s, object);

    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void showLoading(Activity activity) {
        hideLoading();
        try {
            if(!(activity).isFinishing()){
                if(progressDialog==null){
                    progressDialog = DialogUtils.showLoadingDialog(this);
                }else{
                    ImageView image= progressDialog.findViewById(R.id.loader);
                    image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_indefinitely) );
                    progressDialog.show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            LogAnswer(e);
        }

    }

    public void hideLoading() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
            LogAnswer(e);
        }

    }

    public void showMessageOKCancel(Activity activity,String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.btn_ok), okListener)
                .setNegativeButton(getResources().getString(R.string.btn_cancel), null)
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean addPermission(Activity activity,List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,permission)) {
                return false;
            }
        }
        return true;
    }

    public void onFragmentAttached() {
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_CODE_HOVER_PERMISSION == requestCode) {
            mPermissionsRequested = true;
//           RudyHoverMenuService.showFloatingMenu(this);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void LogAnswer(Exception e){
        UserPOJO userPOJO = Utils.setEnviroment(this);
        Answers.getInstance().logCustom(new CustomEvent(e.getMessage())
                .putCustomAttribute("email", userPOJO.getItems().getEmail())
                .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                .putCustomAttribute("id", userPOJO.getItems().getId())
                .putCustomAttribute("level", userPOJO.getItems().getLevel())
                .putCustomAttribute("name", userPOJO.getItems().getName())
                .putCustomAttribute("shop", userPOJO.getItems().getShop())
                .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
        );
    }


    public static void LogException(String url,Exception e){
        UserPOJO userPOJO = Utils.setEnviroment(RudyApplication.getAppContext());
        Answers.getInstance().logCustom(new CustomEvent(e.getMessage())
                .putCustomAttribute("url", url)
                .putCustomAttribute("email", userPOJO.getItems().getEmail())
                .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                .putCustomAttribute("id", userPOJO.getItems().getId())
                .putCustomAttribute("level", userPOJO.getItems().getLevel())
                .putCustomAttribute("name", userPOJO.getItems().getName())
                .putCustomAttribute("shop", userPOJO.getItems().getShop())
                .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
        );
    }

}