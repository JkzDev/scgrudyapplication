package com.scg.rudy.base;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.MotionEvent;
import android.view.View;


import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.scg.rudy.utils.DialogUtils;
import com.scg.rudy.utils.Utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.Unbinder;

/**
 * Created by DekDroidDev on 11/5/2018 AD.
 */
public abstract class BaseFragment extends Fragment{
    public FirebaseAnalytics mFirebaseAnalytics;
    private BaseActivity mActivity;
    private Unbinder mUnBinder;
    private Dialog progressDialog;
    public String shopID,user_id;
    public static Bitmap bit1;
    public static Bitmap bit2;
    public static Bitmap bit3;
    public static Bitmap bit4;
    public static Bitmap bit5;
    public static Bitmap bit6;
    public static Double DragLat = 0.0, DragLng = 0.0;
    public static String address = "";
    public static  String phaseId = "1";
    public static  String cateId = "1";
    public static String project_name = "";
    public static String unit_number = "1";
    public static String floor_number = "2";
    public static String unit_area = "150";
    public static String unit_budget = "2.5";
    public static String c_id = "";// ลูกค้า ID กรณีลูกค้าใหม่ไม่ต้องใส่มา
    public static String c_id_owner = "";
    public static int ctype = 1;
    public static int ctype_owner = 3;//ประเถทลูกค้า ID : 1=ผู้รับเหมา, 2=เจ้าของงาน
    public static String cname = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany = "";// บริษัทลูกค้า
    public static String cphone = "";// เบอร์ติดต่อลูกค้า
    public static String cline = "";// line ID ของลูกค้า
    public static String cnote = "";// noteลูกค้า
    public static String cname_owner = "";// ชื่อ-นามสกุล/ ชื่อเล่น*
    public static String ccompany_owner = "";// บริษัทลูกค้า
    public static String cphone_owner = "";// เบอร์ติดต่อลูกค้า
    public static String cline_owner = "";// line ID ของลูกค้า
    public static String cnote_owner = "";// noteลูกค้า
    public static String other_type = "";
    public static String image64_1 = "";
    public static String image64_2 = "";
    public static String image64_3 = "";
    public static String image64_4 = "";
    public static String image64_5 = "";
    public static String image64_6 = "";
    public static  int catePosition = 0;
    public static  int phasePosition = 0;
    public static  int customerTypePosition = 0;
    public static  int ownerTypePosition = 0;
    private String imagei_1 = "";
    private String imagei_2 = "";
    private String imagei_3 = "";
    private String imagei_4 = "";
    private String imagei_5 = "";
    private String imagei_6 = "";
    public static boolean data1 = false;
    public static boolean data2 = false;
    public static boolean data3 = false;
    public static boolean data4 = false;
    public static boolean data5 = false;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getBaseActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        setUp(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    public void showLoading() {
        hideLoading();
        progressDialog = DialogUtils.showLoadingDialog(mActivity);
    }

    public void hideLoading() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    protected abstract void setUp(View view);

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    public void setCustomTag(String customTag){
        UserPOJO userPOJO = Utils.setEnviroment(getBaseActivity());
        Bundle bundle = new Bundle();
        bundle.putString("email", userPOJO.getItems().getEmail());
        bundle.putString("employee_id", userPOJO.getItems().getEmployeeId());
        bundle.putString("id", userPOJO.getItems().getId());
        bundle.putString("level", userPOJO.getItems().getLevel());
        bundle.putString("name", userPOJO.getItems().getName());
        bundle.putString("shop", userPOJO.getItems().getShop());
        bundle.putString("shop_id",userPOJO.getItems().getShopId());
        mFirebaseAnalytics.logEvent(customTag, bundle);
        Answers.getInstance().logCustom(new CustomEvent(customTag)
                .putCustomAttribute("email", userPOJO.getItems().getEmail())
                .putCustomAttribute("employee_id", userPOJO.getItems().getEmployeeId())
                .putCustomAttribute("id", userPOJO.getItems().getId())
                .putCustomAttribute("level", userPOJO.getItems().getLevel())
                .putCustomAttribute("name", userPOJO.getItems().getName())
                .putCustomAttribute("shop", userPOJO.getItems().getShop())
                .putCustomAttribute("shop_id", userPOJO.getItems().getShopId())
        );
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("email", userPOJO.getItems().getEmail());
        props.put("employee_id", userPOJO.getItems().getEmployeeId());
        props.put("id", userPOJO.getItems().getId());
        props.put("level", userPOJO.getItems().getLevel());
        props.put("name", userPOJO.getItems().getName());
        props.put("shop", userPOJO.getItems().getShop());
        props.put("shop_id",userPOJO.getItems().getShopId());
        props.put("Date", new Date());
        //Appsee.addEvent(customTag, props);

    }

//    public void backToHome(){
//        if (getActivity() instanceof MainActivity) {
//            MainActivity activ = (MainActivity) getActivity();
//            activ.switchContent(mainFragment.newInstance(),mainFragment.TAG);
//        }
//    }

    public interface Callback {
        void onFragmentAttached();
        void onFragmentDetached(String tag);
    }
}