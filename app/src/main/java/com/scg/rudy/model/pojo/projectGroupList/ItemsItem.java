package com.scg.rudy.model.pojo.projectGroupList;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("distance")
	private int distance;

	@SerializedName("developer_name")
	private String developerName;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("sum_unit_budget")
	private String sumUnitBudget;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("sum_units")
	private String sumUnits;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("project_group_type_name")
	private String projectGroupTypeName;

	@SerializedName("sum_trans")
	private String sumTrans;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("name")
	private String name;

	@SerializedName("project_group_type_id")
	private String projectGroupTypeId;

	@SerializedName("id")
	private String id;

	@SerializedName("sum_opp")
	private String sumOpp;

	@SerializedName("developer_id")
	private String developerId;

	@SerializedName("gallery")
	private List<Object> gallery;

	@SerializedName("start_date")
	private String startDate;

	@SerializedName("num_proj")
	private int numProj;

	@SerializedName("status")
	private int status;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setDistance(int distance){
		this.distance = distance;
	}

	public int getDistance(){
		return distance;
	}

	public void setDeveloperName(String developerName){
		this.developerName = developerName;
	}

	public String getDeveloperName(){
		return developerName;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setSumUnitBudget(String sumUnitBudget){
		this.sumUnitBudget = sumUnitBudget;
	}

	public String getSumUnitBudget(){
		return sumUnitBudget;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setSumUnits(String sumUnits){
		this.sumUnits = sumUnits;
	}

	public String getSumUnits(){
		return sumUnits;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProjectGroupTypeName(String projectGroupTypeName){
		this.projectGroupTypeName = projectGroupTypeName;
	}

	public String getProjectGroupTypeName(){
		return projectGroupTypeName;
	}

	public void setSumTrans(String sumTrans){
		this.sumTrans = sumTrans;
	}

	public String getSumTrans(){
		return sumTrans;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setProjectGroupTypeId(String projectGroupTypeId){
		this.projectGroupTypeId = projectGroupTypeId;
	}

	public String getProjectGroupTypeId(){
		return projectGroupTypeId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSumOpp(String sumOpp){
		this.sumOpp = sumOpp;
	}

	public String getSumOpp(){
		return sumOpp;
	}

	public void setDeveloperId(String developerId){
		this.developerId = developerId;
	}

	public String getDeveloperId(){
		return developerId;
	}

	public void setGallery(List<Object> gallery){
		this.gallery = gallery;
	}

	public List<Object> getGallery(){
		return gallery;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	public void setNumProj(int numProj){
		this.numProj = numProj;
	}

	public int getNumProj(){
		return numProj;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"end_date = '" + endDate + '\'' + 
			",distance = '" + distance + '\'' + 
			",developer_name = '" + developerName + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",sum_unit_budget = '" + sumUnitBudget + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",sum_units = '" + sumUnits + '\'' + 
			",user_id = '" + userId + '\'' + 
			",project_group_type_name = '" + projectGroupTypeName + '\'' + 
			",sum_trans = '" + sumTrans + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",name = '" + name + '\'' + 
			",project_group_type_id = '" + projectGroupTypeId + '\'' + 
			",id = '" + id + '\'' + 
			",sum_opp = '" + sumOpp + '\'' + 
			",developer_id = '" + developerId + '\'' + 
			",gallery = '" + gallery + '\'' + 
			",start_date = '" + startDate + '\'' + 
			",num_proj = '" + numProj + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.endDate);
		dest.writeInt(this.distance);
		dest.writeString(this.developerName);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.statusTxt);
		dest.writeString(this.sumUnitBudget);
		dest.writeString(this.shopId);
		dest.writeString(this.latLng);
		dest.writeString(this.sumUnits);
		dest.writeString(this.userId);
		dest.writeString(this.projectGroupTypeName);
		dest.writeString(this.sumTrans);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.name);
		dest.writeString(this.projectGroupTypeId);
		dest.writeString(this.id);
		dest.writeString(this.sumOpp);
		dest.writeString(this.developerId);
		dest.writeList(this.gallery);
		dest.writeString(this.startDate);
		dest.writeInt(this.numProj);
		dest.writeInt(this.status);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.endDate = in.readString();
		this.distance = in.readInt();
		this.developerName = in.readString();
		this.addedDatetime = in.readString();
		this.statusTxt = in.readString();
		this.sumUnitBudget = in.readString();
		this.shopId = in.readString();
		this.latLng = in.readString();
		this.sumUnits = in.readString();
		this.userId = in.readString();
		this.projectGroupTypeName = in.readString();
		this.sumTrans = in.readString();
		this.lastUpdate = in.readString();
		this.name = in.readString();
		this.projectGroupTypeId = in.readString();
		this.id = in.readString();
		this.sumOpp = in.readString();
		this.developerId = in.readString();
		this.gallery = new ArrayList<Object>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.startDate = in.readString();
		this.numProj = in.readInt();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}