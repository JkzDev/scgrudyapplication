package com.scg.rudy.model.pojo.transaction_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("customer_tax_no")
	private String customerTaxNo;

	@SerializedName("pr")
	private List<PrItem> pr;

	@SerializedName("project_id")
	private String projectId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("added_date")
	private String addedDate;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("billing_address")
	private String billingAddress;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("credit_type")
	private String credit_type;

	@SerializedName("number_of_installments")
	private int number_of_installments;

	public int getTax_type() {
		return tax_type;
	}

	public void setTax_type(int tax_type) {
		this.tax_type = tax_type;
	}

	@SerializedName("tax_type")
	private int tax_type;

	@SerializedName("delivery_cost")
	private Double delivery_cost;

	public Double getDelivery_cost() {
		return delivery_cost;
	}

	public void setDelivery_cost(Double delivery_cost) {
		this.delivery_cost = delivery_cost;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@SerializedName("discount")
	private Double discount;

	@SerializedName("discount_type")
	private String discount_type;

	@SerializedName("payment_type")
	private String payment_type;

	public String getDiscount_type() {
		return discount_type;
	}

	public void setDiscount_type(String discount_type) {
		this.discount_type = discount_type;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getCredit_day() {
		return credit_day;
	}

	public void setCredit_day(String credit_day) {
		this.credit_day = credit_day;
	}

	@SerializedName("credit_day")
	private String credit_day;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@SerializedName("note")
	private String note;

	public String getCredit_type() {
		return credit_type;
	}

	public void setCredit_type(String credit_type) {
		this.credit_type = credit_type;
	}

	public int getNumber_of_installments() {
		return number_of_installments;
	}

	public void setNumber_of_installments(int number_of_installments) {
		this.number_of_installments = number_of_installments;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setCustomerTaxNo(String customerTaxNo){
		this.customerTaxNo = customerTaxNo;
	}

	public String getCustomerTaxNo(){
		return customerTaxNo;
	}

	public void setPr(List<PrItem> pr){
		this.pr = pr;
	}

	public List<PrItem> getPr(){
		return pr;
	}

	public void setProjectId(String projectId){
		this.projectId = projectId;
	}

	public String getProjectId(){
		return projectId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setAddedDate(String addedDate){
		this.addedDate = addedDate;
	}

	public String getAddedDate(){
		return addedDate;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setBillingAddress(String billingAddress){
		this.billingAddress = billingAddress;
	}

	public String getBillingAddress(){
		return billingAddress;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	@Override
 	public String toString(){
		return 
			"projectDetailItems{" +
			"shop_id = '" + shopId + '\'' + 
			",customer_tax_no = '" + customerTaxNo + '\'' + 
			",pr = '" + pr + '\'' + 
			",project_id = '" + projectId + '\'' + 
			",user_id = '" + userId + '\'' + 
			",added_date = '" + addedDate + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",billing_address = '" + billingAddress + '\'' + 
			",id = '" + id + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.shopId);
		dest.writeString(this.customerTaxNo);
		dest.writeList(this.pr);
		dest.writeString(this.projectId);
		dest.writeString(this.userId);
		dest.writeString(this.addedDate);
		dest.writeString(this.customerEmail);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.customerPhone);
		dest.writeString(this.billingAddress);
		dest.writeString(this.id);
		dest.writeString(this.customerName);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.shopId = in.readString();
		this.customerTaxNo = in.readString();
		this.pr = new ArrayList<PrItem>();
		in.readList(this.pr, PrItem.class.getClassLoader());
		this.projectId = in.readString();
		this.userId = in.readString();
		this.addedDate = in.readString();
		this.customerEmail = in.readString();
		this.lastUpdate = in.readString();
		this.customerPhone = in.readString();
		this.billingAddress = in.readString();
		this.id = in.readString();
		this.customerName = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}