package com.scg.rudy.model.pojo.pgdetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PrItem implements Parcelable {

	@SerializedName("transaction_id")
	public String transactionId;

	@SerializedName("order_number")
	public String orderNumber;

	@SerializedName("product_id")
	public String productId;

	@SerializedName("last_update")
	public String lastUpdate;

	@SerializedName("net_amount")
	public String netAmount;

	@SerializedName("product_name")
	public String productName;

	@SerializedName("status_txt")
	public String statusTxt;

	@SerializedName("status")
	public String status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.transactionId);
		dest.writeString(this.orderNumber);
		dest.writeString(this.productId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.netAmount);
		dest.writeString(this.productName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
	}

	public PrItem() {
	}

	protected PrItem(Parcel in) {
		this.transactionId = in.readString();
		this.orderNumber = in.readString();
		this.productId = in.readString();
		this.lastUpdate = in.readString();
		this.netAmount = in.readString();
		this.productName = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
	}

	public static final Creator<PrItem> CREATOR = new Creator<PrItem>() {
		@Override
		public PrItem createFromParcel(Parcel source) {
			return new PrItem(source);
		}

		@Override
		public PrItem[] newArray(int size) {
			return new PrItem[size];
		}
	};
}