package com.scg.rudy.model.pojo.promotionDetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PromotionDetail implements Parcelable {

	@SerializedName("total")
	private int total;

	@SerializedName("total_pages")
	private int totalPages;

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setTotalPages(int totalPages){
		this.totalPages = totalPages;
	}

	public int getTotalPages(){
		return totalPages;
	}

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PromotionDetail{" + 
			"total = '" + total + '\'' + 
			",total_pages = '" + totalPages + '\'' + 
			",items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.total);
		dest.writeInt(this.totalPages);
		dest.writeParcelable(this.items, flags);
		dest.writeInt(this.status);
	}

	public PromotionDetail() {
	}

	protected PromotionDetail(Parcel in) {
		this.total = in.readInt();
		this.totalPages = in.readInt();
		this.items = in.readParcelable(Items.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<PromotionDetail> CREATOR = new Parcelable.Creator<PromotionDetail>() {
		@Override
		public PromotionDetail createFromParcel(Parcel source) {
			return new PromotionDetail(source);
		}

		@Override
		public PromotionDetail[] newArray(int size) {
			return new PromotionDetail[size];
		}
	};
}