package com.scg.rudy.model.view_model.audio;

import android.media.AudioFormat;

/**
 * @author jackie
 */

public enum AudioChannel {
    STEREO,
    MONO;

    public int getChannel(){
        switch (this){
            case MONO:
                return AudioFormat.CHANNEL_IN_MONO;
            default:
                return AudioFormat.CHANNEL_IN_STEREO;
        }
    }
}