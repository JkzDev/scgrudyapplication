package com.scg.rudy.model.pojo.projectgroup;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;
import com.scg.rudy.model.pojo.projectgroup.ItemsItem;

import android.os.Parcel;
import android.os.Parcelable;

@Generated("com.asif.gsonpojogenerator")
public class ProjectGroupModel implements Parcelable {

	@SerializedName("items")
	public List<ItemsItem> items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.items);
		dest.writeInt(this.status);
	}

	public ProjectGroupModel() {
	}

	protected ProjectGroupModel(Parcel in) {
		this.items = in.createTypedArrayList(ItemsItem.CREATOR);
		this.status = in.readInt();
	}

	public static final Creator<ProjectGroupModel> CREATOR = new Creator<ProjectGroupModel>() {
		@Override
		public ProjectGroupModel createFromParcel(Parcel source) {
			return new ProjectGroupModel(source);
		}

		@Override
		public ProjectGroupModel[] newArray(int size) {
			return new ProjectGroupModel[size];
		}
	};
}