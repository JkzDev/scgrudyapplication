package com.scg.rudy.model.pojo.dealbyproject.group;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("img")
	private String img;

	@SerializedName("color")
	private int color;

	@SerializedName("BOQ")
	private String bOQ;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setColor(int color){
		this.color = color;
	}

	public int getColor(){
		return color;
	}

	public void setBOQ(String bOQ){
		this.bOQ = bOQ;
	}

	public String getBOQ(){
		return bOQ;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"img = '" + img + '\'' + 
			",color = '" + color + '\'' + 
			",bOQ = '" + bOQ + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.img);
		dest.writeInt(this.color);
		dest.writeString(this.bOQ);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.img = in.readString();
		this.color = in.readInt();
		this.bOQ = in.readString();
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}