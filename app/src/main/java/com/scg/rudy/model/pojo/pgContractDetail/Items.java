package com.scg.rudy.model.pojo.pgContractDetail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("end_date")
	public String endDate;

	@SerializedName("project_group_list_id")
	public String projectGroupListId;

	@SerializedName("developer_name")
	public String developerName;

	@SerializedName("project_group_contact")
	public List<ProjectGroupContactItem> projectGroupContact;

	@SerializedName("added_datetime")
	public String addedDatetime;

	@SerializedName("status_txt")
	public String statusTxt;

	@SerializedName("sum_unit_budget")
	public String sumUnitBudget;

	@SerializedName("shop_id")
	public String shopId;

	@SerializedName("project_group_list_name")
	public String projectGroupListName;

	@SerializedName("sum_units")
	public String sumUnits;

	@SerializedName("user_id")
	public String userId;

	@SerializedName("sum_trans")
	public String sumTrans;

	@SerializedName("last_update")
	public String lastUpdate;

	@SerializedName("name")
	public String name;

	@SerializedName("project_list")
	public List<ProjectListItem> projectList;

	@SerializedName("id")
	public String id;

	@SerializedName("sum_opp")
	public String sumOpp;

	@SerializedName("gallery")
	public List<Object> gallery;

	@SerializedName("num_proj")
	public int numProj;

	@SerializedName("status")
	public int status;

	@SerializedName("start_date")
	public String startDate;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.endDate);
		dest.writeString(this.projectGroupListId);
		dest.writeString(this.developerName);
		dest.writeList(this.projectGroupContact);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.statusTxt);
		dest.writeString(this.sumUnitBudget);
		dest.writeString(this.shopId);
		dest.writeString(this.projectGroupListName);
		dest.writeString(this.sumUnits);
		dest.writeString(this.userId);
		dest.writeString(this.sumTrans);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.name);
		dest.writeList(this.projectList);
		dest.writeString(this.id);
		dest.writeString(this.sumOpp);
		dest.writeList(this.gallery);
		dest.writeInt(this.numProj);
		dest.writeInt(this.status);
		dest.writeString(this.startDate);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.endDate = in.readString();
		this.projectGroupListId = in.readString();
		this.developerName = in.readString();
		this.projectGroupContact = new ArrayList<ProjectGroupContactItem>();
		in.readList(this.projectGroupContact, ProjectGroupContactItem.class.getClassLoader());
		this.addedDatetime = in.readString();
		this.statusTxt = in.readString();
		this.sumUnitBudget = in.readString();
		this.shopId = in.readString();
		this.projectGroupListName = in.readString();
		this.sumUnits = in.readString();
		this.userId = in.readString();
		this.sumTrans = in.readString();
		this.lastUpdate = in.readString();
		this.name = in.readString();
		this.projectList = new ArrayList<ProjectListItem>();
		in.readList(this.projectList, ProjectListItem.class.getClassLoader());
		this.id = in.readString();
		this.sumOpp = in.readString();
		this.gallery = new ArrayList<Object>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.numProj = in.readInt();
		this.status = in.readInt();
		this.startDate = in.readString();
	}

	public static final Creator<Items> CREATOR = new Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}