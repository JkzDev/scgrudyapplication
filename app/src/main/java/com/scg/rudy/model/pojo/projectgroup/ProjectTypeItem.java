package com.scg.rudy.model.pojo.projectgroup;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectTypeItem implements Parcelable {

	@SerializedName("name")
	public String name;

	@SerializedName("id")
	public String id;

	@SerializedName("phases")
	public List<PhasesItem> phases;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
		dest.writeList(this.phases);
	}

	public ProjectTypeItem() {
	}

	protected ProjectTypeItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
		this.phases = new ArrayList<PhasesItem>();
		in.readList(this.phases, PhasesItem.class.getClassLoader());
	}

	public static final Creator<ProjectTypeItem> CREATOR = new Creator<ProjectTypeItem>() {
		@Override
		public ProjectTypeItem createFromParcel(Parcel source) {
			return new ProjectTypeItem(source);
		}

		@Override
		public ProjectTypeItem[] newArray(int size) {
			return new ProjectTypeItem[size];
		}
	};
}