package com.scg.rudy.model.pojo.project_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class NoteItem implements Parcelable {

	@SerializedName("file")
	private String file;

	@SerializedName("last_update")
	private String lastUpdate;

	public void setFile(String file){
		this.file = file;
	}

	public String getFile(){
		return file;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	@Override
 	public String toString(){
		return 
			"NoteItem{" + 
			"file = '" + file + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.file);
		dest.writeString(this.lastUpdate);
	}

	public NoteItem() {
	}

	protected NoteItem(Parcel in) {
		this.file = in.readString();
		this.lastUpdate = in.readString();
	}

	public static final Parcelable.Creator<NoteItem> CREATOR = new Parcelable.Creator<NoteItem>() {
		@Override
		public NoteItem createFromParcel(Parcel source) {
			return new NoteItem(source);
		}

		@Override
		public NoteItem[] newArray(int size) {
			return new NoteItem[size];
		}
	};
}