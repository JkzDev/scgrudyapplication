package com.scg.rudy.model.pojo.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class AllStatusChart implements Parcelable {

	@SerializedName("p_Stat1")
	private String pStat1;

	@SerializedName("p_Stat2")
	private String pStat2;

	@SerializedName("Stat6")
	private String stat6;

	@SerializedName("Stat5")
	private String stat5;

	@SerializedName("Stat4")
	private String stat4;

	@SerializedName("Stat3")
	private String stat3;

	@SerializedName("Stat2")
	private String stat2;

	@SerializedName("Stat1")
	private String stat1;

	@SerializedName("p_Stat5")
	private String pStat5;

	@SerializedName("p_Stat6")
	private String pStat6;

	@SerializedName("p_Stat3")
	private String pStat3;

	@SerializedName("p_Stat4")
	private String pStat4;

	public void setPStat1(String pStat1){
		this.pStat1 = pStat1;
	}

	public String getPStat1(){
		return pStat1;
	}

	public void setPStat2(String pStat2){
		this.pStat2 = pStat2;
	}

	public String getPStat2(){
		return pStat2;
	}

	public void setStat6(String stat6){
		this.stat6 = stat6;
	}

	public String getStat6(){
		return stat6;
	}

	public void setStat5(String stat5){
		this.stat5 = stat5;
	}

	public String getStat5(){
		return stat5;
	}

	public void setStat4(String stat4){
		this.stat4 = stat4;
	}

	public String getStat4(){
		return stat4;
	}

	public void setStat3(String stat3){
		this.stat3 = stat3;
	}

	public String getStat3(){
		return stat3;
	}

	public void setStat2(String stat2){
		this.stat2 = stat2;
	}

	public String getStat2(){
		return stat2;
	}

	public void setStat1(String stat1){
		this.stat1 = stat1;
	}

	public String getStat1(){
		return stat1;
	}

	public void setPStat5(String pStat5){
		this.pStat5 = pStat5;
	}

	public String getPStat5(){
		return pStat5;
	}

	public void setPStat6(String pStat6){
		this.pStat6 = pStat6;
	}

	public String getPStat6(){
		return pStat6;
	}

	public void setPStat3(String pStat3){
		this.pStat3 = pStat3;
	}

	public String getPStat3(){
		return pStat3;
	}

	public void setPStat4(String pStat4){
		this.pStat4 = pStat4;
	}

	public String getPStat4(){
		return pStat4;
	}

	@Override
 	public String toString(){
		return 
			"AllStatusChart{" + 
			"p_Stat1 = '" + pStat1 + '\'' + 
			",p_Stat2 = '" + pStat2 + '\'' + 
			",stat6 = '" + stat6 + '\'' + 
			",stat5 = '" + stat5 + '\'' + 
			",stat4 = '" + stat4 + '\'' + 
			",stat3 = '" + stat3 + '\'' + 
			",stat2 = '" + stat2 + '\'' + 
			",stat1 = '" + stat1 + '\'' + 
			",p_Stat5 = '" + pStat5 + '\'' + 
			",p_Stat6 = '" + pStat6 + '\'' + 
			",p_Stat3 = '" + pStat3 + '\'' + 
			",p_Stat4 = '" + pStat4 + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.pStat1);
		dest.writeString(this.pStat2);
		dest.writeString(this.stat6);
		dest.writeString(this.stat5);
		dest.writeString(this.stat4);
		dest.writeString(this.stat3);
		dest.writeString(this.stat2);
		dest.writeString(this.stat1);
		dest.writeString(this.pStat5);
		dest.writeString(this.pStat6);
		dest.writeString(this.pStat3);
		dest.writeString(this.pStat4);
	}

	public AllStatusChart() {
	}

	protected AllStatusChart(Parcel in) {
		this.pStat1 = in.readString();
		this.pStat2 = in.readString();
		this.stat6 = in.readString();
		this.stat5 = in.readString();
		this.stat4 = in.readString();
		this.stat3 = in.readString();
		this.stat2 = in.readString();
		this.stat1 = in.readString();
		this.pStat5 = in.readString();
		this.pStat6 = in.readString();
		this.pStat3 = in.readString();
		this.pStat4 = in.readString();
	}

	public static final Parcelable.Creator<AllStatusChart> CREATOR = new Parcelable.Creator<AllStatusChart>() {
		@Override
		public AllStatusChart createFromParcel(Parcel source) {
			return new AllStatusChart(source);
		}

		@Override
		public AllStatusChart[] newArray(int size) {
			return new AllStatusChart[size];
		}
	};
}