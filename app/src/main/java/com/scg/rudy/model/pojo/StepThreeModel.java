package com.scg.rudy.model.pojo;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 12/11/2017 AD.
 */

public class StepThreeModel {
    String inPhase;
    ArrayList<phase> phaseArrayList;
    public StepThreeModel(
            String inPhase,
            ArrayList<phase> phaseArrayList
    ){
        this.inPhase = inPhase;
        this.phaseArrayList = phaseArrayList;
    }

    public String getInPhase() {
        return inPhase;
    }

    public void setInPhase(String inPhase) {
        this.inPhase = inPhase;
    }

    public ArrayList<phase> getPhaseArrayList() {
        return phaseArrayList;
    }

    public void setPhaseArrayList(ArrayList<phase> phaseArrayList) {
        this.phaseArrayList = phaseArrayList;
    }

    public static class phase{
        String id;
        String short_name;
        String name;
        ArrayList<subPhase> subPhaseArrayList;
        
        public phase(
                String id,
                String short_name,
                String name,
                ArrayList<subPhase> subPhaseArrayList
        ){
            this.id = id;
            this.short_name = short_name;
            this.name = name;
            this.subPhaseArrayList = subPhaseArrayList;
            
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShort_name() {
            return short_name;
        }

        public void setShort_name(String short_name) {
            this.short_name = short_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<subPhase> getSubPhaseArrayList() {
            return subPhaseArrayList;
        }

        public void setSubPhaseArrayList(ArrayList<subPhase> subPhaseArrayList) {
            this.subPhaseArrayList = subPhaseArrayList;
        }
    }


    public static class subPhase{
        String id;
        String short_name;
        String name;
        String isYes;
        
        public subPhase( 
                String id,
                String short_name,
                String name,
                String isYes){
            this.id = id;
            this.short_name = short_name;
            this.name = name;
            this.isYes = isYes;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShort_name() {
            return short_name;
        }

        public void setShort_name(String short_name) {
            this.short_name = short_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsYes() {
            return isYes;
        }

        public void setIsYes(String isYes) {
            this.isYes = isYes;
        }
    }
}
