package com.scg.rudy.model.pojo.subphase;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("com.asif.gsonpojogenerator")
public class ClassesItem implements Serializable, Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ClassesItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public ClassesItem() {
	}

	protected ClassesItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<ClassesItem> CREATOR = new Parcelable.Creator<ClassesItem>() {
		@Override
		public ClassesItem createFromParcel(Parcel source) {
			return new ClassesItem(source);
		}

		@Override
		public ClassesItem[] newArray(int size) {
			return new ClassesItem[size];
		}
	};
}