package com.scg.rudy.model.pojo.customer_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class TransitionListItem implements Parcelable {

	@SerializedName("project_id")
	private String projectId;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("status")
	private String status;

	public void setProjectId(String projectId){
		this.projectId = projectId;
	}

	public String getProjectId(){
		return projectId;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"TransitionListItem{" + 
			"project_id = '" + projectId + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.projectId);
		dest.writeString(this.orderNumber);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
	}

	public TransitionListItem() {
	}

	protected TransitionListItem(Parcel in) {
		this.projectId = in.readString();
		this.orderNumber = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<TransitionListItem> CREATOR = new Parcelable.Creator<TransitionListItem>() {
		@Override
		public TransitionListItem createFromParcel(Parcel source) {
			return new TransitionListItem(source);
		}

		@Override
		public TransitionListItem[] newArray(int size) {
			return new TransitionListItem[size];
		}
	};
}