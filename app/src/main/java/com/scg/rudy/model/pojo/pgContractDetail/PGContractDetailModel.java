package com.scg.rudy.model.pojo.pgContractDetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PGContractDetailModel implements Parcelable {

	@SerializedName("items")
	public Items items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.items, flags);
		dest.writeInt(this.status);
	}

	public PGContractDetailModel() {
	}

	protected PGContractDetailModel(Parcel in) {
		this.items = in.readParcelable(Items.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Creator<PGContractDetailModel> CREATOR = new Creator<PGContractDetailModel>() {
		@Override
		public PGContractDetailModel createFromParcel(Parcel source) {
			return new PGContractDetailModel(source);
		}

		@Override
		public PGContractDetailModel[] newArray(int size) {
			return new PGContractDetailModel[size];
		}
	};
}