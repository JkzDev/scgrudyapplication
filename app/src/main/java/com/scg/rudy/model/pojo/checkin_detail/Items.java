package com.scg.rudy.model.pojo.checkin_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("phaseprocess")
	private Phaseprocess phaseprocess;

	@SerializedName("project_id")
	private String projectId;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("detail")
	private String detail;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("type")
	private String type;

	@SerializedName("type_txt")
	private String typeTxt;

	@SerializedName("gallery")
	private List<GalleryItem> gallery;

	public String getAddedDatetime() {
		return addedDatetime;
	}

	public void setAddedDatetime(String addedDatetime) {
		this.addedDatetime = addedDatetime;
	}

	public void setPhaseprocess(Phaseprocess phaseprocess){
		this.phaseprocess = phaseprocess;
	}

	public Phaseprocess getPhaseprocess(){
		return phaseprocess;
	}

	public void setProjectId(String projectId){
		this.projectId = projectId;
	}

	public String getProjectId(){
		return projectId;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTypeTxt(String typeTxt){
		this.typeTxt = typeTxt;
	}

	public String getTypeTxt(){
		return typeTxt;
	}

	public void setGallery(List<GalleryItem> gallery){
		this.gallery = gallery;
	}

	public List<GalleryItem> getGallery(){
		return gallery;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"phaseprocess = '" + phaseprocess + '\'' + 
			",project_id = '" + projectId + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",detail = '" + detail + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",type = '" + type + '\'' + 
			",type_txt = '" + typeTxt + '\'' + 
			",gallery = '" + gallery + '\'' + 
			"}";
		}

	public Items() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.phaseprocess, flags);
		dest.writeString(this.projectId);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.detail);
		dest.writeString(this.projectName);
		dest.writeString(this.type);
		dest.writeString(this.typeTxt);
		dest.writeTypedList(this.gallery);
	}

	protected Items(Parcel in) {
		this.phaseprocess = in.readParcelable(Phaseprocess.class.getClassLoader());
		this.projectId = in.readString();
		this.addedDatetime = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.detail = in.readString();
		this.projectName = in.readString();
		this.type = in.readString();
		this.typeTxt = in.readString();
		this.gallery = in.createTypedArrayList(GalleryItem.CREATOR);
	}

	public static final Creator<Items> CREATOR = new Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}