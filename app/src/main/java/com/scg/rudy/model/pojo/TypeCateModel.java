package com.scg.rudy.model.pojo;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 17/1/2018 AD.
 */

public class TypeCateModel {


    /**
     * status : 200
     * items : [{"id":"12","sub_class_name":"เหล็กข้ออ้อย"},{"id":"16","sub_class_name":"เหล็กฉาก"},{"id":"17","sub_class_name":"เหล็กตัว H"},{"id":"19","sub_class_name":"เหล็กตัว C"},{"id":"20","sub_class_name":"เหล็กเส้นกลม"},{"id":"29","sub_class_name":"เหล็กรูปพรรณอื่น ๆ"},{"id":"31","sub_class_name":"เหล็กก่อสร้างอื่น ๆ"}]
     */




    private ArrayList<Items> items;
    public TypeCateModel(ArrayList<Items> items){
        this.items = items;
    }



    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }

    public static class Items {
        /**
         * id : 12
         * sub_class_name : เหล็กข้ออ้อย
         */
        public Items(
                 String id,
                 String sub_class_name
        ){
            this.id = id;
            this.sub_class_name = sub_class_name;
        }


        private String id;
        private String sub_class_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSub_class_name() {
            return sub_class_name;
        }

        public void setSub_class_name(String sub_class_name) {
            this.sub_class_name = sub_class_name;
        }
    }
}
