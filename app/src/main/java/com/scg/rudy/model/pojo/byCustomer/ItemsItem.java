package com.scg.rudy.model.pojo.byCustomer;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("customer_tax_no")
	private String customerTaxNo;

	@SerializedName("customer_phone_extension")
	private String customerPhoneExtension;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("customer_type_txt")
	private String customerTypeTxt;

	@SerializedName("pic")
	private String pic;

	@SerializedName("process_project")
	private String processProject;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("sum_trans")
	private String sumTrans;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("latest_transaction")
	private LatestTransaction latestTransaction;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("customer_grade")
	private String customerGrade;

	@SerializedName("customer_code")
	private String customerCode;

	@SerializedName("customer_company_name")
	private String customerCompanyName;

	@SerializedName("latest_event")
	private LatestEvent latestEvent;

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setCustomerTaxNo(String customerTaxNo){
		this.customerTaxNo = customerTaxNo;
	}

	public String getCustomerTaxNo(){
		return customerTaxNo;
	}

	public void setCustomerPhoneExtension(String customerPhoneExtension){
		this.customerPhoneExtension = customerPhoneExtension;
	}

	public String getCustomerPhoneExtension(){
		return customerPhoneExtension;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setCustomerTypeTxt(String customerTypeTxt){
		this.customerTypeTxt = customerTypeTxt;
	}

	public String getCustomerTypeTxt(){
		return customerTypeTxt;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setProcessProject(String processProject){
		this.processProject = processProject;
	}

	public String getProcessProject(){
		return processProject;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setSumTrans(String sumTrans){
		this.sumTrans = sumTrans;
	}

	public String getSumTrans(){
		return sumTrans;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setLatestTransaction(LatestTransaction latestTransaction){
		this.latestTransaction = latestTransaction;
	}

	public LatestTransaction getLatestTransaction(){
		return latestTransaction;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setCustomerGrade(String customerGrade){
		this.customerGrade = customerGrade;
	}

	public String getCustomerGrade(){
		return customerGrade;
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getCustomerCode(){
		return customerCode;
	}

	public void setCustomerCompanyName(String customerCompanyName){
		this.customerCompanyName = customerCompanyName;
	}

	public String getCustomerCompanyName(){
		return customerCompanyName;
	}

	public void setLatestEvent(LatestEvent latestEvent){
		this.latestEvent = latestEvent;
	}

	public LatestEvent getLatestEvent(){
		return latestEvent;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"customer_type = '" + customerType + '\'' + 
			",customer_tax_no = '" + customerTaxNo + '\'' + 
			",customer_phone_extension = '" + customerPhoneExtension + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",customer_type_txt = '" + customerTypeTxt + '\'' + 
			",pic = '" + pic + '\'' + 
			",process_project = '" + processProject + '\'' + 
			",user_id = '" + userId + '\'' + 
			",sum_trans = '" + sumTrans + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",latest_transaction = '" + latestTransaction + '\'' + 
			",id = '" + id + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",customer_grade = '" + customerGrade + '\'' + 
			",customer_code = '" + customerCode + '\'' + 
			",customer_company_name = '" + customerCompanyName + '\'' + 
			",latest_event = '" + latestEvent + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.customerType);
		dest.writeString(this.customerTaxNo);
		dest.writeString(this.customerPhoneExtension);
		dest.writeString(this.customerPhone);
		dest.writeString(this.customerTypeTxt);
		dest.writeString(this.pic);
		dest.writeString(this.processProject);
		dest.writeString(this.userId);
		dest.writeString(this.sumTrans);
		dest.writeString(this.customerEmail);
		dest.writeString(this.unitBudget);
		dest.writeParcelable(this.latestTransaction, flags);
		dest.writeString(this.id);
		dest.writeString(this.customerName);
		dest.writeString(this.customerId);
		dest.writeString(this.customerGrade);
		dest.writeString(this.customerCode);
		dest.writeString(this.customerCompanyName);
		dest.writeParcelable(this.latestEvent, flags);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.customerType = in.readString();
		this.customerTaxNo = in.readString();
		this.customerPhoneExtension = in.readString();
		this.customerPhone = in.readString();
		this.customerTypeTxt = in.readString();
		this.pic = in.readString();
		this.processProject = in.readString();
		this.userId = in.readString();
		this.sumTrans = in.readString();
		this.customerEmail = in.readString();
		this.unitBudget = in.readString();
		this.latestTransaction = in.readParcelable(LatestTransaction.class.getClassLoader());
		this.id = in.readString();
		this.customerName = in.readString();
		this.customerId = in.readString();
		this.customerGrade = in.readString();
		this.customerCode = in.readString();
		this.customerCompanyName = in.readString();
		this.latestEvent = in.readParcelable(LatestEvent.class.getClassLoader());
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}