package com.scg.rudy.model.pojo.calendar_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("date")
	private String date;

	@SerializedName("dayName")
	private String dayName;

	@SerializedName("year")
	private String year;

	@SerializedName("monthName")
	private String monthName;

	@SerializedName("end_time")
	private String endTime;

	@SerializedName("description")
	private String description;

	@SerializedName("project")
	private Project project;

	@SerializedName("type")
	private String type;

	@SerializedName("title")
	private String title;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("start_time")
	private String startTime;

	@SerializedName("month")
	private String month;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("day")
	private String day;

	@SerializedName("type_txt")
	private String typeTxt;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setDayName(String dayName){
		this.dayName = dayName;
	}

	public String getDayName(){
		return dayName;
	}

	public void setYear(String year){
		this.year = year;
	}

	public String getYear(){
		return year;
	}

	public void setMonthName(String monthName){
		this.monthName = monthName;
	}

	public String getMonthName(){
		return monthName;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setProject(Project project){
		this.project = project;
	}

	public Project getProject(){
		return project;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setMonth(String month){
		this.month = month;
	}

	public String getMonth(){
		return month;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	public void setTypeTxt(String typeTxt){
		this.typeTxt = typeTxt;
	}

	public String getTypeTxt(){
		return typeTxt;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"date = '" + date + '\'' + 
			",dayName = '" + dayName + '\'' + 
			",year = '" + year + '\'' + 
			",monthName = '" + monthName + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",description = '" + description + '\'' + 
			",project = '" + project + '\'' + 
			",type = '" + type + '\'' + 
			",title = '" + title + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",start_time = '" + startTime + '\'' + 
			",month = '" + month + '\'' + 
			",user_id = '" + userId + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",day = '" + day + '\'' + 
			",type_txt = '" + typeTxt + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.date);
		dest.writeString(this.dayName);
		dest.writeString(this.year);
		dest.writeString(this.monthName);
		dest.writeString(this.endTime);
		dest.writeString(this.description);
		dest.writeParcelable(this.project, flags);
		dest.writeString(this.type);
		dest.writeString(this.title);
		dest.writeString(this.shopId);
		dest.writeString(this.startTime);
		dest.writeString(this.month);
		dest.writeString(this.userId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.day);
		dest.writeString(this.typeTxt);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.date = in.readString();
		this.dayName = in.readString();
		this.year = in.readString();
		this.monthName = in.readString();
		this.endTime = in.readString();
		this.description = in.readString();
		this.project = in.readParcelable(Project.class.getClassLoader());
		this.type = in.readString();
		this.title = in.readString();
		this.shopId = in.readString();
		this.startTime = in.readString();
		this.month = in.readString();
		this.userId = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.day = in.readString();
		this.typeTxt = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}