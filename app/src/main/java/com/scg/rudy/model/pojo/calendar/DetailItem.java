package com.scg.rudy.model.pojo.calendar;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class DetailItem implements Parcelable {

	@SerializedName("start_time")
	private String startTime;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("end_time")
	private String endTime;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private String id;

	@SerializedName("type")
	private String type;

	@SerializedName("title")
	private String title;

	@SerializedName("type_txt")
	private String typeTxt;

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setTypeTxt(String typeTxt){
		this.typeTxt = typeTxt;
	}

	public String getTypeTxt(){
		return typeTxt;
	}

	@Override
 	public String toString(){
		return 
			"DetailItem{" + 
			"start_time = '" + startTime + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",type = '" + type + '\'' + 
			",title = '" + title + '\'' + 
			",type_txt = '" + typeTxt + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.startTime);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.endTime);
		dest.writeString(this.description);
		dest.writeString(this.id);
		dest.writeString(this.type);
		dest.writeString(this.title);
		dest.writeString(this.typeTxt);
	}

	public DetailItem() {
	}

	protected DetailItem(Parcel in) {
		this.startTime = in.readString();
		this.lastUpdate = in.readString();
		this.endTime = in.readString();
		this.description = in.readString();
		this.id = in.readString();
		this.type = in.readString();
		this.title = in.readString();
		this.typeTxt = in.readString();
	}

	public static final Parcelable.Creator<DetailItem> CREATOR = new Parcelable.Creator<DetailItem>() {
		@Override
		public DetailItem createFromParcel(Parcel source) {
			return new DetailItem(source);
		}

		@Override
		public DetailItem[] newArray(int size) {
			return new DetailItem[size];
		}
	};
}