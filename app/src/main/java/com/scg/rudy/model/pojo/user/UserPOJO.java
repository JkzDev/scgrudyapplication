package com.scg.rudy.model.pojo.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class UserPOJO implements Parcelable {


    static public UserPOJO create(String serializedData) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(serializedData, UserPOJO.class);
    }

    public String serialize() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UserPOJO{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.items, flags);
        dest.writeInt(this.status);
    }

    public UserPOJO() {
    }

    protected UserPOJO(Parcel in) {
        this.items = in.readParcelable(Items.class.getClassLoader());
        this.status = in.readInt();
    }

    public static final Parcelable.Creator<UserPOJO> CREATOR = new Parcelable.Creator<UserPOJO>() {
        @Override
        public UserPOJO createFromParcel(Parcel source) {
            return new UserPOJO(source);
        }

        @Override
        public UserPOJO[] newArray(int size) {
            return new UserPOJO[size];
        }
    };
}