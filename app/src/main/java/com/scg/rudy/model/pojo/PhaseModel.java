package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 15/2/2018 AD.
 */

public class PhaseModel {
    String id;
    String name;

    public PhaseModel(
            String id,
            String name){
        super();
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
