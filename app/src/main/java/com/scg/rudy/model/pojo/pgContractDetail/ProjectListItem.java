package com.scg.rudy.model.pojo.pgContractDetail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectListItem implements Parcelable {

	@SerializedName("customer_line")
	public String customerLine;

	@SerializedName("pr")
	public List<PrItem> pr;

	@SerializedName("phaseprocess")
	public List<PhaseprocessItem> phaseprocess;

	@SerializedName("rank_name")
	public String rankName;

	@SerializedName("project_owner_phone")
	public String projectOwnerPhone;

	@SerializedName("customer_pic")
	public String customerPic;

	@SerializedName("customer_note")
	public String customerNote;

	@SerializedName("project_name")
	public String projectName;

	@SerializedName("added_datetime")
	public String addedDatetime;

	@SerializedName("house_owner_phone")
	public String houseOwnerPhone;

	@SerializedName("phase_work_progress")
	public String phaseWorkProgress;

	@SerializedName("biz_pr")
	public List<Object> bizPr;

	@SerializedName("last_update")
	public String lastUpdate;

	@SerializedName("unit_budget")
	public String unitBudget;

	@SerializedName("rank")
	public String rank;

	@SerializedName("id")
	public String id;

	@SerializedName("customer_code")
	public String customerCode;

	@SerializedName("gallery")
	public List<Object> gallery;

	@SerializedName("add_by_user")
	public String addByUser;

	@SerializedName("customer_phone_extension")
	public String customerPhoneExtension;

	@SerializedName("project_address")
	public String projectAddress;

	@SerializedName("customer_phone")
	public String customerPhone;

	@SerializedName("project_owner_line")
	public String projectOwnerLine;

	@SerializedName("project_stories")
	public String projectStories;

	@SerializedName("work_progress")
	public String workProgress;

	@SerializedName("opportunity")
	public String opportunity;

	@SerializedName("project_owner_note")
	public String projectOwnerNote;

	@SerializedName("house_owner_phone_extension")
	public String houseOwnerPhoneExtension;

	@SerializedName("shop_id")
	public String shopId;

	@SerializedName("champ_customer_code")
	public String champCustomerCode;

	@SerializedName("project_owner_name")
	public String projectOwnerName;

	@SerializedName("lat_lng")
	public String latLng;

	@SerializedName("user_id")
	public String userId;

	@SerializedName("house_owner_note")
	public String houseOwnerNote;

	@SerializedName("customer_email")
	public String customerEmail;

	@SerializedName("house_owner_line")
	public String houseOwnerLine;

	@SerializedName("unit_area")
	public String unitArea;

	@SerializedName("phase_name")
	public String phaseName;

	@SerializedName("project_type_name")
	public String projectTypeName;

	@SerializedName("status")
	public String status;

	@SerializedName("trans")
	public String trans;

	@SerializedName("customer_type")
	public String customerType;

	@SerializedName("is_private")
	public String isPrivate;

	@SerializedName("note")
	public List<Object> note;

	@SerializedName("project_owner_phone_extension")
	public String projectOwnerPhoneExtension;

	@SerializedName("phase_id")
	public String phaseId;

	@SerializedName("units")
	public String units;

	@SerializedName("customer_type_txt")
	public String customerTypeTxt;

	@SerializedName("house_owner_name")
	public String houseOwnerName;

	@SerializedName("sell_complete_percent")
	public int sellCompletePercent;

	@SerializedName("customer_company")
	public String customerCompany;

	@SerializedName("opportunity_n")
	public int opportunityN;

	@SerializedName("project_type_id")
	public String projectTypeId;

	@SerializedName("customer_tax_no")
	public String customerTaxNo;

	@SerializedName("phase_pic")
	public String phasePic;

	@SerializedName("trans_n")
	public String transN;

	@SerializedName("province_id")
	public String provinceId;

	@SerializedName("audios")
	public List<Object> audios;

	@SerializedName("customer_name")
	public String customerName;

	@SerializedName("owner_type")
	public String ownerType;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.customerLine);
		dest.writeList(this.pr);
		dest.writeList(this.phaseprocess);
		dest.writeString(this.rankName);
		dest.writeString(this.projectOwnerPhone);
		dest.writeString(this.customerPic);
		dest.writeString(this.customerNote);
		dest.writeString(this.projectName);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.houseOwnerPhone);
		dest.writeString(this.phaseWorkProgress);
		dest.writeList(this.bizPr);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.unitBudget);
		dest.writeString(this.rank);
		dest.writeString(this.id);
		dest.writeString(this.customerCode);
		dest.writeList(this.gallery);
		dest.writeString(this.addByUser);
		dest.writeString(this.customerPhoneExtension);
		dest.writeString(this.projectAddress);
		dest.writeString(this.customerPhone);
		dest.writeString(this.projectOwnerLine);
		dest.writeString(this.projectStories);
		dest.writeString(this.workProgress);
		dest.writeString(this.opportunity);
		dest.writeString(this.projectOwnerNote);
		dest.writeString(this.houseOwnerPhoneExtension);
		dest.writeString(this.shopId);
		dest.writeString(this.champCustomerCode);
		dest.writeString(this.projectOwnerName);
		dest.writeString(this.latLng);
		dest.writeString(this.userId);
		dest.writeString(this.houseOwnerNote);
		dest.writeString(this.customerEmail);
		dest.writeString(this.houseOwnerLine);
		dest.writeString(this.unitArea);
		dest.writeString(this.phaseName);
		dest.writeString(this.projectTypeName);
		dest.writeString(this.status);
		dest.writeString(this.trans);
		dest.writeString(this.customerType);
		dest.writeString(this.isPrivate);
		dest.writeList(this.note);
		dest.writeString(this.projectOwnerPhoneExtension);
		dest.writeString(this.phaseId);
		dest.writeString(this.units);
		dest.writeString(this.customerTypeTxt);
		dest.writeString(this.houseOwnerName);
		dest.writeInt(this.sellCompletePercent);
		dest.writeString(this.customerCompany);
		dest.writeInt(this.opportunityN);
		dest.writeString(this.projectTypeId);
		dest.writeString(this.customerTaxNo);
		dest.writeString(this.phasePic);
		dest.writeString(this.transN);
		dest.writeString(this.provinceId);
		dest.writeList(this.audios);
		dest.writeString(this.customerName);
		dest.writeString(this.ownerType);
	}

	public ProjectListItem() {
	}

	protected ProjectListItem(Parcel in) {
		this.customerLine = in.readString();
		this.pr = new ArrayList<PrItem>();
		in.readList(this.pr, PrItem.class.getClassLoader());
		this.phaseprocess = new ArrayList<PhaseprocessItem>();
		in.readList(this.phaseprocess, PhaseprocessItem.class.getClassLoader());
		this.rankName = in.readString();
		this.projectOwnerPhone = in.readString();
		this.customerPic = in.readString();
		this.customerNote = in.readString();
		this.projectName = in.readString();
		this.addedDatetime = in.readString();
		this.houseOwnerPhone = in.readString();
		this.phaseWorkProgress = in.readString();
		this.bizPr = new ArrayList<Object>();
		in.readList(this.bizPr, Object.class.getClassLoader());
		this.lastUpdate = in.readString();
		this.unitBudget = in.readString();
		this.rank = in.readString();
		this.id = in.readString();
		this.customerCode = in.readString();
		this.gallery = new ArrayList<Object>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.addByUser = in.readString();
		this.customerPhoneExtension = in.readString();
		this.projectAddress = in.readString();
		this.customerPhone = in.readString();
		this.projectOwnerLine = in.readString();
		this.projectStories = in.readString();
		this.workProgress = in.readString();
		this.opportunity = in.readString();
		this.projectOwnerNote = in.readString();
		this.houseOwnerPhoneExtension = in.readString();
		this.shopId = in.readString();
		this.champCustomerCode = in.readString();
		this.projectOwnerName = in.readString();
		this.latLng = in.readString();
		this.userId = in.readString();
		this.houseOwnerNote = in.readString();
		this.customerEmail = in.readString();
		this.houseOwnerLine = in.readString();
		this.unitArea = in.readString();
		this.phaseName = in.readString();
		this.projectTypeName = in.readString();
		this.status = in.readString();
		this.trans = in.readString();
		this.customerType = in.readString();
		this.isPrivate = in.readString();
		this.note = new ArrayList<Object>();
		in.readList(this.note, Object.class.getClassLoader());
		this.projectOwnerPhoneExtension = in.readString();
		this.phaseId = in.readString();
		this.units = in.readString();
		this.customerTypeTxt = in.readString();
		this.houseOwnerName = in.readString();
		this.sellCompletePercent = in.readInt();
		this.customerCompany = in.readString();
		this.opportunityN = in.readInt();
		this.projectTypeId = in.readString();
		this.customerTaxNo = in.readString();
		this.phasePic = in.readString();
		this.transN = in.readString();
		this.provinceId = in.readString();
		this.audios = new ArrayList<Object>();
		in.readList(this.audios, Object.class.getClassLoader());
		this.customerName = in.readString();
		this.ownerType = in.readString();
	}

	public static final Creator<ProjectListItem> CREATOR = new Creator<ProjectListItem>() {
		@Override
		public ProjectListItem createFromParcel(Parcel source) {
			return new ProjectListItem(source);
		}

		@Override
		public ProjectListItem[] newArray(int size) {
			return new ProjectListItem[size];
		}
	};
}