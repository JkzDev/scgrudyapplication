package com.scg.rudy.model.pojo.byCustomer;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class LatestEvent implements Parcelable {

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("status")
	private String status;

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LatestEvent{" + 
			"last_update = '" + lastUpdate + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.lastUpdate);
		dest.writeString(this.status);
	}

	public LatestEvent() {
	}

	protected LatestEvent(Parcel in) {
		this.lastUpdate = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<LatestEvent> CREATOR = new Parcelable.Creator<LatestEvent>() {
		@Override
		public LatestEvent createFromParcel(Parcel source) {
			return new LatestEvent(source);
		}

		@Override
		public LatestEvent[] newArray(int size) {
			return new LatestEvent[size];
		}
	};
}