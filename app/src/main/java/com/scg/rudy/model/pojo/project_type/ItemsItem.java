package com.scg.rudy.model.pojo.project_type;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("phases")
	private List<PhasesItem> phases;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPhases(List<PhasesItem> phases){
		this.phases = phases;
	}

	public List<PhasesItem> getPhases(){
		return phases;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",phases = '" + phases + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
		dest.writeList(this.phases);
	}

	public ItemsItem(String id,String name,List<PhasesItem> phases) {
		this.id = id;
		this.name = name;
		this.phases = phases;

	}

	protected ItemsItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
		this.phases = new ArrayList<PhasesItem>();
		in.readList(this.phases, PhasesItem.class.getClassLoader());
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}