package com.scg.rudy.model.pojo.promotionDetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("promotion_code")
	private String promotionCode;

	@SerializedName("min_qty")
	private String minQty;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("product_unit")
	private String productUnit;

	@SerializedName("expired_date")
	private String expiredDate;

	@SerializedName("limit_qty")
	private String limitQty;

	@SerializedName("remain_day")
	private int remainDay;

	@SerializedName("promotion_description")
	private String promotionDescription;

	@SerializedName("promotion_conditions")
	private String promotionConditions;

	@SerializedName("commission")
	private String commission;

	@SerializedName("remain_qty")
	private String remainQty;

	@SerializedName("sku")
	private String sku;

	@SerializedName("start_date")
	private String startDate ;

	public void setPromotionCode(String promotionCode){
		this.promotionCode = promotionCode;
	}

	public String getPromotionCode(){
		return promotionCode;
	}

	public void setMinQty(String minQty){
		this.minQty = minQty;
	}

	public String getMinQty(){
		return minQty;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setProductUnit(String productUnit){
		this.productUnit = productUnit;
	}

	public String getProductUnit(){
		return productUnit;
	}

	public void setExpiredDate(String expiredDate){
		this.expiredDate = expiredDate;
	}

	public String getExpiredDate(){
		return expiredDate;
	}

	public void setLimitQty(String limitQty){
		this.limitQty = limitQty;
	}

	public String getLimitQty(){
		return limitQty;
	}

	public void setRemainDay(int remainDay){
		this.remainDay = remainDay;
	}

	public int getRemainDay(){
		return remainDay;
	}

	public void setPromotionDescription(String promotionDescription){
		this.promotionDescription = promotionDescription;
	}

	public String getPromotionDescription(){
		return promotionDescription;
	}

	public void setPromotionConditions(String promotionConditions){
		this.promotionConditions = promotionConditions;
	}

	public String getPromotionConditions(){
		return promotionConditions;
	}

	public void setCommission(String commission){
		this.commission = commission;
	}

	public String getCommission(){
		return commission;
	}

	public void setRemainQty(String remainQty){
		this.remainQty = remainQty;
	}

	public String getRemainQty(){
		return remainQty;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"promotion_code = '" + promotionCode + '\'' + 
			",min_qty = '" + minQty + '\'' + 
			",product_name = '" + productName + '\'' + 
			",product_unit = '" + productUnit + '\'' + 
			",expired_date = '" + expiredDate + '\'' + 
			",limit_qty = '" + limitQty + '\'' + 
			",remain_day = '" + remainDay + '\'' + 
			",promotion_description = '" + promotionDescription + '\'' + 
			",promotion_conditions = '" + promotionConditions + '\'' + 
			",commission = '" + commission + '\'' + 
			",remain_qty = '" + remainQty + '\'' + 
			",sku = '" + sku + '\'' + 
			",start_date = '" + startDate + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.promotionCode);
		dest.writeString(this.minQty);
		dest.writeString(this.productName);
		dest.writeString(this.productUnit);
		dest.writeString(this.expiredDate);
		dest.writeString(this.limitQty);
		dest.writeInt(this.remainDay);
		dest.writeString(this.promotionDescription);
		dest.writeString(this.promotionConditions);
		dest.writeString(this.commission);
		dest.writeString(this.remainQty);
		dest.writeString(this.sku);
		dest.writeString(this.startDate);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.promotionCode = in.readString();
		this.minQty = in.readString();
		this.productName = in.readString();
		this.productUnit = in.readString();
		this.expiredDate = in.readString();
		this.limitQty = in.readString();
		this.remainDay = in.readInt();
		this.promotionDescription = in.readString();
		this.promotionConditions = in.readString();
		this.commission = in.readString();
		this.remainQty = in.readString();
		this.sku = in.readString();
		this.startDate = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}