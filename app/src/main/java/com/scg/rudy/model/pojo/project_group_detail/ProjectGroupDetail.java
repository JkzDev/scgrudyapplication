package com.scg.rudy.model.pojo.project_group_detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ProjectGroupDetail implements Parcelable {

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProjectGroupDetail{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.items, flags);
		dest.writeInt(this.status);
	}

	public ProjectGroupDetail() {
	}

	protected ProjectGroupDetail(Parcel in) {
		this.items = in.readParcelable(Items.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ProjectGroupDetail> CREATOR = new Parcelable.Creator<ProjectGroupDetail>() {
		@Override
		public ProjectGroupDetail createFromParcel(Parcel source) {
			return new ProjectGroupDetail(source);
		}

		@Override
		public ProjectGroupDetail[] newArray(int size) {
			return new ProjectGroupDetail[size];
		}
	};
}