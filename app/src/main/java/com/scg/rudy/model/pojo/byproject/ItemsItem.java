package com.scg.rudy.model.pojo.byproject;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("is_private")
	private String isPrivate;

	@SerializedName("rank_name")
	private String rankName;

	@SerializedName("distance")
	private int distance;

	@SerializedName("project_owner_phone")
	private String projectOwnerPhone;

	@SerializedName("rating")
	private String rating;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("pic")
	private String pic;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("import_type_name")
	private String importTypeName;

	@SerializedName("house_owner_name")
	private String houseOwnerName;

	@SerializedName("house_owner_phone")
	private String houseOwnerPhone;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("rank")
	private String rank;

	@SerializedName("id")
	private String id;

	@SerializedName("import_type")
	private String importType;

	@SerializedName("assign_users")
	private String assignUsers;

	@SerializedName("project_type_id")
	private String projectTypeId;

	@SerializedName("last_purchase")
	private String lastPurchase;

	@SerializedName("project_address")
	private String projectAddress;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("project_stories")
	private String projectStories;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("work_progress")
	private String workProgress;

	@SerializedName("customer_phone_list")
	private List<CustomerPhoneListItem> customerPhoneList;

	@SerializedName("added_datetime_display")
	private String addedDatetimeDisplay;

	@SerializedName("has_transaction")
	private String hasTransaction;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("project_owner_name")
	private String projectOwnerName;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("have_gallery")
	private int haveGallery;

	@SerializedName("province_id")
	private String provinceId;

	@SerializedName("last_update_txt")
	private String lastUpdateTxt;

	@SerializedName("unit_area")
	private String unitArea;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("phase_name")
	private String phaseName;

	@SerializedName("project_type_name")
	private String projectTypeName;

	@SerializedName("status")
	private String status;

	@SerializedName("trans")
	private String trans;

	public void setIsPrivate(String isPrivate){
		this.isPrivate = isPrivate;
	}

	public String getIsPrivate(){
		return isPrivate;
	}

	public void setRankName(String rankName){
		this.rankName = rankName;
	}

	public String getRankName(){
		return rankName;
	}

	public void setDistance(int distance){
		this.distance = distance;
	}

	public int getDistance(){
		return distance;
	}

	public void setProjectOwnerPhone(String projectOwnerPhone){
		this.projectOwnerPhone = projectOwnerPhone;
	}

	public String getProjectOwnerPhone(){
		return projectOwnerPhone;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setImportTypeName(String importTypeName){
		this.importTypeName = importTypeName;
	}

	public String getImportTypeName(){
		return importTypeName;
	}

	public void setHouseOwnerName(String houseOwnerName){
		this.houseOwnerName = houseOwnerName;
	}

	public String getHouseOwnerName(){
		return houseOwnerName;
	}

	public void setHouseOwnerPhone(String houseOwnerPhone){
		this.houseOwnerPhone = houseOwnerPhone;
	}

	public String getHouseOwnerPhone(){
		return houseOwnerPhone;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setImportType(String importType){
		this.importType = importType;
	}

	public String getImportType(){
		return importType;
	}

	public void setAssignUsers(String assignUsers){
		this.assignUsers = assignUsers;
	}

	public String getAssignUsers(){
		return assignUsers;
	}

	public void setProjectTypeId(String projectTypeId){
		this.projectTypeId = projectTypeId;
	}

	public String getProjectTypeId(){
		return projectTypeId;
	}

	public void setLastPurchase(String lastPurchase){
		this.lastPurchase = lastPurchase;
	}

	public String getLastPurchase(){
		return lastPurchase;
	}

	public void setProjectAddress(String projectAddress){
		this.projectAddress = projectAddress;
	}

	public String getProjectAddress(){
		return projectAddress;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setProjectStories(String projectStories){
		this.projectStories = projectStories;
	}

	public String getProjectStories(){
		return projectStories;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setWorkProgress(String workProgress){
		this.workProgress = workProgress;
	}

	public String getWorkProgress(){
		return workProgress;
	}

	public void setCustomerPhoneList(List<CustomerPhoneListItem> customerPhoneList){
		this.customerPhoneList = customerPhoneList;
	}

	public List<CustomerPhoneListItem> getCustomerPhoneList(){
		return customerPhoneList;
	}

	public void setAddedDatetimeDisplay(String addedDatetimeDisplay){
		this.addedDatetimeDisplay = addedDatetimeDisplay;
	}

	public String getAddedDatetimeDisplay(){
		return addedDatetimeDisplay;
	}

	public void setHasTransaction(String hasTransaction){
		this.hasTransaction = hasTransaction;
	}

	public String getHasTransaction(){
		return hasTransaction;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setProjectOwnerName(String projectOwnerName){
		this.projectOwnerName = projectOwnerName;
	}

	public String getProjectOwnerName(){
		return projectOwnerName;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setHaveGallery(int haveGallery){
		this.haveGallery = haveGallery;
	}

	public int getHaveGallery(){
		return haveGallery;
	}

	public void setProvinceId(String provinceId){
		this.provinceId = provinceId;
	}

	public String getProvinceId(){
		return provinceId;
	}

	public void setLastUpdateTxt(String lastUpdateTxt){
		this.lastUpdateTxt = lastUpdateTxt;
	}

	public String getLastUpdateTxt(){
		return lastUpdateTxt;
	}

	public void setUnitArea(String unitArea){
		this.unitArea = unitArea;
	}

	public String getUnitArea(){
		return unitArea;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	public void setProjectTypeName(String projectTypeName){
		this.projectTypeName = projectTypeName;
	}

	public String getProjectTypeName(){
		return projectTypeName;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setTrans(String trans){
		this.trans = trans;
	}

	public String getTrans(){
		return trans;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"is_private = '" + isPrivate + '\'' + 
			",rank_name = '" + rankName + '\'' + 
			",distance = '" + distance + '\'' + 
			",project_owner_phone = '" + projectOwnerPhone + '\'' + 
			",rating = '" + rating + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",pic = '" + pic + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",import_type_name = '" + importTypeName + '\'' + 
			",house_owner_name = '" + houseOwnerName + '\'' + 
			",house_owner_phone = '" + houseOwnerPhone + '\'' + 
			",added_by = '" + addedBy + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",rank = '" + rank + '\'' + 
			",id = '" + id + '\'' + 
			",import_type = '" + importType + '\'' + 
			",assign_users = '" + assignUsers + '\'' + 
			",project_type_id = '" + projectTypeId + '\'' + 
			",last_purchase = '" + lastPurchase + '\'' + 
			",project_address = '" + projectAddress + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",project_stories = '" + projectStories + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",work_progress = '" + workProgress + '\'' + 
			",customer_phone_list = '" + customerPhoneList + '\'' + 
			",added_datetime_display = '" + addedDatetimeDisplay + '\'' + 
			",has_transaction = '" + hasTransaction + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",project_owner_name = '" + projectOwnerName + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",have_gallery = '" + haveGallery + '\'' + 
			",province_id = '" + provinceId + '\'' + 
			",last_update_txt = '" + lastUpdateTxt + '\'' + 
			",unit_area = '" + unitArea + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			",project_type_name = '" + projectTypeName + '\'' + 
			",status = '" + status + '\'' + 
			",trans = '" + trans + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.isPrivate);
		dest.writeString(this.rankName);
		dest.writeInt(this.distance);
		dest.writeString(this.projectOwnerPhone);
		dest.writeString(this.rating);
		dest.writeString(this.phaseId);
		dest.writeString(this.pic);
		dest.writeString(this.projectName);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.importTypeName);
		dest.writeString(this.houseOwnerName);
		dest.writeString(this.houseOwnerPhone);
		dest.writeString(this.addedBy);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.unitBudget);
		dest.writeString(this.rank);
		dest.writeString(this.id);
		dest.writeString(this.importType);
		dest.writeString(this.assignUsers);
		dest.writeString(this.projectTypeId);
		dest.writeString(this.lastPurchase);
		dest.writeString(this.projectAddress);
		dest.writeString(this.customerPhone);
		dest.writeString(this.projectStories);
		dest.writeString(this.opportunity);
		dest.writeString(this.workProgress);
		dest.writeList(this.customerPhoneList);
		dest.writeString(this.addedDatetimeDisplay);
		dest.writeString(this.hasTransaction);
		dest.writeString(this.statusTxt);
		dest.writeString(this.shopId);
		dest.writeString(this.projectOwnerName);
		dest.writeString(this.latLng);
		dest.writeInt(this.haveGallery);
		dest.writeString(this.provinceId);
		dest.writeString(this.lastUpdateTxt);
		dest.writeString(this.unitArea);
		dest.writeString(this.customerName);
		dest.writeString(this.phaseName);
		dest.writeString(this.projectTypeName);
		dest.writeString(this.status);
		dest.writeString(this.trans);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.isPrivate = in.readString();
		this.rankName = in.readString();
		this.distance = in.readInt();
		this.projectOwnerPhone = in.readString();
		this.rating = in.readString();
		this.phaseId = in.readString();
		this.pic = in.readString();
		this.projectName = in.readString();
		this.addedDatetime = in.readString();
		this.importTypeName = in.readString();
		this.houseOwnerName = in.readString();
		this.houseOwnerPhone = in.readString();
		this.addedBy = in.readString();
		this.lastUpdate = in.readString();
		this.unitBudget = in.readString();
		this.rank = in.readString();
		this.id = in.readString();
		this.importType = in.readString();
		this.assignUsers = in.readString();
		this.projectTypeId = in.readString();
		this.lastPurchase = in.readString();
		this.projectAddress = in.readString();
		this.customerPhone = in.readString();
		this.projectStories = in.readString();
		this.opportunity = in.readString();
		this.workProgress = in.readString();
		this.customerPhoneList = new ArrayList<CustomerPhoneListItem>();
		in.readList(this.customerPhoneList, CustomerPhoneListItem.class.getClassLoader());
		this.addedDatetimeDisplay = in.readString();
		this.hasTransaction = in.readString();
		this.statusTxt = in.readString();
		this.shopId = in.readString();
		this.projectOwnerName = in.readString();
		this.latLng = in.readString();
		this.haveGallery = in.readInt();
		this.provinceId = in.readString();
		this.lastUpdateTxt = in.readString();
		this.unitArea = in.readString();
		this.customerName = in.readString();
		this.phaseName = in.readString();
		this.projectTypeName = in.readString();
		this.status = in.readString();
		this.trans = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}