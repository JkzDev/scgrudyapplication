package com.scg.rudy.model.pojo.customer_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("project_overview")
	private List<Object> projectOverview;

	@SerializedName("customer_line")
	private String customerLine;

	@SerializedName("trading_overview")
	private List<Object> tradingOverview;

	@SerializedName("pic")
	private String pic;

	@SerializedName("customer_type_txt")
	private String customerTypeTxt;

	@SerializedName("transaction_list")
	private List<TransactionListItem> transactionList;

	@SerializedName("champ_customer_code")
	private String champCustomerCode;

	@SerializedName("phone")
	private String phone;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("credit_overview")
	private CreditOverview creditOverview;

	@SerializedName("project_list")
	private List<ProjectListItem> projectList;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("phone_extension")
	private String phoneExtension;

	@SerializedName("customer_grade")
	private String customerGrade;

	@SerializedName("customer_code")
	private String customerCode;

	@SerializedName("customer_company")
	private String customerCompany;

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setProjectOverview(List<Object> projectOverview){
		this.projectOverview = projectOverview;
	}

	public List<Object> getProjectOverview(){
		return projectOverview;
	}

	public void setCustomerLine(String customerLine){
		this.customerLine = customerLine;
	}

	public String getCustomerLine(){
		return customerLine;
	}

	public void setTradingOverview(List<Object> tradingOverview){
		this.tradingOverview = tradingOverview;
	}

	public List<Object> getTradingOverview(){
		return tradingOverview;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setCustomerTypeTxt(String customerTypeTxt){
		this.customerTypeTxt = customerTypeTxt;
	}

	public String getCustomerTypeTxt(){
		return customerTypeTxt;
	}

	public void setTransactionList(List<TransactionListItem> transactionList){
		this.transactionList = transactionList;
	}

	public List<TransactionListItem> getTransactionList(){
		return transactionList;
	}

	public void setChampCustomerCode(String champCustomerCode){
		this.champCustomerCode = champCustomerCode;
	}

	public String getChampCustomerCode(){
		return champCustomerCode;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setCreditOverview(CreditOverview creditOverview){
		this.creditOverview = creditOverview;
	}

	public CreditOverview getCreditOverview(){
		return creditOverview;
	}

	public void setProjectList(List<ProjectListItem> projectList){
		this.projectList = projectList;
	}

	public List<ProjectListItem> getProjectList(){
		return projectList;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setPhoneExtension(String phoneExtension){
		this.phoneExtension = phoneExtension;
	}

	public String getPhoneExtension(){
		return phoneExtension;
	}

	public void setCustomerGrade(String customerGrade){
		this.customerGrade = customerGrade;
	}

	public String getCustomerGrade(){
		return customerGrade;
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getCustomerCode(){
		return customerCode;
	}

	public void setCustomerCompany(String customerCompany){
		this.customerCompany = customerCompany;
	}

	public String getCustomerCompany(){
		return customerCompany;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"customer_type = '" + customerType + '\'' + 
			",project_overview = '" + projectOverview + '\'' + 
			",customer_line = '" + customerLine + '\'' + 
			",trading_overview = '" + tradingOverview + '\'' + 
			",pic = '" + pic + '\'' + 
			",customer_type_txt = '" + customerTypeTxt + '\'' + 
			",transaction_list = '" + transactionList + '\'' + 
			",champ_customer_code = '" + champCustomerCode + '\'' + 
			",phone = '" + phone + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",credit_overview = '" + creditOverview + '\'' + 
			",project_list = '" + projectList + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",phone_extension = '" + phoneExtension + '\'' + 
			",customer_grade = '" + customerGrade + '\'' + 
			",customer_code = '" + customerCode + '\'' + 
			",customer_company = '" + customerCompany + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.customerType);
		dest.writeList(this.projectOverview);
		dest.writeString(this.customerLine);
		dest.writeList(this.tradingOverview);
		dest.writeString(this.pic);
		dest.writeString(this.customerTypeTxt);
		dest.writeList(this.transactionList);
		dest.writeString(this.champCustomerCode);
		dest.writeString(this.phone);
		dest.writeString(this.customerEmail);
		dest.writeParcelable(this.creditOverview, flags);
		dest.writeList(this.projectList);
		dest.writeString(this.customerName);
		dest.writeString(this.phoneExtension);
		dest.writeString(this.customerGrade);
		dest.writeString(this.customerCode);
		dest.writeString(this.customerCompany);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.customerType = in.readString();
		this.projectOverview = new ArrayList<Object>();
		in.readList(this.projectOverview, Object.class.getClassLoader());
		this.customerLine = in.readString();
		this.tradingOverview = new ArrayList<Object>();
		in.readList(this.tradingOverview, Object.class.getClassLoader());
		this.pic = in.readString();
		this.customerTypeTxt = in.readString();
		this.transactionList = new ArrayList<TransactionListItem>();
		in.readList(this.transactionList, TransactionListItem.class.getClassLoader());
		this.champCustomerCode = in.readString();
		this.phone = in.readString();
		this.customerEmail = in.readString();
		this.creditOverview = in.readParcelable(CreditOverview.class.getClassLoader());
		this.projectList = new ArrayList<ProjectListItem>();
		in.readList(this.projectList, ProjectListItem.class.getClassLoader());
		this.customerName = in.readString();
		this.phoneExtension = in.readString();
		this.customerGrade = in.readString();
		this.customerCode = in.readString();
		this.customerCompany = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}