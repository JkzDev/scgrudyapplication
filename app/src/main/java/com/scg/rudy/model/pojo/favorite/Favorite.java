package com.scg.rudy.model.pojo.favorite;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Favorite implements Parcelable {

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Favorite{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.items);
		dest.writeInt(this.status);
	}

	public Favorite() {
	}

	protected Favorite(Parcel in) {
		this.items = new ArrayList<ItemsItem>();
		in.readList(this.items, ItemsItem.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<Favorite> CREATOR = new Parcelable.Creator<Favorite>() {
		@Override
		public Favorite createFromParcel(Parcel source) {
			return new Favorite(source);
		}

		@Override
		public Favorite[] newArray(int size) {
			return new Favorite[size];
		}
	};
}