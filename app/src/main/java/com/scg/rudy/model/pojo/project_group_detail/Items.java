package com.scg.rudy.model.pojo.project_group_detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("customer_line")
	private String customerLine;

	@SerializedName("developer_name")
	private String developerName;

	@SerializedName("customer_note")
	private String customerNote;

	@SerializedName("project_group_contact")
	private List<ProjectGroupContactItem> projectGroupContact;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("project_group_list_name")
	private String projectGroupListName;

	@SerializedName("sum_units")
	private String sumUnits;

	@SerializedName("sum_trans")
	private String sumTrans;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_company")
	private String customerCompany;

	@SerializedName("customer_code")
	private String customerCode;

	@SerializedName("gallery")
	private List<String> gallery;

	@SerializedName("num_proj")
	private int numProj;

	@SerializedName("start_date")
	private String startDate;

	@SerializedName("customer_tax_no")
	private String customerTaxNo;

	@SerializedName("project_group_list_id")
	private String projectGroupListId;

	@SerializedName("customer_phone_extension")
	private String customerPhoneExtension;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("sum_unit_budget")
	private String sumUnitBudget;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("champ_customer_code")
	private String champCustomerCode;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("name")
	private String name;

	@SerializedName("project_list")
	private List<ProjectListItem> projectList;

	@SerializedName("sum_opp")
	private String sumOpp;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("status")
	private int status;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setCustomerLine(String customerLine){
		this.customerLine = customerLine;
	}

	public String getCustomerLine(){
		return customerLine;
	}

	public void setDeveloperName(String developerName){
		this.developerName = developerName;
	}

	public String getDeveloperName(){
		return developerName;
	}

	public void setCustomerNote(String customerNote){
		this.customerNote = customerNote;
	}

	public String getCustomerNote(){
		return customerNote;
	}

	public void setProjectGroupContact(List<ProjectGroupContactItem> projectGroupContact){
		this.projectGroupContact = projectGroupContact;
	}

	public List<ProjectGroupContactItem> getProjectGroupContact(){
		return projectGroupContact;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setProjectGroupListName(String projectGroupListName){
		this.projectGroupListName = projectGroupListName;
	}

	public String getProjectGroupListName(){
		return projectGroupListName;
	}

	public void setSumUnits(String sumUnits){
		this.sumUnits = sumUnits;
	}

	public String getSumUnits(){
		return sumUnits;
	}

	public void setSumTrans(String sumTrans){
		this.sumTrans = sumTrans;
	}

	public String getSumTrans(){
		return sumTrans;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCustomerCompany(String customerCompany){
		this.customerCompany = customerCompany;
	}

	public String getCustomerCompany(){
		return customerCompany;
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getCustomerCode(){
		return customerCode;
	}

	public void setGallery(List<String> gallery){
		this.gallery = gallery;
	}

	public List<String> getGallery(){
		return gallery;
	}

	public void setNumProj(int numProj){
		this.numProj = numProj;
	}

	public int getNumProj(){
		return numProj;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	public void setCustomerTaxNo(String customerTaxNo){
		this.customerTaxNo = customerTaxNo;
	}

	public String getCustomerTaxNo(){
		return customerTaxNo;
	}

	public void setProjectGroupListId(String projectGroupListId){
		this.projectGroupListId = projectGroupListId;
	}

	public String getProjectGroupListId(){
		return projectGroupListId;
	}

	public void setCustomerPhoneExtension(String customerPhoneExtension){
		this.customerPhoneExtension = customerPhoneExtension;
	}

	public String getCustomerPhoneExtension(){
		return customerPhoneExtension;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setSumUnitBudget(String sumUnitBudget){
		this.sumUnitBudget = sumUnitBudget;
	}

	public String getSumUnitBudget(){
		return sumUnitBudget;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setChampCustomerCode(String champCustomerCode){
		this.champCustomerCode = champCustomerCode;
	}

	public String getChampCustomerCode(){
		return champCustomerCode;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setProjectList(List<ProjectListItem> projectList){
		this.projectList = projectList;
	}

	public List<ProjectListItem> getProjectList(){
		return projectList;
	}

	public void setSumOpp(String sumOpp){
		this.sumOpp = sumOpp;
	}

	public String getSumOpp(){
		return sumOpp;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"end_date = '" + endDate + '\'' + 
			",customer_type = '" + customerType + '\'' + 
			",customer_line = '" + customerLine + '\'' + 
			",developer_name = '" + developerName + '\'' + 
			",customer_note = '" + customerNote + '\'' + 
			",project_group_contact = '" + projectGroupContact + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",project_group_list_name = '" + projectGroupListName + '\'' + 
			",sum_units = '" + sumUnits + '\'' + 
			",sum_trans = '" + sumTrans + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",customer_company = '" + customerCompany + '\'' + 
			",customer_code = '" + customerCode + '\'' + 
			",gallery = '" + gallery + '\'' + 
			",num_proj = '" + numProj + '\'' + 
			",start_date = '" + startDate + '\'' + 
			",customer_tax_no = '" + customerTaxNo + '\'' + 
			",project_group_list_id = '" + projectGroupListId + '\'' + 
			",customer_phone_extension = '" + customerPhoneExtension + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",sum_unit_budget = '" + sumUnitBudget + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",champ_customer_code = '" + champCustomerCode + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",user_id = '" + userId + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",name = '" + name + '\'' + 
			",project_list = '" + projectList + '\'' + 
			",sum_opp = '" + sumOpp + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.endDate);
		dest.writeString(this.customerType);
		dest.writeString(this.customerLine);
		dest.writeString(this.developerName);
		dest.writeString(this.customerNote);
		dest.writeList(this.projectGroupContact);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.projectGroupListName);
		dest.writeString(this.sumUnits);
		dest.writeString(this.sumTrans);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.customerCompany);
		dest.writeString(this.customerCode);
		dest.writeList(this.gallery);
		dest.writeInt(this.numProj);
		dest.writeString(this.startDate);
		dest.writeString(this.customerTaxNo);
		dest.writeString(this.projectGroupListId);
		dest.writeString(this.customerPhoneExtension);
		dest.writeString(this.customerPhone);
		dest.writeString(this.statusTxt);
		dest.writeString(this.sumUnitBudget);
		dest.writeString(this.shopId);
		dest.writeString(this.champCustomerCode);
		dest.writeString(this.latLng);
		dest.writeString(this.userId);
		dest.writeString(this.customerEmail);
		dest.writeString(this.name);
		dest.writeList(this.projectList);
		dest.writeString(this.sumOpp);
		dest.writeString(this.customerName);
		dest.writeString(this.customerId);
		dest.writeInt(this.status);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.endDate = in.readString();
		this.customerType = in.readString();
		this.customerLine = in.readString();
		this.developerName = in.readString();
		this.customerNote = in.readString();
		this.projectGroupContact = new ArrayList<ProjectGroupContactItem>();
		in.readList(this.projectGroupContact, ProjectGroupContactItem.class.getClassLoader());
		this.addedDatetime = in.readString();
		this.projectGroupListName = in.readString();
		this.sumUnits = in.readString();
		this.sumTrans = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.customerCompany = in.readString();
		this.customerCode = in.readString();
		this.gallery = new ArrayList<String>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.numProj = in.readInt();
		this.startDate = in.readString();
		this.customerTaxNo = in.readString();
		this.projectGroupListId = in.readString();
		this.customerPhoneExtension = in.readString();
		this.customerPhone = in.readString();
		this.statusTxt = in.readString();
		this.sumUnitBudget = in.readString();
		this.shopId = in.readString();
		this.champCustomerCode = in.readString();
		this.latLng = in.readString();
		this.userId = in.readString();
		this.customerEmail = in.readString();
		this.name = in.readString();
		this.projectList = new ArrayList<ProjectListItem>();
		in.readList(this.projectList, ProjectListItem.class.getClassLoader());
		this.sumOpp = in.readString();
		this.customerName = in.readString();
		this.customerId = in.readString();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}