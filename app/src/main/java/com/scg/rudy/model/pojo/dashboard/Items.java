package com.scg.rudy.model.pojo.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("target_cross_selling")
	private String targetCrossSelling;

	@SerializedName("p_customer")
	private String pCustomer;

	@SerializedName("project")
	private String project;

	@SerializedName("target_project")
	private String targetProject;

	@SerializedName("all_status_chart")
	private AllStatusChart allStatusChart;

	@SerializedName("p_transaction")
	private String pTransaction;

	@SerializedName("p_cross_selling")
	private String pCrossSelling;

	@SerializedName("sum_trans")
	private SumTrans sumTrans;

	@SerializedName("cross_selling")
	private String crossSelling;

	@SerializedName("salechart")
	private List<SalechartItem> salechart;

	@SerializedName("p_project")
	private String pProject;

	@SerializedName("target_trans")
	private String targetTrans;

	@SerializedName("target_customer")
	private String targetCustomer;

	@SerializedName("transaction")
	private String transaction;

	@SerializedName("customer")
	private String customer;

	@SerializedName("favorite")
	private String favorite;

	@SerializedName("target_favorite")
	private String target_favorite;

	@SerializedName("p_favorite")
	private String p_favorite;

	public String getFavorite() {
		return favorite;
	}

	public void setFavorite(String favorite) {
		this.favorite = favorite;
	}

	public void setTargetCrossSelling(String targetCrossSelling){
		this.targetCrossSelling = targetCrossSelling;
	}

	public String getTarget_favorite() {
		return target_favorite;
	}

	public void setTarget_favorite(String target_favorite) {
		this.target_favorite = target_favorite;
	}

	public void setP_favorite(String p_favorite) {
		this.p_favorite = p_favorite;
	}

	public String getP_favorite() {
		return p_favorite;
	}

	public String getTargetCrossSelling(){
		return targetCrossSelling;
	}

	public void setPCustomer(String pCustomer){
		this.pCustomer = pCustomer;
	}

	public String getPCustomer(){
		return pCustomer;
	}

	public void setProject(String project){
		this.project = project;
	}

	public String getProject(){
		return project;
	}

	public void setTargetProject(String targetProject){
		this.targetProject = targetProject;
	}

	public String getTargetProject(){
		return targetProject;
	}

	public void setAllStatusChart(AllStatusChart allStatusChart){
		this.allStatusChart = allStatusChart;
	}

	public AllStatusChart getAllStatusChart(){
		return allStatusChart;
	}

	public void setPTransaction(String pTransaction){
		this.pTransaction = pTransaction;
	}

	public String getPTransaction(){
		return pTransaction;
	}

	public void setPCrossSelling(String pCrossSelling){
		this.pCrossSelling = pCrossSelling;
	}

	public String getPCrossSelling(){
		return pCrossSelling;
	}

	public void setSumTrans(SumTrans sumTrans){
		this.sumTrans = sumTrans;
	}

	public SumTrans getSumTrans(){
		return sumTrans;
	}

	public void setCrossSelling(String crossSelling){
		this.crossSelling = crossSelling;
	}

	public String getCrossSelling(){
		return crossSelling;
	}

	public void setSalechart(List<SalechartItem> salechart){
		this.salechart = salechart;
	}

	public List<SalechartItem> getSalechart(){
		return salechart;
	}

	public void setPProject(String pProject){
		this.pProject = pProject;
	}

	public String getPProject(){
		return pProject;
	}

	public void setTargetTrans(String targetTrans){
		this.targetTrans = targetTrans;
	}

	public String getTargetTrans(){
		return targetTrans;
	}

	public void setTargetCustomer(String targetCustomer){
		this.targetCustomer = targetCustomer;
	}

	public String getTargetCustomer(){
		return targetCustomer;
	}

	public void setTransaction(String transaction){
		this.transaction = transaction;
	}

	public String getTransaction(){
		return transaction;
	}

	public void setCustomer(String customer){
		this.customer = customer;
	}

	public String getCustomer(){
		return customer;
	}

	@Override
 	public String toString(){
		return 
			"projectDetailItems{" +
			"target_cross_selling = '" + targetCrossSelling + '\'' + 
			",p_customer = '" + pCustomer + '\'' + 
			",project = '" + project + '\'' + 
			",target_project = '" + targetProject + '\'' + 
			",all_status_chart = '" + allStatusChart + '\'' + 
			",p_transaction = '" + pTransaction + '\'' + 
			",p_cross_selling = '" + pCrossSelling + '\'' + 
			",sum_trans = '" + sumTrans + '\'' + 
			",cross_selling = '" + crossSelling + '\'' + 
			",salechart = '" + salechart + '\'' + 
			",p_project = '" + pProject + '\'' + 
			",target_trans = '" + targetTrans + '\'' + 
			",target_customer = '" + targetCustomer + '\'' + 
			",transaction = '" + transaction + '\'' + 
			",customer = '" + customer + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.targetCrossSelling);
		dest.writeString(this.pCustomer);
		dest.writeString(this.project);
		dest.writeString(this.targetProject);
		dest.writeParcelable(this.allStatusChart, flags);
		dest.writeString(this.pTransaction);
		dest.writeString(this.pCrossSelling);
		dest.writeParcelable(this.sumTrans, flags);
		dest.writeString(this.crossSelling);
		dest.writeList(this.salechart);
		dest.writeString(this.pProject);
		dest.writeString(this.targetTrans);
		dest.writeString(this.targetCustomer);
		dest.writeString(this.transaction);
		dest.writeString(this.customer);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.targetCrossSelling = in.readString();
		this.pCustomer = in.readString();
		this.project = in.readString();
		this.targetProject = in.readString();
		this.allStatusChart = in.readParcelable(AllStatusChart.class.getClassLoader());
		this.pTransaction = in.readString();
		this.pCrossSelling = in.readString();
		this.sumTrans = in.readParcelable(SumTrans.class.getClassLoader());
		this.crossSelling = in.readString();
		this.salechart = new ArrayList<SalechartItem>();
		in.readList(this.salechart, SalechartItem.class.getClassLoader());
		this.pProject = in.readString();
		this.targetTrans = in.readString();
		this.targetCustomer = in.readString();
		this.transaction = in.readString();
		this.customer = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}