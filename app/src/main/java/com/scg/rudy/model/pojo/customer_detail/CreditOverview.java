package com.scg.rudy.model.pojo.customer_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CreditOverview implements Parcelable {

	@SerializedName("p_credit_remain")
	private String pCreditRemain;

	@SerializedName("credit_remain")
	private String creditRemain;

	@SerializedName("p_credit_balance")
	private String pCreditBalance;

	@SerializedName("credit_balance")
	private String creditBalance;

	@SerializedName("credit_limit")
	private String creditLimit;

	@SerializedName("opportunity")
	private String opportunity;

	public void setPCreditRemain(String pCreditRemain){
		this.pCreditRemain = pCreditRemain;
	}

	public String getPCreditRemain(){
		return pCreditRemain;
	}

	public void setCreditRemain(String creditRemain){
		this.creditRemain = creditRemain;
	}

	public String getCreditRemain(){
		return creditRemain;
	}

	public void setPCreditBalance(String pCreditBalance){
		this.pCreditBalance = pCreditBalance;
	}

	public String getPCreditBalance(){
		return pCreditBalance;
	}

	public void setCreditBalance(String creditBalance){
		this.creditBalance = creditBalance;
	}

	public String getCreditBalance(){
		return creditBalance;
	}

	public void setCreditLimit(String creditLimit){
		this.creditLimit = creditLimit;
	}

	public String getCreditLimit(){
		return creditLimit;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	@Override
 	public String toString(){
		return 
			"CreditOverview{" + 
			"p_credit_remain = '" + pCreditRemain + '\'' + 
			",credit_remain = '" + creditRemain + '\'' + 
			",p_credit_balance = '" + pCreditBalance + '\'' + 
			",credit_balance = '" + creditBalance + '\'' + 
			",credit_limit = '" + creditLimit + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.pCreditRemain);
		dest.writeString(this.creditRemain);
		dest.writeString(this.pCreditBalance);
		dest.writeString(this.creditBalance);
		dest.writeString(this.creditLimit);
		dest.writeString(this.opportunity);
	}

	public CreditOverview() {
	}

	protected CreditOverview(Parcel in) {
		this.pCreditRemain = in.readString();
		this.creditRemain = in.readString();
		this.pCreditBalance = in.readString();
		this.creditBalance = in.readString();
		this.creditLimit = in.readString();
		this.opportunity = in.readString();
	}

	public static final Parcelable.Creator<CreditOverview> CREATOR = new Parcelable.Creator<CreditOverview>() {
		@Override
		public CreditOverview createFromParcel(Parcel source) {
			return new CreditOverview(source);
		}

		@Override
		public CreditOverview[] newArray(int size) {
			return new CreditOverview[size];
		}
	};
}