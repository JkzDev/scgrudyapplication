package com.scg.rudy.model.pojo.project_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class AudiosItem implements Parcelable {

	@SerializedName("file")
	private String file;

	@SerializedName("name")
	private String name;

	public void setFile(String file){
		this.file = file;
	}

	public String getFile(){
		return file;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"AudiosItem{" + 
			"file = '" + file + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.file);
		dest.writeString(this.name);
	}

	public AudiosItem() {
	}

	protected AudiosItem(Parcel in) {
		this.file = in.readString();
		this.name = in.readString();
	}

	public static final Parcelable.Creator<AudiosItem> CREATOR = new Parcelable.Creator<AudiosItem>() {
		@Override
		public AudiosItem createFromParcel(Parcel source) {
			return new AudiosItem(source);
		}

		@Override
		public AudiosItem[] newArray(int size) {
			return new AudiosItem[size];
		}
	};
}