package com.scg.rudy.model.pojo.customer_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class TransactionListItem implements Parcelable {

	@SerializedName("user_id")
	private String userId;

	@SerializedName("project_id")
	private String projectId;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("sale_name")
	private String saleName;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("status")
	private String status;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProjectId(String projectId){
		this.projectId = projectId;
	}

	public String getProjectId(){
		return projectId;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSaleName(String saleName){
		this.saleName = saleName;
	}

	public String getSaleName(){
		return saleName;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"TransactionListItem{" + 
			"user_id = '" + userId + '\'' + 
			",project_id = '" + projectId + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",sale_name = '" + saleName + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.userId);
		dest.writeString(this.projectId);
		dest.writeString(this.orderNumber);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.saleName);
		dest.writeString(this.projectName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
	}

	public TransactionListItem() {
	}

	protected TransactionListItem(Parcel in) {
		this.userId = in.readString();
		this.projectId = in.readString();
		this.orderNumber = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.saleName = in.readString();
		this.projectName = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<TransactionListItem> CREATOR = new Parcelable.Creator<TransactionListItem>() {
		@Override
		public TransactionListItem createFromParcel(Parcel source) {
			return new TransactionListItem(source);
		}

		@Override
		public TransactionListItem[] newArray(int size) {
			return new TransactionListItem[size];
		}
	};
}