package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 6/2/2018 AD.
 */

public class PRCodeDetailModel {
    /**
     * id : 613
     * project_id : 1036
     * shop_id : 5
     * user_id : 23
     * pr_code : PR6103-0209
     * number : 30985
     * sku_code : 613
     * competitor_id : null
     * qty : 4.00
     * default_price : 54.00
     * amount : 10.00
     * status : 2
     * remark : hello
     * logg : null
     * deliver_date : 0000-00-00
     * deliver_type : 0
     * create_date : null
     * loss_reason : null
     * request_price : 2.50
     * approve_price : null
     * rev : 0
     * pr_id : 209
     * qo_id : null
     * item_name : กระเบื้องผืนหลังคา นิวสไตล์ สี ALDER (ลายไม้)
     * item_code : 2119030200019
     * item_unit : null
     */
    public String id;
    public String project_id;
    public String shop_id;
    public String user_id;
    public String pr_code;
    public String number;
    public String sku_code;
    public String competitor_id;
    public String qty;
    public String default_price;
    public String amount;
    public String status;
    public String remark;
    public String logg;
    public String deliver_date;
    public String deliver_type;
    public String create_date;
    public String loss_reason;
    public String request_price;
    public String approve_price;
    public String rev;
    public String pr_id;
    public String qo_id;
    public String item_name;
    public String item_code;
    public String item_unit;
    public PRCodeDetailModel(
        String id,
        String project_id,
        String shop_id,
        String user_id,
        String pr_code,
        String number,
        String sku_code,
        String competitor_id,
        String qty,
        String default_price,
        String amount,
        String status,
        String remark,
        String logg,
        String deliver_date,
        String deliver_type,
        String create_date,
        String loss_reason,
        String request_price,
        String approve_price,
        String rev,
        String pr_id,
        String qo_id,
        String item_name,
        String item_code,
        String item_unit){
        this.id = id;
        this.project_id = project_id;
        this.shop_id = shop_id;
        this.user_id = user_id;
        this.pr_code = pr_code;
        this.number = number;
        this.sku_code = sku_code;
        this.competitor_id = competitor_id;
        this.qty = qty;
        this.default_price = default_price;
        this.amount = amount;
        this.status = status;
        this.remark = remark;
        this.logg = logg;
        this.deliver_date = deliver_date;
        this.deliver_type = deliver_type;
        this.create_date = create_date;
        this.loss_reason = loss_reason;
        this.request_price = request_price;
        this.approve_price = approve_price;
        this.rev = rev;
        this.pr_id = pr_id;
        this.qo_id = qo_id;
        this.item_name = item_name;
        this.item_code = item_code;
        this.item_unit = item_unit;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPr_code() {
        return pr_code;
    }

    public void setPr_code(String pr_code) {
        this.pr_code = pr_code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSku_code() {
        return sku_code;
    }

    public void setSku_code(String sku_code) {
        this.sku_code = sku_code;
    }

    public String getCompetitor_id() {
        return competitor_id;
    }

    public void setCompetitor_id(String competitor_id) {
        this.competitor_id = competitor_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDefault_price() {
        return default_price;
    }

    public void setDefault_price(String default_price) {
        this.default_price = default_price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLogg() {
        return logg;
    }

    public void setLogg(String logg) {
        this.logg = logg;
    }

    public String getDeliver_date() {
        return deliver_date;
    }

    public void setDeliver_date(String deliver_date) {
        this.deliver_date = deliver_date;
    }

    public String getDeliver_type() {
        return deliver_type;
    }

    public void setDeliver_type(String deliver_type) {
        this.deliver_type = deliver_type;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getLoss_reason() {
        return loss_reason;
    }

    public void setLoss_reason(String loss_reason) {
        this.loss_reason = loss_reason;
    }

    public String getRequest_price() {
        return request_price;
    }

    public void setRequest_price(String request_price) {
        this.request_price = request_price;
    }

    public String getApprove_price() {
        return approve_price;
    }

    public void setApprove_price(String approve_price) {
        this.approve_price = approve_price;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public String getPr_id() {
        return pr_id;
    }

    public void setPr_id(String pr_id) {
        this.pr_id = pr_id;
    }

    public String getQo_id() {
        return qo_id;
    }

    public void setQo_id(String qo_id) {
        this.qo_id = qo_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getItem_unit() {
        return item_unit;
    }

    public void setItem_unit(String item_unit) {
        this.item_unit = item_unit;
    }
}

