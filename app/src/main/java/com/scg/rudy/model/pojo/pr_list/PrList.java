package com.scg.rudy.model.pojo.pr_list;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PrList implements Serializable, Parcelable {

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PrList{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.items);
		dest.writeInt(this.status);
	}

	public PrList() {
	}

	protected PrList(Parcel in) {
		this.items = in.createTypedArrayList(ItemsItem.CREATOR);
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<PrList> CREATOR = new Parcelable.Creator<PrList>() {
		@Override
		public PrList createFromParcel(Parcel source) {
			return new PrList(source);
		}

		@Override
		public PrList[] newArray(int size) {
			return new PrList[size];
		}
	};
}