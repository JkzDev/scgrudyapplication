package com.scg.rudy.model.pojo.checkin_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class GalleryItem implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("pic_name")
	private String picName;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPicName(String picName){
		this.picName = picName;
	}

	public String getPicName(){
		return picName;
	}

	@Override
 	public String toString(){
		return 
			"GalleryItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",pic_name = '" + picName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
		dest.writeString(this.picName);
	}

	public GalleryItem() {
	}

	protected GalleryItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
		this.picName = in.readString();
	}

	public static final Parcelable.Creator<GalleryItem> CREATOR = new Parcelable.Creator<GalleryItem>() {
		@Override
		public GalleryItem createFromParcel(Parcel source) {
			return new GalleryItem(source);
		}

		@Override
		public GalleryItem[] newArray(int size) {
			return new GalleryItem[size];
		}
	};
}