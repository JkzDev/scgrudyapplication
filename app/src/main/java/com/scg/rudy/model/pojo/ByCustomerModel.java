package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 11/11/2017 AD.
 */

public class ByCustomerModel {
    String id;
    String name;
    String last_name;
    String unit_number;
    String user_status;
    String grand_total;
    String img;

    String type;
    String project_count;
    String total_budget;
    String underconstruction;
    String totalopp;
    String usergrade;
    String userrate;



    public ByCustomerModel(
            String id,
            String name,
            String last_name,
            String unit_number,
            String user_status,
            String grand_total,
            String img,
            String type,
            String project_count,
            String total_budget,
            String underconstruction,
            String totalopp,
            String usergrade,
            String userrate
    ){
        this.id = id;
        this.name = name;
        this.last_name = last_name;
        this.unit_number = unit_number;
        this.user_status = user_status;
        this.grand_total = grand_total;
        this.img = img;
        this.type = type;
        this.project_count = project_count;
        this.total_budget = total_budget;
        this.underconstruction = underconstruction;
        this.totalopp = totalopp;
        this.usergrade = usergrade;
        this.userrate = userrate;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProject_count() {
        return project_count;
    }

    public void setProject_count(String project_count) {
        this.project_count = project_count;
    }

    public String getTotal_budget() {
        return total_budget;
    }

    public void setTotal_budget(String total_budget) {
        this.total_budget = total_budget;
    }

    public String getUnderconstruction() {
        return underconstruction;
    }

    public void setUnderconstruction(String underconstruction) {
        this.underconstruction = underconstruction;
    }

    public String getTotalopp() {
        return totalopp;
    }

    public void setTotalopp(String totalopp) {
        this.totalopp = totalopp;
    }

    public String getUsergrade() {
        return usergrade;
    }

    public void setUsergrade(String usergrade) {
        this.usergrade = usergrade;
    }

    public String getUserrate() {
        return userrate;
    }

    public void setUserrate(String userrate) {
        this.userrate = userrate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUnit_number() {
        return unit_number;
    }

    public void setUnit_number(String unit_number) {
        this.unit_number = unit_number;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
