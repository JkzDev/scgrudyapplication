package com.scg.rudy.model.pojo.departmentdata_new;

import java.io.Serializable;

public class ProjectNew implements Serializable {
    String name;
    String type;
    String style;
    String dateStart;
    String dateEnd;
    String unit;
    String unitArea;

    public ProjectNew(String name, String type, String style, String dateStart, String dateEnd, String unit, String unitArea) {
        this.name = name;
        this.type = type;
        this.style = style;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.unit = unit;
        this.unitArea = unitArea;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitArea() {
        return unitArea;
    }

    public void setUnitArea(String unitArea) {
        this.unitArea = unitArea;
    }
}
