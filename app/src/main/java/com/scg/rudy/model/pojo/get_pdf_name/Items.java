package com.scg.rudy.model.pojo.get_pdf_name;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("pdf_name")
	private String pdfName;

	@SerializedName("status")
	private String status;

	public void setPdfName(String pdfName){
		this.pdfName = pdfName;
	}

	public String getPdfName(){
		return pdfName;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"pdf_name = '" + pdfName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.pdfName);
		dest.writeString(this.status);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.pdfName = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}