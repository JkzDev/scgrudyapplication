package com.scg.rudy.model.pojo.qo_to_so;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ToSo implements Parcelable {

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private int status;

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ToSo{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.items);
		dest.writeInt(this.status);
	}

	public ToSo() {
	}

	protected ToSo(Parcel in) {
		this.items = in.readString();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ToSo> CREATOR = new Parcelable.Creator<ToSo>() {
		@Override
		public ToSo createFromParcel(Parcel source) {
			return new ToSo(source);
		}

		@Override
		public ToSo[] newArray(int size) {
			return new ToSo[size];
		}
	};
}