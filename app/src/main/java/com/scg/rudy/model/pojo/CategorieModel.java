package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 8/11/2017 AD.
 */

public class CategorieModel {
    String id;
    String name;

    public CategorieModel(
            String id,
            String name){
        this.id = id;
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
