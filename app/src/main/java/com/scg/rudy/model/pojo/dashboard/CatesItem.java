package com.scg.rudy.model.pojo.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CatesItem implements Parcelable {

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CatesItem{" + 
			"total_amount = '" + totalAmount + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.totalAmount);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public CatesItem() {
	}

	protected CatesItem(Parcel in) {
		this.totalAmount = in.readString();
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<CatesItem> CREATOR = new Parcelable.Creator<CatesItem>() {
		@Override
		public CatesItem createFromParcel(Parcel source) {
			return new CatesItem(source);
		}

		@Override
		public CatesItem[] newArray(int size) {
			return new CatesItem[size];
		}
	};
}