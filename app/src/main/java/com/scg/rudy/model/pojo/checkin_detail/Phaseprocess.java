package com.scg.rudy.model.pojo.checkin_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Phaseprocess implements Parcelable {

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("sub_phases")
	private List<SubPhasesItem> subPhases;

	@SerializedName("phase_name")
	private String phaseName;

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setSubPhases(List<SubPhasesItem> subPhases){
		this.subPhases = subPhases;
	}

	public List<SubPhasesItem> getSubPhases(){
		return subPhases;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	@Override
 	public String toString(){
		return 
			"Phaseprocess{" + 
			"phase_id = '" + phaseId + '\'' + 
			",sub_phases = '" + subPhases + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.phaseId);
		dest.writeList(this.subPhases);
		dest.writeString(this.phaseName);
	}

	public Phaseprocess() {
	}

	protected Phaseprocess(Parcel in) {
		this.phaseId = in.readString();
		this.subPhases = new ArrayList<SubPhasesItem>();
		in.readList(this.subPhases, SubPhasesItem.class.getClassLoader());
		this.phaseName = in.readString();
	}

	public static final Parcelable.Creator<Phaseprocess> CREATOR = new Parcelable.Creator<Phaseprocess>() {
		@Override
		public Phaseprocess createFromParcel(Parcel source) {
			return new Phaseprocess(source);
		}

		@Override
		public Phaseprocess[] newArray(int size) {
			return new Phaseprocess[size];
		}
	};
}