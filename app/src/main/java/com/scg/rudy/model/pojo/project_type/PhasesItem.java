package com.scg.rudy.model.pojo.project_type;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PhasesItem implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("subphases")
	private String subphases;

	@SerializedName("id")
	private String id;

	@SerializedName("detail")
	private String detail;

	@SerializedName("subphases_list")
	private ArrayList<SubphasesListItem> subphasesList;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setSubphases(String subphases){
		this.subphases = subphases;
	}

	public String getSubphases(){
		return subphases;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setSubphasesList(ArrayList<SubphasesListItem> subphasesList){
		this.subphasesList = subphasesList;
	}

	public ArrayList<SubphasesListItem> getSubphasesList(){
		return subphasesList;
	}

	@Override
 	public String toString(){
		return 
			"PhasesItem{" + 
			"name = '" + name + '\'' + 
			",subphases = '" + subphases + '\'' + 
			",id = '" + id + '\'' + 
			",detail = '" + detail + '\'' + 
			",subphases_list = '" + subphasesList + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.subphases);
		dest.writeString(this.id);
		dest.writeString(this.detail);
		dest.writeTypedList(this.subphasesList);
	}

	public PhasesItem(
			String name,
			String subphases,
			String id,
			String detail,
			ArrayList<SubphasesListItem> subphasesList
	) {
		this.name = name;
		this.subphases = subphases;
		this.id = id;
		this.detail = detail;
		this.subphasesList = subphasesList;
	}

	protected PhasesItem(Parcel in) {
		this.name = in.readString();
		this.subphases = in.readString();
		this.id = in.readString();
		this.detail = in.readString();
		this.subphasesList = in.createTypedArrayList(SubphasesListItem.CREATOR);
	}

	public static final Parcelable.Creator<PhasesItem> CREATOR = new Parcelable.Creator<PhasesItem>() {
		@Override
		public PhasesItem createFromParcel(Parcel source) {
			return new PhasesItem(source);
		}

		@Override
		public PhasesItem[] newArray(int size) {
			return new PhasesItem[size];
		}
	};
}