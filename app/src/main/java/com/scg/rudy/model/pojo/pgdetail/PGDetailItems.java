package com.scg.rudy.model.pojo.pgdetail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PGDetailItems implements Parcelable {

	@SerializedName("end_date")
	public String endDate;

	@SerializedName("project_group_list_id")
	public String projectGroupListId;

	@SerializedName("developer_name")
	public String developerName;

	@SerializedName("added_datetime")
	public String addedDatetime;

	@SerializedName("shop_id")
	public String shopId;

	@SerializedName("project_group_list_name")
	public String projectGroupListName;

	@SerializedName("user_id")
	public String userId;

	@SerializedName("last_update")
	public String lastUpdate;

	@SerializedName("name")
	public String name;

	@SerializedName("project_list")
	public List<ProjectListItem> projectList;

	@SerializedName("id")
	public String id;

	@SerializedName("gallery")
	public List<Object> gallery;

	@SerializedName("start_date")
	public String startDate;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.endDate);
		dest.writeString(this.projectGroupListId);
		dest.writeString(this.developerName);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.shopId);
		dest.writeString(this.projectGroupListName);
		dest.writeString(this.userId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.name);
		dest.writeList(this.projectList);
		dest.writeString(this.id);
		dest.writeList(this.gallery);
		dest.writeString(this.startDate);
	}

	public PGDetailItems() {
	}

	protected PGDetailItems(Parcel in) {
		this.endDate = in.readString();
		this.projectGroupListId = in.readString();
		this.developerName = in.readString();
		this.addedDatetime = in.readString();
		this.shopId = in.readString();
		this.projectGroupListName = in.readString();
		this.userId = in.readString();
		this.lastUpdate = in.readString();
		this.name = in.readString();
		this.projectList = new ArrayList<ProjectListItem>();
		in.readList(this.projectList, ProjectListItem.class.getClassLoader());
		this.id = in.readString();
		this.gallery = new ArrayList<Object>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.startDate = in.readString();
	}

	public static final Creator<PGDetailItems> CREATOR = new Creator<PGDetailItems>() {
		@Override
		public PGDetailItems createFromParcel(Parcel source) {
			return new PGDetailItems(source);
		}

		@Override
		public PGDetailItems[] newArray(int size) {
			return new PGDetailItems[size];
		}
	};
}