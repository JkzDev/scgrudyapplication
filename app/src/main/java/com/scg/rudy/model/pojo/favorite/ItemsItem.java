package com.scg.rudy.model.pojo.favorite;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("total")
	private int total;

	@SerializedName("type")
	private String type;

	@SerializedName("list")
	private ArrayList<ListItem> list;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setList(ArrayList<ListItem> list){
		this.list = list;
	}

	public ArrayList<ListItem> getList(){
		return list;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"total = '" + total + '\'' + 
			",type = '" + type + '\'' + 
			",list = '" + list + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.total);
		dest.writeString(this.type);
		dest.writeList(this.list);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.total = in.readInt();
		this.type = in.readString();
		this.list = new ArrayList<ListItem>();
		in.readList(this.list, ListItem.class.getClassLoader());
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}