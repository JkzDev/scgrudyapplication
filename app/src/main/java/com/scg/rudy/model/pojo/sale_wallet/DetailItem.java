package com.scg.rudy.model.pojo.sale_wallet;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class DetailItem implements Parcelable {

	@SerializedName("transaction_id")
	private String transactionId;

	@SerializedName("no")
	private int no;

	@SerializedName("promotion_name")
	private String promotionName;

	@SerializedName("ispaid")
	private String ispaid;

	@SerializedName("total_commission")
	private String totalCommission;

	@SerializedName("total_amount")
	private String totalAmount;

	@SerializedName("date_paid")
	private String datePaid;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("percent")
	private String percent;

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setNo(int no){
		this.no = no;
	}

	public int getNo(){
		return no;
	}

	public void setPromotionName(String promotionName){
		this.promotionName = promotionName;
	}

	public String getPromotionName(){
		return promotionName;
	}

	public void setIspaid(String ispaid){
		this.ispaid = ispaid;
	}

	public String getIspaid(){
		return ispaid;
	}

	public void setTotalCommission(String totalCommission){
		this.totalCommission = totalCommission;
	}

	public String getTotalCommission(){
		return totalCommission;
	}

	public void setTotalAmount(String totalAmount){
		this.totalAmount = totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}

	public void setDatePaid(String datePaid){
		this.datePaid = datePaid;
	}

	public String getDatePaid(){
		return datePaid;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPercent(String percent){
		this.percent = percent;
	}

	public String getPercent(){
		return percent;
	}

	@Override
 	public String toString(){
		return 
			"DetailItem{" + 
			"transaction_id = '" + transactionId + '\'' + 
			",no = '" + no + '\'' + 
			",promotion_name = '" + promotionName + '\'' + 
			",ispaid = '" + ispaid + '\'' + 
			",total_commission = '" + totalCommission + '\'' + 
			",total_amount = '" + totalAmount + '\'' + 
			",date_paid = '" + datePaid + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",percent = '" + percent + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.transactionId);
		dest.writeInt(this.no);
		dest.writeString(this.promotionName);
		dest.writeString(this.ispaid);
		dest.writeString(this.totalCommission);
		dest.writeString(this.totalAmount);
		dest.writeString(this.datePaid);
		dest.writeString(this.orderNumber);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.percent);
	}

	public DetailItem() {
	}

	protected DetailItem(Parcel in) {
		this.transactionId = in.readString();
		this.no = in.readInt();
		this.promotionName = in.readString();
		this.ispaid = in.readString();
		this.totalCommission = in.readString();
		this.totalAmount = in.readString();
		this.datePaid = in.readString();
		this.orderNumber = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.percent = in.readString();
	}

	public static final Parcelable.Creator<DetailItem> CREATOR = new Parcelable.Creator<DetailItem>() {
		@Override
		public DetailItem createFromParcel(Parcel source) {
			return new DetailItem(source);
		}

		@Override
		public DetailItem[] newArray(int size) {
			return new DetailItem[size];
		}
	};
}