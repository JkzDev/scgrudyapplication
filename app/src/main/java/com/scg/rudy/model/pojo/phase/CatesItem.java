package com.scg.rudy.model.pojo.phase;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CatesItem implements Parcelable {

	@SerializedName("img")
	private String img;

	@SerializedName("color")
	private String color;

	@SerializedName("BOQ")
	private String bOQ;

	@SerializedName("classes")
	private List<ClassesItem> classes;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setBOQ(String bOQ){
		this.bOQ = bOQ;
	}

	public String getBOQ(){
		return bOQ;
	}

	public void setClasses(List<ClassesItem> classes){
		this.classes = classes;
	}

	public List<ClassesItem> getClasses(){
		return classes;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CatesItem{" + 
			"img = '" + img + '\'' + 
			",color = '" + color + '\'' + 
			",bOQ = '" + bOQ + '\'' + 
			",classes = '" + classes + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.img);
		dest.writeString(this.color);
		dest.writeString(this.bOQ);
		dest.writeTypedList(this.classes);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public CatesItem() {
	}

	protected CatesItem(Parcel in) {
		this.img = in.readString();
		this.color = in.readString();
		this.bOQ = in.readString();
		this.classes = in.createTypedArrayList(ClassesItem.CREATOR);
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<CatesItem> CREATOR = new Parcelable.Creator<CatesItem>() {
		@Override
		public CatesItem createFromParcel(Parcel source) {
			return new CatesItem(source);
		}

		@Override
		public CatesItem[] newArray(int size) {
			return new CatesItem[size];
		}
	};
}