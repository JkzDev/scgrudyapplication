package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 6/2/2018 AD.
 */

public class BasketModel {
    /**
     * id : 148
     * product_name : ปูนฉาบอิฐมวลเบาสำเร็จรูปถุง 50 กก.
     * price : 90.00
     * voulme : 99.00
     * total : 8910.00
     * status_id : 1
     * status : เสนอขาย
     */
    String id;
    String shop_id;
    String product_id;
    String project_id;
    String item_name;
    String recommend;
    String amount;
    String price;
    String price1;
    String bill;
    public BasketModel(
            String id,
            String shop_id,
            String product_id,
            String project_id,
            String item_name,
            String recommend,
            String amount,
            String price,
            String price1,
            String bill){
        this.id =id;
        this.shop_id = shop_id;
        this.product_id =product_id;
        this.project_id = project_id;
        this.item_name = item_name;
        this.recommend=recommend;
        this.amount = amount;
        this.price = price;
        this.price1 =price1;
        this.bill = bill;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }
}

