package com.scg.rudy.model.pojo.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("is_private")
	private String isPrivate;

	@SerializedName("default_phase_id")
	private String defaultPhaseId;

	@SerializedName("default_units")
	private String defaultUnits;

	@SerializedName("default_project_stories")
	private String defaultProjectStories;

	@SerializedName("shop")
	private String shop;

	@SerializedName("target_cross_selling")
	private String targetCrossSelling;

	@SerializedName("level")
	private String level;

	@SerializedName("default_unit_area")
	private String defaultUnitArea;

	@SerializedName("target_project")
	private String targetProject;

	@SerializedName("pic")
	private String pic;

	@SerializedName("default_unit_budget")
	private String defaultUnitBudget;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("level_name")
	private String levelName;

	@SerializedName("phone")
	private String phone;

	@SerializedName("employee_id")
	private String employeeId;

	@SerializedName("name")
	private String name;

	@SerializedName("nickname")
	private String nickname;

	@SerializedName("default_project_type_id")
	private String defaultProjectTypeId;

	@SerializedName("id")
	private String id;

	@SerializedName("target_trans")
	private String targetTrans;

	@SerializedName("target_customer")
	private String targetCustomer;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@SerializedName("currency")
	private String currency;


	@SerializedName("default_tax_type")
	private String default_tax_type;

	public String getDefault_tax_type() {
		return default_tax_type;
	}

	public void setDefault_tax_type(String default_tax_type) {
		this.default_tax_type = default_tax_type;
	}

	public void setIsPrivate(String isPrivate){
		this.isPrivate = isPrivate;
	}

	public String getIsPrivate(){
		return isPrivate;
	}

	public void setDefaultPhaseId(String defaultPhaseId){
		this.defaultPhaseId = defaultPhaseId;
	}

	public String getDefaultPhaseId(){
		return defaultPhaseId;
	}

	public void setDefaultUnits(String defaultUnits){
		this.defaultUnits = defaultUnits;
	}

	public String getDefaultUnits(){
		return defaultUnits;
	}

	public void setDefaultProjectStories(String defaultProjectStories){
		this.defaultProjectStories = defaultProjectStories;
	}

	public String getDefaultProjectStories(){
		return defaultProjectStories;
	}

	public void setShop(String shop){
		this.shop = shop;
	}

	public String getShop(){
		return shop;
	}

	public void setTargetCrossSelling(String targetCrossSelling){
		this.targetCrossSelling = targetCrossSelling;
	}

	public String getTargetCrossSelling(){
		return targetCrossSelling;
	}

	public void setLevel(String level){
		this.level = level;
	}

	public String getLevel(){
		return level;
	}

	public void setDefaultUnitArea(String defaultUnitArea){
		this.defaultUnitArea = defaultUnitArea;
	}

	public String getDefaultUnitArea(){
		return defaultUnitArea;
	}

	public void setTargetProject(String targetProject){
		this.targetProject = targetProject;
	}

	public String getTargetProject(){
		return targetProject;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setDefaultUnitBudget(String defaultUnitBudget){
		this.defaultUnitBudget = defaultUnitBudget;
	}

	public String getDefaultUnitBudget(){
		return defaultUnitBudget;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setLevelName(String levelName){
		this.levelName = levelName;
	}

	public String getLevelName(){
		return levelName;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setEmployeeId(String employeeId){
		this.employeeId = employeeId;
	}

	public String getEmployeeId(){
		return employeeId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setDefaultProjectTypeId(String defaultProjectTypeId){
		this.defaultProjectTypeId = defaultProjectTypeId;
	}

	public String getDefaultProjectTypeId(){
		return defaultProjectTypeId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTargetTrans(String targetTrans){
		this.targetTrans = targetTrans;
	}

	public String getTargetTrans(){
		return targetTrans;
	}

	public void setTargetCustomer(String targetCustomer){
		this.targetCustomer = targetCustomer;
	}

	public String getTargetCustomer(){
		return targetCustomer;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"is_private = '" + isPrivate + '\'' + 
			",default_phase_id = '" + defaultPhaseId + '\'' + 
			",default_units = '" + defaultUnits + '\'' + 
			",default_project_stories = '" + defaultProjectStories + '\'' + 
			",shop = '" + shop + '\'' + 
			",target_cross_selling = '" + targetCrossSelling + '\'' + 
			",level = '" + level + '\'' + 
			",default_unit_area = '" + defaultUnitArea + '\'' + 
			",target_project = '" + targetProject + '\'' + 
			",pic = '" + pic + '\'' + 
			",default_unit_budget = '" + defaultUnitBudget + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",level_name = '" + levelName + '\'' + 
			",phone = '" + phone + '\'' + 
			",employee_id = '" + employeeId + '\'' + 
			",name = '" + name + '\'' + 
			",nickname = '" + nickname + '\'' + 
			",default_project_type_id = '" + defaultProjectTypeId + '\'' + 
			",id = '" + id + '\'' + 
			",target_trans = '" + targetTrans + '\'' + 
			",target_customer = '" + targetCustomer + '\'' + 
			",email = '" + email + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.isPrivate);
		dest.writeString(this.defaultPhaseId);
		dest.writeString(this.defaultUnits);
		dest.writeString(this.defaultProjectStories);
		dest.writeString(this.shop);
		dest.writeString(this.targetCrossSelling);
		dest.writeString(this.level);
		dest.writeString(this.defaultUnitArea);
		dest.writeString(this.targetProject);
		dest.writeString(this.pic);
		dest.writeString(this.defaultUnitBudget);
		dest.writeString(this.shopId);
		dest.writeString(this.levelName);
		dest.writeString(this.phone);
		dest.writeString(this.employeeId);
		dest.writeString(this.name);
		dest.writeString(this.nickname);
		dest.writeString(this.defaultProjectTypeId);
		dest.writeString(this.id);
		dest.writeString(this.targetTrans);
		dest.writeString(this.targetCustomer);
		dest.writeString(this.email);
		dest.writeString(this.status);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.isPrivate = in.readString();
		this.defaultPhaseId = in.readString();
		this.defaultUnits = in.readString();
		this.defaultProjectStories = in.readString();
		this.shop = in.readString();
		this.targetCrossSelling = in.readString();
		this.level = in.readString();
		this.defaultUnitArea = in.readString();
		this.targetProject = in.readString();
		this.pic = in.readString();
		this.defaultUnitBudget = in.readString();
		this.shopId = in.readString();
		this.levelName = in.readString();
		this.phone = in.readString();
		this.employeeId = in.readString();
		this.name = in.readString();
		this.nickname = in.readString();
		this.defaultProjectTypeId = in.readString();
		this.id = in.readString();
		this.targetTrans = in.readString();
		this.targetCustomer = in.readString();
		this.email = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}