package com.scg.rudy.model.pojo.by_location_new;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("app_name")
	private String appName;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("color")
	private String color;

	@SerializedName("pin")
	private String pin;

	@SerializedName("distance")
	private float distance;

	@SerializedName("project_address")
	private String projectAddress;

	@SerializedName("id")
	private String id;

	@SerializedName("pic")
	private String pic;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("app_id")
	private int appId;

	@SerializedName("status")
	private String status;

	@SerializedName("biz_id")
	private String biz_id;

	@SerializedName("is_eyes")
	private boolean is_eyes;

	@SerializedName("pulled")
	private boolean pulled;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@SerializedName("user_name")
	private String user_name;

	public boolean isPulled() {
		return pulled;
	}

	public void setPulled(boolean pulled) {
		this.pulled = pulled;
	}

	public String getBiz_id() {
		return biz_id;
	}

	public void setBiz_id(String biz_id) {
		this.biz_id = biz_id;
	}

	public boolean isIs_eyes() {
		return is_eyes;
	}

	public void setIs_eyes(boolean is_eyes) {
		this.is_eyes = is_eyes;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setAppName(String appName){
		this.appName = appName;
	}

	public String getAppName(){
		return appName;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setDistance(float distance){
		this.distance = distance;
	}

	public float getDistance(){
		return distance;
	}

	public void setProjectAddress(String projectAddress){
		this.projectAddress = projectAddress;
	}

	public String getProjectAddress(){
		return projectAddress;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setAppId(int appId){
		this.appId = appId;
	}

	public int getAppId(){
		return appId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"shop_id = '" + shopId + '\'' + 
			",app_name = '" + appName + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",color = '" + color + '\'' + 
			",pin = '" + pin + '\'' + 
			",distance = '" + distance + '\'' + 
			",project_address = '" + projectAddress + '\'' + 
			",id = '" + id + '\'' + 
			",pic = '" + pic + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",app_id = '" + appId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.shopId);
		dest.writeString(this.appName);
		dest.writeString(this.latLng);
		dest.writeString(this.color);
		dest.writeString(this.pin);
		dest.writeFloat(this.distance);
		dest.writeString(this.projectAddress);
		dest.writeString(this.id);
		dest.writeString(this.pic);
		dest.writeString(this.projectName);
		dest.writeInt(this.appId);
		dest.writeString(this.status);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.shopId = in.readString();
		this.appName = in.readString();
		this.latLng = in.readString();
		this.color = in.readString();
		this.pin = in.readString();
		this.distance = in.readFloat();
		this.projectAddress = in.readString();
		this.id = in.readString();
		this.pic = in.readString();
		this.projectName = in.readString();
		this.appId = in.readInt();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}