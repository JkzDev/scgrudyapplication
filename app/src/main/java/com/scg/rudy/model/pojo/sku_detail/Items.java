package com.scg.rudy.model.pojo.sku_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Serializable, Parcelable {

	@SerializedName("img")
	private String img;

	@SerializedName("class_id")
	private String classId;

	@SerializedName("description")
	private String description;

	@SerializedName("cate_id")
	private String cateId;

	@SerializedName("recommend")
	private String recommend;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("description_short")
	private String descriptionShort;

	@SerializedName("unit")
	private String unit;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("price3")
	private String price3;

	@SerializedName("stock")
	private String stock;

	@SerializedName("sku_code")
	private String skuCode;

	@SerializedName("gallery")
	private List<Object> gallery;

	@SerializedName("price2")
	private String price2;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setClassId(String classId){
		this.classId = classId;
	}

	public String getClassId(){
		return classId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCateId(String cateId){
		this.cateId = cateId;
	}

	public String getCateId(){
		return cateId;
	}

	public void setRecommend(String recommend){
		this.recommend = recommend;
	}

	public String getRecommend(){
		return recommend;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setDescriptionShort(String descriptionShort){
		this.descriptionShort = descriptionShort;
	}

	public String getDescriptionShort(){
		return descriptionShort;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPrice3(String price3){
		this.price3 = price3;
	}

	public String getPrice3(){
		return price3;
	}

	public void setStock(String stock){
		this.stock = stock;
	}

	public String getStock(){
		return stock;
	}

	public void setSkuCode(String skuCode){
		this.skuCode = skuCode;
	}

	public String getSkuCode(){
		return skuCode;
	}

	public void setGallery(List<Object> gallery){
		this.gallery = gallery;
	}

	public List<Object> getGallery(){
		return gallery;
	}

	public void setPrice2(String price2){
		this.price2 = price2;
	}

	public String getPrice2(){
		return price2;
	}

	@Override
 	public String toString(){
		return 
			"projectDetailItems{" +
			"img = '" + img + '\'' + 
			",class_id = '" + classId + '\'' + 
			",description = '" + description + '\'' + 
			",cate_id = '" + cateId + '\'' + 
			",recommend = '" + recommend + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",description_short = '" + descriptionShort + '\'' + 
			",unit = '" + unit + '\'' + 
			",price = '" + price + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",price3 = '" + price3 + '\'' + 
			",stock = '" + stock + '\'' + 
			",sku_code = '" + skuCode + '\'' + 
			",gallery = '" + gallery + '\'' + 
			",price2 = '" + price2 + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.img);
		dest.writeString(this.classId);
		dest.writeString(this.description);
		dest.writeString(this.cateId);
		dest.writeString(this.recommend);
		dest.writeString(this.shopId);
		dest.writeString(this.descriptionShort);
		dest.writeString(this.unit);
		dest.writeString(this.price);
		dest.writeString(this.name);
		dest.writeString(this.id);
		dest.writeString(this.price3);
		dest.writeString(this.stock);
		dest.writeString(this.skuCode);
		dest.writeList(this.gallery);
		dest.writeString(this.price2);
	}

	public Items() {
	}

	protected Items(Parcel in) {
		this.img = in.readString();
		this.classId = in.readString();
		this.description = in.readString();
		this.cateId = in.readString();
		this.recommend = in.readString();
		this.shopId = in.readString();
		this.descriptionShort = in.readString();
		this.unit = in.readString();
		this.price = in.readString();
		this.name = in.readString();
		this.id = in.readString();
		this.price3 = in.readString();
		this.stock = in.readString();
		this.skuCode = in.readString();
		this.gallery = new ArrayList<Object>();
		in.readList(this.gallery, Object.class.getClassLoader());
		this.price2 = in.readString();
	}

	public static final Parcelable.Creator<Items> CREATOR = new Parcelable.Creator<Items>() {
		@Override
		public Items createFromParcel(Parcel source) {
			return new Items(source);
		}

		@Override
		public Items[] newArray(int size) {
			return new Items[size];
		}
	};
}