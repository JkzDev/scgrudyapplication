package com.scg.rudy.model.pojo.project_type;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectType implements Parcelable {

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProjectType{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.items);
		dest.writeInt(this.status);
	}

	public ProjectType() {
	}

	protected ProjectType(Parcel in) {
		this.items = in.createTypedArrayList(ItemsItem.CREATOR);
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ProjectType> CREATOR = new Parcelable.Creator<ProjectType>() {
		@Override
		public ProjectType createFromParcel(Parcel source) {
			return new ProjectType(source);
		}

		@Override
		public ProjectType[] newArray(int size) {
			return new ProjectType[size];
		}
	};
}