package com.scg.rudy.model.pojo.checkin_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubPhasesItem implements Parcelable {

	@SerializedName("sub_phase_id")
	private String subPhaseId;

	@SerializedName("sub_phase_name")
	private String subPhaseName;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("status")
	private String status;

	public void setSubPhaseId(String subPhaseId){
		this.subPhaseId = subPhaseId;
	}

	public String getSubPhaseId(){
		return subPhaseId;
	}

	public void setSubPhaseName(String subPhaseName){
		this.subPhaseName = subPhaseName;
	}

	public String getSubPhaseName(){
		return subPhaseName;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SubPhasesItem{" + 
			"sub_phase_id = '" + subPhaseId + '\'' + 
			",sub_phase_name = '" + subPhaseName + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.subPhaseId);
		dest.writeString(this.subPhaseName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
	}

	public SubPhasesItem() {
	}

	protected SubPhasesItem(Parcel in) {
		this.subPhaseId = in.readString();
		this.subPhaseName = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<SubPhasesItem> CREATOR = new Parcelable.Creator<SubPhasesItem>() {
		@Override
		public SubPhasesItem createFromParcel(Parcel source) {
			return new SubPhasesItem(source);
		}

		@Override
		public SubPhasesItem[] newArray(int size) {
			return new SubPhasesItem[size];
		}
	};
}