package com.scg.rudy.model.pojo.subphase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CatesItem implements Serializable, Parcelable {

	@SerializedName("img")
	private String img;

	@SerializedName("color")
	private int color;

	@SerializedName("classes")
	private List<ClassesItem> classes;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setColor(int color){
		this.color = color;
	}

	public int getColor(){
		return color;
	}

	public void setClasses(List<ClassesItem> classes){
		this.classes = classes;
	}

	public List<ClassesItem> getClasses(){
		return classes;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CatesItem{" + 
			"img = '" + img + '\'' + 
			",color = '" + color + '\'' + 
			",classes = '" + classes + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.img);
		dest.writeInt(this.color);
		dest.writeList(this.classes);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public CatesItem() {
	}

	protected CatesItem(Parcel in) {
		this.img = in.readString();
		this.color = in.readInt();
		this.classes = new ArrayList<ClassesItem>();
		in.readList(this.classes, ClassesItem.class.getClassLoader());
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<CatesItem> CREATOR = new Parcelable.Creator<CatesItem>() {
		@Override
		public CatesItem createFromParcel(Parcel source) {
			return new CatesItem(source);
		}

		@Override
		public CatesItem[] newArray(int size) {
			return new CatesItem[size];
		}
	};
}