package com.scg.rudy.model.pojo.project_group_detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ProjectGroupContactItem implements Parcelable {

	@SerializedName("note")
	private String note;

	@SerializedName("contact_type_name")
	private String contactTypeName;

	@SerializedName("phone")
	private String phone;

	@SerializedName("contact_type_id")
	private String contactTypeId;

	@SerializedName("line")
	private String line;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("name")
	private String name;

	@SerializedName("company")
	private String company;

	@SerializedName("id")
	private String id;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setContactTypeName(String contactTypeName){
		this.contactTypeName = contactTypeName;
	}

	public String getContactTypeName(){
		return contactTypeName;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setContactTypeId(String contactTypeId){
		this.contactTypeId = contactTypeId;
	}

	public String getContactTypeId(){
		return contactTypeId;
	}

	public void setLine(String line){
		this.line = line;
	}

	public String getLine(){
		return line;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ProjectGroupContactItem{" + 
			"note = '" + note + '\'' + 
			",contact_type_name = '" + contactTypeName + '\'' + 
			",phone = '" + phone + '\'' + 
			",contact_type_id = '" + contactTypeId + '\'' + 
			",line = '" + line + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",name = '" + name + '\'' + 
			",company = '" + company + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.note);
		dest.writeString(this.contactTypeName);
		dest.writeString(this.phone);
		dest.writeString(this.contactTypeId);
		dest.writeString(this.line);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.name);
		dest.writeString(this.company);
		dest.writeString(this.id);
	}

	public ProjectGroupContactItem() {
	}

	protected ProjectGroupContactItem(Parcel in) {
		this.note = in.readString();
		this.contactTypeName = in.readString();
		this.phone = in.readString();
		this.contactTypeId = in.readString();
		this.line = in.readString();
		this.lastUpdate = in.readString();
		this.name = in.readString();
		this.company = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<ProjectGroupContactItem> CREATOR = new Parcelable.Creator<ProjectGroupContactItem>() {
		@Override
		public ProjectGroupContactItem createFromParcel(Parcel source) {
			return new ProjectGroupContactItem(source);
		}

		@Override
		public ProjectGroupContactItem[] newArray(int size) {
			return new ProjectGroupContactItem[size];
		}
	};
}