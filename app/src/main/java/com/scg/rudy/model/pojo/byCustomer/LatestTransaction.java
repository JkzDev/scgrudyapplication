package com.scg.rudy.model.pojo.byCustomer;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class LatestTransaction implements Parcelable {

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("transaction")
	private String transaction;

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setTransaction(String transaction){
		this.transaction = transaction;
	}

	public String getTransaction(){
		return transaction;
	}

	@Override
 	public String toString(){
		return 
			"LatestTransaction{" + 
			"last_update = '" + lastUpdate + '\'' + 
			",transaction = '" + transaction + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.lastUpdate);
		dest.writeString(this.transaction);
	}

	public LatestTransaction() {
	}

	protected LatestTransaction(Parcel in) {
		this.lastUpdate = in.readString();
		this.transaction = in.readString();
	}

	public static final Parcelable.Creator<LatestTransaction> CREATOR = new Parcelable.Creator<LatestTransaction>() {
		@Override
		public LatestTransaction createFromParcel(Parcel source) {
			return new LatestTransaction(source);
		}

		@Override
		public LatestTransaction[] newArray(int size) {
			return new LatestTransaction[size];
		}
	};
}