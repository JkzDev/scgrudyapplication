package com.scg.rudy.model.pojo.project_group_detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ProjectListItem implements Parcelable {

	@SerializedName("is_private")
	private String isPrivate;

	@SerializedName("rank_name")
	private String rankName;

	@SerializedName("countPR")
	private String countPR;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("units")
	private String units;

	@SerializedName("pic")
	private String pic;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("rank")
	private String rank;

	@SerializedName("id")
	private String id;

	@SerializedName("opportunity_n")
	private double opportunityN;

	@SerializedName("countFAV")
	private String countFAV;

	@SerializedName("project_type_id")
	private String projectTypeId;

	@SerializedName("project_stories")
	private String projectStories;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("trans_n")
	private String transN;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("unit_area")
	private String unitArea;

	@SerializedName("phase_name")
	private String phaseName;

	@SerializedName("project_type_name")
	private String projectTypeName;

	@SerializedName("trans")
	private String trans;

	@SerializedName("status")
	private String status;

	public void setIsPrivate(String isPrivate){
		this.isPrivate = isPrivate;
	}

	public String getIsPrivate(){
		return isPrivate;
	}

	public void setRankName(String rankName){
		this.rankName = rankName;
	}

	public String getRankName(){
		return rankName;
	}

	public void setCountPR(String countPR){
		this.countPR = countPR;
	}

	public String getCountPR(){
		return countPR;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setUnits(String units){
		this.units = units;
	}

	public String getUnits(){
		return units;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setOpportunityN(int opportunityN){
		this.opportunityN = opportunityN;
	}

	public double getOpportunityN(){
		return opportunityN;
	}

	public void setCountFAV(String countFAV){
		this.countFAV = countFAV;
	}

	public String getCountFAV(){
		return countFAV;
	}

	public void setProjectTypeId(String projectTypeId){
		this.projectTypeId = projectTypeId;
	}

	public String getProjectTypeId(){
		return projectTypeId;
	}

	public void setProjectStories(String projectStories){
		this.projectStories = projectStories;
	}

	public String getProjectStories(){
		return projectStories;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setTransN(String transN){
		this.transN = transN;
	}

	public String getTransN(){
		return transN;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUnitArea(String unitArea){
		this.unitArea = unitArea;
	}

	public String getUnitArea(){
		return unitArea;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	public void setProjectTypeName(String projectTypeName){
		this.projectTypeName = projectTypeName;
	}

	public String getProjectTypeName(){
		return projectTypeName;
	}

	public void setTrans(String trans){
		this.trans = trans;
	}

	public String getTrans(){
		return trans;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProjectListItem{" + 
			"is_private = '" + isPrivate + '\'' + 
			",rank_name = '" + rankName + '\'' + 
			",countPR = '" + countPR + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",units = '" + units + '\'' + 
			",pic = '" + pic + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",rank = '" + rank + '\'' + 
			",id = '" + id + '\'' + 
			",opportunity_n = '" + opportunityN + '\'' + 
			",countFAV = '" + countFAV + '\'' + 
			",project_type_id = '" + projectTypeId + '\'' + 
			",project_stories = '" + projectStories + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",trans_n = '" + transN + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",user_id = '" + userId + '\'' + 
			",unit_area = '" + unitArea + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			",project_type_name = '" + projectTypeName + '\'' + 
			",trans = '" + trans + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.isPrivate);
		dest.writeString(this.rankName);
		dest.writeString(this.countPR);
		dest.writeString(this.phaseId);
		dest.writeString(this.units);
		dest.writeString(this.pic);
		dest.writeString(this.projectName);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.unitBudget);
		dest.writeString(this.rank);
		dest.writeString(this.id);
		dest.writeDouble(this.opportunityN);
		dest.writeString(this.countFAV);
		dest.writeString(this.projectTypeId);
		dest.writeString(this.projectStories);
		dest.writeString(this.opportunity);
		dest.writeString(this.shopId);
		dest.writeString(this.transN);
		dest.writeString(this.latLng);
		dest.writeString(this.userId);
		dest.writeString(this.unitArea);
		dest.writeString(this.phaseName);
		dest.writeString(this.projectTypeName);
		dest.writeString(this.trans);
		dest.writeString(this.status);
	}

	public ProjectListItem() {
	}

	protected ProjectListItem(Parcel in) {
		this.isPrivate = in.readString();
		this.rankName = in.readString();
		this.countPR = in.readString();
		this.phaseId = in.readString();
		this.units = in.readString();
		this.pic = in.readString();
		this.projectName = in.readString();
		this.lastUpdate = in.readString();
		this.unitBudget = in.readString();
		this.rank = in.readString();
		this.id = in.readString();
		this.opportunityN = in.readInt();
		this.countFAV = in.readString();
		this.projectTypeId = in.readString();
		this.projectStories = in.readString();
		this.opportunity = in.readString();
		this.shopId = in.readString();
		this.transN = in.readString();
		this.latLng = in.readString();
		this.userId = in.readString();
		this.unitArea = in.readString();
		this.phaseName = in.readString();
		this.projectTypeName = in.readString();
		this.trans = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<ProjectListItem> CREATOR = new Parcelable.Creator<ProjectListItem>() {
		@Override
		public ProjectListItem createFromParcel(Parcel source) {
			return new ProjectListItem(source);
		}

		@Override
		public ProjectListItem[] newArray(int size) {
			return new ProjectListItem[size];
		}
	};
}