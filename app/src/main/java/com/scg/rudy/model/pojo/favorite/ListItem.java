package com.scg.rudy.model.pojo.favorite;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ListItem implements Parcelable {

	@SerializedName("unit")
	private String unit;

	@SerializedName("BOQ")
	private String bOQ;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("pic")
	private String pic;

	@SerializedName("price3")
	private String price3;

	@SerializedName("stock")
	private String stock;

	@SerializedName("price2")
	private String price2;

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setBOQ(String bOQ){
		this.bOQ = bOQ;
	}

	public String getBOQ(){
		return bOQ;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setPrice3(String price3){
		this.price3 = price3;
	}

	public String getPrice3(){
		return price3;
	}

	public void setStock(String stock){
		this.stock = stock;
	}

	public String getStock(){
		return stock;
	}

	public void setPrice2(String price2){
		this.price2 = price2;
	}

	public String getPrice2(){
		return price2;
	}

	@Override
 	public String toString(){
		return 
			"ListItem{" + 
			"unit = '" + unit + '\'' + 
			",bOQ = '" + bOQ + '\'' + 
			",price = '" + price + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",pic = '" + pic + '\'' + 
			",price3 = '" + price3 + '\'' + 
			",stock = '" + stock + '\'' + 
			",price2 = '" + price2 + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.unit);
		dest.writeString(this.bOQ);
		dest.writeString(this.price);
		dest.writeString(this.name);
		dest.writeString(this.id);
		dest.writeString(this.pic);
		dest.writeString(this.price3);
		dest.writeString(this.stock);
		dest.writeString(this.price2);
	}

	public ListItem() {
	}

	protected ListItem(Parcel in) {
		this.unit = in.readString();
		this.bOQ = in.readString();
		this.price = in.readString();
		this.name = in.readString();
		this.id = in.readString();
		this.pic = in.readString();
		this.price3 = in.readString();
		this.stock = in.readString();
		this.price2 = in.readString();
	}

	public static final Parcelable.Creator<ListItem> CREATOR = new Parcelable.Creator<ListItem>() {
		@Override
		public ListItem createFromParcel(Parcel source) {
			return new ListItem(source);
		}

		@Override
		public ListItem[] newArray(int size) {
			return new ListItem[size];
		}
	};
}