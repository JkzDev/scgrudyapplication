package com.scg.rudy.model.pojo.pgdetail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PhaseprocessItem implements Parcelable {

	@SerializedName("phase_pic")
	public String phasePic;

	@SerializedName("subphaseprocess")
	public List<SubphaseprocessItem> subphaseprocess;

	@SerializedName("rank")
	public String rank;

	@SerializedName("opportunity")
	public String opportunity;

	@SerializedName("work_progress")
	public String workProgress;

	@SerializedName("phase_id")
	public String phaseId;

	@SerializedName("phase_name")
	public String phaseName;

	@SerializedName("status_txt")
	public String statusTxt;

	@SerializedName("trans")
	public String trans;

	@SerializedName("status")
	public String status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.phasePic);
		dest.writeTypedList(this.subphaseprocess);
		dest.writeString(this.rank);
		dest.writeString(this.opportunity);
		dest.writeString(this.workProgress);
		dest.writeString(this.phaseId);
		dest.writeString(this.phaseName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.trans);
		dest.writeString(this.status);
	}

	public PhaseprocessItem() {
	}

	protected PhaseprocessItem(Parcel in) {
		this.phasePic = in.readString();
		this.subphaseprocess = in.createTypedArrayList(SubphaseprocessItem.CREATOR);
		this.rank = in.readString();
		this.opportunity = in.readString();
		this.workProgress = in.readString();
		this.phaseId = in.readString();
		this.phaseName = in.readString();
		this.statusTxt = in.readString();
		this.trans = in.readString();
		this.status = in.readString();
	}

	public static final Creator<PhaseprocessItem> CREATOR = new Creator<PhaseprocessItem>() {
		@Override
		public PhaseprocessItem createFromParcel(Parcel source) {
			return new PhaseprocessItem(source);
		}

		@Override
		public PhaseprocessItem[] newArray(int size) {
			return new PhaseprocessItem[size];
		}
	};
}