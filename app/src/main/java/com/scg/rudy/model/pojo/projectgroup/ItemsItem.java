package com.scg.rudy.model.pojo.projectgroup;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("project_type")
	public List<ProjectTypeItem> projectType;

	@SerializedName("name")
	public String name;

	@SerializedName("id")
	public String id;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.projectType);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.projectType = in.createTypedArrayList(ProjectTypeItem.CREATOR);
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Creator<ItemsItem> CREATOR = new Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}