package com.scg.rudy.model.pojo.Error;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class APIError extends RuntimeException implements Parcelable {

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private int status;

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"APIError{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.items);
        dest.writeInt(this.status);
    }

    public APIError() {
    }

    public APIError(Parcel in) {
        this.items = in.readString();
        this.status = in.readInt();
    }

    public static final Parcelable.Creator<APIError> CREATOR = new Parcelable.Creator<APIError>() {
        @Override
        public APIError createFromParcel(Parcel source) {
            return new APIError(source);
        }

        @Override
        public APIError[] newArray(int size) {
            return new APIError[size];
        }
    };
}