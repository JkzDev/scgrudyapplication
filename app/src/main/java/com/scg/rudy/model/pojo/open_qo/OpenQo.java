package com.scg.rudy.model.pojo.open_qo;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("com.asif.gsonpojogenerator")
public class OpenQo implements Serializable, Parcelable {

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private int status;

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OpenQo{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.items);
		dest.writeInt(this.status);
	}

	public OpenQo() {
	}

	protected OpenQo(Parcel in) {
		this.items = in.readString();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<OpenQo> CREATOR = new Parcelable.Creator<OpenQo>() {
		@Override
		public OpenQo createFromParcel(Parcel source) {
			return new OpenQo(source);
		}

		@Override
		public OpenQo[] newArray(int size) {
			return new OpenQo[size];
		}
	};
}