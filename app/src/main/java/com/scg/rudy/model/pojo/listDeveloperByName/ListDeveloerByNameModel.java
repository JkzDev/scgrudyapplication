package com.scg.rudy.model.listDeveloperByName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ListDeveloerByNameModel implements Parcelable {

	@SerializedName("items")
	public List<ItemsItem> items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.items);
		dest.writeInt(this.status);
	}

	public ListDeveloerByNameModel() {
	}

	protected ListDeveloerByNameModel(Parcel in) {
		this.items = new ArrayList<ItemsItem>();
		in.readList(this.items, ItemsItem.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Creator<ListDeveloerByNameModel> CREATOR = new Creator<ListDeveloerByNameModel>() {
		@Override
		public ListDeveloerByNameModel createFromParcel(Parcel source) {
			return new ListDeveloerByNameModel(source);
		}

		@Override
		public ListDeveloerByNameModel[] newArray(int size) {
			return new ListDeveloerByNameModel[size];
		}
	};
}