package com.scg.rudy.model.pojo.checkin_history_list;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("project_id")
	private String projectId;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("detail")
	private String detail;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("type")
	private String type;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("type_txt")
	private String typeTxt;

	@SerializedName("gallery")
	private List<GalleryItem> gallery;

	public void setProjectId(String projectId){
		this.projectId = projectId;
	}

	public String getProjectId(){
		return projectId;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setTypeTxt(String typeTxt){
		this.typeTxt = typeTxt;
	}

	public String getTypeTxt(){
		return typeTxt;
	}

	public void setGallery(List<GalleryItem> gallery){
		this.gallery = gallery;
	}

	public List<GalleryItem> getGallery(){
		return gallery;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"project_id = '" + projectId + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",detail = '" + detail + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",type = '" + type + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",type_txt = '" + typeTxt + '\'' + 
			",gallery = '" + gallery + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.projectId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.detail);
		dest.writeString(this.projectName);
		dest.writeString(this.type);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.typeTxt);
		dest.writeTypedList(this.gallery);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.projectId = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.detail = in.readString();
		this.projectName = in.readString();
		this.type = in.readString();
		this.addedDatetime = in.readString();
		this.typeTxt = in.readString();
		this.gallery = in.createTypedArrayList(GalleryItem.CREATOR);
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}