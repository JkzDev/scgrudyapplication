package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 6/2/2018 AD.
 */

public class QTListModel {
    /**
     * id : 215
     * project_id : null
     * shop_id : null
     * user_id : null
     * pr_code : null
     * pr_id : 215
     * qo_number : 96
     * revision : 0
     * revise_from_id : null
     * status : null
     * remark : hello
     * month : null
     * year : null
     * shipping_type : 0
     * shipping_status : 0
     * shipping_cost : 0
     * discount_percentage : null
     * vat_percentage : 0
     * create_date : 2018-03-19 15:57:51
     * qo_code : QO6103-0096
     * discout_percentage : null
     */
    public String id;
    public String project_id;
    public String shop_id;
    public String user_id;
    public String pr_code;
    public String pr_id;
    public String qo_number;
    public String revision;
    public String revise_from_id;
    public String status;
    public String remark;
    public String month;
    public String year;
    public String shipping_type;
    public String shipping_status;
    public String shipping_cost;
    public String discount_percentage;
    public String vat_percentage;
    public String create_date;
    public String qo_code;
    public String discout_percentage;
    
    public QTListModel (
            String id,
            String project_id,
            String shop_id,
            String user_id,
            String pr_code,
            String pr_id,
            String qo_number,
            String revision,
            String revise_from_id,
            String status,
            String remark,
            String month,
            String year,
            String shipping_type,
            String shipping_status,
            String shipping_cost,
            String discount_percentage,
            String vat_percentage,
            String create_date,
            String qo_code,
            String discout_percentage
    ){
        this.id = id;
        this.project_id = project_id;
        this.shop_id = shop_id;
        this.user_id = user_id;
        this.pr_code = pr_code;
        this.pr_id = pr_id;
        this.qo_number = qo_number;
        this.revision = revision;
        this.revise_from_id = revise_from_id;
        this.status = status;
        this.remark = remark;
        this.month = month;
        this.year = year;
        this.shipping_type = shipping_type;
        this.shipping_status = shipping_status;
        this.shipping_cost = shipping_cost;
        this.discount_percentage = discount_percentage;
        this.vat_percentage = vat_percentage;
        this.create_date = create_date;
        this.qo_code = qo_code;
        this.discout_percentage = discout_percentage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPr_code() {
        return pr_code;
    }

    public void setPr_code(String pr_code) {
        this.pr_code = pr_code;
    }

    public String getPr_id() {
        return pr_id;
    }

    public void setPr_id(String pr_id) {
        this.pr_id = pr_id;
    }

    public String getQo_number() {
        return qo_number;
    }

    public void setQo_number(String qo_number) {
        this.qo_number = qo_number;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getRevise_from_id() {
        return revise_from_id;
    }

    public void setRevise_from_id(String revise_from_id) {
        this.revise_from_id = revise_from_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getShipping_type() {
        return shipping_type;
    }

    public void setShipping_type(String shipping_type) {
        this.shipping_type = shipping_type;
    }

    public String getShipping_status() {
        return shipping_status;
    }

    public void setShipping_status(String shipping_status) {
        this.shipping_status = shipping_status;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(String shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public String getDiscount_percentage() {
        return discount_percentage;
    }

    public void setDiscount_percentage(String discount_percentage) {
        this.discount_percentage = discount_percentage;
    }

    public String getVat_percentage() {
        return vat_percentage;
    }

    public void setVat_percentage(String vat_percentage) {
        this.vat_percentage = vat_percentage;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getQo_code() {
        return qo_code;
    }

    public void setQo_code(String qo_code) {
        this.qo_code = qo_code;
    }

    public String getDiscout_percentage() {
        return discout_percentage;
    }

    public void setDiscout_percentage(String discout_percentage) {
        this.discout_percentage = discout_percentage;
    }
}

