package com.scg.rudy.model.pojo.contractType;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ContractTypePojo implements Parcelable {

	@SerializedName("items")
	public List<ItemsItem> items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.items);
		dest.writeInt(this.status);
	}

	public ContractTypePojo() {
	}

	protected ContractTypePojo(Parcel in) {
		this.items = in.createTypedArrayList(ItemsItem.CREATOR);
		this.status = in.readInt();
	}

	public static final Creator<ContractTypePojo> CREATOR = new Creator<ContractTypePojo>() {
		@Override
		public ContractTypePojo createFromParcel(Parcel source) {
			return new ContractTypePojo(source);
		}

		@Override
		public ContractTypePojo[] newArray(int size) {
			return new ContractTypePojo[size];
		}
	};
}