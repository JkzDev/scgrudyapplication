package com.scg.rudy.model.pojo.dealbyproject.classsku;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SkuItem implements Parcelable {

	@SerializedName("promotion_remain_day")
	private String promotionRemainDay;

	@SerializedName("BOQ")
	private String bOQ;

	@SerializedName("class_id")
	private String classId;

	@SerializedName("recommend")
	private String recommend;

	@SerializedName("pic")
	private String pic;

	@SerializedName("hot")
	private int hot;

	@SerializedName("ispromotion")
	private String ispromotion;

	@SerializedName("unit")
	private String unit;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("promotion_detail_id")
	private String promotionDetailId;

	@SerializedName("fav")
	private int fav;

	@SerializedName("commission")
	private String commission;

	@SerializedName("id")
	private String id;

	@SerializedName("used2use")
	private int used2use;

	@SerializedName("stock")
	private String stock;

	@SerializedName("promotion_remain_qty")
	private String promotionRemainQty;

	public void setPromotionRemainDay(String promotionRemainDay){
		this.promotionRemainDay = promotionRemainDay;
	}

	public String getPromotionRemainDay(){
		return promotionRemainDay;
	}

	public void setBOQ(String bOQ){
		this.bOQ = bOQ;
	}

	public String getBOQ(){
		return bOQ;
	}

	public void setClassId(String classId){
		this.classId = classId;
	}

	public String getClassId(){
		return classId;
	}

	public void setRecommend(String recommend){
		this.recommend = recommend;
	}

	public String getRecommend(){
		return recommend;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setHot(int hot){
		this.hot = hot;
	}

	public int getHot(){
		return hot;
	}

	public void setIspromotion(String ispromotion){
		this.ispromotion = ispromotion;
	}

	public String getIspromotion(){
		return ispromotion;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPromotionDetailId(String promotionDetailId){
		this.promotionDetailId = promotionDetailId;
	}

	public String getPromotionDetailId(){
		return promotionDetailId;
	}

	public void setFav(int fav){
		this.fav = fav;
	}

	public int getFav(){
		return fav;
	}

	public void setCommission(String commission){
		this.commission = commission;
	}

	public String getCommission(){
		return commission;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUsed2use(int used2use){
		this.used2use = used2use;
	}

	public int getUsed2use(){
		return used2use;
	}

	public void setStock(String stock){
		this.stock = stock;
	}

	public String getStock(){
		return stock;
	}

	public void setPromotionRemainQty(String promotionRemainQty){
		this.promotionRemainQty = promotionRemainQty;
	}

	public String getPromotionRemainQty(){
		return promotionRemainQty;
	}

	@Override
 	public String toString(){
		return
			"SkuItem{" +
			"promotion_remain_day = '" + promotionRemainDay + '\'' +
			",bOQ = '" + bOQ + '\'' +
			",class_id = '" + classId + '\'' +
			",recommend = '" + recommend + '\'' +
			",pic = '" + pic + '\'' +
			",hot = '" + hot + '\'' +
			",ispromotion = '" + ispromotion + '\'' +
			",unit = '" + unit + '\'' +
			",price = '" + price + '\'' +
			",name = '" + name + '\'' +
			",promotion_detail_id = '" + promotionDetailId + '\'' +
			",fav = '" + fav + '\'' +
			",commission = '" + commission + '\'' +
			",id = '" + id + '\'' +
			",used2use = '" + used2use + '\'' +
			",stock = '" + stock + '\'' +
			",promotion_remain_qty = '" + promotionRemainQty + '\'' +
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.promotionRemainDay);
		dest.writeString(this.bOQ);
		dest.writeString(this.classId);
		dest.writeString(this.recommend);
		dest.writeString(this.pic);
		dest.writeInt(this.hot);
		dest.writeString(this.ispromotion);
		dest.writeString(this.unit);
		dest.writeString(this.price);
		dest.writeString(this.name);
		dest.writeString(this.promotionDetailId);
		dest.writeInt(this.fav);
		dest.writeString(this.commission);
		dest.writeString(this.id);
		dest.writeInt(this.used2use);
		dest.writeString(this.stock);
		dest.writeString(this.promotionRemainQty);
	}

	public SkuItem() {
	}
	public SkuItem(
			String promotionRemainDay,
			String bOQ,
			String classId,
			String recommend,
			String pic,
			int hot,
			String ispromotion,
			String unit,
			String price,
			String name,
			String promotionDetailId,
			int fav,
			String commission,
			String id,
			int used2use,
			String stock,
			String promotionRemainQty
	) {
		this.promotionRemainDay =promotionRemainDay;
		this.bOQ =bOQ;
		this.classId =classId;
		this.recommend =recommend;
		this.pic =pic;
		this.hot =hot;
		this.ispromotion =ispromotion;
		this.unit =unit ;
		this.price = price;
		this.name = name;
		this.promotionDetailId = promotionDetailId;
		this.fav =fav;
		this.commission  =commission;
		this.id = id;
		this.used2use =used2use;
		this.stock = stock;
		this.promotionRemainQty = promotionRemainQty;


	}




	protected SkuItem(Parcel in) {
		this.promotionRemainDay = in.readString();
		this.bOQ = in.readString();
		this.classId = in.readString();
		this.recommend = in.readString();
		this.pic = in.readString();
		this.hot = in.readInt();
		this.ispromotion = in.readString();
		this.unit = in.readString();
		this.price = in.readString();
		this.name = in.readString();
		this.promotionDetailId = in.readString();
		this.fav = in.readInt();
		this.commission = in.readString();
		this.id = in.readString();
		this.used2use = in.readInt();
		this.stock = in.readString();
		this.promotionRemainQty = in.readString();
	}

	public static final Parcelable.Creator<SkuItem> CREATOR = new Parcelable.Creator<SkuItem>() {
		@Override
		public SkuItem createFromParcel(Parcel source) {
			return new SkuItem(source);
		}

		@Override
		public SkuItem[] newArray(int size) {
			return new SkuItem[size];
		}
	};
}