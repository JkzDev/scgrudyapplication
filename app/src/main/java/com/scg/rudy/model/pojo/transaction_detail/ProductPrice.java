package com.scg.rudy.model.pojo.transaction_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProductPrice implements Parcelable {

	@SerializedName("price")
	private String price;

	@SerializedName("price3")
	private String price3;

	@SerializedName("price2")
	private String price2;

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setPrice3(String price3){
		this.price3 = price3;
	}

	public String getPrice3(){
		return price3;
	}

	public void setPrice2(String price2){
		this.price2 = price2;
	}

	public String getPrice2(){
		return price2;
	}

	@Override
 	public String toString(){
		return 
			"ProductPrice{" + 
			"price = '" + price + '\'' + 
			",price3 = '" + price3 + '\'' + 
			",price2 = '" + price2 + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.price);
		dest.writeString(this.price3);
		dest.writeString(this.price2);
	}

	public ProductPrice() {
	}

	protected ProductPrice(Parcel in) {
		this.price = in.readString();
		this.price3 = in.readString();
		this.price2 = in.readString();
	}

	public static final Parcelable.Creator<ProductPrice> CREATOR = new Parcelable.Creator<ProductPrice>() {
		@Override
		public ProductPrice createFromParcel(Parcel source) {
			return new ProductPrice(source);
		}

		@Override
		public ProductPrice[] newArray(int size) {
			return new ProductPrice[size];
		}
	};
}