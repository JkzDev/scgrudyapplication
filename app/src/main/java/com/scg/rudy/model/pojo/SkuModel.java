package com.scg.rudy.model.pojo;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 17/1/2018 AD.
 */

public class SkuModel {

    /**
     * status : 200
     * items : [{"id":"1","item_name":"Cpac Super Plus กำลังอัด 280 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"2","item_name":"คอนกรีตกำลังอัด 380 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"3","item_name":"คอนกรีตหยาบ","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"4","item_name":"มอร์ตาร์สำหรับเคลือบท่อปั๊ม ขนาดปูน 300กล./ลบ.ม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"5","item_name":"คอนกรีตกันซึม กำลังอัด 300 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"6","item_name":"คอนกรีตกันซึมกำลังอัด 280 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"7","item_name":"คอนกรีตกำลังอัด 180 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"8","item_name":"คอนกรีตกันซึม กำลังอัด 280 กก./ตร.ซม.(ปั๊ม)","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"9","item_name":"คอนกรีตรถโม่เล็ก กำลังอัด 380 Ksc (CUBE)","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"10","item_name":"คอนกรีตกำลังอัด 180 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"11","item_name":"คอนกรีตกำลังอัด 240 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"13","item_name":"คอนกรีตกำลังอัด 280 กก./ตร.ซม. รถเล็ก","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"14","item_name":"คอนกรีตกำลังอัด 240 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"15","item_name":"คอนกรีตหยาบ","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"18","item_name":"คอนกรีตกำลังอัด 240 กก./ตร.ซม. เท 13/3/59","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"23","item_name":"คอนกรีตหยาบ","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"24","item_name":"คอนกรีตกันซึม กำลังอัด 320 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"25","item_name":"คอนกรีตรถโม่เล็ก กำลังอัด 320 Ksc (CUBE)","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"26","item_name":"คอนกรีตกันซึม กำลังอัด 320 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"28","item_name":"คอนกรีตรถโม่เล็ก กำลังอัด 180 Ksc (CUBE)","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"35","item_name":"คอนกรีตหยาบ onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"36","item_name":"คอนกรีตกำลังอัด 280 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"37","item_name":"คอนกรีตกันซึม 240 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"38","item_name":"คอนกรีตกำลังอัด 240 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"39","item_name":"คอนกรีตงานชายฝั่งทะเล กำลังอัด 380 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"41","item_name":"คอนกรีตงานชายฝั่งทะเล กำลังอัด 380 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"42","item_name":"คอนกรีตกำลังอัด 180 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"44","item_name":"คอนกรีตกำลังอัด 380 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"45","item_name":"Cpac Super Plus กำลังอัด 240 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"47","item_name":"คอนกรีตพิมพ์ลาย กำลังอัด 240 ksc","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"48","item_name":"มอร์ต้าสำหรับเคลือบท่อปั๊ม ขนาดปูน 500 กก./ลบ.ม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"49","item_name":"Cpac Super Plus กำลังอัด 240 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"50","item_name":"คอนกรีตกำลังอัด 280 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"52","item_name":"คอนกรีตกันซึมกำลังอัด 320 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"53","item_name":"คอนกรีตกำลังอัด 280 กก./ตร.ซม. OnSite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"54","item_name":"คอนกรีตกำลังอัด 180 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"58","item_name":"มอร์ต้าสำหรับเคลือบท่อปั๊ม ขนาดปูน 500 กก./ลบ.ม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"59","item_name":"คอนกรีตกำลังอัด 280 กก./ตร.ซม. onsite","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"60","item_name":"คอนกรีตทับหน้ากำลังอัด 400 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"61","item_name":"คอนกรีตงานชายฝั่งทะเล กำลังอัด 380 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"62","item_name":"คอนกรีตกำลังอัด 180 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"64","item_name":"คอนกรีตแข็งตัวเร็ว 24 ชม.กำลังอัด 320 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"65","item_name":"คอนกรีตหยาบ LN02150","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"66","item_name":"คอนกรีตทับหน้ากำลังอัด 280 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"68","item_name":"คอนกรีตแข็งตัวเร็ว 24 ชม.กำลังอัด 240 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"},{"id":"69","item_name":"คอนกรีตงานพื้นดาดฟ้าซีแพคกำลังอัด 280 กก./ตร.ซม.","price1":"0.00","bestsell":"0","recommend":"1"}]
     */

    private ArrayList<skuItems> items;

    public ArrayList<skuItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<skuItems> items) {
        this.items = items;
    }

    public static class skuItems {
        /**
         * id : 1
         * item_name : Cpac Super Plus กำลังอัด 280 กก./ตร.ซม.
         * price1 : 0.00
         * bestsell : 0
         * recommend : 1
         */
        public skuItems(
                 String id,
                 String item_name,
                 String price1,
                 String bestsell,
                 String recommend,
                 String unit_code,
                 String stock
        ){
            this.id = id;
            this.item_name = item_name;
            this.price1 = price1;
            this.bestsell = bestsell;
            this.recommend = recommend;
            this.unit_code = unit_code;
            this.stock = stock;
        }

        private String id;
        private String item_name;
        private String price1;
        private String bestsell;
        private String recommend;
        private String unit_code;
        private String stock;

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getUnit_code() {
            return unit_code;
        }

        public void setUnit_code(String unit_code) {
            this.unit_code = unit_code;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public String getPrice1() {
            return price1;
        }

        public void setPrice1(String price1) {
            this.price1 = price1;
        }

        public String getBestsell() {
            return bestsell;
        }

        public void setBestsell(String bestsell) {
            this.bestsell = bestsell;
        }

        public String getRecommend() {
            return recommend;
        }

        public void setRecommend(String recommend) {
            this.recommend = recommend;
        }
    }
}
