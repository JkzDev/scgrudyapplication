package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 12/11/2017 AD.
 */

@SuppressWarnings("ALL")
public class ByProjectModel {
    String id;
    String name;
    String phase_id;
    String latest_bill;
    String latest_bill_text;
    String blueprint;
    String blueprint_txt;
    String place;
    String assignment;
    String opportunity;
    String starorder;
    String newunit;
    String phase_name;

    public ByProjectModel(
            String id,
            String name,
            String phase_id,
            String latest_bill,
            String latest_bill_text,
            String blueprint,
            String blueprint_txt,
            String place,
            String assignment,
            String opportunity,
            String starorder,
            String newunit,
            String phase_name
    ){
        this.id = id;
        this.name = name;
        this.phase_id = phase_id;
        this.latest_bill = latest_bill;
        this.latest_bill_text = latest_bill_text;
        this.blueprint = blueprint;
        this.blueprint_txt = blueprint_txt;
        this.place = place;
        this.assignment = assignment;
        this.opportunity = opportunity;
        this.starorder = starorder;
        this.newunit = newunit;
        this.phase_name = phase_name;


    }

    public String getStarorder() {
        return starorder;
    }

    public void setStarorder(String starorder) {
        this.starorder = starorder;
    }

    public String getNewunit() {
        return newunit;
    }

    public void setNewunit(String newunit) {
        this.newunit = newunit;
    }

    public String getPhase_name() {
        return phase_name;
    }

    public void setPhase_name(String phase_name) {
        this.phase_name = phase_name;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhase_id() {
        return phase_id;
    }

    public void setPhase_id(String phase_id) {
        this.phase_id = phase_id;
    }

    public String getLatest_bill() {
        return latest_bill;
    }

    public void setLatest_bill(String latest_bill) {
        this.latest_bill = latest_bill;
    }

    public String getLatest_bill_text() {
        return latest_bill_text;
    }

    public void setLatest_bill_text(String latest_bill_text) {
        this.latest_bill_text = latest_bill_text;
    }

    public String getBlueprint() {
        return blueprint;
    }

    public void setBlueprint(String blueprint) {
        this.blueprint = blueprint;
    }

    public String getBlueprint_txt() {
        return blueprint_txt;
    }

    public void setBlueprint_txt(String blueprint_txt) {
        this.blueprint_txt = blueprint_txt;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(String opportunity) {
        this.opportunity = opportunity;
    }
}
