package com.scg.rudy.model.pojo.projectgroup;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubphasesListItem implements Parcelable {

	@SerializedName("name")
	public String name;

	@SerializedName("id")
	public String id;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public SubphasesListItem() {
	}

	protected SubphasesListItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Creator<SubphasesListItem> CREATOR = new Creator<SubphasesListItem>() {
		@Override
		public SubphasesListItem createFromParcel(Parcel source) {
			return new SubphasesListItem(source);
		}

		@Override
		public SubphasesListItem[] newArray(int size) {
			return new SubphasesListItem[size];
		}
	};
}