package com.scg.rudy.model.pojo;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 4/1/2018 AD.
 */

public class ViewDetailModel {
    public String shop = "";
    public String service_info_logistic = "";
    public String lng = "";
    public String service_info_claim = "";
    public Customer customer;
    public Customer_owner customer_owner;
    public String group_of_non_purchase_product = "";
    public String id = "";
    public String last_update = "";
    public String rate = "";
    public String unit_budget = "";
    public String type_of_contact = "";
    public String phase_duration = "";
    public String latest_bill_value = "";
    public String user_id = "";
    public String service_info_promotion = "";
    public String phase_id = "";
    public String place = "";
    public String lat = "";
    public String latest_bill = "";
    public String unit_number = "";
    public String service_info_credit = "";
    public String floor_number = "";
    public String shop_id = "";
    public String status = "";
    public String cate_id = "";
    public String service_info_service = "";
    public Project project ;
    public String service_info_staff = "";
    public String sub_phases_list = "";
    public String service_info_product = "";
    public String unit_area = "";
    public Opportunities_all opportunities_all;
    public String unit_status = "";
    public String address = "";
    public String service_info_price = "";
    public String unit_type = "";
    public String blueprint = "";
    public String unit_name = "";
    public String customer_id = "";
    public ArrayList<Pr_code> arrayPr_code;
    public String  customer_id_owner;


    public ViewDetailModel(
            String shop,
            String service_info_logistic,
            String lng,
            String service_info_claim,
            Customer customer,
            Customer_owner customer_owner,
            String group_of_non_purchase_product,
            String id,
            String last_update,
            String rate,
            String unit_budget,
            String type_of_contact,
            String phase_duration,
            String latest_bill_value,
            String user_id,
            String service_info_promotion,
            String phase_id,
            String place,
            String lat,
            String latest_bill,
            String unit_number,
            String service_info_credit,
            String floor_number,
            String shop_id,
            String status,
            String cate_id,
            String service_info_service,
            Project project,
            String service_info_staff,
            String sub_phases_list,
            String service_info_product,
            String unit_area,
            Opportunities_all opportunities_all,
            String unit_status,
            String address,
            String service_info_price,
            String unit_type,
            String blueprint,
            String unit_name,
            String customer_id,
            ArrayList<Pr_code> arrayPr_code,
            String customer_id_owner){
        super();

        this.shop = shop;
        this.service_info_logistic = service_info_logistic;
        this.lng = lng;
        this.service_info_claim =service_info_claim;
        this. customer = customer;
        this.customer_owner = customer_owner;
        this.group_of_non_purchase_product = group_of_non_purchase_product;
        this.id = id;
        this.last_update = last_update;
        this.rate = rate;
        this.unit_budget = unit_budget;
        this.type_of_contact = type_of_contact;
        this.phase_duration = phase_duration;
        this.latest_bill_value = latest_bill_value;
        this.user_id = user_id;
        this.service_info_promotion = service_info_promotion;
        this.phase_id = phase_id;
        this.place = place;
        this.lat = lat;
        this.latest_bill = latest_bill;
        this.unit_number = unit_number;
        this.service_info_credit = service_info_credit;
        this.floor_number = floor_number;
        this.shop_id = shop_id;
        this.status = status;
        this.cate_id = cate_id;
        this.service_info_service =service_info_service;
        this.project = project;
        this.service_info_staff = service_info_staff;
        this.sub_phases_list = sub_phases_list;
        this.service_info_product = service_info_product;
        this.unit_area = unit_area;
        this.opportunities_all = opportunities_all;
        this.unit_status = unit_status;
        this.address = address;
        this.service_info_price = service_info_price;
        this.unit_type = unit_type;
        this.blueprint = blueprint;
        this.unit_name = unit_name;
        this.customer_id = customer_id;
        this.arrayPr_code = arrayPr_code;
        this.customer_id_owner = customer_id_owner;
    }

    public String getCustomer_id_owner() {
        return customer_id_owner;
    }

    public void setCustomer_id_owner(String customer_id_owner) {
        this.customer_id_owner = customer_id_owner;
    }

    public ArrayList<Pr_code> getArrayPr_code() {
        return arrayPr_code;
    }

    public void setArrayPr_code(ArrayList<Pr_code> arrayPr_code) {
        this.arrayPr_code = arrayPr_code;
    }

    public Customer_owner getCustomer_owner() {
        return customer_owner;
    }

    public void setCustomer_owner(Customer_owner customer_owner) {
        this.customer_owner = customer_owner;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getService_info_logistic() {
        return service_info_logistic;
    }

    public void setService_info_logistic(String service_info_logistic) {
        this.service_info_logistic = service_info_logistic;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getService_info_claim() {
        return service_info_claim;
    }

    public void setService_info_claim(String service_info_claim) {
        this.service_info_claim = service_info_claim;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getGroup_of_non_purchase_product() {
        return group_of_non_purchase_product;
    }

    public void setGroup_of_non_purchase_product(String group_of_non_purchase_product) {
        this.group_of_non_purchase_product = group_of_non_purchase_product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getUnit_budget() {
        return unit_budget;
    }

    public void setUnit_budget(String unit_budget) {
        this.unit_budget = unit_budget;
    }

    public String getType_of_contact() {
        return type_of_contact;
    }

    public void setType_of_contact(String type_of_contact) {
        this.type_of_contact = type_of_contact;
    }

    public String getPhase_duration() {
        return phase_duration;
    }

    public void setPhase_duration(String phase_duration) {
        this.phase_duration = phase_duration;
    }

    public String getLatest_bill_value() {
        return latest_bill_value;
    }

    public void setLatest_bill_value(String latest_bill_value) {
        this.latest_bill_value = latest_bill_value;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getService_info_promotion() {
        return service_info_promotion;
    }

    public void setService_info_promotion(String service_info_promotion) {
        this.service_info_promotion = service_info_promotion;
    }

    public String getPhase_id() {
        return phase_id;
    }

    public void setPhase_id(String phase_id) {
        this.phase_id = phase_id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLatest_bill() {
        return latest_bill;
    }

    public void setLatest_bill(String latest_bill) {
        this.latest_bill = latest_bill;
    }

    public String getUnit_number() {
        return unit_number;
    }

    public void setUnit_number(String unit_number) {
        this.unit_number = unit_number;
    }

    public String getService_info_credit() {
        return service_info_credit;
    }

    public void setService_info_credit(String service_info_credit) {
        this.service_info_credit = service_info_credit;
    }

    public String getFloor_number() {
        return floor_number;
    }

    public void setFloor_number(String floor_number) {
        this.floor_number = floor_number;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCate_id() {
        return cate_id;
    }

    public void setCate_id(String cate_id) {
        this.cate_id = cate_id;
    }

    public String getService_info_service() {
        return service_info_service;
    }

    public void setService_info_service(String service_info_service) {
        this.service_info_service = service_info_service;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getService_info_staff() {
        return service_info_staff;
    }

    public void setService_info_staff(String service_info_staff) {
        this.service_info_staff = service_info_staff;
    }

    public String getSub_phases_list() {
        return sub_phases_list;
    }

    public void setSub_phases_list(String sub_phases_list) {
        this.sub_phases_list = sub_phases_list;
    }

    public String getService_info_product() {
        return service_info_product;
    }

    public void setService_info_product(String service_info_product) {
        this.service_info_product = service_info_product;
    }

    public String getUnit_area() {
        return unit_area;
    }

    public void setUnit_area(String unit_area) {
        this.unit_area = unit_area;
    }

    public Opportunities_all getOpportunities_all() {
        return opportunities_all;
    }

    public void setOpportunities_all(Opportunities_all opportunities_all) {
        this.opportunities_all = opportunities_all;
    }

    public String getUnit_status() {
        return unit_status;
    }

    public void setUnit_status(String unit_status) {
        this.unit_status = unit_status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getService_info_price() {
        return service_info_price;
    }

    public void setService_info_price(String service_info_price) {
        this.service_info_price = service_info_price;
    }

    public String getUnit_type() {
        return unit_type;
    }

    public void setUnit_type(String unit_type) {
        this.unit_type = unit_type;
    }

    public String getBlueprint() {
        return blueprint;
    }

    public void setBlueprint(String blueprint) {
        this.blueprint = blueprint;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public static class Customer {
        public String id;
        public String last_update;
        public String phone;
        public String company;
        public String name;
        public String img;
        public String last_name;
        public String line;
        public String type;
        public String note;

        public Customer( 
                 String id,
                 String last_update,
                 String phone,
                 String company,
                 String name,
                 String img,
                 String last_name,
                 String line,
                 String type,
                 String note){
            super();
            this.id =id;
            this.last_update =last_update;
            this.phone = phone;
            this.company =company;
            this.name =name;
            this.img = img;
            this.last_name =last_name;
            this.line = line;
            this.type = type;
            this.note = note;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class Customer_owner{
        public String id;
        public String last_update;
        public String phone;
        public String company;
        public String name;
        public String img;
        public String last_name;
        public String line;
        public String type;
        public String note;
        public Customer_owner(
                String id,
                String last_update,
                String phone,
                String company,
                String name,
                String img,
                String last_name,
                String line,
                String type,
                String note){
            super();
            this.id =id;
            this.last_update =last_update;
            this.phone = phone;
            this.company =company;
            this.name =name;
            this.img = img;
            this.last_name =last_name;
            this.line = line;
            this.type = type;
            this.note = note;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class Project {
        public String img1;
        public String img2;
        public String img3;
        public String img4;
        public String img5;
        public String img6;
        
        public Project(
                String img1,
                String img2,
                String img3,
                String img4,
                String img5,
                String img6){
            this.img1 =img1;
            this.img2 = img2;
            this.img3 =img3;
            this.img4 =img4;
            this.img5 = img5;
            this.img6 = img6;
            
            
        }
        

        public String getImg1() {
            return img1;
        }

        public void setImg1(String img1) {
            this.img1 = img1;
        }

        public String getImg2() {
            return img2;
        }

        public void setImg2(String img2) {
            this.img2 = img2;
        }

        public String getImg3() {
            return img3;
        }

        public void setImg3(String img3) {
            this.img3 = img3;
        }

        public String getImg4() {
            return img4;
        }

        public void setImg4(String img4) {
            this.img4 = img4;
        }

        public String getImg5() {
            return img5;
        }

        public void setImg5(String img5) {
            this.img5 = img5;
        }

        public String getImg6() {
            return img6;
        }

        public void setImg6(String img6) {
            this.img6 = img6;
        }
    }

    public static class Opportunities_all {
        public ArrayList<Phase> phase;
        public Current_phase current_phase;
        
        public Opportunities_all(
                ArrayList<Phase> phase,
                Current_phase current_phase){
            super();
            this.phase = phase;
            this.current_phase = current_phase;
            
        }

        public ArrayList<Phase> getPhase() {
            return phase;
        }

        public void setPhase(ArrayList<Phase> phase) {
            this.phase = phase;
        }

        public Current_phase getCurrent_phase() {
            return current_phase;
        }

        public void setCurrent_phase(Current_phase current_phase) {
            this.current_phase = current_phase;
        }
    }

    public static class Phase {
        public String phase_opp;
        public String phase_status;
        public String phase_id;
        public String phase_name;
        public String phase_sales;

        public Phase(
                 String phase_opp,
                 String phase_status,
                 String phase_id,
                 String phase_name,
                 String phase_sales){
            super();
            this.phase_opp = phase_opp;
            this.phase_status = phase_status;
            this.phase_id = phase_id;
            this.phase_name = phase_name;
            this.phase_sales = phase_sales;

        }


        public String getPhase_opp() {
            return phase_opp;
        }

        public void setPhase_opp(String phase_opp) {
            this.phase_opp = phase_opp;
        }

        public String getPhase_status() {
            return phase_status;
        }

        public void setPhase_status(String phase_status) {
            this.phase_status = phase_status;
        }

        public String getPhase_id() {
            return phase_id;
        }

        public void setPhase_id(String phase_id) {
            this.phase_id = phase_id;
        }

        public String getPhase_name() {
            return phase_name;
        }

        public void setPhase_name(String phase_name) {
            this.phase_name = phase_name;
        }

        public String getPhase_sales() {
            return phase_sales;
        }

        public void setPhase_sales(String phase_sales) {
            this.phase_sales = phase_sales;
        }
    }

    public static class Current_phase {
        public String phase_opp;
        public String phase;
        public String phase_balance;

        public Current_phase(
                 String phase_opp,
                 String phase,
                 String phase_balance){
            super();
            this.phase_opp = phase_opp;
            this.phase = phase;
            this.phase_balance = phase_balance;
        }
        
        
        public String getPhase_opp() {
            return phase_opp;
        }

        public void setPhase_opp(String phase_opp) {
            this.phase_opp = phase_opp;
        }

        public String getPhase() {
            return phase;
        }

        public void setPhase(String phase) {
            this.phase = phase;
        }

        public String getPhase_balance() {
            return phase_balance;
        }

        public void setPhase_balance(String phase_balance) {
            this.phase_balance = phase_balance;
        }
    }

    public static class Pr_code{
        String pr_code;
        String created;
        String last_update;

        public  Pr_code(
                String pr_code,
                String created,
                String last_update
        ){
            this.pr_code = pr_code;
            this.created = created;
            this.last_update = last_update;
        }


        public String getPr_code() {
            return pr_code;
        }

        public void setPr_code(String pr_code) {
            this.pr_code = pr_code;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }
    }

}
