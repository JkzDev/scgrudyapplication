package com.scg.rudy.model.pojo.project_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.scg.rudy.model.pojo.byproject.CustomerPhoneListItem;
import com.scg.rudy.ui.project_detail.Models.GalleryModel;

@Generated("com.asif.gsonpojogenerator")
public class Items implements Parcelable {

	@SerializedName("customer_line")
	private String customerLine;

	@SerializedName("pr")
	private List<PrItem> pr;

	@SerializedName("phaseprocess")
	private List<PhaseprocessItem> phaseprocess;

	@SerializedName("rank_name")
	private String rankName;

	@SerializedName("project_owner_phone")
	private String projectOwnerPhone;

	@SerializedName("customer_pic")
	private String customerPic;

	@SerializedName("customer_note")
	private String customerNote;



	@SerializedName("project_name")
	private String projectName;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("house_owner_phone")
	private String houseOwnerPhone;

	@SerializedName("phase_work_progress")
	private String phaseWorkProgress;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("rank")
	private String rank;

	@SerializedName("id")
	private String id;

	@SerializedName("customer_code")
	private String customerCode;

	@SerializedName("champ_customer_code")
	private String champCustomerCode;

	public List<String> getGallery() {
		return gallery;
	}

	public void setGallery(List<String> gallery) {
		this.gallery = gallery;
	}

	public List<GalleryModel> getGalleryNew() {
		return galleryNew;
	}

	public void setGalleryNew(List<GalleryModel> galleryNew) {
		this.galleryNew = galleryNew;
	}

	@SerializedName("gallery")
	private List<String> gallery;

	@SerializedName("galleryNew")
	private List<GalleryModel> galleryNew;

	@SerializedName("add_by_user")
	private String addByUser;

	@SerializedName("customer_phone_extension")
	private String customerPhoneExtension;

	@SerializedName("project_address")
	private String projectAddress;

	@SerializedName("customer_phone")
	private String customerPhone;

	@SerializedName("project_owner_line")
	private String projectOwnerLine;

	@SerializedName("project_stories")
	private String projectStories;

	@SerializedName("work_progress")
	private String workProgress;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("project_owner_note")
	private String projectOwnerNote;

	@SerializedName("house_owner_phone_extension")
	private String houseOwnerPhoneExtension;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("project_owner_name")
	private String projectOwnerName;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("house_owner_note")
	private String houseOwnerNote;

	@SerializedName("customer_email")
	private String customerEmail;

	@SerializedName("house_owner_line")
	private String houseOwnerLine;

	@SerializedName("unit_area")
	private String unitArea;

	@SerializedName("phase_name")
	private String phaseName;

	@SerializedName("project_type_name")
	private String projectTypeName;

	@SerializedName("status")
	private String status;

	@SerializedName("trans")
	private String trans;

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("is_private")
	private String isPrivate;

	@SerializedName("note")
	private List<NoteItem> note;

	@SerializedName("project_owner_phone_extension")
	private String projectOwnerPhoneExtension;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("units")
	private String units;

	@SerializedName("customer_type_txt")
	private String customerTypeTxt;

	@SerializedName("house_owner_name")
	private String houseOwnerName;

	@SerializedName("sell_complete_percent")
	private int sellCompletePercent;

	@SerializedName("customer_company")
	private String customerCompany;

	@SerializedName("opportunity_n")
	private double opportunityN;

	@SerializedName("project_type_id")
	private String projectTypeId;

	@SerializedName("customer_tax_no")
	private String customerTaxNo;

	@SerializedName("phase_pic")
	private String phasePic;

	@SerializedName("trans_n")
	private String transN;

	@SerializedName("province_id")
	private String provinceId;

	@SerializedName("audios")
	private List<AudiosItem> audios;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("owner_type")
	private String ownerType;

	@SerializedName("customer_phone_list")
	private List<CustomerPhoneListItem> customerPhoneList;

	public List<CustomerPhoneListItem> getCustomerPhoneList() {
		return customerPhoneList;
	}

	public void setCustomerPhoneList(List<CustomerPhoneListItem> customerPhoneList) {
		this.customerPhoneList = customerPhoneList;
	}

	public String getChampCustomerCode() {
		return champCustomerCode;
	}

	public void setChampCustomerCode(String champCustomerCode) {
		this.champCustomerCode = champCustomerCode;
	}

	public void setCustomerLine(String customerLine){
		this.customerLine = customerLine;
	}

	public String getCustomerLine(){
		return customerLine;
	}

	public void setPr(List<PrItem> pr){
		this.pr = pr;
	}

	public List<PrItem> getPr(){
		return pr;
	}

	public void setPhaseprocess(List<PhaseprocessItem> phaseprocess){
		this.phaseprocess = phaseprocess;
	}

	public List<PhaseprocessItem> getPhaseprocess(){
		return phaseprocess;
	}

	public void setRankName(String rankName){
		this.rankName = rankName;
	}

	public String getRankName(){
		return rankName;
	}

	public void setProjectOwnerPhone(String projectOwnerPhone){
		this.projectOwnerPhone = projectOwnerPhone;
	}

	public String getProjectOwnerPhone(){
		return projectOwnerPhone;
	}

	public void setCustomerPic(String customerPic){
		this.customerPic = customerPic;
	}

	public String getCustomerPic(){
		return customerPic;
	}

	public void setCustomerNote(String customerNote){
		this.customerNote = customerNote;
	}

	public String getCustomerNote(){
		return customerNote;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setHouseOwnerPhone(String houseOwnerPhone){
		this.houseOwnerPhone = houseOwnerPhone;
	}

	public String getHouseOwnerPhone(){
		return houseOwnerPhone;
	}

	public void setPhaseWorkProgress(String phaseWorkProgress){
		this.phaseWorkProgress = phaseWorkProgress;
	}

	public String getPhaseWorkProgress(){
		return phaseWorkProgress;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}

	public String getCustomerCode(){
		return customerCode;
	}

	public void setAddByUser(String addByUser){
		this.addByUser = addByUser;
	}

	public String getAddByUser(){
		return addByUser;
	}

	public void setCustomerPhoneExtension(String customerPhoneExtension){
		this.customerPhoneExtension = customerPhoneExtension;
	}

	public String getCustomerPhoneExtension(){
		return customerPhoneExtension;
	}

	public void setProjectAddress(String projectAddress){
		this.projectAddress = projectAddress;
	}

	public String getProjectAddress(){
		return projectAddress;
	}

	public void setCustomerPhone(String customerPhone){
		this.customerPhone = customerPhone;
	}

	public String getCustomerPhone(){
		return customerPhone;
	}

	public void setProjectOwnerLine(String projectOwnerLine){
		this.projectOwnerLine = projectOwnerLine;
	}

	public String getProjectOwnerLine(){
		return projectOwnerLine;
	}

	public void setProjectStories(String projectStories){
		this.projectStories = projectStories;
	}

	public String getProjectStories(){
		return projectStories;
	}

	public void setWorkProgress(String workProgress){
		this.workProgress = workProgress;
	}

	public String getWorkProgress(){
		return workProgress;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setProjectOwnerNote(String projectOwnerNote){
		this.projectOwnerNote = projectOwnerNote;
	}

	public String getProjectOwnerNote(){
		return projectOwnerNote;
	}

	public void setHouseOwnerPhoneExtension(String houseOwnerPhoneExtension){
		this.houseOwnerPhoneExtension = houseOwnerPhoneExtension;
	}

	public String getHouseOwnerPhoneExtension(){
		return houseOwnerPhoneExtension;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setProjectOwnerName(String projectOwnerName){
		this.projectOwnerName = projectOwnerName;
	}

	public String getProjectOwnerName(){
		return projectOwnerName;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setHouseOwnerNote(String houseOwnerNote){
		this.houseOwnerNote = houseOwnerNote;
	}

	public String getHouseOwnerNote(){
		return houseOwnerNote;
	}

	public void setCustomerEmail(String customerEmail){
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail(){
		return customerEmail;
	}

	public void setHouseOwnerLine(String houseOwnerLine){
		this.houseOwnerLine = houseOwnerLine;
	}

	public String getHouseOwnerLine(){
		return houseOwnerLine;
	}

	public void setUnitArea(String unitArea){
		this.unitArea = unitArea;
	}

	public String getUnitArea(){
		return unitArea;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	public void setProjectTypeName(String projectTypeName){
		this.projectTypeName = projectTypeName;
	}

	public String getProjectTypeName(){
		return projectTypeName;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setTrans(String trans){
		this.trans = trans;
	}

	public String getTrans(){
		return trans;
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setIsPrivate(String isPrivate){
		this.isPrivate = isPrivate;
	}

	public String getIsPrivate(){
		return isPrivate;
	}

	public void setNote(List<NoteItem> note){
		this.note = note;
	}

	public List<NoteItem> getNote(){
		return note;
	}

	public void setProjectOwnerPhoneExtension(String projectOwnerPhoneExtension){
		this.projectOwnerPhoneExtension = projectOwnerPhoneExtension;
	}

	public String getProjectOwnerPhoneExtension(){
		return projectOwnerPhoneExtension;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setUnits(String units){
		this.units = units;
	}

	public String getUnits(){
		return units;
	}

	public void setCustomerTypeTxt(String customerTypeTxt){
		this.customerTypeTxt = customerTypeTxt;
	}

	public String getCustomerTypeTxt(){
		return customerTypeTxt;
	}

	public void setHouseOwnerName(String houseOwnerName){
		this.houseOwnerName = houseOwnerName;
	}

	public String getHouseOwnerName(){
		return houseOwnerName;
	}

	public void setSellCompletePercent(int sellCompletePercent){
		this.sellCompletePercent = sellCompletePercent;
	}

	public int getSellCompletePercent(){
		return sellCompletePercent;
	}

	public void setCustomerCompany(String customerCompany){
		this.customerCompany = customerCompany;
	}

	public String getCustomerCompany(){
		return customerCompany;
	}

	public void setOpportunityN(double opportunityN){
		this.opportunityN = opportunityN;
	}

	public double getOpportunityN(){
		return opportunityN;
	}

	public void setProjectTypeId(String projectTypeId){
		this.projectTypeId = projectTypeId;
	}

	public String getProjectTypeId(){
		return projectTypeId;
	}

	public void setCustomerTaxNo(String customerTaxNo){
		this.customerTaxNo = customerTaxNo;
	}

	public String getCustomerTaxNo(){
		return customerTaxNo;
	}

	public void setPhasePic(String phasePic){
		this.phasePic = phasePic;
	}

	public String getPhasePic(){
		return phasePic;
	}

	public void setTransN(String transN){
		this.transN = transN;
	}

	public String getTransN(){
		return transN;
	}

	public void setProvinceId(String provinceId){
		this.provinceId = provinceId;
	}

	public String getProvinceId(){
		return provinceId;
	}

	public void setAudios(List<AudiosItem> audios){
		this.audios = audios;
	}

	public List<AudiosItem> getAudios(){
		return audios;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setOwnerType(String ownerType){
		this.ownerType = ownerType;
	}

	public String getOwnerType(){
		return ownerType;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"customer_line = '" + customerLine + '\'' + 
			",pr = '" + pr + '\'' + 
			",phaseprocess = '" + phaseprocess + '\'' + 
			",rank_name = '" + rankName + '\'' + 
			",project_owner_phone = '" + projectOwnerPhone + '\'' + 
			",customer_pic = '" + customerPic + '\'' + 
			",customer_note = '" + customerNote + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",house_owner_phone = '" + houseOwnerPhone + '\'' + 
			",phase_work_progress = '" + phaseWorkProgress + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",rank = '" + rank + '\'' + 
			",id = '" + id + '\'' + 
			",customer_code = '" + customerCode + '\'' + 
			",gallery = '" + gallery + '\'' + 
			",add_by_user = '" + addByUser + '\'' + 
			",customer_phone_extension = '" + customerPhoneExtension + '\'' + 
			",project_address = '" + projectAddress + '\'' + 
			",customer_phone = '" + customerPhone + '\'' + 
			",project_owner_line = '" + projectOwnerLine + '\'' + 
			",project_stories = '" + projectStories + '\'' + 
			",work_progress = '" + workProgress + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",project_owner_note = '" + projectOwnerNote + '\'' + 
			",house_owner_phone_extension = '" + houseOwnerPhoneExtension + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",project_owner_name = '" + projectOwnerName + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",user_id = '" + userId + '\'' + 
			",house_owner_note = '" + houseOwnerNote + '\'' + 
			",customer_email = '" + customerEmail + '\'' + 
			",house_owner_line = '" + houseOwnerLine + '\'' + 
			",unit_area = '" + unitArea + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			",project_type_name = '" + projectTypeName + '\'' + 
			",status = '" + status + '\'' + 
			",trans = '" + trans + '\'' + 
			",customer_type = '" + customerType + '\'' + 
			",is_private = '" + isPrivate + '\'' + 
			",note = '" + note + '\'' + 
			",project_owner_phone_extension = '" + projectOwnerPhoneExtension + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",units = '" + units + '\'' + 
			",customer_type_txt = '" + customerTypeTxt + '\'' + 
			",house_owner_name = '" + houseOwnerName + '\'' + 
			",sell_complete_percent = '" + sellCompletePercent + '\'' + 
			",customer_company = '" + customerCompany + '\'' + 
			",opportunity_n = '" + opportunityN + '\'' + 
			",project_type_id = '" + projectTypeId + '\'' + 
			",customer_tax_no = '" + customerTaxNo + '\'' + 
			",phase_pic = '" + phasePic + '\'' + 
			",trans_n = '" + transN + '\'' + 
			",province_id = '" + provinceId + '\'' + 
			",audios = '" + audios + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",owner_type = '" + ownerType + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

	}

//	public Items() {
//	}
//
//	@Override
//	public int describeContents() {
//		return 0;
//	}
//
//	@Override
//	public void writeToParcel(Parcel dest, int flags) {
//		dest.writeString(this.customerLine);
//		dest.writeTypedList(this.pr);
//		dest.writeTypedList(this.phaseprocess);
//		dest.writeString(this.rankName);
//		dest.writeString(this.projectOwnerPhone);
//		dest.writeString(this.customerPic);
//		dest.writeString(this.customerNote);
//		dest.writeString(this.champCustomerCode);
//		dest.writeString(this.projectName);
//		dest.writeString(this.addedDatetime);
//		dest.writeString(this.houseOwnerPhone);
//		dest.writeString(this.phaseWorkProgress);
//		dest.writeString(this.lastUpdate);
//		dest.writeString(this.unitBudget);
//		dest.writeString(this.rank);
//		dest.writeString(this.id);
//		dest.writeString(this.customerCode);
//		dest.writeTypedList(this.gallery);
//		dest.writeString(this.addByUser);
//		dest.writeString(this.customerPhoneExtension);
//		dest.writeString(this.projectAddress);
//		dest.writeString(this.customerPhone);
//		dest.writeString(this.projectOwnerLine);
//		dest.writeString(this.projectStories);
//		dest.writeString(this.workProgress);
//		dest.writeString(this.opportunity);
//		dest.writeString(this.projectOwnerNote);
//		dest.writeString(this.houseOwnerPhoneExtension);
//		dest.writeString(this.shopId);
//		dest.writeString(this.projectOwnerName);
//		dest.writeString(this.latLng);
//		dest.writeString(this.userId);
//		dest.writeString(this.houseOwnerNote);
//		dest.writeString(this.customerEmail);
//		dest.writeString(this.houseOwnerLine);
//		dest.writeString(this.unitArea);
//		dest.writeString(this.phaseName);
//		dest.writeString(this.projectTypeName);
//		dest.writeString(this.status);
//		dest.writeString(this.trans);
//		dest.writeString(this.customerType);
//		dest.writeString(this.isPrivate);
//		dest.writeTypedList(this.note);
//		dest.writeString(this.projectOwnerPhoneExtension);
//		dest.writeString(this.phaseId);
//		dest.writeString(this.units);
//		dest.writeString(this.customerTypeTxt);
//		dest.writeString(this.houseOwnerName);
//		dest.writeInt(this.sellCompletePercent);
//		dest.writeString(this.customerCompany);
//		dest.writeDouble(this.opportunityN);
//		dest.writeString(this.projectTypeId);
//		dest.writeString(this.customerTaxNo);
//		dest.writeString(this.phasePic);
//		dest.writeString(this.transN);
//		dest.writeString(this.provinceId);
//		dest.writeTypedList(this.audios);
//		dest.writeString(this.customerName);
//		dest.writeString(this.ownerType);
//	}
//
//	protected Items(Parcel in) {
//		this.customerLine = in.readString();
//		this.pr = in.createTypedArrayList(PrItem.CREATOR);
//		this.phaseprocess = in.createTypedArrayList(PhaseprocessItem.CREATOR);
//		this.rankName = in.readString();
//		this.projectOwnerPhone = in.readString();
//		this.customerPic = in.readString();
//		this.customerNote = in.readString();
//		this.champCustomerCode = in.readString();
//		this.projectName = in.readString();
//		this.addedDatetime = in.readString();
//		this.houseOwnerPhone = in.readString();
//		this.phaseWorkProgress = in.readString();
//		this.lastUpdate = in.readString();
//		this.unitBudget = in.readString();
//		this.rank = in.readString();
//		this.id = in.readString();
//		this.customerCode = in.readString();
//		this.gallery = in.createTypedArrayList(GalleryModel.CREATOR);
//		this.addByUser = in.readString();
//		this.customerPhoneExtension = in.readString();
//		this.projectAddress = in.readString();
//		this.customerPhone = in.readString();
//		this.projectOwnerLine = in.readString();
//		this.projectStories = in.readString();
//		this.workProgress = in.readString();
//		this.opportunity = in.readString();
//		this.projectOwnerNote = in.readString();
//		this.houseOwnerPhoneExtension = in.readString();
//		this.shopId = in.readString();
//		this.projectOwnerName = in.readString();
//		this.latLng = in.readString();
//		this.userId = in.readString();
//		this.houseOwnerNote = in.readString();
//		this.customerEmail = in.readString();
//		this.houseOwnerLine = in.readString();
//		this.unitArea = in.readString();
//		this.phaseName = in.readString();
//		this.projectTypeName = in.readString();
//		this.status = in.readString();
//		this.trans = in.readString();
//		this.customerType = in.readString();
//		this.isPrivate = in.readString();
//		this.note = in.createTypedArrayList(NoteItem.CREATOR);
//		this.projectOwnerPhoneExtension = in.readString();
//		this.phaseId = in.readString();
//		this.units = in.readString();
//		this.customerTypeTxt = in.readString();
//		this.houseOwnerName = in.readString();
//		this.sellCompletePercent = in.readInt();
//		this.customerCompany = in.readString();
//		this.opportunityN = in.readDouble();
//		this.projectTypeId = in.readString();
//		this.customerTaxNo = in.readString();
//		this.phasePic = in.readString();
//		this.transN = in.readString();
//		this.provinceId = in.readString();
//		this.audios = in.createTypedArrayList(AudiosItem.CREATOR);
//		this.customerName = in.readString();
//		this.ownerType = in.readString();
//	}
//
//	public static final Creator<Items> CREATOR = new Creator<Items>() {
//		@Override
//		public Items createFromParcel(Parcel source) {
//			return new Items(source);
//		}
//
//		@Override
//		public Items[] newArray(int size) {
//			return new Items[size];
//		}
//	};
}