package com.scg.rudy.model.pojo.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SumTrans implements Parcelable {

	@SerializedName("number")
	private String number;

	@SerializedName("transaction")
	private String transaction;

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setTransaction(String transaction){
		this.transaction = transaction;
	}

	public String getTransaction(){
		return transaction;
	}

	@Override
 	public String toString(){
		return 
			"SumTrans{" + 
			"number = '" + number + '\'' + 
			",transaction = '" + transaction + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.number);
		dest.writeString(this.transaction);
	}

	public SumTrans() {
	}

	protected SumTrans(Parcel in) {
		this.number = in.readString();
		this.transaction = in.readString();
	}

	public static final Parcelable.Creator<SumTrans> CREATOR = new Parcelable.Creator<SumTrans>() {
		@Override
		public SumTrans createFromParcel(Parcel source) {
			return new SumTrans(source);
		}

		@Override
		public SumTrans[] newArray(int size) {
			return new SumTrans[size];
		}
	};
}