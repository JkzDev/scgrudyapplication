package com.scg.rudy.model.pojo.pgdetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubphaseprocessItem implements Parcelable {

	@SerializedName("subphase_name")
	public String subphaseName;

	@SerializedName("rank")
	public String rank;

	@SerializedName("opportunity")
	public String opportunity;

	@SerializedName("subphase_id")
	public String subphaseId;

	@SerializedName("status_txt")
	public String statusTxt;

	@SerializedName("status")
	public String status;

	@SerializedName("trans")
	public String trans;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.subphaseName);
		dest.writeString(this.rank);
		dest.writeString(this.opportunity);
		dest.writeString(this.subphaseId);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
		dest.writeString(this.trans);
	}

	public SubphaseprocessItem() {
	}

	protected SubphaseprocessItem(Parcel in) {
		this.subphaseName = in.readString();
		this.rank = in.readString();
		this.opportunity = in.readString();
		this.subphaseId = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
		this.trans = in.readString();
	}

	public static final Creator<SubphaseprocessItem> CREATOR = new Creator<SubphaseprocessItem>() {
		@Override
		public SubphaseprocessItem createFromParcel(Parcel source) {
			return new SubphaseprocessItem(source);
		}

		@Override
		public SubphaseprocessItem[] newArray(int size) {
			return new SubphaseprocessItem[size];
		}
	};
}