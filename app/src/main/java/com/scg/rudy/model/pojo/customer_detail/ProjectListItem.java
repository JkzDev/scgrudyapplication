package com.scg.rudy.model.pojo.customer_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectListItem implements Parcelable {

	@SerializedName("project_type_id")
	private String projectTypeId;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("project_address")
	private String projectAddress;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("id")
	private String id;

	@SerializedName("sale_name")
	private String saleName;

	@SerializedName("project_name")
	private String projectName;

	public void setProjectTypeId(String projectTypeId){
		this.projectTypeId = projectTypeId;
	}

	public String getProjectTypeId(){
		return projectTypeId;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setProjectAddress(String projectAddress){
		this.projectAddress = projectAddress;
	}

	public String getProjectAddress(){
		return projectAddress;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSaleName(String saleName){
		this.saleName = saleName;
	}

	public String getSaleName(){
		return saleName;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	@Override
 	public String toString(){
		return 
			"ProjectListItem{" + 
			"project_type_id = '" + projectTypeId + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",user_id = '" + userId + '\'' + 
			",project_address = '" + projectAddress + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",id = '" + id + '\'' + 
			",sale_name = '" + saleName + '\'' + 
			",project_name = '" + projectName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.projectTypeId);
		dest.writeString(this.latLng);
		dest.writeString(this.userId);
		dest.writeString(this.projectAddress);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.unitBudget);
		dest.writeString(this.opportunity);
		dest.writeString(this.phaseId);
		dest.writeString(this.id);
		dest.writeString(this.saleName);
		dest.writeString(this.projectName);
	}

	public ProjectListItem() {
	}

	protected ProjectListItem(Parcel in) {
		this.projectTypeId = in.readString();
		this.latLng = in.readString();
		this.userId = in.readString();
		this.projectAddress = in.readString();
		this.lastUpdate = in.readString();
		this.unitBudget = in.readString();
		this.opportunity = in.readString();
		this.phaseId = in.readString();
		this.id = in.readString();
		this.saleName = in.readString();
		this.projectName = in.readString();
	}

	public static final Parcelable.Creator<ProjectListItem> CREATOR = new Parcelable.Creator<ProjectListItem>() {
		@Override
		public ProjectListItem createFromParcel(Parcel source) {
			return new ProjectListItem(source);
		}

		@Override
		public ProjectListItem[] newArray(int size) {
			return new ProjectListItem[size];
		}
	};
}