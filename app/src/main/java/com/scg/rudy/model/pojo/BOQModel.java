package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 11/11/2017 AD.
 */

public class BOQModel {
    String id;
    String phase;
    String product_cate;
    String unit;
    String area;


    public BOQModel(
            String id,
            String phase,
            String product_cate,
            String unit,
            String area
    ){
        super();
        this.id = id;
        this.phase = phase;
        this.product_cate = product_cate;
        this.unit = unit;
        this.area = area;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getProduct_cate() {
        return product_cate;
    }

    public void setProduct_cate(String product_cate) {
        this.product_cate = product_cate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
