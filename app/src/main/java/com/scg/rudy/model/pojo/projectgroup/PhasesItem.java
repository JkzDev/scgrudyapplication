package com.scg.rudy.model.pojo.projectgroup;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PhasesItem implements Parcelable {

	@SerializedName("name")
	public String name;

	@SerializedName("subphases")
	public String subphases;

	@SerializedName("id")
	public String id;

	@SerializedName("detail")
	public String detail;

	@SerializedName("subphases_list")
	public List<SubphasesListItem> subphasesList;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.subphases);
		dest.writeString(this.id);
		dest.writeString(this.detail);
		dest.writeTypedList(this.subphasesList);
	}

	public PhasesItem() {
	}

	protected PhasesItem(Parcel in) {
		this.name = in.readString();
		this.subphases = in.readString();
		this.id = in.readString();
		this.detail = in.readString();
		this.subphasesList = in.createTypedArrayList(SubphasesListItem.CREATOR);
	}

	public static final Creator<PhasesItem> CREATOR = new Creator<PhasesItem>() {
		@Override
		public PhasesItem createFromParcel(Parcel source) {
			return new PhasesItem(source);
		}

		@Override
		public PhasesItem[] newArray(int size) {
			return new PhasesItem[size];
		}
	};
}