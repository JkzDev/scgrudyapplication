package com.scg.rudy.model.pojo.byproject;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CustomerPhoneListItem implements Parcelable {

	@SerializedName("phone")
	private List<String> phone;

	@SerializedName("name")
	private String name;

	@SerializedName("pic")
	private String pic;

	@SerializedName("type")
	private String type;

	public void setPhone(List<String> phone){
		this.phone = phone;
	}

	public List<String> getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"CustomerPhoneListItem{" + 
			"phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",pic = '" + pic + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringList(this.phone);
		dest.writeString(this.name);
		dest.writeString(this.pic);
		dest.writeString(this.type);
	}

	public CustomerPhoneListItem() {
	}

	protected CustomerPhoneListItem(Parcel in) {
		this.phone = in.createStringArrayList();
		this.name = in.readString();
		this.pic = in.readString();
		this.type = in.readString();
	}

	public static final Parcelable.Creator<CustomerPhoneListItem> CREATOR = new Parcelable.Creator<CustomerPhoneListItem>() {
		@Override
		public CustomerPhoneListItem createFromParcel(Parcel source) {
			return new CustomerPhoneListItem(source);
		}

		@Override
		public CustomerPhoneListItem[] newArray(int size) {
			return new CustomerPhoneListItem[size];
		}
	};
}