package com.scg.rudy.model.pojo.audio_note;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DekDroidDev on 11/6/2018 AD.
 */
public class AudioNote implements Parcelable {
    String type;
    String name;
    String file;

    public AudioNote(
            String type,
            String name,
            String file
    ){
        super();
        this.type = type;
        this.name = name;
        this.file = file;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeString(this.file);
    }


    protected AudioNote(Parcel in) {
        this.type = in.readString();
        this.name = in.readString();
        this.file = in.readString();
    }

    public static final Parcelable.Creator<AudioNote> CREATOR = new Parcelable.Creator<AudioNote>() {
        @Override
        public AudioNote createFromParcel(Parcel source) {
            return new AudioNote(source);
        }

        @Override
        public AudioNote[] newArray(int size) {
            return new AudioNote[size];
        }
    };
}
