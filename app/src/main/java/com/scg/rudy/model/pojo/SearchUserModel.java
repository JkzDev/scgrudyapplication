package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 11/11/2017 AD.
 */

public class SearchUserModel {
    String id;
    String type_id;
    String type;
    String name;
    String last_name;
    String phone;
    String company;
    String pic;

    public SearchUserModel(
            String id,
            String type_id,
            String type,
            String name,
            String last_name,
            String phone,
            String company,
            String pic
    ){
        this.id = id;
        this.type_id = type_id;
        this.type = type;
        this.name =name;
        this.last_name =last_name;
        this.phone = phone;
        this.company =company;
        this.pic = pic;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
