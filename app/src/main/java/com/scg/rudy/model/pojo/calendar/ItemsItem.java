package com.scg.rudy.model.pojo.calendar;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("date")
	private String date;

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("detail")
	private List<DetailItem> detail;

	@SerializedName("type")
	private String type;

	@SerializedName("day")
	private String day;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setDetail(List<DetailItem> detail){
		this.detail = detail;
	}

	public List<DetailItem> getDetail(){
		return detail;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"date = '" + date + '\'' + 
			",shop_id = '" + shopId + '\'' + 
			",user_id = '" + userId + '\'' + 
			",detail = '" + detail + '\'' + 
			",type = '" + type + '\'' + 
			",day = '" + day + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.date);
		dest.writeString(this.shopId);
		dest.writeString(this.userId);
		dest.writeList(this.detail);
		dest.writeString(this.type);
		dest.writeString(this.day);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.date = in.readString();
		this.shopId = in.readString();
		this.userId = in.readString();
		this.detail = new ArrayList<DetailItem>();
		in.readList(this.detail, DetailItem.class.getClassLoader());
		this.type = in.readString();
		this.day = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}