package com.scg.rudy.model.pojo.sku_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("com.asif.gsonpojogenerator")
public class SkuDetail implements Serializable, Parcelable {

	@SerializedName("total")
	private int total;

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SkuDetail{" + 
			"total = '" + total + '\'' + 
			",items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.total);
		dest.writeSerializable(this.items);
		dest.writeInt(this.status);
	}

	public SkuDetail() {
	}

	protected SkuDetail(Parcel in) {
		this.total = in.readInt();
		this.items = (Items) in.readSerializable();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<SkuDetail> CREATOR = new Parcelable.Creator<SkuDetail>() {
		@Override
		public SkuDetail createFromParcel(Parcel source) {
			return new SkuDetail(source);
		}

		@Override
		public SkuDetail[] newArray(int size) {
			return new SkuDetail[size];
		}
	};
}