package com.scg.rudy.model.pojo.project_group_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubphaseprocessItem implements Parcelable {

	@SerializedName("subphase_name")
	private String subphaseName;

	@SerializedName("rank")
	private String rank;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("subphase_id")
	private String subphaseId;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("status")
	private String status;

	@SerializedName("trans")
	private String trans;

	public void setSubphaseName(String subphaseName){
		this.subphaseName = subphaseName;
	}

	public String getSubphaseName(){
		return subphaseName;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setSubphaseId(String subphaseId){
		this.subphaseId = subphaseId;
	}

	public String getSubphaseId(){
		return subphaseId;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setTrans(String trans){
		this.trans = trans;
	}

	public String getTrans(){
		return trans;
	}

	@Override
 	public String toString(){
		return 
			"SubphaseprocessItem{" + 
			"subphase_name = '" + subphaseName + '\'' + 
			",rank = '" + rank + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",subphase_id = '" + subphaseId + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",status = '" + status + '\'' + 
			",trans = '" + trans + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.subphaseName);
		dest.writeString(this.rank);
		dest.writeString(this.opportunity);
		dest.writeString(this.subphaseId);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
		dest.writeString(this.trans);
	}

	public SubphaseprocessItem() {
	}

	protected SubphaseprocessItem(Parcel in) {
		this.subphaseName = in.readString();
		this.rank = in.readString();
		this.opportunity = in.readString();
		this.subphaseId = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
		this.trans = in.readString();
	}

	public static final Parcelable.Creator<SubphaseprocessItem> CREATOR = new Parcelable.Creator<SubphaseprocessItem>() {
		@Override
		public SubphaseprocessItem createFromParcel(Parcel source) {
			return new SubphaseprocessItem(source);
		}

		@Override
		public SubphaseprocessItem[] newArray(int size) {
			return new SubphaseprocessItem[size];
		}
	};
}