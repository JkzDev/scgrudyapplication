package com.scg.rudy.model.json_parser;

import com.scg.rudy.model.pojo.BOQModel;
import com.scg.rudy.model.pojo.ByLocationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 * @author DekDroidDev
 * @date 11/11/2017 AD
 */

public class Parser {

    public static ArrayList<BOQModel> BoqModelParser(String response){
        ArrayList<BOQModel> boqModels = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(response);
            JSONArray items = object.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonObject = items.getJSONObject(i);
                boqModels.add(new BOQModel(
                        jsonObject.optString("id",""),
                        jsonObject.optString("phase",""),
                        jsonObject.optString("product_cate",""),
                        jsonObject.optString("unit",""),
                        jsonObject.optString("area","")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  boqModels;
    }

    public static ArrayList<ByLocationModel> byLocationModels(String response){
        ArrayList<ByLocationModel> byLocationModels = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(response);
            JSONArray items = object.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonObject = items.getJSONObject(i);
                byLocationModels.add(new ByLocationModel(
                        jsonObject.optString("id",""),
                        jsonObject.optString("shop_id",""),
                        jsonObject.optString("project_name",""),
                        jsonObject.optString("lat_lng",""),
                        jsonObject.optString("unit_area",""),
                        jsonObject.optString("unit_budget",""),
                        jsonObject.optString("project_stories",""),
                        jsonObject.optString("customer_name",""),
                        jsonObject.optString("status",""),
                        jsonObject.optString("added_datetime",""),
                        jsonObject.optString("distance",""),
                        jsonObject.optString("last_update","")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  byLocationModels;
    }

}
