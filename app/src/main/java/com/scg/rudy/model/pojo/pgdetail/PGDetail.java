package com.scg.rudy.model.pojo.pgdetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PGDetail implements Parcelable {

	@SerializedName("items")
	public PGDetailItems items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.items, flags);
		dest.writeInt(this.status);
	}

	public PGDetail() {
	}

	protected PGDetail(Parcel in) {
		this.items = in.readParcelable(PGDetailItems.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Creator<PGDetail> CREATOR = new Creator<PGDetail>() {
		@Override
		public PGDetail createFromParcel(Parcel source) {
			return new PGDetail(source);
		}

		@Override
		public PGDetail[] newArray(int size) {
			return new PGDetail[size];
		}
	};
}