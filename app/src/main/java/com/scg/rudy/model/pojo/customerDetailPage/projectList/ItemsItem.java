package com.scg.rudy.model.pojo.customerDetailPage.projectList;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("project_type_id")
	private String projectTypeId;

	@SerializedName("project_address")
	private String projectAddress;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("pic")
	private String pic;

	@SerializedName("project_name")
	private String projectName;

	@SerializedName("added_datetime")
	private String addedDatetime;

	@SerializedName("import_type_name")
	private String importTypeName;

	@SerializedName("lat_lng")
	private String latLng;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("unit_budget")
	private String unitBudget;

	@SerializedName("id")
	private String id;

	@SerializedName("sale_name")
	private String saleName;

	@SerializedName("phase_name")
	private String phaseName;

	@SerializedName("project_type_name")
	private String projectTypeName;

	public void setProjectTypeId(String projectTypeId){
		this.projectTypeId = projectTypeId;
	}

	public String getProjectTypeId(){
		return projectTypeId;
	}

	public void setProjectAddress(String projectAddress){
		this.projectAddress = projectAddress;
	}

	public String getProjectAddress(){
		return projectAddress;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setPic(String pic){
		this.pic = pic;
	}

	public String getPic(){
		return pic;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public String getProjectName(){
		return projectName;
	}

	public void setAddedDatetime(String addedDatetime){
		this.addedDatetime = addedDatetime;
	}

	public String getAddedDatetime(){
		return addedDatetime;
	}

	public void setImportTypeName(String importTypeName){
		this.importTypeName = importTypeName;
	}

	public String getImportTypeName(){
		return importTypeName;
	}

	public void setLatLng(String latLng){
		this.latLng = latLng;
	}

	public String getLatLng(){
		return latLng;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setUnitBudget(String unitBudget){
		this.unitBudget = unitBudget;
	}

	public String getUnitBudget(){
		return unitBudget;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSaleName(String saleName){
		this.saleName = saleName;
	}

	public String getSaleName(){
		return saleName;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	public void setProjectTypeName(String projectTypeName){
		this.projectTypeName = projectTypeName;
	}

	public String getProjectTypeName(){
		return projectTypeName;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"project_type_id = '" + projectTypeId + '\'' + 
			",project_address = '" + projectAddress + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",pic = '" + pic + '\'' + 
			",project_name = '" + projectName + '\'' + 
			",added_datetime = '" + addedDatetime + '\'' + 
			",import_type_name = '" + importTypeName + '\'' + 
			",lat_lng = '" + latLng + '\'' + 
			",user_id = '" + userId + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",unit_budget = '" + unitBudget + '\'' + 
			",id = '" + id + '\'' + 
			",sale_name = '" + saleName + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			",project_type_name = '" + projectTypeName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.projectTypeId);
		dest.writeString(this.projectAddress);
		dest.writeString(this.opportunity);
		dest.writeString(this.phaseId);
		dest.writeString(this.pic);
		dest.writeString(this.projectName);
		dest.writeString(this.addedDatetime);
		dest.writeString(this.importTypeName);
		dest.writeString(this.latLng);
		dest.writeString(this.userId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.unitBudget);
		dest.writeString(this.id);
		dest.writeString(this.saleName);
		dest.writeString(this.phaseName);
		dest.writeString(this.projectTypeName);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.projectTypeId = in.readString();
		this.projectAddress = in.readString();
		this.opportunity = in.readString();
		this.phaseId = in.readString();
		this.pic = in.readString();
		this.projectName = in.readString();
		this.addedDatetime = in.readString();
		this.importTypeName = in.readString();
		this.latLng = in.readString();
		this.userId = in.readString();
		this.lastUpdate = in.readString();
		this.unitBudget = in.readString();
		this.id = in.readString();
		this.saleName = in.readString();
		this.phaseName = in.readString();
		this.projectTypeName = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}