package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 9/10/2017 AD.
 */

public class LoginModel {
    String id;
    String shop_id;
    String shop;
    String employee_id;
    String name;
    String email;
    String level;

    public LoginModel(
            String id,
            String shop_id,
            String shop,
            String employee_id,
            String name,
            String email,
            String level ){
        super();
        this.id = id;
        this.shop_id = shop_id;
        this.shop = shop;
        this.employee_id = employee_id;
        this.name = name;
        this.email = email;
        this.level = level;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
