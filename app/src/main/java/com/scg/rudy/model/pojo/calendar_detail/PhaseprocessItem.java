package com.scg.rudy.model.pojo.calendar_detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PhaseprocessItem implements Parcelable {

	@SerializedName("subphaseprocess")
	private List<SubphaseprocessItem> subphaseprocess;

	@SerializedName("rank")
	private String rank;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("work_progress")
	private String workProgress;

	@SerializedName("phase_id")
	private String phaseId;

	@SerializedName("phase_name")
	private String phaseName;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("trans")
	private String trans;

	@SerializedName("status")
	private String status;

	public void setSubphaseprocess(List<SubphaseprocessItem> subphaseprocess){
		this.subphaseprocess = subphaseprocess;
	}

	public List<SubphaseprocessItem> getSubphaseprocess(){
		return subphaseprocess;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setWorkProgress(String workProgress){
		this.workProgress = workProgress;
	}

	public String getWorkProgress(){
		return workProgress;
	}

	public void setPhaseId(String phaseId){
		this.phaseId = phaseId;
	}

	public String getPhaseId(){
		return phaseId;
	}

	public void setPhaseName(String phaseName){
		this.phaseName = phaseName;
	}

	public String getPhaseName(){
		return phaseName;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setTrans(String trans){
		this.trans = trans;
	}

	public String getTrans(){
		return trans;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PhaseprocessItem{" + 
			"subphaseprocess = '" + subphaseprocess + '\'' + 
			",rank = '" + rank + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",work_progress = '" + workProgress + '\'' + 
			",phase_id = '" + phaseId + '\'' + 
			",phase_name = '" + phaseName + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",trans = '" + trans + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.subphaseprocess);
		dest.writeString(this.rank);
		dest.writeString(this.opportunity);
		dest.writeString(this.workProgress);
		dest.writeString(this.phaseId);
		dest.writeString(this.phaseName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.trans);
		dest.writeString(this.status);
	}

	public PhaseprocessItem() {
	}

	protected PhaseprocessItem(Parcel in) {
		this.subphaseprocess = new ArrayList<SubphaseprocessItem>();
		in.readList(this.subphaseprocess, SubphaseprocessItem.class.getClassLoader());
		this.rank = in.readString();
		this.opportunity = in.readString();
		this.workProgress = in.readString();
		this.phaseId = in.readString();
		this.phaseName = in.readString();
		this.statusTxt = in.readString();
		this.trans = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<PhaseprocessItem> CREATOR = new Parcelable.Creator<PhaseprocessItem>() {
		@Override
		public PhaseprocessItem createFromParcel(Parcel source) {
			return new PhaseprocessItem(source);
		}

		@Override
		public PhaseprocessItem[] newArray(int size) {
			return new PhaseprocessItem[size];
		}
	};
}