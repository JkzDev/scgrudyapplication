package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 5/2/2018 AD.
 */

public class CartModel {
    String projectId;
    String skuId;
    String type;
    String name;
    String Cdate;
    String phase;
    String deliveryType;
    String total;
    String price;
    String note;

    public  CartModel(
            String projectId,
            String skuId,
            String type,
            String name,
            String Cdate,
            String phase,
            String deliveryType,
            String total,
            String price,
            String note
    ){
        this.projectId = projectId;
        this.skuId = skuId;
        this.type = type;
        this.name = name;
        this.Cdate = Cdate;
        this.phase = phase;
        this.deliveryType = deliveryType;
        this.total = total;
        this.price = price;
        this.note = note;
        
    }
    
    
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCdate() {
        return Cdate;
    }

    public void setCdate(String cdate) {
        Cdate = cdate;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
