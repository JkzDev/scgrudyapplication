package com.scg.rudy.model.pojo.forgot_password;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("com.asif.gsonpojogenerator")
public class ForgotPassword implements Serializable, Parcelable {

	@SerializedName("items")
	private String items;

	@SerializedName("status")
	private int status;

	public void setItems(String items){
		this.items = items;
	}

	public String getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ForgotPassword{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.items);
		dest.writeInt(this.status);
	}

	public ForgotPassword() {
	}

	protected ForgotPassword(Parcel in) {
		this.items = in.readString();
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ForgotPassword> CREATOR = new Parcelable.Creator<ForgotPassword>() {
		@Override
		public ForgotPassword createFromParcel(Parcel source) {
			return new ForgotPassword(source);
		}

		@Override
		public ForgotPassword[] newArray(int size) {
			return new ForgotPassword[size];
		}
	};
}