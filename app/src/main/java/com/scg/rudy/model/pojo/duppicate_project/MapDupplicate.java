package com.scg.rudy.model.pojo.duppicate_project;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class MapDupplicate implements Parcelable {

	@SerializedName("items")
	private ArrayList<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setItems(ArrayList<ItemsItem> items){
		this.items = items;
	}

	public ArrayList<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MapDupplicate{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.items);
		dest.writeInt(this.status);
	}

	public MapDupplicate() {
	}

	protected MapDupplicate(Parcel in) {
		this.items = in.createTypedArrayList(ItemsItem.CREATOR);
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<MapDupplicate> CREATOR = new Parcelable.Creator<MapDupplicate>() {
		@Override
		public MapDupplicate createFromParcel(Parcel source) {
			return new MapDupplicate(source);
		}

		@Override
		public MapDupplicate[] newArray(int size) {
			return new MapDupplicate[size];
		}
	};
}