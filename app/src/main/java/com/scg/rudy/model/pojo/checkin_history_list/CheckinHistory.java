package com.scg.rudy.model.pojo.checkin_history_list;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CheckinHistory implements Parcelable {

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CheckinHistory{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.items);
		dest.writeInt(this.status);
	}

	public CheckinHistory() {
	}

	protected CheckinHistory(Parcel in) {
		this.items = new ArrayList<ItemsItem>();
		in.readList(this.items, ItemsItem.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<CheckinHistory> CREATOR = new Parcelable.Creator<CheckinHistory>() {
		@Override
		public CheckinHistory createFromParcel(Parcel source) {
			return new CheckinHistory(source);
		}

		@Override
		public CheckinHistory[] newArray(int size) {
			return new CheckinHistory[size];
		}
	};
}