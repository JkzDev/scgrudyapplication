package com.scg.rudy.model.pojo.calendar_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class CalendarDetail implements Parcelable {

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CalendarDetail{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.items, flags);
		dest.writeInt(this.status);
	}

	public CalendarDetail() {
	}

	protected CalendarDetail(Parcel in) {
		this.items = in.readParcelable(Items.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<CalendarDetail> CREATOR = new Parcelable.Creator<CalendarDetail>() {
		@Override
		public CalendarDetail createFromParcel(Parcel source) {
			return new CalendarDetail(source);
		}

		@Override
		public CalendarDetail[] newArray(int size) {
			return new CalendarDetail[size];
		}
	};
}