package com.scg.rudy.model.pojo.projectgroup;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectGroupPojo implements Parcelable {

	@SerializedName("items")
	public List<ItemsItem> items;

	@SerializedName("status")
	public int status;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.items);
		dest.writeInt(this.status);
	}

	public ProjectGroupPojo() {
	}

	protected ProjectGroupPojo(Parcel in) {
		this.items = new ArrayList<ItemsItem>();
		in.readList(this.items, ItemsItem.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Creator<ProjectGroupPojo> CREATOR = new Creator<ProjectGroupPojo>() {
		@Override
		public ProjectGroupPojo createFromParcel(Parcel source) {
			return new ProjectGroupPojo(source);
		}

		@Override
		public ProjectGroupPojo[] newArray(int size) {
			return new ProjectGroupPojo[size];
		}
	};
}