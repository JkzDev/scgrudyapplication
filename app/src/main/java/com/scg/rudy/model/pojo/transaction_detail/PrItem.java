package com.scg.rudy.model.pojo.transaction_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PrItem implements Parcelable {

	@SerializedName("transaction_id")
	private String transactionId;

	@SerializedName("shop_invoice")
	private String shopInvoice;

	@SerializedName("note")
	private String note;

	@SerializedName("date_delivered")
	private String dateDelivered;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("date_qo")
	private String dateQo;

	@SerializedName("discount")
	private String discount;

	@SerializedName("discount_type")
	private String discount_type;

	@SerializedName("price_rank")
	private String priceRank;

	@SerializedName("date_so")
	private String dateSo;

	@SerializedName("product_price")
	private ProductPrice productPrice;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("approve_by")
	private String approveBy;

	@SerializedName("date_pr")
	private String datePr;

	@SerializedName("date_paid")
	private String datePaid;

	@SerializedName("price")
	private String price;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("id")
	private String id;

	@SerializedName("net_amount")
	private String netAmount;

	@SerializedName("status")
	private String status;

	@SerializedName("check")
	private boolean check;

	public String getUnit_id() {
		return unit_id;
	}

	public String getCustom_unit() {
		return custom_unit;
	}

	public void setUnit_id(String unit_id) {
		this.unit_id = unit_id;
	}

	public void setCustom_unit(String custom_unit) {
		this.custom_unit = custom_unit;
	}

	@SerializedName("unit_id")
	private String unit_id;

	@SerializedName("custom_unit")
	private String custom_unit;

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setShopInvoice(String shopInvoice){
		this.shopInvoice = shopInvoice;
	}

	public String getShopInvoice(){
		return shopInvoice;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setDateDelivered(String dateDelivered){
		this.dateDelivered = dateDelivered;
	}

	public String getDateDelivered(){
		return dateDelivered;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setDateQo(String dateQo){
		this.dateQo = dateQo;
	}

	public String getDateQo(){
		return dateQo;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public String getDiscount_type() {
		return discount_type;
	}

	public void setDiscount_type(String discount_type) {
		this.discount_type = discount_type;
	}

	public void setPriceRank(String priceRank){
		this.priceRank = priceRank;
	}

	public String getPriceRank(){
		return priceRank;
	}

	public void setDateSo(String dateSo){
		this.dateSo = dateSo;
	}

	public String getDateSo(){
		return dateSo;
	}

	public void setProductPrice(ProductPrice productPrice){
		this.productPrice = productPrice;
	}

	public ProductPrice getProductPrice(){
		return productPrice;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setApproveBy(String approveBy){
		this.approveBy = approveBy;
	}

	public String getApproveBy(){
		return approveBy;
	}

	public void setDatePr(String datePr){
		this.datePr = datePr;
	}

	public String getDatePr(){
		return datePr;
	}

	public void setDatePaid(String datePaid){
		this.datePaid = datePaid;
	}

	public String getDatePaid(){
		return datePaid;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setNetAmount(String netAmount){
		this.netAmount = netAmount;
	}

	public String getNetAmount(){
		return netAmount;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PrItem{" + 
			"transaction_id = '" + transactionId + '\'' + 
			",shop_invoice = '" + shopInvoice + '\'' + 
			",note = '" + note + '\'' + 
			",date_delivered = '" + dateDelivered + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",date_qo = '" + dateQo + '\'' + 
			",discount = '" + discount + '\'' + 
			",price_rank = '" + priceRank + '\'' + 
			",date_so = '" + dateSo + '\'' + 
			",product_price = '" + productPrice + '\'' + 
			",product_name = '" + productName + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",approve_by = '" + approveBy + '\'' + 
			",date_pr = '" + datePr + '\'' + 
			",date_paid = '" + datePaid + '\'' + 
			",price = '" + price + '\'' + 
			",product_id = '" + productId + '\'' + 
			",qty = '" + qty + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",id = '" + id + '\'' + 
			",net_amount = '" + netAmount + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	public PrItem() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.transactionId);
		dest.writeString(this.shopInvoice);
		dest.writeString(this.note);
		dest.writeString(this.dateDelivered);
		dest.writeString(this.orderNumber);
		dest.writeString(this.dateQo);
		dest.writeString(this.discount);
		dest.writeString(this.priceRank);
		dest.writeString(this.dateSo);
		dest.writeParcelable(this.productPrice, flags);
		dest.writeString(this.productName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.approveBy);
		dest.writeString(this.datePr);
		dest.writeString(this.datePaid);
		dest.writeString(this.price);
		dest.writeString(this.productId);
		dest.writeString(this.qty);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.id);
		dest.writeString(this.netAmount);
		dest.writeString(this.status);
		dest.writeByte(this.check ? (byte) 1 : (byte) 0);
	}

	protected PrItem(Parcel in) {
		this.transactionId = in.readString();
		this.shopInvoice = in.readString();
		this.note = in.readString();
		this.dateDelivered = in.readString();
		this.orderNumber = in.readString();
		this.dateQo = in.readString();
		this.discount = in.readString();
		this.priceRank = in.readString();
		this.dateSo = in.readString();
		this.productPrice = in.readParcelable(ProductPrice.class.getClassLoader());
		this.productName = in.readString();
		this.statusTxt = in.readString();
		this.approveBy = in.readString();
		this.datePr = in.readString();
		this.datePaid = in.readString();
		this.price = in.readString();
		this.productId = in.readString();
		this.qty = in.readString();
		this.lastUpdate = in.readString();
		this.id = in.readString();
		this.netAmount = in.readString();
		this.status = in.readString();
		this.check = in.readByte() != 0;
	}

	public static final Creator<PrItem> CREATOR = new Creator<PrItem>() {
		@Override
		public PrItem createFromParcel(Parcel source) {
			return new PrItem(source);
		}

		@Override
		public PrItem[] newArray(int size) {
			return new PrItem[size];
		}
	};
}