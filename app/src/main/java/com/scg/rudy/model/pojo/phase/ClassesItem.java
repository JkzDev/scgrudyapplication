package com.scg.rudy.model.pojo.phase;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ClassesItem implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("fav")
	private int fav;

	@SerializedName("id")
	private String id;

	@SerializedName("ispromotion")
	private int ispromotion;


	public int getIspromotion() {
		return ispromotion;
	}

	public void setIspromotion(int ispromotion) {
		this.ispromotion = ispromotion;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setFav(int fav){
		this.fav = fav;
	}

	public int getFav(){
		return fav;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ClassesItem{" + 
			"name = '" + name + '\'' + 
			",fav = '" + fav + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	public ClassesItem() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeInt(this.fav);
		dest.writeString(this.id);
		dest.writeInt(this.ispromotion);
	}

	protected ClassesItem(Parcel in) {
		this.name = in.readString();
		this.fav = in.readInt();
		this.id = in.readString();
		this.ispromotion = in.readInt();
	}

	public static final Creator<ClassesItem> CREATOR = new Creator<ClassesItem>() {
		@Override
		public ClassesItem createFromParcel(Parcel source) {
			return new ClassesItem(source);
		}

		@Override
		public ClassesItem[] newArray(int size) {
			return new ClassesItem[size];
		}
	};
}