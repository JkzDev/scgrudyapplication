package com.scg.rudy.model.pojo.dealbyproject.class_by_phase;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("sum_rank")
	private int sumRank;

	@SerializedName("ispromotion")
	private int ispromotion;

	@SerializedName("name")
	private String name;

	@SerializedName("fav")
	private int fav;

	@SerializedName("id")
	private String id;

	public void setSumRank(int sumRank){
		this.sumRank = sumRank;
	}

	public int getSumRank(){
		return sumRank;
	}

	public void setIspromotion(int ispromotion){
		this.ispromotion = ispromotion;
	}

	public int getIspromotion(){
		return ispromotion;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setFav(int fav){
		this.fav = fav;
	}

	public int getFav(){
		return fav;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"sum_rank = '" + sumRank + '\'' + 
			",ispromotion = '" + ispromotion + '\'' + 
			",name = '" + name + '\'' + 
			",fav = '" + fav + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.sumRank);
		dest.writeInt(this.ispromotion);
		dest.writeString(this.name);
		dest.writeInt(this.fav);
		dest.writeString(this.id);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.sumRank = in.readInt();
		this.ispromotion = in.readInt();
		this.name = in.readString();
		this.fav = in.readInt();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}