package com.scg.rudy.model.pojo;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 12/1/2018 AD.
 */

public class PushProductModel {
    /**
     * status : 200
     * items : {"phase":[{"id":"2","name":"งานโครงสร้าง","short_name":"เฟส 2"}],"sub_phase":[{"id":"7","name":"งานเสาชั้น 1","short_name":"เสา","rate":"1.2","detail":[{"id":"50","categories":"4","name":"ทราย/หิน/กรวด","sub_phase":"4,5,7,8,9","image":""},{"id":"48","categories":"4","name":"เหล็ก","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110323225790.jpeg"},{"id":"49","categories":"4","name":"คอนกรีตผสมเสร็จ","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110326075326.jpeg"},{"id":"51","categories":"4","name":"ไม้แปรรูปและไม้อัด","sub_phase":"2,5,7,8,9","image":"https://files.merudy.com/products/3201801110326438829.jpeg"}]},{"id":"8","name":"งานพื้นชั้น 1","short_name":"พื้น","rate":"2.56","detail":[{"id":"50","categories":"4","name":"ทราย/หิน/กรวด","sub_phase":"4,5,7,8,9","image":""},{"id":"48","categories":"4","name":"เหล็ก","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110323225790.jpeg"},{"id":"49","categories":"4","name":"คอนกรีตผสมเสร็จ","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110326075326.jpeg"},{"id":"51","categories":"4","name":"ไม้แปรรูปและไม้อัด","sub_phase":"2,5,7,8,9","image":"https://files.merudy.com/products/3201801110326438829.jpeg"}]},{"id":"9","name":"งานคานชั้น 2","short_name":"คาน","rate":"4.77","detail":[{"id":"50","categories":"4","name":"ทราย/หิน/กรวด","sub_phase":"4,5,7,8,9","image":""},{"id":"48","categories":"4","name":"เหล็ก","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110323225790.jpeg"},{"id":"49","categories":"4","name":"คอนกรีตผสมเสร็จ","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110326075326.jpeg"},{"id":"51","categories":"4","name":"ไม้แปรรูปและไม้อัด","sub_phase":"2,5,7,8,9","image":"https://files.merudy.com/products/3201801110326438829.jpeg"}]},{"id":"10","name":"งานเสาชั้น 2","short_name":"เสา","rate":"1.21","detail":[""]},{"id":"11","name":"งานพื้นชั้น 2","short_name":"พื้น","rate":"2.56","detail":[""]},{"id":"12","name":"งานระบบท่อน้ำ","short_name":"ท่อน้ำ","rate":"1.07","detail":[{"id":"54","categories":"15","name":"ท่อประปา","sub_phase":"6,12,21,34","image":""}]},{"id":"13","name":"งานอะเส","short_name":"อะเส","rate":"1.32","detail":[{"id":"27","categories":"8","name":"เคมีภัณฑ์","sub_phase":"13,14,18,26,27","image":""},{"id":"34","categories":"8","name":"สีและอุปกรณ์","sub_phase":"13,14,18,26,27","image":""}]}]}
     */
    private int status;
    private Items items;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Items getItems() {
        return items;
    }

    public void setItems(Items items) {
        this.items = items;
    }






    public static class Items {
        private ArrayList<Phase> phase;
        private ArrayList<SubPhase> sub_phase;


        public Items(
                ArrayList<Phase> phase,
                ArrayList<SubPhase> sub_phase
        ){
            this.phase = phase;
            this.sub_phase = sub_phase;
        }

        public ArrayList<Phase> getPhase() {
            return phase;
        }

        public void setPhase(ArrayList<Phase> phase) {
            this.phase = phase;
        }

        public ArrayList<SubPhase> getSub_phase() {
            return sub_phase;
        }

        public void setSub_phase(ArrayList<SubPhase> sub_phase) {
            this.sub_phase = sub_phase;
        }

        public static class Phase {
            /**
             * id : 2
             * name : งานโครงสร้าง
             * short_name : เฟส 2
             */
            private String id;
            private String name;
            private String short_name;


            public Phase (
                    String id,
                    String name,
                    String short_name
            ){
              this.id = id;
              this.name = name;
              this.short_name = short_name;

            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShort_name() {
                return short_name;
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }
        }

        public static class SubPhase {
            /**
             * id : 7
             * name : งานเสาชั้น 1
             * short_name : เสา
             * rate : 1.2
             * detail : [{"id":"50","categories":"4","name":"ทราย/หิน/กรวด","sub_phase":"4,5,7,8,9","image":""},{"id":"48","categories":"4","name":"เหล็ก","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110323225790.jpeg"},{"id":"49","categories":"4","name":"คอนกรีตผสมเสร็จ","sub_phase":"4,5,7,8,9","image":"https://files.merudy.com/products/3201801110326075326.jpeg"},{"id":"51","categories":"4","name":"ไม้แปรรูปและไม้อัด","sub_phase":"2,5,7,8,9","image":"https://files.merudy.com/products/3201801110326438829.jpeg"}]
             */

            private String id;
            private String name;
            private String short_name;
            private String rate;
            private ArrayList<Detail> detail;

            public  SubPhase(
                    String id,
                    String name,
                    String short_name,
                    String rate,
                    ArrayList<Detail> detail
            ){
                this.id = id;
                this.name = name;
                this.short_name = short_name;
                this.rate = rate;
                this.detail = detail;
            }


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShort_name() {
                return short_name;
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public String getRate() {
                return rate;
            }

            public void setRate(String rate) {
                this.rate = rate;
            }

            public ArrayList<Detail> getDetail() {
                return detail;
            }

            public void setDetail(ArrayList<Detail> detail) {
                this.detail = detail;
            }

            public static class Detail {
                /**
                 * id : 50
                 * categories : 4
                 * name : ทราย/หิน/กรวด
                 * sub_phase : 4,5,7,8,9
                 * image :
                 */

                private String id;
                private String categories;
                private String name;
                private String sub_phase;
                private String image;

                public Detail(
                        String id,
                        String categories,
                        String name,
                        String sub_phase,
                        String image
                ){
                    this.id = id;
                    this.categories = categories;
                    this.name = name;
                    this.sub_phase = sub_phase;
                    this.image = image;

                }



                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getCategories() {
                    return categories;
                }

                public void setCategories(String categories) {
                    this.categories = categories;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getSub_phase() {
                    return sub_phase;
                }

                public void setSub_phase(String sub_phase) {
                    this.sub_phase = sub_phase;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }
            }
        }
    }
}
