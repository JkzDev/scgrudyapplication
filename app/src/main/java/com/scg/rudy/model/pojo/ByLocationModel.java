package com.scg.rudy.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DekDroidDev on 12/11/2017 AD.
 */

public class ByLocationModel implements Parcelable {
    String id;
    String shop_id;
    String project_name;
    String lat_lng;
    String unit_area;
    String unit_budget;
    String project_stories;
    String customer_name;
    String status;
    String added_datetime;
    String distance;
    String last_update;

    public ByLocationModel(
            String id,
            String shop_id,
            String project_name,
            String lat_lng,
            String unit_area,
            String unit_budget,
            String project_stories,
            String customer_name,
            String status,
            String added_datetime,
            String distance,
            String last_update
    ){
        this.id = id;
        this.shop_id = shop_id;
        this.project_name = project_name;
        this.lat_lng = lat_lng;
        this.unit_area = unit_area;
        this.unit_budget = unit_budget;
        this.project_stories = project_stories;
        this.customer_name = customer_name;
        this.status = status;
        this.added_datetime = added_datetime;
        this.distance = distance;
        this.last_update = last_update;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getLat_lng() {
        return lat_lng;
    }

    public void setLat_lng(String lat_lng) {
        this.lat_lng = lat_lng;
    }

    public String getUnit_area() {
        return unit_area;
    }

    public void setUnit_area(String unit_area) {
        this.unit_area = unit_area;
    }

    public String getUnit_budget() {
        return unit_budget;
    }

    public void setUnit_budget(String unit_budget) {
        this.unit_budget = unit_budget;
    }

    public String getProject_stories() {
        return project_stories;
    }

    public void setProject_stories(String project_stories) {
        this.project_stories = project_stories;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdded_datetime() {
        return added_datetime;
    }

    public void setAdded_datetime(String added_datetime) {
        this.added_datetime = added_datetime;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.shop_id);
        dest.writeString(this.project_name);
        dest.writeString(this.lat_lng);
        dest.writeString(this.unit_area);
        dest.writeString(this.unit_budget);
        dest.writeString(this.project_stories);
        dest.writeString(this.customer_name);
        dest.writeString(this.status);
        dest.writeString(this.added_datetime);
        dest.writeString(this.distance);
        dest.writeString(this.last_update);
    }

    protected ByLocationModel(Parcel in) {
        this.id = in.readString();
        this.shop_id = in.readString();
        this.project_name = in.readString();
        this.lat_lng = in.readString();
        this.unit_area = in.readString();
        this.unit_budget = in.readString();
        this.project_stories = in.readString();
        this.customer_name = in.readString();
        this.status = in.readString();
        this.added_datetime = in.readString();
        this.distance = in.readString();
        this.last_update = in.readString();
    }

    public static final Parcelable.Creator<ByLocationModel> CREATOR = new Parcelable.Creator<ByLocationModel>() {
        @Override
        public ByLocationModel createFromParcel(Parcel source) {
            return new ByLocationModel(source);
        }

        @Override
        public ByLocationModel[] newArray(int size) {
            return new ByLocationModel[size];
        }
    };
}
