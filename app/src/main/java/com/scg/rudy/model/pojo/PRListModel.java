package com.scg.rudy.model.pojo;

/**
 * Created by DekDroidDev on 6/2/2018 AD.
 */

@SuppressWarnings("ALL")
public class PRListModel {
    String id;
    String project_id;
    String pr_code;
    String total;
    String create_date;

    public PRListModel(
            String id,
            String project_id,
            String pr_code,
            String total,
            String create_date
    ){
        this.id = id;
        this.project_id = project_id;
        this.pr_code = pr_code;
        this.total = total;
        this.create_date = create_date;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getPr_code() {
        return pr_code;
    }

    public void setPr_code(String pr_code) {
        this.pr_code = pr_code;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }
}

