package com.scg.rudy.model.pojo.choose_sku;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.asif.gsonpojogenerator")
public class ChooseSku implements Parcelable {

	@SerializedName("total")
	private String total;

	@SerializedName("total_pages")
	private int totalPages;

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setTotalPages(int totalPages){
		this.totalPages = totalPages;
	}

	public int getTotalPages(){
		return totalPages;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ChooseSku{" + 
			"total = '" + total + '\'' + 
			",total_pages = '" + totalPages + '\'' + 
			",items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.total);
		dest.writeInt(this.totalPages);
		dest.writeList(this.items);
		dest.writeInt(this.status);
	}

	public ChooseSku() {
	}

	protected ChooseSku(Parcel in) {
		this.total = in.readString();
		this.totalPages = in.readInt();
		this.items = new ArrayList<ItemsItem>();
		in.readList(this.items, ItemsItem.class.getClassLoader());
		this.status = in.readInt();
	}

	public static final Parcelable.Creator<ChooseSku> CREATOR = new Parcelable.Creator<ChooseSku>() {
		@Override
		public ChooseSku createFromParcel(Parcel source) {
			return new ChooseSku(source);
		}

		@Override
		public ChooseSku[] newArray(int size) {
			return new ChooseSku[size];
		}
	};
}