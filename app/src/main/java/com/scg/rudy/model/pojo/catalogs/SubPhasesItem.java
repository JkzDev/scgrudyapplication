package com.scg.rudy.model.pojo.catalogs;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubPhasesItem implements Parcelable {

	@SerializedName("cates")
	private List<CatesItem> cates;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setCates(List<CatesItem> cates){
		this.cates = cates;
	}

	public List<CatesItem> getCates(){
		return cates;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SubPhasesItem{" + 
			"cates = '" + cates + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.cates);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public SubPhasesItem() {
	}

	protected SubPhasesItem(Parcel in) {
		this.cates = in.createTypedArrayList(CatesItem.CREATOR);
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<SubPhasesItem> CREATOR = new Parcelable.Creator<SubPhasesItem>() {
		@Override
		public SubPhasesItem createFromParcel(Parcel source) {
			return new SubPhasesItem(source);
		}

		@Override
		public SubPhasesItem[] newArray(int size) {
			return new SubPhasesItem[size];
		}
	};
}