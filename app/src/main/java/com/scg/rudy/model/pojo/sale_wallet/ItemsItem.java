package com.scg.rudy.model.pojo.sale_wallet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("shop_id")
	private String shopId;

	@SerializedName("total")
	private String total;

	@SerializedName("money")
	private String money;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("opportunity")
	private String opportunity;

	@SerializedName("id")
	private String id;

	@SerializedName("total_pages")
	private int totalPages;

	@SerializedName("detail")
	private List<DetailItem> detail;

	public void setShopId(String shopId){
		this.shopId = shopId;
	}

	public String getShopId(){
		return shopId;
	}

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setMoney(String money){
		this.money = money;
	}

	public String getMoney(){
		return money;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setOpportunity(String opportunity){
		this.opportunity = opportunity;
	}

	public String getOpportunity(){
		return opportunity;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTotalPages(int totalPages){
		this.totalPages = totalPages;
	}

	public int getTotalPages(){
		return totalPages;
	}

	public void setDetail(List<DetailItem> detail){
		this.detail = detail;
	}

	public List<DetailItem> getDetail(){
		return detail;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"shop_id = '" + shopId + '\'' + 
			",total = '" + total + '\'' + 
			",money = '" + money + '\'' + 
			",user_id = '" + userId + '\'' + 
			",opportunity = '" + opportunity + '\'' + 
			",id = '" + id + '\'' + 
			",total_pages = '" + totalPages + '\'' + 
			",detail = '" + detail + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.shopId);
		dest.writeString(this.total);
		dest.writeString(this.money);
		dest.writeString(this.userId);
		dest.writeString(this.opportunity);
		dest.writeString(this.id);
		dest.writeInt(this.totalPages);
		dest.writeTypedList(this.detail);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.shopId = in.readString();
		this.total = in.readString();
		this.money = in.readString();
		this.userId = in.readString();
		this.opportunity = in.readString();
		this.id = in.readString();
		this.totalPages = in.readInt();
		this.detail = in.createTypedArrayList(DetailItem.CREATOR);
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}