package com.scg.rudy.model.pojo.pgContractDetail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ProjectGroupContactItem implements Parcelable {

	@SerializedName("note")
	public String note;

	@SerializedName("contact_type_name")
	public String contactTypeName;

	@SerializedName("phone")
	public String phone;

	@SerializedName("contact_type_id")
	public String contactTypeId;

	@SerializedName("line")
	public String line;

	@SerializedName("last_update")
	public String lastUpdate;

	@SerializedName("name")
	public String name;

	@SerializedName("company")
	public String company;

	@SerializedName("id")
	public String id;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.note);
		dest.writeString(this.contactTypeName);
		dest.writeString(this.phone);
		dest.writeString(this.contactTypeId);
		dest.writeString(this.line);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.name);
		dest.writeString(this.company);
		dest.writeString(this.id);
	}

	public ProjectGroupContactItem() {
	}

	protected ProjectGroupContactItem(Parcel in) {
		this.note = in.readString();
		this.contactTypeName = in.readString();
		this.phone = in.readString();
		this.contactTypeId = in.readString();
		this.line = in.readString();
		this.lastUpdate = in.readString();
		this.name = in.readString();
		this.company = in.readString();
		this.id = in.readString();
	}

	public static final Creator<ProjectGroupContactItem> CREATOR = new Creator<ProjectGroupContactItem>() {
		@Override
		public ProjectGroupContactItem createFromParcel(Parcel source) {
			return new ProjectGroupContactItem(source);
		}

		@Override
		public ProjectGroupContactItem[] newArray(int size) {
			return new ProjectGroupContactItem[size];
		}
	};
}