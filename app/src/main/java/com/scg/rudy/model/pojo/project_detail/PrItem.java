package com.scg.rudy.model.pojo.project_detail;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class PrItem implements Parcelable {

	@SerializedName("transaction_id")
	private String transactionId;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("product_id")
	private String productId;

	@SerializedName("last_update")
	private String lastUpdate;

	@SerializedName("net_amount")
	private String netAmount;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("status_txt")
	private String statusTxt;

	@SerializedName("status")
	private String status;

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setNetAmount(String netAmount){
		this.netAmount = netAmount;
	}

	public String getNetAmount(){
		return netAmount;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setStatusTxt(String statusTxt){
		this.statusTxt = statusTxt;
	}

	public String getStatusTxt(){
		return statusTxt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PrItem{" + 
			"transaction_id = '" + transactionId + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",product_id = '" + productId + '\'' + 
			",last_update = '" + lastUpdate + '\'' + 
			",net_amount = '" + netAmount + '\'' + 
			",product_name = '" + productName + '\'' + 
			",status_txt = '" + statusTxt + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.transactionId);
		dest.writeString(this.orderNumber);
		dest.writeString(this.productId);
		dest.writeString(this.lastUpdate);
		dest.writeString(this.netAmount);
		dest.writeString(this.productName);
		dest.writeString(this.statusTxt);
		dest.writeString(this.status);
	}

	public PrItem() {
	}

	protected PrItem(Parcel in) {
		this.transactionId = in.readString();
		this.orderNumber = in.readString();
		this.productId = in.readString();
		this.lastUpdate = in.readString();
		this.netAmount = in.readString();
		this.productName = in.readString();
		this.statusTxt = in.readString();
		this.status = in.readString();
	}

	public static final Parcelable.Creator<PrItem> CREATOR = new Parcelable.Creator<PrItem>() {
		@Override
		public PrItem createFromParcel(Parcel source) {
			return new PrItem(source);
		}

		@Override
		public PrItem[] newArray(int size) {
			return new PrItem[size];
		}
	};
}