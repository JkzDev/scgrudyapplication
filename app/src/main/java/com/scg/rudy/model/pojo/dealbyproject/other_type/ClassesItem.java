package com.scg.rudy.model.pojo.dealbyproject.other_type;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ClassesItem implements Parcelable {

	@SerializedName("sum_rank")
	private int sumRank;

	@SerializedName("ispromotion")
	private int ispromotion;

	@SerializedName("name")
	private String name;

	@SerializedName("fav")
	private int fav;

	@SerializedName("id")
	private String id;

	@SerializedName("sku")
	private List<SkuItem> sku;

	public void setSumRank(int sumRank){
		this.sumRank = sumRank;
	}

	public int getSumRank(){
		return sumRank;
	}

	public void setIspromotion(int ispromotion){
		this.ispromotion = ispromotion;
	}

	public int getIspromotion(){
		return ispromotion;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setFav(int fav){
		this.fav = fav;
	}

	public int getFav(){
		return fav;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSku(List<SkuItem> sku){
		this.sku = sku;
	}

	public List<SkuItem> getSku(){
		return sku;
	}

	@Override
 	public String toString(){
		return 
			"ClassesItem{" + 
			"sum_rank = '" + sumRank + '\'' + 
			",ispromotion = '" + ispromotion + '\'' + 
			",name = '" + name + '\'' + 
			",fav = '" + fav + '\'' + 
			",id = '" + id + '\'' + 
			",sku = '" + sku + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.sumRank);
		dest.writeInt(this.ispromotion);
		dest.writeString(this.name);
		dest.writeInt(this.fav);
		dest.writeString(this.id);
		dest.writeList(this.sku);
	}

	public ClassesItem() {
	}

	protected ClassesItem(Parcel in) {
		this.sumRank = in.readInt();
		this.ispromotion = in.readInt();
		this.name = in.readString();
		this.fav = in.readInt();
		this.id = in.readString();
		this.sku = new ArrayList<SkuItem>();
		in.readList(this.sku, SkuItem.class.getClassLoader());
	}

	public static final Parcelable.Creator<ClassesItem> CREATOR = new Parcelable.Creator<ClassesItem>() {
		@Override
		public ClassesItem createFromParcel(Parcel source) {
			return new ClassesItem(source);
		}

		@Override
		public ClassesItem[] newArray(int size) {
			return new ClassesItem[size];
		}
	};
}