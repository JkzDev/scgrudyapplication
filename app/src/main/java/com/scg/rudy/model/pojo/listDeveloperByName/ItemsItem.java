package com.scg.rudy.model.listDeveloperByName;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("name")
	public String name;

	@SerializedName("description")
	public String description;

	@SerializedName("id")
	public String id;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.description);
		dest.writeString(this.id);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.name = in.readString();
		this.description = in.readString();
		this.id = in.readString();
	}

	public static final Creator<ItemsItem> CREATOR = new Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}