package com.scg.rudy.model.pojo.phase;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class ItemsItem implements Parcelable {

	@SerializedName("cates")
	private List<CatesItem> cates;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setCates(List<CatesItem> cates){
		this.cates = cates;
	}

	public List<CatesItem> getCates(){
		return cates;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"cates = '" + cates + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.cates);
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public ItemsItem() {
	}

	protected ItemsItem(Parcel in) {
		this.cates = in.createTypedArrayList(CatesItem.CREATOR);
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<ItemsItem> CREATOR = new Parcelable.Creator<ItemsItem>() {
		@Override
		public ItemsItem createFromParcel(Parcel source) {
			return new ItemsItem(source);
		}

		@Override
		public ItemsItem[] newArray(int size) {
			return new ItemsItem[size];
		}
	};
}