package com.scg.rudy.model.pojo.project_type;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.asif.gsonpojogenerator")
public class SubphasesListItem implements Parcelable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SubphasesListItem{" + 
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.name);
		dest.writeString(this.id);
	}

	public SubphasesListItem() {
	}

	protected SubphasesListItem(Parcel in) {
		this.name = in.readString();
		this.id = in.readString();
	}

	public static final Parcelable.Creator<SubphasesListItem> CREATOR = new Parcelable.Creator<SubphasesListItem>() {
		@Override
		public SubphasesListItem createFromParcel(Parcel source) {
			return new SubphasesListItem(source);
		}

		@Override
		public SubphasesListItem[] newArray(int size) {
			return new SubphasesListItem[size];
		}
	};
}