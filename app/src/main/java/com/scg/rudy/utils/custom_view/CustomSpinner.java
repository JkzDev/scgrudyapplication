package com.scg.rudy.utils.custom_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scg.rudy.R;

import java.util.ArrayList;

/**
 * Created by DekDroidDev on 3/1/2018 AD.
 */

public class CustomSpinner  extends BaseAdapter {
    private Context _context;
    private ArrayList<String>  itemList;
    private static LayoutInflater inflater = null;
    public CustomSpinner(Context context,ArrayList<String>  List) {
        _context=context;
        itemList=List;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return  itemList.size();
    }

    @Override
    public String getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView==null){
            holder= new Holder();
            convertView = inflater.inflate(R.layout.custom_spinner, null);
            holder.detail  = convertView.findViewById(R.id.name);
            holder.subname  = convertView.findViewById(R.id.subname);
            convertView.setTag(holder);
        }else{
            holder = (Holder)convertView.getTag();
        }

        try{
            holder.detail.setText(itemList.get(position));
            if(itemList.get(position).contains("[")){
                String sub[] = itemList.get(position).toString().split("\\[");
                holder.subname.setVisibility(View.VISIBLE);

//            holder.detail.setText("เฟส "+(position+1));
                holder.detail.setText("เฟส "+(position+1)+" "+sub[0]);
                holder.subname.setText(sub[1].replace("]",""));
                if(sub[1].replace("]","").toString().equalsIgnoreCase("")){
                    holder.subname.setVisibility(View.GONE);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        };


        return convertView;
    }



    public class Holder {
        TextView detail;
        TextView subname;

        public TextView getDetail() {
            return detail;
        }

        public void setDetail(TextView detail) {
            this.detail = detail;
        }
    }





}
