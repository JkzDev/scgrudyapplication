package com.scg.rudy.utils.broadcast_receiver;

import android.content.Context;

import com.scg.rudy.ui.project_detail.ProjectDetailActivity;

import java.util.Date;

import static com.scg.rudy.utils.Utils.showToast;

public class CallReceiver extends PhoneCallReceiver {
    ProjectDetailActivity projectDetailActivity;
    
    
    public CallReceiver(ProjectDetailActivity projectDetailActivity,String number) {
        super();
        this.projectDetailActivity = projectDetailActivity;
        onIncomingCallStarted(projectDetailActivity,number,new Date());
    }

    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start)
    {
        //
        showToast(projectDetailActivity,"onIncomingCallStarted");
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end)
    {
        //
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start)
    {
        //
        showToast(projectDetailActivity,"onOutgoingCallStarted");
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end)
    {
        //
        showToast(projectDetailActivity,"onOutgoingCallEnded");
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start)
    {
        //
    }
}
