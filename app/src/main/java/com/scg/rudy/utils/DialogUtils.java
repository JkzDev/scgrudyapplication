/*
 *    Copyright (C) 2018 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.scg.rudy.utils;

import com.afollestad.materialdialogs.MaterialDialog;
import com.scg.rudy.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupWindow;


/**
 * Created by DekDroidDev on 11/5/2018 AD.
 */

public class DialogUtils {

    private DialogUtils() {
        // This utility class is not publicly instantiable
    }

    public  static MaterialDialog. Builder getBaseDialogBuilder (Context context, String title, String content)  {
        return  new MaterialDialog.Builder(context)
                .title(title)
                .maxIconSize( 60 )
                .negativeColorRes(R.color.green)
                .positiveColorRes(R.color.green)
                .dividerColorRes(R.color.green)
                .widgetColorRes(R.color.green)
                .content(content);
    }
    public  static MaterialDialog. Builder getBaseDialogBuilderWithLargerIcon (Context context, String title, String content)  {
        return  new MaterialDialog.Builder(context)
                .title(title)
                .maxIconSize( 120 )
                .negativeColorRes(R.color.green)
                .positiveColorRes(R.color.green)
                .dividerColorRes(R.color.green)
                .widgetColorRes(R.color.green)
                .content(content);
    }


    public static Dialog showLoadingDialog(Activity activity) {
//        Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
//        ImageView image= dialog.findViewById(R.id.loader);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.custom_dialog_loading);
//        image.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_indefinitely) );
//        View rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content);


        Dialog dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_loading);
//        Utils.DialogLoader(dialog, context);

        ImageView image= dialog.findViewById(R.id.loader);
        image.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.rotate_indefinitely) );
        dialog.show();



        return dialog;
    }

    public  static MaterialDialog showOKInfoDialog(Context context, String title, String content) {
        return getBaseDialogBuilder(context, title, content)
                .iconRes(R.drawable.ok)
                .maxIconSize( 60 )
                .show();
    }

    public  static MaterialDialog showInfoDialog (Context context, String title, String content)  {
        return getBaseDialogBuilder(context, title, content)
                .iconRes(android.R.drawable.ic_dialog_info)
                .maxIconSize( 60 )
                .show();
    }

    public  static MaterialDialog showWarningDialog (Context context, String title, String content)  {
        return getBaseDialogBuilder(context, title, content)
                .iconRes(android.R.drawable.stat_sys_warning)
                .maxIconSize( 60 )
                .show();
    }

    public  static MaterialDialog showErrorDialog (Context context, String title, String content)  {
        return getBaseDialogBuilder(context, title, content)
                .iconRes(android.R.drawable.stat_notify_error)
                .maxIconSize( 60 )
                .show();
    }

    public  static MaterialDialog showProgressDialog (Context context, String title, String content)  {
        return  new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .progress( true , 0 )
                .widgetColorRes(R.color.green)
                .canceledOnTouchOutside( false )
                .show();
    }

}
