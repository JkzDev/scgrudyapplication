package com.scg.rudy.utils.logger;
import android.util.Log;
/**
 * Created by DekDroidDev on 30/3/2018 AD.
 */
public class LoggerFactory {
    public static void getVerboseLog(String TAG, String message){
        Log.wtf(TAG, message);
    }

    public static void getDebugLog(String TAG, String message){
        Log.wtf(TAG, message);
    }

    public static void getInfoLog(String TAG, String message){
        Log.wtf(TAG, message);
    }

    public static void getWarningLog(String TAG, String message){
        Log.wtf(TAG, message);
    }

    public static void getErrorLog(String TAG, String message){
        Log.wtf(TAG, message);
    }

    public static void getWftLog(String TAG, String message){
        Log.wtf(TAG, message);
    }
}
