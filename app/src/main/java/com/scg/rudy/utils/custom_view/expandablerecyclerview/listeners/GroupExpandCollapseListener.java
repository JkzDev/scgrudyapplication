package com.scg.rudy.utils.custom_view.expandablerecyclerview.listeners;


import com.scg.rudy.utils.custom_view.expandablerecyclerview.models.ExpandableGroup;

public interface GroupExpandCollapseListener {

  void onGroupExpanded(ExpandableGroup group);
  void onGroupCollapsed(ExpandableGroup group);
}
