package com.scg.rudy.utils.custom_view.expandablerecyclerview.models;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class ExpandableGroup<T extends Parcelable> implements Parcelable {
  private String id;
  private String name;
  private String fav;
  private String ispromotion;
  private String sum_rank;
  private List<T> skuItems;

  public ExpandableGroup(
          String id,
          String name,
          String fav,
          String ispromotion,
          String sum_rank,
          List<T> skuItems) {
    this.id = id;
    this.name = name;
    this.fav = fav;
    this.ispromotion = ispromotion;
    this.sum_rank = sum_rank;
    this.skuItems = skuItems;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFav() {
    return fav;
  }

  public void setFav(String fav) {
    this.fav = fav;
  }

  public String getIspromotion() {
    return ispromotion;
  }

  public void setIspromotion(String ispromotion) {
    this.ispromotion = ispromotion;
  }

  public String getSum_rank() {
    return sum_rank;
  }

  public void setSum_rank(String sum_rank) {
    this.sum_rank = sum_rank;
  }

  public void setSkuItems(List<T> skuItems) {
    this.skuItems = skuItems;
  }



  public List<T> getSkuItems() {
    return skuItems;
  }

  public int getItemCount() {
    return skuItems == null ? 0 : skuItems.size();
  }

  @Override
  public String toString() {
    return "ExpandableGroup{" +
        "name='" + name + '\'' +
        ", skuItems=" + skuItems +
        '}';
  }

  protected ExpandableGroup(Parcel in) {
    this.id = in.readString();
    this.name = in.readString();
    this.fav = in.readString();
    this.ispromotion = in.readString();
    this.sum_rank = in.readString();
    byte hasItems = in.readByte();
    int size = in.readInt();
    if (hasItems == 0x01) {
      skuItems = new ArrayList<T>(size);
      Class<?> type = (Class<?>) in.readSerializable();
      in.readList(skuItems, type.getClassLoader());
    } else {
      skuItems = null;
    }
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.id);
    dest.writeString(this.name);
    dest.writeString(this.fav);
    dest.writeString(this.ispromotion);
    dest.writeString(this.sum_rank);


    if (skuItems == null) {
      dest.writeByte((byte) (0x00));
      dest.writeInt(0);
    } else {
      dest.writeByte((byte) (0x01));
      dest.writeInt(skuItems.size());
      final Class<?> objectsType = skuItems.get(0).getClass();
      dest.writeSerializable(objectsType);
      dest.writeList(skuItems);
    }
  }

  @SuppressWarnings("unused")
  public static final Creator<ExpandableGroup> CREATOR =
      new Creator<ExpandableGroup>() {
        @Override
        public ExpandableGroup createFromParcel(Parcel in) {
          return new ExpandableGroup(in);
        }

        @Override
        public ExpandableGroup[] newArray(int size) {
          return new ExpandableGroup[size];
        }
      };
}
