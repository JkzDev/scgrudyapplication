package com.scg.rudy.utils.custom_view.hover;
import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.scg.hover.Content;
import com.scg.rudy.R;
import com.scg.rudy.utils.custom_view.hover.theming.HoverTheme;
import com.scg.rudy.utils.custom_view.hover.theming.HoverThemer;

import de.greenrobot.event.EventBus;
public class ColorSelectionContent extends FrameLayout implements Content {

    private static final int MODE_ACCENT = 0;
    private static final int MODE_BASE = 1;

    private EventBus mBus;
    private HoverThemer mHoverThemer;
    private int mMode;
    private HoverTheme mTheme;
    private TabLayout mTabLayout;
    private ColorPicker mColorPicker;
    private TextView mAttributionTextView;

    public ColorSelectionContent(@NonNull Context context, @NonNull EventBus bus, @NonNull HoverThemer hoverThemer, @NonNull HoverTheme theme) {
        super(context);
        mBus = bus;
        mHoverThemer = hoverThemer;
        mTheme = theme;
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_color_selection_content, this, true);
        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
        mColorPicker = (ColorPicker) findViewById(R.id.colorpicker);
        mAttributionTextView = (TextView) findViewById(R.id.textview_attribution);

        mTabLayout.addTab(mTabLayout.newTab().setText("Accent Color"), true);
        mTabLayout.addTab(mTabLayout.newTab().setText("Primary Color"));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    mMode = MODE_ACCENT;
                    mColorPicker.setColor(mTheme.getAccentColor());
                    mColorPicker.setNewCenterColor(mTheme.getAccentColor());
                    mColorPicker.setOldCenterColor(mTheme.getAccentColor());

                } else if (tab.getPosition() == 1) {
                    mMode = MODE_BASE;
                    mColorPicker.setColor(mTheme.getBaseColor());
                    mColorPicker.setNewCenterColor(mTheme.getBaseColor());
                    mColorPicker.setOldCenterColor(mTheme.getBaseColor());

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });

        mColorPicker.setColor(mTheme.getAccentColor());
        mColorPicker.setNewCenterColor(mTheme.getAccentColor());
        mColorPicker.setOldCenterColor(mTheme.getAccentColor());
        mColorPicker.addSaturationBar((SaturationBar) findViewById(R.id.saturationbar));
        mColorPicker.addValueBar((ValueBar) findViewById(R.id.valuebar));
        mColorPicker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(int color) {
                HoverTheme theme;
                if (MODE_ACCENT == mMode) {
                    theme = new HoverTheme(color, mTheme.getBaseColor());
                } else {
                    theme = new HoverTheme(mTheme.getAccentColor(), color);
                }
                mHoverThemer.setTheme(theme);
            }
        });

        updateView();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBus.register(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        mBus.unregister(this);
        super.onDetachedFromWindow();
    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {

    }

    @Override
    public void onHidden() {

    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mTheme = newTheme;
        updateView();
    }

    private void updateView() {
        mTabLayout.setSelectedTabIndicatorColor(mTheme.getAccentColor());
        mTabLayout.setTabTextColors(0xFFCCCCCC, mTheme.getAccentColor());
        mAttributionTextView.setTextColor(mTheme.getAccentColor());
    }
}