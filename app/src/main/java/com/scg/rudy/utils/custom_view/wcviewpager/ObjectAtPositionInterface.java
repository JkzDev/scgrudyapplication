package com.scg.rudy.utils.custom_view.wcviewpager;

/**
 * Created by DekDroidDev on 18/5/2018 AD.
 */
public interface ObjectAtPositionInterface {

    /**
     * Returns the Object for the provided position, null if position doesn't match an object (i.e. out of bounds)
     **/
    Object getObjectAtPosition(int position);
}