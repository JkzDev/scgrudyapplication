package com.scg.rudy.utils.notification;

import android.content.Context;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.scg.rudy.RudyApplication;

import org.json.JSONObject;

/**
 *
 * @author DekDroidDev
 * @date 8/8/2018 AD
 */
public class RudyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String notificationID = notification.payload.notificationID;
        String title = notification.payload.title;
        String body = notification.payload.body;
        String smallIcon = notification.payload.smallIcon;
        String largeIcon = notification.payload.largeIcon;
        String bigPicture = notification.payload.bigPicture;
        String smallIconAccentColor = notification.payload.smallIconAccentColor;
        String sound = notification.payload.sound;
        String ledColor = notification.payload.ledColor;
        int lockScreenVisibility = notification.payload.lockScreenVisibility;
        String groupKey = notification.payload.groupKey;
        String groupMessage = notification.payload.groupMessage;
        String fromProjectNumber = notification.payload.fromProjectNumber;
        String rawPayload = notification.payload.rawPayload;

        String customKey;


        Context context = RudyApplication.getAppContext();
        if (data != null) {
//            customKey = data.optString("customkey", null);
//            if (customKey != null)
                Log.i("RudyNoti", " : " + data.toString());
        }else{
            Log.i("RudyNoti", " NULL data");
        }


    }
}
