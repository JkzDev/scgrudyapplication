package com.scg.rudy.utils.custom_view.expandablerecyclerview.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class ChildViewHolder extends RecyclerView.ViewHolder {

  public ChildViewHolder(View itemView) {
    super(itemView);
  }
}
