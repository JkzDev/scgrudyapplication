package com.scg.rudy.utils;


import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;
import com.scg.rudy.BuildConfig;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import okhttp3.OkHttpClient;

import static com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888;

/**
 * @author jackie
 */
@GlideModule
public class RudyGlideModule extends AppGlideModule {
    @Override
    public void applyOptions(@NonNull Context context, @NonNull GlideBuilder builder) {
        super.applyOptions(context, builder);

        // 20mb
        int memoryCacheSizeBytes = 1024 * 1024 * 100;
        // 30mb
        int bitmapPoolSizeBytes = 1024 * 1024 * 130;

        builder.setDiskCache(new InternalCacheDiskCacheFactory(context,"rudyCache", memoryCacheSizeBytes));
        builder.setBitmapPool(new LruBitmapPool(bitmapPoolSizeBytes));
        builder.setMemoryCache(new LruResourceCache(memoryCacheSizeBytes));
//        builder.setDefaultRequestOptions(new RequestOptions().format(PREFER_ARGB_8888));
        builder.setDefaultRequestOptions(requestOptions(context));


    }

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new OkHttpProfilerInterceptor());
        }

        OkHttpClient client = builder.build();


        OkHttpUrlLoader.Factory factory = new OkHttpUrlLoader.Factory(client);
        glide.getRegistry().replace(GlideUrl.class, InputStream.class, factory);


    }



    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }

    private static RequestOptions requestOptions(Context context){
        return new RequestOptions()
                .signature(new ObjectKey(System.currentTimeMillis() / (24 * 60 * 60 * 1000)))
//                .override(256, 256)
                .fitCenter()
                .encodeFormat(Bitmap.CompressFormat.PNG)
                .encodeQuality(80)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(PREFER_ARGB_8888)
                .skipMemoryCache(false);
    }
}