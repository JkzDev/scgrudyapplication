/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.utils.custom_view.hover.drawview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.scg.hover.Content;
import com.scg.rudy.R;
import com.scg.rudy.data.remote.ApiEndPoint;
import com.scg.rudy.data.remote.ApiHelper;
import com.scg.rudy.data.remote.RudyService;
import com.scg.rudy.model.pojo.Error.APIError;
import com.scg.rudy.utils.Utils;
import com.scg.rudy.utils.custom_view.DrawingView;
import com.scg.rudy.utils.custom_view.hover.theming.HoverTheme;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

import de.greenrobot.event.EventBus;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scg.rudy.utils.Utils.showToast;
import static java.lang.String.format;

/**
 * Use this class to try adding your own content to the Hover menu.
 *
 * @author jackie
 */
@SuppressLint("ViewConstructor")
public class DrawViewContent extends FrameLayout implements Content, View.OnClickListener {
    private final EventBus mBus;
    private DrawingView mDrawingView;
    private DrawingView mainDrawingView;
    private ImageView eraseButton, newButton, brush_size, close_drawview;
    private Button saveButton;
    private RelativeLayout drawButton;
    private float smallBrush, mediumBrush, largeBrush;
    private ImageView currPaint;
    private ImageView skin;
    private ImageView black;
    private ImageView red;
    private ImageView green;
    private ImageView blue;
    private ImageView yellow;
    private LinearLayout brush_color;
    private int lineColorSelect = 0;
    private RelativeLayout bg_brush;
    private RelativeLayout bg_erase;
    private ImageButton next_page;
    private ImageButton pre_page;
    private TextView page, message_status;
    public static int pageTotal = 1;
    int currentPage = 1;
    private FrameLayout rootDrawLayout;
    private ArrayList<DrawingView> drawingViewArrayList;
    public static final String TAG = "drawFragment";
    private ArrayList<Bitmap> bitmapArrayList;
    private RelativeLayout bgText;
    private ImageView buttonText;
    private RudyService rudyService;


    public DrawViewContent(@NonNull Context context, @NonNull EventBus bus) {
        super(context);
        mBus = bus;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_drawing_view, this, true);
        initView(view);

        rudyService = ApiHelper.getClient();


        bitmapArrayList = new ArrayList<>();
        // Getting the initial paint color.
        drawButton.setOnClickListener(this);
        eraseButton.setOnClickListener(this);
        buttonText.setOnClickListener(this);
        newButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        // Set the initial brush size
        brush_size.setImageResource(R.drawable.small);
        mDrawingView.setBrushSize(smallBrush);
        close_drawview.setOnClickListener(this);
        next_page.setOnClickListener(this);
        pre_page.setOnClickListener(this);

        mDrawingView.setLastBrushSize(smallBrush);

//        mTitleTextView = (TextView) findViewById(R.id.textview_title);
    }

    private void initView(View v) {
        drawingViewArrayList = new ArrayList<>();
        brush_size = v.findViewById(R.id.brush_size);
        brush_color = v.findViewById(R.id.brush_color);
        close_drawview = v.findViewById(R.id.close_drawview);
        bg_brush = v.findViewById(R.id.bg_brush);
        bg_erase = v.findViewById(R.id.bg_erase);
        next_page = v.findViewById(R.id.next_page);
        pre_page = v.findViewById(R.id.pre_page);
        page = v.findViewById(R.id.page);
        rootDrawLayout = v.findViewById(R.id.rootDrawLayout);
        mDrawingView = v.findViewById(R.id.drawing);
        mainDrawingView = mDrawingView;
        drawButton = v.findViewById(R.id.buttonBrush);
        eraseButton = v.findViewById(R.id.buttonErase);
        newButton = v.findViewById(R.id.buttonNew);
        saveButton = v.findViewById(R.id.buttonSave);
        message_status = v.findViewById(R.id.message_status);
        bgText = (RelativeLayout) findViewById(R.id.bgText);
        buttonText = (ImageView) findViewById(R.id.buttonText);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBus.registerSticky(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        mBus.unregister(this);
        super.onDetachedFromWindow();
    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {

    }

    @Override
    public void onHidden() {

    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
//        mTitleTextView.setTextColor(newTheme.getAccentColor());
    }

    public int drawViewChildCount(ViewGroup parent) {
        int count = 0;
        for (int x = 0; x < parent.getChildCount(); x++) {
            if (parent.getChildAt(x) instanceof DrawingView) {
                count++;
            }
        }
        return count;
    }


    public void paintClicked(View view) {
        if (view != currPaint) {
            // Update the color
            ImageView imageButton = (ImageView) view;
            String colorTag = imageButton.getTag().toString();
            mDrawingView.setColor(colorTag);
            brush_color.setBackgroundColor(Color.parseColor(colorTag));
            // Swap the backgrounds for last active and currently active image button.
            imageButton.setImageDrawable(getResources().getDrawable(R.drawable.pallet_pressed));
            currPaint.setImageDrawable(getResources().getDrawable(R.drawable.pallet));
            currPaint = (ImageView) view;
//            mDrawingView.setErase(false);
            mDrawingView.setBrushSize(mDrawingView.getLastBrushSize());
        }
    }

    private void showBrushSizeChooserDialog(View showUnderView) {
        bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
        bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        bgText.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        mDrawingView.addDrawText(false);
        final PopupWindow popup = new PopupWindow(getContext());
        final View brushDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_brush_size, null);
        popup.setContentView(brushDialog);
        LinearLayout paintLayout = brushDialog.findViewById(R.id.paint_colors);
        currPaint = (ImageView) paintLayout.getChildAt(lineColorSelect);
        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.pallet_pressed));

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());

        skin = brushDialog.findViewById(R.id.skin);
        black = brushDialog.findViewById(R.id.black);
        red = brushDialog.findViewById(R.id.red);
        green = brushDialog.findViewById(R.id.green);
        blue = brushDialog.findViewById(R.id.blue);
        yellow = brushDialog.findViewById(R.id.yellow);

        skin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 1;
                popup.dismiss();
            }
        });
        black.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 0;
                popup.dismiss();
            }
        });
        red.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 2;
                popup.dismiss();
            }
        });
        green.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 3;
                popup.dismiss();
            }
        });
        blue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 4;
                popup.dismiss();
            }
        });
        yellow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                paintClicked(v);
                lineColorSelect = 5;
                popup.dismiss();
            }
        });

        mDrawingView.setBrushSize(mDrawingView.getLastBrushSize());

        ImageButton smallBtn = brushDialog.findViewById(R.id.small_brush);
        smallBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(smallBrush);
                mDrawingView.setLastBrushSize(smallBrush);
                brush_size.setImageResource(R.drawable.small);
                popup.dismiss();
            }
        });
        ImageButton mediumBtn = brushDialog.findViewById(R.id.medium_brush);
        mediumBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(mediumBrush);
                mDrawingView.setLastBrushSize(mediumBrush);
                brush_size.setImageResource(R.drawable.medium);
                popup.dismiss();
            }
        });

        ImageButton largeBtn = brushDialog.findViewById(R.id.large_brush);
        largeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawingView.setBrushSize(largeBrush);
                mDrawingView.setLastBrushSize(largeBrush);
                brush_size.setImageResource(R.drawable.large);
                popup.dismiss();
            }
        });
        mDrawingView.setErase(false);
        popup.showAsDropDown(showUnderView, -(int) getContext().getResources().getDimension(R.dimen._10sdp), 0, Gravity.LEFT);
    }

    private void showEraserSizeChooserDialog() {
        bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
        bgText.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        mDrawingView.setErase(true);
        mDrawingView.addDrawText(false);
        mDrawingView.setBrushSize(getResources().getDimension(R.dimen._20sdp));
    }

    private void showTextChooserDialog() {
        bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        bgText.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
        mDrawingView.addDrawText(true);



//        mDrawingView.setErase(true);
//        mDrawingView.setBrushSize(getResources().getDimension(R.dimen._20sdp));
    }

    private void showNewPaintingAlertDialog(View v) {

//        popup_save_note

        final PopupWindow popup = new PopupWindow(getContext());
        final View confirmDialog = LayoutInflater.from(getContext()).inflate(R.layout.popup_save_note, null);
        popup.setContentView(confirmDialog);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.setClippingEnabled(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        TextView title = confirmDialog.findViewById(R.id.title);
        TextView content = confirmDialog.findViewById(R.id.content);
        RelativeLayout submit = confirmDialog.findViewById(R.id.submit);
        RelativeLayout cancel = confirmDialog.findViewById(R.id.cancel);

        title.setText("New drawing");
        content.setText("Start new drawing (you will lose the current drawing)?");
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                rootDrawLayout.removeAllViews();
                rootDrawLayout.addView(mainDrawingView);
                mDrawingView = mainDrawingView;
                bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
                bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                brush_color.setBackgroundColor(Color.BLACK);
                mDrawingView.setColor("BLACK");
                lineColorSelect = 0;
                mDrawingView.setErase(false);
                mDrawingView.setBrushSize(mDrawingView.getLastBrushSize());
//                mDrawingView.setLastBrushSize(smallBrush);
                mDrawingView.startNew();
                currentPage = 1;
                pageTotal = 1;
                page.setText(format("%d/%d", currentPage, pageTotal));
                pre_page.setVisibility(View.INVISIBLE);
                popup.dismiss();
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        popup.showAsDropDown(v, Gravity.CENTER, 0, 0);

    }

    private void showSavePaintingConfirmationDialog(View v) {
        final PopupWindow popup = new PopupWindow(getContext());
        final View confirmDialog = LayoutInflater.from(getContext()).inflate(R.layout.popup_save_note, null);
        popup.setContentView(confirmDialog);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setOutsideTouchable(true);
        popup.setClippingEnabled(true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        TextView title = confirmDialog.findViewById(R.id.title);
        TextView content = confirmDialog.findViewById(R.id.content);
        RelativeLayout submit = confirmDialog.findViewById(R.id.submit);
        RelativeLayout cancel = confirmDialog.findViewById(R.id.cancel);

        title.setText("Save drawing");
        content.setText("Save drawing to device Gallery?");
        submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                bitmapArrayList = new ArrayList<>();
                //save drawing
                for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                    if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                        mDrawingView = (DrawingView) rootDrawLayout.getChildAt(x);

                        mDrawingView.setDrawingCacheEnabled(true);
                        String imgSaved = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), mDrawingView.getDrawingCache(),
                                UUID.randomUUID().toString() + ".png", "drawing");
//                        if(x==rootDrawLayout.getChildCount()-1){
                        if (imgSaved != null) {
                            Uri uri = Uri.parse(imgSaved);
                            Bitmap bmp = null;
                            try {
                                InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
                                bmp = BitmapFactory.decodeStream(inputStream);
                                inputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bmp);
                            bitmapArrayList.add(bmp);
                            if (!Utils.project_id.equalsIgnoreCase("")) {
                                addNote(bmp);
                            } else {
//                                    Toast savedToast = Toast.makeText(getContext(), "Drawing saved to Gallery!", Toast.LENGTH_LONG);
//                                    savedToast.show();

                                message_status.setText("บันทึกข้อความที่จดไว้ ลงในแกลอรี่เรียบร้อย");
                                message_status.setVisibility(VISIBLE);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        message_status.setVisibility(GONE);
                                    }
                                }, 2 * 1000);


                                rootDrawLayout.removeAllViews();
                                rootDrawLayout.addView(mainDrawingView);
                                mDrawingView = mainDrawingView;
                                bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
                                bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                                brush_color.setBackgroundColor(Color.BLACK);
                                mDrawingView.setColor("BLACK");
                                lineColorSelect = 0;
                                mDrawingView.setErase(false);
                                mDrawingView.setBrushSize(smallBrush);
                                mDrawingView.setLastBrushSize(smallBrush);
                                mDrawingView.startNew();
                                currentPage = 1;
                                pageTotal = 1;
                                page.setText(format("%d/%d", currentPage, pageTotal));
                                pre_page.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            Toast unsavedToast = Toast.makeText(getContext(), "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
                            unsavedToast.show();
                        }
//                        }
                        mDrawingView.destroyDrawingCache();
                    }
                }

                popup.dismiss();
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        popup.showAsDropDown(v, Gravity.CENTER, 0, 0);

    }

    private Call<String> callAddNote(String _note) {
        return rudyService.addNote(
                Utils.APP_LANGUAGE,
                Utils.project_id,
                _note
        );
    }


    @SuppressLint("StaticFieldLeak")
    private void addNote(Bitmap bitmap){
        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bitmap);
        callAddNote(image64).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Gson gson = new Gson();
                String resp = response.body();
                if (resp.contains("200")) {

                    message_status.setText("รายการที่จดได้บันทึกลงในหน่วยงานล่าสุดเรียบร้อยแล้ว");
                    message_status.setVisibility(VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            message_status.setVisibility(GONE);
                        }
                    }, 2 * 1000);

                    rootDrawLayout.removeAllViews();
                    rootDrawLayout.addView(mainDrawingView);
                    mDrawingView = mainDrawingView;
                    bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
                    bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                    brush_color.setBackgroundColor(Color.BLACK);
                    mDrawingView.setColor("BLACK");
                    lineColorSelect = 0;
                    mDrawingView.setErase(false);
                    mDrawingView.setBrushSize(smallBrush);
                    mDrawingView.setLastBrushSize(smallBrush);
                    mDrawingView.startNew();
                    currentPage = 1;
                    pageTotal = 1;
                    page.setText(format("%d/%d", currentPage, pageTotal));
                    pre_page.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }


//    @SuppressLint("StaticFieldLeak")
//    private void addNote(final String url, Bitmap bitmap) {
////        Dialog progressDialog = DialogUtils.showLoadingDialog((Activity) getContext());
////        progressDialog.show();
//
//        String image64 = "data:image/jpg;base64," + Utils.BitMapToString(bitmap);
//        final RequestBody requestBody = new FormBody.Builder()
//                .add("note", image64)
//                .build();
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                String response = "error";
//                response = Utils.postData(url, requestBody);
//                return response;
//            }
//
//            @Override
//            protected void onPostExecute(String string) {
//                super.onPostExecute(string);
////                hideLoading();
////                progressDialog.dismiss();
//                if (string.contains("200")) {
//
//
////
////                    Toast savedToast = Toast.makeText(getContext(), "Drawing saved to you last project!", Toast.LENGTH_LONG);
////                    savedToast.show();
//
//                    message_status.setText("รายการที่จดได้บันทึกลงในหน่วยงานล่าสุดเรียบร้อยแล้ว");
//                    message_status.setVisibility(VISIBLE);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            message_status.setVisibility(GONE);
//                        }
//                    }, 2 * 1000);
//
//                    rootDrawLayout.removeAllViews();
//                    rootDrawLayout.addView(mainDrawingView);
//                    mDrawingView = mainDrawingView;
//                    bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
//                    bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
//                    brush_color.setBackgroundColor(Color.BLACK);
//                    mDrawingView.setColor("BLACK");
//                    lineColorSelect = 0;
//                    mDrawingView.setErase(false);
//                    mDrawingView.setBrushSize(smallBrush);
//                    mDrawingView.setLastBrushSize(smallBrush);
//                    mDrawingView.startNew();
//                    currentPage = 1;
//                    pageTotal = 1;
//                    page.setText(format("%d/%d", currentPage, pageTotal));
//                    pre_page.setVisibility(View.INVISIBLE);
//                }
//            }
//        }.execute();
//    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.buttonBrush) {// Show brush size chooser dialog
            showBrushSizeChooserDialog(v);

        } else if (id == R.id.buttonErase) {// Show eraser size chooser dialog
            showEraserSizeChooserDialog();

        } else if (id == R.id.buttonText) {// Show eraser size chooser dialog
            showTextChooserDialog();

        } else if (id == R.id.buttonNew) {// Show new painting alert dialog
            showNewPaintingAlertDialog(v);

        } else if (id == R.id.buttonSave) {// Show save painting confirmation dialog.
            showSavePaintingConfirmationDialog(v);

        }

//        else if (id == R.id.skin) {
//            paintClicked(v);
//            lineColorSelect = 1;
//
//        } else if (id == R.id.black) {
//            paintClicked(v);
//            lineColorSelect = 0;
//
//        } else if (id == R.id.red) {
//            paintClicked(v);
//            lineColorSelect = 2;
//
//        } else if (id == R.id.green) {
//            paintClicked(v);
//            lineColorSelect = 3;
//
//        } else if (id == R.id.blue) {
//            lineColorSelect = 4;
//            paintClicked(v);
//
//        } else if (id == R.id.yellow) {
//            lineColorSelect = 5;
//            paintClicked(v);
//
//        }

        else if (id == R.id.next_page) {
            bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
            bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            brush_color.setBackgroundColor(Color.BLACK);
            mDrawingView.setColor("BLACK");
            lineColorSelect = 0;
            mDrawingView.setErase(false);
            mDrawingView.setBrushSize(smallBrush);
            mDrawingView.setLastBrushSize(smallBrush);
            if (currentPage == pageTotal) {
                pageTotal++;
                XmlPullParser parser = getResources().getXml(R.xml.drawing_view);
                try {
                    parser.next();
                    parser.nextTag();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final AttributeSet attr = Xml.asAttributeSet(parser);
                final DrawingView drawingView = new DrawingView(getContext(), attr);
                drawingView.setId(currentPage);
                drawingViewArrayList.add(drawingView);
                rootDrawLayout.addView(drawingView);
            }

            currentPage++;
            if (currentPage > 1) {
                pre_page.setVisibility(View.VISIBLE);
            }
            for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                    if (rootDrawLayout.getChildAt(x).getId() == currentPage - 1) {
                        rootDrawLayout.getChildAt(x).bringToFront();

                        mDrawingView = (DrawingView) rootDrawLayout.getChildAt(x);
                        mDrawingView.setBrushSize(smallBrush);

                    }
                }
            }
            Log.i("next_page", " : " + currentPage);
            rootDrawLayout.invalidate();
            page.setText(format("%d/%d", currentPage, pageTotal));


        } else if (id == R.id.pre_page) {
            bg_brush.setBackgroundColor(getContext().getResources().getColor(R.color.bg_step4));
            bg_erase.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            brush_color.setBackgroundColor(Color.BLACK);
            mDrawingView.setColor("BLACK");
            lineColorSelect = 0;
            mDrawingView.setErase(false);
            mDrawingView.setBrushSize(smallBrush);
            mDrawingView.setLastBrushSize(smallBrush);
            currentPage--;
            if (currentPage == 1) {
                pre_page.setVisibility(View.INVISIBLE);
            } else {
                pre_page.setVisibility(View.VISIBLE);
            }

            Log.i("pre_page", " : " + currentPage);
//                rootDrawLayout.getChildAt(currentPage-1).bringToFront();
            if (currentPage > 1) {
                for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                    if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                        if (rootDrawLayout.getChildAt(x).getId() == currentPage - 1) {
                            rootDrawLayout.getChildAt(x).bringToFront();
                            mDrawingView = (DrawingView) rootDrawLayout.getChildAt(x);
                            mDrawingView.setBrushSize(smallBrush);
                        }
                    }
                }
            } else {
                for (int x = 0; x < rootDrawLayout.getChildCount(); x++) {
                    if (rootDrawLayout.getChildAt(x) instanceof DrawingView) {
                        if (rootDrawLayout.getChildAt(x).getId() == R.id.drawing) {
                            rootDrawLayout.getChildAt(x).bringToFront();
                            mDrawingView = (DrawingView) rootDrawLayout.getChildAt(x);
                            mDrawingView.setBrushSize(smallBrush);
                        }
                    }
                }
            }
            page.setText(format("%d/%d", currentPage, pageTotal));
        }
    }
}
