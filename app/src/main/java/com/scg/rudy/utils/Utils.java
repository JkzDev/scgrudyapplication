package com.scg.rudy.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scg.rudy.R;
import com.scg.rudy.RudyApplication;
import com.scg.rudy.model.pojo.promotionDetail.Items;
import com.scg.rudy.model.pojo.promotionDetail.PromotionDetail;
import com.scg.rudy.model.pojo.user.UserPOJO;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.CacheControl;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.HttpException;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

/**
 * Created by DekDroidDev on 5/10/2017 AD.
 */

public class Utils {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static String PERF_NAME_LANG = "language";
    public static String PERF_NAME_CURRENCY_FOR_SHOW = "currencyForShow";

    public static String PERF_LOGIN = "LoginData";
    public static final String TEMP_PHOTO_FILE_NAME = "RudyTemp.jpg";
    public static Uri mCropImageUri;
    public static String project_id = "";
    public static String _url ="";
    public static int tabSelect =1;
    public static String pdfName = "";
    //for support multiple language
    public static String APP_LANGUAGE = "th";
    public static String currency = "THB";
    public static String currencyForShow = "บาท";
    private static String pathFolderDataNewProject = "/RudyTemp/DataNewProject";


    public static String getData(String url){
//        _url = url;
//        String STARTUP_TRACE = url.replace("https://api.merudy.com/th/","").replace("?un=rudy&pa=b538c6fd231ef0fbf7f09fe393663cf8","");
//        Trace mTrace= FirebasePerformance.getInstance().newTrace(STARTUP_TRACE);
//        mTrace.start();
        String _response;
        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
//        mTrace.incrementCounter("requests sent "+STARTUP_TRACE);
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().noCache().build())
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                _response = response.body().string();
            } else {
                _response = "Not Success - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }
//        mTrace.stop();
        return _response;
    }

    public static void setSliderViews(Context context ,SliderLayout sliderLayout, ArrayList<String> bannerImage) {

        sliderLayout.setIndicatorAnimation(IndicatorAnimations.DROP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderLayout.setScrollTimeInSec(1); //set scroll delay in seconds :
        sliderLayout.SetSizePagerIndicator(4);

        for (int i = 0; i < bannerImage.size(); i++) {

            DefaultSliderView sliderView = new DefaultSliderView(context);
            //sliderView.setImageDrawable(R.drawable.ic_image_dialog_exacept);
            sliderView.setImageUrl(bannerImage.get(i));
            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);

//            sliderView.setDescription("The quick brown fox jumps over the lazy dog.\n" +
//                    "Jackdaws love my big sphinx of quartz. " + (i + 1));
//            final int finalI = i;
//
//            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
//                @Override
//                public void onSliderClick(SliderView sliderView) {
//                    Toast.makeText(ProductRecommendNewActivity.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
//
//                }
//            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }

    public static String moneyFormat(String text){
        if(text==null){
            text = "1";
        }
        text = text.replace("null","0").replace(",","");
        if(text.length()==0){
            text = "0";
        }


        Double xdouble = Double.parseDouble(text);
        DecimalFormat formatter = new DecimalFormat("#,###,###.##");
        return  formatter.format(xdouble);
    }

    public static String postData(String url, String username,String password) {
        String _response;
        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        RequestBody requestBody = new FormBody.Builder()
                .add("userID", username)
                .add("password", password)
                .build();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        try {
            _response = client.newCall(request).execute().body().string();

        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }

        return _response;
    }

    public static String postData(String url,RequestBody requestBody) {
        String _response;
        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("content-type", "text/html; charset=utf-8;application/x-www-form- urlencoded")
                .post(requestBody)
                .build();
        try {
            _response = client.newCall(request).execute().body().string();

        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }

        return _response;
    }

    public static String postData(String url) {
        String _response;
        RequestBody requestBody = new FormBody.Builder()
                .add("", "")
                .build();
        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("content-type", "text/html; charset=utf-8")
                .post(requestBody)
                .build();
        try {
            _response = client.newCall(request).execute().body().string();

        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }

        return _response;
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String getLang(Context context){
        SharedPreferences Notiprefs = PreferenceManager.getDefaultSharedPreferences(context);
        String lang = Notiprefs.getString(Utils.PERF_NAME_LANG, "th");
        return lang;
    }

    public static void setLang(Context context,String lang){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(Utils.PERF_NAME_LANG, lang);
        editor.commit();
    }

    public static String getCurrencyForShow(Context context){
        SharedPreferences Notiprefs = PreferenceManager.getDefaultSharedPreferences(context);
        String currencyForShow = Notiprefs.getString(Utils.PERF_NAME_CURRENCY_FOR_SHOW, context.getResources().getString(R.string.bath));
        return currencyForShow;
    }

    public static void setCurrencyForShow(Context context,String _currency){
        currency = _currency;
        if (currency.equalsIgnoreCase("THB")){
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(Utils.PERF_NAME_CURRENCY_FOR_SHOW, context.getResources().getString(R.string.bath));
            editor.commit();
        }else{
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(Utils.PERF_NAME_CURRENCY_FOR_SHOW, context.getResources().getString(R.string.dollar));
            editor.commit();
        }
    }

    public static void setChangeLangCurrencyForShow(Context context){
        if (currency.equalsIgnoreCase("THB")){
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(Utils.PERF_NAME_CURRENCY_FOR_SHOW, context.getResources().getString(R.string.bath));
            editor.commit();
        }else{
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(Utils.PERF_NAME_CURRENCY_FOR_SHOW, context.getResources().getString(R.string.dollar));
            editor.commit();
        }
    }

    public static void  showToast(Activity activity, String message){
        Toast.makeText(activity,message,Toast.LENGTH_LONG).show();
    }

    public static boolean isEmailValid(Activity activity, EditText editText) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = editText.getText().toString();

        Pattern pattern = compile(expression, CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }else {
            editText.setError(activity.getString(R.string.valid_email));
        }
        return isValid;
    }

    public static void savePrefer(Context context,String prefer,String saveString){
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(prefer, saveString);
        editor.commit();
    }
    public static String getPrefer(Context context,String prefer){
        SharedPreferences Notiprefs = PreferenceManager.getDefaultSharedPreferences(context);
        String pre = Notiprefs.getString(prefer, "");
        return pre;
    }

    public static Parcelable getPreferParcel(Context context,String prefer){
        SharedPreferences Notiprefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = Notiprefs.getString(prefer, null);
//        String pre = Notiprefs.getString(prefer, "");
        return json == null ? null : new Gson().fromJson(json, Location.class);
    }


    public static UserPOJO setEnviroment(Activity activity) {
        Utils.requestPermission(activity, Manifest.permission.RECORD_AUDIO);
        Utils.requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        String json = getPrefer(activity,PERF_LOGIN);
        return  UserPOJO.create(json);

    }
    public static UserPOJO setEnviroment(Context context) {
        String json = getPrefer(context,PERF_LOGIN);
        return  UserPOJO.create(json);

    }







    public static String BitMapToString(Bitmap bitmap){
        String profileimg="";
        ByteArrayOutputStream ByteStream=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, ByteStream);
        byte [] b=ByteStream.toByteArray();
        profileimg= Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("ImageString",profileimg);
        return profileimg;
    }
    public static String BitMapToStringPNG(Bitmap bitmap){
        String profileimg="";
        ByteArrayOutputStream ByteStream=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 60, ByteStream);
        byte [] b=ByteStream.toByteArray();
        profileimg= Base64.encodeToString(b, Base64.DEFAULT);

        Log.d("ImageString",profileimg);
        return profileimg;
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager)   context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        imm.showSoftInput(view,0);
//        view
    }

    public static void inputMaxLength(EditText editText,int length) {

        editText.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; ++i)
                {
                    if (!compile("[1234567890.]*").matcher(String.valueOf(source.charAt(i))).matches())
                    {
                        return "";
                    }
                }
                return null;
            }
        },new InputFilter.LengthFilter(length)});

//        view
    }

    public static ArrayList<String> searchFileNameDataNewProject(){
        ArrayList<String> fileNameList = new ArrayList<>();


        File dir = Environment
                .getExternalStoragePublicDirectory(pathFolderDataNewProject);
        File[] files = dir.listFiles();

        if (files != null){
            for (File file : files) {
                if (file.getName().startsWith("DataNewProject")) {
                    fileNameList.add(file.getName());
                }
            }
        }

        return fileNameList;
    }


    public static String readFromFileDataNewProject(Context context, String fileName) {

        String ret = "";

        try {
            FileInputStream inputStream = new FileInputStream(Environment.getExternalStoragePublicDirectory(pathFolderDataNewProject) + "/" + fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();

                File file = new File(Environment.getExternalStoragePublicDirectory(pathFolderDataNewProject) + "/" + fileName);
                file.delete();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static String getFileNameDataNewProject(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("DDMMYYY_HHmmss");
        String formattedDate = df.format(c.getTime());
        return "DataNewProject" + formattedDate + ".txt";
    }

    public static void writeToFileDataNewProject(String data, Context context) {

        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                pathFolderDataNewProject
                        );

        // Make sure the path directory exists.
        if(!path.exists())
        {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, getFileNameDataNewProject());

        // Save your stream, don't forget to flush() it before closing it.

        try
        {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        }
        catch (IOException e)
        {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }




    public static void DialogLoader(final Dialog dialog, final Context context){
        ImageView image= dialog.findViewById(R.id.loader);
        image.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_indefinitely) );
        if(!((Activity) context).isFinishing()){
            dialog.show();
        }
    }


    public static boolean createDirIfNotExists(String path) {
        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("TravellerLog :: ", "Problem creating Image folder");
                return false;
            }
        }
        return true;
    }


    public static PopupWindow displayPopupWindow(Activity acttivity,View anchorView) {
        PopupWindow popup = new PopupWindow(acttivity);
        View layout = acttivity.getLayoutInflater().inflate(R.layout.popup_content, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView,anchorView.getLeft(),0,Gravity.CENTER);

        return popup;
//        popup.showAtLocation(anchorView,Gravity.CENTER, 0,0);
    }


    public static void requestPermission(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, 0);
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() <= 0;
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static boolean isTabletDevice(Context activityContext) {
        // Verifies if the Generalized Size of the device is XLARGE to be
        // considered a Tablet
        boolean xlarge = ((activityContext.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_XLARGE);

        // If XLarge, checks if the Generalized Density is at least MDPI
        // (160dpi)
        if (xlarge) {
            DisplayMetrics metrics = new DisplayMetrics();
            Activity activity = (Activity) activityContext;
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            // MDPI=160, DEFAULT=160, DENSITY_HIGH=240, DENSITY_MEDIUM=160,
            // DENSITY_TV=213, DENSITY_XHIGH=320
            // Yes, this is a tablet!
            return metrics.densityDpi == DisplayMetrics.DENSITY_DEFAULT
                    || metrics.densityDpi == DisplayMetrics.DENSITY_HIGH
                    || metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM
                    || metrics.densityDpi == DisplayMetrics.DENSITY_TV
                    || metrics.densityDpi == DisplayMetrics.DENSITY_XHIGH;
        }

        // No, this is not a tablet!
        return false;
    }

    public static  String getProviderName(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)
        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        return locationManager.getBestProvider(criteria, true);
    }

    public static void onShowPromotion(Activity activity, PromotionDetail promotionDetail){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.popup_promotion);
        dialog.setCancelable(true);
        int width = (int) (activity.getResources().getDisplayMetrics().widthPixels * 0.95);
        int height = (int) (activity.getResources().getDisplayMetrics().heightPixels * 0.8);
        dialog.getWindow().setLayout(width, height);
        Items promotionDetailItem = promotionDetail.getItems();

        TextView promo = dialog.findViewById(R.id.promo);
        TextView duration = dialog.findViewById(R.id.duration);
        TextView remain = dialog.findViewById(R.id.remain);
        TextView skuCode = dialog.findViewById(R.id.skuCode);
        TextView name = dialog.findViewById(R.id.name);
        TextView price = dialog.findViewById(R.id.price);
        TextView stock = dialog.findViewById(R.id.stock);
        TextView minBuy = dialog.findViewById(R.id.minBuy);
        TextView unit = dialog.findViewById(R.id.unit);
        TextView detail = dialog.findViewById(R.id.detail);
        TextView condition = dialog.findViewById(R.id.condition);
        RelativeLayout submit = dialog.findViewById(R.id.submit);

        promo.setText(promotionDetailItem.getPromotionCode());
        duration.setText(promotionDetailItem.getStartDate()+" ถึง "+promotionDetailItem.getExpiredDate());
        remain.setText(String.valueOf(promotionDetailItem.getRemainDay()));
        skuCode.setText(promotionDetailItem.getSku());
        name.setText(promotionDetailItem.getProductName());
        price.setText(promotionDetailItem.getCommission());
        stock.setText(promotionDetailItem.getRemainQty());
        minBuy.setText(promotionDetailItem.getMinQty());
        unit.setText(promotionDetailItem.getProductUnit());
        detail.setText(promotionDetailItem.getPromotionDescription());
        condition.setText(promotionDetailItem.getPromotionConditions());


        dialog.show();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public static void SelectDateDialogs(Context context, final EditText editText){
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                view.setMinDate(System.currentTimeMillis() - 1000);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale(Utils.APP_LANGUAGE.toUpperCase()));
                editText.setText(sdf.format(calendar.getTime()));
            }

        };

        DatePickerDialog dialog = new DatePickerDialog(context, date,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
//        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dialog.show();
    }


    public static String handleApiError(Context context,Throwable error) {
        String message =  error.getLocalizedMessage();
        if (error instanceof HttpException) {
            switch (((HttpException) error).code()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                    message = "Unauthorised User ";
                    break;
                case HttpsURLConnection.HTTP_FORBIDDEN:
                    message = "Forbidden";
                    break;
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                    message =  "Internal Server Error";
                    break;
                case HttpsURLConnection.HTTP_BAD_REQUEST:
                    message = "Bad Request";
                    break;
//                case NetworkUtils.isNetworkConnected(context):
//                    mView.onError("No Internet Connection");
//                    break;
                default:
                    message = error.getLocalizedMessage();
                    break;
            }
        }

        if( !NetworkUtils.isNetworkConnected(context)){
            message = "No Internet Connection";
        }
        return message;
    }

    public static String getVersionName(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return "v"+pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return context.getString(android.R.string.unknownName);
        }
    }

    public static int getVersionCode(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }





}
