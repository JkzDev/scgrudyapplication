package com.scg.rudy.utils.notification;

import android.content.Context;
import android.content.Intent;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.scg.rudy.RudyApplication;
import com.scg.rudy.ui.favorite.FavoriteDetailActivity;
import com.scg.rudy.ui.main.MainActivity;
import com.scg.rudy.ui.transaction_detail.TransactionDetailActivity;
import com.scg.rudy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.scg.rudy.base.BaseActivity.LogException;

/**
 *
 * @author DekDroidDev
 * @date 8/8/2018 AD
 */
public class RudyNotificationOpenedHandler  implements OneSignal.NotificationOpenedHandler {
    // This fires when a notification is opened by tapping on it.
    private OSNotificationAction.ActionType actionType;
    private JSONObject data;
    private String launchUrl;
    private JSONObject sensor;
    private String openURL ;
    private Object activityToLaunch;
    private String transaction_id;
    private String project_id;
    private String type;
    private Intent intent;
    private Context context;
    private Intent intentOpen;
//    1 = fav
//    2 = cart

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        actionType = result.action.type;
        data = result.notification.payload.additionalData;
        launchUrl = result.notification.payload.launchURL;
        activityToLaunch = MainActivity.class;
        context =  RudyApplication.getAppContext();
        intent = new Intent(context, (Class<?>) activityToLaunch);
        if (data != null) {
            try {
                sensor = data.getJSONObject("sensor");
                openURL = data.optString("openURL", null);
                if (sensor != null){
                    transaction_id = sensor.optString("transaction_id",null);
                    project_id = sensor.optString("project_id",null);
                    type = sensor.optString("type",null);

                   switch (type){
                       case "1" :
                           activityToLaunch = FavoriteDetailActivity.class;
                           intent = new Intent(context, (Class<?>) activityToLaunch);
                           intent.putExtra("transaction_id", transaction_id);
                           Utils.project_id = project_id;
                           intentOpen = new Intent(context, MainActivity.class);
                           intentOpen.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                           context.startActivity(intentOpen);

                           break;
                       case "2" :
                           activityToLaunch = TransactionDetailActivity.class;
                           intent = new Intent(context, (Class<?>) activityToLaunch);
                           intent.putExtra("transaction_id", transaction_id);
                           intentOpen = new Intent(context, MainActivity.class);
                           intentOpen.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                           context.startActivity(intentOpen);
                           break;



                           default:
                               activityToLaunch = MainActivity.class;
                               intent = new Intent(context, (Class<?>) activityToLaunch);
                               break;

                   }

                }
//                if (openURL != null){
//                    Log.i("OneSignal", "openURL to webview with URL value: " + openURL);
//                }

            } catch (JSONException e) {
                e.printStackTrace();
                LogException(data.toString() ,e);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

//        intent.putExtra("openURL", openURL);
//        Log.i("OneSignalExample", "openURL = " + openURL);


//        if (actionType == OSNotificationAction.ActionType.ActionTaken) {
//            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
//
//            if (result.action.actionID.equals("id1")) {
//                Log.i("OneSignalExample", "button id called: " + result.action.actionID);
//                activityToLaunch = GreenActivity.class;
//            } else
//                Log.i("OneSignalExample", "button id called: " + result.action.actionID);
//        }




    }
}