package com.scg.rudy.utils.custom_view.autolabel;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scg.rudy.R;

import static android.view.Gravity.CENTER;

/*
 * Copyright (C) 2015 Created by DekDroidDev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class LabelProject extends LinearLayout {

    private TextView mTextView;
    private RelativeLayout label_bg;
    private OnClickCrossListener listenerOnCrossClick;
    private OnLabelProjectClickListener listenerOnLabelClick;

    public LabelProject(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LabelProject(Context context, int textSize, int iconCross,
                        boolean showCross, int textColor, int backgroundResource, boolean labelsClickables, int padding) {
        super(context);
        init(context, textSize, iconCross, showCross, textColor,
            backgroundResource, labelsClickables, padding);
    }

    private void init(final Context context, int textSize, int iconCross,
            boolean showCross, int textColor, int backgroundResource, boolean labelsClickables, int padding) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View labelView = inflater.inflate(R.layout.label_project, this, true);

        LinearLayout linearLayout = (LinearLayout) labelView.findViewById(R.id.llLabel);
        linearLayout.setBackgroundResource(backgroundResource);
        linearLayout.setGravity(CENTER);
        linearLayout.setPadding(padding, padding, padding, padding);

        if(labelsClickables){
            linearLayout.setClickable(true);
            linearLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listenerOnLabelClick != null) {
                        listenerOnLabelClick.onClickLabelProject((LabelProject) labelView);
                    }
                }
            });
        }

        mTextView = (TextView) labelView.findViewById(R.id.tvLabel);
        label_bg = labelView.findViewById(R.id.label_bg);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        mTextView.setGravity(CENTER);
        mTextView.setTextColor(textColor);
    }

    public RelativeLayout getLabelBG() {
        return label_bg;
    }

    public TextView getmText() {
        return mTextView;
    }

    public String getText() {
        return mTextView.getText().toString();
    }
    public int getTextId() {
        return mTextView.getId();
    }


    public void setText(String text) {
        mTextView.setText(text);
    }
    public void setTextId(int Id) {
        mTextView.setId(Id);
    }

    /**
     * Set a callback listener when the cross icon is clicked.
     *
     * @param listener Callback instance.
     */
    public void setOnClickCrossListener(OnClickCrossListener listener) {
        this.listenerOnCrossClick = listener;
    }

    /**
     * Interface for a callback listener when the cross icon is clicked.
     */
    public interface OnClickCrossListener {

        /**
         * Call when the cross icon is clicked.
         */
        void onClickCross(LabelProject label);
    }

    /**
     * Set a callback listener when the {@link LabelProject} is clicked.
     *
     * @param listener Callback instance.
     */
    public void setOnLabelProjectClickListener(OnLabelProjectClickListener listener) {
        this.listenerOnLabelClick = listener;
    }

    /**
     * Interface for a callback listener when the {@link LabelProject} is clicked.
     * Container Activity/Fragment must implement this interface.
     */
    public interface OnLabelProjectClickListener {

        /**
         * Call when the {@link LabelProject} is clicked.
         */
        void onClickLabelProject(LabelProject label);
    }
}
