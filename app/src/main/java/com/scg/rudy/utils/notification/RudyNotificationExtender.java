package com.scg.rudy.utils.notification;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;

import java.math.BigInteger;
/**
 *
 * @author DekDroidDev
 * @date 10/8/2018 AD
 */
public class RudyNotificationExtender extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read Properties from result
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = builder -> {
            // Sets the background notification color to Red on Android 5.0+ devices.
            return builder.setColor(new BigInteger("003C8F", 16).intValue());
        };

        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
//        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);
//         Return true to stop the notification from displaying

        return true;
    }
}