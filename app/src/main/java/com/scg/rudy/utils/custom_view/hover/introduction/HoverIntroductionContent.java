package com.scg.rudy.utils.custom_view.hover.introduction;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.scg.hover.Content;
import com.scg.rudy.R;
import com.scg.rudy.utils.custom_view.hover.theming.HoverTheme;

import de.greenrobot.event.EventBus;

/**
 * @author jackie
 */
@SuppressLint("ViewConstructor")
public class HoverIntroductionContent extends FrameLayout implements Content {

    private final EventBus mBus;
    private View mLogo;
    private HoverMotion mHoverMotion;
    private TextView mHoverTitleTextView;
    private TextView mGoalsTitleTextView;

    public HoverIntroductionContent(@NonNull Context context, @NonNull EventBus bus) {
        super(context);
        mBus = bus;
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_content_introduction, this, true);

        mLogo = findViewById(R.id.imageview_logo);
        mHoverMotion = new HoverMotion();
        mHoverTitleTextView = (TextView) findViewById(R.id.textview_hover_title);
        mGoalsTitleTextView = (TextView) findViewById(R.id.textview_goals_title);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mBus.registerSticky(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        mBus.unregister(this);
        super.onDetachedFromWindow();
    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {
        mHoverMotion.start(mLogo);
    }

    @Override
    public void onHidden() {
        mHoverMotion.stop();
    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mHoverTitleTextView.setTextColor(newTheme.getAccentColor());
        mGoalsTitleTextView.setTextColor(newTheme.getAccentColor());
    }
}