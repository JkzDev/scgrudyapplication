/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scg.rudy.utils.custom_view.hover.service;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.view.ContextThemeWrapper;

import com.scg.hover.HoverMenu;
import com.scg.hover.HoverView;
import com.scg.hover.window.HoverMenuService;
import com.scg.rudy.R;
import com.scg.rudy.utils.custom_view.hover.Bus;
import com.scg.rudy.utils.custom_view.hover.DemoHoverMenu;
import com.scg.rudy.utils.custom_view.hover.DemoHoverMenuFactory;
import com.scg.rudy.utils.custom_view.hover.theming.HoverTheme;

import java.io.IOException;


/**
 * Demo {@link RudyHoverMenuService}.
 * @author jackie
 */
public class RudyHoverMenuService extends HoverMenuService {

    private static final String TAG = "RudyHoverMenuService";

    public static void showFloatingMenu(Context context) {
        context.startService(new Intent(context, RudyHoverMenuService.class));
    }

    private DemoHoverMenu mDemoHoverMenu;

    @Override
    public void onCreate() {
        super.onCreate();
        Bus.getInstance().register(this);
    }

    @Override
    public void onDestroy() {
        Bus.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    protected Context getContextForHoverMenu() {
        return new ContextThemeWrapper(this, R.style.AppTheme);
    }

    @Override
    protected void onHoverMenuLaunched(@NonNull Intent intent, @NonNull HoverView hoverView) {
        hoverView.setMenu(createHoverMenu());
        hoverView.collapse();
    }

    private HoverMenu createHoverMenu() {
        try {
            mDemoHoverMenu = new DemoHoverMenuFactory().createDemoMenuFromCode(getContextForHoverMenu(), Bus.getInstance());
//            mDemoHoverMenuAdapter = new DemoHoverMenuFactory().createDemoMenuFromFile(getContextForHoverMenu());
            return mDemoHoverMenu;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mDemoHoverMenu.setTheme(newTheme);
    }

}
