package com.scg.rudy.utils.custom_view.expandablerecyclerview.listeners;


public interface ExpandCollapseListener {

  void onGroupExpanded(int positionStart, int itemCount);
  void onGroupCollapsed(int positionStart, int itemCount);
}
