package com.scg.rudy.utils.custom_view.expandablerecyclerview.listeners;

public interface OnGroupClickListener {
  boolean onGroupClick(int flatPos);
}