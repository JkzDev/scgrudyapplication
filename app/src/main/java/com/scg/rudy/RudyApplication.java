package com.scg.rudy;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.androidnetworking.AndroidNetworking;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.onesignal.OneSignal;
import com.scg.rudy.data.network.receiver.ConnectivityReceiver;
import com.scg.rudy.utils.AppLogger;
import com.scg.rudy.utils.custom_view.hover.Bus;
import com.scg.rudy.utils.custom_view.hover.appstate.AppStateTracker;
import com.scg.rudy.utils.custom_view.hover.theming.HoverTheme;
import com.scg.rudy.utils.custom_view.hover.theming.HoverThemeManager;
import com.scg.rudy.utils.notification.RudyNotificationOpenedHandler;
import com.scg.rudy.utils.notification.RudyNotificationReceivedHandler;

import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.scg.rudy.utils.TypefaceUtil.overrideFont;

/**
 *
 * @author JackieFABbrigade ja
 * @date 3/9/2017 AD
 */

public class RudyApplication extends Application  {
    public static Context context;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        try {
            MultiDex.install(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate();
        AndroidNetworking.initialize(getApplicationContext());
        context = getApplicationContext();
        setupTheme();
        setupAppStateTracking();
        Fabric.with(this, new Crashlytics());
        sAnalytics = GoogleAnalytics.getInstance(this);

        overrideFont(getApplicationContext(), "SERIF", "fonts/CSPraKasFD.otf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/CSPraKasFD.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        AppLogger.init();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new RudyNotificationOpenedHandler())
                .setNotificationReceivedHandler(new RudyNotificationReceivedHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .autoPromptLocation(false)
                .init();
    }
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//           Tracker t = analytics.newTracker("UA-69462340-1");
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }


    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static RudyApplication get(Context context) {
        return (RudyApplication) context.getApplicationContext();
    }

    public static Context getAppContext() {
        if(context==null){
            context = getAppContext();
        }
        return context;
    }

    private void setupTheme() {
        HoverTheme defaultTheme = new HoverTheme(
                ContextCompat.getColor(this, R.color.white),
                ContextCompat.getColor(this, R.color.colorPrimary));
        HoverThemeManager.init(Bus.getInstance(), defaultTheme);
    }

    private void setupAppStateTracking() {
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        AppStateTracker.init(this, Bus.getInstance());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (activityManager.getAppTasks().size() > 0) {
                AppStateTracker.getInstance().trackTask(activityManager.getAppTasks().get(0).getTaskInfo());
            }
        }
    }

}
